Microsoft_Wo-g_toan_cau.doc         HD�2HD�6                BOOKMOBI   0     0  �      �     �     +�     ;�     K�     [�     k�     {�     ��   	  ��   
  ��     ��     ˸     ۹     �     ��    �    �    +�    ;�    K�    [�    k�    {�    ��    ��    ��    ��    ��    ��    ��    ��     �   ! V   " X   # �<   $ �d   % �H   & �    ' ��   ( �`   ) s(   * 	Zd   + 
Ph   , 
�   - 
�(   . 
�T   /       � !     MOBI   �    �閧8   ����������������������������������������   #  �   ;                  #                   R                                ��������                      ,      .      -           ����    ��������   EXTH   �      d   maintt  ,   X              �                ���������������=�=�=�=�@�@�@�@�@�@�   �          �         �         �     P  Microsoft Word - Amstrong Ch. 19 Thuong truong toan cau.doc                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     <body><font size="-1">Niên khoá 2006-2007 </font> <font size="-2">Bài đọc Ch. 19 Thương trường toàn cầu </font> <b><h1>THƯƠNG TRƯỜNG TOÀN CẦU </h1> <h2>Tiếp thị toàn cầu trong thế kỷ hai mươi mốt</h2></b> <p>Th ế giới đang thu hẹp nhanh chóng với sự phát triển mau lẹ của viễn thông, giao thông và các dòng tài chính. Các sản phẩm được phát triển ở một quốc gia – ví da Gucci, hàng điện tử Sony, hamburger McDonald’s, sushi Nhật Bản, ô tô BMW của Đức – đang được đón nhận nhiệt tình tại các nước khác. Chúng ta không ngạc nhiên khi nghe chuyện một thương gia Đức mặc một bộ vét Ý gặp một người bạn Anh tại một nhà hàng Nhật rồi sau đó về nhà uống rượu vốt ka Nga và xem chương trình <i>West Wing</i> trên vô tuyến truyền hình. </p> <p>Th ương mại quốc tế đang bùng nổ. Từ năm 1969, số lượng công ty đa quốc gia trên thế giới tăng trưởng từ 7000 lên hơn 63000. Một số trong đó thực sự là những hãng khổng lồ. Trên thực tế, trong 100 “nền kinh tế” lớn nhất trên thế giới, chỉ có 47 là các quốc gia. Con số 53 còn lại là các công ty đa quốc gia. Exxon Mobile, công ty lớn nhất thế giới, có doanh thu hàng năm lớn hơn GDP của tất cả các nước ngoại trừ 20 quốc gia lớn nhất.</p> <p>Nh ập khẩu hàng hoá và dịch vụ hiện chiếm 24 phần trăm GDP trên toàn thế giới, gấp đôi mức của 40 năm trước đây. Thương mại quốc tế hiện chiếm một phần tư GDP của Hoa Kỳ, và từ năm 1996 đến 2006, xuất khẩu của Hoa Kỳ dự kiến tăng 51 phần trăm. Thương mại thế giới hiện chiếm 29 phần trăm GDP thế giới, tăng 10 phần trăm từ năm 1990.</p> <p>Nhi ều công ty Hoa Kỳ từ lâu đã thành công trong việc tiếp thị quốc tế: Coca Cola, General Electric, IBM, Gillette, Colgate, Caterpillar, Ford, Boeing, McDonald’s và hàng tá công ty Hoa Kỳ khác đã biến thế giới thành thị trường của họ. Và ở Hoa Kỳ, những tên tuổi như Sony, Toyota, BP, Nestlé, Nokia, Nestle, và Prudential đã trở thành ngôn ngữ của các hộ gia đình. Các sản phẩm và dịch vụ khác có vẻ như của người Mỹ thật ra lại được sản xuất hay được sở hữu bởi các công ty nước ngoài: sách Bantam, kem Baskin Robbins, vô tuyến truyền hình GE và RCA, sữa Carnaton, thực phẩm Pillsbury, hãng phim Universal, và khách sạn Motel 6, ấy là mới chỉ kể một vài trường hợp. Một nhà phân tích nhận xét: “Đã có hai phần ba lĩnh vực công nghiệp hoặc hoạt động trên toàn cầu, hoặc đang trong quá trình thực hiện điều đó. Michelin, hãng sản xuất săm lốp của Pháp, hiện đang kiếm được 35 phần trăm thu nhập ở Hoa Kỳ, trong khi Johnson &amp; Johnson thực hiện 43 phần trăm công việc kinh doanh ở nước ngoài … Phạm vi hoạt động của mỗi giám đốc là thế giới.”</p> <p>Nh ưng ngày nay, cạnh tranh toàn cầu đang ngày càng khốc liệt. Các công ty nước ngoài đang năng nổ mở rộng hoạt động vào các thị trường quốc tế mới, và các thị trường nước nhà không còn nhiều cơ hội nữa. Gần như không có ngành nào còn được an toàn trước sự cạnh tranh nước ngoài. Nếu các công ty chậm trễ tiến tới quốc tế hoá, họ sẽ có nguy cơ bị đứng ngoài các thị trường đang tăng trưởng ở Đông và Tây Âu, Trung Quốc và vành đai Thái Bình Dương, nước Nga và những nơi khác. Những công ty ở lại nước nhà để được an toàn chẳng những đánh mất cơ hội bước vào các thị trường khác mà còn có nguy cơ đánh mất cả thị tr ường nước nhà. Những công ty trong nước chưa từng nghĩ đến các đối thủ cạnh tranh nước ngoài bất thình lình nhận ra rằng các đối thủ đó hiện đã ở ngay trong sân sau nhà mình mất rồi.</p> <i>Nhiều công ty Hoa Kỳ đã biến thế giới thành thị trường của họ</i><br/> <img recindex="00001"/><p>Tr ớ trêu thay, trong khi nhu cầu hoạt động ở nước ngoài của các công ty hiện nay lớn hơn so với trước kia, thì rủi ro cũng vậy. Các công ty vươn ra nước ngoài có thể đứng trước đồng tiền và chính phủ các nước hết sức bất ổn, các qui định và chính sách nhà nước hạn chế, và các hàng rào thương mại cao ngất. Tham nhũng cũng là một vấn đề ngày càng gia tăng – các quan chức tại một số nước thường ban bố cho doanh nghiệp không phải dựa trên người bỏ thầu tốt nhất mà là người hối lộ nhiều nhất.</p> <p><b>Công ty toàn c ầu </b>là một công ty, thông qua hoạt động ở hơn một quốc gia, đạt được các lợi thế về tiếp thị, sản xuất, nghiên cứu phát triển, và tài chính mà những công ty nội địa thuần tuý không thể có được. Công ty toàn cầu xem thế giới như một thị trường. Họ tối thiểu hoá tầm quan trọng của biên giới quốc gia và phát triển các thương hiệu “xuyên quốc gia”. Họ huy động vốn, tiếp nhận nguyên liệu và linh kiện, sản xuất và tiếp thị hàng hoá bất kỳ nơi nào họ có thể làm được tốt nhất. Ví dụ, công ty thang máy Otis Elevator tiếp nhận hệ thống cửa thang máy từ Pháp, các linh kiện bánh răng nhỏ từ Tây Ban Nha, điện tử từ Đức, và các động cơ đặc biệt từ Nhật Bản. Họ chỉ sử dụng Hoa Kỳ như một nơi để hoà nhập hệ thống. Một chuyên gia tiếp thị toàn cầu nói: “Các biên giới là của thế kỷ 20. Các công ty xuyên quốc gia đưa tình trạng “không nhà nước” lên một trình độ mới.” </p> <p>Đ iều này không có nghĩa là các công ty qui mô vừa và nhỏ phải hoạt động trên hàng chục đất nước thì mới thành công. Các công ty này có thể len vào một khe nhỏ chuyên môn của thị trường toàn cầu. Nhưng thế giới đang dần dần trở nên nhỏ hơn, và mỗi công ty hoạt động trong một ngành công nghiệp toàn cầu - bất kể lớn hay nhỏ - phải đánh giá và xây dựng chỗ đứng của mình trên thị trường thế giới.</p> <p>Phong trào toàn c ầu hoá nhanh chóng có nghĩa là mọi công ty sẽ phải trả lời một số câu hỏi cơ bản: Chúng ta sẽ cố gắng thiết lập vị thế thị trường nào trong nước, trong khu vực kinh tế, và trên toàn cầu? Các đối thủ cạnh tranh toàn cầu của chúng ta là ai; các chiến lược và nguồn lực của họ là gì? Ta nên tự sản xuất hay gia công ngoài các sản phẩm? Ta nên thành lập các liên minh chiến lược gì với các công ty khác trên thế giới?</p> <p>Nh ư thể hiện qua hình 19.1, một công ty đứng trước sáu quyết định chính trong tiếp thị toàn cầu. Trong chương này, chúng ta sẽ thảo luận từng quyết định một cách chi tiết.</p> <b>Hình 19.1 Các quyết định chính trong tiếp thị toàn cầu </b><br/> <font size="-1">Quyết <p>Xem xét Quyết Quyết Quyết định về Quyết môi định tham định tham định làm chương định về tổ trường gia hoạt gia thị thế nào trình chức tiếp tiếp thị động quốc trường tham gia tiếp thị thị toàn toàn cầu tế nào thị trường toàn cầu cầu </p></font> <b>Công ty toàn cầu </b> <p>M ột công ty, thông qua hoạt động ở hơn một quốc g ia, đạt được các lợi thế về nghiên cứu phát triển, sản xuất, tiếp thị, và tài chính thể hiện qua chi phí cũng như danh tiếng mà những công ty nội địa thuần tuý không thể có được. </p> <b>Thuế quan </b><br/> Thuế mà chính phủ đánh vào hàng nhập khẩu, được thiết kế nhằm huy động thu ngân sách hay bảo hộ các công ty trong nước. <br/> <b>Hạn ngạch </b><br/> Hạn chế số lượng hàng hoá mà một nước nhập khẩu sẽ chấp nhận trong những chủng loại sản phẩm nhất định. <h2><b>Xem xét môi trường tiếp thị toàn cầu</b></h2> <p>Tr ước khi quyết định liệu có nên hoạt động trên thị trường quốc tế hay không, công ty phải am hiểu môi trường tiếp thị toàn cầu. Môi trường đó đã thay đổi nhiều trong hai thập niên qua, tạo ra nhiều vận hội mới cũng như những vấn đề mới. </p> <h2><b>Hệ thống thương mại quốc tế</b></h2> <p>Các công ty Hoa K ỳ nhìn ra nước ngoài phải bắt đầu bằng việc tìm hiểu <i>hệ thống thương mại </i>quốc tế. Khi bán hàng sang một nước khác, một công ty Hoa Kỳ đứng trước nhiều qui định hạn chế thương mại khác nhau. Phổ biến nhất là <b>thuế quan</b>, thuế mà chính phủ nước ngoài đánh vào những hàng hoá nhập khẩu nhất định. Thuế quan có thể được thiết kế để huy động số thu hay để bảo hộ các doanh nghiệp trong nước. Nhà xuất khẩu cũng có thể gặp phải <b>hạn ngạch</b> qui định số lượng hàng hoá mà đất nước nhập khẩu sẽ chấp nhận trong một số chủng loại hàng hoá cụ thể. Mục đích của hạn ngạch là giữ gìn ngoại hối, bảo hộ ngành công nghiệp và công việc làm của địa phương. Một <b>lệnh cấm vận</b>, hay tẩy chay, hay cấm hoàn toàn một số loại hàng nhập khẩu, là hình thức mạnh nhất của hạn ngạch. </p> <p>Các công ty Hoa K ỳ có thể gặp phải các biện pháp<b> kiểm soát ngoại hối,</b> hạn chế số lượng ngoại tệ và tỷ giá hối đoái so với các đồng tiền khác. Các công ty cũng có thể gặp phải các<b> hàng rào thương mại phi thuế</b>, như sự thành kiến chống lại việc bỏ thầu của các công ty Hoa Kỳ, hay các tiêu chuẩn sản phẩm hạn chế, hay những luật lệ khác không phù hợp với các tính năng sản phẩm của Hoa Kỳ: </p> <p>M ột trong những phương thức khôn khéo nhất mà người Nhật đã tìm ra để ngăn các nhà sản xuất nước ngoài tham gia thị trường nội địa của họ là lấy cớ “tính độc nhất”. Chính phủ Nhật Bản lập luận, làn da của người Nhật rất khác, cho nên các công ty mỹ phẩm nước ngoài phải thử nghiệm sản phẩm của họ ở Nhật trước khi bán. Người Nhật nói bao tử của họ nhỏ và chỉ đủ chỗ cho <i>mikan, </i> quả quýt địa phương, cho nên cam Hoa Kỳ nhập khẩu phải được hạn chế. Bây giờ người Nhật lại đưa ra điều mà có thể là một lập luận rất khập khiểng: Tuyết của họ rất khác, vì thế thiết bị trượt tuyết cũng khác. </p> <p>Đồ ng thời, cũng có một số áp lực <i> giúp ích</i> cho hoạt động thương mại giữa các quốc gia. Ví dụ như Hiệp định chung về thuế quan và mậu dịch (GATT) và các hiệp định mậu dịch tự do khu vực.</p> <p><b>L ệnh cấm vận </b><br/> Cấm nhập khẩu một sản phẩm cụ thể. <br/> <b>Các biện pháp kiểm soát ngoại hối </b></p> Qui định hạn chế của chính phủ về giá trị giao dịch ngoại hối với các nước khác và về tỷ giá hối đoái so với các đồng tiền khác. <br/> <b >Các hàng rào thương mại phi thuế</b> <p>Các hàng rào phi ti ền tệ đối với các sản phẩm nước ngoài, như sự thành kiến chống lại việc bỏ thấu của một công ty nước ngoài, hay các tiêu chuẩn sản phẩm ngược với các tính năng sản phẩm của một công ty nước ngoài. </p> <b>Tổ chức thương mại quốc tế và GATT </b> <p>Hi ệp định chung về thuế quan và mậu dịch (GATT) là một hiệp định từ 57 năm trước được thiết kế nhằm đẩy mạnh thương mại thế giới thông qua giảm thuế quan và các hàng rào thương mại quốc tế khác. Từ khi hiệp định được ký kết năm 1948, các quốc gia thành viên (hiện lên đến 146 thành viên) đã nhóm họp qua tám vòng đàm phán GATT để đánh giá lại các hàng rào thương mại và ấn định các qui tắc mới cho thương mại quốc tế. Bảy vòng đàm phán đầu tiên giúp giảm thuế suất bình quân đối với các hàng hoá công nghiệp chế tạo từ 45 phần trăm xuống chỉ còn 5 phần trăm. </p> <p>Vòng đàm phán GATT mới hoàn tất gần đây, vòng đàm phán Uruguay, kéo dài suốt 7 năm trời trước khi kết luận vào năm 1993. Lợi ích của vòng đàm phán này sẽ được cảm nhận trong nhiều năm khi hiệp định giúp đẩy mạnh tăng trưởng thương mại toàn cầu. Hiệp định hạ thấp thuế quan hàng hoá còn lại của thế giới thêm 30 phần trăm nữa, thúc đẩy thương mại hàng hoá toàn cầu thêm 10 phần trăm, hay 270 tỷ USD theo USD hiện hành cho đến năm 2002. Hiệp định cũng mở rộng GATT để bao hàm ngoại thương nông sản và dịch vụ, đồng thời thắt chặt việc bảo vệ bản quyền, bằng phát minh, nhãn hiệu hàng hoá, và các tài sản trí tuệ khác. </p> <p>Ngoài vi ệc hạ thấp các hàng rào thương mại và ấn định các tiêu chuẩn toàn cầu cho ngoại thương, vòng đàm phán Uruguay còn thành lập Tổ chức thương mại thế giới (WTO) để thực thi các qui tắc của GATT. Nói chung, WTO hành động như một tổ chức bao trùm, giám sát GATT, dàn xếp các vụ tranh chấp toàn cầu, và ban hành các biện pháp chế tài thương mại. Tổ chức GATT trước đây không bao giờ đặt ra những thẩm quyền như vậy. Một vòng đàm phán mới của GATT, vòng đàm phán Doha, bắt đầu ở Doha, Qatar, vào cuối năm 2001 và dự kiến đi đến kết luận vào tháng 1 năm 2005. </p> <p><i>WTO và GATT: Hi ệp định chung về thuế quan và mậu dịch (GATT) thúc đẩy thương mại thế giới thông qua hạ thấp thuế quan và các hàng rào thương mại khác. WTO, tổ chức giám sát GATT, bắt đầu vòng đàm phán mới ở Doha, Qatar vào cuối năm 2001. </i></p> <img recindex="00002"/><b>Các khu vực mậu dịch tự do </b> <p>M ột số quốc gia thành lập <i> khu vực mậu dịch tự do</i> hay các <b>cộng đồng kinh tế</b>. Đây là các nhóm nước tổ chức lại để hoạt động hướng tới các mục tiêu chung trong việc điều tiết hoạt động thương mại quốc tế. Ví dụ về một cộng đồng như vậy là<i> Liên minh châu Âu (EU)</i>. Được thành lập năm 1957, EU bắt đầu tạo ra một thị trường châu Âu duy nhất thông qua hạ thấp các rào cản để cho các dòng sản phẩm, dịch vụ, tài chính và lao động giữa các quốc gia thành viên lưu thông tự do, và xây dựng các chính sách thương mại với các quốc gia phi thành viên. Ngày nay, EU tiêu biểu cho một trong những thị trường lớn nhất trên thế giới. 25 quốc gia thành viên hiện tại bao gồm khoảng 448 triệu người tiêu dùng và chiếm hơn 20 phần trăm xuất khẩu của thế giớ�i. </p> <b>Cộng đồng kinh tế</b><br/> Một nhóm quốc gia tổ chức lại để hoạt động hướng tới các mục tiêu chung trong việc điều tiết hoạt động thương mại quốc tế. <p>S ự thống nhất châu Âu mang lại những cơ hội thương mại khổng lồ cho Hoa Kỳ và các công ty khác bên ngoài châu Âu. Tuy nhiên, điều đó cũng đặt ra những mối đe doạ. Như hệ quả của sự thống nhất, các công ty châu Âu sẽ lớn mạnh hơn và trở nên cạnh tranh hơn. Tuy nhiên, có lẽ mối quan ngại thật sự lớn hơn là các hàng rào thấp hơn <i>bên trong </i>châu Âu sẽ tạo ra những bức tường<i> bên ngoài </i>dày hơn. Một số nhà quan sát hình dung một “pháo đài châu Âu” chồng chất những thuận lợi thiên về phía các công ty từ các nước châu Âu nhưng cản trở các công ty bên ngoài thông qua dựng lên các chướng ngại. </p> <p>Quá trình ti ến tới thống nhất châu Âu đã diễn ra một cách chậm chạp – nhiều người còn nghi ngờ sự thống nhất hoàn chỉnh liệu có đạt được hay không. Tuy nhiên, trong những năm gần đây, 12 quốc gia thành viên đã có những bước tiến đáng kể hướng tới thống nhất thông qua sử dụng euro làm đồng tiền chung. Việc sử dụng rộng rãi đồng tiền chung giúp giảm phần lớn rủi ro tiền tệ gắn liền với việc kinh doanh ở châu Âu, làm cho những quốc gia thành viên có đồng tiền yếu trước đây trở thành những thị trường hấp dẫn hơn.</p> <p>Th ậm chí với việc sử dụng đồng euro, cũng không chắc rằng EU sẽ chống lại được 2000 năm truyền thống và trở thành “nước Mỹ của châu Âu”. Như một nhà quan sát hỏi: “Liệu một cộng đồng mà nói ít nhất một chục loại ngôn ngữ và có hàng tá nền văn hoá khác nhau có thực sự sánh bước cùng nhau và hoạt động như một thực thể thống nhất?” Cho dù các biên giới kinh tế và chính trị có thể được dỡ bỏ, nhưng sự khác biệt về văn hoá và xã hội vẫn còn. Và các công ty tiếp thị ở châu Âu sẽ đứng trước hàng đống luật lệ địa phương đến nản lòng. Tuy nhiên, ngay cả nếu chỉ thành công một phần, sự thống nhất vẫn làm cho châu Âu trở thành một lực lượng toàn cầu đáng nể. </p> <p>Ở Bắc Mỹ, Hoa Kỳ và Canada đã dần dần bãi bỏ các hàng rào thương mại vào năm 1989. Vào tháng 1 năm 1994, hiệp định Mậu dịch tự do Bắc Mỹ (NAFTA) thiết lập một khu vực mậu dịch tự do giữa Hoa Kỳ, Mexico và Canada. Hiệp định tạo ra một thị trường duy nhất cho 36 triệu dân, sản xuất và tiêu thụ 6,7 nghìn tỷ USD hàng hoá và dịch vụ. Khi được thực hiện trong 15 năm, NAFTA sẽ bãi bỏ hết mọi hàng rào thương mại và các biện pháp hạn chế đầu tư giữa ba quốc gia này. </p> <p>Nh ư vậy cho đến bây giờ, hiệp định đã cho phép hoạt động thương mại giữa các nước được khởi sắc. Mỗi ngày Hoa Kỳ trao đổi hơn nửa nghìn tỷ USD hàng hoá và dịch vụ với Canada, đối tác thương mại lớn nhất. Từ khi hiệp định được ký kết vào năm 1993, thương mại hàng hoá giữa Mexico và Hoa Kỳ tăng hơn ba lần, tổng cộng hiện nay là 232 tỷ USD. Trước thành công rõ rệt của NAFTA, các cuộc thảo luận hiện đang diễn ra để tìm cách thành lập một khu vực mậu dịch tự do châu Mỹ (FTAA). Khu vực mậu dịch tự do khổng lồ này sẽ bao gồm 34 quốc gia thành viên trải dài từ eo biển Bering cho đến mũi Horn, với dân số 800 triệu người, GDP kết hợp lên đến hơn 13 nghìn tỷ USD, và hơn 3,4 nghìn tỷ giá trị thương mại thế giới hàng năm.</p> <p><i>Các c ộng đồng kinh tế: Liên minh châu Âu tiêu biểu cho một trong những thị trường lớn nhất. 25 quốc gia thành viên hiện tại của Liên minh bao gồm hơn 448 triệu người tiêu dùng và chiếm 20 phần trăm xuất khẩu thế giới.</i></p> <img recindex="00003"/><p>Các khu v ực mậu dịch tự do khác đã được thành lập ở châu Mỹ Latin và Nam Mỹ. Ví dụ, MERCOSUR hiện đang liên kết 6 thành viên, bao gồm các thành viên đầy đủ Argentina, Brazil, Paraguay, và Uruguay và các thành viên dự bị là Bolivia và Chile. Với dân số hơn 200 triệu và một nền kinh tế kết hợp hơn 1 nghìn tỷ USD một năm, các quốc gia này tạo thành một khối mậu dịch lớn nhất sau NAFTA và Liên minh châu Âu. Có một cuộc thương thảo về hiệp định mậu dịch tự do giữa EU và MERCOSUR, và các quốc gia thành viên MERCOSUR đang xem xét việc sử dụng một đồng tiền chung, đồng merco.</p> <p>Cho dù xu h ướng thành lập các khu vực mậu dịch tự do gần đây đã gây ra sự phấn khích to lớn và các cơ hội thị trường mới, một số người xem đây là phúc hoạ lẫn lộn. Ví dụ, ở Hoa Kỳ, các công đoàn lo sợ rằng NAFTA sẽ dẫn đến sự di cư việc làm công nghiệp sang Mexico, nơi tiền lương thấp hơn rất nhiều. Các nhà môi trường lo lắng rằng các công ty không muốn chơi theo luật lệ nghiêm khắc của Cơ quan bảo vệ môi trường Hoa Kỳ sẽ di dời sang Mexico, nơi các qui định về ô nhiễm vẫn còn lỏng lẻo. </p> <p>M ỗi quốc gia có những đặc điểm độc đáo mà ta cần am hiểu. Sự sẵn sàng của một quốc gia trước các sản phẩm và dịch vụ khác nhau và sức thu hút của quốc gia đó như một thị trường đối với các công ty nước ngoài phụ thuộc vào môi trường kinh tế, chính trị pháp lý, và văn hoá của họ. </p> <h2><b>Môi trường kinh tế</b></h2> <p>Nhà ti ếp thị quốc tế phải nghiên cứu nền kinh tế của từng quốc gia. Hai yếu tố kinh tế phản ánh sức thu hút của một quốc gia như một thị trường là: cơ cấu công nghiệp và phân phối thu nhập.</p> <i>Cơ cấu công nghiệp </i>của một nước định hình các nhu cầu sản phẩm và dịch vụ, mức thu nhập và công việc làm của đất nước đó. Có bốn loại cơ cấu công nghiệp như sau: <p>• <i>Các nền kinh tế chỉ vừa đủ sống: </i>Trong một nền kinh tế chỉ vừa đủ sống, đại đa số dân chúng mưu sinh trong hoạt động nông nghiệp đơn giản. Họ tiêu dùng phần lớn sản lượng của họ và đổi phần còn lại lấy các hàng hoá và dịch vụ đơn giản. Họ ít có các cơ hội thị trường. </p> <p>• <i>Các nền kinh tế xuất khẩu nguyên liệu thô: </i>Các nền kinh tế này có một hay nhiều nguồn tài nguyên thiên nhiên phong phú nhưng nghèo nàn trong những lĩnh vực khác. Phần lớn thu nhập của họ hình thành từ xuất khẩu các tài nguyên này. Ví dụ như Chile (thiếc và đồng), Zaire (đồng, cobalt, và cà phê), và Saudi Arabia (dầu). Các quốc gia này là những thị trường tốt cho các thiết bị lớn, công cụ, vật tư, xe tải. Nếu có nhiều cư dân nước ngoài và một tầng lớp thượng lưu giàu có, họ cũng là một thị trường của những hàng hoá sang trọng. </p> <p>• <i>Các nền kinh tế đang công nghiệp hoá:</i> Trong một nền kinh tế đang công nghiệp hoá, công nghiệp chế tạo chiếm từ 10 đến 20 phần trăm nền kinh tế; ví dụ như Ai Cập, Ấn Độ, và Brazil. Khi sản xuất gia tăng, đ�ất nước cần nhiều hơn hàng nhập khẩu nguyên liệu dệt may, thép, máy móc thiết bị công nghiệp nặng, và cần ít hơn hàng nhập khẩu dệt may thành phẩm, sản phẩm giấy, và xe ô tô. Công nghiệp hoá thường tạo ra một tầng lớp dân chúng giàu có mới và một tầng lớp trung lưu nhỏ, nhưng tăng trưởng; cả hai tầng lớp này đều cần các loại hàng hoá nhập khẩu mới.</p> <p>• <i>Các nền kinh tế công nghiệp: </i>Các nền kinh tế công nghiệp là các nước xuất khẩu chính hàng công nghiệp chế tạo, dịch vụ, và các quỹ đầu tư. Họ mua bán hàng hoá giữa họ với nhau và cũng xuất khẩu sang các loại nền kinh tế khác những mặt hàng như nguyên liệu thô và bán thành phẩm. Các hoạt động công nghiệp đa dạng của các quốc gia công nghiệp và tầng lớp trung lưu đông đảo ở đó làm cho họ trở thành những thị trường phong phú cho đủ loại hàng hoá.</p> <p>Y ếu tố kinh tế thứ hai là <i>phân phối thu nhập </i>của một đất nước. Những nước có các nền kinh tế chỉ vừa đủ sống có thể bao gồm chủ yếu những hộ gia đình với thu nhập gia đình rất thấp. Ngược lại, các quốc gia công nghiệp có thể có các hộ gia đình thu nhập thấp, trung bình, và cao. Vẫn có những quốc gia chỉ có các hộ gia đình thu nhập rất thấp hoặc rất cao. Tuy nhiên, trong nhiều trường hợp, các nước nghèo hơn có thể có một tầng lớp người tiêu dùng tuy ít ỏi nhưng giàu có với thu nhập cao ngất. Đồng thời, ngay cả ở những nền kinh tế đang phát triển và thu nhập thấp, người ta vẫn tìm cách mua những sản phẩm quan trọng đối với họ: </p> <p>Giáo s ư triết học Nina Gladziuk suy nghĩ cẩn thận trước khi miễn cưỡng trả những đồng zloty khó nhọc của bà cho danh mục hàng tiêu dùng hoa cả mắt của Ba Lan. Nhưng chắc chắn bà phải chi tiêu thôi. Cho dù chỉ kiếm được 550 USD một tháng từ hai công việc học thuật, ở độ tuổi 41, Gladziuk đang tận hưởng thú vui mua sắm giúp làm thay đổi lối sống của bà sau nhiều năm khó khăn dưới cơ chế kinh tế kế hoạch tập trung. Trong những năm qua, bà đã trang bị một căn hộ mới trong một vùng dân cư được ưa thích gần cánh rừng Kabaty của Warsaw, phung phí chi tiêu cho những sản phẩm xinh đẹp được sản xuất ở nước ngoài, và trải qua một kỳ nghỉ cuối tuần ở Paris trước khi tham gia một cuộc hội thảo được trường đại học tài trợ … Hãy đáp ứng nhu cầu của tầng lớp người tiêu dùng đang tăng nhanh của Trung Âu. Từ những người lao động trí thức như Gladziuk cho đến người công nhân phân xưởng ở Budapest cho đến giới chuyên môn trẻ tuổi tân thời ở Prague, thu nhập gia tăng và sự tự tin trào dâng như hệ quả của bốn năm tăng trưởng kinh tế. Trong các nền kinh tế dẫn đầu khu vực - Cộng hoà Czech, Hungary, và Ba Lan - lớp khách hàng mới đang tăng trưởng chẳng những về số lượng mà cả về sự tinh tế. Gần một phần ba toàn bộ người Czech, người Hungary, và người Ba Lan - khoảng 17 triệu người - ở độ tuổi dưới ba mươi, với ước mơ về một cuộc sống tốt đẹp và thói quen mua sắm cần được cung ứng hàng hoá. Và họ đã khám phá ra Internet. Số dân trực tuyến trong khu vực đang tăng trưởng hơn 25 phần trăm một năm, và chi tiêu trên mạng gia tăng với tốc độ nhanh hơn nhiều.</p> <p><i>Các n ền kinh tế đang phát triển: Ở Đông Âu, các công ty đang cung ứng hàng hoá cho một tầng lớp khách hàng mới ước mơ về một cuộc sống tốt đẹp và thói quen mua sắm để đáp ứng những người đang nóng lòng đón lấy mọi thứ, từ hàng hoá tiêu dùng phương tây cho đến thời trang cao cấp và những chiếc điện thoại di động đời mới nhất.</i></p> <img recindex="00004"/><p>Nh ư vậy, các nhà tiếp thị quốc tế đứng trước nhiều thử thách trong việc tìm hiểu xem thử môi trường kinh tế sẽ ảnh hưởng như thế nào đến các quyết định về việc tham gia thị trường nào và tham gia như thế nào. </p> <h2><b>Môi trường chính trị pháp lý</b></h2> <p>Các qu ốc gia rất khác biệt nhau về môi trường chính trị pháp lý. Khi quyết định liệu có nên kinh doanh tại một đất nước nào đó, chí ít có bốn yếu tố chính trị pháp lý nên được xem xét: thái độ đối với việc mua hàng quốc tế, bộ máy chính phủ, sự ổn định chính trị và các qui định về tiền tệ. </p> <p>Trong <i>thái độ đối với việc mua hàng quốc tế, </i>một số quốc gia tương đối cởi mở trước các công ty nước ngoài trong khi một số nước khác khá chống đối. Ví dụ, Ấn Độ gây lo ngại cho các doanh nghiệp nước ngoài bằng hạn ngạch nhập khẩu, các qui định hạn chế tiền tệ, và hạn chế tỷ lệ phần trăm đội ngũ quản lý không phải là người bản xứ. Vì thế, nhiều công ty Hoa Kỳ đã rời bỏ Ấn Độ. Ngược lại, các quốc gia châu Á láng giềng như Singapore, Thái Lan, Malaysia, và Philippines lại ve vãn các nhà đầu tư nước ngoài và dồn dập trao cho họ các động cơ khuyến khích và các điều kiện hoạt động ưu đãi. </p> <p>Y ếu tố thứ hai là<i> bộ máy chính phủ</i> - mức độ mà chính phủ sở tại điều hành một hệ thống hiệu quả để giúp các công ty nước ngoài: thủ tục hải quan hiệu quả, thông tin thị trường tốt, và các yếu tố khác hỗ trợ hoạt động kinh doanh. Người Mỹ thường bị sốc trước việc các hàng rào thương mại tại một số quốc gia biến mất một cách nhanh chóng khi có một khoản tiền thích hợp (hối lộ) được trao cho một quan chức nào đó. </p> <p><i>Ổ n định chính trị</i> là một vấn đề khác. Toà nhà chính phủ có thể đổi chủ, và đôi khi sự thay đổi đó diễn ra bằng bạo lực. Ngay cả nếu không có thay đổi, chính phủ cũng có thể quyết định phản ứng trước những cảm giác phổ cập mới. Tài sản của các công ty nước ngoài có thể bị trưng thu, tiền bạc họ nắm giữ có thể bị khoá chặt, hay hạn ngạch nhập khẩu, hay các khoản thuế mới có thể được ban hành. Các nhà tiếp thị quốc tế có thể thấy có lãi khi hoạt động kinh doanh ở một đất nước bất ổn, nhưng tình huống bất ổn sẽ ảnh hưởng đến cách thức họ xử lý các vấn đề kinh doanh và tài chính. </p> <p>Cu ối cùng, các công ty cũng phải xem xét các <i>qui định về tiền tệ. </i> Người bán muốn giữ lợi nhuận bằng một đồng tiền có giá trị đối với họ. Một cách lý tưởng, người mua có thể trả bằng đồng tiền của người bán hay bằng các đồng tiền quốc tế khác. Thiếu điều này, người bán có thể chấp nhận một đồng tiền bị khoá – nghĩa là đồng tiền mà việc chuyển nó ra khỏi đất nước bị hạn chế bởi chính phủ của người mua - nếu họ có thể mua những hàng hoá khác ở đất nước đó cần thiết cho bản thân họ hay có thể bán ở nơi khác để lấy lại đồng tiền họ cần. Ngoài các hạn chế về đ�ồng tiền, tỷ giá hối đoái thay đổi cũng có thể mang lại những rủi ro lớn cho người bán. </p> <p>Ph ần lớn thương mại quốc tế liên quan đến giao dịch tiền mặt. Thế nhưng nhiều quốc gia có quá ít đồng tiền mạnh để trả cho hàng hoá mua từ các nước khác. Họ có thể muốn trả bằng những mặt hàng khác thay vì trả bằng tiền, điều này dẫn đến một thông lệ thực hành thương mại ngày càng tăng gọi là <b>hàng đổi hàng</b> (countertrade). Hàng đổi hàng tạo nên khoảng 20 phần trăm tổng thương mại thế giới. Hoạt động hàng đổi hàng có nhiều dạng: <i>Đổi hàng trực tiếp (barter)</i>, là trực tiếp trao đổi hàng hoá hay dịch vụ, như khi các nhà chăn nuôi gia súc Úc đổi bò còn sống lấy hàng hoá Indonesia như bia, dầu cọ, và xi măng. Một hình thức khác là<i> trả bù thành phẩm </i> hay<i> mua lại thành phẩm (compensation </i>hay<i> buyback</i>), trong đó người bán bán máy móc, thiết bị hay công nghệ cho một đất nước khác và đồng ý nhận thanh toán bằng sản phẩm tạo ra. Như vậy, Goodyear cung cấp nguyên liệu và đào tạo Trung Quốc vận hành một xưởng in để đổi lấy các nhãn hiệu in thành phẩm. Một hình thức khác là<i> đổi hàng gián tiếp (counterpurchase)</i> trong đó người bán nhận thanh toán bằng tiền mặt nhưng đồng ý chi tiêu một phần tiền đó ở đất nước người mua. Ví dụ, công ty Pepsi bán si rô cola cho Nga nhận đồng rúp và đồng ý mua rượu vốt ka Stolichnaya chế tạo tại Nga để bán tại Hoa Kỳ. </p> <b>Hàng đổi hàng </b><br/> Thương mại quốc tế liên quan đến việc trao đổi trực tiếp hay gián tiếp hàng hoá để lấy những hàng hoá khác thay cho tiền mặt. <p>Các doanh v ụ hàng đổi hàng có thể rất phức tạp. Ví dụ, một vài năm trước đây, hãng ô tô DaimlerChrysler đồng ý bán 30 chiếc xe tải cho Romania để đổi lấy 150 chiếc xe jeep của Romania, rồi sau đó bán xe jeep lại cho Ecuador để lấy chuối, rồi lại bán chuối cho một chuỗi siêu thị Đức lấy đồng tiền Đức. Thông qua quá trình xoay vòng này, DaimlerChrysler cuối cùng đã nhận được thanh toán bằng đồng tiền Đức.</p> <h2><b>Môi trường văn hoá</b></h2> <p>M ỗi quốc gia có những phong tục tập quán và những điều kiêng kỵ riêng. Khi thiết kế các chiến lược tiếp thị toàn cầu, các công ty phải tìm hiểu xem thử văn hoá ảnh hưởng như thế nào đến phản ứng của người tiêu dùng trong từng mảng thị trường thế giới. Đồng thời, họ cũng phải tìm hiểu xem thử các chiến lược của họ ảnh hưởng đến văn hoá địa phương ra sao.</p> <b>Tác động của văn hoá đối với chiến lược tiếp thị</b> <p>Ng ười bán phải xem xét cách thức người tiêu dùng ở những quốc gia khác nhau nghĩ về sản phẩm và sử dụng sản phẩm như thế nào trước khi lên kế hoạch cho một chương trình tiếp thị. Thường có những chuyện bất ngờ gây ngạc nhiên. Ví dụ, một người đàn ông Pháp bình quân sử dụng mỹ phẩm và chăm sóc nhan sắc nhiều gấp đôi so với vợ. Người Đức và người Pháp ăn nhiều mì ống Ý đóng gói hơn so với người Ý. Trẻ em Ý thích ăn các thanh sô cô la giữa các miếng bánh mì như một thức ăn nhẹ. Phụ nữ ở Tanzania sẽ không cho trẻ em ăn trứng vì sợ làm chúng hói đầu hay bất lực.</p> Các công ty bỏ qua những điểm khác biệt như vậy có thể phạm phải những sai lầm tốn kém và ngượng ngùng. Đây là một ví dụ: <p>McDonald’s và Coca Cola đã xoay xở trước việc xúc phạm cả thế giới Hồi giáo bằng cách đặt một lá cờ Saudi Arabi trên các bao bì của họ. Thiết kế lá cờ bao gồm một đoạn từ kinh Koran (thánh kinh của đạo Hồi), và các tín đồ Hồi giáo có cảm giác mãnh liệt rằng kinh thánh thiêng liêng của họ không bao giờ bị chèn lót và bị quăng vào sọt rác. Nike cũng đứng trước một tình huống tương tự tại các nước Ả Rập khi các tín đồ Hồi giáo phản đối một biểu trưng cách điệu “Air” trên các đôi giày của họ, có vẻ giống như chữ Ả Rập “Allah”. Nike xin lỗi và thu hồi các đôi giày về. </p> <p>Các chu ẩn mực và hành vi kinh doanh cũng rất khác nhau giữa các nước. Các giám đốc kinh doanh Hoa Kỳ cần được chỉ dẫn tường tận về những vấn đề này trước khi kinh doanh tại một đất nước khác. Đây là một vài ví dụ về các hành vi kinh doanh toàn cầu khác nhau:</p> <p>• Người Nam Mỹ rất thích ngồi hay đứng rất gần nhau khi họ nói chuyện kinh doanh - thật sự gần đến mức mũi họ gần như chạm nhau Các nhà kinh doanh Hoa Kỳ có xu hướng lùi dần khi người Nam Mỹ tiến tới gần hơn. Cả hai có thể đi đến chỗ bị xúc phạm. </p> <p>• Đàm phán nhanh và dứt khoát, vốn có tác dụng tốt ở các nơi khác trên thế giới, lại thường không thích hợp ở Nhật Bản và các quốc gia châu Á khác. Hơn nữa, khi tiếp xúc đối diện, các doanh nhân Nhật Bản hiếm khi nói “không”. Vì thế, người Mỹ có xu hướng trở nên mất kiên nhẫn bởi phải mất thời gian cho những câu trao đổi lịch sự về thời tiết hay những chủ đề khác trước khi đi vào công việc kinh doanh. Và họ trở nên thất vọng khi họ không biết họ đang ở đâu. Tuy nhiên, nếu người Mỹ mau lẹ vào đề, người Nhật sẽ cảm thấy hành vi này thật xúc phạm. </p> <p>• Khi các doanh nhân Hoa Kỳ trao đổi danh thiếp, mỗi bên thường chỉ nhìn lướt qua danh thiếp của bên kia rồi cất vào một chiếc ví để tham chiếu sau này. Tuy nhiên, ở Nhật Bản, các doanh nhân thường nghiêm túc xem xét danh thiếp lẫn nhau khi chào hỏi, cẩn thận lưu ý mối quan hệ công ty và vị trí chức vụ. Họ tỏ ra tôn trọng tấm danh thiếp cũng hệt như họ tôn trọng con người. Đồng thời, họ trao danh thiếp cho người quan trọng nhất trước tiên. </p> <p><i>S ơ suất trước những khác biệt về văn hoá có thể dẫn đến những sai lầm hết sức lúng túng. Khi Nike nhận thấy rằng biểu trưng “Air” cách điệu trên các đôi giày nhìn tương tự như chữ “Allah” trong ký tự Ả Rập, họ phải xin lỗi và thu hồi sản phẩm về. </i></p> <img recindex="00005"/>Cũng vì lẽ đó, những công ty am hiểu sắc thái văn hoá có thể sử dụng chúng để khai thác lợi thế khi định vị sản phẩm trên thị trường quốc tế. Ta hãy xem ví dụ sau: <p>M ột chương trình quảng cáo trên truyền hình trong những ngày này ở Ấn Độ cho thấy một bà mẹ đang chìm đắm trong mơ mộng: cô con gái của bà trong một cuộc thi sắc đẹp ăn mặc hệt như nàng công chúa Bạch Tuyết đang khiêu vũ trên sân khấu. Chiếc váy dài tha thướt của nàng trắng tinh khiết. Chất liệu vải của một ứng viên khác, một cô nàng đang nhảy múa ở đàng sau, màu xam xám. Chẳng lạ gì, nàng Bạch Tuyết giành được giải thưởng cao nhất. Bà mẹ bừng tỉnh trong tiếng cười của gia đình thắm thiết – và tự hào nhìn vào chiếc máy giặt Whirlpool White Magic. Màn quảng cá�o là thành quả của 14 tháng nghiên cứu của công ty Whirlpool về tâm lý người tiêu dùng Ấn Độ. Cùng với những điều khác, Whirlpool nhận thấy rằng các bà nội trợ Ấn Độ ưa thích vệ sinh và tinh khiết, mà họ gắn liền với màu trắng. Vấn đề là vải trắng thường xuống màu sau vài lần giặt máy với nguồn nước địa phương. Ngoài việc xúc động trước lòng yêu mến sự tinh khiết trong màn quảng cáo, công ty Wirlpool còn thiết kế máy giặt theo sở thích khách hàng để đặc biệt tốt với sợi vải trắng. Whirlpool hiện là thương hiệu hàng đầu trên thị trường máy giặt hoàn toàn tự động đang tăng trưởng nhanh của Ấn Độ. </p> <p>Nh ư vậy, am hiểu truyền thống văn hoá, thị hiếu và hành vi có thể giúp các công ty chẳng những tránh được những sai lầm bối rối mà còn tranh thủ được lợi thế của các cơ hội văn hoá hài hoà. </p> <p><i>Môi tr ường văn hoá toàn cầu: Thông qua am hiểu các sắc thái văn hoá, Whirlpool đã trở thành thương hiệu hàng đầu trên thị trường máy giặt hoàn toàn tự động đang tăng trưởng nhanh chóng của Ấn Độ. Công ty thiết kế loại máy giặt giúp quần áo trắng lại càng trắng hơn. </i></p> <img recindex="00006"/><b>Tác động của các chiến lược tiếp thị đối với văn hoá </b> <p>Trong khi các nhà ti ếp thị lo lắng về tác động của văn hoá đối với các chiến lược tiếp thị toàn cầu của họ, những người khác có thể lo âu về tác động của các chiến lược tiếp thị đối với văn hoá toàn cầu. Ví dụ, một số nhà phê bình lập luận rằng “toàn cầu hoá” có thể thật sự có nghĩa là “Mỹ hoá”:</p> <p>B ước vào các khu mua sắm, giữa các ổ bán thức ăn nhanh và các cửa hàng mái vòm, một nhóm thanh niên túm tụm vào một mớ hỗn độn quần kiểu lính lùng thùng, ván trượt tuyết, và những câu tiếng lóng. Họ thầm đánh giá một người phụ nữ chuệnh choạng đi qua mặc chiếc áo hiệu DKNY, tay cầm một tờ báo <i>Times</i>. Cô lướt qua một chàng trai đội một chiếc mũ bóng chày Yankee đang chuyện trò bên chiếc điện thoại Motorola về bộ phim Matin Scorsese mà anh xem tối qua. </p> <p>Đ ó là một quang cảnh Mỹ điển hình – chỉ có điều nó không phải ở Mỹ, mà ở Anh. Văn hoá Mỹ phổ biến đến thế. Quang cảnh đó có thể được nhìn thấy ở hàng chục thành phố, Buddapest hay Berlin, nếu không phải là Bogota hay Bordeaux. Thậm chí là Manila hay Moscow. Là một siêu cường toàn cầu không đối thủ, người Mỹ xuất khẩu văn hoá của họ trên một qui mô chưa từng thấy… Một đôi khi, ý tưởng Mỹ được truyền thông, như quyền cá nhân, tự do ngôn luận, và tôn trọng phụ nữ, và làm giàu cho văn hoá địa phương. Nhưng cũng có lúc, chủ nghĩa duy vật hay tệ hơn trở thành thông điệp và các truyền thống địa phương bị tiêu tán.</p> <p>Nh ững người phê phán lo ngại rằng ngày càng có nhiều người trên thế giới tiếp xúc với lối sống Mỹ trong thực phẩm họ ăn, những cửa hàng họ đi mua sắm, và các chương trình truyền hình hay phim ảnh họ xem thì họ sẽ càng đánh mất đi bản sắc văn hoá riêng mình. Họ cho rằng việc tiếp xúc với với giá trị và sản phẩm Mỹ sẽ xói mòn văn hoá của họ và Âu hoá thế giới.</p> <h2><b>Quyết định có nên tham gia thị trường thế giới hay không </b></h2> <p>Ch ẳng phải mọi công ty đều cần phải phiêu lưu vào thị trường thế giới để sống còn. Ví dụ, hầu hết các doanh nghiệp địa phương đều cần tiếp thị giỏi trên thị trường nội địa. Hoạt động nội địa dễ dàng hơn và an toàn hơn. Các nhà quản lý chẳng cần phải học hỏi ngôn ngữ và luật pháp của một đất nước khác. Họ không phải đối phó với những đồng tiền bất ổn, đứng trước tình trạng bấp bênh về chính trị và pháp lý, hay thiết kế lại sản phẩm để thích nghi với những kỳ vọng khác biệt của khách hàng. Tuy nhiên, những công ty hoạt động trong các ngành công nghiệp toàn cầu, phải cạnh tranh trên cơ sở toàn cầu để thành công. </p> <p>Có m ột số yếu tố lôi kéo các công ty vào đấu trường toàn cầu. Các đối thủ cạnh tranh toàn cầu có thể tấn công vào thị trường nội địa của công ty thông qua chào bán những sản phẩm tốt hơn hay giá rẻ hơn. Công ty có thể muốn phản công lại các đối thủ này trên thị trường nước nhà của họ để ràng buộc nguồn lực của họ. Hay thị trường nội địa của công ty có thể bị đình trệ hoặc thu hẹp, và các thị trường nước ngoài có thể tiêu biểu cho doanh số cao hơn và các cơ hội lợi nhuận. Hoặc khách hàng của công ty có thể mở rộng ra nước ngoài và đòi hỏi phải có các dịch vụ quốc tế đi kèm.</p> <p>Tr ước khi ra nước ngoài, công ty phải cân nhắc một số rủi ro và giải đáp nhiều câu hỏi về khả năng hoạt động toàn cầu của mình. Liệu công ty có am hiểu thị hiếu và hành vi mua hàng của người tiêu dùng tại các nước khác hay không? Liệu công ty có chào bán được những sản phẩm hấp dẫn cạnh tranh hay không? Liệu công ty có thích nghi được với văn hoá kinh doanh của các nước khác và làm ăn hữu hiệu với người dân nước ngoài? Liệu các nhà quản lý công ty có được những kinh nghiệm quốc tế cần thiết? Liệu ban giám đốc có cân nhắc đến tác động của các qui định và môi trường chính trị của các nước khác?</p> <p>Do nh ững khó khăn của việc tham gia thị trường nước ngoài, nhiều công ty không hành động cho đến khi một số tình huống hay biến cố đẩy họ ra đấu trường toàn cầu. Có ai đó - một nhà xuất khẩu trong nước, một nhà nhập khẩu nước ngoài, hay chính phủ nước ngoài - yêu cầu công ty bán hàng ra nước ngoài. Hoặc công ty bị dư thừa công suất và cần tìm thêm thị trường cho hàng hoá của họ. </p> <h2><b>Quyết định tham gia thị trường nào</b></h2> <p>Tr ước khi ra nước ngoài, công ty nên cố gắng xác định <i>các mục tiêu và chính sách tiếp thị</i> quốc tế. Công ty nên quyết định<i> khối lượng </i>hàng hoá họ muốn bán ra thị trường nước ngoài. Hầu hết các công ty chỉ khởi đầu với số lượng nhỏ ở hải ngoại. Một số công ty muốn duy trì mức độ nhỏ bé, xem thị trường quốc tế chỉ như một phần nhỏ công việc kinh doanh của họ. Một số công ty khác có những kế hoạch to lớn hơn, xem thị trường quốc tế ngang bằng hoặc thậm chí quan trọng hơn kinh doanh nội địa. </p> <p>Các công ty c ũng cần quyết định sẽ tiếp thị trên<i> bao nhiêu</i> quốc gia. Các công ty phải cẩn thận không dàn trải hoạt động quá mỏng hay mở rộng vượt quá năng lực của họ thông qua hoạt động ở quá nhiều quốc gia quá sớm. Kế đến, công ty cần quyết định về <i>loại</i> quốc gia nào công ty sẽ bước vào. Sức thu hút của một quốc gia phụ thuộc vào sản phẩm, các yếu tố địa lý, thu nhập, d ân số, môi trường chính trị, và các yếu tố khác. Người bán có thể thích một nhóm quốc gia nhất định hoặc một số khu vực cụ thể trên thế giới hơn. Trong những năm gần đây, nhiều thị trường mới đã nổi lên, mang lại những cơ hội lớn lao cũng như những thử thách nản lòng. </p> Sau khi liệt kê một số thị trường quốc tế khả dĩ, công ty phải sàng lọc và xếp hạng từng thị trường. Hãy xem ví dụ sau: <p>Nhi ều nhà tiếp thị mơ ước bán hàng cho 1,3 tỷ người dân Trung Quốc. Ví dụ, Colgate đang tiến hành một cuộc chiến cam go ở Trung Quốc, tìm cách kiểm soát thị trường kem đánh răng lớn nhất thế giới. Đất nước của những người không thường xuyên chải răng này mở ra một tiềm năng to lớn. Chỉ có 20 phần trăm dân nông thôn Trung Quốc chải răng hàng ngày, vì thế Colgate và các đối thủ cạnh tranh đang ra sức theo đuổi các chương trình quảng bá và giáo dục, từ các chiến dịch quảng cáo khổng lồ cho đến tham quan các trường học địa phương để tài trợ nghiên cứu chăm sóc răng miệng. Thông qua những nỗ lực như vậy trên thị trường 350 triệu USD này, Colgate đã mở rộng thị phần từ 7 phần trăm năm 1995 lên 35 phần trăm ngày nay, bất chấp sự cạnh tranh với một thương hiệu sở hữu nhà nước được quản lý bởi Unilever và Crest của Procter &amp; Gamble. </p> <p><i>Quy ết định tham gia thị trường Trung Quốc khổng lồ của Colgate xem ra khá đơn giản. Sử dụng các chương trình xúc tiến và giáo dục năng động, Colgate đã mở rộng thị phần từ 7 phần trăm lên 35 phần trăm trong không đầy một thập niên.</i></p> <img recindex="00007"/><p>Quy ết định tham gia thị trường Trung Quốc của Colgate xem ra khá thẳng thắn và đơn giản. Trung Quốc là một thị trường khổng lồ mà không có nhiều đối thủ cạnh tranh kỳ cựu. Ứng với tỷ lệ chải răng thấp, thị trường vốn đã to lớn này có thể tăng trưởng thậm chí còn to lớn hơn. Thế nhưng chúng ta vẫn còn thắc mắc liệu <i>chỉ riêng qui mô thị trường không thôi</i> có đủ là lý do để chọn Trung Quốc hay không. Colgate cũng phải xem xét những yếu tố khác: Liệu Colgate có thể khắc phục rào cản văn hoá và thuyết phục người tiêu dùng Trung Quốc chải răng thường xuyên hơn? Liệu Trung Quốc có kham nổi các công nghệ sản xuất và phân phối cần thiết? Liệu Colagate có thể tiếp tục cạnh tranh hữu hiệu với hàng tá đối thủ địa phương? Liệu chính quyền Trung Quốc có luôn ổn định và nâng đỡ? Thành công hiện nay của Colgate ở Trung Quốc cho thấy rằng họ có thể trả lời “Có” cho tất cả các câu hỏi này. Tuy nhiên, tương lai của công ty ở Trung Quốc vẫn tràn ngập những điều không chắc chắn.</p> <p>Các th ị trường toàn cầu nên được xếp hạng dựa trên một vài yếu tố, bao gồm qui mô thị trường, tăng trưởng thị trường, chi phí kinh doanh, lợi thế cạnh tranh, và mức độ rủi ro. Mục tiêu là xác định tiềm năng của từng thị trường, sử dụng các chỉ báo như trình bày trong bảng 19.1. Sau đó, nhà tiếp thị phải quyết định những thị trường nào mang lại sinh lợi dài hạn từ đầu tư cao nhất. </p> <p><b>B ảng 19.1 Các chỉ báo về tiềm năng thị trường <br/> Các đặc điểm nhân khẩu học </b></p> <p>Giáo d ục <br/> Qui mô và tăng trưởng dân số<br/> Cơ cấu tuổi dân số</p> <b>Các đặc điểm địa lý</b> <p>Khí h ậu <br/> Diện tích<br/> Mật độ  dân số - thành thị, nông thôn<br/> Cơ cấu giao thông và sự tiếp cận thị trường</p> <b>Các yếu tố kinh tế</b> <p>Qui mô và t ăng trưởng GDP <br/> Phân phối thu nhập <br/> Cơ sở hạ tầng công nghiệp <br/> Tài nguyên thiên nhiên<br/> Nguồn tài lực và nhân lực </p> <b>Các yếu tố xã hội </b> <p>L ối sống, niềm tin và giá trị của người tiêu dùng<br/> Chuẩn mực và phương pháp kinh doanh Chuẩn mực xã hội <br/> Ngôn ngữ</p> <b>Các yếu tố chính trị và pháp lý </b> <p>Các ưu tiên của quốc gia<br/> Sự ổn định chính trị<br/> Thái độ của chính phủ đối với thương mại toàn cầu <br/> Bộ máy nhà nước <br/> Các qui định về tiền tệ và thương mại </p> <h2><b>Quyết định làm thế nào tham gia thị trường </b></h2> <p>M ột khi công ty đã quyết định bán hàng vào thị trường nước ngoài, công ty phải xác định phương thức tham gia tốt nhất. Các phương án chọn lựa của công ty là<i> xuất khẩu, liên doanh, </i>và <i>đầu tư trực tiếp</i>. Hình 19.2 trình bày ba chiến lược tham gia thị trường cùng với các phương án mà từng chiến lược mang lại. Như hình này cho thấy, từng chiến lược kế tiếp theo liên quan đến sự cam kết và rủi ro nhiều hơn, đồng thời sự kiểm soát và lợi nhuận tiềm năng cũng lớn hơn. </p> <b>Hình 19.2 Các chiến lược tham gia thị trường <br/> Xuất khẩu Liên doanh Đầu tư trực tiếp </b> Gián tiếp Cấp phép nhượng quyền Phương tiện lắp ráp Trực tiếp Hợp đồng sản xuất Phương tiện sản xuất Hợp đồng quản lý<br/> Liên kết sở hữu <br/> <i>Mức độ cam kết, rủi ro, kiểm soát, và lợi nhuận tiềm năng</i> <h2><b>Xuất khẩu </b></h2> <p>Cách đơn giản nhất để bước vào một thị trường nước ngoài là thông qua <b>xuất khẩu</b>. Công ty có thể thỉnh thoảng xuất khẩu một cách thụ động những lượng hàng hoá thặng dư, hoặc công ty có thể chủ động mở rộng xuất khẩu vào một thị trường cụ thể. Trong bất luận trường hợp nào, công ty cũng sản xuất toàn bộ hàng hoá ở nước nhà. Công ty có thể sửa đổi hàng hoá đôi chút hoặc cứ giữ nguyên như thế trên thị trường xuất khẩu. Việc xuất khẩu liên quan đến sự thay đổi ít nhất trong các tuyến sản phẩm, tổ chức, đầu tư, hay sứ mệnh của công ty.</p> <b>Xuất khẩu </b><br/> Tham gia vào một thị trường nước ngoài thông qua bán hàng hoá sản xuất ở nước nhà, thường ít có sự thay đổi điều chỉnh hàng hoá. <p>Các công ty th ường bắt đầu bằng <i> xuất khẩu gián tiếp</i>, làm việc thông qua các trung gian tiếp thị quốc tế độc lập. Xuất khẩu gián tiếp liên quan đến sự đầu tư ít nhất vì công ty không cần phải giám sát tổ chức tiếp thị hay thiết lập các đầu mối quan hệ. Nó cũng ít rủi ro hơn. Các trung gian tiếp thị quốc tế mang bí quyết và dịch vụ đến cho các đầu mối quan hệ, vì thế người bán thường ít mắc phải sai lầm hơn.</p> <p>Sau cùng ng ười bán cũng có thể chuyển sang<i> xuất khẩu trực tiếp, </i>trong đó họ tự lo liệu việc xuất khẩu. Việc đầu tư và rủi ro sẽ tương đối nhiều hơn trong chiến lược này, nhưng sinh lợi tiềm năng cũng lớn hơn. Công ty có thể thực hiện việc xuất khẩu trực tiếp theo nhiều cách: Công ty có thể thiết lập một phòng xuất khẩu trong nước để tiến hành các hoạt động xuất khẩu. Công ty cũng có thể thành lập một chi nhánh bán hàng ở nước ngoài để lo liệu việc bán hàng, phân  phối, và quảng bá xúc tiến bán hàng. Chi nhánh bán hàng giúp công ty có được sự hiện diện và kiểm soát chương trình nhiều hơn tại thị trường nước ngoài và thường đóng vai trò như một trung tâm trưng bày và trung tâm dịch vụ khách hàng. Thỉnh thoảng công ty cũng có thể đưa nhân viên bán hàng của công ty ra nước ngoài để tìm kiếm đối tác, khách hàng. Cuối cùng, công ty có thể thực hiện việc xuất khẩu thông qua các nhà phân phối nước ngoài mua và sở hữu hàng hoá của công ty, hay thông qua các đại lý nước ngoài, đại diện cho công ty để bán hàng.</p> <h2><b>Liên doanh </b></h2> <p>Ph ương pháp thứ hai để tham gia thị trường nước ngoài là<b> liên doanh </b>- kết hợp với một công ty nước ngoài để sản xuất hay tiếp thị sản phẩm hay dịch vụ. Liên doanh khác với xuất khẩu ở chỗ công ty kết hợp với một đối tác ở nước sở tại để bán hay tiếp thị ra nước ngoài. Nó khác với đầu tư trực tiếp ở chỗ có sự kết hợp với ai đó ở nước ngoài. Có bốn loại liên doanh: cấp phép nhượng quyền, hợp đồng sản xuất, hợp đồng quản lý, và liên kết sở hữu.</p> <b>Cấp phép nhượng quyền </b> <p>C ấp phép nhượng quyền là cách đơn giản nhất để một nhà sản xuất thực hiện việc tiếp thị quốc tế. Công ty ký kết hợp đồng với một đơn vị được nhượng quyền trên thị trường nước ngoài. Với một khoản phí bản quyền, đơn vị được nhượng quyền này mua quyền sử dụng qui trình sản xuất của công ty, nhãn hiệu hàng hoá, bằng phát minh, bí mật thương mại, hay các khoản mục có giá trị khác. Như vậy, công ty bước vào thị trường với ít rủi ro nhất; đơn vị được nhượng quyền tiếp nhận kỹ năng sản xuất hay một sản phẩm có tên tuổi mà không phải bắt đầu từ con số không. </p> <b>Liên doanh </b><br/> Tham gia vào các thị trường nước ngoài thông qua kết hợp với các công ty nước ngoài để sản xuất hay tiếp thị hàng hoá hay dịch vụ. <br/> <b>Cấp phép nhượng quyền </b> <p>M ột phương pháp để tham gia thị trường nước ngoài trong đó công ty ký kết hợp đồng với một đơn vị được nhượng quyền trên thị trường nước ngoài, cho đơn vị đó được quyền sử dụng qui trình sản xuất, nhãn hiệu hàng hoá, bằng phát minh, bí mật thương mại, hay các khoản mục giá trị khác với một khoản phí nhượng quyền. </p> <p>Coca Cola ti ếp thị toàn cầu thông qua cấp phép nhượng quyền đóng chai trên khắp thế giới và cung ứng si rô cho các đơn vị được nhượng quyền để họ sản xuất sản phẩm. Ở Nhật Bản, bia Budweiser chảy ra từ nhà máy bia Kirin, kem Lady Borden được sản xuất từ trại sản xuất bơ sữa Meiji, và thuốc lá Marlboro được cuộn trên dây chuyền sản xuất ở nhà máy thuốc lá Nhật Bản. Nhà môi giới trực tuyến E*TRADE đã thiết lập các trang web mang thương hiệu E*TRADE theo hợp đồng nhượng quyền tại một vài nước. Và công viên Disneyland ở Tokyo được sở hữu và điều hành bởi công ty Oriental Land của Nhật Bản theo giấy phép từ công ty Walt Disney Hoa Kỳ. </p> <p>Tuy nhiên, h ợp đồng nhượng quyền cũng có một số nhược điểm tiềm tàng. Công ty ít có sự kiểm soát đối với đơn vị được nhượng quyền hơn so với khi công ty sở hữu các phương tiện sản xuất riêng. Ngoài ra, nếu đơn vị được nhượng quyền rất thành công thì công ty đã bỏ mất khoản lợi nhuận này; đ�ồng thời, nếu và khi hợp đồng kết thúc, công ty có thể nhận ra họ đã tạo ra một đối thủ cạnh tranh.</p> <b>Hợp đồng sản xuất </b> <p>M ột phương án khác là hợp đồng sản xuất – công ty hợp đồng với một nhà sản xuất trên thị trường nước ngoài để sản xuất sản phẩm hay cung ứng dịch vụ của công ty. Công ty Sears sử dụng phương pháp này để mở các cửa hàng ở Mexico và Tây Ban Nha, ở đó công ty tìm thấy các nhà sản xuất địa phương đủ tiêu chuẩn để sản xuất phần lớn các sản phẩm mà công ty bán ra. Nhược điểm của hợp đồng sản xuất là việc kiểm soát qui trình sản xuất giảm sút và mất đi lợi nhuận tiềm tàng từ sản xuất. Lợi ích của phương án này là khởi sự nhanh hơn, ít rủi ro hơn, và có cơ hội sau này hợp tác hoặc mua lại nhà sản xuất địa phương.</p> <b>Hợp đồng quản lý </b> <p>Theo ph ương án<b> hợp đồng quản lý</b>, công ty trong nước cung ứng kỹ năng quản lý cho một công ty nước ngoài mà cung ứng vốn tại thị trường nước ngoài đó. Công ty trong nước xuất khẩu dịch vụ quản lý chứ không phải sản phẩm. Hilton sử dụng phương án này để quản lý các khách sạn trên thế giới. </p> <p>H ợp đồng quản lý là một phương pháp có rủi ro thấp để tham gia vào thị trường nước ngoài, và mang lại thu nhập ngay từ đầu. Thoả thuận này thậm chí còn hấp dẫn hơn nếu công ty hợp đồng có quyền chọn mua một số cổ phần trong công ty được quản lý về sau. Tuy nhiên, thoả thuận sẽ không khôn ngoan nếu công ty có thể sử dụng tài năng quản lý khan hiếm trong những trường hợp tốt hơn, hay nếu công ty có thể kiếm được nhiều lợi nhuận hơn thông qua thực hiện toàn bộ công việc kinh doanh. Hợp đồng quản lý cũng làm cho công ty không thể thiết lập hoạt động riêng của mình trong một thời gian. </p> <b>Hợp đồng sản xuất </b><br/> Một liên doanh, trong đó một công ty hợp đồng với nhà sản xuất tại thị trường nước ngoài để sản xuất sản phẩm hay cung ứng dịch vụ của công ty. <br/> <b>Hợp đồng quản lý</b> <p>M ột liên doanh, trong đó công ty trong nước cung ứng kỹ năng quản lý cho một công ty nước ngoài cung ứng vốn; công ty trong nước xuất khẩu dịch vụ chứ không xuất khẩu sản phẩm. </p> <p><i>C ấp phép nhượng quyền: Tokyo Disneyland được sở hữu và điều hành bởi công ty Oriental Land (một công ty phát triển của Nhật Bản), theo giấy phép nhượng quyền của công ty Walt Disney Hoa Kỳ. </i></p> <img recindex="00008"/><b>Liên kết sở hữu </b> <p>M ột công ty liên doanh liên kết sở hữu bao gồm một công ty kết hợp với một nhà đầu tư nước ngoài để thành lập một doanh nghiệp địa phương, trong đó, họ cùng chia xẻ quyền sở hữu và kiểm soát. Công ty có thể mua lại quyền lợi trong một công ty địa phương, hay hai bên có thể thành lập một công ty liên doanh mới. Liên kết sở hữu có thể cần thiết vì những lý do kinh tế hay chính trị. Công ty có thể thiếu nguồn lực tài chính, vật chất, hay quản lý để thực hiện doanh nghiệp một cách đơn độc. Hay chính phủ nước ngoài có thể đòi hỏi phải liên kết sở hữu như một điều kiện để tham gia thị trường nước họ. </p> <b>Liên kết sở hữu </b> <p>M ột liên doanh, trong đó công ty kết hợp với một nhà đầu tư ở thị trường nước ngoài để tạo ra một doanh nghiệp địa phương, và hai bên cùng chia xẻ quyền sở hữu và kiểm soát. </p> <p>KFC tham gia th ị trường Nhật Bản thông qua một công ty liên doanh liên kết sở hữu với tập đoàn Mitsubishi Nhật Bản. KFC đi tìm một phương cách tốt đẹp để tham gia thị trường thức ăn nhanh rộng lớn nhưng khó khăn của Nhật Bản. Đổi lại, Mitsubishi, một trong những nhà sản xuất thịt gia cầm lớn nhất Nhật Bản, am hiểu văn hoá Nhật Bản và có tiền để đầu tư. Hợp lại, họ giúp KFC thành công trên thị trường Nhật Bản nửa khép nửa mở. Thật ngạc nhiên, với sự dìu dắt của Mitsubishi, KFC đã dứt khoát triển khai một vị thế phi Nhật Bản cho các cửa hàng Nhật của họ: </p> <p>Khi KFC l ần đầu tiên bước chân vào thị trường Nhật Bản, người Nhật không cảm thấy thoải mái với ý tưởng về thức ăn nhanh và nhượng quyền. Họ cho rằng thức ăn nhanh là giả tạo không tự nhiên và không lành mạnh. Để xây dựng niềm tin, KFC xây dựng các chương trình quảng cáo mô tả phiên bản xác thực nhất của những ngày đầu của Đại tá Sander khả dĩ. Các chương trình quảng cáo mô tả nét nổi bật của bà mẹ miền nam hoàn hảo và nêu bật triết lý của KFC – lòng hiếu khách miền nam, truyền thống xa xưa của người Mỹ, và việc nấu nướng tại nhà đích thực. Với “Mái nhà xưa Kentucky của tôi” do Stephen Foster chơi nhạc nền, pha quảng cáo cho thấy bà mẹ đại tá Sander đang nấu nướng và cho các cháu ăn món gà rán KFC được chế biến với 11 gia vị bí mật. Nó gợi lên hình ảnh làm bếp trong gia đình từ miền nam nước Mỹ, định vị KFC như một thực phẩm ngon lành, quý tộc. Cuối cùng, gần như không có đủ món gà rán đặc biệt này để cung ứng cho người Nhật. Chiến dịch thành công rực rỡ, và không đầy tám năm, KFC đã mở rộng sự có mặt của họ từ 400 điểm bán hàng lên đến hơn 1000. Hầu hết người Nhật bây giờ đã thuộc lòng “Mái nhà xưa Kentucky của tôi”.</p> <p>Liên k ết sở hữu có những nhược điểm nhất định. Các đối tác có thể bất đồng về các chính sách đầu tư, tiếp thị, hay các chính sách khác. Trong khi nhiều công ty Hoa Kỳ muốn tái đầu tư thu nhập để tăng trưởng, các công ty địa phương thường thích được nhận thu nhập; và trong khi các công ty Hoa Kỳ chú trọng vào vai trò của tiếp thị, các nhà đầu tư trong nước có thể chỉ dựa vào việc bán hàng. </p> <h2><b>Đầu tư trực tiếp </b></h2> <p>S ự tham gia nhiều nhất của công ty vào một thị trường nước ngoài đạt được thông qua <b>đầu tư trực tiếp </b>– phát triển các phương tiện sản xuất hay lắp ráp đặt cơ sở ngay tại nước ngoài. Nếu công ty đã có kinh nghiệm trong hoạt động xuất khẩu và nếu thị trường nước ngoài đủ lớn, các phương tiện sản xuất nước ngoài mang lại nhiều lợi thế. Công ty có thể có chi phí thấp hơn dưới hình thức lao động hay nguyên vật liệu rẻ hơn, các động cơ khuyến khích của chính phủ nước ngoài, và tiết kiệm phí vận chuyển. Công ty có thể cải thiện hình ảnh của mình tại nước sở tại nhờ tạo ra công việc làm. Nói chung, công ty sẽ phát triển mối quan hệ sâu sắc hơn với chính phủ, khách hàng, các nhà cung ứng địa phương, và các nhà phân phối, cho phép công ty điều chỉnh sản phẩm thích ứng với thị trường địa phương hơn. Cuối cùng, công ty duy trì được sự kiểm soát hoàn toàn việc đầu tư và do đó, có thể triển khai các chính  sách sản xuất và tiếp thị phục vụ các mục tiêu quốc tế dài hạn. </p> <p>Nh ược điểm chính của đầu tư trực tiếp là công ty đứng trước nhiều rủi ro, như đồng tiền bị hạn chế và phá giá, thị trường sa sút, hay những thay đổi của chính phủ. Trong một số trường hợp, công ty không có chọn lựa nào khác ngoại trừ chấp nhận những rủi ro này nếu muốn hoạt động tại nước sở tại. </p> <b>Đầu tư trực tiếp </b><br/> Bước vào thị trường nước ngoài thông qua xây dựng các phương tiện sản xuất hay lắp ráp đặt cơ sở tại nước ngoài. <h2><b>Quyết định về chương trình tiếp thị toàn cầu</b></h2> <p>Các công ty ho ạt động tại một hay nhiều thị trường nước ngoài phải quyết định điều chỉnh tổ hợp tiếp thị thích ứng với điều kiện địa phương đến mức nào. Trong một trường hợp cực đoan, các công ty toàn cầu sử dụng một <b>tổ hợp tiếp thị chuẩn hoá</b>, chủ yếu bán cùng những loại sản phẩm và sử dụng các phương pháp tiếp thị như nhau trên toàn thế giới. Ở cực đoan ngược lại, công ty sử dụng<b> tổ hợp tiếp thị điều chỉnh.</b> Trong trường hợp này, nhà sản xuất điều chỉnh các yếu tố trong tổ hợp tiếp thị cho từng thị trường mục tiêu, chịu chi phí nhiều hơn nhưng hy vọng đạt được thị phần và sinh lợi nhiều hơn. </p> <p>V ấn đề nên điều chỉnh hay chuẩn hoá tổ hợp tiếp thị từng được tranh luận nhiều trong những năm gần đây. Một mặt, một số nhà tiếp thị toàn cầu tin rằng công nghệ làm cho thế giới trở nên nhỏ hẹp hơn, và các nhu cầu của người tiêu dùng trên thế giới đang trở nên tương tự nhau nhiều hơn. Niềm tin này lát đường cho những “thương hiệu toàn cầu” và việc tiếp thị toàn cầu chuẩn hoá. Đổi lại, thương hiệu và sự chuẩn hoá toàn cầu mang lại sức mạnh thương hiệu to lớn hơn và chi phí hạ hơn nhờ lợi thế kinh tế theo qui mô. </p> <p>M ặt khác, khái niệm tiếp thị cho rằng các chương trình tiếp thị sẽ hữu hiệu hơn nếu được sửa đổi theo nhu cầu riêng của từng nhóm khách hàng mục tiêu. Nếu khái niệm này áp dụng trong một đất nước, thì nó sẽ được áp dụng thậm chí còn nhiều hơn trên các thị trường quốc tế. Bất chấp sự hội tụ toàn cầu, người tiêu dùng tại các quốc gia khác nhau vẫn vô cùng khác biệt nhau về nền tảng văn hoá. Họ còn hết sức khác nhau về nhu cầu, mong muốn, sức chi tiêu, thị hiếu sản phẩm, và phương thức mua sắm. Vì những khác biệt này khó mà thay đổi, phần lớn các nhà tiếp thị điều chỉnh sản phẩm, giá cả, kênh lưu thông, và hoạt động quảng bá xúc tiến để phù hợp với mong muốn của người tiêu dùng tại mỗi nước. </p> <b>Tổ hợp tiếp thị chuẩn hoá </b> <p>M ột chiến lược tiếp thị quốc tế để về cơ bản sử dụng cùng một sản phẩm, chương trình quảng cáo, các kênh phân phối, và các yếu tố khác trong tổ hợp tiếp thị trên tất cả các thị trường nước ngoài của công ty. </p> <b>Tổ hợp tiếp thị điều chỉnh </b> <p>M ột chiến lược tiếp thị quốc tế để điều chỉnh các yếu tố trong tổ hợp tiếp thị cho từng thị trường mục tiêu quốc tế, chịu chi phí nhiều hơn nhưng hy vọng có được thị phần và sinh lợi nhiều hơn. </p> <p>Tuy nhiên, ti ếp thị toàn cầu không phải là một vấn đề dứt khoát phải theo bên này hay bên kia, mà đúng hơn là vấn đề�� về mức độ. Hầu hết các nhà tiếp thị quốc tế cho rằng các công ty nên “tư duy toàn cầu nhưng hành động địa phương” - rằng họ nên tìm cách cân đối giữa chuẩn hoá và điều chỉnh. Các nhà tiếp thị này chủ trương một chiến lược “toàn cầu”, trong đó công ty chuẩn hoá các yếu tố tiếp thị cốt lõi và địa phương hoá những yếu tố khác. Cấp công ty sẽ đề ra phương hướng chiến lược toàn cầu; các đơn vị địa phương sẽ tập trung vào các khác biệt cụ thể của người tiêu dùng giữa các thị trường toàn cầu. Simon Clift, lãnh đạo tiếp thị hàng hoá tiêu dùng toàn cầu của công ty Unilever, cho rằng: “Chúng ta đang cố gắng cân đối giữa việc trở nên toàn cầu một cách thiếu suy xét và trở nên địa phương một cách vô vọng.” </p> <p>L’Oreal, m ột công ty sản phẩm chăm sóc cá nhân toàn cầu hết sức thành công, hoạt động theo cách này. Công ty tiếp thị các thương hiệu toàn cầu nhưng điều chỉnh chúng để đáp ứng các sắc thái văn hoá của từng thị trường địa phương (xem Tiếp thị thực tế 19.2). Tương tự, McDonald’s sử dụng cùng một công thức hoạt động cơ bản như vậy trong các nhà hàng trên khắp thế giới, nhưng điều chỉnh thực đơn theo thị hiếu địa phương. Họ sử dụng tương ớt thay cho sốt cà chua nấm cho hamburger ở Mexico. Ở Hàn Quốc, họ bán thịt heo nướng kèm bánh bao với nước tương có vị tỏi. Ở Ấn Độ, nơi bò được xem là loài vật linh thiêng, McDonald’s phục vụ burger gà, cá, rau cải, pizza McPuffs, McAloo Tikki (burger cà chua xắt lát), và Maharaja Mac – hai miếng thịt cừu, nước sốt đặc biệt, rau diếp, phô mai, dưa chua, hành, trên một chiếc bánh bao vừng.</p> <p><b>Ti ếp thị thực tế 19.2 L’Oreal điều chỉnh các thương hiệu toàn cầu cho phù hợp với văn hoá địa phương </b></p> <p>Làm th ế nào một công ty Pháp với tổng<br/> giám đốc điều hành người Anh tiếp thị<br/> thành công phiên bản Nhật của một loại <br/> son môi Mỹ trên thị trường nước Nga?<br/> Hãy hỏi L’Oreal, một công ty sản phẩm <br/> chăm sóc cá nhân quốc tế hết sức thành<br/> công. Với trụ sở đặt tại Pháp, L’Oreal <br/> bán hơn 17 tỷ USD mỹ phẩm, sản <br/> phẩm chăm sóc tóc, chất thơm và nước <br/> hoa mỗi năm ở 150 quốc gia trên toàn<br/> cầu, trở thành công ty mỹ phẩm lớn <br/> nhất thế giới. Có đến 85 sản phẩm được <br/> bán ra mỗi giây, chiếm 13 phần trăm <br/> toàn bộ mỹ phẩm được tiêu thụ trên thế<br/> giới. <br/> Danh mục tổng quát các thương hiệu <br/> toàn cầu của L’Oreal bao gồm Garnier, <br/> Maybelline, Redken, Lancome, Helena<br/> Ví dụ, năm 1996 công ty mua lại nhà sản xuất mỹ phẩm trang điểm tẻ nhạt của Mỹ, Maybelline. Để tiếp lại sinh lực và toàn cầu hoá thương hiệu này, công ty chuyển các trụ sở đơn vị từ Tennessee về thành phố New York, và thêm chữ “New York” vào nhãn hiệu. Biểu tượng quả táo lớn mang nét bảnh bao phố thị, đã có tác dụng tốt, định vị mức giá vừa phải của một thương hiệu mỹ phẩm trang điểm thường ngày. Sự thay đổi mang lại cho Maybelline 20 phần trăm thị phần trong chủng loại sản phẩm này ở Tây Âu. Việc định vị sản phẩm nhắm vào giới trẻ đô thị cũng thắng lợi ở châu Á. Như một nhà phân tích ngành nhận định:<br/> <i>Bí mật thành công quốc tế đáng kinh ngạc của L’Oreal là gì? Công ty tiếp thị các thương hiệu toàn cầ!�u thông qua tìm hiểu xem chúng hấp </i></p> <p>Rubinsteim, Kiehl’s, Biotherm, <i>n như thế nào trước bản sắc văn hoá trên </i> Softsheen Carson, Vichy, và nước hoa<br/> Ralph Lauren và Giorgio Armani. Điều <br/> ấn tượng là, L’Oreal đã đạt được tỷ lệ<br/> tăng trưởng lợi nhuận quốc tế hai chữ<br/> số suốt 19 năm liền. Năm ngoái, doanh <br/> số tăng vọt gần 70 phần trăm ở Trung<br/> Quốc, 40 phần trăm ở Nga, và 30 phần <br/> trăm ở Ấn Độ. <br/> Bí mật của thành công quốc tế đáng<br/> kinh ngạc của L’Oreal là gì? Công ty<br/> tiếp thị thương hiệu toàn cầu thông qua<br/> tìm hiểu xem thử sản phẩm hấp dẫn <br/> như thế nào trước bản sắc văn hoá trên<br/> từng thị trường địa phương. Một người <br/> quan sát nói: “L’Oreal chỉ là của nước <br/> Pháp khi nào họ muốn. Còn những lúc <br/> khác, công ty này vui vẻ trở thành của <br/> châu Phi, châu Á, hay bất kỳ nơi nào <br/> khác mà công ty bán hàng”. Nhà bán lẻ<br/> mỹ phẩm khổng lồ mua các thương<br/> hiệu địa phương, tô điểm lại đôi chút,<br/> và xuất khẩu chúng trên toàn cầu, tiêu<br/> biểu cho một bộ mặt khác đối với từng<br/> người tiêu dùng trên thế giới.</p> <i>từng thị trường địa phương cụ thể. Công ty đã trở thành Liên hiệp quốc của sắc đẹp.</i><br/> <img recindex="00009"/><p>Đ ó là một buổi chiều rực nắng bên ngoài siêu thị Parkson ở Thượng Hải, và một cuộc chiến tiếp thị đang diễn ra ác liệt để tranh giành sự chú ý của phụ nữ Trung Quốc. Các cô người mẫu dong dỏng cao, nũng nịu trong những chiếc váy màu be và áo mỏng tang phân phát các tờ bướm quảng cáo son môi mới của Revlon với sắc màu mùa xuân. Nhưng nỗ lực của họ bị lấn át bởi pha trình diễn bắt mắt của L’Oreal cho thương hiệu Maybelline. Với một giai điệu rộn ràng, hai người mẫu đang khiêu vũ trong ánh sáng lung linh trên một chiếc bục trước một phông nền mô tả đường chân trời New York. Nhạc dừng, và một chuyên gia trang điểm đang tô điểm cho gương mặt cô người mẫu trong khi một nữ nhân viên bán hàng trình bày điểm chính: “Thương hiệu này đến từ Mỹ. Rất hợp thời trang.” Cô hô hào trên mi crô: “Nếu bạn muốn trở nên đúng mốt, hãy chọn Maybelline.” Ít có người phụ nữ nào trong đám đông nhận ra rằng thương hiệu Maybelline “New York” thuộc về hãng mỹ phẩm khổng lồ L’Oreal của Pháp. </p> <p>Cho dù th ương hiệu Maybelline thịnh vượng nhờ sự bơm truyền năng lượng Mỹ, L’Oreal không chỉ đơn thuần dựng lên một quan niệm phương tây. Thay vì thế, công ty công nhận quan niệm văn hoá về sắc đẹp khác nhau trên khắp thế giới. Thật ra, công ty vẫn thường vượt ra ngoài thông lệ để thử thách các nhận thức phổ thông về sắc đẹp. Ví dụ, phần đầu của một báo cáo hàng năm gần đây mô tả một người mẫu Nhật Bản với mái tóc đỏ rực và son môi màu tím. Các biển quảng cáo cho thuốc nhuộm tóc Garnier ở Moscow mô tả các người mẫu châu Phi và châu Á với mái tóc vàng hoe. </p> <p>T ổng giám đốc điều hành L’Oreal, Lindsay Owens Jones khẳng định rằng bí mật của việc quản lý thương hiệu tốt là tác động đến đúng đối tượng khán giả với đúng sản phẩm. Bà nói: “Từng thương hiệu được định vị trên một phân khúc thị trường hết sức chính xác.” Đối với L’Oreal, điều đó có nghĩa là tìm các thương hiệu địa phương, chải chuốt lại, định vị chúng cho một thị trường mục tiêu cụ thể, và xuất khẩu chúng cho các khách hàng mới trên toàn cầu. Để hỗ trợ cho nỗ lực này, công ty phải tiêu tốn 4 tỷ USD hàng năm nhằm sửa đổi các thông điệp tiếp thị toàn cầu cho phù hợp với văn hoá địa phương trên khắp thế giới. </p> <p>Ví d ụ như việc thôn tính và sáp nhập các thương hiệu Soft Sheen và Carson của L’Oreal gần đây. Thoạt đầu chỉ được tiếp thị ở Hoa Kỳ, thương hiệu Soft Sheen và Carson giờ đây tạo ra hơn 30 phần trăm doanh thu nước ngoài. Ở Nam Phi, nơi thương hiệu có đến 41 phần trăm thị phần, L’Oreal đã làm việc theo tinh thần địa phương để xúc tiến thử các sản phẩm mới. Ở Senegal, các nhà tiếp thị của công ty đang tổ chức các buổi đào tạo cho thợ làm tóc.</p> <p>Ngoài vi ệc sửa đổi thông điệp và biện pháp xúc tiến, bản thân các sản phẩm của L’Oreal phải phù hợp với nhu cầu địa phương ứng với con người, văn hoá, và khí hậu hết sức đa dạng. Hướng tới mục đích này, tính theo tỷ trọng trong doanh thu, L’Oreal chi tiêu hơn 50 phần trăm so với giá trị bình quân toàn ngành cho việc nghiên cứu và phát triển sản phẩm. Chỉ riêng năm ngoái, công ty đã làm hồ sơ cho 490 bằng phát minh. Ví dụ, các trung tâm nghiên cứu ở Nhật Bản tập trung vào nhu cầu của làn da và loại tóc châu Á. Viện nghiên cứu da và tóc các dân tộc L’Oreal chuyên nghiên cứu nhu cầu của người tiêu dùng nguồn gốc châu Phi. Một đường hầm hút gió có kiểm soát khí hậu ở Pháp giúp công ty tìm hiểu tác động của thời tiết đối với mỹ phẩm. Công tác nghiên cứu phát triển giúp L’Oreal xây dựng công thức sản phẩm để sử dụng ở những môi trường nhiệt độ cao, độ ẩm cao như Ấn Độ. Nghiên cứu phát triển cũng xây dựng nền tảng cho phân khúc tăng trưởng nhanh nhất của L’Oreal – cái gọi là mỹ phẩm tích cực được thiết kế về y sinh học bởi các nhà khoa học và các chuyên gia da liễu của công ty. </p> <p>Nh ững cụm từ dường như mâu thuẫn nhau có thể được sử dụng để mô tả các thương hiệu của L’Oreal: khoa học và thần thánh, tiếp thị đại trà và truyền miệng, sự tinh tế của Pháp và vẻ bảnh bao phố thị của New York, phù hợp và độc đáo, sang trọng và vừa túi tiền. Làm thế nào một công ty có thể dự phần vào mọi phân khúc của thị trường trên nhiều bình diện đến thế? Đối với L’Oreal, tiếp thị những thứ khác nhau đối với nhiều người khác nhau có nghĩa là “dẫn truyền sức quyến rũ của những nền văn hoá khác nhau thông qua nhiều sản phẩm,” như một nhà phân tích ngành từng nói. Một người quan sát khác nhận định: </p> <p>Ng ược hẳn với các thương hiệu phương tây khác như Coca Cola và McDonald’s, chỉ đưa ra một biểu tượng văn hoá duy nhất, L’Oreal có thể lôi kéo người tiêu dùng châu Á chẳng hạn, bằng nét thanh lịch của người Pháp, phong điệu của người Mỹ, hay vẻ tao nhã của người Ý. Bạn nóng lòng mua một phần giấc mơ Mỹ thì Maybelline New York sẵn sàng phục vụ bạn. Bạn muốn một phong cách La Mã chăng, có Giorgio Armani ở đó để bạn chiếm lấy.</p> <p>Các s ản phẩm của L’Oreal được tìm thấy trong các cửa hàng sang trọng, các thẩm mỹ viện, các nhà thuốc tây, các cửa hàng bách hoá, và thậm chí ở các tiệm tạp hoá. Công ty có mặt ở mọi nơi có khách hàng và mang lại những g ì khách hàng mong muốn – Vichy Laboratories trong các hiệu thuốc tây, Giorgio Armani trong các cửa hàng cao cấp, Maybelline giá phải chăng trong các siêu thị bình dân Wal Mart. Khi tổng giám đốc Owens Jones gần đây phát biểu trong một hội nghị UNESCO, không ai chớp mắt khi nghe bà mô tả L’Oreal như “Liên hiệp quốc của sắc đẹp.”</p> <p><font size="-1"><i>Ngu ồn: </i>Trích dẫn và các thông tin khác từ Gali Edmondson, “The Beauty of Global Branding,”<i> Business Week, </i> 28-6-1999, trang 70-75; Richard Tomlinson, “L’Oreal’s Global Makeover,” <i>Fortune, </i> 30-9-2002, trang 141; Eurozfile Bakgrounder: L’Oreal, 11-9-2001, tiếp cận trực tuyến ở<font color="#0000FF"> www.hemscott.couk</font>; “Top Global Brands,” <i>Global Cosmetic Industry</i>, 2-2003, trang 28-34; “History: making Sure the Hair Creams Taste OK,” tiếp cận trực tuyến ở<font color="#0000FF"> www.iwon.com</font>, tháng 7-2003; “Consumer Products Brief: L’Oreal,” <i>Wall treet Journal</i>, 23-2-2004, trang 1; Vito J. Racanelli, “Touching Up,” 16-2-2004, trang 18-19; và thông tin tiếp cận được ở<font color="#0000FF"> www.loreal.com</font>, tháng 1-2005.</font></p> <h2><b>Sản phẩm </b></h2> <p>Có n ăm chiến lược để điều chỉnh sản phẩm và xúc tiến quảng bá trên một thị trường toàn cầu (xem hình 19.3). Trước tiên ta thảo luận ba chiến lược sản phẩm rồi sau đó sẽ thảo luận hai chiến lược xúc tiến.</p> <b>Hình 19.3 Năm chiến lược sản phẩm và xúc tiến quốc tế</b><br/> Sản phẩm <br/> Không thayn Phát triển sản sản phẩm phẩm phẩm mới Không thay đổi 1. Mở rộng đơn 3. Điều chỉnh<br/> <sub>Xúc tiến </sub> xúc tiến giản sản phẩm <sub>5. Phát minh</sub> Điều chỉnh xúc 2. Điều chỉnh 4. Điều chỉnh sản phẩm tiến truyền thông kép <p><b>M ở rộng sản phẩm đơn giản </b>có nghĩa là tiếp thị sản phẩm trên một thị trường nước ngoài mà không có thay đổi gì. Các nhà quản lý cấp cao nói với mọi người “Hãy chấp nhận sản phẩm như hiện tại và đi tìm khách hàng.” Tuy nhiên, bước thứ nhất là tìm hiểu xem thử người tiêu dùng nước ngoài có sử dụng sản phẩm đó và họ ưa chuộng dạng nào.</p> <b>Mở rộng sản phẩm đơn giản </b><br/> Tiếp thị sản phẩm trên một thị trường nước ngoài mà không thay đổi gì. <p>Vi ệc mở rộng đơn giản có thể thành công trong một số trường hợp và có thể trở thành thảm hoạ trong một số trường hợp khác. Bột ngũ cốc dinh dưỡng của Kellogg, dao cạo của Gillette, bia Heineken, và các công cụ của Black &amp; Decker tất cả đều được bán ra thành công với gần như một dạng hệt như nhau trên khắp thế giới. Nhưng công ty thực phẩm General Foods giới thiệu Jell O dạng bột tiêu chuẩn ở thị trường Anh cuối cùng nhận thấy rằng người tiêu dùng Anh ưa chuộng loại dạng bánh xốp rắn hay bánh ngọt hơn. Tương tự, Philips bắt đầu kiếm được lợi nhuận ỏ Nhật Bản chỉ sau khi công ty thu gọn kích thước của máy pha cà phê để vừa với kích thước nhỏ của nhà bếp ở Nhật cũng như thu nhỏ kích thước máy cạo râu để vừa với bàn tay nhỏ của người Nhật. Mở rộng đơn giản cũng hấp dẫn vì nó không đòi hỏi phải tốn thêm chi phí phát triển sản phẩm, thay đổi sản xuất, hay sự xúc tiến mới. Nhưng nó có thể tốn kém trong dài hạn nếu sản phẩm không thoả mãn người tiêu dùng nước ngoài. </p> <p><b>Đ iều chỉnh sản phẩm </b>liên quan đến việc thay đổi sản phẩm để phù hợp với điều kiện hay mong muốn  của địa phương. Ví dụ, dầu gội Vidal Sassoon của Procter &amp; Gamble mang một hương thơm độc nhất trên toàn thế giới, nhưng với mức độ khác nhau ở từng nước: mùi hương nồng hơn ở châu Âu, nhưng nhẹ hơn ở Nhật Bản, nơi người tiêu dùng thích mùi hương thoang thoảng hơn. Công ty Gerber phục vụ người Nhật thức ăn em bé mà có thể làm cho nhiều người tiêu dùng phương Tây chán ngán – những món ưa thích của địa phương bao gồm cá bơn, rau spinach hầm, mì ống cod roe, thịt hầm men bia, và cá mòi xay với sốt củ cải trắng. Và Nokia, nhà sản xuất điện thoại di động Phần Lan chế tạo theo nhu cầu khách hàng 6100 series điện thoại cho từng thị trường chính. Các nhà phát triển đã xây dựng tính năng nhận biết giọng nói cho châu Á, nơi mà các bàn phím là một vấn đề, và tăng âm lượng chuông để có thể nghe được trên những đường phố đông đúc của châu Á. </p> <b>Điều chỉnh sản phẩm</b><br/> Điều chỉnh một sản phẩm để thích ứng với điều kiện địa phương hay mong muốn của thị trường nước ngoài.<br/> <b>Phát minh sản phẩm </b><br/> Sáng tạo ra những sản phẩm hay dịch vụ mới cho các thị trường nước ngoài. <p><b>Phát minh s ản phẩm </b>bao gồm việc sáng tạo ra những thứ mới mẻ cho một thị trường đất nước cụ thể. Chiến lược này có thể có hai hình thức. Nó có thể có nghĩa là duy trì hay giới thiệu lại các dạng sản phẩm trước đây mà ngẫu nhiên phù hợp với nhu cầu của một đất nước nào đó. Volkswagen tiếp tục sản xuất và bán kiểu xe VW Beetle ở Mexico mãi cho đến gần đây. Hay công ty có thể sáng tạo một sản phẩm mới để đáp ứng nhu cầu ở một đất nước cụ thể. Ví dụ, Sony bổ sung kiểu chữ “U” cho dòng máy tính cá nhân VAIO để đáp ứng các nhu cầu riêng biệt của người tiêu dùng Nhật Bản, cho dù nó không hấp dẫn lắm ở Hoa Kỳ và các thị trường thế giới khác: </p> <p>Ki ểu chữ U có lẽ là sản phẩm “Nhật Bản” nhất trong toàn bộ dòng sản phẩm VAIO Sony. Chiếc máy tính xách tay bé nhất trên thế giới, bề rộng không đến 7 inch, với màn hình đường chéo 6 inch, nó làm cho một chiếc máy tính thông thường trông giống như có vóc dáng của một võ sĩ sumo. Sony nhận thấy rằng những chuyến xe lửa vào giờ cao điểm đến Tokyo chỉ đơn giản là quá đông đúc nên không cho phép nhiều hành khách sử dụng máy tính xách tay. Mark Hanson, phó giám đốc Sony nói: “Những người duy nhất ở Tokyo có được sự xa hoa của chiếc máy tính là những người đầu tiên bước lên xe lửa.” Ông giải thích, hai tay giữ chặt đế chiếc máy tính và tựa ngón tay cái lên bàn phím: “Ưu điểm của máy tính chữ U là nó mang lại cho người sử dụng sự trải nghiệm cái mà tôi gọi là một chiếc máy tính đứng.” Làm thể nào đưa được loại máy này vào thị trường Hoa Kỳ? Sự khác biệt về văn hoá thật là nản lòng. So với người Nhật, ở Mỹ có nhiều người đánh máy không nhìn vào bàn phím hơn (một vài ký tự Nhật Bản chuyển tải được rất nhiều), và những người đánh máy không nhìn bàn phím có thể phản đối việc đánh máy bằng ngón tay cái. Và ở Mỹ thì ít có người phải chịu đựng cảnh đi lại vào giờ cao điểm theo kiểu Tokyo </p> <h2><b>Xúc tiến </b></h2> <p>Các công ty có th ể thực hiện cùng một chiến lược xúc tiến mà họ sử dụng ở nước nhà, hoặc thay đổi chiến lư ợc theo từng thị trường nước ngoài. Ta hãy xem các thông điệp quảng cáo. Một số công ty toàn cầu sử dụng các chương trình quảng cáo chuẩn hoá trên khắp thế giới. Lẽ dĩ nhiên, ngay cả trong những chiến dịch xúc tiến hết sức chuẩn hoá, vẫn cần có một vài thay đổi nhỏ để điều chỉnh về ngôn ngữ và những khác biệt văn hoá nho nhỏ. Ví dụ, Guy Laroche sử dụng gần như cùng những chương trình quảng cáo như nhau cho hương thơm Drakkar Noir ở châu Âu cũng như các nước Ả Rập. Tuy nhiên, họ khéo léo làm dịu đi các chương trình quảng cáo ở Ả Rập để phù hợp với sự khác biệt văn hoá trong thái độ đối với khoái lạc. </p> <p>Đ ôi khi màu sắc cũng được thay đổi để tránh những điều kiêng kỵ. Màu tím gắn liền với cái chết ở hầu hết các nước châu Mỹ Latin, màu trắng là màu tang tóc ở Nhật Bản, và màu xanh lục gắn liền với bệnh rừng ở Malaysia. Ngay cả cái tên cũng phải được thay đổi. Ở Thuỵ Điển, công ty Helene Curtis đổi tên dầu gội đầu Every Night (mỗi đêm) thành Every Day (mỗi ngày) vì người Thuỵ Điển thường gội đầu vào buổi sáng. Và công ty Kellogg phải đổi tên bột ngũ cốc dinh dưỡng Bran Buds ở Thuỵ Điển, nơi mà từ này đại khái được dịch ra thành “burned farmer” (nông dân bị cháy). (Xem thêm những sơ suất về ngôn ngữ trong Tiếp thị thực tế 19.3.) </p> <p><b>Ti ếp thị thực tế 19.3<br/> Coi chừng ngôn ngữ của bạn </b></p> <p>Nhi ều công ty toàn cầu gặp khó khăn khi vượt qua hàng rào ngôn ngữ, với hậu quả có khi là sự ngượng ngùng nhẹ nhàng nhưng cũng có lúc là sự thất bại hoàn toàn. Những thương hiệu và những câu quảng cáo tưởng chừng vô thưởng vô phạt có thể mang những ý nghĩa ngầm ẩn bất ngờ khi được dịch sang các ngôn ngữ khác. Việc dịch thuật bất cẩn có thể làm cho một nhà tiếp thị trở nên có vẻ ngớ ngẩn trước khách hàng nước ngoài.</p> <p>Chúng ta ai c ũng từng tình cờ gặp một vài ví dụ khi mua hàng từ các nước khác. Đây là một ví dụ từ một công ty ở Đài Loan đang cố gắng hướng dẫn trẻ em cách lắp đặt một đoạn dốc vào garage cho các chiếc ô tô đồ chơi: “Before you play with, fix waiting plate by yourself as per below diagram. But after you once fixed it, you can play with as is and no necessary to fix off again.”<br/> (Trước khi bạn chơi, bạn hãy tự gắn tấm bảng đợi theo sơ đồ dưới đây. Nhưng sau khi bạn đã gắn xong, bạn có thể chơi mà không cần phải gắn lại.” (Từ “fix” có nghĩa là gắn, lắp, nhưng còn có nghĩa là đấm mồm, trừng phạt, trả thù, cùng một số lỗi ngữ pháp khác – ND). Nhiều công ty Hoa Kỳ có tội về hành động bạo lực như vậy khi tiếp thị ra nước ngoài.</p> <p><i>Các rào c ản ngôn ngữ toàn cầu: Một số thương hiệu chuẩn hoá không được dịch ổn thỏa sang các ngôn ngữ toàn cầu. </i></p> <img recindex="00010"/><p>Nh ững sơ suất kinh điển về ngôn ngữ liên quan đến những thương hiệu chuẩn hoá không được dịch tốt sang tiếng nước ngoài. Khi lần đầu tiên Coca Cola tiếp thị thương hiệu Coke vào Trung Quốc hồi thập niên 20, công ty xây dựng một nhóm ký tự tiếng Hoa mà khi phát âm lên sẽ nghe như tên sản phẩm. Đáng tiếc thay, những ký tự này khi được dịch ra có nghĩa là “cắn con nòng nọc bằng sáp”. Bây giờ, các ký tự trên chai Coca Trung Quốc dịch ra thành “hạnh phúc trong miệng.” </p> <p>M ột số nhà sản xuấ�t ô tô Hoa Kỳ cũng có những vấn đề tương tự khi thương hiệu của họ vấp phải hàng rào ngôn ngữ. Thương hiệu ô tô Nova của công ty Chevy dịch sang tiếng Tây Ban Nha là<i> no va</i> - có nghĩa là “nó không đi.” GM đổi tên thành Caribe và doanh số gia tăng. Buick đổi tên xe ô tô mui kín LaCrosse thành the Allure ở Canada sau khi biết rằng tên cũ giống với một từ của người Quebec nói về thủ dâm. Và Rolls Royce tránh tên Silver Mist trên thị trường Đức, nơi mà<i> mist </i>có nghĩa là “phân bón”. Tuy nhiên, công ty Sunbeam lại gia nhập thị trường Đức với máy duỗi tóc xoăn hiệu Mist Stick. Như lẽ ra nên được dự đoán từ đầu, người Đức ít sử dụng loại máy “chiếc gậy phân bón” này. Số phận tương tự cũng chờ đón Colgate khi họ giới thiệu kem đánh răng ở Pháp tên gọi Cue, tình cờ cũng là tên của một tạp chí khiêu dâm khét tiếng. </p> <p>M ột công ty với ý định tốt bán dầu gội đầu ở Brazil với tên gọi Evitol. Chẳng bao lâu công ty nhận ra họ đang bán một “thuốc ngừa thai của gầu”. Một công ty Hoa Kỳ kể lại, họ gặp trục trặc khi tiếp thị sữa Pet ở các vùng nói tiếng Pháp. Xem ra, từ<i> pet </i>trong tiếng Pháp, ngoài những nghĩa khác, còn có nghĩa là “đánh rắm”. Tương tự, IKEA tiếp thị ghế ngồi làm việc cho trẻ em tên FARTFULL (từ này có nghĩa là “vận động nhanh” trong tiếng Thụy Điển). Công ty Hunt Wesson giới thiệu các sản phẩm Big John ở Quebec là Gros Jos trước khi nhận ra rằng nó có nghĩa là “ngực to” ở Pháp. Sự hớ hênh này không có ảnh hưởng rõ rệt đối với doanh số. </p> <p>Interbranch ở London, công ty từng sáng tạo các tên gọi gia đình như Prozac và Acura, mới đây đã xây dựng một “danh mục xấu hổ” bao gồm những thương hiệu này và những thương hiệu nước ngoài khác mà bạn không bao giờ thấy trong A&amp;P địa phương: giấy vệ sinh Krapp (Đan Mạch), bột ngũ cốc Crapsy Fruit (Pháp), giấy vệ sinh Happy End (Đức), sữa chua Mukk (Ý), nước chanh Zit (Đức), bột cà ri Poo (Argentina), và nước chanh Pschitt (Pháp).</p> <p>Du khách th ường nhận được những lời khuyên với ý tốt từ các công ty dịch vụ mà lại mang những ý nghĩa rất khác với ý định tốt của họ. Thực đơn trong một nhà hàng Thuỵ Sĩ tự hào viết: “Our wines leave you nothing to hope for.” (Rượu vang của chúng tôi không để lại gì cho bạn hy vọng). Các biển hiệu trong một khách sạn Nhật Bản thông báo: “You are invited to take advantage of the chambermaid.” (Mời bạn lợi dụng cô phục vụ phòng.) Trong một phòng giặt ở Rome, người ta khuyên: “Ladies, leave your clothes here and spend the afternoon having a good time.” (Quý bà quý cô hãy để lại quần áo ở đây và trải qua buổi chiều với những giờ khắc vui thú.) Tờ quảng cáo ở một hãng cho thuê ô tô Tokyo đưa ra một lời khuyên uyên bác: “When passenger of foot heave in sight, tootle the horn. Trumpet him melodiously at first, but if he still obstacles your passage, tootle him with vigor.” (Khi khách bộ hành xuất hiện, hãy thổi còi. Thoạt đầu chỉ thổi anh ta một cách êm dịu, nhưng nếu anh ta vẫn cản trở lối đi, hãy thổi anh ta mạnh hơn.) </p> <p>Các ch ương trình quảng cáo thường mất – hay được – cái gì đó trong dịch thuật. Khẩu hiệu “get loose with Coors” (thả lỏng với Coors) của bia Coors trong tiếng Tây Ban Nha hoá ra là “chạy với Coors”. Chương trình xúc tiến của công ty Coca Cola “Coke adds life” (Coke tăng cường cuộc sống) trong tiếng Nhật dịch ra thành “Coke đưa tổ tiên trở về từ cõi chết.” Ngành sữa hiểu ra quá muộn rằng câu hỏi quảng cáo của người Mỹ “Got Milk?” (Uống sữa nhé?) dịch sang tiếng Mexico nghe rất khiêu khích: “Bạn đang chảy sữa à?” Khẩu hiệu của KFC “fingerlickn’ good” (ngon quá liếm ngón tay) hoá ra là “ăn hết mấy ngón tay bạn đi” trong tiếng Hoa. Và dòng chữ kinh điển của Frank Perdue “It takes a tough man to make a tender chicken” (phải có một chàng trai cứng rắn để làm con gà mềm) có ý nghĩa tăng cường hơn trong tiếng Tây Ban Nha: “Phải có một chàng trai khuấy động để làm một con gà trìu mến.” Ngay cả khi cùng một ngôn ngữ, việc dùng từ giữa các nước cũng có thể khác nhau. Vì thế, dòng chữ quảng cáo máy hút bụi Electrolux của người Anh “Nothing sucks like an Electrolux” (không gì hút bằng một máy hút bụi Electrolux) sẽ ít thu hút khách hàng ở Hoa Kỳ. </p> <p><font size="-1"><i>Ngu ồn: </i> Xem David A. Ricks, “Perspectives: Translation Blunders in International Business, <i>Journal of Language for International Business</i>, 7:2, 1996, trang 50-55; “But Will It Sell in Tulsa?”, <i>Newsweek, </i><i>Newsweek, </i> 1997, trang 8; Ken Friedenreich, “The Lingua Too Franca,” <i>World Trade</i>, tháng 4-1998, trang 98; Sam Solley, “Developing a Name to Work Worldwide,” <i>Marketing</i>, 21-12-2000, trang 27; Thomas T. Sermon, “Cutting Corners in Language Risky Business,” <i>Marketing News</i>, 23-4-2001, trang 9; Lara L. Sowinski, “Ulbersetzung, Traduzione, or Traduccion,” <i>World Trade</i>, tháng 2-2002, trang 48-49; Marrtin Croft, “Mind Your Language,” <i>Marketing, </i> 19-6-2003; trang 35-39; và Mark Lasswell, “Lost in Transation,” <i>Business 2.0</i>, tháng 8-2004, trang 68-70. </font></p> <p>Các công ty khác theo đuổi chiến lược <b>điều chỉnh truyền thông</b>, nghĩa là điều chỉnh hoàn toàn các thông điệp quảng cáo theo các thị trường địa phương. Các chương trình quảng cáo của Kellogg ở Hoa Kỳ quảng bá hương vị và sự bổ dưỡng của bột ngũ cốc Kellogg so với thương hiệu của các đối thủ cạnh tranh. Ở Pháp, nơi người tiêu dùng uống ít sữa và ăn ít hơn trong bữa điểm tâm, các chương trình quảng cáo của Kellogg phải thuyết phục người tiêu dùng rằng bột ngũ cốc là một món điểm tâm ngon miệng và lành mạnh. Ở Ấn Độ, nơi nhiều người tiêu dùng ăn sáng những món chiên xào, nặng bụng, chương trình quảng cáo của Kellogg thuyết phục khách hàng chuyển sang chế độ điểm tâm nhẹ hơn nhưng bổ dưỡng hơn.</p> <b>Điều chỉnh truyền thông </b><br/> Một chiến lược truyền thông toàn cầu, điều chỉnh hoàn toàn các thông điệp quảng cáo để thích ứng với các thị trường địa phương. <p>T ương tự, công ty Coca Cola bán nước giải khát hàm lượng calorie thấp với thương hiệu Diet Coke ở Bắc Mỹ, Anh, Trung Đông và Viễn Đông, nhưng bán sản phẩm này với thương hiệu Coke Light ở những nơi khác. Theo giám đốc thương hiệu toàn cầu của Diet Coke, ở những nước nói tiếng Tây Ban Nha, biển quảng cáo Coke Light “định vị thức uống nhẹ là một thức uống đáng mơ ước, chứ không phải như một cách để cảm thấy ngon miệng như Diet Coke được định vị ở Hoa Kỳ”. Sự “định vị thức uống đáng mơ ước” này thể hiện một nghiên cứu cho thấy rằng “Ở những nơi khác trên thế giới, Coca Cola Light được xem như một thương hiệu sôi nổi, ứa ra sự tự tin về tình dục.” </p> <p>Các ph ương tiện truy ền thông cũng cần được điều chỉnh trên thị trường quốc tế vì các phương tiện truyền thông sẵn có rất khác nhau giữa nước này với nước khác. Ví dụ như, thời gian quảng cáo trên truyền hình rất hạn chế ở châu Âu, từ 4 giờ một ngày ở Pháp cho đến không có giờ nào ở các nước Scandinavia. Các nhà quảng cáo phải đăng ký mua thời gian quảng cáo trước nhiều tháng, và họ ít kiểm soát được thời gian phát sóng. Các tạp chí cũng rất khác nhau về mức độ hữu hiệu. Ví dụ, tạp chí là một phương tiện truyền thông chính ở Ý và chỉ là một phương tiện phụ ở Áo. Báo chí là phương tiện truyền thông quốc gia ở Anh, nhưng chỉ là phương tiện địa phương ở Tây Ban Nha. </p> <h2><b>Giá cả</b></h2> <p>Các công ty c ũng phải đối mặt với nhiều vấn đề khi xây dựng giá quốc tế. Ví dụ, công ty Black &amp; Docker định giá các công cụ trên toàn cầu như thế nào? Họ có thể ấn định một mức giá đồng nhất trên khắp thế giới, nhưng mức giá đó có thể quá cao ở một nước nghèo và không đủ cao ở một nước giàu. Họ có thể tính mức giá mà người tiêu dùng ở mỗi nước có thể mua được, nhưng chiến lược này bỏ qua sự khác biệt về chi phí thực tế giữa các nước. Cuối cùng, công ty có thể sử dụng một khoản cộng thêm (bao gồm phí và lãi) tiêu chuẩn cho mọi nơi, nhưng phương pháp này có thể làm cho giá sản phẩm Black &amp; Docker vượt ra ngoài thị trường ở một số nước có chi phí cao.</p> Để xử lý những vấn đề như vậy, P&amp;G điều chỉnh việc định giá theo các thị trường địa phương. Ví dụ, ở châu Á, công ty đã chuyển sang một mô hình định giá từng bậc: <p>Khi l ần đầu tiên bước vào thị trường châu Á, P&amp;G đã sử dụng cách tiếp cận từng giúp công ty thành công đến thế ở Hoa Kỳ. Công ty phát triển những sản phẩm tốt hơn và tính giá hơi cao hơn các đối thủ cạnh tranh. Công ty cũng tính giá một túi bột giặt Tide hay một chai dầu gội Pantene ở châu Á gần như cao tương đương với giá ở Bắc Mỹ. Nhưng giá cao như thế hạn chế sự hấp dẫn của P&amp;G trên thị trường châu Á, nơi phần lớn người tiêu dùng chỉ kiếm được vài USD một ngày. Hai phần ba dân số Trung Quốc thu nhập không đến 25 USD một tháng. Vì thế năm ngoái P&amp;G thực hiện chiến lược định giá từng bậc giúp công ty cạnh tranh với các thương hiệu địa phương rẻ hơn, đồng thời vẫn bảo vệ được giá trị thương hiệu toàn cầu của công ty. Công ty giảm chi phí sản xuất ở châu Á, hợp lý hoá lại các kênh phân phối, và định dạng lại tuyến sản phẩm để tạo ra những mức giá phải chăng hơn. Ví dụ, công ty giới thiệu một túi bột giặt Tide Clean White (Tide trắng tinh) 320 gram với giá 23 cents, so với giá 33 cents của túi Tide Tripple Action (Tide ba tác động). Tide trắng tinh không mang lại những lợi ích như tẩy sạch vết ố và lưu lại hương thơm, và nó chứa ít enzyme làm sạch hơn. Nhưng chi phí sản xuất của nó thấp hơn và có tác dụng tốt hơn hẳn các thương hiệu khác ở mức giá thấp. Kết quả của chính sách định giá mới của P&amp;G thật ngoạn mục. Sử dụng cùng cách tiếp cận đó cho kem đánh răng, hiện nay P&amp;G bán nhiều kem đánh răng Crest ở Trung Quốc hơn ở Hoa Kỳ. </p> <p>B ất kể các công ty định giá sản phẩm như thế nào, giá nước ngoài của họ có lẽ sẽ cao hơn giá trong nước  của các sản phẩm tương thích. Một túi xách hiệu Gucci có thể bán với giá 60 USD ở Ý và 240 USD ở Hoa Kỳ. Tại sao? Gucci phải đối phó với vấn đề<i> leo thang giá cả</i>. Công ty phải cộng thêm chi phí vận chuyển, thuế quan, lợi nhuận của nhà nhập khẩu, lợi nhuận của người bán buôn, và lợi nhuận của nhà bán lẻ vào giá thành sản xuất. Tuỳ thuộc vào những chi phí cộng thêm này, sản phẩm có thể phải bán với giá gấp đôi hay gấp năm lần ở một đất nước khác để có được cùng mức lợi nhuận. Ví dụ, một chiếc quần bò Levi’s giá 30 USD ở Hoa Kỳ thường bán giá 63 USD ở Tokyo và 88 USD ở Paris. Một chiếc máy tính bán 1000 USD ở New York có thể có giá 1000 EUR ở Anh. Một chiếc ô ô Ford giá 20.000 USD ở Hoa Kỳ có thể bán hơn 80.000 USD ở Hàn Quốc. </p> <p>M ột vấn đề khác liên quan đến việc định giá hàng hoá là công ty chở hàng đến các công ty con ở nước ngoài của họ. Nếu công ty tính giá cho công ty con quá cao, công ty có thể phải nộp thuế nhập khẩu cao hơn, đồng thời đóng thuế thu nhập thấp hơn ở đất nước đó. Nếu công ty tính giá cho công ty con quá thấp, công ty có thể bị cáo buộc là<i> phá giá</i>. Phá giá xảy ra khi công ty định giá thấp hơn chi phí hoặc thấp hơn so với giá họ tính ở thị trường nước nhà. Ví dụ, Liên minh tôm miền nam Hoa Kỳ, đại diện cho hàng ngàn cơ sở nuôi tôm nhỏ ở miền đông nam nước Mỹ, hiện đang kiện sáu nước (Trung Quốc, Thái Lan, Việt Nam, Ecuador, và Brazil) bán phá giá tôm vào thị trường Hoa Kỳ. Uỷ ban Thương mại quốc tế Hoa Kỳ đồng ý và đã kiến nghị Bộ Thương mại ban hành thuế đối với tôm nhập khẩu từ các nước bị cáo buộc. Chính phủ nhiều nước luôn luôn theo dõi các vụ lạm dụng phá giá, và thường buộc các công ty phải ấn định mức giá như của các đối thủ cạnh tranh khác cho cùng những sản phẩm như nhau hay tương tự như nhau. </p> <p>Các áp l ực kinh tế và công nghệ có tác động đối với việc định giá toàn cầu. Ví dụ, trong Liên minh châu Âu, việc chuyển sang đồng euro làm giảm sự khác biệt giá cả. Khi người tiêu dùng nhận ra sự khác biệt giá cả giữa các nước, các công ty buộc phải hài hoà mức giá ở những nước sử dụng chung một đồng tiền. Các công ty và các nhà tiếp thị chào bán những sản phẩm hay dịch vụ độc đáo hay cần thiết nhất sẽ ít chịu ảnh hưởng nhất của sự “minh bạch giá cả” như vậy.</p> <p>Đố i với Marie Claude Lang, một nhân viên bưu điện người Bỉ 72 tuổi về hưu, euro là thứ tốt nhất sau nước đóng chai, hay xúc xích đồng quê nước Pháp. Luôn luôn đi loanh quanh để mặc cả, bà Lang đang bệ vệ bước qua các gian hàng to lớn của siêu thị Auchan ở Roncq, Pháp, cách nhà bà ở Werick khoảng 15 phút lái xe… Bà Lang đến Pháp mỗi cách tuần trong nhiều năm để mua dự trữ nước uống đóng chai, sữa, và sữa chua. Nhưng việc ban hành đồng euro… đã mở ra trước mắt bà nhiều sản phẩm hơn với giá đồng loạt thấp hơn. Hôm nay bà thấy xúc xích đồng quê rẻ hơn khoảng 5 cents của đồng euro, một khoản tiết kiệm mà bà không nhận thấy khi bà phải tính sự chênh lệch giữa đồng franc Pháp và đồng tiền Bỉ. Ở biên giới châu Âu, đồng euro biến thành niềm vui của phiếu mua hàng. Lẽ dĩ nhiên, những người châu Âu có ý thức về giá cả từ lâu đã băng qua biên giới để tìm mọi thứ từ  máy vô tuyến truyền hình cho đến những chai Coca Cola rẻ hơn. Nhưng sự minh bạch mới đang làm cho việc so sánh tổng thể trở nên dễ dàng hơn.</p> <p>Internet c ũng làm cho sự khác biệt giá cả toàn cầu trở nên dễ nhận thấy hơn. Khi các công ty bán hàng qua Internet, khách hàng có thể thấy sản phẩm được bán bao nhiêu ở các nước khác nhau. Họ cũng có thể đặt hàng trực tiếp từ địa điểm của công ty hay đặt qua đại lý chào mức giá thấp nhất. Điều này sẽ buộc các công ty hướng tới việc định giá quốc tế chuẩn hoá. </p> <h2><b>Các kênh phân phối </b></h2> <p>Công ty qu ốc tế phải có một <b>quan điểm phân phối tổng thể</b> về vấn đề phân phối sản phẩm cho người tiêu dùng sau cùng. Hình 19.4 trình bày ba đầu mối chính liên kết giữa người bán và người mua sau cùng. Đầu mối thứ nhất,<i> tổ chức trụ sở của người bán</i>, giám sát các kênh phân phối và là một phần của kênh phân phối. Đầu mối thứ hai,<i> các kênh giữa các quốc gia,</i> lưu thông sản phẩm qua biên giới nước ngoài. Đầu mối thứ ba,<i> các kênh trong phạm vi từng quốc gia</i>, lưu chuyển sản phẩm từ điểm tiếp nhận nước ngoài đến người tiêu dùng sau cùng. Một số nhà sản xuất Hoa Kỳ có thể nghĩ công việc của họ đã xong một khi sản phẩm rời khỏi tay họ, nhưng họ sẽ làm ăn khấm khá hơn nếu chú ý hơn đến quá trình tiến hành công việc mua bán đến tay người tiêu dùng ở các nước ngoài.</p> <b>Quan điểm phân phối tổng thể</b> <p>Thi ết kế các kênh phân phối quốc tế có xem xét đến tất cả các đầu mối cần thiết trong việc phân phối sản phẩm của người bán đến người mua sau cùng, bao gồm tổ chức trụ sở của người bán, các kênh giữa các nước, và các kênh trong phạm vi từng nước. </p> <b>Hình 19.4 Khái niệm kênh phân phối tổng thể cho hoạt động tiếp thị quốc tế</b><br/> Tổ chức <br/> <sup>trụ sở Các Người của kênh sử dụngngười Các trong sau Người bán cho kênh phạm vi cùngbán hoạt giữa các từng hayđộng quốc gia </sup> quốc gia người <sub>tiếp thị mua quốc tế</sub> <p>Các kênh phân ph ối trong phạm vi quốc gia rất khác nhau giữa nước này so với nước khác. Thứ nhất, có sự khác biệt lớn giữa <i>số lượng </i>và<i> loại hình trung gian</i> phục vụ từng thị trường nước ngoài. Ví dụ, một công ty Hoa Kỳ tiếp thị ở Trung Quốc phải hoạt động thông qua một mê cung nản lòng các nhà buôn và nhà bán lẻ dưới sự kiểm soát của nhà nước. Các nhà phân phối Trung Quốc thường kinh doanh luôn sản phẩm của các đối thủ cạnh tranh và thường từ chối chia xẻ ngay cả những thông tin bán hàng và tiếp thị cơ bản với nhà cung ứng. Xoay xở ngược xuôi để bán hàng là một khái niệm xa lạ với các nhà phân phối Trung Quốc, những người quen bán mọi thứ họ tiếp nhận được. Làm việc và vượt qua hệ thống này đôi khi đòi hỏi nhiều thời gian và đầu tư. </p> <p>Ví d ụ như khi lần đầu tiên Coca được bán trên thị trường Trung Quốc, khách hàng phải đạp xe đến tận nhà máy đóng chai để lấy sản phẩm. Nhiều chủ cửa hàng thậm chí không có đủ điện để chạy các máy làm lạnh nước giải khát. Hiện nay, Coca Cola đã thiết lập các kênh phân phối trực tiếp, đầu tư nhiều vào các xe tải và tủ lạnh, và nâng cấp đường dây điện để có nhiều nhà bán lẻ có thể lắp đặt các máy làm lạnh. Cô ng ty cũng xây dựng một đội quân hơn 10.000 đại diện bán hàng để thường xuyên đến thăm những người bán lại, thường là đi bộ hay đi xe đạp, để kiểm tra trữ lượng và hồ sơ bán hàng của họ. Một người quan sát ngành nhận xét: “Coca Cola và các nhà đóng chai đang cố gắng lên bản đồ mọi siêu thị, nhà hàng, hiệu cắt tóc, hay các chợ, nơi người ta có thể tiêu thụ một lon Coca. Những số liệu này giúp Coca đến gần người tiêu dùng hơn, bất kể đó là những siêu thị lớn, các tiệm mì bình dân, hay các trường học.”</p> <p>M ột điểm khác biệt nữa nằm ở<i> qui mô</i> và<i> đặc điểm của các đơn vị bán lẻ</i> nước ngoài. Trong khi các chuỗi cửa hàng bán lẻ qui mô lớn chiếm lĩnh thị trường Hoa Kỳ, phần lớn việc bán lẻ ở các nước khác được thực hiện bởi những nhà bán lẻ nhỏ, độc lập. Ở Ấn Độ, hàng triệu nhà bán lẻ điều hành những cửa hàng bé xíu hay bán hàng trong chợ. Họ chào giá cao, nhưng giá thực tế có thể thấp hơn nhiều thông qua mặc cả. Các siêu thị có thể chào bán giá thấp hơn, nhưng siêu thị thì khó xây dựng và hoạt động do nhiều rào cản kinh tế và văn hoá. Thu nhập thì thấp và người dân thích mua sắm hàng ngày với những giá trị nhỏ hơn là mua hàng tuần với giá trị lớn. Họ cũng thiếu nhà kho và tủ lạnh để giữ thực phẩm trong vài ngày. Bao bì đóng gói không phát triển lắm vì làm tăng thêm chi phí quá nhiều. Những yếu tố này làm cho việc bán lẻ qui mô lớn không phát triển nhanh ở các nước đang phát triển.</p> <h2><b>Quyết định về tổ chức tiếp thị toàn cầu</b></h2> <p>Các công ty qu ản lý hoạt động tiếp thị quốc tế theo chí ít ba phương thức: Trước tiên, phần lớn các công ty tổ chức một phòng xuất khẩu, rồi thành lập một đơn vị chuyên trách xuất khẩu, và cuối cùng trở thành một tổ chức toàn cầu.</p> <p>M ột công ty thông thường tham gia tiếp thị quốc tế thông qua đơn thuần giao hàng ra nước ngoài. Nếu doanh số quốc tế phát triển, công ty tổ chức một <i> phòng xuất khẩu</i> với một trưởng phòng và một vài trợ lý. Khi doanh số gia tăng, phòng xuất khẩu có thể mở rộng để bao gồm các dịch vụ tiếp thị khác nhau nhằm tích cực theo dõi công việc. Nếu công ty chuyển sang thành lập liên doanh hay đầu tư trực tiếp, phòng xuất khẩu sẽ không còn đủ để đáp ứng công việc nữa. </p> <p>Nhi ều công ty tham gia vào nhiều thị trường quốc tế và nhiều dự án liên doanh. Công ty có thể xuất khẩu sang một nước, cấp phép nhượng quyền sang một nước khác, thành lập một liên doanh liên kết sở hữu ở một nước thứ ba, và sở hữu một công ty con ở một nước thứ tư. Chẳng chóng thì chầy, công ty sẽ thành lập một <i>đơn vị chuyên trách quốc tế</i> hay một công ty con để lo liệu mọi hoạt động quốc tế. </p> <p>Đơ n vị chuyên trách quốc tế được tổ chức theo nhiều cách. Nhân sự công ty của đơn vị chuyên trách quốc tế bao gồm các chuyên gia tiếp thị, sản xuất, nghiên cứu, tài chính, kế hoạch, và tổ chức cán bộ. Đơn vị này lên kế hoạch và cung ứng các dịch vụ cho các đơn vị kinh doanh khác nhau, mà có thể được tổ chức theo một trong ba cách. Các đơn vị kinh doanh có thể là <i>các tổ chức địa lý,</i> với các giám đốc ở từng quốc gia chịu trách nhiệm về lực lượng bán hàng, chi nhánh bán hàng, các nhà phân phố�i, và các đơn vị được cấp phép nhượng quyền ở từng nước riêng biệt. Hoặc các đơn vị kinh doanh có thể được là<i> các nhóm sản phẩm quốc tế</i>, mỗi nhóm chịu trách nhiệm bán hàng toàn cầu của các nhóm sản phẩm khác nhau. Cuối cùng, các đơn vị kinh doanh có thể là các <i>công ty con quốc tế, </i>từng công ty chịu trách nhiệm bán hàng và lợi nhuận riêng.</p> <p>Nhi ều công ty đã đi qua giai đoạn đơn vị chuyên trách quốc tế và trở thành các <i>tổ chức toàn cầu</i>. Họ không còn suy nghĩ về chính họ như các nhà tiếp thị quốc gia bán sản phẩm ra nước ngoài, và bắt đầu tư duy như các nhà tiếp thị toàn cầu. Ban giám đốc cấp cao và nhân viên công ty lên kế hoạch sản xuất, các chính sách tiếp thị, các dòng tài chính, và hệ thống hậu cần toàn cầu. Các đơn vị hoạt động toàn cầu báo cáo trực tiếp cho tổng giám đốc điều hành hay ban điều hành của tổ chức, chứ không báo cáo cho lãnh đạo đơn vị chuyên trách quốc tế. Các cán bộ điều hành được đào tạo về hoạt động kinh doanh trên toàn thế giới, chứ không chỉ là nội địa <i>hay </i>quốc tế. Công ty tuyển dụng giám đốc từ nhiều nước, mua linh kiện và vật tư ở nơi có chi phí thấp nhất và đầu tư ở nơi có sinh lợi kỳ vọng cao nhất. </p> <p>B ước vào thế kỷ 21, các công ty lớn phải trở nên toàn cầu hơn nếu họ hy vọng cạnh tranh. Khi các công ty nước ngoài thâm nhập thành công vào thị trường nội địa của họ, các công ty phải lưu chuyển năng động hơn sang thị trường nước ngoài. Họ sẽ phải thay đổi từ những công ty chỉ xem hoạt động quốc tế là phụ, trở thành những công ty xem toàn bộ thế giới như một thị trường không biên giới duy nhất.</p> <h2><b>Xem lại các khái niệm </b></h2> <p>Trong quá kh ứ, các công ty Hoa Kỳ ít chú ý đến thị trường quốc tế. Họ có thể kiếm được chút doanh số dôi dư thông qua xuất khẩu, điều đó cũng tốt. Nhưng thị trường lớn vẫn là nước nhà, và nó tràn ngập cơ hội. Ngày nay, các công ty không còn chỉ có thể quan tâm tới thị trường nội địa mà thôi, bất kể qui mô lớn nhỏ thế nào. Nhiều ngành là những ngành công nghiệp toàn cầu, và các công ty hoạt động toàn cầu đạt được chi phí thấp hơn và nhận thức thương hiệu cao hơn. Đồng thời, <i>tiếp thị toàn cầu </i>phải chịu nhiều rủi ro do tỷ giá hối đoái biến thiên, chính phủ các nước bất ổn, thuế quan bảo hộ và các hàng rào thương mại, cùng một số yếu tố khác. Ứng với những lợi ích và rủi ro tiềm tàng của tiếp thị quốc tế, các công ty cần một phương thức hệ thống để ra các quyết định tiếp thị toàn cầu. </p> <p><b>1. Thảo luận về hệ thống thương mại quốc tế, các môi trường kinh tế, chính trị pháp lý, và văn hoá ảnh hưởng như thế nào đến các quyết định tiếp thị quốc tế của công ty. </b></p> <p>Công ty ph ải am hiểu <i>môi trường tiếp thị toàn cầu</i>, đặc biệt là hệ thống thương mại quốc tế. Công ty phải đánh giá <i>các đặc điểm kinh tế, chính trị pháp lý, và văn hoá </i>của từng thị trường nước ngoài. Sau đó công ty phải quyết định liệu công ty có muốn thâm nhập thị trường nước ngoài và xem xét các rủi ro cũng như lợi ích tiềm năng. Công ty phải quyết định khối lượng doanh số quốc tế mà công ty muốn thực hiện, bao nhiêu đất nước công ty muốn tiếp thị hàng hoá hay dịch vụ, và những thị trường cụ thể nào công ty muốn tham gia. Quyết định này đòi hỏi phải cân nhắc tỷ suất sinh lợi từ đầu tư so với mức độ rủi ro.</p> <b>2. Mô tả ba phương pháp chủ yếu để tham gia các thị trường quốc tế. </b> <p>Công ty ph ải quyết định tham gia từng thị trường đã chọn như thế nào, liệu thông qua<i> sản xuất, liên doanh, </i>hay<i> đầu tư trực tiếp</i>. Nhiều công ty bắt đầu như những nhà xuất khẩu, tiến tới liên doanh, và cuối cùng thực hiện việc đầu tư trực tiếp trên thị trường nước ngoài. Khi <i>xuất khẩu, </i>công ty tham gia thị trường nước ngoài bằng cách gửi và bán sản phẩm thông qua các trung gian tiếp thị quốc tế (xuất khẩu gián tiếp) hay thông qua các phòng ban của công ty, các chi nhánh, hay đại diện bán hàng, hay đại lý (xuất khẩu trực tiếp). Khi thành lập một liên doanh, công ty tham gia thị trường nước ngoài thông qua liên kết với các công ty nước ngoài để sản xuất hay tiếp thị sản phẩm hay dịch vụ. Khi<i> cấp phép nhượng quyền, </i> công ty bước vào thị trường nước ngoài thông qua hợp đồng với đơn vị được nhượng quyền ở nước ngoài, trao quyền sử dụng qui trình sản xuất, nhãn hiệu hàng hoá, bằng phát minh, bí mật thương mại, và các khoản mục giá trị khác và nhận một khoản phí nhượng quyền. </p> <b>3. Giải thích cách thức công ty điều chỉnh tổ hợp tiếp thị như thế nào đối với các thị trường quốc tế. </b> <p>Các công ty c ũng phải quyết định xem sản phẩm, việc quảng bá xúc tiến, giá cả, và các kênh phân phối sẽ được điều chỉnh bao nhiêu để thích ứng với từng thị trường nước ngoài. Ở một cực đoan, các công ty toàn cầu sử dụng một <i>tổ hợp tiếp thị chuẩn hoá</i> trên toàn thế giới. Những công ty khác sử dụng một <i>tổ hợp tiếp thị điều chỉnh</i>, trong đó họ điều chỉnh tổ hợp tiếp thị cho từng thị trường mục tiêu, chịu tốn thêm chi phí nhưng hy vọng có thị phần và lợi nhuận nhiều hơn. </p> <b>4. Nhận diện ba hình thức tổ chức tiếp thị quốc tế chính. </b> <p>Công ty ph ải xây dựng một tổ chức hữu hiệu để tiếp thị quốc tế. Phần lớn các công ty bắt đầu bằng một <i> phòng xuất khẩu</i> và dần dần phát triển thành <i>đơn vị chuyên trách quốc tế. </i>Một vài đơn vị này trở thành<i> các tổ chức toàn cầu,</i> với hoạt động tiếp thị toàn cầu được lên kế hoạch và quản lý bởi các cán bộ cao cấp của công ty. Các tổ chức toàn cầu xem toàn bộ thế giới như một thị trường không biên giới duy nhất. </p> <h2><b>Xem lại các thuật ngữ then chốt </b></h2> <font size="-1">Adapted marketing mix Tổ hợp tiếp thị điều Licensing Cấp phép nhượng quyền chỉnh<br/> Communication Điều chỉnh truyền thông Management contracting Hợp đồng quản lý adaption<br/> Contract manufacturing Hợp đồng sản xuất Nontariff trade barriers <br/> Countertrade Hàng đổi hàng Product adaption Direct investment Đầu tư trực tiếp Product invention Các hàng rào thương mại phi thuế <p>Đ iều chỉnh sản phẩm Phát minh sản phẩm Economic community Cộng đồng kinh tế Quota Hạn ngạch </p> Embargo Lệnh cấm vận Standardized marketing ếp thị chuẩn mix hoá Exchange controls Kiểm soát ngoại hối Straight product Mở rộng sản phẩm đơn extension giản Exporting Xuất khẩu Tariff Thuế quan Global firm Công ty toàn cầu Whole channel view Quan điểm kênh phân phố�i tổng thể <p>Joint ownership <br/> Joint venturing <br/> Liên kết sở hữu <br/> Liên doanh</p></font>  </body></html> �GIF89a�� �  #$(&+(#4'//*7B28GE5b86A3I20f<NHcB6LK:Se7mK5njDJ!G/F+$E,4G7&F:4S+%U8'U:4j/o5'F<Id<EPEPK2Vc:nKmP1pe8IKEFHVJTGJVURLDTMTUUGVYUQXhHeGIeWFvIHyUYdGYdWWvIWwXJfeJcxGvfFzvYfgZjvYtf\ryjXJe\mheGfgXisHisYwhGvkUxtJysXgifejvisiiuxukftktwuhwxw=X�<r�H]�Wo�Rs�fh�gv�hy�rk�v{�v{�d{�<�IN�VT�fP�RZ�eo�Vp�rk�Wt�tL��R��Z��x��s��y��y��Z��\��r��t��~�܅8�8/�91�7J�Y�T5�b�i6�L7�w�p6�WH�Zh�pN�xm�TI�[d�tP�ui�;4�?:�>@�L9�d�q4�O8�o5�SH�Vj�oR�uj�VN�]`�lS�ti�|��|��{����}��z��{���7��7��:��X��s��X��x��U��m��O��u��z؊ь5ߤի:��3����-̌R̐oԭNҫr�K�t��J�r��0����*����-��W��p��L��o��R��q�����������������������������������������������ő�Ǵ�ȭ��Ö�ť�Ɛ�̳�⺓�Ә���ɱ����ѕ�ӛ�ͭ�ϵ�界圢比糫Ȼ�������Ŕ�Ȳ���Ȗ�̲����������������������������         !�   � !�MBPW�������ؾ���������ٷ��ؾ����٬����٬�٫�������������ڽ��ګ���������ڥ�����������ۥ���WWWEEE�E���E���������VDD�>D>>>>>=���������������ۙ����������������o�o����3!����ܒ����������tst����tntm�sssssmmm���2�2222222���,,�+111�+%�%   � ����������� ,    ��  � ӤY�fM5�<x  �C  P�@�L��Ç�	�@�Dh��%
QÆ	/^La�P�d�Z�4A(Tᡤ��^}�� ��9��Ĉ� a���O[
 ;�i Ҳ��C��A��;�� AK�""%���޴L�p���<�Mc9�!7��x��6����Ƨ��s��ys�������&u�ʂ=C�-�3jƌQ�M-Z�AƊ����	�"Ѧ�D�	+V8�l�%En�03J��������;w:l(�]�HNȗ/�+	E�t��aC�a �0H �0D�	)4p@,���
,�P�)��)��B�+�pC�@T&���	��b�	D �0R`#i���D �E4@ I 	���GN�tRKP�� 
�$5�-��#�LÈRK��7��a�3U&Ջ,���J(p@W�֝W]d�s��҃a=ՠS,M�CZ:��=�!F2��SO>�c�9�=�4��a�6d�V&Gc��Q*��]����A�Xim@&X`�����`���̰�@��6�5l��E8Dqӕ3��e��P��qL���(���-��0M�`�x��/hPA'�7�}<WD`�ހ�t+��B
!��B!6�b,D0!+�8A &��6��Ā��A�-�|"(��b�(���>p@�a��C(���5p��
���$�"�0�2M>�l�4
_��>�x��7ณT<�,��=�$ ��Y�����
t �DB7VK+����3�0����BJ�:���O>���>��Ƨ�K�F��U^Ya�n6��u��ͺ�e�Ff��ʙi�1l���OP{	(��s3!3�Yg���$_�)(��5�nP\@�K�p���s�p@~p�A
����)|< �����(9�p���p�78�^��$�K'�߉J����Le0�K�� �mg;@ �ԑ� %v+�U�ր��wK�`���IAH� od�k晴;jȍa�"��X��R'�i,k� �@  �NRi H�rD�� ��f ��!R�X����g@�����6�q����E����3Lb0�:�1r�s�3� HT�>V��-��
>��`���5L
�򖷦1⍂�9�G�`dz$؊�.н}�`��@Z�������D���dQS���w��ȼ7�1
  �_/� :�!aa	H�N���(e	�JV��� ��:/r�v$h�Yю����\�2E	�@�w�c�HJ.t"k����7�nx��؆0d3� *xbT����-qN]�J<OX7��`6�[� �EL��6�y�1)y��q����95fU�[$d�M4���a�Ȫ�]F��a�`����JsxdC�0�"0� ���:
�E���d)�CAć�"�2��h�` �.�}���� �W/�v@�傸� /rC��X�� 3X���F=�a �`E��4� ��v�!�_
XT���v�3*�!X2�Q�h0O��;�4(�3J#0ER�V�$/yEF.L13�XfRRa�4�!JpE/j�	0� �U"�B��8�	Q	i�J��/EI�/��Ke@�	����"y8i�-���AcP5�G��t�S��$A�Cb�PF4�A,F�j�@��e�9|�U�*T	C�-hA)��� @)��D�wDA�S��H %]�sh �  ��� L�/�@�Z �؋$P�|�6�=(f��8���0�����2���ɢ���0��n`i"����e���o~sE3��JLx��� 8'D0�Ύ職;��X� �0)� A	d��HZ�S3C2:kQ�(p@KF �5�[�-b	�v����	�� (�A�5X� ���� )��P_8@1��'�0�w�#8}\a2,�T�
V�*����CL�� 7$�MC���𰂺�R	F2�;]a)9���G B��j<;D�����S���(�x�c�^ �0�^5@�W�k=B�+�p�w�A�e��d�p�ȁe���crc� �=�Q�f�� Bt�]Z@c�P�H ۢ�H%2Zu��2�Z $��=���$R����j�W7 ��Q8�9�'R"��9�A�p�F d%nT�g+���x@o �ޠ���Sץ@���/���$��1"�f�����\T�TU:M��C\�et��"чճ~��!�`e(CY���Psř^��ь�{��؀��hF>�1��"Ox9ȃ�v@`?��{Z���@
�@��%���ǤZ�ff�wd���p9��qj�Y�y��"��3 7Pb ��0�$�"�N�%>"[D�A9C$D>�d�� Cc7`B,aF���^�aN`(�
� �0
����*�^B#q.��v �wKW^z�yGfb&a&t�<��7&� �E_30
�V� a� �P,�3z��:���ym��	��n��z��`sH��`��	���fa~�{�1b�Tl�[!E�6py�t�/ 4V.��p�@��v%0�W��}��/���M&�q0$� ���*���4�Ǥ�p� s�� ��b��TY�%ff0R *�K�r&tu�XmS$Bs3�e�AA���z0&�[�
� #�K#� <0*W(
HQ����
��]T�'Qj��
&�^B(�lc3Q�wCB}4xq0k�����gi��u�V�760�@ư��O�P� �����Bz��G���AU������z��2�z��z2n��zt8{wx�Ơ,�,z�&�, ���1��,/ @�#
�6�c屉�!dPC�J �;P0��B�0=�Ӗ3�܀` fp��~���h����E` ��԰La&�+�!) !*��ؘ)0g��"�%�d P�:�X���B4�#�
�j�vB+pN�;����
h�>$���Pa {�����j|� zWB�u 7�D � bp` ˙#�	�'_RT4 6�����_taE0
�0)��T5,�b`�va�Fm�6U��U�0n3Y��0n�V���z�G}Пtؓv
��|�n&)*l��Ԕ�3ϐ�26@��N������pס��"z�;�}�C�qWm�Q �h0`0a�X����n0}I�� ��h�Y�9p"!a��'�M�dg�2@� a3#a�D �N'ЁO1]��2vG!24��AE@��&hj9�
d����i$(� 
*1� �k�� � k!��U�^X$3�+�,�x�R�F�$ S�Y���8#�F��SZ9�W9l�l�z}0�p�&	޶`��z�Gn�@��z�W��6�`a���Ky�E�bPi�o3pK�<������ȡ�c����@�Q�\^=F�c�"iBl��%a0��r0���ܐ-s��n��Pf �@@��%�K� �"uv��/ ���z��qNt3Х�j!��$E$ \H�U�l�g$�G����;;X#�^�	d&�
k
%��G���� 9b#+��f^6#�uQ*�x,Q��;�3y�!�a-T�Go�y��b��������z�� �����0	�@��'n�)	������7�w�� �4<
o�JG��3�ðpsQ����}���Q���=f�7'e� �n�(WYviٰ����h@��Y��� a����܀W$�%�Z�{�{ ��P�I�p,�#;i��N;�$us4j�w��# B$�'ƥ�(P����(� <�s¡�����G��t0�ːk@kPk,`3X�Rq6�%�� *�*K0f G��Рlf��G)8��ӵ��������ye+����0h������pj��0	��z���0�����9n��G�R9�$U��1���iE/̷f�(`�P倡wW�A��U����k Qq q�c-z 3�L&w�()���f��З����nP_�L^` � f� 	A�-:�� @����p
�`cѐhm�NJ�@�$S$4a���jb�:!E,q�$ 1�p����&<XuVd�|*(P�ųѳ�KxjP6)�Ac�'��"���7p`�B�h8t�Ia�8�W`kԵl���<�Y5	���-D��@.,· �$<	pK���j�`�7	�����f1� B�� ��q��|`L�P��z�!q��pV<�%�}ds�і&�Yt���`��P
�՗���`�� ����b`q�j� �3p ,C�w�"�M,R '-��@'�
/��m��m^���#Q�� $ 
g
�( %"��?�1�hj������"\j |����	��EXd#p#6rzf焴�H?�+�2�1 p��/�D�tA<��B�b�S�^+*���ll���	� �j+��p�� �(|��<��pK�7		��X��=�$"Ky��XI J ��+)�(�x����WX|W�u����]a���e�]���e�X�`N@�7 �P�{��(����Yf�2���{���!Ԇ0�@Ӡ�PaAX:j��eZO�� $@�åY-4)a�p�
��ڥʨY�(q&�����u�Dͻ6�D�E�c���*�@�# ՖS)�P|F�0���ە#��9�0٭�#L��� D��*����1���`��ڻ:V`Xp��a쉠A�s�;
�1��*���
�P�t�vuW�}��"�#�����"Z��G��i�#���ײ�׀�!�����7�rb��NQ1��jK�A=����p
�y4P�#�[O�d
�SR����G��������ᝌ��ml-�!��k�.j��['D�6*!�������ũs�R0�@)�R��&��v�QE���8�l�0�6	����`�j��.	���� �� �� �a>�e��ç��A9] @l��0
4&
�В"X��������RZ�Q�ݖ 1��f-�Y�X��ڡc�p�> �� � f���N� Pq M?�Q���r
D�pѐ�)�U1�;N�<h���뺵ɠVu���_bR�pᦜ������֮��$��΋������K�|��Fr(b�s�_�͍M��N��τ�Y�,L�����p���<)���q�M�� ?n�VPX��m17 �be�f �5/r��=
�@� �v�e�c�J:�@�v� �B�P�f�a����=��O0]!���sa@�&g¹��O?6��  uc� �ji�����[ ���	>#F�@�a����z� �	K��A�b	>�z����(�2d4h��  .����
�S  � 8�3�P 9}���0U(PhJ!���e�O�Vs�ޕ˗��he��i�V�Z9mڶ]Wn�9i�ƅ��Y�c�"-��$I��LR'/1�����Sg	�8Ȕ M�4.��s�!����ʕ+_ඹ�v�\61"�H�ƔQј��ŋ%KF�5jT������A�rA58]�	H<���jЛ8�h��Q���L�g�n����5j��Q�G�(,�S��g�#�0� Ch"8 ��<��� �f��&�qN���(��(j���0`�:���^��ZDF��."ċl�\�\>)�Ĕ�
�@�I�Op@��ʢ�J�
+<�'/�`)�"� *���(����d��p��h�������ӭ���5�EM�����3�%�0I �u����I�9��t�1�u ��C��
+�����FU��B��V a)F���Qv+[�Xⶪǃ0xZ���#��k���]�����Fn�����h`����VX��z�����t��ܸ�1Ā�1���A���A�<��3�ps�!�Q�%-h�g[�hb>Fᓇ>f
�ǋ@4B	%>2$�v�!�����%�����` ]-�)�/��2���`C��P#�4��'�8��i�9e�h�˙7�LmO��:ۮ�޸+���bïE=�d�E�,�G�d�q.�4,u>UG�s8�"D��Dgw�m�'o�H���[�yf�v{�%�V��)���]��Р�h���:���  <��o�@��0�p��yb�|��z��f�f|�j���l�1|��Fp�H` ���i.���u5�������?>a>N�$
����bd��
�FЀ���e>j��P���Jj��� |h�)|�4@ P� �B ��K>�R
����lx� ���d�8��0�z��Neq��Ҷ�6��j#�jԒeLbQ���� A��Y�\��e��u�#�8�f�As��@�8�1�An/���Z,��� �q��1�P��7�XB�3,�� ������@�Z7Q�����mG�P7���g�<Ƹ4|�Jjp#W�{G{�a�d$�� ��3���f��0���*2X �6�i���Ϝ��@"��"	a@BP����J ȀD2WL�>RB�&貖�!f�I9Q�;�8V�T#�PJ:��
���E L�3J��	L�m
��ۡ'�h����gabH�Җ(�e5lXF���>��n[�i�	\�æ� �8�14�t�@�MoJ@ "CҐ�1��ȱ!h�H��Q0�s�a�(�`�Q(�t�4�t:p��.�%����3��lR[Z27�G�z��0�R`�30��X��Hj��ĉ>�   �[�(÷z>b�`�l����i��s��U��"ؠHEJ���'�g��!
�D"Sx�H�Q�� A# ����,��
��N����}� %��P���A[���P&3t�W��. `�*9�hq�A߼c�bC^@*5�mK|�}�r�$��}	L1`;�t-3�K���B=�:�JFtT����M'a;�����8D|ԥ�JN�@Bl�)<WY������C��S����]�:n�ε�u���<������.P�ɘ����0#zד%2Y�}p���G7�э0H�AUZ��T<������'��!J��2�̓�G�ČH&���9Z���P�����[�@z A\�p )1�0Q�$��JHC���T/e���O�Ls`B���V�1)D�baCϢD���J4���6C� �z�
P�����K`8S~�M�"���SBŅ$� ;��@�0�$�-o9r"JE1	n��"H�ј3�Q��
G8����c!D������5��.w�l��Ҁg���=�q�m��G2��<Yr��5�1f~�#a C���C	��2�q%����9�Q�c��
O0��P͉�v�����N�$�EA����e�Fp]�~�d��CN�iN�A���4
?�4D�'+|
�0,e G�R,-(��
z����b/e)[��V��ʅؕ�6��N���`�en�	\t����9��CFB��'�av���}ж$�M�A8�o'���o\5#�X5�(n���I��a���� gY�c�vLD��o���<�q�{��ƸA+��x�z߂
[p�c�؞3�1HnXb��b
�� ����s�3jx�kh:-`�)�3����	��s��; `(0�S%�[��Q��S P�Z�����  L;��	�� �#�
�y�x�1!������ų���<71�% $`����;�=99 ��+C�@��b�`���H�8��A����nc���O1�q���=�=;��:8D�4B��)@��+'��ː�:�g�>���� �Pֹ�gA�����H`x���g �180PP�`v������j�9�2h`�d��z@_�Q�W�0�h��I e���8��@�@n��zP�+p 8��Bh��������P���H�0K�Z 1\(�Q@QPAA�ِS@��x� ��� B�40!��3��:�(��@ ���TJ���BY�   �Ě6�
s"(�Q��z��36=�0D6�ȓ�a��ԁ x6��������>h�k�s��������I ��#D�+DB��:��hX �(�X�c�K4��>bQ.i�1�����X��/SL��q�� �:e��n���)������X:3X�fp���Y�tFj8@�ڒ����)�\�	��c�3 (�q,��j�&j�:�����2'y|�I�{��K�G��s�|��I�G|� hkX�ôP܃?����eQ���HkD�����
���1��.� �
���gh
|�����/��ɚ����=�O���6H�z���6K�;��K�ådJ\�����4�d�0D@�u�==�sI#8�Qx���KKE��X�J����iX���HR����	L�+,)���V�ch%c@9oX�"�l8��)@���;���	�j@��Fms`:ˁ"(�����&�y�>���	T�Z�M�S3���I�<�G��s8!1C L� m�� �$( @�O;�	�-U��z ������«H��4�SGX�'�<�`����>a�;�O��q�Oh0��s��Ճ:胸�a5���d]7B4�fE=l�@DfH��!�#h��Cˁ��Q@��5!P.i{ѯ���S.�!|�Di�6I�$� �dFXDxx���4`���2�h?��pX%'x{�9U���A�f�dX�c2�.]3j(�/}��$�r��	'A�t�sD��ۼ �7\�߄ٚ�Z0��P\�0I��Z�Py+ՃC��Cx�S Ż�C�@�4Q�!�+�Z �T��.���hӡ,)խ�)H��A�p�ǙI6�����W��%J<r�Z}�f����I�I�:��:P��E�:�Z�0;8�A���J�������'�3�ր�����e +spQ�CŹ�$���E�6�Pō�2x�3�~��ip���w�?��
w)Xo�+{48�R�n 4q0$��h�N:���\q��T^3(� G��b�6='���O��� ��S@u0��S8�1N@8@�I��x�P�A��PGD���%*@� �ۡ��/��������h� ,I�6)!���J-0�s��؀��*� ���ϻ��4ԋ�P�C�=o�CHP��P��k�u�\���h��Vl�6�b)�bv0HҀ8S3�R�V�ت0W�x�� �St�=U�]S\�vE���o�0���l�w&`���]b9�Q���2n84�}0�10�w0�-���Ȁ���_��m��^Iͪ������s��t*��������t��VZ��P�MCH�>�MI��x�eIX� >CA�  (�Z���T� �2���T��Ljt��؜% �%(a��3��$�T!��H���:����<�`��Q�e� 1zS\�e7;p�fE�f���M�B��(�J�|VK#��� 8SI8�5���cH]�s�䐸�Yc�� %yci����8m0�lq�4@|Ȟfp6 �Y����艞�+,���4����� ���^̕���&Ӓ�v�3�i_�1
�F�O8��H[�Y�ZC�SG	Z@��y�ٽyHY�f�>�� >I8�� ��(Z��jf�+�����H�([��%�fpn &`�E�@�BO�k�����K���'��x�۹`��q�c�V��s\ǥ��Vhf���^��V�*�bA8��b!`1��������c~����˹��`cs&�G:sFg��+��}��1��wp�C2�-���@p�
�`d2xE6؞��n`��) N���؎�&_	��3m ����U����j�)��E�A��pA0�E3S��Y0����P_u�K��� �Ep�e)I�ADՃ���1�h���E��4A�Ķ JE��xl�xI-K�-�����vr�6 �ot�e�l���D>��v���~��0L@ �q�J@�0�n�Ǖmԛւ.�$.b(��kKhB��U�� )�Q�x�n�[���n�Ӏ���i�V������0x(4xP�c~h�{����F���B��l @��o�����~�	��jR�m QF�=8@P�-G��	��xp����Z W`�O�Q�r� I0=��	%��C� <@h<�;���u�� �S!�����o� �N�(� � )��
��yr'�r�le���v� 00�1�!c|��G*b��#�
XV9�PC�P�� l�A�y`�&����~c�ι���b)��V�+H�:5��±�j��c�����0�R4��I�'hn��X��0)��y2hu;�c31��꺚��g �	|�"���R�������B��a��r:��o ����S�T!�fQ.��=�>0�p`@� 3�(�	����x � ����`/�᭸�ɦ�글��ԥ�̂G�O�T�&��2��reAaG�Z��#���i�Ax��E�gmy�ʏ���x=���'Dp6�׉` ���X��tn����R���1�pN��Q�f�M2���la��8�`�{и݀��Y=j�����4h7�P�w���"�q�q�$ʔ0d8�!�IjΜɤ��%v��I�'P(��@��ąAG�!��,���2 1���F�1�j�O���z�"a���� �-��\�b*i���� �o��8�@ƌ&̨�8��%(`y0���q�OȨT�H�D���,�����{�0@�@�� �&�����.�m�x�.)�.�)@o?| Q�:��B��u��>\�����{؞�M-h ���f�F����FG,!�(K�1J=�Ut�$h�A}@���	$4H��h�'t�$��|��@)DF��CM2LLc7�pv5��p�4�|T
7,D���� L'���(��M�H	�M�a�P=EB#0�(�q�T�Q�Ջ+&4`@UU�T�(�t�'¨ً)#���  �Auo]��+��R�	��[  �ے��� (����6�b ��E
@�4��>0 ���'\`]TPl�	g�Y��� ��+��Φ[���� H9�r�  ̺ԁ � �Zg �Uw�y�r�	��	|δ�F.�W�K ��(7��fR�(�أ �5ܝ�lZ�`�@�J�]��	�b�����C=n��8�p$>���D	z�D�827��1�@�0t�I2��K9�3M4Q)�3Bl	o�	fQ&����@}�fX����@E�%�D�K.w�5��o��+����	�Ij@d6���@L���d��
)`8��>-�Wv�A����ڣ����W�k��V��76q2N:��w�e�A��u�0�xښ���4�M��.��+��@��8_��7D�K2 �
Nðr��˰�����0��!�����`�@`���G���<�D��
�8m��En���
b�n�!:c��V����lR�����,m|���PL���� N$8��a@������Y�)&��'P�����8�A�+b�(h�$�� 
��T-	U؛���$
�T ��
0�'QCA�H��;@��!��a�W���lp��7z�6hZJ7��\@��!� +�
;�X0+rM��:�� ń����]ƻ�%���j�y�x�2��6H�	G0�RI �q���i����0�B'� ��w>T�4��%��p4�A���6̰�a��E��Y;�"p��#hx�h��ج(��I(8Al@0�I^��uRc/� Ըd��p8\z�U� 
P�b�D��RBy�3(/������b�(�&���� HDbZ28'f�S�J���(G
�	��:S��� �!D��3"Bz`߲���_�t��l4w�͙��s�)"	:D����K�	��@�Ӳ�R�uP�@f`|�6�A>�$��<�0�C��&��":���	1�b-�0`�
%�>�@�H�n��H�C��7����x�l\�@�>��a��G:I2����%+�l3� ���m�A�skC�kR~�4
E�P�\w;ˬԠ�U�P�DTQ��CP�$$N0�1�� (@�!@d�Dـ@v�lR%�Á�L���*�v� ��'��g�E���+���řOPC葏���R.��2]�L#E)��Ϩ9�Y+x���'<�] +X}� ��D(�Z���2�BPЂ<�0����%�.vP�sP��R��(�= +�т���F7�n�f�D�1��4�,8IZ���4XCʙ�l3@���Ϻ���J@��28(���)%�[:�<�a�z��I�.h1����J�O�A�z��
�`��E8aZR���UK�,�J�D*�z�vc��@��)x�;O�H�����^���-X8��v�S���G�6��)\:u��A�v�C{���ͭ�OP�� �?a�Z��)�@�ܭ}�P�ipǰ��--T>e��}�����I��Ħ6���p��ژ,T�l��$��7ܐ1�e��"��8?�%�\�'�۝�c�M����j,��a#���R�$��8�9ҁ�AT(	ш�8*]�Q!	���8�^�qlz;���vNP��� ��)��%�z>�J˩z"蝰�ND�	� /h,!�('0ZԢəz�� EX��PFJP��e|�^���:��Q���fIr���z|*���$eD�v�~2�S�$�&�g�r��� v�I`'8o����� ��3���i
�WFWCN͋(6HX�+�"�Fk�Ѣ���(�I�{t�E�����^���0����D�9�2����V�q��𖖔�a�1�I�)��9�.�C:\�9�C9$�2����:��:����y�9��9��$H��E� ��Ս]�� � @!@� ��@	��B)�z�W�%���%�eW��L����mL,�Y^�^��
�i����k�p�p�Ȃ��P,YH9��p��[�]E}���DA}(G
$@H@P���F�UE~L���(�n	A�q��/���P�--`1�d�a)�I0�r䀎�d�H��;�?t�6�b5=C),?ԃ�̌��_����F���m�D��ܞqæ!`��P������OT�)��%��:\�%L� DC>ȃ:�C;� :@]֥C:��%�B
�Ä��$��$��CӍ���QW��
���� ^�������5��բa� ���A�U��ia�y�ְhl�ۂe���T�%L^Շ��,9\t�J��B����PP�F�@�I�PH��Ӕ)E	,�(�KP4@���0�=(H4�/чÁO� 
֬x���r�LR�G��8l�h�8��;�7�$D5���ϕŌ���b(����I��l����%@��m��M�R(]�u"4��^�%���� �C<���c4��:L�%XZc4B:��<`�:H�9��$8$\�$,�%�,���Pƥ �nډ
Z�%$��ڠ-d=�@�Xa�Mg$�o%�,%G�ib��m�� L���l��؂!| R\ER��Te���^*,fs��
�@�O��%�Ѝ%2|�� ���@A$�C3(qx��U��øT /9�R@\}���@�K����>hV)h�0��S>��ʡ��� ����\JD�Jx ������:y�5���ǔ�����(E�	4�:T#^B9Ml�� ��
�� �:��%\�$`�=��9�:x�j�C:4.��B�:"A��ct�o g	gA����Ugҽ s� ��D��tNd���	��l���'�X���qX�/y��1ˈ����4LU:�=e}��D�P �Od�	ݚK"���K�=	܀(��0Et30Ȁ%�����؆SiI���_�t�K��L�}Y)�������>���h)�G8�
��
�@
�����8P
`�z@���@�nM(��I�Q0�c�P�:�i��
�)�i� ��:��:��8 � X�%�i:\�e�9��=b�9��M�%X�$L�$@�����ih�`���]g֞�P� sJ�)���M�%L�$
���"���L�!؇K��
Ȁ�YǈxO�����F���Ђ$D�qX��0�YH��)G�9it9M�f�)�f�	AQ��5����3��@�r$Xe�<L�j���mJ�0H^��O��� �6�A�eC��k��k��5��3����^
�k
�n���@
��ګ��ϠDM����I<	Zl�	�����Z�vi4A4�c>��%x�:�� �)��9<�$T� L�v��N�8��ʣ:�-�`W��P Z<ɒ$`��	�Ɠ�œ�$����(�@<*�U$�у��(�����ӑ@AX�J��Z}\L^�Gq���'��j��9Rض^�	�Bh�蜯��mp�$-A���K�#Pؘ� �� �U����X�I�U&�TVb�����Ӳpn��!iČ�����̐�l��qg}D�@����(��"����(��V8��M��MP�7�`��:�L6�'nIsT,;���ƣ:�f�C>��;N�)��:��%��)H)��l
�)-��8�l˶�)�q�@	T`�F����B������ s�	L�(�!Ld%PB��N�!L=�Ú��>����8`B��\�]��x�Č���a`�l�O^�J:�m{�ZU)E�!K�~���ĝ�{*�xګ
��;�s=�s��p�U,A�r2THR�g-�Ҳ�P��v�$y���e�7h�3P�ʑ�8<S)�� �@�q�.	�n
4@��ݫ��\έ�5dP ?	fH��8c'"��d�q�֍�'ol�Vl�o9dr��#b`9D�9���~�9�&��/$�J��P�v�Z�
-ϝqm�,Md��2g4�	,�D&mҲ�$��2�b+�@�XS�4���!�w�p�� L.	����n�:K�mp���#�a��3�CT�@ �|��A�@� ��;��b�Cc�X�݀AI	(��4���g�4o)�H%,e��UL�%�TL����7�xs�(�A�+�@�n��n��t�
f������kN[АJ�\�ܜʩ$iQ�Ues	�V�I�r��t� �Ѭ A4L�W���7�9��8<����X�$��!��)��\_E�\� �@��&�����D��F!�0[B%��1w�>$6?,v*�0A��l/� �
Ҙ�Rh�(
Ԡ��-����wQL��e���̭"m�� 0�4�B��xW
��n)�(��2��b3�O�(Zu�I��v�UUsC��$oVXu�vFY��k�qw�|�� ���@����M�^��4@��t{7@�+������L��:5���z���X��bbR$) �mTn4{�UO� ��x�µ�`ўB4�B��@�R��K�!�B��uv	������8�BA)t3�;��;@��Qi�:9=�C�+v*A������� ��(�C>��§!�çB9�)�F�M@� ��9���;	B:�  ����D���+��5�y
���PA��;�= �����n;��ZW� ,4?("-Y`=t�x��DuÒC;n���Cٷ�t����G�@�r�ٿ�M������l��M˜�����( �����z�ē�!����G�eQIJ��ܯ��RTW
g���W��,��!�!H�擶���<� �ǻ�,�p0F��F���4��	H$$��%T����2s>|���/�(�b��><�;L�)�C:��) �;��(C3d�:	�
� ���������� ��z�t̳ ��H������K߀���pZ�8-@��GO�@z�6#A�f�Fy�b���'P�0���#Gc'@h���'P�$q�;,\Yba4l�y@�0��HEcV"�ŋ����E���FX!B�XDK��6��p �Ԩ��f.��( �/_b4�r��$l��c`0�/�0�2�LFo^�	�2e��=�I�cH�fx�{X4�5h \8م6�֑_�S�����K��_��n�@��Fd2DH�!0b���b�Ay����'�޻S��z7p�]nu�2�|�����AAҙ�!փ�����'��~�����FX0��0Z�4��A�� �ڇ�i"��Q�a�"��k��Q�aN��8 J��<X)%�H0����&� �	�.� I�
��< J�H���� ����j�9Y�3�� ��:K���J�.s���.��M� 
@ʅXCɄ&�)��0AŅ4�76OJ�C$1EMI��TE;�>\����P�E�G1�
� 8�
�J�0m@S�F�`�X�8`�f[h��:�ǆQP�F:B$��R#ю�����%�`�<�ZPo�%rOy�ɇ�|�ǔi��ǝ���K�Qn�Ї�th�C��ᆉ^�A�+6���X0CDf`��d"%F9%��>��ZfF�a3!�H"�%� �9c:H��	%�T|��p�0���i��%/;0+k��� �45��&�ȶ��|���9W���H�8�j!M���;���y���: ��F�(%.���7MYZ
r; �1�Ify�J�$#K5e��P��Pda6\5�M^�����
x���0����b@�f�Y
���9"��o�C@&��|�cn�$�B_Xj���_>y�)gFzޡ����Ɯw6�'��*�S䡅���8���-h(#�D�j��	7��PA�`$��p�<�c���!��ɐjF�ag<#��:b����haOH��� �a�Z�����A�:l�JP�0��~�ėt@v���B�+���n$��SV|��fyE&OYA��FCjрb�%�L�d�"̣7��AIB�H�IX���-n��GXB9A�VU8���`�-�
X�6�h]#�p�v0q!,0��$e!���o�5��  �i�MXP���Kp�(N`�C�B�$�G���k�0�`0�v{GA��L��c^�3�A�A���c�+�7�0
)$������^���-�b��0�Ja�H5a"�� D�
��c�(�9���\@S�,��z^`��,�=c?q �D?�@`dXP9|Z�����+5����#ab$X�RH`7��%Z�s�+�>�-h@[�Jgy�2�(�F�ր�5����x� x��������mTΪ�TǛ
�zt�#`��\�B���ak^5�?��y��,dAIKj�����xu�`U`rv�� �H@�}�CJPŻ��� `�_�z�ȲDX����Q٢�1D0�Ǒ}�B	DP�2]�4��pOw�GMy���&\���l�#E�Bʢ��%�OP�zd��"e�0E/S!��
>�Q����<:���!�`F"G�P3 cfXEk��p^�C�	N^r���%xT!�2��٨�3��hdC���3@b�5��ƈ����&@�ۃ�\0� �&L]�f0�5�p�  9Ȇ��:��6��\��L��C��0$`��
4���@1+���R�
�sͅ%-1�?�5���	�0��΢���"�GƆRz!�*�`��.��Hġ��E,�}8�VՀ�܆8�����,���Ԗ�%A�Mg��?�e�2He�/v���B��R���n��c����9E4N�r��S��:l!�h�;�����iD��`�w�|�C�Ә@ѻ^[ȣ�u�㵏z�c��	CI0
y�������_zd3C4�aU"��%x�a��!�g�iLc�R7�4�/e���h@���Ɲ���\��H2̀c�怪��@d2 y�D�y����di�K>�1^�D�� �$�	|��'�7,A�jKX�̃�#&M�*I<��j�s`��G�6bȃ"��� ��	DP��H�#�^wE���b��e�Ek8���_�Ҷ��P^�i!	_�!z´G�q
A].��B�Qt�^��Ok���r��P�x�;�as�"�р�4�R�rb�$��l��i�`	�o�)���q�o�F*�ˇOX���6(�ჯk�c
�Z���?�-���E�a�m���c��0�0���7���l���n0��c��@� ���!�!���a����6�^��~��,�6�`.�b� ��rn�������`=6�f��RV�~�
���H�f���d�h��� i� �ꦮ�n!�`��Β�l��$�L �ᲆ��c";�(�(�.����L��ZBҪ&URE<k ^�oJ`�b�h�TL�l�Hp��>�)�#����k|����_�a*��#�bo�N�a.�A.�!�A�a�@
�A
V������D�ҁ�b���̀�0�A�������"z��!�6C¯����jɢ�#"�@H� �JÐH� ��flr�N̠ܠô����� � ��a����H��� �80r�6�i>�D����xN�������"��I#n��`)�	^J�H����F!�Is�P�XҒ���&	���I$�6X�m�,�ð��(��n(dBJ�,�@ �n����p)���FV��,/�!4Ϧ�C\�,9e	e�I��ޣ;����g�������(}d��m����� S�Jq^���ې�����!��'��2B 8��m��G^@ޢ�]d	�i	�����\�5Sd���l�Ѐ� B�(�� ��@�q݌�8��a�����rhrh�s:].A��+.�m��� �Ę�BFCR�� V 0$����eJ��"/�!�!��X2	���[G��n�fa@a@RL� ��M ���'/A.���*�:����u&�Ϊ
E��&F�9�a	�������GT,o��PB�@o)@O&�@�e>�k^�A�r�N!#`��#�K`��,qdq��fp��*1����!��� �/|)���Mgd�h}�!璑���`4�A[����o��.j(���ih�&L�4�E�/N�,�8,vS���xS�@���a9�d9Eu�� ��  ��b���� $����!�.r"��	`P�c2n�=� l��OLaJ����AhA��*i�fs,6Tu$	��F`-�5�������c.aЪ�:�C;�.��� �0��)�dE_���Z� [�A	`�r��kF@��#�[*�<�c_�$N���-�Ii!.�!LAz�.�I���J�Kua��M޶��ާK_��4gSAN��T	��g��_� ���)�d/rN�%ަaO�eO�P�-5����j ��֟b�Q�o��Q��08wS�N�&n���;����T���sl5��8<PVq�.��������!ų����f�P%(�C��	���iT�uNa�Aq�A&�na��[�pNaYoA��54�J�RVeU���	��� &k��U���:4���Ͳ�E_� �H�PTV��d��kv2: ��#���
Q�d"�,[�Q�	|�^fqd�IO�ޞA��G}���M����J�=�^o���,sR�>a�(X�K�^�a�]� ����W[��eG~�a�Fa�`t����&h�6 �H���Q�ms��&U�,���फ़���/n�S/�#�ŉ��:�`���8�Ȱp_��|�	| ����@q�!<�����(b�R"3<�8WY!6f�����a�A���>�/�|�N�5TgfAJ���XN`��X� � �u���.tC1��XBE�WYJ@H@J�+l`Z���@z	�,Q: 	�N&�#g�4d��r�4�D8RAe���L�|�#k낄�e�t.�%f!ARa	R!>`/�ʡ;,��ҁ��zI���@������`�b�{�3M���G s4�vj�0�a��6��l�v�O ED�Q�!����mU���m/������\�CR�����U/P7Ȁ��`0�	����q�?�at��`��� ` |�	�z	�U0Il�	�a��aNR	-!Ԣa0a
̠��kS�� uac�f�4Li!J�f@f�(�:� ���A�w����(�ThBz_镒�ib��9���7a9E�\��b�!�aH&*��M6#�:��=�ԍ|�!�.poX���ѧt���a��e?���<�~0iWx�1�{�����%��G�׍��!�Ԡ������tL��LI�����qh�$H��C��il8���Xʀ� ���ז���� �V�,�[x���3;�8m� J��K���@�=C�lt��R wL�p�j2�@�`![�L�F��Na�n�0����i
��&w�ꌒ�� �	�ښ
ƭ	����/!��p���p�j�Xo�	;U*F�h�N�I@2"�2&c�Ї}{t!�l��N�����!Nᙎ�&���A�t� ܁��M�Y�!���%�v���!��y��y�!^qۊ���I��c�O�R���$�۞��J�LeI�� � ��q���R��d]�W��ySSu��xh�� �������#��,��oW��8�̠��!�A�]
�Jd~��0�!W<r �� y�ry������̊l:w�s�a�{z���ŏ�� 4�`�ӌ��,�&t ˾x���� ���a��캲�C������8n�&l�d���J/�.7�N�H@ By�R�1��n�a>�9�|�gd�e���^ꁞ�'�2��i=>
~�A���FFy�`��F�������֡�G	�'�X~�)gO�&��Y]�\���� P�@�i � �%l�о����;?��R���b���`8_�ݡ�UJ!ۋ�	�� Rw<�����6��� ��Nt�"R�	���ޏA�A:��Aķ�~.	�`�Xa��K �� �
nf f@]ޭ1�����A��e���'����j������b ��1" �E�;����  $H�@2J�;$.0@��Á�Ȼx�?~��y�U�ܻr�HN��L$�w+Y�s�2ջ}�h�2$o�Q߽��S�>�C�$|Yo�Q�P�Pb�G�@��۷�\�}�<��7�]*%>G�;�c׫7n4C#L4�� Q��xK��@��&�0L���0q�:v\�q\5z�Ç�m��3���ϛ�5
OP|:0`Ĉ1S����۶ۦJU��T�X�`���c]�-[&Z3i͚#c�>'��x�	�O�:5H�&7�Lac���k�ڷ�g�=j�̜7f̌ C���EQB	e�R�S%�0�:���� !wrH;�h�O=�d�O>�ܡ$�DB	$� �H|	$�C @ �x� � �g�,A	�q@Dp �D
$�� �;���D`p���N<��?�SN9ԜT�40���P0�v�<��#�;����;��P�<��Kp�9�(��gNK�b�����VzVM�%?Լs�ZiM�)W�|�a�QF��F	N9�WA6a�Fd�=�+j�����p���8f��Zb� j�M F�l3�m�n���7���-c�3�,CMs��{UG�@E �CO0s�R<�D�O<�L2�8C�=�Y�N{��Ï3�wR�p��%�(�0@���	�p��,I4�Hc�:�h�p��	""�Xb�'��@�E@��	P�c
$�0�_A=4Im��',t���MY�K$�Q9<q4i9��cN90z�J���l1哏;��b��#�P�;��4M*|�2P%�CR=��PSD!� |�BO�-�sU��z�e�_~�9��E]8�z����NP`2L@X	�Պ*��B��i��jf��,G�n�3��Ǌ�laFC)���l��m)���]tq�1�ی������E|������OLq^2�8��=��ӎv��=���c��M�!�bw��3���$2�@x��!�����#2G{�A�K��,\D��׀��5�m��\�3�(�R�)"  ��(v!�C	#��Ԣd )���� 걑/�D�Z=.Eř��'/YɛV2�J	�0GX�QA�BJ?�A8L�����(~��Thl&/����)�x�s�cd>>� ��qI�:Ƶ��k��L�+1�A��ڝb�U���3�����Q��[�L�L��Ӎ����T�"7(��!�f��	4���c$�����u���@ �������g8ΰ�9��l������`</Fp�H������q�V����5��|8#C���$n&	��v�@Ӏ@�  �Fc� �-�` ��RĎ@�B"��
"w(a"-���0
sĩ�@�:G[�sH\�����h�<l�7Qa	I�#M�(B��o?ڑ��%�c���O:չ�d@���(5�.������Ql�x��0��V�,�\R��ф�W�i��X���@�K8Bp`��<k8��TQ�`��z�C*�#3��L�2�a�.4!=Ȉ�(�����2p7��/zݯ	$�5�a�p�3�_�z0���G�ڛ�@P�AA�A|ÂD��!Dg�|�e>_��;&�I\�"B�"@`Q���� � �9")}4#
qF&@4Ё���C6������C� ��>�B���BBE0�Dn��[9,U��ӈ�4�1�T�$�p��8�A��f�#᪖��yɝ�+?�e�0�.ye�^�8@0\.eS��"M�����n��ˌ7|'a	8�Kb�Ӝ����4D�g[�f7L0�(����?��_t�c�U�x�
l �֛3������װ�5�����킸����#���p� d�	�xe�A�XW�!@�1,����"\X3D,� 0 P�M  � X�C�Ƌ���tn`F-��N0F$��p�@��S�K���b�rT���a�6��S5IGF�?�ō�9:�$�� 5�p"@\�82=:�)���#��
�=7R� ���*T���a�\�2H'0�Ę9�y&��Đ�`��X�=6��7��f7��A�v6*NQ�THa�Q�2�՜qE�70�2\�I�}@#A:F/h�"wm3���(0Ɛ�qA���K\r����-9(׾����O@��8�$:��%��óO?m=�%@�;�Q��� >(�F�J!�`����� �Wjx��i*���	E��;��U�������&/cIN\B̈3FA��x�F�1E�Q����I* Ω��ܑ.���9�V)�E�4��IqQz���: C[&�`ʳ<��;��3 X��n,�� �;���PD�6� �1
-� ϳKa�uײ�dF@S����p���c.l�E��w{a'PiH��Mzw$L E�TR%F-�/ �7&��xS�=�@�$��1r�^,0,���0P�0��p�^"	����z���T2�e0�Q<�" >s!V��Dp S��_�&DPS+UDBb O�Y�@yUG� /�7&ܠT#�6n�)'&�G� xX�q���~(q66
�C8F���~'AFp3)t�[�%�pV�p�d�0�2�Y2@FPF�����s����bh f�����n��Ȁ��n ��	 �Y�P� �`�Q�`/��{@S@9Y�L�P
8x6��/`=x.�qv#�00�0
,P�5
r�>��3@���Vf$�{��'� �G%- �j��e����1�4` � 3��, 	�@!}8�`����p�xz}�'P�҈�Q� 9�i7 I��E�5�VRiwYD�fD��R6��a1q�P���0&�@�@�iq�xiS$Qxs�`c��U�H)�~�����~95���5�yqGF3��t�Ӎ=FFc:5BAE�h���4Pps7����*��s �s[6�`��*�x,� �h ��牞�91=bn�:14^nxR4bJ�]�1^ vi7�����c.	d)�O�̀�1w禓uQ@C�1wŷ� �� 	���y�_@M�`ɠ<]�4� /� ��34CD,:017`I0�0����@P*���� ���F �6��f#qG@+`{�B���{Z
|���W�-55q��n#@�p
��/P�����@\��~$�p!q� ~z
�9
�p�7
c��`59
�[���$q�r"'�	�i9j5	7F�9�*�	*@Psr�$��B1/�s� \怵�s �
��[� �����`� K�D���!�مivG���� ӊI�R l[6�_IM���.� �R�0��M8��3A�*�֪Ձ�����������z��ƀɀȠ<S`3����@4D ��A:`�0 �0P��y�0		����'{���`P �V͙me#`8`+p��Z��W%��`�n*E�,u�J�$1�d5Ԡ�Z�Zp?S���b��� �3_k1gZ|A1B (aQc�8b����g{
����db-����r@�Uz;��(w�����*(���.���ʸ�:��R
 ���� 
0��<σR� 
��J��2bqV(@��5�� �`�ຂPYDP� ���+��W����͐��L�P$������Q;�G1���� ��@����@
�=��O���� #+�M�k, Rp7	��t�� ������ Ru�m�� 0B4�F^Wj;�$���5 �(5��`:�V'�(�Ѵ�?M�/�4
�0�k'� +LHrh�븆k�7d[1p1kQ{�OQGe�aq�
Ѡ��HdT73������7 ��*U�@�:���1���a\�@�۫�� �3��� @ k[Ǩ[��/��:qe�f�ZNP膗�����PxҠ?�k."Z�r�Ȅ7�_�3���Hd|���@ʒ0�@�j ��pv�L�� 3�<@p9�[/@4A4v�A��00
� "����j���+�����T:�{4d# �30�xG;%��C	|�6o $��e[s�����E�>60Uh��d��KI�/5�g���7L��7L�>L1�\�/��#O!�#:(O�#rl�E� *8�m���I���eaܸ�< .<� `,��� �ã+� �3r����F��**B@��'�6p�3
��TLVLF}'�� ���.S K^}�K $��,�m�xՑ�@ʸ�֒��`ʤL���0Mk�f��d�� ���O�_�ē>pP@�������!����� ���e �ž/#D Dy�Ɨ~9%�5P e p�$ ��)���czD�j
)��d��D ��n5���N%�D���tЭb��[�*�@*��i6`�����	�*~���;n���b쀓; .y��3 �-X�-�eu$9�kci�
KP9HF�A�$ "�! -p� ̰�A�F}��>0p�����!�7&Sp��3�W͋�+�9�׺����� �����lm	lל���f �@�[iA442�.�"�@�련hC��`���̼
q�(�:�� 0 f^C �L}9���ڝH4 |E�ڰ��m���R��f{
��m
q��I�!|Q1}��PR�O60��y��׽�����4�~!k$�]z��� *��ǫ����!H4n�Y���.��@j���� C$�(�*�2n$
�6�Fp�z- "P����N��%尙�,�7 +z/��Ӡ.� � �$��"p�>XC7�<�-�Q�n}���`�<�ֳ 	���~V-�fp�4����|O�e�vO�~/�-�6tS�=���� �/�lC�zj���@SB%S"��7��V�b*$�(.����0bD/��on%6�]��5U��$P�h�YO&[�
��GI<���5c�H����=� ǥ�ݟ�d��@ 2��x��-�� �2:^,������l0^�@�GA�0ʎ6�GV���A��"��!����,#���Ǯ�Bp�ܓ� I�S�A��R��3��ㄻ<Q�wJ��A���L�l	pͦ�P���ʃf��H�f�@����6�/0
/��PB�@�-�^� ٖ�� �Pplo��4=#�+��}��r�5CYi�(>��T ��`���]�t�A�L��$:hЀA��.h�0�DF"$TjP�rԾz��ɤ9s�M�8s��s_�{���{W���i�-�6ʩ�%K�,aV��5~Y��ȨE�(N��� �O�j1��IN�<a�B�3j�y�9�v��O���ɐ$�+�vhРh��Qk:jP�"B��h�bC��$݂�^s�
$�� !P�@y#�*�LC6�Ɋ$8РG�'F��@�   ��h�{��1�pA��Y�$͢5i.I�53֬ٱdl�<�Q�z��'ѢE��!��0[ީ��|<0�Yg�wR��^z���a��OP Tj �x��ъj���#:؀#4  :r �{ܱ�+ ����f�k��&�AL$"(�I���0��9�J҈�P�����Af��*�V���3g�GM3Ӭi�����;�3MsV�����J,�N����B�Ђ.6"e�	�;��.����k��Ԙe�c�.��A0Y�Q��먼�Y��!����H 	"�@�^`��h�Z�Z��U'n���]��5���3 =N0�J0���TH!7� N$��O\�oA��b
c�1�������X�>��ZpQϔ`��%�z�y�����|��E��'�B�ƖTD	Q����d��M��I���h�� w�G w���2�f�j����$Nx����@A�� ��N�`˖LR�!�� ���g�4���l��1P�9�d�M��4[�����N"4�6� -E��!�H��b��x�R�H��k֡&�|�	���FIz�|abd\�����|�&�=�����mf�c�qfL09F��-7�	l "x"^x�jI�`q�S79�.�n\�X`!4�DӠ���^��Cؐr�OX�H���-�g��3�x揖Z&�e(��gy�C�;26�G0��xD,>V�aă��(^�. ` �\����F*� K����g<�tp�3d�H��3����IN��X>�q�G���J�7"��%*1���'��&�8[�&Ÿ�	�Y��F������$5�[�2�@�s��F��&�%/��W{*ce�a
���f8��}�KSy��^���.��l�����G�x��5�Qpԃ$ BX8IP���E8X��019�RWh�U��;*1���q̯��W/%!����ъ�\זH=�> � !	Cċ�Ƅd!� �$�Fx&8	Z�b���"q�G��B�>�1�|�c�1�  p�@4��E��׼�2$0 o�#�� A*��A�Cr6Bv�6�'�qj'��!���o|#�X��0�N�>��D�A�p��&��	P`cX�P���Sp�����>�cA��.xA/���o� !_R�0�c�0�M#��d��E�iP�=�4�R��	PX�3���յg��ؔ��!>�0�(��E��t�&0�Z9��\�Y�!����h@�]�B-���@��qjܘL�``��,n��Y�bB! ?�1b���H.,ш\C栅9�����,A���8�Q����pݲb�"b�0�̑��Dg:��rԳ	U��HЂ��%B0EX4s�CD�<���G��p�j��H�6Ӛ"X�n��i
�j�$���j$T��@�M`AuV�����и5�q�|�	�`� ��&bU�҄���2&2̠L2F���T�� 9��"�@4F�)nԃ	l�k���A|x( ��z�J�-NK�#�ʆ�	�%-$a�?���$�9�id�	�Z_T ���	-�~�\L���Ņz!w�	��Э$l��[ W�똄q�u���8ȑ�qB]8@>p�H*R�a�Z�c�Y+����� '� 8`l!�,R"��W.��� Qaw�t"�T%#!�(�T6�;�6�"?��T�o��ZGJ:������~�MA�=�Y�1�`~3A
>�&Z�K2�HƪÞ��� ��L���L2�������Q�@�:����.�����Y���� �-�J��� 
Ҩǝ�џ�@S<�8���a1�	]�����LP��{A���h4Ȱ�z�B���$zk�[���#A	G����G:��Nx��f�"Nq��O��:5���;�����Gu�����<�Ć�sԍ�����D��e��2��6�1d*n�+��mZ}NW�m@�'���ARR�L!M�w����t!�a+���~�_ ��vB B��{��;(��=P`B��3
polH�3����+=߄�7&�8��A���MX=�<#p�Jz�c�{Ȋ��h@\p-I�s�|@}�ʂ(8�ڀ�)`�S)�CP�hx�t`4ax ^��}��^��^��K��KP'��IX�D8���b��yP��wz'!Wʧĉ�u@����S�5�0�hp,y!�6���A!� �X"�"��rPZ0�S��	�ⶓm�0�K=:D�����x?a�А�������X�&�9�(�c`���X�'��:Z1~#?ʹ�eP�A�>Ta�e�i�e��rghL�(P����>c�6h�d�'p�)(��#=����E=�=؃*ӿ\$�$��k�~`�Ԋw:$�q��@{(�e��SZ��3�N�Їs �����z(�x��t�L{4wP�t�O�a �ruX�lxqɠ{:�p�-*��<\���������� @@A����j	%�H�\+�r8�B���5��[�k�ӫC�|(j0�8��О��1*� k�_Y�E꽺�p�H�&p��ʗ%S��h�KiL@�B��#�ι�B*Kih�iR\K�9X��2�gP�&h�c<Ɵ��]�<�9�,9׈�tF~(��P�1iL��чupF�t�|xLrX�|�p��qL�����bH���ż��{�xPH0�EX�[��q�wJr��P���\��H2����Q�����s����� =(4b�� S)9�h��q�d;����8��8�@�6��I�)��C?������ � ��"1�d�HA6p+&��C���Ec(6@J%c틆볜d`���A���@�)X�($O��#g��;�D�P��8?6������_�˂3<�rx{�@l�ft�v�Qp<�>Ɋk��fl����҇�;�ԅ\ ��b��_�K(�x��F�]�H8'H��KP�R3�R;�|85u �B�@H�h�w(� �Q8��j!==��#�
8�>Ќ�ؚ�Ȉ$h�r�#@q,QN6�
%0����Ԙ����!�6��|?!  �c�c�F�7'��&X�Aʋ��c��{�Ơ��e`���JYq��h'������jR���>gXR��0
,�¿�+����?��2��������D:}`�ǔ��
{���LxʇIH�s@Ru8��2�
��)m�[��s�FI�WYЅY0'J Sy��|ЇA�qЇv@��rz��Y�#eH:�i0�Q(N���p,uq�S����SQ@8�CxH�h����I�X�s,�*����:l�����P� naџ=UHa�x��d��`�"Д���i�d�L 9���ˊ�+���d-$���*g�/ �y��g���Mhh6�$p���Z�`F��?�SQk��|x���� }@���Q;�XzE5r�.��y����
�)�YP�t��K �GӅF�R�-�N�N��|���qh��r%q��]jH�i��i�H��'� A���Ν���m���ɫ׀\R)ٛ�Vb-v� ��ɤUZ��9�O�
�X�ty����!`����"x$6X�)�7)۰m|��.�cP�b�*Ր�;��܋���bU��3�bue(��l�L�3Pa
��_��k����,����֒3�|W�5L?aF��Q!�\��L����sP}��|��,%�٢�v̅K���:��͐\����O��|e�v8��4��i%рBb1A(��@��|4N�{�� ����!]d�# ��S��!�<�#�����)����Է97PUR=���l�ŤL��S$���iP�Kf���(E'ËXm�Y��AҔ�[��(���ı��+Rt*cp6�gvf��]���a*��k�a��a`lQA���%W�m�!>�"�\3>��|�ׂ݇�;,u4�M�,ƅt��[��RKc��r�3T�.4=���PZjH�V9�%|��5�=�S� �d������ ��*&y!h���H���Od��dZ8�	�X�%��<�j���� �[:���U>J�Ћip�if���>_�h�F��he�BP�(���&�$�킻x$���{�����K_L\���j��'y�Ax���k$b�|'���=�v���[ؘ�u�_��s�b~�z�t�O(�vȊxB�3��3mu8�y7. �^�i���N��w�=N���!�薱�h�h� (A茒Z	I�ϋ���Nu�"ON�i+b�(2��]	UD>�0Ꚑ�_�"�:BR�Z������ѩ�*i�1�r_MXb���&��h�.P��{$�i�.�=6�e3h  `�g`ph�VnV�n��_���6#@�kg|�}�r�
!V0fd;}H%�G$�Wr`�.���z6]
y�^҅���,XԊv�uH4��S#�$Xh� �p�-*!���S��Q��H�Y�=p(������(T�"w1%��V�I����� �����I�0w��� ��(���"���a�V6?�d[�t�`u�he�J+�����и�t�$��R�6��`�Y[g����m�k���L��Q��o�]���e���s`;p$��2���}4��gy�g\(5'�lt���}XsH"E�S@�`Ӿ�c�5I J�i �4��တ��ﬀ����փ0�i��+ьh!x�u��`���O6CL���4O�F�s��6Հc�_@�{i����A�*&c���Q�۴DK(M0���g�D���P����ۙ��ʉ� {�c �R<�ዔ �x���h����m`��Xwu	o�A��-LD�:|�Q4z��~4��=�r�,���q^��x�]���6ct���W���w"�r�����8��'�H�IyP�ھH��=��S�(]�ũ��6C��H����c�+�H@���1�/8�������^}���P͎�@����)���t�%k3� �����DR�t�Lpy�wb��E�PG�L��wd�3E4�d���3�	��g��r�k����VWk,������s�\���5y�%�@wH�8Cz:UN��^���%�WDI�.޲�+W9}���Sw�ܸ|��I�d�\�q����Ň�4�Ih(R��Gj�N*��]���F� �t����p�BB&��E�!>'�9qᄞ�AG�Pz�T�$B0Ai���,Uz��$��@��(~�x"6����Ŏ7V,���Í�D|`3�N 9!�DH���f5��!CƤY3iO^m��f˘�AH3cBю֛Z�B��!�݌�1c̔AQ��XcɌ�P�B˱cΒQ���L����X�̙=�ȒՓ_���|Lb�) ��D��=�x� ���� H -�X�b��!ѴC$�c-�"� �"�)=$Q/��.1Y�H.�ȳS>�xr:&�N�%�N;�SK-|�&�	D�p	Id�S��fN>Z�#�)$4��Sc��A .�#�8����\ �X5� I	P2�B����-@^}���M4�Xd�Y��e�Mb��c?�$��$4��g��v��BMh�Zm��R�Ќ3�8� �����"��}E ��'�1�H3�Ham�3E2�t��w�^3�!��
+ �L2ND��3[h�_��[_}�bJ�"dJ�.� �b�a���!�C�!��B�V��w��8��.���E�Hb�.B�3�%稃a>���O:,�ӎ9���
���3ΣT��4�LS�P�-�9w^�^b2��"p�:S�y9k�	H�P�wv`�U����!�-���yЁs���M�e���Vj���:6鄘Zb� (�$���������j�7�Z0��E2�4�L3vBBڢ!� �@�k� �2� ���2�(c���D7ȶ�,��ɘׄ
�P�ۖ{Ct1np�Z^o}'�y=~z���|t
B�2�9
�N(p��c�8z�S!�B�� ����/�F�h�%p1KHb#�њ����H-��K�a�^��f�љ!ѳi�@��;f��/nJ���r&s�)�R+�բa���U�b<x b �6�h /( �R���$�7!B�x"8GY�R�1��2�}|1��Q�hF2f�(j����"��|�+�NP��Y�?x�<[mktqT�t�Qh�Y���  a�fi�� ֮�P��D��a�y�q�% y�Pƶ��o��s�X$&lg��K^ۛW�4���AB��ԧ�uD�A�	�
?~�� zHfN`=���'��h�tV��!�H�:v	]L�ȅ-�1�N�1�8.F҉|�C� �r
=��>9�� �9��Ec�xG9�1�i !)*��"�q�IR�!��At��z A+��
b�xi!�a�0� �
�����㊔�߲xL2�}���I�5�Ch%|G=��{p&  �N���DIcI��bŚmA�s�`2FQ�QL����vʃc�2��l1�Z�"���wB7&0a
�s��|���`
Mlb��&|n>�)�����i��-N�Z���=�Na{~��G0�9Zb:hS�<����A|O?�16��"���D:,҈ɏ����'&Q�df��,@q��٧>�.?�q�i��|]����D1�0i!���P&Q�^tM���V�Q�� �ܒ�4����I�ۧ���0���>ޑ�;ƴ���,4�Bh���G�.s����L%�t�5l肬�@�dLۂ�1L�f c��	��CI�DI�ر;�1�hX�u��$����o� 0�B~ЪCC]l��!+YY��0�e-kل���PP�D��Ԫ�3Wk�|�Y_pFH�D��[��-��#$�K4B�hOER	N�%k�0j!�[�҃���9�1��A�bf��RVJ �����y�&Q���$�6���A���8�҅��.P�`hb�Xo�`�(�-�Q�B�(�4�AԼ��������_��.j���D@TƉ`��4A5l��kZ��՘�:g�X��i��ƃ�E.jAN] <�(G*�c�ny���j��3Vm8ڰ�n�����u��"�	Y0h	��1�y��M�h͑�|B�!�`s�zE{��,xޢlJ��Ņ%,�[Y�b� ?�1�s�|D�<�DC�B���)�	�I�'Ԙs�PlL��� ���A@�Ԩ	��a�ES=��*kY�  p��0���˘�D��Mo�4:�Q(A	�(C�Q@�i��1z5ޡm
wq~7�A�۞�
�*���AX�dc��Ģ��`&��ё�3�-�+c:��-j�%Y�׷�0�a�Q��*����ph���01���CC����Ud˻ߝ_a0'����=���j1H>�X�e��ei�,`fH��A.��me�0薌H�"<B�݂-8]ˬ�;�<�-,��1�f�BW��#=
5A4H`�( �� ��B]��t�����C:T�H�.��	 BY|��� ��� ��`���w5Q5�`���T>�<��<���Q�4\�)L�)��4[��-�= ��(��4PG�Q�44���Ā� �M�m�  uX}�@�h�10	b�X3�F��8�hK3LC3 �"=)�B����,�B�iߎŃ;�´ ��d�<��|Ǌ��ĵ�����HT̛i�e��4�B���:�Cʥ�1�V�2�-��Vp��A��֋hS/<�"H�-,�#Z.��9��ʀQ.C.C<��<h���*� �(S4� �F��TEN� a`�8�f$ʌ�9,P�9�����A�]�\����J�M�;��\�ܱ%[���4���L*�� ��L�(��PL�bP#���>�0�\A"^�Ub����U� \C��$N⧐
�_t�jL�0���wTm��<�BrB`�v��`�)T����"���	�B0�C9���<�c<�y������48C�l�|]$DeiE4��)dZ"�:,X��V76�������8^V�yO�BDD�C<B#H�,,�"̂.��8�I|���C.��0@'��uE,�
uE	HA@ژ�����ƚ��y��H�gGҙW� ���A��I���Z�P�����ܨ�����P��-H�  ��b�Q����(�C����H�(t��.�4��0�>��� V^A�(V��Un�W����$�ը؞�@i� ��J
���(�4@�\��1A<�q�,å-`�`��`��-����0�3x��­Fu<AD�Pe�E�,Y������K�%����� �/�O>B��ى��x������C��%�$X.�?L�'�D>��<=�/PD/��C��e��25� <J45$��j~�j~��	F�'G�';]݀�@��S��$��H��{5l@�]�M
���  ��h<
(�,�A%h�4x� H� $�H���L�hA"�h=<AH ��`%l���L=x[�%���������@
�
���5C�R�Y���1����_�B<�����&\Ղ[��[UV�ݥw��'i,�)C��|$bű��t�hng�lNu]��n��9�9D�m��4"� �����B#,��-ЂHHB:�ӑ�IH�@~��8 -����u"Fvn�C6�^� �ס��ݍCyJ�G�ČtEWD�h�'�ΧH�A}�A�X�� P�\TWMw΍_H	�(h�yΈ��aO�d4M��9��2��Q%�3D��@3ԃ"�V�k\���P�UJ�U�$@ �X�
T���շ�@�݀�*>C�A����G�-ï��`z/���0�`�,�uE�[��)H��C$�\.Ot���=��Kf�4��� °�ʱ�2�9LB���ms���F��D�E`E���G�Ĥj*�	1�Ȣ�B�1�edBb��]�Iw*⩐h�&F�]G��XL.���S��I&���]�\�)�+�IAd2�J�L�B9\*�B�y.O�!P��:Hk�H�M4�bLAuP����i��������l�VJ��n���$��_m�h�%���A���� WY�\lĆ�FpԠ&(�1��R�-^)�U��tO4���u���p`B�� l����l�N2��1<A�F<�8���f9����� �P �>�/�h-H4D���(�.�l��BT�0hL.��9�I�CO��I�C-$ ���m�k�m,�A}�2l���Tv��x!�-=�h B2����������$+$�{����A�X@9�(8A^�(��d�@y_*L,A@�B4xVQ�!P�!Q��rp
?�C�54���"J��j�1����h\�~�D"gDIh؞�l �� A
��D�_2��w��18����"�4똒 ��ԇ	P�ĉ��M��E��@Ay$Cay�� 'ck$�l�3~&��������\LI6n��Lj�-�4�)k����W��C<���V�xB�4]I4�$��>h�DĂD�v6��ٚ�V��A��2�3h�T̅JmF��m=�M���ݦ����������A؁ԁ�g�A7���e�4��H���]P�J�a�2�@M��F�`7Q�C;H�{÷�U�94&�3�28���F�� ��V���bW^AX�H����u��ʖ�`�pb�2� l��(�0���,Cp��[a_-�B��Y���p �W_�Ņ{@8�*��1�@*mp��r2_YZ�3
B&S����>��,] ��L>��%����V h��f�=��dc;E:��6ӵ�Ɉf	��N�زvي�k���3?(E�Ei@��v=+E� e��e�-�B���W@�r���G���@����CB�cU�A�$��vSZAi	��7o�єt|����9�4DC30�Vx�M��[�U�@�
�D��
�]D	�`�(�
 #4�4��r\/�n�eN'M�Q�0(|ϰB�� PB���{JnM`@(�Ro �D��lK���|��1D@�u����h�i֩���}�Sp��6��:=�̜C$P%,���m�cf�#d�K4�f�!�N:|Ěx�֚vC�jD�BoLs��:`2�G>�(h@T "5 �-�E��!dE�YV:������<U>��|&������DC�Kqv�:��q3�^��w��C��ј��:tI�!|�Ž�=x�@=@C4{0���(� ﱀV��b��P�e�� 	�@
 oL"���l$<ơ�t�ىL�'[Sh����Fm}��0k
x���^�iV�~N�%@U��4�}��7&���G���-̈����:`'@%$�"P�#@B#��t��Ցf����7��|�9��ӥL>���<�n�/x*/ �/^�z�5�TBC��G�Q�(R��@F$4h<@�  #h���	C�Ɲry��9���8���:!B�"@��	�\�r��--WnZ�hʔE5�YԨ�N����wL�����:yk�S�V^<u挱���j�`���Eǎ*�$��1�)H�PCǏ+�*�w s�$lvnA��O~D� ��]&0%Kv�k6��5c6���8t A��}"��i�=B�p@�c͚�~���	lg�_g���Y26]�9�9wz�6������mv0@ C�f��Ek.s�ps��f{�᧝I(y��K(�dZhA�SNQ�S:d�=�N(g�|ƙdAu&Q��|:'~N�Gu�%�^t��~��k��[,ԅ�q"ANY������~+ྌH�2��D&ZN����N1Ħ�r�Ʉ<�ʎ���C�C���,��20�����j����S�|�)��ʇ���*GH�Ԝf��k6c�i�����$�@�`��-~��6 !&b`����I �0;Aj�4X��"5�ڰK<V�µc���	טaF�4�.��� �BD\B"�(Bb�AF9F�js���������c#��C�>����d��@0�>����pYj��A�~��g�K<��I,�d�6%CS1�q��G�q�i�IbTQ}���|hiDYx�ȁ|�QV���H#�蔌�� ��0 ���Dڒ$� �r\��)�jJ½�P�'��! iw�m�)uʡ��h��:@G��;�sD�	k�F�QR���d�a�d4}��k�� TH8 T�,�R���"`�Aj���,  0�$:k�<� �-��c�1C��0&�d��T�"��B���j�Ѓ���?�raRL�qט)�����h��5؎�ͮ���m��3$ÃN��g:�	
P��@��.8�AA�И>(���;��(t���`�P�\B�q����9l3J�cA<ˇ�$1y����E,B���P� (r��!|p� *ÏQ|D#$耖��5��&|��8ґ�rL*�P	؅�AH
IHB�.!bViW�5��!
Q{�S����#*.���'�r��ofa�;���tHNoP�Ӧ�a�L�?�A�X`��`U`И܀	��50� @W���h6��$ O@ֳ�����4�xͳbp��U�a�2��Qbe���~ q�I ��ܤqe0�SXF�q�.��yu���p]�#,x�MtC���K*bB4�Q\�>!��X����	R��*�v�#����'d�?�L?���<���P�`Q>j� \HB�P$l&I�o���	����At�AL�A�I�xRe.�`�FI5b��|�\,G:�[�bm$�����&0�Ppנ�I����N��9����nŏl�L'��1�+lm�[���G*���4*�� )H@b7� ���l���\C3��g6����.W0�t������d�k�ф.c7p_��9��B�,�!��MvEc�b�1��汖,h�0��ټ��}�
`��{^�m	3|���d	K!��n��������>�Q�vP�����&��zp�b��&q��0�H�$�Q#I�(�<�:f!	V�B�B��Mq�����!+�c���� KZ��R�x/	�%�xZ[rȚH!������.cP�⣟���;�Q�dS�Vw�Q���@���1�D.�)s�<"ٷ>
jqԘ^��Į� ���2c6�)��@ 0Sⵝ�w�K�.S�/ҚV<���b��v�	0nm���o����):qr	b\�XFpG1����y���
����&]������`��.ӪQ�!S��) f6��e�Gz�Q1�����"�
��H&3��ơ���E���$fHB�����Af:S����6�!�eQZ��H:Y�P9�(�>"�bIL2����x܊�r�Cv�2C��g8#�{��4�qǯ�8Qx#�!E�iTŎ��� ���H�Q��:����eq}2�4� ��e,���b����\&�/�f6��o�L��܅��Y<?0��\��Y�<�1|���Z���!�5fX��n��[!@��Ml�ڄٸ��l�>��c4�YS��u���!�d�^4` �!
�ܤ�v�C��XG{�[As�м#n�S�UhC��<<�K�b�(U)!&A兰� ��pА��?�@d���'��p������K6A���R��)��x������"+܎�q4Ʀ'�U�P�Ti�<	*�~�K��(*La�|@j�@*8��bp�z	��T�\� ��v6�:�Z`
R�rh�<�I_�,X`~�%�%p��n:���adm|aI�`� 
���a;B���:ă� <�^ �5���t���#�B��=@##6�|�aP�|�����ς�˂:!�!i@a��P�rA��`����A$�\����&�Bn!�"i�&p� to�t�ĈM�hI&�2!�4{�Ĩ�d$�D�N �Tj�����Do\�6.�.b�:�D�ت��/���QT̋܊��N��,��P�̎��-bQQ���]| Lg���a����$�2XI�4C�v��H`0�� �}`~@�	X��<��>��N��&���/
� ����,�b_��X@����n_:����!�c'�%��>`�AP ��������#��"a�p�raHr(\�����`�ҁ�f��DFf���&�����'�(���~A �&��iR"8l[,� *��K��3l�%@`�߾Q�b*FS���^�v���H�N.O���$�o��ʔ"��p�B+�J-�
RN�,�B)N������!������1׫E��|nW���lbp�𹜀��*��t	԰_*�6%���m��֡%F�5�)0!6a��b��� <-~��.� 6�`V�� �.���P&�!\�=�� B |sP4����<��� �!����:�#���'���&�a�Z/%�A$'s�:�� ��Fm/x$�rO(u��,$!#���#�j�LB7�/�m-��+RAl�N!A*F�`lL������*��$/#��*ۢ)	��F�B�m��raI
���J	���J�4c��"�a�� 
@Ѓ>�#�&��0�� �,� |@�h#Z������N"B���IpK��0�4��%K����K��+��.��������C:Je��& ��aP6A�	4���� fuV��#Ɂ�B>�s�r�>mOda��%EF�!`!�@n�Yo��p���������G�F!��E��"��@B�B�.�F@��Q(噺�P�CC�C4�P���@$�	v
��P���%/��)'o�T��F.�*��$��)T�l�� J������J�KvK��ԋ �t�vE�N�ܭn��b_� 6�`7gu�}ă:�a"�i�s�������!�
QB� N���a6�!K�A6� bC��q6�!Y`x�#=�� �)��K�FD6 ��";�bҤ���
f�o�o�W���ڳ���A�YeA�U�anr�aG�fZ���4WA�P��S�d�x�t�UiTF81��h&]�k�$JLJR��=إP���
f__Bـ�� ��	�`
���`�o���as����ɄlI�B�"�Ҫ+� ��+��R@X �A0M�d�!��"
�2!�L��j�`yR�� �f��q<�6�aSl�n���iA�k���L"j�j����KR3�����	N�.V�S멞���%T�^�*�R*���4�Sr8��3R�!;��qC���3reA�a��v�rJ��Z}r���Ut�H��H�t�hl!a A��2�a��K�2#�R�h7#*�&n��-V�|�p����@NAEma�څ+J��7�D�ygOpRނ��l��-�!{�U�E,d���a]�l̀"k1b�֗}��}�w�4�ͪ29��H�	�`~�b���,kù�g��	��y��$�2h�FA��ef!��}#�b�6�$<b�x���`Uv�.����G7R(�5:9�=P���h��O 1�j����j+80�@���#�����l�Gz����'s®�'��A#�t�B&m���; 1*�ތ�K�m-�!��ܡ�tL��E� 	�XqL�,�Ϗ�2���""��(�)����DU
�%g,d�p��rP 	\b��
��D���JI�d�1F�ԕ Z��N7@4�`<���"���dk�������O�2��L���o��eN!A��b �b�
d��<JX~�� �` �����i��)m<V
	��욉*���0�t3�a���;̨:�{���x�������~�(B 4��%ڢ��s2��xG��XAeB$"��u/]ӕ�@P1R4���(z�!����)vL+���$No�b.ࢥ��*�b-�z�"����LnH�IlON	|���:d� ��0���A�V�e�*mX@6�����6ʑ	���)�0�ԗOU�\Lj�a��Bl�,h׎\@0R��!�lU��XjLFc` F��.�BU�@�=����%�0a���j����
�@�R'�A��!��{�����@�� ����s������'�'��t��T> �Z��F����@[4��l���[c�qXqp� ��?i-��+����*���ܯ�:��G�*��+%RDY>�	FV�1�;�FvdS��A��7�^I����T��.`#���5�aV��Y*I����AO!��)Xb)(��$e�+���a�}`U��RS���|m� ��I7NM��=,3�^@
�`�����F�!�@�5�����
�(9��ݓ�Ar� �����A����5tݢg~��:\�dI""������0� ����/^�嬠-�(J��%vP��{�����z%wI�n�(��1.�*���^����b�F�=��	:���G��^�Wy�+0p#N�2m�N@�I �`~`���@ ���������2�]c(��%�!���jD=��
1����Q<��X�%�@���&Ae��C4�:m�]C�̨������n���	?=���	z�:� �(yС{���r��q�_a�|�����}�!"8���5�ꖣ���Вb�	Ƽ7�  NE+g��r�4�N����F�G�Dy��=̇��CsA�3mԔQ)�;uJ
>�D	B�Mg6q�lS͙��Π�����Q~�8   'P��ꁄ$��a�CK�)0¦¦�4&0�Q�Fm�[h���W��@}G���7�ܺ�����Voh={q+>��Y��m�J��D�	<�2�sgSM�nyʔ!Q�L	Z=�D(�26H�1e�4E�l��AC 	��z@ܹ��9������u?�bi���ׯﾸ���K,�̳ZϪ{��X��/�|�X�x��%˕jA������ $�6� !`p�E�0��A�LS�4�8ʈ dbBtJ9Ѵh�E��PA�#D�C�Ei��E��8Q_�#� ��#OB,��S��MT���Mm@��PxSM9��	�yp�St��F>��L2�L1�1�P3�S��[|�c�Q�d���铏���s�5��s�:�)�>м��d[h!�Apu�S� �	���!���Z��:ɇT'$QDS`�ۭ���4��*� � q�$|p$�4�lsw���,��ǝw���y��r,����,�z�ݧy��b� :+H�G�3J'`�`FH�� W8J�B���@�8"J��d������
MT��2���C䑏s�:~h��,Y�-�ȴ�j���4����O�8��Q�0�R0�g$�ƨ�o�ħ178���{�g��ص�Q��W��Hs]��CW���u�1�I�E	D���W�����B�c
�j��$8��	#�0�l�ކ����*���"	� �r�.�uz��,���W�~��"�x����7����W�|��Kv��^-�
� �8H\@����0�}���+zh�4���@+��'��E��#�������H��#�NB��&�(���6�k��n�F�C.=�Y�$�nT��R 3�O���rjd�	��F=�Qj�pkr�G�N������5G�-.2��=��%�[���~`76D�n1� T�t|� p�@(PEET����	H��$!	G��mt59r�jr�'�|���iu/����#\��Ȣ�t�,Z��^��u�ZO#@!.q��\~ܝvv7�Y�'@š�Q�1�h�b^���Il`ѓ�����i��D�@<���l+Z��&"�]�!٘<@���L� %��Iʡ2[��P� F!���A ��p�M��KFiƧ� �
V$(Z:� �0�J��An�p����^�B�h@�Pe�`u(~ 0�:h=�gh�+��B:�h�Z�F;Ŏ��4�R[�7X�J
P`0�"i��S�lH���q9�
�����:���hq?rR<�p�z�u���a�s�#�#JR2>S\��W<���� � �RX�)	�!����HE��Zt
"@E+������LH��Ǆ9�1S��%X9U�I��G:laє4&S�Id X3+���@F{ L�U1	���' ��hF[Hx~8�0�!��ԵrBC٭A����L/5
>��|�?t��'�L�+?�	Z �l4QD�i��*C�����yUcE$@A�ʈ����^�&��8�.p;�0������Y �RoA�C8x���Q�ÞF$�	n�z����J]�zV��4J�<9J���J�W��xm���El��e<?�M$�M�;��1%A�}c?t�2,A�-V��W�@&S`��l�-��&^��M�a���>�� �c㤳F{k�d&$C�l��=uOF����(� �{�"R�y�a��ݺ��0� ĭl�Sb��w�s�Qw�U~+��VS8��7�Nz� ��]���3c�8�Ư�+�c��:�I<B�=�P�:���%�g[����h�ў�h1��H.���� �T�	���1�@ܺ�r�X1���W���{	2B�id��"Jr�A^%99}Q��p9�W�2��_�J���\� !8@
Z0�4���U��5�q(p�#�l� L+a�@xր���f��	�pF[��j��%�Fm�i6-z�N�a�� &�yY�O�&lE���5E�-l�*��f�}�C����bK�NR'�:���J�;��J�����n�+�-�
*P1���b��:��Pv>ڡ]̢���6($
Sn*�3D���a�+���)��@�K(�7�/ϴ(E_�b���f	�D� ��n[��~�PG:�����G�-q�"�!�%�\�����C��*L!
>�^���O �ga+?�OHN�4�8��'	B� �_0����T'&��0B�A�t5H��0��^�B{aP%�brr�\�p�Y�u��b&A����{e�*�fgv,���@n'I0����Rv�k��"��<�3&#`�x�f�`�@�0	��	�p�Y�Spw l�R8ӆE�
�g
X$^Z� )�K J�� �u� K�0sB}H@(�K��G1�$|Fv#H�1�J�de���7dÇ"������e��*0��44��f6%��$7(ra+��N|��d& R�L������s��?�@BWҰ+1� )�eP8�{��5�bip��'f?�QO�uM�? �!v��*)���莥�{��2-1
5��ʀ����.�k��+�XS�<��p����l#1���@
�Q���A^��j��,'`w�9:�+(<ޖb��I��/��_X� $0
uXI�'@�@'0"�g"[�2�x1�X7�|� �5d2)��I�O9Yar%H0
v�"��G��*Ѥ�FVp -@�l�_�f]pԐ!�+�$� )04vFf� 0�ԋ�%;��b�(d xQBq(��_3s)z񌒒P]ci����^�hyQ����?�B�d�DB:�p�!�0�Nr����}�˰�9XfD_fD�2��=U���_�s�l������yI�)�Q^�!�蕝�a
,�����m/~a�z-0��_�� �_�D�0��!��{
ad��K�|�8|+��"�I)YR�LT�23!�1�@T��}�P0QqV�/e�/@]���lfr��ty�Pq�0�7� ���� [�]c�E�(Й(���l=�lA�l�����C�B\�D\��Cl��u�)��1��w �(����$����Q| ���`+w�ku9�p�u�ٚך�gx�p�v�0	�E#i^�f�敄����`I�0��z��,�w]�IR Va�z�$!�r�2y
I�N�:o�y"pd�6=�ԈEI|HYpLr
�`r��Hy?���2ag�U*'����2<�zp�rP�pAl �"z#a ʠ�$ -0g����l�Z�O0TB=�3���F��`�DJ��P�����5�C{i��tM�1�]� 	;f�r7[�N�$1�{�a
� �+hv�����+NB����B:(�x�"��`	�9t�RqG(f�0�����x� �*R�ڝ�5��z^bX�9������Ia���#!1��P�y�����=y1C64R�N���1�G��e�2,q
�oC�L��
����CH�G��H�H�S�"V�s�]���ۙ�e ��-�*����@C�Y��j15��(�j�����j�E����+AҐ]sC��쐀�� 1��[�?��!�uc���w'��iG18
�v{
;
�0
��p����B_�i�m�k��������z�0:�2�i(f��`�p��{
����{
�i�֖G~��S����<� �c$0�c��c��¢���{��}1='K���H|��2�Y�`I�倉�8Y�@���}���0�x$
Rq
���vxW)�i `4�����&]�����0���$��M�68�P�U'�'[�P���l?2=���#�0�	� {�����	��C'z�\й밙����/6�h���(�Y*؂f,�,B рk��R����3��tJ��3,��`� �$iL��q���������L��}����y�; )V�Dl��z0�_ǡ�\����?&��Q���=��"�X������ W���@*�}��?��ʹ<�Rx�Pj���L���$����Ӡ��O0�幎�q�
��ÕK�GH��9��[���`��>{�v�#W:��d�Ґ	�pҘ�Q�G����xuO7���>�A]�D��<�v]��<-*�:��$a:���!9���5v�+y����v!;�PLH��	�*��^,�˵�·���1l=˳��-!:tJ���I��1�_�D����p.����b�=�W��"��VC9L��d�7���K�!W�}��L��
-3,��_x4�-�l��}�L�+��9"'aN ']��0�*`�0�Sbg�- M�&9]�1�P�����}��2=�igا��#vA�lчr�_��Y�C0e��Pn®x���*0����إ@���U��/ϊ ��1�R���u�99��_nGE����$Td8���޾+���v� U���k]�0%n�1����_ɳ_�Fau}��I�{}��R1��<�0~����"#�$"� "�0fe�I����2(p͠��9��m
ח
��G|`�*��2��_���9	FB �-02
E�B��W��a�M]�4���A�0��8�CC H2�j���p�AB��৚|
�C�)�V0�.#����6A	�P�PA �&X`�5�<���,�<P~du P ������`�=�_�� �aEu:m6f�l�0	�l��2�	��+���vU^�ut*x�b=����p �syE�G �Ká�w�<�u�EN�B�!̠����O^X��V�[��^n�P��ev䑞=̂0Z�^i
<f
+���{������4(A��y��BPr�6ܚ������� ���ǐҘ��C�-B0Q�}Af��Lݡ�!�>\�Y��.���-� ��,�Y�6�&s��r�����p��X N��@V� ?~P`�� �&K�<"'�/�/�>̿���1<���� �����N�����c%WI����ƴ����B��GQR�9��7�c�DG@?RpEpa?���& V��N�2V��14�H��2"�}��Q�j�F8���xdlx�� ��P�2Z�e a� S�N� ���.,��B� �1�(!BNx�x��G=�0�Z2g͠�a	��3j͜-��%S�!1b�� Đc��7�\4N���P�AB��m�P.]���"�C��Di6��6>"D`��m�?�j�7/�.`���� ��� , P@�0Q#GM9iY;�l��W(��Bj�.���ӸC�"I"4i-u�n�-����g�����9���8[Α??����4�˗��#PB"HԨQ��g/��$Z ����~�W;.���K��rT�j�a�c�I�j&l&�Κi��N��s�9��iLA�<$�@1EQ�c��M�F[�хF[@1E�N)'SlIǖ��CH��J+����$� &��g$�CA�P i<6h�[�)��\�fh��,�
!�ޤfs�ZJi��D@�$M�٤3.�H�+.�K��d�(ؘ�'XH��~Xk-� �&�`C�bӫL�!Xs�U� �� &C�����N]�RFM8Ҁ"�J��5�P;  lr�YG�v��'}�N�`��:��=�9���N9.:☳�8��J!����%����D�W��e"��>������=����1��Q��c��F�;�����a��i����hD�D$J���4�JL8�f�1Sj���F�L)G�YL�d��Ȣ�$k��Rʇ��_�Z����x��f�1ۦ��Y&�� +��*����D�k��uL6�ę���d���*�����*M�`"b��S�xB-������R�mUaSk�T@V 
���b�Օ3�:c"�Q�B]�b	X��
�~�U�+ �qԩ��v�y�2���b;�.��[o��CL!��sB�䍦^!H@A_�
�QN�w�%^��IHA
^�0��&5��P�$$a��X9��!��"cd�ɄT"2)H!	KX�F��2u�L� \�0��'g/�
� =�b4*��R����G�XZ�Z �  hf%�kߩ
�5��4P�1��et�]HF��1dc?�[�(j���0�u�����t�A� CB�zB��� 
\XF��� .?xB$3*- !�d����
�ƀ�YV�0� ��1�kLd]�q�	�E�,%��I�1 j��ʀ *�эn
1	H�Y�b_< :��j{I4 �f�>=T���ё� A/�ԣ>�C���g	I������E 	�8�; l�	R�B!�$��|,��26e|�c2��P$�0	��)VV�%t~�AyF@�`/�Y�L`�x�HH�B�%
 	I�!e �[j�#O�� ��� �#�[�@)�fR��28���MBa#�20�r"p|㛝�!�:^�mj# i�6% H@���c(����$#�9��a-z���f5i2QCe�
`�� z{Vf�(`�W�-w9�e%B GRn�I�C���_ cLd���P{Lc �5M�3��	�w4����@	�Z���c(0�	j��~��&,�#�-{�A`�F�h���S���Ń�5��x���B!�=�E4�)���`�� 	.0��nt�.�k���aIH���oAS��--_e&�Q��RU^v%�4	�;?�\!���Bh`[����/n�S��AHcը����	������� C�qnf=���L��S�+\7'�Ij���@�G�cb@��s�� ������5�Qsl&�h�= <VՒ��]V�,��h�#Ԫֵ��ds���2�8p@v��ַ��m'=SR�	���zHa	E=�pٓP�6�	?hns���d��	M�iD����5�h��ݎ��尟����n�Ex��p��'�P�g���G��������5'pGIm1�T���)1�ґ#l�tK���5g
(�(y^Б�����f�������/6k��H���$���&qg\Z[��� @ZAnbi�Y0�LdP�$� � �us]�B�񳁄l`0��3(; R��1��aj���XG�F��%�&x���dV�����0 �!�!�||������ef��0��_*�:��[��t?���y}'(�B��`�'��_n� xέ�L�H�4	_D���������G��b��/H�;h�jql(B;�`��n{��}���w�������:P�,�B#B�f����x?��iF�V�)��-HA�0��E�9���p��? �ъ� ��#;�u��]��X�1r��@��fE@b�LpA�}5&x'``�H��.`���tZ�~��؀
����(�Sj�G3��{�1�� ���I�4P�,�� ����(��(�B�r�Q9�
���bԺ�Q1���$�h8�mɴ<� �a�$���i��Qc�WC��z��k��R�.|& �g���P�H(3�!�㊁�k.&��K�C��J=���@�(�=� ��1�����p���,;y��S8�Uɯ�h�²:����K>~�g8��:��j�.h�&�4H))��1���p��;+�* h!�Ah�c�����K��*���,�	�z�ȿ9�84.��X�@����'�d�*�?��`�?d�p������#R Ĩ(X4�[��:��1�a�,�¹
��f1���c: c���ĀS9�
�%
 �X��~����M�{`�.H5H
�Ȝ͹�+��CTS90 Ǡ$�w����;ȉS�Cu��	:�$(���|�hJ(@) =3�J'(���E̲�� �����,��xH]0?��J@��QH�x��8&!�"��7�`�.`g�6�g��ch�T4/c�e�d���<Ѐ~�!p1�A����X0A�.0���L��*�L�/�?.�F�y��e��K�}��J ��v2�P�?��es� D@$�t���Y�H����0&�G���\�Ȭ�Ѐ��h�$��$��:�N�,灞���
�+F�E������;�-
	y1�z�V�B��+�}e�,�T+C��5<�`'ތ�C@�>�R���T�<�Wk.L.���{�ʼ�&�������6��/�rD����х ����kӃ9>�:y���*�@(Y����I��-�6��U��0j����e�s��`l�Q��� @���;�V�
5���q1$� S~C�
��
�@ �>��H=�DG�{�H� ���Q�����0��*��)LЄ�H�U�
Kà3ӑ�,�	B8�I��B8���o�2��+F�ȴ;�yr�$��k����Iۦz��h��If�5Жl9���'(�$ȨA�0�v�9��&��Х �%��'��Oq׿�;��@� �tCH�����0 ~}�T�������(GD��:�ׅ����)��6A`(�*���،��,��,�� � Ղ�d�c8�cp�]L� 8� �{8����u@�c��U���8+H������8ip�~�!P�vs��= DE�8����� �<�@��ـe@�z����u�N�eMP)��dCh w��ײ�:�i�s0���:r�D�X�0;�����^�  �n��IS���4��OH���JPhEPf]����́�`�x��jH8�r��r}�g0��Q Gx��otǭ�FF}O��)��  � ��#�/o�6��QAH� 1�O¥��	2�Q���Y�)A$8�%���`��X*Ղ*�Ɏ�ə���،�_�ɂH��M��E`� �Q8L��z��.�c�� �!芞x�i�?�i��G��[?�(�rL��4�X���ڭM��E��A@�͇�M:jX��c���i7Gk 苀ᬏ A ���ڀ�h`k�A��d�Q�H�,�:���� ҈����9�<���8�$(�d��&�)k�6�V�e�m�'���r�p-TB�U�t�OI@;f	q�d8�E28�Man�^��qLG0^Hm�"Q��ʲ,; ��8��>��V�s0�����AX����%P�X�݂*���՜���__��-��ؙ|��8`̿p�Sh�U8�	7��
CI��S�!�Лk W�`�M����ۀ��e;Tv�F@\h�Ѓ���j��-��\?��|@:
��y�`1� [�Nh�� 2^F�G6����s8�I(@�9�!��a��S؀؀930&����)��'@0eMPIzV0��14<g-]&��Rm���������o<�h�h@Y�>�MG��D�]H]�#�;'�K�j��"�b��N��I\7�%��L�k;Q S��>A8@6�x�Z>_�`�]~R]���a�Xa�_��7y*�0�ZsA� �3��j�L��	++p���l���/�əD�؝>8�>vZ�@��VT�XԭE�1;���d����
��zx�w�a�M`�� ��J^}nT��j#n�&�'"�B��a"�C8N"H�0�X v@C�J=��z��$�k�Ԅ��nR�fК�i0|��9<����"��s' �o� �c@�0ڻ!���}� ��@�&p����'X��mݶ���N&���@��@���E�&�x�%���h(�������5l��X�-f�݂h�^Axl�h�$Or�?�:����!�H��j 7� 	H 	��p��,HM�.X6�a�*s�y���"���Y�i \`c�h�2ܚ�����63s.K5"}����i��k�Ы��[���s��zM�� �����ݷ}�oN�i���M�&�J�t�=N��yj�Am��k��	�'���vp�U�t��")p)�6}�6}^k���븤��ʯ�BAH���k��'�B��'��s(���݂���\n�]�_av(�����$n��;�h���1kN��?&?� +ĉ�&�E`<��6x��C�!	�#Ŗ���?Ts5bE��n�:�"��#��i�s��Bwn�k#jj�6��)��呇j���wzwST\66�z�iT��d�ȓY�!pO5�z��1T.R���ݣn'oԨ��6��`�!bv�VpL&�	�*C�>h�!JG�5᭽���#��E$��+�vvA��u�}���=��Ƀ�4����`_(��\��\�������q�fh�Lj�� �@� ���_l��F��L`�� ���2��-G ,�xe;�3/G�����)��~{����ŏ"���N���ׂ��vd^�k����H�I�\X�>8�� v�a���;��b�HN&���#E^� ���B����!��z?�<���E1b����E�/R����dG)R�Ha%̐/` ���N�+[0����);�X�bE
�+H� �B$�E�)h��դW�z�\<[�N`�z"�-[�D�$(I$-�<���]A� >d���-l��ۆ0�-^�asx��-Y�l��%rd�?b�H�� h!�N�0  �Ԩ`��@�V�l<� A�TG�q����H��@r	/��(�&C��N�ā ��@�k(;"�fєE�E���t��`<����J������0��b�'aCBD�GA+0W$!�PL�  ��L1��RH1EU�(R�W=1�0�̨Q3Q��A��,��K:V�
	!L#1qL2�t�,�L/�dPH]�DX%DR��TՕ��	��C�'d�y�)��r
[H$!�Ew�5���ŏ3�\C5�@�L3�H*�1� �E���y
�eZ��Bg�ٖ�<��Z ���Z�6�mY�ڄ
�޶[����v[��Y�[��]Ȅ�N8�7t��@�5���hk� WP(��2����y��}\�-W��UT��ҋ+��2 '�`��A�sNH���,�|�mt�8�U���r�C�L��T+��$�M�dB)�$RHG��2M+%Ą1ɰ!�JL���,J��HU�I��I�`�s�D�Vk� �9�%����b�)��D\�t{J!� �"�^|5��3o734�ڝh�����5�@3�]\�eYp&����s��z�k��v����`�n�P l�� ��@�w��2a�T=�ܳ;7�h�]���k�U�g�Sn4����R;L����	/��+��"K.��!vA-�V�s�É+6��+�b���8�$����$U>�� ��� K,S�Gp4*�$/ ��1���Gց�FĄ"Hh$K���TR���-�J�PȔ뜢��V�#�=��/4E[��="�E
IX�F!���oc3�67hD#�QѠ�4�&E)�R�qF�6������AlN�*1�*Y���r��2�&t���H� ':�yNs�w��!�(�R�@d���h$h�N�	co�	�AM���D!�> ���B=Y,���E#� �M�a(����9�9̺�<D �`Ad$#��!��";��z��lIKPV� 5& YN"2|dF.P�	��)t�	&\�	gRA���itY
ATt�u�$$��� t��Ao.4��H�;=}��8�P �% 	�K����M��13��d`4R͠F3�X�D=CӀ[4N:g<Q��4p�-���8ŻPC� |H5�1M��+da�i��L���058��#r��e�bͲ���à
��t���}Ԅ��T��d�1��f� y�9�x�5I�M]�C�����"�IP�:�ק�qv�46�-\�bqC�@�A,���X�E#����XA4r��0&�IG���'��0���f�, �H��"��e\�bJ>љN3EM�Y�S=�	x=ABO ]"4��fDΦ���Èr{Fe�y�fq��=�ܒ�DHAJnS�i��A`7��i�HFp�VC�����
�t�Bq�9v
��UY��|�3 :�~���1� ݠs�c ��� 	HlI���*��ž�w�Y�!	�WD"���9=v�@�S_� ����(�7�#Axi�"��f�,i,��5�t��T����bB�Yg
有�h�Z���\I�0�B]���8�&�Qܰ_#�
u�P��	%A�"h�.�����fLZe�2��L3Rn�{[��1�Jet}�x�79A�	�Hi��8 �&�͂gD�9�̩��5��U:J��'�P޷!���/h@
�խ��Y!H����R)6c)d&�C�@�`G�d���%��,r��Bt���%�%ܠ�D��X�΁���b��n��}��,3[P@�l�⌉3휕
�))�$�����9��0jK��P|�09��	�0�.0`�|�$Q�_QAA�Q�PA��( ��}A�p�I�wU���ĵBh�(�1�h$��M|K�a�i�"��7C�8=l�3�Hh�-Z�6�.�r���GL��3�0"�U�	��� (�hDf���I�Y���c�u�	��9	��͡��S#$�@v}麫�w����	V��
{���uIX�oYB� @=p9�Ny��Ӈ-g�f�2��,!By}�����0��\�U�Y���M#+��+�4���	�0C`0[uV���u�	�[3�siJ; ����	<�U��$�[(���ˢ��퉠�M�@D�)[Z�Ot0A� DG@�����Z٭�|�� Z聘���F�(�η@pXAxFj� ��D�tG0A�x����3,È���HR��H [Aȟ��τD����ݬ5�C	��ّIB����½Gژs�����$,��aW�8	(܊�@���m�XC,#���5�1\�21�u����4�H��S
q��$E Н%� ��:�2�̎��N��	W=��u��9��U������� ����(^�����L�-��p�w��3@�� C2HcF�&4�b!Ϥ�L�� A��� �D|�Fp�K� �� �ctAZ|n��dHt�D��\<��M�L�<�Y�@��8��͒�<�,�@�0��x^n�IKs4����C=P�9D���1v<ݒ��d� ނ0�,<�%�� ������ �%��xpG��4^)���8��A�:��R��0M��b'֙u��
��Ӑ���I,��%�|���+��N���ҙ��Rpe��Y*� A_6�_ڢ)�]�B1�/:ZvU�C$�ހd~�ۈT4��ܨW4��1XZ0Q�Fvĕ�	�!�ǹ�F
��K ܀�,������q�g����O�\�u�1^c��&#m�("0�ى�A2�
�`u��A��x�u��LZ4D�:�d=���ݒM����!`]���H$��*��-P��B�1�P��=��]���4@���)��0D��(C��T>7��X��Ah%���4��o5	Y��X*ImQ�T���Ӹ�H�"'re>��@l�U�GǙ�t��eҹE1

/.�cb�E\ wI&�H��0
4�͒F&d&J�4H�yq�؍B<��!DC9�:� v�>�tA�`l�m��J IJl`t$d`Z�0�m�ߍH���<�,ҟ�ωd�̕���RGHK�x��9��:�C=��9��4��}f�l@��N0��!!4_��#�'!h̒a��6K� \��� �
�!�1����8$�5�X��� �Y��H IQ����D��VG�V �`I��k�"gA��P��I^����Y�_�e1*��~�_.s��1:��\ Q���D4R�3HQ�
,HM)5H�HIC3$F2P�4�� �)xiI��0AhlG|d�p=F��������@GB��)#UVea�x���2rv��"M&�,�&爘Z>^u�N�B�dwN�w�3��,NhV��M�h�!ԡ�=�#@B��͊�c���Ȏ'	 B�����O�	σk���W���	�_ь�p���n�DIE��N� �YR���	�%+�Un���*�2�B)�
�-��֫�.w�J9����M�|T��9C��z9��x�;��9���#EaȜo�)$@�@l�l�mK��F�& L��0��-�"!d��v��v�"6ø8�2��o:0-����J�ٱ�FjwR�<D�IAߑi����P�@(�BH!,B���B$�f�B���2a3D� <^<Γ>�@h�a�,�2�f�-5de���\�LQ E�P�E���Y�� �� ��>�P��	�i%�X�fEA�u�(a�h�
Z_�.抮�L �A�)5�n���M�"7�{أ��v[9����9�'A��?6AU��&Cpt ��ml,����V\�ս`
P�4��)&���`�2,R��BIat�#)�nZZ�L�H� X,�Fj���e��(T� @� ;X<.G��!�#,�#4�-X%�!�d�������)��]gWU!�g�9ˉ���#��5N��Q�pI�L8�:e� �����A�k� �_?�b(&���XLW`���	���.CI�����f1��.����5t�Q0#�&QJ�T�T�L�n{9�)�(��-��gx��`���@@J� �p�i�t�A
x � d@������cAC��q���n�1Ь̪�1��18R1)�1p���#5�V�2�NGYچ<��Ʊ?�r-C�abl�����H� $(�",�#<ª�"��E5&o�*�6���TɌH��1r�g���2�p��e�$	���X����%	����L4+���V �����\��v��V$kuX�1
iCc+���+D5��M��L����4<��3��ٍ1xX?��K�3�Ԭ�,  O��lP#/01,��) Bx@z�A�Y�f�t�&�2��z�W�$C���V�7B�/W���)àB��Z6��в���)�Ѕ��fB
L�����"2� !("�0�j^�!�oZu91�W���_U9��F �@s�
O�n�1��N@��9P� �iaI �*riM�9-M�������>�p1MA���ko�\�h%�=p�@�ntn�6��6�b�pCp�2@d2�4�.�V�4$�v��v*ld�n�R��E�� ��(lw�"3���2Ll���4���f��i%ÿ�T��"��3D&�更�V;��7(s��n����Z�2f�r��#I�շ�VZ�+!�"�^;B0��1�-2A�$� �(ȯ�ݬ��{?A�Gu�����p�y��Y"Ei�pH�p=q6(n�=A�@^@���9����ٖ�;�N��u���eRln�9C!fA��4D#fs����?Q�l��M�k�����W)[�20!���g�Nl��Fǲ1��x� h�
n���J��a�N;#12�T�0�cJ
#�oe�� $/����6�")l�+�fzT��w�)'ع�K!�ru�P5�� ܁�/"P�S$�=�/$@�ߩ��oF�p��o�($�T��B���d�E
S�NL�e/k�V]��\����-����;WD�}R|�k�p�p:��c��7n?<�+4���E�]sW�S�]�Ja�Fo'��H����~3H�1x�J����
���#~�������rt� ��w/�#�)�n�xr6y��%7K���]��Tee=�r��6�vN��ڮ4D���4@Ds&MZ��͎�e Bw Y�t��%��$Y�D� &S�L9��رf"����1�.[Ni��eBe�nb�f�e���G�E�-R$%����)`�x!5��KW=ڂkQ�E�j�Z��
�b��"+ҫ��%�D]�M�w�עD�$Yx	�E�$V�xp�Ɗ/N"�.	!��HI2��Р��r9��2e̔-;�Zu3j���=�&-�ի���f;Z�S��0�ڂ	!)L�!$&΋|l�$B	bD��"���Q>N��d3c�O:��~�x���)��;Z4��f�r5�����Qؠ���o���h��f3h�)�٭6��f�.�Ď;@�dH ��\,���E:�'D�L�Y��e2A�6�0f�9�$c�CH��pe�ANR��������b�*�Vh�(�
�,���-��j-���48�����������,�Z �-��ZaL=����!^���$3��%KB��e��� �R�L�K�H;J�%G�b�hd+�i���]m�<�iTmf
�*g��`P!*&|(�(ě��A����b�A�K!��`"
"�#��M�/U�SV�����A��7��t�>��3o
cx;I���3f �VŰ �Y0��P�'���;�@�E01cB�ń�.�Hș�R��}A&ҥ�n2�����%q2	ʠ��*.��d��9�B�L?�B����d�+6�s�`-���*��|�����1�"Bl$�l�A2�t�HC�1$3�Nc��P�R&f�fFH�E��UۦY�4����&'i:�7AF�d�A�f�9�H�d���6�H��(P_�Y����&��s&vi��i�"����A���wc�X�x㍯�����ikfL���������Y!�;A;���D �8�E� 䐗�`�'�NZ��B&i���S�=c�i_c\w����hn��S��'��)+O�RW�䗩<�M$ ���;��,t��Ͼ�':ii.c
!	��-��/dq̤�Ƙ"�-1�b��&%�F3R�� ��}�f�hFk3����F1���f7ł̓$c��ID���)�v�2����(c����m̳�f�$��M����4�0���Є.|�#P�����,(H�w?�B"� �&�.u������{���2����
����B����! 1�߁L
 �jZ��#!#�J��;�l�[��j$�	[IJ����KLq[��N�E��6�`�Iis)!
Ȗ�P�t)
:���J1Pp�=���Se㡣�{֐��r�=�D~8�6Θ���1�h���\��)�K
B�I�Ps�.�YD�Q���Q(C�|�Om���*i"A�'��m�3���� ;   GIF89a��  ""%('$"/&(%%)*1+5&(755)%4+186&863;D*9HF:9F2FIVm3NM1Vg4etII#I,E+$J+0G7'G:3U,#T&2U8'V<3i%h5j7)I9PG;ax?@OHGF*GI5LQ-JS7VF*VH5VT,WW6Zb9lCfE*fI5eY,eY6uH)vK4vV,vW6pe7EJDCIXGVGFYVYJCWHYWXDV[UIXeUfOQmogJDgKVfXEjYTxKCwYEtYUr[iolLoyl.l�Sv�T~�g|�k~�^�RZ�}u�Xs�v[��Y��q��r��|��w��x�Ǐ8�6.�9�;+�U5�j9�S4�k:�YE�[c�hE�iU�uF�yV�gF�jT�wF�wV�xi�XE�rN�yf�83�U5�i8�\/�q6�ZE�oN�xf�uN�|e�z���<��;��T��n��|��U��f��v��f��w��g��u��h��w��W��q�;̎V˗pѡ[Φw�P�p�Y�u�����������������������������������������������ɩ�Ǘ�������Ӝ���Ե����Θ�Н�ɩ�ɬ�ɳ�˴�ժ�֬�׳�׵�ƫ�Ư�Ⱥ�Ǻ�ګ�֬�غ�ֻ�蛆㞡鰒㬥䮳亦軳����ľ����Ú�Ǯ�Ě�Ʃ�ʶ�ӫ�Ӻ�Ũ�ȸ�Ѫ�ջ��������������������������������������������������������������������������������������               !�   � !�MBPW��������ؾ������������ٷ�ľ����٬������ٸ�����٬٫���������������������ڥ�ګڬ�����WEE�E�ۤ�DDDDDD>���������������������������������ܘ���������������t����nnnn222�2�222� ����+�+�+++�       �� ��  ������������������� ����� ,    � � !��k�^{�����^Ȫ�X-b���]܄��G�+v��m[:|�R�K�2�=}�̥���6hۖ��vm;}��L�2e�k�jAc�l�S�M�:E�t��dLu�ܶ��t�ײ,=z���S;�:p��}#�-ڷ`���0_�|���/à�����o����F�r�hтe�L�/U�9����7���}S͚rks���fG;߽{���̭/���N����ȓ7ūy��)䅌�%���麴����.�i�1"2mϴ��F�����|Ƴ�ɢ��1KVKYVf� (�0�����r���`�3KmÔM�SXc���n����=ꄘ�9�s�M�1�3233�s�9d�#J���4�HÌ4�$�2
��.�Тd-J6餒���ȔT:�H#�8�H#�@
$�t�$���H��(�#�@b
(��b�-��b'0ԤL.��*��'��|Sh6߬cN9�Cۣ��3����s�o�f��r��$��r�.ە�ݩ��J�x�u��&�ԕ'�HT=��6��<�T(�|r%9�W�9�3��2h���$x`2�4�SLI��90V�a���!>9~[a���t���@�5ڞ�n���m �%#��=�L3�xd�1cp2�Tb-��p%	'�0-�Tbq�g��.g��%g2�[r%'I%+?r	"�ČH%�@�Ȗ7_�H"<�!��|s"��ܦ)nv�(l��&ӌ�b�ԧ�yʞ��EM^��EM��Vꩼ��K�h�J�%����u���vx���S��C��K�x�>��s-�A��8�w�9_3L1�(d�����($����5a�m�Gn�b��N�'r{��:�����\H#���۫M�,ek2�lL���0-�W����d���@l12!W\1�Ӈ\��!�l2%)�|	�����Ŕ|��?������#rI�,�s�]>"��;{�"v�D ����֦�S9�d�թ:��p�#�"RG�D�k@�=d�J�rx\�]�x�UV�"	��XV1�"�g�Y�2��bTN*�-�ʶ�:������龥�����N�"!�����
�@��|�%>�pW���ftČ��g]�`�3Ȩ���io���Pv��L��C%�`�SY�;�Ѕ-���<b/}}D$�7>�l�}<�3f��ݏ~z��\���l.{�ۮS����lfێw8>�)�c�BR2����&.��;�B	=E�:t�54t��$�Y�~X�],�@�C�2�q�>�)��с�-�&8�C���BNtq�<���R�.,�q3��P�t��s]�MyF3����/��;�h�豱d+����Ab2��d���3T�3��mADjҒ��d-�F걓�#�D:ӗ��~�_��W	���T��Ny�2�Ylek�Qy
��-�n��.z�̰���(�����x>Ś�tJ9R"�r��,C᪓�� b�"������uӇQY�|d�N�1�-!�:i"�qԇ$B��Mڕn��W��	�.�+wA�������%,IIj��F��6���s#-���e���E#�`B��|'k�q�&r�׉  Gf�K�e�;�L7�Z��~��%@�R�-2�|�O��2J�lf3{����>�.����"���W&�nl[���dXcP�<�A�ëN��:�kX�4�Q>�A�&-�rM2�:al6(�U�rBAѰ�˕�X���[�s�aY��؊���M��-^��$�
�.��g��A^v�TQZT��^��8HA2����Ck�Ј9&��Uj%� R�"���7J��VB�'+��� �K�9}c��I�ȑ=b��e(O������ߵX)K�2�Nwd$�D®k1�.�T�/T因^Lu�ڰ:�!��9��S��^�p�(<6E�l���H&��� ��PA�|
��(�����/�pd�䎝����ݮ۪��#}Xē	�1����l���zdv^�Q�;ТkXBMK=���Գ�Di;�B�|/����KdZ�h%�Pe��ˣ2�'7��5B5�xtE��>��r޳#�\A�L��[���#�S��'W$��*���m��.h1�J�)�x�<|�����Pu2|(�N��n�q�CX�F/�[,�b�)M��8KT�r�8w���mr�+�)��<��v[�"�ƀP��+BCА4���e/`>�qy��0u����NhK��x^N�^���E� �C#V������h�G��=n�A�eLӼ�K$"��D�r/jR��y��e%x��r���ͦ;�?0�'�D��]�2��o�IU�N��Ny�;8��yp���hz3L�k>���MoJ�tbT��?���_u]��)*�P����r�7,�;� �NgW!��aR�w� ��,��;��P-��"��b�B0)dF��u�0�aw@x�Zl�2�2*�`�U�w`� ��Q	q 2v�6hs�j ���j�_	�TfQU|�f(sH�I�`?ez�Gt�p�T�"\�ŅsV	%�73?� 	��|ʷ�A�'\�|p��'s�'s7WsfM�e�`�g��k���VB�CT�V� ELW�P��t'dT� ִ��G�6u�g��C�H1 �l�r:2���O%"H!�U!E�"̀�P	���� �_��K!��ЀK�;710)�/�r�$w 1)�0u�`v2�`@*�Hw�wP�� �U*p`qPq�f`spo%@fP��f y��KsU�G�Hw�T�Wf,�{��p�QRTg#�1�%A�1���(Sr$	�wri��������r�rfSs� Pu�������T���V�ȉ��`T5j�%j�s3w`%�vBŠ$*�%E0�^�+ҐO���]WT4D�@LQ��a�� g	�0�9�c�`$d�<f�p�De�<D֍ƣ0'xn�1~0y3g��m3p� eLw�oW�*��j`M`Hh��Qq�VR���-g6�"i�D		�e%tv��gDZn�< �@A��eF��	2�06��|ʧ|)'���e����� *z��	"s	��	��G�aC�b�Xj�ԓoUi����xj�`ޤC��`nޤ���  �<Q�'B��:��E:���@D^��Pc�@�p�PmW�0R	���@FȐ	zi0��� ���`�P<� $��hЖ��FVx��m9��QQ��උ��6�	%�Lp����+Y	�gQ�����Jnc*ÚPJf��%WRg����J:8KY:*:h*$AN*K`��i|�i�'�ɦ�ɜss('*��6�6���f~?i��yM����H��Yj�hj�IjEWt��Hx0�a�p_Mw��HQ� �PND;Zy�2�JN bX�x� W�)$'D�@"��w�j�P0*��`0ǰ���	��m�h`�n��Yc fcHr��0H3C*I���6s�� �cЊ��v �p6Db6DsK���6��y����Pe��G{Ȯ�:*��rbڤKڤ�����H�Ir�%\�7�0u�r,w��D!�J/� Hi��'���1�ؔ�F_��������f�HU��V�9)w@	�V�Lqj�%*"�3����°-̠��p.�2���`X�p�yk�HN14�0Q��pxP�}`�h�}��}�~�	=�����0�aQ��6IboC[�G	v �%��foH�eC�yeD�e�d������-�*~��K�	����J���d��Ȥ���ر�����)�kT�0��@�n����s�r)7�(}s�Hy0W�	�V-[_陼:yMw�V�(��J�� `EgMF�V�v�jpk=	W����� �3��@�0���-�{��>�����0Q$�#��V��� �DO�R	J1c0�FH���~�aQ�A�Й⺯p���n�H�Z	6Gԩ�����qeS���!*ա�1���6h�|F��KܑG�DZ;��LZ6ܡę��Y�z�����'�W�|t�|���^|���S��)��-[ �۲�!yz�w���i}���a�O���ƾT�uˈ�� �&��=��%��$��-�P-����"R��P��8���^�jЦ�F�ʁ����kϐ0C�ZQ	i�~0dU[ʳc��0*H2U�a}�ܸ1\*�Qn�2���9��⁸hä��A��%A���C�jS�����;�Z*�f�Ī�@c���P��В���sz�X����	s�$Ʒ��k}i<�ɐƖ֮g���[}��t���x< Ci��p� ��NW�ۄ_�P�*Ҫ�$bM0�;0r����9�`PD ���Y�����nIA��c�a+������ sP���<Hb��Q������HB�ա/h�`
�vpf����i���g3��ђL
�u�_C�pr�*�>Ll��!Ù��I*���K�ͥ���q��W��a�'7�
����ɱ������ْHb���S�����-��ꉕa���x\jFyZi������W��"��BT��J����Z2fMJ�_�LMwB����u��|���|����c�]~0=!�A�Փ���h�(��l,�� �vp֎����Q���@�G;0��|��q��<�� 	S���ƙs�A	�r6����ݲ!s������-Lᮋ�KZ�$y��T��*~��i��������^��,��]}�ܸ-��9yM���]swZ�&��㞈�`ejW{#+`K�Lx%�H�`�d�g����� EA%}w"ݐ�'���U�8��8}_9m�j�i���`�dH����������TF�n��ǈpL�^��F?�l����^	;�<�+�n��{r`�Dsr�Pi��Kیм̘n͇��3���6S͆-�����\����y���|+�H�|�k����D���б��>�yڮ�Vi�	aP^����va�y��H��y��d���E+/\�"CT!��Ľl\�ka^�t۽�x�%xۆj�a�;�S��2�>���e��dW���Ȑ%�����C#�N�t���q�*���j�{��ͱ^_0
񹴠�؜-�w�̪���J6O_eci��.s
4�H� �@Й�Vl��.���k�Ⱦ�6���^_�&��:�kn}�]}lj�j�>`�9~���@�kY��A'橺4&DP��-D�rE�r�@�x�9q���a=!x0�0<q[1,:����2�'H`���PoSEH�'�HXP�y1`�
1��-
pВt��.�m���s � 
f�7��	Q���%_b���������R����6|�]��~��/�q�jä�t	�����|r�,��.�L����������ƿ|�!��Q� ѫ�.d�2���^Ą-#��Db�"#f�����Ҩq�5�$G�$���6�+��S�r�9��d�d�l�5�ט��4��e�t!�X�V%mE�={��X%;ȐU�d��?uh-CF�k�;aw١e���;��9�ZIY5wza�#���^p��q���^�5L�/^��܁T�V/^������fd�z��[��@��z�l��K���j���K�u���x��Ƨy���;�g��=M�4�ַuٺ}�k[��î���d]�lݚl��m���ޭ��^b|�$8�A�����k�Bd�
�o!�"��e��(##
�=�$چ(�2��'kHzP'��Y&���E�v�D�*�e�;���s�٪�^�8
�:R,��h1�g�Z
��AK��AqZЪ$���H;+�3�
�]԰�(�X�^l��G�O�]*y䒽jx�2,+��e���^*I=�+h=�݋���\m�j6aM�K��MԖS��2����M��&=η�N��9^ �D����^n�e�[��%���4�8�8�!MCuOL��=Ƞ��辽�[�XJ9"f�AЩ�@jϖJl��#�Fl�ۊ��=Gny뎜hq����E!%i_PR��t9p�i��K�;��i���QJG��*�d��*5ĢŚJ꠪L(����	�몹��e�9*�%P�YS.8�kPmـ��G��9���RLG�C.uf�3^�A4��ګ7�VC�A��1d.����(;�4��V�`�:�SO5-69��U�V�d�Y���bu�e��«�ٳ��uҳA�o���=��|�i���B�"���[Y�]�#Eb‗�[Т��;@	
�q`Ei�G�Z�����ɗ�q���f��P��I,i��%t��*ɩ���F��5BƎ;�Q�.��:J;$�X�Я�+J)h�!�?��*6A�\����HD��aimi���5_�'7��%Q�KL���y��,��=�ql+_�!�i�&XéB��P<gU��jT�[�i ���	��@b��/9�����4�>��O�B M�"KЌ�b�"+ڏ"��GЁ��C1�'1c+���h!bL�%$"I�hq<��!��]`��J�!J��HDB�:�*Ȱ�Ş�J��sMq�h�';T#,���_�]��T�%�bn��P�
�n&��'L�	Ӳ����f=����
�� ([|�#�ѦŰ���Z��ԙ�蒃�J����͇͘J3�q�-S\��i�i̓�X�=��'8m�/LTb<�b$Yb�b�H�WbdZ�1t�3�8~�$���Ѣw��/n�f Ox���VhQэ�d+>ɗJ�G�[O���8�	]�ǡT)HU�"$�āIYQR^��Ȳh��X�TB��P羲��EH�5@�J�����OV{�Yβ��\���� 6d:��"1�2�Q��������i��d������_Yp3�/�*���yllx�Q�Rm��乬�An�|��e}�Y��!��'đ����P�# A��!���.��^�"F� #(���[�Fe(��"z�n�
�.��~�����XfV	�`�+�U V�f�QR���Cƒ@�X>�J��@�]5v��\�?a�'+;�5a��2^]2p�Ձ3�h�{�ʚJp�^g�5�|-p�Ee<XƔm��N��c��&7w�_}X)K�g-_������Ŝ��}�q��i[�D8�'eJQ/\����r�o���w�+FY��^\c�P����D�+�X
1�#�zugH��x���j�9�S�x�Qg��yH��%.���!C�L�&��+����RPkO����^Ln�|���Z������4]�ZU~p��tV'���RF��M�P����;�Ye-��Xàuϋ��,%z�>��;2�8��p��-��7Z�LIt�DFv�k��Ǜ�Ku���^	s
��M�����#+
x��:�(H��{="^ZZe��ޫyi���`���Eo�8E�A��?�τq�h���V��%��W+8T�=��=��7���eX{@g�⛶��-�ٷY�b2��U�gU�\�Ǻ�!1���c��G���O����@~2{�i�����C��C뇉P,ʹ�(���D%BaFL|��E���	DL"��������~�2���,N���g��9� IxV�Vp���d:W`i���y��Sk\��]k��	�x!���X�?+�+�N-���)l.s�ۼ�4��_EÝ���}�9F�Ns���E.�Ӟ�	$�=w�yn���}�B���>�������h1���V�
"�lGr!]�.Y��|��i�qL����ئns9�JhG`����!{�}���3���;�WJ�ڏiª��1�=^;�\Z��لd���C ����4%Z*ڧ~ʥ��مS�9��=^H�pI>��W��ܛ���="D�p:�3B"t��s�^1���9a��k�4:ȡ'固.�'/����bX1��� 	
q�������+
�p"������D�6[�(��;�>B��0�����l8�JD��1bi4^˼g!�`i��Z�����
�� 2 ��,�0��r� 2:�0�Л�˛W:���=�=�A諹���(���E":�s�+�`�9$:`�g�F��x���>/�F�c-b�F�H���H�h;��r
�"C2��[�0\�FH�^ �1P�F`H�������5ЧV!$�dDA�!�����!�L�Pa�Xq+���؄P�IE�ʫR31��&��3�o����=�C>�K>䫹������&�9���s�hF�� cFiTB.tɩaL:0��b �C�"֢Ja�q��!��p�I?��̒:l<8��G�Pȶ�����z��]��껱TE��,� ��Q��������ã�`a���dj�P�^0 �T�ȉ&��Pt�ET��@�W��[����A��A�=]3F�ૹ��b,F]KB���3Ji�m\���F.�I�[�,d����k�F���3���kh;���(�$������DxG`G 1~��ɏJ�-^CEs�!BE�8�X�^�&|�p�OUd���0�:�^`ڠ֐,���l���+\Z/�%�0�����0�o��AZ�IM�W1M��I�k��Н�@��=�l�i��i4���­Z�m&�����N�˿�(�Ӿ�1�,GP�G�5��;�K5��K@��p��)u�/|�^رx
�̺'��%���/���\���q��J�O �5s���J� T��r�Sk�eƠ������=�����PC�M��:Ԥ9�ì���PT�p��^���Tb��?�zQk���,Pݪ���S�'���:�+*�J������HZ�:�G��D���	�#8�DA���Vlx�'����7Ki�I HH�D(�D��9��f;6`�&؁3���@����S��*|��τ|1�S���S Mۓ9�q���Aܳ�"�B`��i��O��Rb$:�c�i��jhF_��.�B�d"֚�R��k|�3��i�Cx�P%��;��k��iH������D�a%d��V�/C�I���O�j �U�h�V��K@/y�D@�p��&`�P����h 0p��e ( 0   �5 Hh�&�â�;��1Xj�7m"�R1V�����^X�[؂5F�+؝4X��ɛ-����M�E�>h<�ܴX�������e<Y<���'�LC��ʒ�� �E��	�jX���Q�9ZZA��Y����P�Y^K"�i(ZC۳�H9�1�����
��Vp�6�(����x��� �[����-�X�p 8���
� �_
x��� ��U `��U�VӀٺ!Պ��k!��%0�h�%;E�o�E���I�d��U�҄\"�ƞ�\J�I5Mu���VBO�g]ѩC6l|�QC�Ŝ�����w�]ۭ4kb�Eb#VbE�Eq��Vr�i��<�V�P��Ô�J�� ; 88�3����� �Ѐpc�5���9>��%� �H��=�;��@� @ @�x  	� 	��� 
� )���_	��P 	X��|�Y�HeQ�V��X�,��<�,��d���ټA���K�=���Tm܃���-�bT��,T�X�L�Y��$<B՜��*���S���TUo|�k>b�x�!�	#~��c���p扣�Y#���b�q^�E<��l�L�@��Z;�~6WtE�0߃���ch߆f�� �c=  ����=>  0�@6�8�8d�dD�����xd
�dJ� 	XdLn 	h�E�P��G`H ��V���l�e̪�*��+��������Ϭ��e��d&B].��[\�E�I:$d�O6�5�k�Fֺv�F�L��FqN�p�l�k%�k�����vn^�Psj�ة`0F�9��)c6����(�( �� �5h�vc����0��c>>   Ն[ p�Ն�;^���c����c:v튮h=v��h=N�?����ߒ�_H���_)��I����L�i�f�U�Ph���@u��1!M@�hY�s�sP��Y��b)8S���\f0��`�����@���;X�KJ�$��e6Ԃ�9�����I�Wb���7����L���V;�Z;�9�5P80�&ȁ(�/��l�&�����l� �l���Mh��`�0 (_8������nm�~m�6 ���:f�@&�x�,� �h.7n+�r��_ ENnK��� Kn䕖�7��	� LN 	p �6��Q��nO޹�h[��;�+B��Ϊ�j��V������V�`ֈ鰹��]V���t�^���\]KJ^M�k��9��\$���T�l��W8�6(c3؁]_�ʦ��ζ�a� ǀc/vȀ� X `�`v��v!7�c������q!�[:� �c�Հ��"��q�]��v[֞�ٞc:�~[��c�r׆r�(�c�����(wm��w���4�_5��9��� 8��
�w�n�h F(��9\`H�]��RE|��Ug��w��ȌL%
�[�W\��K��J��3� ����ř��O�\Hʋ���̹]`���t����o`h��-�r�u�q�c�5h�e_8 ��������e� � �� _fG���u/��w��c��������~c�%�w�c����[ �w��w|��?��@��ɏr����(����h�i9��� �~�	��
�x��_�V�([0� ��ܻ�`j��~%uy�Y�c%G�8�Ry�-�S��ü6���3E8�eܚ�y�0F�M���#z]��Zy����fu��9��ME���h ��֖���m�v�H[�w ��   P` ��`��P�<l�BC��5l0��@Ǆ
!���C�/2n��bɗ"�t��J�Q�4 ��>�
 0Ԁ �,m`��R�PT@�� �"�����	� a
&��0�-��Х A�
	*V4h��`[�l���%H�z�܋`b��۵�2jĬQ��l�ϽD[{,yr�Ҏs~�kZ�j�!G&f�$D�U�d�V.S����8r��W^8�[�q�yaྠ��%�qw�މ��u�wt��s����X�[К�@I0�ۗ�$ �ZUB�Q\B*L����A�Dp���(���
.��Jv��L�d J&��"��PQG�G��P^-�ߏ}%�@Uh� B%�T}���	TY�L�@Ye�5��5�\dVP�
-���O� cc�v�$��F�d��F�gĀ�����i2~��̡�Qڡ��	5��Xi��K%� ��#�\ba��
#�`��u�]Gp�E�a�aw�wq��x�����˩תd��g G� S�g��П=�%�S]=���z���!X�R=��R	/�t�Ũ�F�h�
�`bJ'q��͂��D��.%T����O�d_��U�]ee�TS���Eu@UUU��D�@Z��ZkMpWC� �E2�K`�j�#����ixRvMg���B�9Z��q�����gi�Lc�l�9F�n��f����B*$��}0���+w��
�`ҙ����z�6.s�a����.���d�� PT5� �0��:��BԮ��^RJ�@�b��I\�/�(��.t�aF㪠¸(��7�K"�4���M�9�b�R��S�\+�0�ؗNUk�d�\1�q�C&���Hq\�Q�:% T ��c��rT �F���^	Yu���%�,�H����sm����-Q��іf�9mi�"�(���F2Ӏ����S�M8� (vF3��;U�x�B�좄�!�u���9m�9O�B�6� ��PD�b�� E�V���x� r��
Vg �@/���t���%(y�T��x`\`\�
���������h`���9�`�!�r�s��Pҹ�]+x�ʣ�� ��)AZʐ|R$�eI��R�����%d�C�[TV  �ejR��TP�E@��_+%a���3~�h.#�]m3�$ 58L^p3���,��=AJ8��`pLA*F�:�����s+��-=�ɕ�dx��0:lSrv�ʹ���E"Xd �%)���9LquQT�"�Eȭ�C�g��!Ε�^{�Ib��!�nu0���:ڥ 0��G��^!��Q\��	r���IkRE=o{S��tP�͢ز2��!Uk���{�p�X�|'����g�" /*�A �9ԉ����|����� 1�jV_�����5���IQ�M�)��AH�^5c�����2쮤S����0���IX�*nљar���B�@ �����g
4���'ŋA����Eh�')nS����jAz�:�b�Y��
��ʨ�&*�n!⑾DȎ*!�K<Av�q�J�"P+	)rbd�ןk�3�w�'�|��%e���w&�Y��E���y�<T�0�
�<-�36f\�6Lbvf��XԄ��VfPh���.�4��^e�"����+�[s`������a���[��8q�qzX�6���=�$\������h2Q P��`�e�:�	�Z�T�Z�v�\��g�V ��a�B�M.�X �^Tv0i�u�2GQFu�?���0���\%4_ȍ�U���<�%/�
cJ�hz�3U�M���#�z��Ӌ��[�	R��0�"���2�q���5&�	��f�51:L^+
P�pa�
�k�1	-+��onSl�f����W�|�ې'`M��=����p��PM��Rʪ�o?�L���A�Vǹ*W��Q����5a�-�h��@�Y%�%dĎ�2�j��(&�ʥ�n�j��� �7�|�e�%`}��\��Di���B}
�6�i��w}�>B�@��{�+0Ű|5�hf�����p}�l\c���z���u�f��.{6�n�`n�����ưsM��s�(.�)D�N�,6U��Y6}��l�{�1�Sc����<69;�a/��ط����A쳽�����-`+�@�[^����U�T�a���0Z�3� ��_���p�Y�-���l�妫���b^6)p��7x�V#� 
և©�8��wZԺ�0װ"9OG0sbÂP�u�蜶 ~�2�
|�x1���}�e��������m]�u]ک]����y��5dC�6�Z��Z��5Xa��u���B�ݐ9��[��X-^a����c8��E^�a��0�;��{��{PC.\�g!�R �OI ����EC��A��@��@�@A�@�ȼ���tܼ��D��De�D���[��Rr�ޚ��JHH� �u��al��@�yV�U]dAL��E9��5���E_H��T�(�Sh���m�����}�Z�UU
��^@Q*A���X��� v+�� 
`�! -v��yݭ-`-J`ZE܁�0U�װGޜJv�J�^����y��NF��	�Dt@�q�6��qT�$�gK����	X���^�eU�����m�8�ĵ!r���D������]\�@����D�����z^<AtEz�t�E^�E�'P��I�`�Ā��� f���S��]�#�_�̛��ߜ)b�4}q�L��_��I*����t .��`�]�,�,�&`�����-6��	Mڽ���� G^QGai[��r��{����sx#�I�cD��ec���[��f���`8"�z�œa�U�Aƣ�aru���DN�bUCV\��`�FЀ]�U��\F���5$�M�� \T�Q$Ff�xd$��lΦD�����
���Uŋ��E�~�����^����p���E�X@O�	T���@E'u�$<�0�"S�bUf� R%Tr]�'Tb%V��-�ש�]��l�)H�"��u�.LCaLC2^
��n�Z��y�F6��f��{�F^��yT^����eh����Kh�N	�E�r� lY<�h˥�^����<��XI�fC��D����D)���ɗa��!��֡E�f�A�n���'�fmz���I�t^=^�݅���ME�P����<"�UNsJ'!�V~'�d�v��@A���Z��)�b�a`ع�S**|��.�"V�'|ƕ{6�-���ݚ������ul�YRކn��l�1(m�eth.h�^B^屪d�*e��zc��B|��AtO̽��(V���A�+D���i�p���Y�� �zTIaT�Ş��H��י��ĭf��Dv��Jim~�m�&|�^��z)�y�D����D���e#�RE���	�R!])�E�ivAH,�A�����^�Ū�' ����e�z�'���F�Ne|j�5�$0��6��%�V�q�`^�*�2�dPlx臺�%�{�*�-h���t(��2N�)Aq���qk� ���qM���U1��I�iV�&�u�l�JV `��kv�`�m��m~B�ڦGA��&i���rFR�=Qx��u�#�XIXɀL�p������N����rڸ��C��I@���lȺ�y�,z�"ʆסg���^�,�����0�*��`��fh���"/1\��*�6�^���r�ðao�R5ć@��0YEH�$���A���ea��L�G���c��0d��#f-�%F��^v������~��km��%�>�Ʋ���Yt�����9@��Ex}O���
�x"��������W|$�� �c>�<Z_���)D���®C�,:*�g��'�,-��^_A[�9h��>F�*.�k���ګv��-Ѣ.ذG||��� <��Q��kHa��o��G���yk��(�Rkq����(����<\i~m��f���$p�*�z�|�l�{�����f*�W�F �\,̼�<Y�f�E������)�r�����yLT=jAB�1������1s3q��_6tC�E�2�l�F`{��&`7C�����`�2o��I�AF���Jm��i���������<-_jF"�@�E �1�}Y ��ߝ�@���bvk�Z�{�r�N$I��-r�϶�a
<�
�l�pm�m�N)��!H��Sp^����fF�9�����d�\�p��DQ�4��g������-�������Z��!sVg��j�3��_s�5��,.q5K�{j�7���T��=�� :���ҳXQ螀1 ����>�3?�l�ao�q>�Ww/kE 0�
�Skj�&CO�2r�� �H�W.W�I� �\Չ�D�#َ��m�&��+�ʦ�n��2�2l�u�Ԧfv܅���tR�RU�)��{_<����
8�t���D�Tg_j�!�wVB!s!*�27sؕ��v�Y�uT�w|K�Y�Tʷ}�n>ǥ��;Q脞d8�iv��*�U��s>6��W-5�10������oTl�A��q�����,�i6���#q%W�~^9��h���Ț���rki&��-�.𹶫�桴x�^t�
 &ߦa����P|�M���}矢їN�i�Ea���n���T@:�2�Ga��{�dx��x��1��533�ޮ{&��y7�ݞ{3}Ϸ{���gC9x3:��T~ddc�r���s`Sa?��~h=�Ƥ+�����7v�'�d^���ŵvG�qV��y��f�!�hAD�@A@��˙���^�i�@���6E��g$��6�}'�V �f���d���p�2^ �u)������n?lF�λH�� d����$�9!^z�Ƶ�6ꡧݡ�9�3��;|����Þ����/����.8a���m�ڇ.X�C��}3��5cG݅��hmS6r]���D���������c��+{�rB��nk�f�E���{l���F)nn|$��E�;gԚ�O�A���B��K�d:�s.QF���6��)~���AP�N�������0���!�w!���!&3['�}:��w7���g�4'��g��[�4�~��图7c<�C���5h�2��ﳇ)�<�<�K8��<??Fԕ5��<Gxgm�AX�l��E���Ya�6�_T:�t�z�t�.��wAH�J�)�d�e� $O�$y�?�nϰ^ _�<b�s�lAt,�?ۿ@�
�j�#Pb����HHP�.R�p��)[�x�R�K�.S��Xȑ�@�P���fLZĀr��B=��%�i��]˶�)�l�eCuj��W�b�U*U�[��+��زW�v+'��ӥN�f�֋X.�v��K�1�~��ۗ�`j��.,�qbǆ�^�֫5�H�֌Xsa��� P @i^�p��1b���v�g'�mD-]|o1��d-Z8��Ȋ�a��CFu��\��L��_>?�RB��UDl�b�
��WH82̫�]`uy�����WaVY�#���	P���è�(�0��BX����V��*���"4�	�``�B(�~����0"o�ʑ�r�����:k���"kH���-��Yk�w�j�-���+��b�kK�0k#21=ے3��ұ�*ˆ�o��L�b��ӳ2C#���X�=ThA6�ps�ŜT�� (��ͷ�+n8I� bت��_�-6d`!��~��ᎀ�����SȽ�V�U�(�b0�{���ۯ?�������
$x��.�{E��}E�j_�ia	�),��
� ���0���z2$�Ab|񧢌�ƭ�����R����j,��,�`��Z���߷z�2b��,�����03�L���|�L3����������x����7;�Θ�JD����jM-��텠��A�Q� �7�~;N9I%�͠!.�!#�3B6�@�aku��b�	�E6>	"p� XZ���[!5��B8/��/X`�v���lY��W��6k�V�dy%���.@� V{��r#dp�]�"���F'�({ydJ�|Y����d����r�(ł��(˒R-��|8��z&���l1��L��!3����,�b0#fy�l�y��h>��
8���U7��j�c��F�kzR��>���h��@AT��M_��P5�Jk�6��-Pq#BL��A$~�P����� h@QL6҅OPKX��V���JKWI�ۚUAat1:���CC%����R����a��QX`�݅�,li��rg�'��I\��Ɍ��+}�zez^�14|C�-C=2Nce�`���'��;;  � �V�*��.�DB!
�i��紨ak�i�r�6��:^#!�^%��J��q���!ԪV,h[4��|�ahED���m����n��!�E0c�-�0�a 	F^������
!	Ch�]���!L�Tx#�9J�Z���laR,��('eQ��C�<�(����-c���@6�/5��c
>�-�3�C��7ǎ���[��R�h�� c|ГiT�ʢ�6���&i�~�R������Ȇ��nC�@:�r�^г�$�9b���&�]i��|.1�*n��V����SC�[����e����.P��Jn=8�K��l�D/2݋x�,���(�5x�:*J�I/��Z�Ŗ�-��̝��	�v��eM"lXx�+Ydг�C#�FЎ��
-D%�1��M1��>j�B�E�A��'���iơd%m�)�fU^�N�KJT�l*�a�% ��m]F��48!��?�яT[aUiYժaXL�����Y���eU�&C� TB]ҭ+2H�I�Cx�P@pZ�Bh
�����պ�Z�+��R7�a�wlX�Er,���sx�$�lYx�<`Fz1s�eZ�S�����xP��8fr�Y�43�>�� X��d
���E�9H���6jQ�¨�V�!��e��� 
<�d�r��d�g%���iݤ����
?q�� �Z��{��k-�n�%����^�2N�0��j��RhAK�{9���tE���t��2��e��d��ز ��R�I,b�S�Y����T�Z�e-����^�<���02�Lf
Q͌c�p<����&Ͱ���;0�  �sLz���ষ����`Х����p��:�Ӊmx�A/x�E�uy)���.qN�+�2@x�OU��7с&��3�<S�Ѱx/�t�!��D�"��hR�	�d@�dU� wy::<0��y��r��{M:�;m}k�ֺ���K\�[=鳎���Y� 4�hz�R�Mm�E��n��JKlk��2�E#k%�k���C6@  �ȝ⁸qs��$���;q�P��q�0��}oO��6F�B':�O|!
?���oZ�V׺&-5���;���(�8��V���BG��X��т��x��0P� ��~`�Xw
Ẉ�Z`sN��V���W��
.�Sѳ��w����d��������@�>06P1�O�Z��ulc����f0�zF�_��n/l�,Q��Fj���~r� 9���!�6>-7�����,;x l~ph����F��HЩ�k�V!��L�Vh�f��h/�OΎ���Z�J[*�[xo
���`#�� �V~���e<�)�h�d�n��n ����K�`mtM鞎��ށ����p��0��c��o�֮e�.3�����ڪ-�J�"ʠ�P�����`+� i:I�X�7�#�x�70�ʢ��F'n��:�i�@�ˠ�B��p�T���DT$9��F��h)Q����
�*���+�ZOZ̫\~@���P��1�\D�|�D0Bz�l�#MHD�*'hb����26��E���̌�"�����nݑ�A�p���V�/��y
�R����்Z�����n��(�����c��  ��f�"G�Q��.1
061�:rjk����`�B�;� $OB�����d�&�K�>���k��s1�*�qV�r`�&���D�@D:"DF�)��A�ʄ$�z��l�e�E�� ���~��2Un.in �t�,����n���qKl�Q��P�r�2��"�Ff3�����Q�0�O1Y�w���P�\Ǝ�aJ ��}N� 71S�
l��$P8���}<�l l�6�-���� %'O9h�>A/�$/�`�&eq?A��8]�cѬJ 4��,�C�Q�$�({�Y$�L�\��Y��!�V����jb�X咄i8!`�C�#�6o.
���Q/�r/��?�R��ϰ/�S�r�'#c�����h ��&4��� !���0� ʇ �6l 4s�Ttx`[�3u.�@%6ƍ���r�˸jZRus�8�8a�� <DQ$F�
/
��&WAb�8Yqxq��˾2�Ƌq��1����U�ۆV�t*q�D����(M(���%���"gT�<a��
� 
��'�0
0��o/���5@��Q����A+��bA��p�fZ�� w,23J�J.xdu���bu�T��4  �.A/GU6�j��Pn�ܞOq�VoT����û������D���@��;J�#�r��&O��j�?��9��tq?�@�Z��V!%X�jrBd�=��Lq�l��<�"| &X���",�(-�� d�`5d
\DT�'l
� ��<!ʨ����!?��.�h.���bs�?�NQ��Ґd�h���R�P�2#3�!f*.�U��fJU�H.�USUG��⯸�)�V1`O� Z��"�Z�:l��ާ�DM	J�:��i�&q�<�:�l�T���7q����L�=o��5'�����^A؋�EC d`{Q��@
�����m�ṃZ #�S_)E0	����c�V8Wf���\A�	
�@P�IT���b�r3v��װ?IV/��vÐ��<�v�s�$f�p3r$��
��H��n�6fc���VX����As�g����<jV��R�O	��h/GST�}s
� ���6�--�&��$8"�lU�$��7�v&U�Om��@lg�pV�����4D\��\��\c!. ]V�"�+sD#2E 7#f�"�2%����(Dr:�scaVPR�$��Fg�j�D�Z�bi�/�Rm����8@!U��2�U��R�'|jz�wy�7��wU��f�az�B.�a�gh��)�7_$���|̇"�k��� �طa�6��MEgTkE#��ThH�6��ã$�
����S}oN[c��ց��lq��Z��(��,�� ����\"˥\L�"P(M��!�&��vrZXZ��MoHP�Da$�D��	,*��1/S6����w��u��d�p5x�f\֍����Wy��z����G�"*���"��X)ԉu�	���h_k� �V�N4�8�y}���6�j;G-E�Yw�$J���}�ط�;��4��_�� d�����K����",-s�B �"�í�>�Y`�#� ��g���g�s}��V�S��y�h8�%���u�A�i�����kW��X~W���:,��w��#+�x=C��yg�~�z���B��-��Hh�+hGG���u K���Lci[$��89.��u}=���=�����y��!�7����jȲ�[�P��9�8UA��h��Dz����uT4%����!n[+y+�%\��tڅv��eAlY~z�?A�ڨS��'�ɘ�ڪ���ۙ�[����z�.Ûd����!�9|�aU�9��Ž�ʭ�����a��-rD{�zю�aH |g�ܠ��@(��.����,�Sj�~�"ϻ>I]r�Y$>{$�L�x7��b�궩�Xa��9_�ouq�^�g
XbT��ʣUp�#�\ť'�:�rZ��`#:���۸��L�$!�ṱ\��������ɜ��Y��������������e�!��|��hzG�hߛU���V��蜄v��U[�4@"�ౣ� �����(|#�&Ӎ�$�8�#<��H�Z���\$��$��S�PT4�������8q9_\!�B`��
�`}11BN�u^-­�
�XB)=�#� ���4[f����s�{�[��=�$Ap��QA���(�!��̯��������pc�rc�Έ�sŀ!ћr�34��Ku�ӺyKuy}��֋������ܤz�f`��#-e��9��9��I1��G/� �$/9jsl3�H%�\�U]����l��7nc#����������}��*$�'g� ����z���L��`D�%��ڙ\���Y���|��s�;��ڸ��]x9��՝����ߝ���=�QU��A����o��1����f�YG�|y�B)����a�����5_u�X|�n�9A6�� 9���������ab�Y1�8���#�PHG}ԡ%�Thq鏋�
��e4Fqc<
dQZ�g��b�8]���~��a<�٫[h�%�������
����B�C��F��������]�}�`������)Ie�B��<x���c�"Dy�U��Qb�w��q��ȏ�DFL���Ȏ =r$FLj3iR���浝ԮQ��3�СD��,wݵnٔv�6�ӧR�BEǴ\6�C�r�,S���h�@�   b�xc(�N��p�©C]qҢ�/_-�8쩋�.��pjܥ�bN����'ō7{��%r�*Pb��ᖇ�0Xˀ��#�>���)�'�V�t��;��O�����*V,޲��Z�ʔ�Q:�b%k����b5���;,Xa�|����o�}�,q�g��/N�nO�(�!�h5�<�PFQ��BFa��D�H"u����N:� LH+�N3��N;��PAeTP!�NSX5%T�Mus�QL��W-ET7�� ���F��e�_Q&H_�9�'�u�%e�q��_��U�����f�}���EaDv�p6��iP@���|��*����+�%���+�������\��5W\aXA�t�)��w���v���z��
Kf��Zj����~��3�*��BP�T`=%� �&���9��.yTG"e�L5�p�NH����)�L;�G1�["5��TnN,�"�0��K9V6?ڋ�T�ֻ�Diբ�ZQs�p � �jP�U]x�EGP�%�'�U�Xby-f�bZD!a_p��fn��2�G(�-�Z�)��B�7� �P�n�	�o���u�)G�+ٝ���SX�]x�����vY�wjf�z��9-K~��7�}h�N~]�WH��R�A	1�=yG��E���l��-<餃�9!�SV� �	&�t�mI�"�*"����T�M�ʄ��)��O=��PP=��F5;�b	u�P_�k��P}��4l���Bj�u��Q���U�%aփ��b�IF�����Eh����bU��P�Q3;����J�p����Ь��*�h��,�8��ߤ4/��9�"������U���`!6�m����d1��X�-?���'�$����;��7	�M!�G����o�q��
26�Mh���z�Q�r��[�H��s�'�:OR��.,\���f���_�S����~�@�C	��&!Oy��]�譯fP(�!<�C�E��y[�`3�EF|^�L��=H�iNF���� �~0p���G��lPD+�n|�Ei�8�iZo@� p���g����gU�I��ғA�p'V�gEc�%�8�!$Q�B�@~���A��%�
�5:b�hmN@�2���r|�t� '����u$"J|���U�r�ӊ��"��rF-��v��JIrD��uuP�H$i`-rD��bF���%z�RM
��GÈ�zyɋ�3�a��'}$��I9��N���j`�S���7؁(	C(C	�h�H��$%�G9|���JZN�> *��*�O?������T��<Č��Ap�"&L����B��n	���,ѐ��[�9��ot��؄&"�	dl�$9,;=@$��S$��
K���+��b�Pד�Xc]]�b��2�/��FLY�ɘF���Y(:ǎj
#K�nUs-���~��_�R0����L�d��̜����#����V�
��nyp(�8��D�SQ*�B3�n�E�/i�BӚ�)T�G�^z��`+X���
�}9��g<���܎8�3���'?��B!�*e!XQ�E�qb���X!VD�U�md"� l5
[� �s%�/� ' �[JD�����\�,�=��,�,2��w2}�G�}ݼ�0�%1^�(Gk]T���@�3�wֳ?5�b��nƃ�f@��'�܅C�	L�K�"��,$ڥ!�dv!�ݘu��k�NmP�༘��8�
�)�P��/-�{�W���\�~�0��`-�
n-�8@@�_����h*��s4]V��#l�{f�(��r��7(rb�8����\���8r� �$ȍ�ja���b(� �(#��x�I�a)��S��=���ђ�^V��6��#�N堬��T��� ���(s��1�r��H�Ё��
���̃薸�i�� S��J:f܍����E�,�.�4w�ɚ��C�NC)���&ԟPT+>�
ݠڽ,N�W��#� �~�T��[�:��BՆ�*�
4b[E����ǘ�I��0��h�X�:ЁT�m[d��.7�Q�L�{��:�]o��"
 xh+�"�ٸ�;h2�{�γ3!-��#*OY��z�TX�e�q(���52^q2c��s(��Ԥhr��=��NnS�4L5�d�C��ld���v[���c1���b��?����u�4}^P��d/�n��X��8H�� 'U��@�?��v��v�c'+�	��*�*�A@��l$d+ �`M~$M�4x�'x�@x �bxex�ڠ��x%�C�
O`&���y�2!��y��!�-��5q �.��@�:��EP5/��:aT��;�bqe�zF2r�{L��]��'"��]�W3�p%��|�\}�dQ@Q�]U ��h;''�&'GPu�~��>�]�cQ�G��^Be(��?��	�7)�5�U�f'�`�u�D�`�t6�j7l��Af�rga�BBt'� R(�� ��h��x��maC��y�s�@I` tP�rY�݀�,9�O7��"eYd{�Ue\�O��/�5q�F-�R�y�����`  �$.�eH?bh1dh�5#%j}6'�a1ox1�V������hX�hS�s��S�'y��X�]@4���$�����EP[�<bwk+ `��n7-�?�0�(a���ɖ6"da _P%~tr��"��D9�w!	2O�8�� 
N�&�B�	�Uo�d����L	�7E�d�s�p�����ER����#`�Sf@j��f3B���g �0#Y<0��5�f�g6�'�؏r�"�H2��RX 
y��[�X0[p(�|�5^���9���^����ш^ �9�^0?03�b�e���uPP(����!K��6ڡ�;Iwj�5�HtS7B�EY�h�#� ���p
��S9��5���Հ�,Q:�P"�`�:q�Ж@ZHQfA�E#�s9#�����.vy{��{:qe6��%��)��|6%�z���'|ďm8}�'��GP��w$Ꙣ�/3�	�
4@t�G��I�6`J�^�a�S'�WGlSpC ��a�- k1��,ْ�R@1	��
��6��rgw��l�tB?���B�`��Ey�� �wx�T0�I ����y��y[�%a��2�Spf��Ԁ��@��j��*�jy���qh��D�8���e�׀��{�� <q�F  k� y�#g=c�BJ�}��1��t��ih/S�Z@�uH��vR/s��A?�y�	J�h���3
��Jā�HE��@�ևUR3�`���ln����Av7����ZsLe#BaJ7��B�`����Z���m{�q�Li��@O@���Od��0�����CK��J�,�P�>�"��"8���:>�qld�7a��j���q=��R(�x���J[�q���JpP���$0 �3���3bX�BgS@|�<�Ն�9M3r&�3]�2Z�]Z;I�9v�9�S��:P3e��6�1�P7)^���U�@^Pt��t#�1�S��]M�
���q6�����T�]�5��Ɣ'T7�0� h����ֹ�1ԋܔ��:O��Ԁ
Y Y 
԰��i�<�Lf"�)���z��b��� v���U��*��:0A���h�������5[f��$0�6���'�6��7^�u%�Z��9s2M��
&�J2 �=�u�Q�1�09#�:C|����D�tG���z@�T��r��t6`�g�~��0���R
6��X��89*��{���9�ȹ�������CI�M�d�G��@
�`I��
��,��c�,���{��� g v��P�0����Đ�/�"IĠ利ы�7˩�e��〨g\�F�����[bt5eS�e^��<qᴽ5��i-uRWK���\�A'P k��a�C�������Z����JL�r�� 
��
<s�; �>eoP(XA�j���*��<YB`
RtC�,di�'��m���7	�D&�,����ۺ��yF���xC�7�.0U�"����p�>1��f@�ppv��v���𢼏*��z�^�P>q�J�G����4�k�������[�����'�\��[�{�?���:]/s�-5�s(��s��J#�~��x��PP��q(��ԚɈ
���`I<�v:pf@ʤm0�ȑK`�*|���l�!�����	o5bP������˷h�݆��
�xj��;O��@!
�!FpӐ���xBdC�ׅ:�h�� f`�v �i`i �p����c��4q
�p�:��I�������iƳ� ` �hQyRg�\<�~���͕HN+��'g#:2U�봕��s�]G`��b'��qҬA^��8�]��	 |T����
� 
4}�@i<��:��l l���kYA���؁��R)�XB��W�0R� 8����i��{cW`��`�0�*v���7�
���WpI�p��8f�7t�z���.j��� 	v��m=}؉m�˟�ש�`�ld��`���b�թ����ڲ�;�Y M��=s���>�^<���0P Sbh �ǳm����i>�i�m����ȷ��Aҫ�>D��9��CE��B�0��	3��L9M՝�m�r s���0�Da�<��@�jE*��TB�����	tQ��	�Xm�g����m(�7A" �0Ӱ��p���F��@��!��C���7t)B��ت����Fw�נv�$N�gp�9��;�ϻ��`��'{���٢J�IƔz�  �{����]3�{�b8}�|=��V��[�W�]�L����vRk$�~DW���*����<)K#����Ɍ��`�i0�@iv@i���	���Y��5ɶ��M�q�o W7���X}��x���6�74�מ�֦^IFp�b?A8K���eX����zZ񩨊��y=��@�#nj w �xɩ���__�����`ޜDld�a,����d�p@��f�H|�ڪ�P��f��<#c��T>�ُ���q��I`^k�C�
���Ƙ~��lU�q�r� ݠ�� ���u~�g��p��B�k��U���Wz��Υ%���B����}4�-�n�0��ږ�yC��P
�@
�p��
�px��pc@�	�PJn<�J�|�����S���p`���η����H�١����� �Ϋ�l��e�-۵r��Ŧ t����E62b<��L��;,v$y��((�h��2
�1_j�Y�SK�]t��R��#FbĀqèѡEa@�C�H�uؠ�铪W�bY����X���
��U�Q� 9�3�N!DgΤ9CH."9�r����T,��bŒ���ر�d-�X��X�e�#,xp�Y��e�R�P�7n�(��/��{�T�f����������:��ڱ�����7m�彋���M�4iڄ�����g��V�6BjٿY���5�޻_S��r׈ݲ��3p�4���`6���/��?�qлƚ^n����o����/��08��T�������v�j�3̐��<:Q����(Z���c��k�	'-x�ɧ*�����!�x��(���P�3X�	
P�Re+��b�V��,�8�#�9���-�҈��A⊋/9� ����WĒ����tl2@)�l��"�A+&�V3M��Z����b��5�d�n7M9�㸩&�嘫F�f��7N����/���3�?�;��D��5>[ƹ�Tp�cL��\�A����UVZ�ra���^��"��8�IK
q�sQ���7T��_����q'O|�"�*^�hHy Q`X<Æ��78�j�XTY�K� #�,�΢c�����+��QSM;��9�&��
1pX�Ʊ��g2��1�B���<[t4j�Դ�l��5J+m:�آ����Ѧd��:�j���ү5]G�l*�� ��N;jƹջr�K)Y�� �?���cѣ�b����W��2(`�h Ŏ���r�,�HEtF�$7<��-
��-n�&x��Ŝ8�I�|y���(��h��@,���O4�ʭ��,��deO��hC�=����(dD�L��:{�,�[��>eG��A5�e�>)]ј��t�{�1j�e�����m$G��AF5��ܤ�_�T.$�KQ?i�Nn%A><������9��PB��Õ����	��I+gxD` l�l�\S.���u�A���F�:ԥd_/�CLb�:C������>��|�DvQ��6ԡ��`D��Gt0<Uhe+]�X,V��,�x	��CD �r ��&D4"���C��ű|}�����������
qI��h��=��N���I��Ԡ���}rU��Zs�a�n���P�=�
Ё�젆!�35Z�Gm��x*��ō�x�{��[�� �AG9�q�k��HG:���̈́04�e-hr8�,�$����
�Hw���%/y��� �q�G���(Ł��%;�B<7� ,��(�Ҙ<6��{�Q�a�B�OĤ	(rCM�B�ԏ�@D!���
,^��S3�،g��d�p�K
��G=�5H�oj��)�g��}�6��6�������G:�!�'��Kpz)�u��k��7W+�`��QV6��D��v�E��MlV��,VӁo�Ә�4gc�	�`j��,"�ɓs�,c�,E�HQ�t8�K`��p����!
�	,zB�/҉�pq-��P	�C�ǆ7�+WK��F7~eO{�D��D7��ylS%��9�b0�<�žB>���f���̻]��c���"<j�#��e������Ҕ��<��լ귛�F(�V��0m�!�,P����NB�f8��-q��%�h"�Y!�,��1�w���t�߬YN�:�l�@: 9\�lر\�PD��1a$
"�HZ�ҁ��5mAY{��~B���-k����݁�*GO��xW�����F�&�`Y����9�1ΈH!q	I�����.�<�1r�给cfaCmE��UD�T��|���M��jJK5XU�����a�oXb��	� U`�6�����<�� v*(7\�'��-�#�:�����K������a������F/�P  Ј�v��˾�]�|,�u��]�M��#��a������	NP�W|2On��3��q��c+l�g��*��
8:��jV3�b��7d&n��	!�K�9�{�Ld(�2��7Ѓ>_�l�>p|#3��w�K�H�f5�Bj���4����4�ҡ�O#`=�N��S�����P��20i	�ki�7cIk<נ�ꐆ�X�ִ&�Amr+^6��g{3�j	�����?&����)����D�r�.�#��7i��R�dz�V�UE�1eN@��
S� ެ0�|�a�F9���M�ɥ��N-*�l��t	q	\�B��$�	���cf�/�[����|�X/�A!&�B�i;�~΁~T���D(T�_�"bzB�Z����ƪTŎo�"������W���Y{�ӵ�莄 ���FP�j�k�31m���b6f�&�1�j���{6i!�k�,Ȑ0����=�����-��Kڿ(b�-y3�v��{���|�-��N��(�1�3���,'����8��#V�B���`P�S8S��B���8���^ Ip�D�����S>F��4�"�~{����>�����9�k�KNh��3�D�?S;0A��_�s %y�^:8��۾��0��
��A�宅�0�4�;!3�c,�0,�('�R��Ei��i���	�1ш�(X���8�.�{��a��Q<���3T-yK	C��'B|3��#�O B+��,��Bh�����1Ј"�{�*<�ˀ��U��3<�\��xDhJ�j�S��B �o�$ʘ�h��𲙚�C瓾X�X0��ㄋ$-(��s�����9@���u�U�z��S�`p�t�*J��u��R��j����I�ȏ��q����k��Dp&_�k��c���B�r�@nz1���Z�6��EA�Z H���1А7:��Ӂ�`LF��a�{3�0�r�(��E�A��<QBO�<��'�-����f�'�p{�l8����(�̼ʎ\`�B�<��HI�$FX�z�hp#���s>�l����c��"��??�H�T�N��?�$܈�|��J�Ě<�,0�,hoПI�9]3![�\ ]8J]�;ثg"�lE���B m�&o��m�@l6i����  ���ʋ":X7��HF��L��7����\��A���!B}#B���md-~�8��+G�7`Bܻ�y�W�G�x#yD�I���Z��\8@j����#^(�̞�d7��y�C9�H:���h8=��(2-�H�|րR�T��R���)�|�V��+Ȃ/͂C���>��+2'j0HЅe������:��]��k�S�h��?��rJϫ�[��L��Ak+(���p�\�K2��Kr�K���r4P��K�dм�<��PO3|÷zL*+��{�7%9�����qAl|�zܮ+�ٛ��I.V�L[Ȏ`b����2�D��h@H7�B��)!���V�<�X� �C��)���R)�H@T��|���RC���R_Z�1�3�5�3'``C@���k�g24�+3��k�8��ʹ��@���J�ئn�JA}Os(T�6shF= �Y� ����)��꽻��M�Ӄ�%|�d�ɻ�|�Q��	u�S�<U Q�-{��m��Z]<�k�h����B4+y�M��w�c� l��B]��`*�5T������>��C{���Y�~���M�H�T��As=���C@D�,�$��>�`�i�- j�5�����ڻS %C^(�K0�g��?�ZX�^(��*U�X��쪍�ׅ�gCOTh3b��2 �4 (�T�SY���^���9��h�Y�Z�y3�儡�Yْ�}U(Oh�ŷ+"B�����#��Z� L#eM�Z}G�c.P`���e}�^�S��Pxɒ�6�5��mm۷���"p�[�e���9��[�5W�����\�]��]퀵����R��P���@i�78�Dh�H9�E���^�[���&pJ6՝��X���O�#�]���i{�\��F5����K�TY�<�И�c���(�6��A��� �AX��bւP�|U���<���R�T��V �.ݼoB|�k|1Dd�ll_7:ȃ�=Hq�)U����UP8�	H[����E`� ~�+9�8�V���>�4j��[?�5_!��yNr�@|�ߜAl�I������������:�jڦD�6oR�H6�I�<�I�Hx�D��VĪ	∽�nB,#�X�Ի{]c��Ah�xT(pP+^NeY�*�"�}��6���G������Q8�Q�c�U�Q�^I �Vp�i��C�����|c���l�!L�k�kEv#Wp4֘�CX�Yj��ih�u�I0F``x_�F[pP��$�9�����*����W�/��$H;*���sU�+��Z~�A�eHf���G�|��I�����1j QXki^2����F0��g�զ!Nb��@�c�rX$��m�k�܅�P 	� #����D��|�g0v�r�ǻd^h<F�c�sc�-�Rhh�i�f��icW�2�-�S�&�L�1��|��U�N��Dv�Ԍ>�RXd�����[h���I	l��C@cC�핉R~��sM"]`��hVfeP�ח��A��[ľ��s����Ԑ/�u�PBᥑ/�pj�F�rH1$�@��H�6� 5(p5��S5 ��m��
�;<,dg
lD�ZL��-�xT	�����EH��*q���Ⱦ$D���������K
㖒Ϧۧ���Y�R8�H���N�i8U*���KUO0_z����P�o��*$�ox��rH�"yT�QC���X��s2�i0��f�SH�H��� j"��*�*���FO	�`%;(�H\FWN�j�,�������t�W���������[��k�;@50�M?X;�UK]v�Xۦ���w('
/v�:��tn��80�� �?�.q���gI8�B`q_W�1.2.�C�[�DRP`���^Ӗ�r���q�Z��-�VRk\f��*I�4�r.��pp�t�C��&cY�oX�z�o��l�j�G5>���U`j:�r��y�Q �<��X&*@?(=�E�s���)΍��|���^���d~��g,l�g:�:P�:@��d8,	lO� �V��3ult`X3�g�rBg�Mbb����p/�6�<꽖���<���voq^��6�GDhrqo�g��TP�i��j�����s��r�ͫbC�����S���(���tw��op}�SX������lȆ23��R}ɀ�i��97��~�o�F=Oh��m�\�J���������C_�rU����e��<�kʆ^H��OE5@�3�t5����@�ec�w ��6u�c~
z����L�O�u� �p	 �D@�m��H�z�l�W���Ws��YUx����RT��o����p��Z�P�I��QA*dH�!Q�Bu
%� EU��}��8�3�����)UԾ��V.[6l.�e�f��5l���e[�CQ�@��-�Fp�ԉ�'n)=pK�����ۼ��Fa�s���D2
5}��M{V-ڵ�{v.�{v�o�=x{��,7��r�ޱ�W;s|��-W���r���|8�a����S,����z�QSG5f̜Ac�L�n��|�;��౻�noÍ�!On.��t̝��g���!$(���H�B��Od�B��K��^�yO�.�~~w�>O��`�R�$��^}��E}YR�'}� E� �PX���	�AU�J4�h�Tu��y��B���\sM6��4M5ո4Ӌ��Ē�
(���JFF�TRL�3O�O�CU8NEEV��V2T߸ŖZ[�3<��Η�QV1��
1�0RN>Ԁ��Y� 
e@��1��<�S�"XV�Y���=�-6����:���e�^�K#p��������v�fK6��[cұ�j��gN��F��s��:�k�UPAP��y�-2�|� �� �!�'`1bH|�}'���H��)��WP*� ����PwaE�@q�(]T�Y�?rJ�|�H.��b1�P#5�`3���X36�p�L٠�#��x/��(%��L"	U8K}��)��'�*;fu
�Vbɥ>2�e<t�F�;z)v�_j���)���[���:�ԕ�̹(�g�r�=g��/�d�4a�U
Z:�dF�ِ�����v/�@҈s�av�A1��5<|�����J+��-�s��\s�Y'��w���"Ȯ	z>qm��"��!�3��H�����~�|�z�y��a)T�����+u��(
+oQ4^�QFQuD�4"mnJ.�L#�����L/��L��n�W��Q�8E��C&rU�T5��ߨ��AX}�ж_�B,Z(K[�T����~�^|Q��PC��:��@x�b0���=@q���{��- Ab�㠃�0��tU��L�4c�Ð�4:��6��d�˸FhR��Ĭ*:��Uc\%ĉ�V�s1v5���r��ܰ� ��!�0�V���A����ǉP�?�H�E��+,2'�?~�BJ^�6���H}�F.D�^����4�׽�`R&5�4L 5J	�R��'��-�~���+J�-���� cMq��Ĭ�[�͒�o(����A1����^�ᵳlp�� �\�QZ��{r����B�E1c��WDz&�4�a�H36�-4Q4"pp#+%J�N���#��E:�Y�lx%��@>��\� ��H44��(I\L�P�O~�P���"�)P�	�e�;z�VXl'�x�w��HU�Gz���"G��ߝ�R2�)l�6\L�`ܣÚ�0�R�؜�w/R�oHB
��g?q�C�(IAb����|�҂�d�K.�ָ�_xM)
0�Ajb�.~�����j6��@�"n!�|��.���h�qy�����f+�Y�(4~=�|�31L4�ᒓ���Jq逜V ���r��m�J�ͅQ[��,�Uх|Ţ_��A���my�(��r��$d��IE�b��G�D��d���Df$�`�9����LI-XM�Z�jp�a�^.�HPh����H�ե�#��`GW���p̣��w�(�o���B[��ֶ��Ї��t�b(2���=ơ��"���@�KUs:�Y��Q�I���Gb�h��0��:�YDt��b۬�D�2��M$��Z�.���\ ���n(D���f�����Z6��T�'u�CF�
�4����4 �;Z[����A�f����'�w��`,/��Ո*<Ѯ�I	�0_%xa�,��(X5��I����(m��9�(�Y8��y ��5.\���y[�0�-�B}�� ��]�"���t�S]�q���E�|��`��j6�2�9r��*T��P.'<���T�%�W6�:\u���{˫��?�X��-`A��@�G��t���Hƈ
Q\\+���Oz�g��/�KE*(\1ILc�x��Zn�ԍ�X�`DE �<��Q"���z�ގ���Б�b
HT�("�&�M�*���d.PA�1.S�4�R��$���YUl�z�u���븵LCpX�Ҟ/=L�i���z��\����-|K>z���F�Jv7�hhC��г�)�_�]O�̣/w�=���'��V�A�l\�	�	Uě%g��e��@>\���"�'y*<	�A ��(V�q-�ŜZ��EL���?z���M_"��i)}�+�).A�~�`S�}�7�\�~[��HF���R)�	n��w���p��Zn��б�Z�]����F�����]5) ^�XSb�߄Mh�5܂ҡ�$�J�B.��-��-�-H1�C���)PC��V:��� �jr,�k����ٚ��� �$\�4�՟}��qWsITV���A�\��^Ť\����,Z�D���"$��Q��OEH�!a����GlD�1V�{$؂	^�RI�������d����B*C����IS�R���7�C�[���;���C=̃+�0⤥0�]�C/�)�[޹�:`����ͅ<�✀Xo�b$Y]�F��5�A/,�2�H1�ac]C.؂5��Jl�-�F��`p��"��-Q�a#p0�t��l� ��@�N�!������p�n~�0�^)�\.�.H����z!y�$���L�V����a��hD�m�`��������L��%�#0��I|�7�_�$	��_��;�C8�5�"-fb&��+p0���T�}Z��ݝ"^ԅQf�Y�Eb�]�<�:��ਛ<P�����F9\0t����^0�p`��0
<�IBBe(���(�9LJ�8�5��0�(����P�8�ICt�㜄Y;̃6��t�s�6��5ԁ��
����o5^�!����f��=����N*L)�$*�B�y�ʽY����,�#@�9"K�D��0d����H��I�D��{���AH���HGN]�0Sqd��(��IbR,���:�݃�5xC;ȃ;�g>�$5�D(ނ*�~�"X�b���݉;��5�A9 �����ߠ	4�[�@��]҈F6܂��C:���􅠼�o��c���)�ǣ4�s�F6L�9\(���^�q(�*�8��5Tc��;䂌�B����Շ�Ə2�-Pc��c�s�ֿ��k\&f��lN��N$�f!&�n�r��Q!�>�B+�clZ�ZDq:^�#�Ў$�E���dO��<d���G\'l\^�r='#Eg�M�K�R҉��I��8��̃9@�bf�L&�yz�7�C��C%�$��`(�BN�*�g��e�Y�^<$�_�j9�BS�Yԃ9<����C�\�E�pێ��]�> �-ȃ�Z��D�f1�4��;-�����6�p�$�(<PC��';��:̃��F.|�oHF�؇�Wc,���r��q�j�@�@���KJ�)�������w�$A�'���?aC�&Fa��)yW��ˆ|C0��P<d�J*�=!�.�O��� ̣N'%�ת���mg�N���*EE9�]8dç�g;�CӶ�:T�7�5���=��|"��P�j΃ט�^ҕ���7<�Q*�-���^�#���х���_�Ҙ�j`��t<�8-gH�)�e��G��k�n��J.�?��bPC6���F���+p� �hc�?!uX�
,�A��%|f5H.�#=�>�O~��V-w�g��5�%�ܜ��"���,�QOo"Y��ʦ�Bދ�h�Ig,P�'HI�=��!a(���-꣮Z B��p�IV��x����)��4�z��;P�+�O�b��j�����0���!��>��)�5�d�\Jg��7�;|M>�(�*-�a��!\õ���N���.�V�ֶ�P�&5��No�p�C���K�jN��C����	���rPF}ch����M�E)؀<�%�� o������#^l�1��NZm+da�5�����|�BT$@�$T��J��nFD"&�q���H�<��,}�2�4� G&�~1j r�zG�8�Â��p����/LC*�#5���Tbz��{2�4��(�E��g �R���*:�݃"�-�[ J\�-�:���]F��C�\�a(5�B] ��ɶ���F��$�L6��J4�`3��)0�Kf�)l�~���EI2���J�J������1Q?,Xf���@!<�Rqâ����A�c�Z�j�����D��u��V�B�#��rH��\qV/��]Fp"ۭ���r�K���}�~�lub��#��:p���%K�|2�4�f8��4�{�����4��!P�+��[��/@F1�=�C0lk
;��!���C_<�Dh�aD���E_<�;AJZ�5p|Fc�d�ƶ���fC"� ph�
v I�ʥre5�5��eG��k���YBA��NO��A45PX֚��X�i3�4��4��:����ĵ1�`4� H.|45`�"bF�i�vD��QId]8�#Q�K��ף:���Rq$��l<�9tDT�J������g{"0P�����75êN5��!�ATO�{��
�Z�,��}��\S�\��V� ;��q���d��FG9����b^���+E�c;cd,j�Ā�@B!ʩ��l�|�5�&�飉���^lC�x���BkOm��)Զ�).�����_�`�y���f�)`��@�)�'y*D�ԥZ����>�|��>bF�3u��LB�p�&o�4A'5����֝7Ij�'>�:�7����݃>�w*��*fI�L=���C���u4�.p$����p��Sb�ť�
WO8�[��G6�w����ƀxG�o�'��Ao�C3_�NCi�g՚�NR!�xʭ����IBmo՚�n�d�����+@�(rK8_!�7L]/XC�DK$. ��.�tr�9�&n!s�P�|���zzs�I��S�7;��= ���'��I�+�'�;�`�= _��~��UJ��u��7f=�h%Fq^Y��ao�6v����gc:P�l��� B�g�ܙ�x(��P��h��{.4yC4w�x�&셋-�Vz B���'""""��ߑ�T��"�m�-8� ��$����[��m��4u�'��$#U-*��4��7𤖗2��*�BX�5=�>�C �'��.�����] +���]�Ec�E��;��=aȓ9��l��5.��P~�W��W�C�+q�/���Ǉ��C�9,�>��!T��.�Fտ���"[���n�=I��|�&�Ot�ϙ���m��qI�vF����t�!!�1�)H=���-<'u��G���_�M�tNy<K�I2$�9w���C1�w_���o9���y�2�~-���}*�Zz,��Y ��|���#�\:j����
�|�������r����Wo^���F��Jx�X�S�#;x�d��9ӜK��l�t��<s���Q�I�6��t�S�O{��Fզ9s���qC�$]�tI�i�����6U۶�R�u���4U�����i��Md�$�e������H�sQsE-V0j�8����fΣI��-�cI�M�2u��)ֶV㢆[�i������ok�
c�,�\ip�G�[��\9j�Q���5�Ѵ�.Z�������cȜ�yٳ�gp������o_��D�𨙻w�P�I��[�3�j�+�������#�#t�q)%/��B�<�i��fډDm���dR
)��*�)���E��
3r�!�ƺ�K�r���Z�%��rq�ȼ.��/j��+�����Ŵ���TC��,�&�X8-����9vBS�,S�:Ŗ\�L�N[К�ܦ�7o�YG�ln�f7j�	�96�QG��yN�u����4�T3`��4��ً�`�F1��u�[������V��o�o�:.$���ʡ��rnaG$��P�cE҇$}.���6��C���iD��r��l�)�E���H�{��1F�f����I�^�0�}DH!Պ˭\#�.&�J%JU�ۋ���B��j9l��L1�����%��b3F����92$O�%����,\p٭o��͚�
���g�u.9H���uմ2)�Ӭ�M�a�8F�US�N1�vXݺ��`�ھ��{�?g�
_Na�b�Q�A��<����&�R��Al��	!��q�o���ۋ�����&�?��rj�p��*��)8��!|d����׺�㪌H���2��L7�R��V�xK�غl�G%�c�ʁ��?SJ��[��ez\�ǥ�ۼi�7o�y��l����l�z��#Ugp&e�4���S�͊����uj��6k���־�*Xp ��H�A�{�c�C1�q�\�����]���\hZ|k�K�8qKD:��7B�%Nq1l�"����LF�[
�B�8�+0������tK�u8R�� &;߉��R��\�b�Ȣn�
.IB5����$�1��c����ř�a�do���g>�Ifب���a�r�7�z8��>�<�3�8�!vP����Ҩ�?�D-jĘ$0rq�7hm���U(�#�[�B&{5���b *�F�q7�*"ԥ�Z�]�o�jP���8��И��QR�.9�D3Ҋ�h�.���+0�A!��2��e0rɎ����pV1�{A]9I�Vl��0�Ȓ'�%SxI1hA��82�%yl�����KWt��*���lP�(:~So�(�h_�ر�F��%�i"9����M��7.)�3}�R�!
!@Q�4l��K�A7��""i�J)6��R�4�O�YB���"Z�Ie2�2n[O1i�Ce(=IM�ҕhs,�p���rι(MiyY�4.���L�����$�A�V �1փ#�$F쭆.}iyFM���-v2�m��։�}��� ٍn������~czhT��Z#�!�v�#�,��u?�<-���v Xx����:`N���؍>����r��u��"<��K@8��!�?��Sc�Ad\�Z�Vk�h�U(�b;�Q3��GPp�Z�D�F.S�~3V��~��̀�������BM�`�%Vx�D^r��FԤ ~.z��"�ԨF�F\k糣���ez̃���:4zu�j��_J_�4�x����)QS��-�:-Ȭ��\���bC*߀)��m��(4fN�ՠo��q�DJPΕ��A�EaJ���ޮ���]\��V��I0��K����(f�0U*BZ=#T���[5*/#mt��"ˋ�4��m`6����h�UQ��ը��Fٗ9��	ִm���c�URS��!�{���L�}
+��a��z����x�$S�tWm��?�ծ1�6����-bfW����=S�UъX���
d��}�ZNM]�2u��Ү�)`�0�C(/}'$�q�I�d�$���9vg��/}�r*��|����uf�u��(%d�PR-;����M����xS�X%�.9�k�:� ��ў�9���Z�~�f�d>b�?�*�Х2P���jy�Ca�w�#T��F��*���g6�}NQ����@v��(��i
�k�����l-4�,�i9.��L�f����>��a�9]�i$�-�7(����i�]4���S��F���+��zk�r��OS�ٌ�B3�M隦-~�qJ��,˨�g��u,�D����ϣ.B`*պ ��n������rjׁ*��9��e���{��w���h8��`9%�:��Ԧ����:M�����hiw��8��0+I�����,�9�'���MN�N�4�#I*��/�O6b�`����FO5
�>*i�hB�.����rq��֡�B�xOٌ�U���H"$���l����$��q��% e۸˅�E�����ۮ0��/r"G�����/\ʏ'�m����N�u������m���r��,,���ò��PJ���g3 �QK�"p�(��ނSG��e���!�4�D��P���A���hđ��bk������|��R����PVI:L��z�x�hl�o��#����z�Y�kq�-�D��a�a@�m���\��� G�����������ˮ쪢�Ԏ�@��k�PJ�0u���P���t���À���:LQ�!Tn�x��p4���E����:�O��r#��a��Q�!OOV͵p���86���PX�+
$�aA�������f�yل�ɢ	��n�f�p�C�M��Q����Nr�+r���f��"��j�f�̤�'@J(���b�
�<�h��f��q�O,��0�a�-�"����A%��Q��NC�X��bK���h�-����tC{2�No�01/QNБ6����R*�:�$O��A�����B2$��:.e8��hj�bR5�؊ꖐ�w�&С'G۰����o������o�x��2)����n��*��� k2F��a�o��p���q�`� ��m<��������"�Iy�Ay�'N #N:R�8b0� +Sf��!?�"Sm_��s`~��h
eSF�|43:�P�F�K�)�J�l��`r5k�k���>�&��C�넸�&Bptbq��%�ªʥ��!$�O���Qw�(z�l�R��S/�+�p3���3;3�3���q�xa�434�R.��G#�X
~N��&M1i*P�ȴO�#8�as>�-�O�@����.���P6�|T�r3?�7 HAE�s�
���6T�r�&o�C�#���F�����|�ʬ�E��F�oF�/8{s8���,gN��+��9� N!�$)>��pN�f���-J���,o4��A���(�/QM��hB�:��LS�~f�L��hh�!�E�D�ܔF�aF���3�:2XE]EKOIrP,OEK$�4��P5TQ_��Ʉ�Ɍ�>�j�f�h�|�&Nȅ~�E�o��q�,�ΥT��Ev�������4���Td�q{��>�DJ�t���@��^��=�2hv:س�2c�z�d��$��	$@"A�[�s@�+���hJ*OC3�Tj$[n�\��J��k��^qI����rp&�VNH[dB=#Ȇ��'��˲�ae��8Ɍb��j�)��)�U�P`5ZG����P���sX�O�O7*���X�R4�k6������2�B�g�0�4� 	7���V[E�"ali5�\�����in]�v]/�X$��27�0$�� "5������\�_�C���VQ=��K
gq��|�AUnftb��ˢ�ܴ�*�!����z!L�`Q| �tCHfKec�V�~�T���J�:���2~��P ��O���xw#8 �]�:"gWi�2��h�a,e<eA��j�4�Ol:n�|R�i��k� Ql��=����l�J)R��#�j&�a`�#E���V��d����o�&"T�7�"[p����}�`��n:_�NQQS7g ��t�eE�ff8$Ԁ174��G%��/�X%PS|���g�a8f��XwP0)�Ń�vv;Q���@O�:z�#tj��-wP"�$�2U�$��^yxC#��z���ZN=W1}ǅ�����7��pT�RF�7E����1c���
��,e�t4�y$�'P �W���%W+� �3��SX��hX�mY��R�*x�y��0�5�Z?.)h"�ViG�&�!�>O��w1�Ol�j�ĬP�$+s^�����Ty�;���mO��u��������I����q�A#���X?�僇s�d��m+�`9���q�2�Nzq�dFr+w��E����G�Ĺ�c�G=��&7��R�JӐ��u�t!:b��;�[E0sX��ȧ#�0�}��@����fT�A2��YĨ�.A�Q-��ʾ�U�!::�����Q��M�j��w���\���y�Q]֗*���1ؒOJs���H/�8�u�r�ٛ/���X.�Q��qg9s�'x,)J�Y����9��C��#�������H���<rjzj:8jF3��t�]��B��L�r���u�@�V�|:!2�`I����EX��� �A�mBZ:$�x�RbK�����ڏ쨧�#���7��$�8�u#��D�^�Ŏ��<y[���� H~~vfZ<�+��������v[i��i�iGِ��E��+T��]{C�-S��Ǜ[��A�{�U�l��O?n�CƆ0U��˾�"�?T��|��V[E�T��̙җŎ�����P�R�|v8Rx8��H�ڢvC7z�eSJ��f�|D�Ԙ�ĉ��#sQ<s�#�e���O ��MV7X�ZG.����$x��XSx�rOlf@��yk�纇#W��OYʧ�ʛLJ�[���B�(\���K)�j�\��e�""e�e�u!��\8�A����@A��\<U���iJ*ӕt����O�5%�c%�JO<ҝZ#�F��S��4Ӊ�����'�@bs�3.�|���a<A�����t�`�u��$!�SY��uV�&Ɓ�ohr�q��\<uT�{̷p�`��΃��X�������bUϠ����_p:�����0X�r�ZMS�:jB3y@�x3�cKu�ٚ	s����Ņ�i�?�X�]��x�����7ܵ��Q�QVV� �;�y{�h"�cYE�l��0���?g����k4$@�n�^�������ɜTaz�����}�;�z�ų;�2u�>m=r���)7��f�cj�~BE�X����>9���>���A��������/9���f��-�P��]ɭ�܍�p��כQ��gVI*�"G���Eb;;�6T�X�Ŭ՜��F�0��3�� ���]� �e�F���kb�F�!���"J���4�	�k��ڷr��}X��o�ʩ��R��rᦱ#(�Z5�8R��3��iذe�V��uF��[ʴ��zL��M���
��͚�k^�f�9p��uٲ=����i?o���둜9����Ƿ�߿�L�p�||�-YeR��&]�����y�����t�R�����'���y�7��'5��L�9%�Z��֛7;��,�9o�R�+�V��?�J�C�8���yn�u4;~ۺ�1I�S�dܒ�p�K�U�9ß��-ZԨT���B�;v��ԕPZ�VZ�T��;i�5s����O�T�/��a<��c؇ �؈���Q9�<��=+J�gK�VY~���T��v�=��h����lM�$lD���n�����@a#��s�teFk%�uUsSu1G�vu���V!�T�H�ytR6�WM:��PN�4�OB���7�@YdR-��LةeOm=�VP6h�Pݤ�V\Y��W}^TM]��b!��j���ժ>����R�Jf(�N�[�7.���K���i����c��P5�R�葆R�	0��\��*ĕsΩ7�D�p#�M�yt&I&��y�16R6#���{B�'N<�U�O��[;�J��gK�Q�Aa���d5ԥ�΀�v�W@ET/��5��x���}��%֗Q��j��.F��k�3���<��#���3�<D��D'}�ґ59�tI/�l�-AeY��q[�+�`�4&[%ً�Ih�dR6of�X�n������)�N%a���]T�,<���i����N|�Z��i6�[�i�eM�i5qU2"#�e������H"_옃b��n��M����0�Z��M��l�L�.�7�ء�n2#s���\Wu�7��)�D�qj��h�˶wq�=Z����Bku̓�6�$6P�������7cf�c}���;�p���k�70�o�c���7���t��/��;�e�ˠ�hs�V}DV��L�lv�\�Lg���SVh,�ՃX�;V�v��p��~@��$��x� �;[֚.�@�&�ɸƥ<�l��i�BB�0�1	u��1�ELl_JRx�C~F��eF�S�������xx�@�x�&5��u#�D��4��n\�r�{"A2:re.C�h�׍pW2���f���hG����1Cʈ���ô�P�)Q�9�ɖ�֖��LÖA���B��/�11�@,�ڲ�*n$%��J�Y�2��9��4���ǽ#|KJ�ʨ�|�ƍg�� ��@-�a��T���N�A.(�Ԥ���F�ȸD��G��~)��ތ��a����4z �G<+�C{GJ�T�8\e+Q�p�pl��-	�O���'-��%��K+�O�渧LI��B�$!6l>EyG��8y|�g�;��?�A�LgZw�eATK<�q��i�zQ	; ��L+�gP��F����AY�"*B�h�v#��V�i���oZ9%61#ȉ�-�bĞ��Ԙ�����$Ĳ�r�G6;E).��W�+��IL���P�X�ln�oLm�ܩ-�P�A*���WK�u��b`y�/^@BC�T�rM�5�p�3�LBZ���&����x�f�HX��n��]����pR�Z)	zt�C4��H���%��9lr�x�r�C�&Y\�r%�N�v��|�)O�m}cGQ�R��}[��z1�u${d�m�q)v���h���(J����"!.�ƅ��*B��.F��Lh�)� ��4�«�x�$��2�Ie)1j�"���~/{X�T	:T�Eq�;�(��[�5���'��)�//�&�x�9ˤ���9ơ�Ū�ۉX�q�BW�Ѕn�@@���q@�|��>ɘƨ�7ݗ{8�Eq�&��ې�6�<�7QQ%��GL��~�R2m*+iLcR'����	G}R$�y������K��l>�Duώ���1m^��Ҷv̨M?�J�'�s���CS���%�K]�s�Ǆ����,��Mo��ьpm	[@���t�B$pN��e}Ɍ�=]dO���h ^�S�FȩAL>�����H?�G��k4���h]	'���F-��K���IQ�+�
r�7�h%9�bӄ�����e<�t���YJi��#|�Le�Q֑�>�&��2�m4~걧1�!�ŏ.��d'�3#d5
y�w��W=��{S�?M��64{�����u��N���|Y�b��aW�y�\��k�{�����A��y>��~�/,���B�[��{��_o�y����x�^�b�ط���7���{
�q7��-c���jF�ϊ�Ndo�u4G&�'}����H{��5O��3e򕷨��f~�#���x�x����7}��,џ��7p��{��{z�Bg��?R||��hx�G}7�|�w���F��kTTy�V��w�Vd�s�5$��#�;Oa4GA��G�#�3�w~K#z��P�7��'xD��W�����7z�{LǄ�(��W2{!{�9��'���"�d�7�F��|w�pҷ=�p�jg�3G#^Uz���D���� ��,(8C�:*�~y{����@G!�4��"ߠ,����X�%{��|؄��!�'�,�����!X�{�z>8|�0�`H�u��'���"�"n��P��V��V�&dJ�h�]�^C�>�x���Հq@C�r�Jm���8C�'��P	�p4<�#�H,GHDc� 
�`
���X��7�ۑ���P�{��9��|��i������)�x�`˰�t�����pF�i/�w��p�c]��pqu��3����"^A����4� �h�`m�4��P^��r�bڀ�0+�H�~"����Uee�gx����h�"�א)��g�����P�4����(�z1�Ȁ��t��~�� ���Q8�<�t׀C�����A
�,B�qj�"��C�pl�$O�,�uW��PEـ�`j0��yR��0�!�`)ry����x�P�P	hp	�x(�§�P�@	~P	�X�DXlV�=�h�jɄW��YV���)�����G����0���!dȋ!�u��Bq5��%j*Ē�����(�$�Lְ	�yH�	1X;R�WJQ6����H~��]����	h��}<�)���W�	��	����;���:)z(����w{Ç� &���xz*�z�7��w�P� �����pDE+�A�3��&W��q+�P{�4y5��aԠwp	�����N��=	m��[Jm��1�9wx�v�hj;����#*D���@�z�Ez"�xP��P�z(ڢ��zR�{�)��	q����SI{�w��p��� 
�p*h�i�(���MiTPtgBLz3=�����zW��#M���"P� � 
p�`�3��`
o��`��P"@�0����W�jP	�A��M@Őt����x��� 
���0��x��_	ƙՐ�:��:{S���Q�0�Ȱ���@��ׯ�ju��@� �@H ���a�ı�籬���"Y3�JyM�N��Iv���]M!�= �:�� &�Q�n��c.�5p�FJ�5PP�7ӏ	��`8`����M�� {*ȅ��p�@m������y�� r�*&�0�� �ډ��7|���&g��9@�p��5�m�C⩔�,�`�H�����A��~�'�ù!�*�'���R3��cϒ��3�3P'K�a�  
���F@JPy��P�C���/�/e��8st�4Pv@(B"Pе>8p��q���@�������J{��x��e�	������ t8�]���AҢU�M�MP�0��5�H@
˾����J��z��l�����Vo�+'d�r�a����+������t�+���P����Æ�%t~3.�vz�����ĦaLq{�bmPի��~�*�<}J�W������ȹ�u��u%Î��0�p	ր0� 
%@��Vw]���R���P7��t��
H�!���z��&g�L���A�/����O�
� �"�";�ҕ3�����,K�+G>��nߠ
P�p��
����g� 
PP�P�@3���tP(�2Dm@K[;@�P�K�!�!�FS�n 	Ȁ��B��{�zE����y�7?�y��G�@�@ӗ�'D����� 
�\�����	%�� 
��V��t0O@& _
�����|�r�8�誯���C���B+L0V�F�@Ӱ�kJM����
����:9-aـ�`�H*�P��0� #n����JP%�%D�3���� d�P	�@	�`@�`���]�x�y�w@)	��4�0��@U�t1ґ���~x�Pm|��0k�7`��P��xP��`�JIp�PM qc�2!���DP6��nUF�`�wZ(?��+�qDvٰ�p`l`���p	Ӑ�u�6�0; 	�\�p�;F� 	s`�p[�I�o GEy�P
�5@�H���Mp�a��y	��H�`~�O��1�m�5�"�ܿ7Cw��P��#������V�]CQ�po%�PtX=�`z���HPJ0� 	sk��� 
s�5^�N �J �x� ��Fy+��0�����,<��Mה)�� �9�vP	9�~�� +&=+��r	���Pg` s��6�`�`��`
�pĀ-�P�0��#ag*cp��M�5�!�P�RS���`i
g@�PA�	�h�f�J�05A��G!TyK�����5���90���Đ�0CA���Pc����j��\����<�yh�ʢtMG����_L�5���`qG��F�i+g(�����h��0���",RtmȲ�������z������P�z
�����}�	� 񘪟�000�_���୺@	���t	� &��a�����v�i`˰�p��U�fP��Sr�UՀT�f�e0b@e�h@4p���Z	Y�"T�`�� w�Jh`�������~K#�`	���%�Gq@�m��p�P��!p�0�*�p��~�֏ � 
J�J��f�5x���Q?k�4qH�i�P	} ��ϰ̀�dW�Q�ș�	���z�����@�ڐ�p
� �%`��:�����V�n�Ȑ	� ��������9���������ƀ��~�|��/�~�����f�f0����h0;0r�����@���o�я� ���L��!�T	��K�"��[%=3�)�L[Dk�h��c��Amۺ-�#rS5nՐy{O�w�C�)���n�C�V�e1�q���<x���U��T�U�^ŚU�֬��yeo�Xv��~�w�2@}2|�:b��~�wO�S����ˋY&0B�' ��";]�k�,ׅ�n0&�3�N4�r��+X���έ}nj<�&�o�M��`>�0��;x�<}Рѣ�Lr�e� W<<ymØh#3��:��3a���3aԷ;[�~0������m��nm��i|�8�������$t�"�@D0�y��ɫ����+��z+�g�)����þ��M�O���<d��l�k�����qD�;�jC�t�	M4sJ�F5c�����;�d�϶L�1��j��I�2K+�mရd�*�R���s��|�΢�b{��r��9�O��[�1���-@d���=������4R���)�+�������0S�<��yS#�:�ΞIG&t�A�q@ˌ��l�,�XE+��k[2?�+o���6@��H�x��0��[>�n/���J'�K�1�:����c�0L�45sgK�5��ӿ�i����Y��si`x$E8��:k���x�m������&�LIuJ���"�����,�P�A�r�gGv�1Ʒ��Y{d��r&2&��xb6)�1�����L�6�����-�3�4�c@��v;���N]Ĉ�L�M�=�0�lfk��c-�
�6tHD+�٥��FQ� DGF<q�d��!�X2ԎmL�L�������n �-�{�A2���J'�rXFg�X͡XY�J��Wϐ�2�f0"=5�_8���'�4��8��D�����0��7K2����4���z+@�h�'u)�,nc�G�3>2�fς�^�IZ�oUm�������q8�ЀR��h`!�I�6���(/{�J=�q@�!x(�ƀZ8 �XfR׺ՙcG��������v54�0�A�C��w�1��v����{�j͛��Kz�
N��U�E�Uy�|2ᖸ��YhC��zö��ivJ�~؇/� �~����՘�i�1�	 \�A�b}<�������U�q��̙e/c�ܹ�1�����Y`��ԥ΅�{]:��'c,CG6�]i��Bs\c˰���H��ɉ0ͨٳ��D�T�y���X�(^){���x"/���i�q��s>]�
e�B[&�0����1G�,�d���r�z�}ꥍi�GU��� 9O�efC�$$s?1-|���<��,#}��qBX4ԡ8��
=����p�����N�g0�=�Qi��#u��!S��2�"�����n,ҪY��iOO�&��K?&��%V�;���6����1���A�u�@��U9�U�ٚ=y/6<��6Ƿ�d5���G���g\#�t�3r��a�Rdp�)~�^��J$!<,B�d�cA�(q�3a(���6�a�o��4�ͭ����k5}�H�a��ӫN��.���`J�0-��&���+yKL�NkkF>un�\��a�ʖ@�1jc�K��n6+E��,ϧk��BF��6��emc_ \�[�!W�>��`9`�����:
bN�D*�?��V����kX�;Ýipeɠ�w�x�24KT�h� �5N:�2ƒ��X������偔r��~�L.�V�� Ĉ�e/��J�t^J��>�P�IY)1>��r�W=����Ru��ڌF�����'E۸ϴV�Zchc�8%�j׊^�)J#lo��:��E,���!���L���{J^��[�X��c�Y�͊2٘UHǑ���̀*�A�k��q~�@$u��x\T����>���O��g\ůY�M��P�YT��L�D.�A�'�>%>CF2�>�캅��c���Y�D"2�$���{����cGu0�"�F:�,gvg�.uMG�d�X��I6�%Z؇��c��CB|�;L��-�H%�ь�l��E#la�k�JXר42.C`p��Ũ̧��k��ba����X�ˍyҴ���M�6��p����Q��=�M��L�2�} v�ڂ�Q?�0.����Vec�!��k),����5���r�(en�'��KwV绷�1�f��=w�� �K6�A�k�cu��Fm6�J���(����'�x%%�㪁Pb�e4���3� pP�8���� ��.l����2��TyM?�5*/�1���>4����L�JE�p��i0����i�R��k-L�Κ#�\���Ji�"#�}E
���;(�Mg�c�6��΅d8?#)��5�,C^������������ˊ?�+ہv@�k �;�:�6hP�������໹��w��6P�&��@�%��%����= �J@�;X�и�1H���80%��;ZB]�`p88��HX�bظ]��z�B�4���3d3.o�2!츹1���M�=� ���:��_���?(�=)�i3.���ӽ�0�� ��������2�	�҈ )33*��i�h�t���Y�S�k������+��Ą��

Ezb�MR�&�A��h�[k�^��nЅJ�d�d�kЙ�8���xC�A��^�J�J�>=83	;P��ش^�h����SBbH�@�]�6�A���b��] ]`-��:� �0���`����V;� 1Ø\L3x�6�4� ؀(�܎;038�JH����˄[<�4�]\F�8��:D������A?���Ùc��c�B��+D�ɓI�'q��
�L,Eن ���x�l��[��[pJ�ɇ��j7&��]h�X�X��& �k�DD���^P3�D�;��
v K:@���D8�λ�3�3(Ѐ��[(a�\<�G�؁p���bH)[�[!�Sh�6�[�;j �\ȅD#٢: :h���: 4g<-�++?��]Gǣ���9�A]H@]�����ЀX �G�5(�}4k���0Dp��#�:��;��?�>?H�iL;H�4`�>`�����(�4��Ȑ`�����]�E]���\lD��?ݠ�J���+d�E��p��M��M�k�+}����꼆�(ŀ�
��<j�N������|ʿ�
�j
��;a��78 =��b�JhD���,���l:��l(�uxSx�� h�Q�K@�p�`0�x�n4��P=j�$�3��3`����wT�JЙp0�o����K��i��(ݻ[h��$��2�`P���:�B3��q�LH�1��N�H�\�JZ�[�T\L���%X   184 �����v�5��$�� �a,C�H�s�H05����iTNJ�H؀�܀؀� m��ȁ;8(A�ED��ln� 3�IZz%��EɬT �@H� ����|�2��f	;85��f��$@}�����q�]�s �|�v�^��J�H�+{X�{(P�_ �n(x����bHѿ"�K�jp�)[��\���hH`A9HR��9؁P:�;("�HH����DH90aԀ�R�e�9� X (��L�K��3@U�L�����HH�?P�60bP�6�S�I�A��e��J�i��?X�(�b�:�G���U{D���`�;0#l�3@>j4R;�E�o�����5�XUH4(�`�  �9 Ӵ9F�� � %�G����@��́�́60��H�5 V% ��۵LZ�L��[�ȋ$B6p�j-�J d�R�s��쪸����kɆ*� �_�G�m&E���]X}8ཀ
���;��u�{��5PZ����8؅^pb8{��a�́8H��J�����q_ 1j<[�'׃9p���� �8�6��(b6��h�� �+�4���V�������Yp�Ѐ�^�D���TF��������DpO�6��9��T/ER9���� ϣm@%�&�7�]ȁ�dҫ݁$���F�U^5�_5a�:�4��� i4Y}��Lݬ�V5��8�����^�܀����� ��(Mb��qP�}����[.��D�U4mcV>���Y%�� ���?���88���@V��m�gaø��@s{ &�P�P � ��H��)`�Їy�WI<
�5`X,�Z@��T8\9��J)�opJ|P�5(� v�DEk�i�MG�
s�d=J	ޑ�{p%E�P(����ǽ0K؁9(ZC��&��=U9M]�f�Ѐ0 ���H�ń�� uԁ�� �+Fbd�(��]hgl˺|H�����l��F`(��x[�~���b6H�Fh��ƙ�]P�(Z��\؀ We�Հ8�g��0��܁&�F;�Y� ������X�^-�Y`��(�C�i�jE�M���N�����TLh�N�Q�lؖ�( ���� b������GN�੟�!;�]h�(��mʿ"�60�?�|�:��J�[�@��j�(����|���-v�PH^"4�������&�{ $(�,�G;h;�������Ph[;��'}^6^�X�Gj���P�l�P�\ȴl��6B��*�  fb V�q �Y��i4��%F��b(H�Z����-6� �H�Ё`����S[#4rH�0��YA�E0�0��j��8h�Am�q�TӋLp�dJp�6�O܅N�W��x^3���^ ����^b��M�U���[�@]Ho^L;p��ۜ%K8�@�م�������e�q f8����������/��
�Ad��-D8H`�����s�r�8��o`v�� �F��68CP&8�b�A:��nPp���^�,Uh�J@�3��hwjدL��h(�U] ��kZh�n���&h�@�JH�bX�8��P���Y�� Ke�� )-�uyjPL�iX��_y�q�Y����������P��>�yKR�����&P[+F�4��e�7�7XK^�^��\P�=�*�\pN�6�E^�c��Z��o6i;��P�`� �t��5f7�]�EjV���l �t�Xv�X��� 0�]�I�7؁;P�_��a�Vt7��F�� p��^���؁6����R�HPp��X�n�G�L�4PA?@�3�KD(���8��e�`��O$P�k�$`h������x�p�_�4P؀�[��^���\r��$ȁ[�7Ǒ�q��j�[��&�ҷ�G�{���f�j��m��>�D � p���/^X�Aa��7�X+W:���y�Ń� p��A 
<�Ƌ�JjX0�@͆pvp�A�Z"L����8r�0���4g�1�!�Di��IdRg�9Ã��&]� �"��6���0�Dv��Q"G �@��܁#4 �p�3K�l����68�p�FSD�,UBd�F	��WP���9f�DXy�5ꆸ�c��4h��a�(P lzxQ�;��T�D	��B�0���j+������W��ă��:�x!WgE�z]�?�1tlP-�4RI%��R�^~V�!4a�5]s-�xP�P�PC%����m�	2�SL�P|PBl�B�d�A4�7t��1(���d�M7�d�M7K�r�g�0��C�Z @  ��Ib@�L3yp W��@BP  ^�9���e�N��ÕW�Pc�4�Q��Pn�P��v<�$��Q*�wW	��@;�Tهum`Se�y[+��j�٪�
C�����$zQB	"8��Zm41��m�c�%4Rnۅ��al8ʗω7�{�D��"���m��i��Aa!<uZZl��z؁C	x�A{�zI�I	��A�ׄ��s5���@sl��%� rF�!�pC��F"t�qGj��j��p�$VGI&���.��Dh܀<�4m� �C�P.K^cM5�D��8�`�f����4� %dA�d�y@kg�Y�M!�p��q��%�����v!�T"�is�f���d<�M���
jpon�)-f]��xt�`{�ZI��n3U۬8z�V�ʡ��g� G%�����������6��!��:�pت0pt�w ���!��f]!x���twZj�-00K,�AB�z�X��r1�<<(�)v\BV����r	 }$�4��J0���3p�	8�Q�ǁ��gx�A��, ZBH�jt�P��ܰ��4!��j��9��DL��&���0i�\c�(�����o�c������l���4*cTӁ�r��!�Q�@�.�0��d'Z^c�h���0tH`uҫ�4�o�`舓�l�,ҁ�@���A_f�LX�ɼ�Z�Q�@H����e������ �eD�*|9%3�׽��Öx	�QAvЄ�S� 1�A�k�#H���bD�A��^�T�7�Ds3s�W�`�x��) l��`�2,� fr���c蠠iL���W�N2 �� [C�J��. t�um,SC;�j�3q�v@�~��.�qU
]��� �^��$���g�#r4$7�� PI����̴ ��r8܀!p{'A���3�sV5�C���D�� !�T��=tk&a�Q�cQ�L&��aƳ�u�G!AY�b���phVg�@	�R�jͤF9�;�WV� �'� 1.ҟk��栃�T&�*�M�7�;�2&B��23���ך͵ZS �'$
��ْD惐�"3/� ��6&)�A���"����&~�Z/�Ԡ����k]4Mbq԰tg�a!��x�^�Ү���9x��t���mhz_�^��14��j�P�oP��܄*P�	�\y�:�8+O�z��fŚ�̥w����� %Ё��엌 6)���H6���  �T_�'<@6��X�5�����{�����phK��Kd����� ^"Q�05���t��|�&:�@�a�OZ� �� �J�1Kȡ�j̎HbM6�I��S"�t&P�S(6���d��6� (����j$b=���Pܰ�s�4�N4�Ǟ�ID�^��N~79ѱ�g�/��\��x�>�ASlДGvg�8ü�U��sz�5o�����+��\m
F(ᡧ��%��1]��M´5Y�Ms�Ѭ��jT�@�#ڰ2��
�3Cy 0`%l5K ����� �����16ɀj@�{�z��P�6@ a�A���2�S(��t�p�֔-���)���J�ӢmӜd�Sq�m�Y �לj1��-Ұ�]%����� �j��x|�끥R'�Q���K���)l���D$\�Li�3����:�!x�=<g0�s�����n��/(?l�o��7T�0o��Mܒ.�)��#�;۪�D�%��rnw09ġ	{��NJ�f��\��%�C����nC#����M^��D����}���tS�A�����[��4_�v���%E�U+q(��P���J���ࣤ|V؀���7@[g���_�lE/V����~��	�ubɆ�Hʰ�s��ܰ��OJ����@�(%���\��`@�����T�[�;����q�ۙ(Ds�H����K]���I��O�җ�������d��d�Ͷ� ������ԛu���@#�A�DV��Ĝh\C�����,[��^�ݺ�KbD�U���E!X	D�Bd ��P�|�9|;P�*pD�Z�X��է� �
ƙ	��4JB�E��@Sd�(Mr�����ڹԆ�	K��`Ԕ`x� h lLY�D
ߔׄb\t�H�)ݗ}����		���S��״F���mb��W����Ȟ��G�,4�� ���L�]��IA{,� l�kL�M�����������oiX�t������ڜ�Ԇ�J� 0$\	 !�S p�%\�� �!.�!%��@I����љ A0�;��<�;h�7�5|0�Ĝ�W/nߝD�Ie4�}�T8E�� 	�@������+ր���E�'R������")N��	��AU��@f�� }��B�@
�����e �c��Io��L���Ee���$@�"�� vb��[r�D(P� |@4��©��7�0@��-�ۣy��1\Z���T� b��AD�u��eț� �&�$v�K���tiE"K��ƄZ�,�C��8hdGgGr�F~5�0]�����$L��#�#F�@Q_8�S�F��E\/�M��������_)�����o@A��) P$�[��X��:���rr&mƴ��!����#�A,�lq�ɼ�
�� �7�C6C��	�y��B9�a㗜�hՈ̖�u��!fZ.g��������9DO����K%������Cm�@W�Yg�LCRB$ȁK��J�D8A0�G
�p';�0���<�	��e�@Ԑ*VF�A�����@wX'%��S� vf�\TK�f�P$@ZTe����G�@]8�� Td٥�]������+�V���y�u��E=��?*)Y*���Bp%[��]fy����(�\d[m,5dC9���AՑU�<���� N�$/�Ƽ}I��(�u�Bh	:%h��j&�eQ�����lB"6�E�% ��ӎ�  �9����F�8���&~	�4�
4�U`\�4�#��@P:�L���p��2�ca����Dm�T��(k�E_,���z���@�� ��mufB�ČFo�[x��fK�#���f��ח0�,@� ��)�����f�����I@�-8�6�Z��G(m ����!���k�S1��]Mq���;�- � c��a
��f��DjJ�iޅX,"m%X�%�+�.��mQA`����<V��8P�!����;u�� �C�) 8�@Ai���<G�L,���K���h@�UlȜ��8t�We�,xg��K|����?�F�ȉU�K��Ix�"�	�%����z�� �4 '@��)�*m����(���B AĀ@4A"́́�h���DZt��~ٙX���#�ңfv�d�oI��M����
���1:q��d��SAZ�q�BN��F�V N�"���@A`��Fư�~$�$5�0�cx�O����v���P����#ȁ�Fa��<�
j�On��#� �Y!k\o��/��f����@X�W��P���1 ʙW9Q+�ʙ 6aN!p���^��d������t�7���	��&c�E�Y�����e)Q�ɗ�O��5�`�. ����dL�'*�Ai�()�ӹ��,� #o��-@�D�<���R�$0��� �N��d:P�FV2��E��3S�HB�-(D�@�ADqT���(l�= +��S��T����-,2�
�'Y��:~�� ���b�n���z�[j��� 
�KZUd^\A	l*��J0\�2���J(���Go�4���a޷� -��:a��n�VZ�� ;��qK$�Шj�R/��oI����q^�C�Tr�����L� ��T�D�@`ZDa��o�3[�q~1��4S'`�:�-DD���4VfeH4�H����T8�� EUT�j Ԁ#�B� >w͎,��i�e�H���K���9�{1�-I�ƖD߭�ͪ���d͂��n&K4��*�8�$�	���E��"hI���PO(��T�í�q ���A�[����N�n!�ə�	�z�_�b�f0�\4ff�HVn)�VQ��M���m"� �B1H�8�54�4[�H�|C1�X�0<�ps�� .���`P_�F��E��]���ƾ�� d��ޑ�	-7�j�4U����d?��*�(VA�����(�s�K�֟u (�Q�nXg�Q�I�����oߔ���i����;�x���j@e���t��%�ƅr�b��|�6j��;��G@A}��� ��ڶ�j�,�	� V&S��Qxp�XpLY��Ђ-�B{�dHZ�V�{G3~5��(� �j�()͇,� +�|�c��[��}�v�<��[j�S�e���X��(%���@��jQ�w̉V�LF	���4B#�.�U�6�����2��T�O�&Np�.� D�h�VqY&����P'�ߐe���^k$��x�I�g[� �ɗ�U���R��e��%�,���PLV �"Z�a��w<�\i �bw��Nt�@�-C1|4��5@�ȇ||��ȏ�-��/�����辵 L{ԇ���mqH�|6��	�P��8�q��)�J�H�֊S)��-}��T�A�w�8��hK%<�%T������y�����`( �����^��(T���^��MDX�ٺ�5G��6'���Ѭ���Qff���4DkI�PLX3"Uzڬ�݇d���f���j����ß0�V�@/���Ō�����+1��o·��wH:��J35�7��G{@���j+�	� �,N�H����.�!E}0�Cx�Я�h)g7�|�%U��@X|@���v�m}���5˯O8o����/@, 0��H��a� 8t��� �q� (P ��" @!K�+�<�@	�iز]�֫�$Dpvؠq�Ƌ$5@�@�t�Հ�@Xk�%:p$ `,ٌI;l� Bċ7h��`�� �w�"�8@ �!� |ƌ�34h�0s�L�.4,��Da5.���o��}G��h�ߨ�]������TAY��Cm����ŋ&q)���F�.��+R�#�������W�P�"��!+2
0`�o �]2�Z���h����C����߈��Y�B�����j� �(`��ˡ�$RH��,J*���BȢ�����ciB�T�)�<@�.�FFl��^.ɱ�Kx��C^0��ذ���2��B@�"ѫ�6Ȫ��J�<�r�nȡ��"*3 �V�Ȣ��**��1pH,�3�8�3pp�&����ج��>CM�r�Am��Bkͳk��c�%�8ح�&Ը��[�)��8�
k �{���"	����*��\`��{a2��%��+H��,(K$#c����"(!��o��($h$�Bb�!�XR����p
 �/ ��i$-2��%�#��b���K9t�e�Kx�$G�7��i��ƚk�!Ɩ�Ҁ�.gR���b]#���N�>�`��
b*ä<� 5��Lqŵ+C�v{�ؐ��� �vГ0�/�c���b<+U���it4�U�5�RSE�������v��]���q���x� �}K�o��4țV�y�R���"�َxa�\hJ�ř�j������ �O�k��N��O�@9XGZ���<o[��Ѐ�,��(�誰sB@�`K(青K,�d�jx�����ƛ^x�a2��$��mS�*C�<���9l*X]i4!�Ti�^�(�x��0��xݲ��+�2�p@��B C����Ek���Fy��ɚk��j	t ��-����j�ۈq� 0,Z�V��B ,�W���ߒR�}I�ri�np� �O �U��	 eR:
�:W9X5%%)�s�~%$�!��� �O���G���+�1Pj��� �r�� @Ĉ�<0��@	=��~Ѓ(�	K�C!�q	34!V��
�%d��Q�C>R���QZDLN��%_u X�KV��6��sh%ا6��8r�Z�d�n Wh,Av��� ��xQ	H@�5U����FA�Q� (��s�c�����AG�!���O��B l@p��|BP�j���C�G`�:DH�R�F`%�Dg.xAT�—ī��"��f��m}�=ws�H@�8�^l2��P�1� $A�3��d-�A#ᒃ5ր%A���� (��:�2�!y�C%6��B^��0X��If�#��HG`U������G�u9���U��Pm<��@�P�	�V%�rs`C�Є�e5��� X}})�s����x7�ᮆK$��`C4ߘ��X�@	f�5�0�7��Y�v��X-� @-�BD\���RRh�5�L�d=/F�t
��h������L�[U^��3@�6p�su���nKFʪ1��Z��������uCIO:��Y�e�`*��1C��Br�)�o0�U��!}�G	�4����F0�d �&AHŀ�h�^3�]0�[� xA[��� X0Atֆ6�!�P(�n^�����ҙ�^s�f�+j`�F�*���&�(�Y6����9�q�p�ó�ئj��Zء�7X�Ny�*�D-��%I�:�h�=��%)��v?�:�g����>�9鈽��».�^ɾ`�i��<��OK�鏵Ch���\���WBZآ��ƍ�Ӗ����d��e(C(Q�KP�fX���fJB�I
�Ƞ�B��H��]�訐�����8`P����u���J���ю.]2�/?u��& �b��#n1Yg^��:G7SMn�c�Ȳ9��e�:�m��E�CLt��%��G�4؍A�����]s@���\� ��z�6p�9���Ya�@$�*70��ް�b����������%4��J�ԯ�L�OE�$r����2{<a��6�ﴎ9�E,Oc�$Ä?T<��ip�a���͆�?]�:H{uu�����l\d= B�!`T ��A���tC�D2Ճڬ ���ɇ�v�)t۵U�^�@�$B8�-���k|#���f7զ��3\7Ǖ'���j��e:�Z"rZ��q�R��Ah���� �(��0�Zk�%r�`�ೢ��>CD�,A0ܠ�i�$J�r&ʒ��\У�CJ� ��$b��\b|�@�,�FI�d��f-�H����/�@� 4 �2 ���J`���(�*+�,p
@6�^|jj���Z��:z�7V@ ; GIF89a��  1!,%'%0".%-Ga$U-e"L'b,(V31q5R90i?8XJ8Dx;fQ<mhLP*Q3R&1ik0k#k%7IJK3SE8snKp/Mn6lMN6oU3si:PNVPNtGrVLsmmRUhUqokSliu;�4;�)>�B�G�5E�5L�=g�=e�>n�C<�j<�HN�FX�Ug�Pl�iV�lZ�mo�iv�Wy�c|�?�wM�YR�nu�Uf�rk�yR��Z��u��r��s��u��Z��p��{��{���+�%�)3�.�)�(6�2O�9j�A�2H�9h�J6�k7�J7�w6�JO�Nn�pQ�rr�IP�Pl�oP�pr��0�)�)7�/�05�1D�;e�4C�L�O5�o5�J6�j6�LM�Wh�oK�nr�JF�Xm�jH�op�S��Y��t��x��R��r��s��]��x��|��z���5��7��<��Q��v��V��M��o��N��r͓7٧ת4�6��/ΓEˎpӫIϰk�G�m�F�`��7����/��N��l��K��e�����������������������������������������������ʈ�̆�֌�ˋ�ۗ�ɖ�՛�ʘ�ْ�ᤛæ�Ǥ�Ԫ�ʨ�ص�ĳ�ҷ�ɵ�׮�����Ƕ��ȹ�ٹ��ҍ�ї�ά�˱�䏐噣穘讱ƭ�ƺ�¼�ԭ�պ�Ҽ�Ž����Œ�ƹ�Ԗ�α����������������������������������������������������������������      !�   � !�MBPW������ؾظ��������ٸ��Ŭ�������ٸ���٫��ڷ�ڥ��XXXXXXXWWQXFWWE?EEEE??������D>�DD>>>>>�����������������������������v��v���vu����v����33�3333!!!!����ܒ��������ss�����n�m�sssm�21�2����������,,,+,,,++,,,,�+����++�+   �  ����������������� ,    � � 4X�`�*lp0�B�!t��`Ã2D���C�.���0c��z@��d�� ���L.gҼ�@���@�J�"τ3%���ǉ�ƌ��I�C�Τ*5(֪WgZ�I�K�N�hZ�A�h=��@�\�xM����@�40`�Uʅ0�y`�Q�/B�YcW�P��Hp�c�Y�"������;Ck���b�\����zmDϳ�2��;hR�Wi�+�(Ɠ�
D�X%N'?(op9_�x�6H@ ��V�F�s1n%!�:qj�CQ�<�Q)D�U;��:1y����Gv�o8u�ۂ�9pZzE!�Y�	�[G)WaU'Shk�J���QwQ�\uE�׋�p@ 36�m%�Rg%��z9B��Q�#h���aS!�4`V�!��o#ҶaAMrX�����b�U��5!�hV�X�Id�HB֜F?Iy"]���P�1$㌀p+:�@C���e����F	Y��G~�[b[���R�H�O�*��f�m��g��b)���P�Iy�e�TלIz�%د���*�Ul�y(���E&)G�=f%�a
5��2Ŧ�`��dR:�)eⲕjh��Zh�b����f�l�h"K�P�Zv��ǥ�+_,P��3
�ʦ̩�C�W���
Ҧ&�x,Ģ}���:5�cU��[�q�2�qo��{��(��n���q��
XE�Ic �i�5F��J���10Q�Vޫ���u�X%ȥש9��Pc[h��V��N����h6V^�j��d��nl��������-�EGؐ��9�l�C: �b�*�l2;h/b.���jk��̱5�9�ms̶��R]�T��Y&�T�iЉM9�3�?/@#�C��D���:~T�ۑq%��˗��!e��ʦ������Ӗ�3��ۧ�-����ṡ���}&n*���.0_�)��	������n��@̃���5��Pז�d�n�	�� ��P�!eCݛؕ��}�z@Q�~\6���tY��4�Clw�S���A����.kY� � �9E_�����L�A��(L�Nl�E�+���M�XD�u5�yש��Pm�NF�L��UhK�x�ןm/!r;�y�C��\��N&s�W��K�h�����/rQە��@�͋�#N�ִHʎ!}����7�`�)a2\��	.fy��уH:�R�W܅B*����Y��􄉐�I�d&el�"
ܰ���MM���4���(Xh��
w��p|lt�s=�S��.��HuNr&�\�3=ȵɌ��K�Ozު{}�Y$���h~���'� �T�Q�	'ˠ���,�@�J����Oy*s�(Q�i�~�)�k��@�OJ�)'��LgBCj}�Wŝ�>�?n(�L��p�>�P�IȠ�Me��xY�V�b�`�|i%�)���$Kg ��0��Ya5��>�� �]nb�H�E=@[)�!b��"e邨jU aKZ��I�V��$��db��f��Vlh�C~�TQG#Ҥ&����P�a�����DU,H���f*�	2�!�!��O>`��j�)|-�<�ĸ��,�~�\"B1�3�Id��K�}�I�u�H�f=�/����u�H�h�!_G'��:���_�kGRY\N6�P��l�2�	� �T�TA~r_����N�l�F*�{A4Iu�+N�"4j@��I��ɶ���?P���L�ʭ*/��O+�Ƙ�5f}���ҳ�J�9Ŭ�tVa[!��;�LFq5�4���!�s�X�yѪ38p��J;`�WE������ ��t��v�P4u��ٝ�A��7�,H� �#�܌�#ǃ�g�^��A�*��{ܞ8u�@A3X�LI���:�� ye�Rd��*���7o�͐���;� ��q�m�Aq��<dF��8��&.�*q��۱e��*l�����A�(�~�1��E{��]����l;�L�,P�(��rs&#`��=��F"�hB�[ƪ#�2�)����|m��d�7��v�IW��ٍg���fiY����t�mo�`�r�����єt��dN��I<kX�d�H�4�s��H�>�Ҥ�V���e��"��u.D$�j�ir�����UR��mq�C:薝���.jOw�xz����jsg���B��$d]ZJb"=�p�/*�x�����c����>����ja�=�\(2\�p�Id�L{�&�~��/��'�'���hB����j���n���A�BʎvoK ����~u9K%��B+��Oʨ��zAbkںjʧ�4MV` �d��6g����F_]��=�?��2�W���;�W��޷�U5��I?��;0{v1�]DA���z<@��m�~lW6ak�]x�P�e0�0�=��^sss�f��+@@`�>�m�'6S�X`\/�E�&p�����p��t��`��2�p��~:v~އ`e��'�PĂ��q�7{��P��C0v W�!�m�tB�3;����Ih4#�Q^�� ��0�D>ME'��Pn�0�� :�[�'$�G��{W1YOU6gp�gu��Xi뇂��bN'j��l��f6�u-6j�8j3�{���cctH__�_�GvQ����� �M0�7v3`��eZXr�#Kwc(4��_i(R�%f	!&�������`A�R�V#�pwCVc#y=�K'�q��x�T���$�o�[�g�Hm��Y\�v�Ux�Xp�r�v�Օ��x�3 rd7���0��8�Q��'=05�e��\��-��w�g|	�S����hGQ�^�g������� y���͸�� �@�����U��Lk�K�8i,H`Ma��H=�u��i���t�Xl�Gf��&2' 'Џ4@�m�'mK7�bG����p��� ��Њ���G���{W�6��C2�YJ�I|�I� �CAS�6$���g�����
�X+�� *��θ��`����u(�@��
�P/�ksf���t0�x�5j��z�v\��5xm�~>�~䐮َ�Y��&���3@1�h���:�z����Мe�{�H���݇\�>l�5Z|�#G�S�"n 3_cC>�e\�ym�t`�-��BVӰ�����x�0� �0��˘�Px8wƴ��*J��Dg�
'�UulE�x+XqZ�����SU���i�xm�Hu#�L�Mc���g���md���a)���ЊS({
i5��ى��k�tr�!֒wv)4�pV6[��~��v:�M����>����h�*٘�H�th�����8��
P	i"�}��}J��\�f!�DMi��O�8t��X��YK)��*���gFiޘ+���v��mY�z������� �*͙P{c����j�
�U�L@���'.T��W�)' n����F�Hd��7{��
�`����
렍��𥎙��9�eڟ��Һ������>��w�4�z5L�c���4p�9U���p+v���~�D��q�̐�Z�zUɄޘ�Z؏�p�� �*�7�S�;0�Qʖ�U�V��7w�Y�r��IECrUw�!g�&l)�A@���Z���� ��
��^�q��к�t��gZ��)���i*��)��9Ҁ@Pt%Wc5����Ul�t�ħ8Q�8p�KH���G��J����Fiu{*�Y���v��0 y+�m݆;���K�����e�I�ng��:PLV�Y!M�D��Pf$�J�
Yz��Y&P��ӓ�5�,���`Ɛ� �07��!�<�H��t�������I�bJ���֙Aeu��h�]��-e�H���c.������el���`����mԷ�z{p�m4�TI��k�x�q<в��a
������m1��3�D��cZ{n��X���/2�hT�i*@}��rq&��9#I_.Qn8v��Š�1��������
7�q{Eg��гiڟfZ��@��k�J��H멖Iؙ�����Y��yt��� ��6�������h'خV�����w�۶�_��;xqx�qd�05z�Wv��	�[G�c�������V���;�oix��
@ `���<Ʊ*E`d7�������#��P���`�����{�h�:�F���9�����+��H��K����<�{%�\<�UmW�
�*��������t�(�?�b���K���x�8�K���d��Jt ��.p�����̃��v盾l��
��Q�}�"�J�G1usI0~��J���
�   ����,}�*pf��`g�*���ґ��
1��љ����`�i�p���\t+��ؚ�?�6,�8Ӯ���Y��v]�[5y��:�Ï��mY�LȈ;��w� ���'q>�O�)���oH���+:��K��m��p�xf����^r�ʖ�'Y�
gQY��v� ��4?���gX�Xȴ@ ��L �� ]�P�0	��؎}	c0a���	��	k�^�h��P���P�`�#�����`���F�{Ʀ�`�J�D�s��b
��@�@����@x؇7��*�AH_ܦq>���zq����8ż�
*j����b��y�a[�+�q@Ш��c��0@�M�^<����Lh���X�e�v���g\�m��-Yb;�Ӌ �$p՝�Ե�
`��<  � @�> P �P
��	���َ�	��	�p	o��o�Z�ٜ����1����
-Я�(��pҚ��0� s���	�:<����U�%Y�:mc[���e����7,�!76ԗ��wfV��E=�3٧���R<��gv��Ӝ��M�f����V��V<��+ߓ��ji�T�o�\���1�J+�X�9/Ѝ�Vv\~�
&��0 �آn �p���(N�c�����.��0�XpZ�Wp����P�N-��L;��ݶmƀ�C{�D[�����b��+����t�O\Ʃ������{�b��0�˻\�l� �&z��KhNb��o�{�d܄���	��{�@�����/���=� ۉ��x;��}��>J�B��@�e�Cх���w�����L�-~M&[؄ �>����	!��P�6	�@�p.��~U�C�e�� ���N;?�X�YdZ�Ұ��Ѱ�����Y����f;��,����hQ��8�G������`����]�{`��;��K���g�L`\�g�`|�����
���j�q�v���UyӢ~�5��š���3g�8PQ2�'��Y˶&-��N �.�./��< @�����$�	*�	v�z ��_X�X0�UP/�U����i��eM@�J.4K��+�֞�9,���݈~��ˡ	�J����FM�i���e�&��   ./꣎I$!n �B���t���_|xؐ�@�/f��k��`�Z��!Ⴣ#q̘�b�,Q���paC�)[Z|)P��/����K	3�it�	�G�D�S���z��V,��u��e�`�U���"�j��-  @�^�x��)�`G�ƌ��iR����y#�ʕ)c�R�U�&Q�4���U�rup���E_�H�,�4f뤭��N޼�������o{��I[��Ϸ
&���s�F�7TJ0e͞2�|�}���c�0` �� (�~���L�.t�c��o�;T.@z�b�cb��uл��<��4�,ri��޲*5�zb�%�ڎ<�~
b��@��5�f� ;���(��;(B�l� ��Ⱬ� r�*���0�F���-	� 4��+ �؃/=,���K�E�7�2-����5� $�&H��Ͱ:�+$�(%�$�Řfh��6{p�ͷ|�'{����3���+���q"�X[� ]��"�*!����V� ֽ���  �Rc
"$?`詂.����(pٌiA�
 � Y@��k�4$#Th���j&�Zʉ ����]<�!��BI���*�'|}+��bJ���S��k�ʺ-��Mrۃh��X  =�ӛ/���G.Qc5�3�$�L�61�l�B���'Zh�<��iB�^�r�c]��fpÍ�|�������XL_�>�<�5@�Hk�
�l��Rh<
%X-k�;�����&��=�Ҿ�tz�H�ibi��VO ����`+l?�*C�V��������h��t ��xQ�7*�8E(_��W+O�
� �XoS�h���Ё�����X�u�./��1:q��N��=ZV��5���
����]Z1ʫx��:�����醛p���C�~T?~�c|�>��!��/������h�;ս�_a�(��IE*�㝠@�_�R���G=Xz����g V��:7E��p���h��y�r�{���di={�փH�� E5��U1��uu�N'��Pl��)e$
a��1��	��Ʋ��EakT��A�O������Z}�K����021e�v`�,c��dN�B,vQ/��K-ZB/x!hH5Q�X\���8�1KE�2{��H����T��`M�)�Y~H�A�D*1ʩ($.Q=�m@` �B7Aʐ 1d���r�0�z�Jߪ��E���x�
�q�q�.s����D��-=v����F��٭�8{Q /@�z�!x�;$Y�C*�����-=�_W����� *8R[�/}�d.' D
�IC�!R���^&B���eX��
tЄ̉8{��0�#Y�&%���&ʼ�-�C� :�A2^M��҂���e0�Ժ֒|@9���#�
2�!4[�X���l)!6���}��4D�9�I+��� ����n�e Lt�eoX+���}�f��9�Χ�X�3�.�	�p�D#�.�� DW����Ҭdη�k@�����xpx���^
�{6Ȱ�Kzj��D���DO�J�h�Z�B�z����N��J'��0�DU2l���ؤ9`��^�i� �6H���
Vp��}2_�����5���@���.���$��4���a� IZ�Yd˛V
�җA�X/��a3k���Q)[jg0ZѬ@�|)m�����-$-|�T ��@�0��
V$�:	�~h��4�՘B����v���H�� ��	�ϱk[b����Gl^1.�bL��´�N@M�����- �%�0z�>�!�[�B�i�f���oӤi4š�Y�]K�6�a�~^�2=8�������І�vA^�B))̅-P8Ŷp[�F�d�Gm�ci8�sb�.3El�=��<Co(E]_�좗��rV�X�k�����=.�X��C�4�O�q+��9��X�=��"먥4��e�|�o��^I�V�l�4 -�]Vx.k��H�涥E�C�{a�:�3iW|�oR�R�b)��H".x���~�d���2����U߫' ����R�Mt胭��k;�A�n��?3*|w|��4Jb*d^*J��*0��ʛ��t�-ls��.��/B�Wu�mW�X��"�;��l�ɭ�"Z	̐���� �"�x�%������:lK��/��Fo�p��9.  -H�E�[sEl��+l{97�L���C��/qDG�����v��}`MI�:��K_U��ִe03~���ӼF� ,`B��~����4������G��;�=��������h��RdR�(��т,�����c���� �q�Àl$Ʋ���	�VX]`�,���h"ii���{"ǱA�1��Kg�<و�V��&=G;g��a��2{	:��A��(��4����9����ѐ���z�l�&�Z��C�N8�q1h9I��6��
�����0W��� � 肖�4�B��P� ��EӮ��"+B������?�;�@8����J4�5i�*?튃��0�p��h�;��8�8ت+��� ƃ�i��c���>���ZZH�%R���)�,j+�)9û���'�2g�Al�V�d�X=c(�i �^�\��nX�vd[ЅX�V B00���p(ߚ�+�< ,�H������K��x���6�!,���O�.�9�8l���/-H���#볢�x����+ ?P;0��������D�.��X����9�)�=�5}����7��M�),���;0`�
����xEY�6�EY��,��*�����	̀���˹�@_�;r��I�� 8]�V`��G{��m��'Z�*,���	$eT"0�V�
g�����I�hߘX
��Z`�nh�v�Δi�KZ��X��dX@M�Xx�V�����4	�}	��B��������&�* ���	�I��k��85�CŁ� 8�]ȗ��(-��)^C�O;���*�I�I����?4�$?8�3��MX�Ģ|78:5�/�*D���b6gZ�9�Ї\����J��48�J�c�X�J�)��9�*�@\,�t�߬P��Y�[�x( uhZ` �K�H2�!-$k�$���x�8(��-:̜�i�%�h�X�^Ґ�]�����Ϭ��[��ԄY`�*}մ��8�$�a���ҭ�0��;��pZN�XC�L91���/�(%�+��I��4؂+�J�ADP{����I7�5)�˨`)��9(�W{O:@�?к����뿖	�IBD����8�`iW�Pݿ�EC�J�E6�ʰ;�|8��Ԃ	䀰��<���x�k�~ȇx��ϴ�Y��v�uP�y��P0��@P�8�А˹��$˧�w*��i��d��yx�:��^:Fd��Ϝ�i�Ld@c�y��Y��W`�V�.uMج�X�����޻��̬i�
���Np$�aC8O�����7H5Li��l��B�NC��B�ϖ4���.= D�DE ���K忖\ϭ���< l�D���Cs�H�*Ȁ/Ȱ��a��L&��q �ݿ�J��J]��uPY�JY���ɘ�
� 	��h%�C��f8Xc �G!��M�f}�up�w��}x� ��v��v,]`�VЅY�Z`�V`�v��((�(șV؅�<vh�ȇv��vX�n���������]H�^��:i�4�� <��8��L ����P�L��(���A � �ᒒ��8|� ��"�_[��� �m������-��K7����4�ߣ���9p����YC?�M��P�D���ZE�����d��UmBu��ݿ\�J[uP����Pm��5�}P.DЉ>�P{(�C�L�5���ʇfmV�aĝ� v�v��zX%^ɵ��w��y��!8ݜ��d��']���P]��V`
y1cy��y�x����l)�S,D)ɑ0l��r��3��Z��� ���.1�1A�%ߪ
?�L09�\��V��R�B-,(B��6�Z�ʪ�=J�<0T�����k�:Ne�Ǡ�3��=�~��Y�D��BZ�m��� �Z峅P��3�[�M۩�J8 VYָe�}�-��Pf5Q�adxd�!n��a#F�t. g- *�ɕ�yh�����!(* (i��٘zX�F��̽Z(�G�46v��m�`�8�B�L ���$���	�p���ИY�[�����.�9_G@N��L0CЎ�������c��,�8��ʪPOC<����{������˯3��q`�\sOaI�7���+D<tJ
)!x�v`Y$��d�:(��J��۰�X[�j^[.p���p���Ldhf�W`�"N�y��{P�tn�b��!�gy���|���B��K����y��`��j_�E����8����4����:���B� �$8	YH�mP�MF�A,nʘ̪��A$GB�OО��YS�S�HU���n���򩂡�>G|TE#���.�U��pK��=����?X�e9��?<(�Y���`D��ϮՂ��&�h�U�D�55^�fN�tϰ+5�fa/���qɅ�h��-\�k��v��H�y�v�v���6ǋH��:!�& �X{���]���ebwd�!�Z�ϣy4�M�̖�����ţ��X� ��@A�ދ{��IC�.�B�K��I���H=�P����!������;/6L�L+�>��+�dL���sB	*!��>����R��X����P��$o����V�a������۪5�Cc^c�\��8p�K�tW�f��/3N���o��0�A�qy�/ލ����1�� qgu�Gq���+�g��^@
nсf؍i��� X�uЍܠ]�Pд�Ņ$o��K@�)��S��� ��8ǉi����/Gd�T�N����e�5Q0���P^Q���8��v�.���b� X�f�ӯ���3���>�m�5Y�ZPTjS�2���	�ZeQ��:z��:Y����۪�DR�T��p;`�	�}�p��pZ ��5݈�H�^�ߐ��`�a�����` Q��^�			�@��`
�\0������v]j���.�&)��탑r�Ȏ���a@h������)�0��N�1�H1����C�5� �n���yҽ��� �@	 �P���Stj�?�K��z9��M�;���D�K�o��$��04�j�n ��d 3.�kPNNЄ�< �=�	/���P�J^��K��;��PhY���g��uҐ+H�u��-�7��=��٫�p^���nd(oc�:^� IRd��t8`�C����x�<��XI�6M�4a��"ʪ	�,m���͛N4H��@�+X�p���K�^��,+a��+N�` � �;@���.]r��R���Fu���R'-o+f,ǋ-X�l���E�6Ez�����ϗK�.}�4i��gt=���:b�ۦ5j}�铛�k�����?x������=o�<փF��j׶_���h3v\�q"��Q�L�Z��'N��Թc���8����?�q��l���v(���qr�)-�@k;��-�T��<�0#M2�Ԋ1� cL3�#�<��#��I��C���4�9J�c�4&�Y�$���!�`�K�HĚ8�SJ)�D�p$ZHp�SH夗^&p�XU� 
\y�g�u�&���y���U @�   �� ����_����a��a��9��Xh1�"�	����h��FRh��'j������u*f���
q�7�;����;�Gq��Ï���s�8��zͭ�!�]7�1K�����ר��{��{���}z��|��Gu,W}�h�&��w	'�a�ɽ�F2�e�])R3��C����1��/�L2���c��8�c�� �.LaI;��C�D�4�v�HǼ�ZJ�p�dj�V���Nڴ��Oyp��Q90�UV���Vkf��P;�&�/|Ђv�' [s���z։#���W&|�u�#bL*)��z�EZ��t�Jujkq�Z^��Y�wI&��*h���
����g+8��c���j������>�������,ꕃӌ;�`jߍ�p�(�v����9x��:�)�n� '�;�ɼ��ػ�A��3k/�ix���V�%���x�Q?��� C�#���e��H�4DR[�LV~Ѐ\I�^��3�l24 ��� �Ԣ� �-iUiA$����`�3�b	Z�G�V l]�` � >M�P�)�_Չ�t�Yxä`����m�!��ĳа�4ѫ�;=ı�u�[ �+}��r�r88�D��C��]8܁�cm�Y��F� ��f-���&ރ-m�1>�W|�	S�1x�������^~L�&�?S�`-���IT���T!Kh �H�#idj.2&%�B��	�p${�c�XGA��y���h�}`� ���b������-b!V̂L����$�\!��(� �eX�8H ��t��6�r5�-+$['&1��01rx� ���s�`�X��/~�
5�1�i@�Ò�S �dW�~�c��w0��}�cs�y�>���p���"���jlC{��;��/����Y�҃��f�[rܝ��sG:�bxx,����k�yȫĿ
g��3�l$y6y>� e�Q�K�7�UoBG�H*��uD���:�A�n�b���2��u�U��`F7��\�B��`cX:ijH;ZϤ5��,x�/�?$%а�   ԅO�Da �N��%����������X��nj���jL'҈jU��H ʷFZ�#�6���=�'MC����m�X�j(8�Q�p������Őw�-=(��d�k�Qw�{��0�<�8(�x�)��H�=�]�ȫ�'Y��%�$ᣪ%�J��5����e`�`ր���4'u@˰bD+��:�A�v��w�E,b���z�.�3`�Z�@HA�I&f.͙eqZ���7�^���2��,U�&'0�
Zp3P��0����(� �pRk���Ԑ@�[x7���6å��
g�BTC���?��zȜ>!?�SrP.v�#�7�9�v~�;����|([��Q;����m�����S��W���/���O� 5�n����M�$��Ԥ��>�lQO�<�:�z��Lz����W�B'J%P�""� 㗴��@�ьnĸ��@q+X��8�KS3w2�d�S��2�/�C|.9�0�����-�j� �W�˳d2p�
�� ��ZO5�l���w�a?�]4�� hF���@r<��w����@�Ԯ 8H0kr�rM�W�����%j=���5�:Q�L��gi��D��j�3�jSI��ׁ��SG�22�0�4�A2`G��ф&WR
����%lg��x��p)LPQ`T� [e+'ə����#F3����Z���T����)z;`����^p�[�o�u�jDB�$t$��F�$i��W�C���8��!C��:,f�g�(]UUR-��D�<�����x�Մ|uj��&��:�E���j3��$���Y뀑�Ky��E^���➱O5z���%�@x�^;�����ٖL�P��H��Ra!b1 ���Vj�X��Z�����������l�?ɜ����\&Q�#5�?1݂�Dl�@!dâU�IZ�i���C �����Ş��ޢi��I�|@�����L�Q�0��$_�ON��ы１��&d�H@�i��U��Q�H��I��<����I����ITX�0	y���ܟ��'�)�mi�RH�XK�ly�5+�+�DE%b6�3�	)�e��e����n��W��g�i�ٿm^�� \��6\#�@��F�U�����qU��G���A 5���Y����ya��-�d��D�W�J�S9T�'��)|B��q�(��(�\~���N�L�`�<N(^��Pg(R$���������&Yج��Խ��E݃�����0������y��-�R/����yB!�!�d��R�Ib��T��\��P39 +�b�)�Ɇ�H�>��	��}���d��"������Z8.eil�䙄S�
���B6�3B�1���a�>��A3����`���4*FܼK��B¢fDϖH@
� $L��7�{�B���]��!���$@Bl�6VS��N>�&M�RRd*�������ڃ������.�@E��C��i��i���xC\����&_�-�[��a&�T����q`��	�(*a�|"h�"�DO�٥�b�y^%�����q^S�F�}��9��E�U֞��Y���"����C�����U�V�V�`�y���!E��	��%|��"�(��~�((*T$Tb�Ě"�߭�$�c�@z��e�U�	$��r��G������:�Ç��B��Z�Պ���ݕ2 �+�0�����5��]o2�3�_i�J���]m5��$���+2��N�Y͕��9�om������^�`Z!`��^1a�G�Cc�#y����dh�~��l������=5�d�MV
�	&��y ��e���g΀$d$��	�B�����R��An�$E�@Z'�*���ŠhJ�4̃44Y���
�4�3 �2�^�1�B�ԏ� "�Nn�d���&2Y0�ߨE,�����k��	�n��ߌY���I�
��D���:�=R&�"/v
U��rf�� ��i�y������A�g�e��`ya�c��@F�qAF�������>ޡ��_���5Ug�(��?�OR���fl���@x .�a����*���=��_�h��ZT��
Є��@,m+,��>m4-R���R
4�� bBE�&P%����U���W� 7���Ax���lQ�k|��Oz�e�d��T���d�w���a���a@�k/���� �ݙY�����C7�����46
=���\TmB%%$ǦOB��>�,?R�B���������@ڵN�,� eF]IP]�
䆞��$�q(?�Z��8�"jIp����L�loe�_�����2U<�L����A�Z\��A.�T�����r�iy����+�"�=v�^"��HXƁ�b���H,ꫨ�`!(���yHx��pC������0�^��)�p���/�_@���q*$9/�l�$�I,d�!/�l?F�B����0h�],Ho�ZX��d��>ְd����&5U����J͑M�f�FK��fl�l%�PhK�Y�Q���n*�&Ңh|�,i0�^*�vk�-P�F(�@�b�(�g���zF�*�g�PpÙ� k���FpY�y��PN8�t�'Wr�c�-�L.
d�q&�I�"�ʼ.:�f��c.�*���"-�HI�@�UT2P������c�QݪN]�JXTX� ����|/�ɒU�)I��
sBSK�3��3�or��q!)Ý�`��Q�Z�	sA��w !lAt+lq�!�� ��>�A��.�Ţބ����UigD���jR6���2n+���An��xA5tí�Y�v�1>����d�0�QN�":q��,�E����e�l��Қ���'���JX!�C?�:؃4CL�RR.� ��e��913KO���`���<����Yl n���8�nR�����5�@,��/<��}jec(����b��aˁ4�`��w�x!t@祊������"�aXS�,k�CrlBN����� �"|��M.&SíhQb��<��̯�,�/��O����j�ϚhGR�֚��.#�R�$)�dA���F� �<�:�CB؃<P�U��A"�Ucf���~5r���Ȇ��p.~('���U�m�c���1����g��^�t'���^���a�^Ӂ�BZ��q��ia3v��� ��
T��-z�I�ofZ��X������˝.Ă"T����ms�Pd� ���l�����$4�z�5����j7ت�g������O��R�.�dwEl�=|Y���zk����i/�>Nϯ��pâ"��cb���0�T��|�\�sl�Zs��(�ln����_?�~<�d,F��;�q�kAh8"1��z �0�@�̹�ɐ���,����%�!��j�p�@� ܂����R���$7��a�.���@�u(��"�rO�f�x���su�f���,�@f.��>t�vSD���;�C>�C3P���������j������"��#.��9qƾuEa������� �^�����*xc4�~@��=l��^~���a���Ś�d�T2��|x�/,��"W9Xg6�D4����J�bݐ7����V9��N�kdөߚ'�UK3Mc�3��,�t����k�
�Ym�Sk�iz�CxC1��x������.�1J��L[ ���(0/#TY� �qAK���W뙉Fh�| %HG%�<��_C:��6�{��c[����!�
�B�Lu��2�X7�U��C3xC,�>�3|�x��k8&��ʰτ;g~��S��+�-]�R���yS�.�t��woL#���325���
S���ņ�485��Ê�h>�{�?u�K0����z	M�ҏ^/��Z��Y7@���`��%8���Æ(H ��u�x���⌋W��@��Q�4i�c��-.a��g&�8q��y�F�9o�ȉC�M��<{��"B��O�Ҡ�B�3N�����p�]+�n_���޽+wM�O9^��t����/`॑���+N�؊�c^4hl�K�E���x��^ʔS~��d�0f,�ᄎ�:J�����`�;*#]��l�4�ދX/cݻU�E�ط�+�1S�_s���I��n];{��c��y�y�v
�c����i���x�V+f�ށcB!,�!�4��88zJ��L먽�.RH�R�A�H���p#;z!�`�$�LN9eUF)�>��B.�nj�E��0J':d*
��^R����:��RH$(2� � ���������w���(-|���7��@#� [�" J��0�2[L��.���x���:�s/�[��b�*+�`k�=%�6�ȋ���l��3����n.��d���a��@�n�f�['�S��|�Ǟz��c��e�]�Y�Vq��e���˓��8$4���0� �>A�ܨ� =��#^ (H�J "�&��)�2�(#ANp�G2ᤔ9Q�L>��>Z��Ew:jE�d�1G�T�A�� CH	�++�����%�q'�'��gu����`�I<�2�V0ͣ�`�Ae���0GKY̾(;,7�,��M[�
NH�h�yP:
RH�	}�n�w��e�XZ	b<�b���Ԝ47G˔R�N3�8�t 39u��T�~�igf����T�ήU{��n馓Ǻx���<g p�`ဃ+,�`�f-w�Y�����? Կ�U��o����?R��N���CDN2�w�;.II�;���@��C1�̱_;T�ю;����/�ȇ��5��$8��$� ,w4�T{��ǝr�9J��a�d�B뿽����3��l=SM�ܔ��'4��^b����{�@A�P��	d�(0�;X$����<���~��c�X.ba�/��r��f���Gf���f Ǚ�Q�A3�����j��[;�����9Q"��:�a�R�#��V�@�ټ�r��8�9�i�YmĜ�<�8�1�V���8"�P�^�2�x$$��%6ѻyq����#.A<1���E&F�1t���J.1�;t����'L�	HpA3h��2$�5DjWVpp��fI�6�+�$(��NB��М�e��� ק����ic�<����ѓt��hp�L�>�AuF���p��Tŏ��~t��N� ����+��dֶF�Fn�Ob��騃;LDb���6*ΊM�[��%d$���{N��].Yi�\�x3��r��~0G $!iP��E!�����!�/U4r����u�J���FM*.�INx(��W�2�IJ� L�+�� ��R�3.���w�T�5�`L�`(;����? B��e[�w��ô�}��`8�iZ' B5xA �,:'�wƃ����	�A8E(ph�=	&��6�{i�pb�)D� �P�B������o�i�D�ê�R�ukU��D�<�m�8�J�,�"�l܂���,X ?Ȳ@v�8�#1��I�N�S��k���T��K���d���T��˨�P�>�"PC�j@�R3�B��b���:%}�#Gу��b�7����(7����Ɂ2Sclc���l?��� �&��d/�A#\��,!
Yx7y�c?�	Mr"Z��=l�+)��MR�!D?�!]��:�F3�C�WU�j���i� +) ��ޣ�&K[�v��,������v����;s ��f�L.��na�=}N��5x(�h�&L|E2����);�_N���35�%p.�+H� FK�8�X�8����5~'�F]v?9��'h�Pb�ɳ�ŉ�[1�@Y)F�R���]�� ��'`�r�,(
MHE�ӞtpMh�&K�h[b�����-��>��]��ua3:Ōvt�;S4����x�#l>�oB���� t�Ŭe!�sl�sKӨF�sw�@҃d^�>�e�|�����3��C��|5}H���N�tT���P������V_�
��4�J�Lc>P%����;v�k�%4"6O�=l���5����9��@M��m_d{�8��~UӴL�D`�:6B��y��8�5!6db���{`'��m03e}!
�X�.�#p�Ug��Ӆ4ʑ�(��.�[��<���M����=NҖ�Ѻ$?�K]j��m���W�һ��)�	�'�	L�K_�T�I���Q�+��9V��tm$+'��A�>��| �ט[w�1��Y~���ڝ�|�!�yy
HM�'�6�nRz�1�-6 1��Q�-2��6H�x̲x,��`,L��<��Z��)��J'�����
J>�`v�p�9�h�$���d!������:���d/=Z ��LZa�L��FN���	�k��Y�b��0"m0��>�V�Π�>��'�����0��0��
_.��_�paV 〠*���f�]Ч�܉���c�a
�����t�~�@`�5��0 2��m�O�������n/`C7(�N�hۀ��2H�tL��	��G�L(��&P+[D�2��*#*�iv!
�an!;��:@�ҁ�!��>�A�F�.p��SXEd+L�2%n8�:�LN�x��hsT�І�*d�`�Xaa�e�.0$@w>!���߰�|�4!x���%x�����Z|5�g+��
��.cL�
�c�A�nB.N+(NKؼ�#x#gnf���M����J�D��f�P�`.0/+��z��cQ�D��F�(����`��Ek	1z����f�9�&,��9�VAP!ȁ��ʡn��C���!ʡ̦�;�!4x�L�1	O`�r��
����TN�p�Ώ���"��b@�o>�ڋ1)��!;M 9��LI!Amw�."a0loZ^ �> b &5&&}�
֊#}�c�Ad�`dB��B��G�d��� �2K1�ŒS6��d��*%1x��L���I�v�"�J��l��A�E�f�h����ͼ��,��c+�`��d,mF�9��J�C���Q-S�AQ�AS��!��T�/N>m$�T�R��v�YV������B�����\T\fN��Z�nz���P��n��j!MS!�ʪ(!��ef Ώb�*@�a�z�7�n~���)G�EP"Q:5�>��� r��,��L�5 x@���2=��s����,��)D�&�r&'nb�Tpl���m���>�9��ohao���9���@K�@=A�-S!�A/�2��� Δp��r̨
�sD1��� ���E���{�*��֠.�4)	������,���Y��4O�Z�� ��=(����[���ԇJ=2����&uF�u8�Bf�M��G0�
ˣޔ�l#堦s���;#���rO;Н@��TH����&$o��9UЛ`CP����a����9�@��T>�R�@	z�2Af������h�dU�"_�l�Z�X!�T DTК�s��D
�LY<�*�$"��#��\��\ �(�r�(I�nԤ* �6^����/[M� ���1I1�*$fH�<@}2���{K�A�@bg��p�-:�5+/pg��,���`��6~`<~Q��LD#MS&6f�L�`���u�s�.k���TS5�"�_Z�KG��d>el���������AZ /��Rm���\6D>5f?��a$J;J|�Uz�<�`���ZA�������V������iG*�v��y�/\`�?�>�ɖ�̶l��*�GM�G-����6I[�&����{���No� ��o���i]��7����i؊"�D@`-5Jc^�	� ���,�276l 1��j�AZ�
���~����AV@�؊s 6nO� L 
�@H<�����O�#sWww�Eޠ��c�z�xQ����A�aH�}�!,���S��^��`V���%�:�B��:�a	h��v��
a����nw�Ah����`��ei�V1S� ��A���S���e|D��B�C�4�x�����X��V_��Z1X[!�&����hIh5�������,�K����ݔl��`���(r� �X��^�u��u[�N%���xP� �B�Ap��c<� � ��y�@x�e��'M�	�I(;�.+����3�VP�A�`Pyw(��ݾ��qlh�|{�{`S�A���U���WUl�o���n�V�S�_�>�Roq4�|�7��xO�tm�
�}�!��^Ev�b!����p���zT��zȠ�`�/Ѳ��B�)��eX.Z�%�� R�o�+ {4���.[3��>X�.nh�0IęH��ocA�vs��K���8�`����5N���������uQ��Z�������q9���A"z�'�VB�����F��������KH�l7��q�O�K��lQ| x%���� R7:�|�zAeNX�o�;�!TaPAR�3x��U���Re�!
~�PԷ<��}ߗT�����ܷJٔ������p�\�f��iah����&(]Z��!��)Fn�(� P�ƀ��l5a4A�2��5H��� �p��	l<�$|J�bv�}�������E�"(���癮�`��w�B����u}��Q���BYE���{�!�!�۹��,��� h6Ƹ�)�I��v,�t��s��ɼUK�ysQ�r���Q![��!(ܗT�d}i�Ǩ��_A
.���!�lT�GQ%�� =�L}�cƂ@ֺHŸ�cT��}���M�Qܔe���F�_��]�[P�2�|@��F�	*s"*�)܀%�@�/x�J_�<���:�D�z� ^BPF;w�tf 8t�D�&f� ����^�#�"�~��q�(L2(���<�L�����z[�G@������B�&Vy�7�:��$ڹ��z����l��	� �P1���:PN��L��a�鸎��Z!?�`��waW��Z p�ZVo�A�A���Y���̤��h:|=~�K	�U֬<Z x�(��ZAd��?<�E�kr������m��i!>���}!�u��9�5�0*|��S���9�q��LP���W"�;�ZBn�'� z�c3+��֏�t�4b �N�����I�}ӄb�s�$律�Ba��Z��e|`� �m^��\ϝ �Ye��-��#'y��Y{ �e�fۂ� �#P1h������9�Q�~�&J��N��9���1�D͎�2�+e�i����7�řϔ�Ǭ��~�v��gO�4f���z�T�O��𽲵n�4�Q���Ϟ֬@���E���:v�qV�@����U7oQ�ɓ׬�2c�bk�l`f�	�E+�+Z�f�jDB
P�ܠ�1f̠�gϢg��q��8mZ����5��qങ'��,o�Ħ�9p����-˙k�#,�du2�A��$�p �D���s/޻}���c��F�tf۷GΊ�E���[:H�DN(QD�4B�F�Ob��5��NV���y|bxؑ�v��e�:��
)
($tc
)܈D��MTQ?�GG��T��]-Mӌ3��4.2�DQ��SO4��Na� �+����4�HS;^YEUOW���Z��ZcEgVX/�`�
X���A��!��S�8��W+�23a�!�Xb�0�Xd��Z�	hF��uZi'���l��G��[kXh��p��f\k��*�rrx!^xB����A�"�X��	�e�v�Ӎ>�e{F��ǒKz�H�q���}F�
'���:F8ᄃ	dc�E<�O>���y�ܤ�9�XG�w���#�Q�:���L�	�@H`�<EDF�`�G�Lv��Gp���K\�܏.[r�B9eJՎOL�a�0C��)�S�<�W�ع��V:T>Q�uvi����@�q(�`B�[�p� ��J\�(�L7�x�`{�bL,���
d�J�e�mfh��V�3��!}��Zlm�z�m�j�[o������X$���������v�}p��y�8ݨ��z�`�ކR�HN��d�r����Av���CP�E$�����d���!�Hb�wءFY��#6F`c���Q�+[d}�a#x��x�?�g;���h�wH����V�f0��C(L��^!�xăMG+�:�1�yO+�>�1��M�=@�h��"�Jmk�C� &jm&P�
V���ł�[b+�F�3���:�,s�Ͱ�32x�g``�@{x��c�`�'6r`��ś丆5���x���l��YfЊ&h�Z��ݶ��ށ�{R�;�'�r�"��-���́�RՋN���@��F@"|/	� !S�Q�rX=�� �'"Ylbxȃ|0�p�c C�	�	�0RD�#P�H��6r���%�d Mr��������aA	�(|�_���p-k]�Z�R(���Ef	���w������@A��C1�lk[<�жq@��V�J��dq^4��^ �V��#}�XD�R�a[Xr^S���&?�aα��F�smؕobì� ��^Њ H`<������t�T�U�����X���#:�\�dt�"��_�|�� %(		je�KYZ��x�z��\��k�!:f����юP�2g�	�AD�G�&%�Nz�7g�����P1Ռd8�$���<��ô����O����I(��4�"�i�p<�!�l�1i��@� ZP�pm��s��6�r ��BC�����ьB��Fr]��	DCTɝ`48@iY[�җ�Xw�N���-�[�B>����/`R���N����
�!.�f�!y�z������#`�JcW�xI�za�A^1W�&�AX+��w\��X�>���[~�rND���;h�0�D`��cQ�x8�)[YD����j�q��>>K]�A�-�P�a�i�6t>F2�����cR|A�1�r�xJ赼��o��ga�e&]t��r�p� @��kPh�P�n�(݉NT�ݴċQš*rCz%7��}���D#� ��y> .,ˌ�
P*�p����B�O���|����w�AnG��O�N �z����Y,��8kQ�"��x��c��+ފ��{�[��^�rX�����6�ld���}'�Z���=`��r
p4������*��'�!h���4{��Y��]��Zg��vF0xC#�@��'B_����Wҧ��l��S�F�� �дAG=P�Nw���:�	���^fTY�t�G���^��[�`�N�1h0�k�Ġ3�9j�c���+,�ub�A:`�rj�����$^�=�ayt�,�B��J��%P�F[� |�DX��@�$$A|:�G��Q>]C�|0r.������i(��\qT���[m��Ię��2%3K��Y$r�F�{���i����<�!s;�6���s0� �]��y�F��<����31j(/В1h�h�r6�F]�u>duӅ(�Fj��6]G�aw� �Cx]�j�CRj5_�k�v�kFT�1w6�*��Q._��k!x`��*H��� � �tH?���a���H�M���b����Oh%o��C:���o�z��N ZZp�C��	��W#�!�#Xx�> ��2BeR&e��X;Rp^�[�2� rq0@��<l���f>�{�mf~��Z2�~0G�2�����C�0ɀh� ��Q�Q�B�6@=��6L�c*`P�RօiU�i�6�uYGi�Hi�.P�Rv/�^D5/���!o�x��jt�6w)_�x�r�!E�����"3�+�T�a���� � I�������'o�vC�(��=0 ^腲{�w4�y��0p'b�d$�{$��`��1��7�� "�?P( ��Ѵ�$��z��p�0�rW<3,`{@1w��`h�5�0� ��~+�Z4w�@s���B�����HB5�dcR,h�s� j�r�"���uPP�8u���V�isjcwQ�*3�QC�^'�G!��j�krRvv�2`�Qa�'w!�3XT2��3��<������m�� �����0Q�m�S�i�� i�l!-�� ���߃��%���P"�|��'"
|G��"@�"�`;�?&P#�926	+�qҒt�.I�r�}p�5.�[��3��
g��7T��ptx�xsU��e��)s�Z�8��7Vi�= gQChC�D�86=p/��]��6�\�������iHuZ��vE
�85�px������^�1R�AT2@�-
�s�k�v�a���kaA��x�@�V����T���� � /��n)(�RȂ ��3=�zp� ��o���3��/��� �T� | b��	|v��eP6/b�e�i'eTFe-�27��2@�z�{�P����RR�p-Q~{r����p0���@��� s��~wV%0W��\+�~���PIgS����8z;p���
ZCd �ici��Hi��	�]�u` �5�eg��Ӂ��!�Gj�w���⨍���3:4XT��k�6�L�8_��k}��B/�8�E56�G/Qx\�-d-\� o��� �g �ɛ��4��c��oЦ�D�w����1�1� �(� #�|w�3b#�4<������
�P8�P��Lyg�:�C�w�-�O0C��
-�?ih2���0OYgP)�0\Y�X��~�J��8CPC\�h��&��+��0��6�&]=d�塻ȋ�6#Z�!�w4��ڏ/Jm����z��j����ѣ0X�-*��Q��H6���wU���g;@TQ�c�P�Il5 �ǅ�I {�y4{[�Ey���	�:"j؆�>o�";"2}J)�#) J)�M@�M��A �d����ɀ�t��QKs�g�A�Rr��
*�Eٟ��� ��Qyg���U���Y����~a[�����ꠔ�taa;@(���` �iY����qYi�� �8vk^#�1R��*��]D��1T�A��`>��5x�ۮ���'�*��*Rj�2�ccC�k�;z����Bz�F���zq�zo� �K���AXrf���p;|#��i1CX�A0)`&-�t�i^i���t����h�� ���TىQ��𔪅�w�hM�̐T{��
�(�`��|ȫ�R�� �r�g�U	��h�̫���
���o[�/�`i�PYg ��p(����˸�e���ʮ���1p�c!�ژ�R!u�@ڣ`�*4�,���J��x����{�X*����匥W�C�J�t�J	���@g6F�H�؀dc"q*�'�ip/.2�d����h������s\�h�������H��Hg�'���Ǆ�6����e	Ry�G��b����Ds�0�P 8M�\�ǁ�K�h8LFz��V;?�m,��2� ��;K��¬�� ��ʣ��^�Hk'��'(ģј-�%�̑����,V��=Ī"�M,������&/-0��qoR����ocZ{�a�ĳ?V1c1 �dz �+�"	��u��=ѯ؊*�u��(��Ӱh���Q �A�Q`���
)�P�S)�����绔�*�Y%)1�z�\��^I��4�	�1�6���1E;��w��ͬ;
p8/�Y��&��89ͬ̂���ށb\]�3��&Lw�K/[�Eu�Ex�z���׃�8p\����EM�~��<�T���=��wuf�Xq�����bl"I~����-�ͫԝǣ������Bp�U)��� e	��[ѽ��*��Vk�Hٟ�Ks%a�����gkԘ�1(�x\�i��&���1}W��`
0�� j.���9Lw����L\�&`O(=����p��)�Ƅ��:,�b9�����9�q����t�"A W��� �:��5��H}@��>'�)�i�M��������M�J�6��y,�^�M@�@{�sc�Q`M`��(��L�������n���~�P�z��{�<�Ī->�C�
'��A�[z�e�V���R����m�h.�!,޼v���kEJ/���*͔S���i��m�X:�����
ǭ��Gl���mݕ�L��d�{R�o�� 7�ό�"�HT�����!�o�h��p���)��^I�f��U����~���[yV�@���U��{����������D���m�[�����Y����<����)��=�-Pd��ʝ�cI��z�c����s8�(�e��ڼk&\w��0`���`��	��	�@	kps�A0�o��<�	�G/rC�*�7��L����Z��c�"�b����W	�o�m{����a�x{���1�pb�v�e��+�	�']��	�ٯ~�[	����?��s��-Ε7��.�
�2T^ �����X�C@P��z�)����b�+`>����hC�� �ZĊ�c���4z(T�C�:h�AC��5j�0Å�TP�d �hȠ�%�+a��q��/p�8QQFF-n���i�L��&���(P�(�:CH�#:�X1f�:N`�iS��^'�L�FۯhcVD��-Z��x��a�� N�)B��aĆ�@s�͝�x�ܽs�N߻���Q��;z��!�M� ��C�;b�v��m!;���-[����/��[H��i�-��8x��yS]�C�b��UhW�	2��!Q���Bh�.����-d}� +� 4���� ��řz�i�]��m��Z��Ax؈�$� �D�$h��d0Q�b���_�'�xR��b!%�S8Qe)L2�$) 99%�SF��(�X(�Y"�a��	�+,+��.�&ˉ(�2�.�a��h�A�k��3LN#�,�u�q'w&��u6�F�>��DC��"t����r� �f��6�n;�7Jo9�
5��; 
��礃��8ڈ��Y��=r�c9�H�`D9�CA�D�o�㎋oB�)d����� �Ĺ�@\������-�(�h"�J/�(��.��#bp�CK�D��UQ&�$��ğz�)�`%ǢTQ
�L>rG!�<E)�#������)K����ha,���K���T�/L�BhR�mS�]�0��:��"��>��n�?�܆=B3��Fi�]6�C�
��J/�47�.�ME�T��ӆ� ơ��Yc}Cֺ��[���n��8�x���{-�0��B
	p�h�k��w��2�pܻe�٧��Y�c�iFy�9��bVo]-�	����Ѕ�M
Q ��iJ���\�)�xB>-V`�Ɗs�G�q$�R.�G��?��?b3P�����hnYK���f�ȄI ��k&�1�Q@	w�BO*��1:f��k@C�樉�!����*@Jl��m(�6��S���n���
��t��Y�mn.�N���7Y���bUu�� r�]�遴��}�p֡�� F�A����}�C���>���e�C��F3���]�BBb��Vl�!�`$s䝈H��HE�ZQM6�?"/(� �R�W��e�b� �	�!�HC�X����qK<�R��Ҳ�؅L�e�Y��2�%J@�t�&	�A�5�ѧx�2�X�:����� +�Z�Azö��`,�1�fÚ ��?�Ҕ�.�A��Rlk�n�֦��po1����ՍUm��svXCX�:X��:X�"
!���;�D��Spz���l0�ը�5Ʊ�zxc�zW,�ƍt�#s�c�B�/<�_0�	:��ha@(d��T=GhlH����46�L��8bdQ�R�Od.]����@J���R�2��,�dj���
;�2���
�@�� 
A`יRL�̆L�BH�>x���+�!�V���|P>��n��6���P�}��Uz�!���W�
9<g`+õAo��L�ϫ���Dc�&+V������ئ�S<� |��p�!@ъF5�9��w�Ha��`/�s�Gk$�H�hdR�����HB����۽��A��D&d������!
�T�&�S������)��/:�����a0uZ��4����D�A<�iƦj���Z�тut��X�=�я.FtŠy��l���]q�� 죅~�K+��S�o����0Y8��U�dC��Y-`��e3����wB�����l�(�H�o����=hAz�Ø����`M͈Fg����ѣ�cEqQ����+ʭ�W|���I���Q'����I��X��Fᰜ2Ғ�CO�T�w���(s��pB"��=�R�G���/�;�-�t ��(�_��?f��6�p6k�bS��k��،��p�ß�G��A��}x���G=�Q��-vۊ3ET�`۠F��C.��U6&\�0�76Ѝ|����YOr���F�M�n���ϩN���;�؇z�Ι�Г7J $!"�p��� �5!s5i � ���&L1=��zҋ$�I���{CUG��g��i'�Yf/|�eD~�m�w՛Z�_�̓&��RW�ԭ΃��@�XA��N�mLU�[k�T*|)֬#P�`-p�lgǽ���:�a�k��p���\s�A`k?(���ю*f�c�� �@cצv��yp�w�ކӂ4EB�@''P�����Gw�Y�ہV>�����it�c�KR^�D�$�3�^������t����4?c�(�,��?R��cd#����������}N�̺��s��xCBG�Yn�z�ԯXQ�lp�3q&g
ט/K9�� �����
�k2*yx{�\h�Y�"��6{�;{�j�͊<9:��2{�N{".��b�hs6}�Q���<,{�"8��v����<� �  �X�#��![��j���t�=�ӂ-X�6���	�)��5��@�����&� �C�#�I��G`�L��C�F�$�E۾C�h�X����@?�k,v��v"�v����.�?��/S�5K%���&�R:oC��6�� H��< PuXZ9V��������(dD��ɊG,��(B�;�;}��>\�!<p���Z��,k�Ʋ�z"2B$|����ӛ<&�G%\�k<G ڂ`3�B��9)1��y$��$� 9�K
0"�:%(��C�`H��˘R��=t��	����[A�e�4G2D����19�H��� S���PÁ�::5�����,��(Ű۠l��@��J��f�0V@&��k�;c�
�������r����v*�m��7h�E.�}�}��s��Æ>�\��(����)˅��kh !� B8����DD=��2,òy��E���F 򂬨.�=�/����G����褘Ё������	 �:Q���:#����	@�07<�#���|̓��I�MH�C9L�O@�L��J���2%�,CI��!Y	2[񹞃I\D,�N��9���4؂3�Jq�`+k�I���ɴQ��Ȕe:<e�]JV��k���6��]Z�����32��Dvj�2ˁ�@�b�6�l6��Ÿ��}p�,���˯�P��;�y(�H�2�sЃ���D��+�A�R�Y��t�T�ĴL.�����9�=^���3��	�{���L�� ~4�H��/`C�H#}R�36�5ZKM"���AMZ�/����l�HHtD�A� ��3?�+3�٬��D���7��.�A�9���&�3�NI�,JYД̼�"do�f`V��[�"��0K]4887�Ot-��2#V��LPuPu��g��|��f�T{�� 1���?��~`<Q�#3��'�S�=���,�L�,|G�3�#⋉yA. e��l�L��LR%������'M
HW"MWϤ &� & R�M#H��C�>Ml���球����LSG���
����G��$&�i �6�i�R@c
�@-�8T���S�&`dS�Y`�[8�Z��zh�wH���=��<�6 �3�����0JT��}��}xU}UK�0gˇW�|��y���`�s1�ZT<e�s�2,{<-L��!�3�7�B*�Q��� �9[������`	�¸v��6�:)M��G'M�d�	���V%��t��yMR�&��s��h&���"��8�v�!�}6�H��H��,X��od�r9X�i�B��-������!�	��T�Ig�B��b�xЅX`S�@+}@O�\ 4�zC��b�z���)��e�T����Mڤ�PK͇v��'�hE��s���l�U��Z;�!�F٣B�"4���gu���8;*�r��|D��X)e����'��ҔWo�L����Ժ���'�:�R��� �0m7�+�|Ğ(y���i�N2���-X���� �C� J4��;�m��f�� B�O1�a��C�Z ؅w`�v�0�f+�DJ�eSc��kV|+��cl���Mե�%TEZ��ޥ�yȇ���߈�V fH�>y��|�f�J~�б|�>��01Ѓ>�jOX��\����f}G� :{̯@)��8���M� "5���tee"5WW�[~$y� 
M��`zM	P�@T��q��wb�2s��*?!��]L��ȟ��7@���� �C�ứa�M��K.�. ;��K��	���5���ݺ��^��[������0w@����ñ<-�ٻ�19�ST���b5�^6��|�6.�0�h��0<^���r9�xHU} ��ޢ�=d��Jw��k�X��niu��n�f�f������B�CQ�8ҏ��&�:Vֺ+H�$�½`��+U�q���ye#U������jEf;d����<V	�o�Ʀl,g&S��4JԔ� n��+3��9Vy�F���!��h"+�_�-834����ؖ���V�ő]����}�p�2DA�nʃ�uj.���V��i��.r��uc-ڥec9�c;f�`�r9g0B��Z�� ����ه?��[�X��nP��I]��u�N�d�zV/L[iŸ�-Û8C����u`%�`
R��׾�G'mR�e�dC�%R_�j�<T�r��(�?��C��D����o\�2�B�����-]nCE8L���%�a ��*l�;_I�/(�3�e�+<�
[�T��X�[��^n�+�w(opY�]��s��2�u�� �h�{h�W����6�����o�{���EZi��i8o�gV��~p��@Yc\�}�>Q���k��vϛ�nf�������`��V{|̾(�ʴ4�X���o��W���� 	HM%���x�w]Wu�ת��`Q� �b�%�Jʸ�r���c<lx�@0�0A�-Q��N[r^Ӕ�XX�a/h�OV-��`3����\�]f����fz�ݽld�bu����OK�4x� 7m�N�.��h���p�& �i ��h��^)�h6��;��y���Tu`����8��4s�ˠ����|�f�k�n:o���N�-/��[%��5�`I��V8�3(P0�LM&�F/\E?�����e�[���!���c-��w xL�kPFӈ<ƒ�13�4���m����laWz�UkXM݂�5; `TijS�y��0��OOfѦĂ��p��Yr����x��7��~c�ޣ�^&��	�0}χ{�i`|.��u817��Hs-�Ani�~�jP �~x]���<���^Li �V	Q��� �A?��/B�T-��*Ѐ+�  3X�@�H�UV�(���V�q}~�W�k	`+2	9e�Z<�2�^.���m(�E.Kl��ūP]z>�<8j���&��c[�N�N�+��
c�H�z�ڭS���:{�����/��}���{�H�-�����ɒ�`!�E�->	6��L�/�^8�9���/f��$J^��-�0`N��r�jW�<x��qE{���[�j"ٲ���s�}n+Ft+w�Zwݮ��/�n��������ϐ�8�\8VЂ6h`ά��:8��A#�	�E9k�+U����ыG2^�pPa��H��`�;��)�7�\E
&Ψ CfŎ;�c��-�f��[��]<{���{��5u�ރ{'1޾�˯��c��?VCHo��t X�GV�I
r�^�ц��y�B=� ;�JF��3��s�C�y׏W�r� q4Rr����8�HR���D2;�ԓO38@�O7)6�N6���M8|@�8䄓�I��A,����=���yd��Z����Do�Ŧ;긓�^��r`���+���bZ�
H��d4\��i����L�������
���Z6�`��V�^�Jd�ƪ����"�`�u�V�R+.���;��C_����:����:�\ī=���<Vg�@Tө$�A`�v����98RI!E�ƶ�+����V<�_�c��3'�h�[�Q=�\(jJ=��H
+���R�8�79��>e���F�t����&�@�^Jћ��f�e�_[2�5�[l�3'8u�W�����:�bK7vhc,�(f�Q-�h'�F�i'�p�4�B,���X�D�GZ��J��+�l+�*����ZW�u��y�Xް�ލ{
u�N�*Bn�<M� �u�7dl��~�n�p�$�nẨ��{��i�@;���@�HT�4��CaR<���I=hc�m�+G�@4���:M�����8��)c���%{�A8��Ê>��#۰9Q[�c�|m�']=�����%��5f!�X M@Q���4(�Q�L�0s \�4��Jf@$64t�5"d[������0�2�D�7\]-0F��dgf��ʯ�q�u���	��x�(XG?�@�`�������խ���]�*���.ӹ.@ � �� ���3����}�!�\E��w��x!	��E�
��/)F+�����R��O*V$E�$c)���2�8`�Ǚ"q>�!��\��jdc�`��t&3����S5X�"�YQ b��4<0��()\3;��NC�Ԍ�S��BŒ�Э�p��V�	�T.�Ae�P�d�!�R�7���p�A�_����g;��a� �<

����۠FА4t�S^ !I�P�wu��v ��@�m�c=X����pF�H�Y�Gy0�"�|��E#o��`\�#J� C� �?�	Q�'��!ɨ�{�	^�J�,c�l@V�ΉD��%WC��8��]�"6ʺ���.�L{�g'jP}X�ΐ��(���"0#��8Jjʼ�V0��\�R>X�!:�O��
��HJ:5Mr-h%+�ڪ�/�J6���B�����>�E~3�Yt�Y�.���Ї���JId��'�YT:�y�,�x�5�\�uz����.9��]�G9I��a��:8#CT����B�!y�(.و��=�t$8R�~}:�  I�����8�%�fizD���J�L6�'�j?"A?x�_�pW�E�P~��m���j�>�8{�Cv����(N��Ҁ�ר� ���3�Y��ph�z�
`A��T�Ă��%�ؠ����hOՠ-썜�1�.� (L�шF>^ۏ{�X�E�	0��#=�UG3v��uؓ�ڏ9�a�i��<��=�1iL����>����Nw}8�5�A���y"��A
<d�1��$�F��!�f�#�2����t}d$���n���*Y��!|=$!U�%���$\IK>���E���s��vWC�aQ~X��փ��7l�31֫�
��4j���׬� W�k�� ��P*������faY��&��Eթ�L����o�.���,�Mh�<�iLC��F=��1��_EL�BB��%C*���@�q�ݝ�̆6t20^�L�..r��{>�h4����؏4�t�95��0����#�c��X���}d-�VLڃ3�c9����T2�9'Tr�����Lf`����ᯂ8�̃C��.�?v��/7/�@sK j�Hlf�v�|홥�A�I��L9�W��c{6�"�����%��۬2/g��a�X� q�� 
����Є`C���W0f����H/�҄%���7͇Z.��!�;774K=h��e��q�y|?��yӸƋ�ý�:���������f-�ǔ�_G�Dj���@
�|�י� �	��dEە��4U��!�� �<�	|���A�]�E[W9L�	�Ҵu�G����.Հ�� � �,��Fh$�2=�	� ��פ�,!��0a���K��MG�G�F���Y����5a�@�U�9×1�}Y�I��@�@������Q�.HQ,�E�J�2J�\��IC���4䃡��!��ϑ�͝�=��y�=4A
�WދJ��{���N��Ș��;����M	��T�<���׭@& ��Z�q����t�bD,xWY�zPX��8��m؇Q�=L3rI�K�y8yV�@6�/��Y�2����i0���	pF;b����@� lp޾�M2�j�,alhu@nA���@V� ���uh`2О���D��D�އ��Fj$� �!���Z�!Ԡ�����˭���ƽ�qI�D��M�_���5;f$ѵBoIC2��z�Z���<�|a�:�����MG������P�� ������̡�e����14C3x|�[����݁U�1O
��¼��Ⱥ<�7i�� P ��8P��e�X:&��|�2J��@B~M��@d�id���	����g�f��� ���gd�g��<�@B2�iaM�vl�1D�E>\E>\J_*�F~�-pg�a�<�g��@�T>4"���"&����M���Eb$��M�B�, �C��Q#�'2�'3 =N���P���Ip�miE ���W����DA0�8C��D��[��,c3~������]���\TIPU��`�`7��+��DJ����Iޣpc��K�ZuX�S0�½��X���֏�(vxF�S�DJ��M��QtM��p�!��db�JJ�J�<�Tu*����5�"�N�$v>b> Cd�I�2�V;�24C2���CӅ��]�*bW�xA���
E!I����ƞW��	QD�.hA!b����΃zU]~�42Lt�	�%�����X�a��^�8�@e��
LE!A�2Q�*;�!��H�����Q_�jM@���Ҩ���@v`��ޗ}$����:�oրi��kR�a�(�> fd���*��U�4C2Z��k��k�	������3�K���%C�&�4 �1�2�GAɃ3�B����pN(j�n�����~� �g4A��N��.��1$,%j��,()����a4�`WQ�"_�κ*r�m܆aҐ�( Nd��  �����<�[r\3�.�Bj̎p�έmH�F�ĉ�g���&F&���f�� mi�0���1D�Hz$D�(�H��L����G��gԠ@i�P�]P��=�0x���4$E�՜͡_��$%�4t��m]�Z2���!R��P&�0 �0LC0�x]p�d�@���@�������IA��$�e���0`\�:C3������6hC ؁�,�!�	bh]���euA�Ci,qY�B��`&���c�(㕣���(&�1��V���=`�4�C7\�=������4�3|����J���Ͳ���2gu������xٗ�"GZ�P�o�(���
�+�8��բ�$G
P�qP (�<�Y"JC4H/��0`.M�u��-bL��4D�/<�(�*�� EAP�|�~6y��p�����e�2�,��0ѿ�'�@W�e�@B�V�0t�M��tf��b�w�,(60����h#W�EZ�U5Ă2���Z9Z�� 'Ӏ�z�9�8:����"�i��6�Y��������ř����8K+S"=���h܎!ڭO��m� }�GR�-﵆������0��Zy�O~%�1A
L xs
(A0�8� ?��1�<U4����<���<��9S��3�@X�d�)s@��#�ʖ�F�������,��1��r��EA/pn��C�d� �G$2,����\�>lC9\�J�E|�G7x�z��,jTG�F�@J"��.ެ����<424�̂Ǚ���5��+�ǟ����kQ"��\2�m��M�����1)��MX����G:��n+[�'��i8����0Q��U\��(D@7w�7��>��7S�xs/68� �`cv8+�e+�qx%/`A��-�-�0C'�ʂ��&4Yާ� �µ�d�8�<@b���v�4Y��H��,�B+0���5\�4�:��Z&CM/�1@E�����4��u�t�*��o eVxт=(l#6N����|̓{3���ߠ�=�E�_��W��-���/�&ۊ�	����v��+pr���6��g��~0'�%q1m��Y��76r(�>+vb�0�8r ���P �Gvd� H�Q�$��6,�.f�l�1k��Y4H���F�k�Ø��A�Gr��kI�,܂���J�C�B<���Ø�-����u2��9Ow�a�0-
�M�y��S��>���,�{�$S~�"�{Sb�8�=D��r�a+�d~]±\X^�O����y0��@�,��Z� ���@l3rD6�/�>[���8a�88��f�e�88���f1@��h��~4�K���Xh@�lYn�2�:�ʶ�O*���0d_�g�<�ok�L9Y�I�7m�.0w�=��Jw0�B��y,��T��s�'wr�2��d\Ge�uP�Q��~������!|�[�+�7S^���üǩ���	8i�ZN�ƞ�n��K.������o:�1Ӽ��:7O��{x
���8�Gv��ts86b󀮫��b�:4�ոC��h�x����ItB/��6���{�?���4�	��m#���%�%jo>poN�I'0|-%wD�8�nM������aor��u4m7��aF���h��Ђ�17�e����̅���}���n~���J��k���u��O�d���v�Z��+����g�gj�ix��	L@̣�� ��p
� �㼫��������'��:����+�j�>�9���,\/o�1�� A
x��h�@�:���v�i��{E {���d?�,�8��Ȗ����!�Į]�F��$J��Kz�`ذ�6f��8�38n�0CBH�Z�ڱCJ�Z�C&M�u�����g�{��͛��޼�=���'��<z�Ҵg3��;���JbJ ' �8��GʆB�6�"Jeâ��dİ]��R�mY {u�{WȐ�A�0I�E�6�$��J�0�py˖SL8�#��\=:��C�Z7,Y�	˖�خ&
�DJ({��xT@�/W(<j��ɓ��{v�E�ݻ'T����oj7����4�͂eO^��֝���,X/��?(��ݺk"�v��j�a#�@)$J����k�'i`jF����=��o(��9�'���g�~��dC�,���,�:���������*���MF#���I �cή� ����,�R0�1ʈPB�˲LA���l�3y3K6yX�,�t����l{�κ� ���A&7��H(�!=4����"!&x���b��&Nh%�iZ<���<z���;N�ϳ�}\�ɞYX�ny����Z!� ��+Ƞ!"�+{�!"�0�h�cSx��`��
Wji�j��p����֦���pizB*[{h�黥�bʻ{�b-�Ȃ��
�����<����Bi����^!����#��� Z8���.,�5����ɔ`�K07�l�6���+���`��CRJ(d;�N��j�u��g��F���f���H�ګ�&Vh��]�p:�V.��CSm����]�ś��4��^l�X�9;��m���~ڷ�A�aby0v����#� �$hw��$[�iG�u�if���{*����Iq��=�)O{���陆�yE��^�t�����H0��Xa�{�,X*�x\8�e�k/ t˶�cn��׌�0
4��-++-�3�Ԓ%z�{�v�N���j�<m��Nauw��g���a�&a$x�p��X��ŝu�� �V��эfx��P�:g����;���4&H����߇�q�x�[�Z�3��^#�X�&��!�7���C40�ad#9��G����%�kI��y��k8A׈�����\�aO*�l�U]�浗�����U�R�#)/
[����F�!�I��Xl��CA�x5�@ʔP=?!4+$��Ԑ�'0C8�_)��$�92�RqSɻ�{��I<�����C�H�N@?���׈G<��3s���X`7���Z��8���3�
���`S��������<�<��
V$���O/�Q�0�,,�Tt�s0"�n=��8u��r��A��TB���q�0�Q��3��:��P�A�A�S�ܿf4���*<g�V��$�	-DJ#� �@ F��q�8 Aem��l,��L��R��h�&�-@�^qz��dS�Q�)Yv�����Ps[���X�'���(�)د+��;X�3�c��t�;��n�"�`�����"�5&�H�Zge�~~�]�J�c2�VD"?��O�I�%����Xӕ��"�S�D4�M<���J"�Qŝ��27Ϣ��&�{`�:��u�<.��=OK*�K,=s�����z\�4zF$IE-xE>�1�4A0�E�GY�CՎ�|ݝlė\�ԉ�H��l����Rc�B| ��t�O�����0A�N��P*5'�(�Hi?�`�d%������C\�%-qq�[ �C�<�4�!��#m�HF3ΆV_�̘�4�a�´⚹��	��+����8¡�]�n]!/!Q��@'HAdC�3�@+��SX!P��Ga�2���pA1' z��H[������Y����]4�ea��D {�H���D�(1�Z�Ai���ьiC �������F�,�J�[;;st�
;�qHj���`��Y�3�3rs	��ul�Xj�q�8�S��}Pu��>b��Z�2��Ԏ/�2g�?�80���-
���a�0��HZ�b���2Ԟ����k���x�	"� 	��'h !�q4��Z8,P��{k���8�$\��
ZѾu���i�Ȫ-����8�E*1��;��3�<�q���_�cK��;��I�5e#t��P����GLh���~n��hE+�	��A�P�h%ez$F�i�R	]vec)F̑Q)X��������S_��*{��K�����Ї��J(�� �F.o��]'e���p�	�B`��0F20\x��mөρè\��.�\�;��4�a�(G9�q4�ޠ_�;*��7��g�w���3l�k8D"!�Y�t��Ұ�CT��G=�׾z�S����:��uxC��X��|�׾q�#�P�"���4p��+�R�f[��Y�)�y�t�Li,k�i�7��#/ቹ�AU�CU�aĒA�8�p���F  �N�$�.I�`�v��R�@f�c/6N�g�~��"� g��`���EH��>�<�j�}�gn��:���RIװ�N���.m�!�: o	�������
R���W��Š�!8����� R��< B�Z ����T�� Z ��΀�B�"&�p�gtO����� Р��`��!��*����2��� ����D�~�le�`+u����"^.0v�vD�.h�Fܨ6�b���[�LC�qxb˞B�����B1h)v�Z��`f����O���+��Qb��Cf(%;M!����|��f¾�q���f �n����D�J���ԡ�n!;^���|m�J?����m��� �#j>J��`/�
�c��cQ�#���A� �!��d�`!�b����`�U���(�R��DbBl�$R�LO������B�� ��� �2)�)M/� ������|�uX�.#��$8�k0�,0X�	��+������c�!i��	�����E䊙J+(��r��⬣(�ϜA�D���ǒ|��,�0,e7$	ӆ�3;-:�P��ej�����A�L��At������!h�(��}���:I�*;ԡ$�a�|M�Ah���	�n	��:�""�� i�8��tTJ萁N �L�|p%M = �A���� 0������P�z҇�E.����*��*�r,����&�)���� C�r)'4C%C���� ̄��*"�+�^�-�"�H��0X�x���'g�����n��x�	�����b%&��;��(0G���,����x� ��W��+��7:2<{�4��8�Ly!�#����A@B�*�(��7����i8�!�l�xp;���A�(l�h�������Q�i�� Z�i4s8l�-8R H3���x�G�@=����=uL��a���?Y�΀� ��L  4gEq� ��(��B#�� ,��Bߠ�$��,T2�Z'T+��|���K2ƵH�0P�K������U�2��ԯ���.�dx��;�,\�a+H	��RJ�bA>ąkzQ;�(ЬBd}�!�/��g��z��� $����Jd�tK�0Ԁ��`x��aê�?d7 = �6����}�(�Z�	�f��"a^`K����Nh"{�j� ;!ER`R"iL��!:�����a���`|�h!��>��$���`�s"A'W� zB*�E��(�)Eq,� B?��C��X'�B;t)1��`��(��D�UtG]��2���M�� ʠȀ�b4~�vm7FEa�/�Aq�O����o��6K�!���1=laɃ),�ᆓ��4G��Fp��'*�n#�"��#�L3W��h�a^�6Ebk_a4$[��A�ʿV@ga�gPd�A����"�~���!���ʡ�҉��k~f F����m��R�3W�V�"�	�`X��Pi����azS�Jnc�����V�``�Z� @��,k,^��A�(�R)',� Z�Z�uZ�8q�r�5B����u/��� :#tK1&�4����>��>��ACq�����`u�O�v&b2��I���|�o0��a�,V:����VD _$�lb2V�Pz响�W馈�����WL���0WZ���R^ ��ʩ�o�f�
�V�Z��P0�ʩ�^��.�&Z�ط~>��?�p@p����d>`Af�Q�L�����x^�x�9��{� e���`�L���N�V���� W�`�)�M�T~h@�uB���/T*=T+/��� C��r3Z���Y�XsYA��� JT\E�tIt4� �@��*�)��+��� V�u��]�.#�!�!٤����ω8Bv@Ak�I��BI�h#Y��⨣#8�QdG�Kvx<�	0퐚�t6�
ɗ�4W��Xa��v���~��Ήf�J�E�� 7C�������w�D�"�������iv�p��O�N�F!2�X`�ӹot���y~�N��T��b/nL�n� <��AL��a�ܡ�Q(��
.�΀���n��E,�������(� )�u,%�q/T��X[��q+�)��>
$�&�Dw��y�	� 	(a��R)�ئ{:��о�����؋�!i���AZ��P[Z!`K���I&��"E�8�C�c�NO {�4:�� .��^
!�P���W�ו�:^!���f�f��~>�����G̎N�����f�4��|����ran����)����>`T��={���qٚy0;�u��yPN�	�!�mX��<����-��Y���@ЖԀ`��PX ����W��"�oX���Rw*��Y�X��5)� +������/T���� 3Vڥ�X���~Z��2���Y�{u� )� ��������0������v��k����[��cfe��b G��b&�067��9����S#G��=�%���i� k|v�6);�mYHiֱX�M����ny��f輪�#&� ��q��+�3��8[2��O��<�)>�ˉ#(���y��	j�����A%�����A�a�����a���mUM�'��*fX�7ӵ�
Q�{�#�)�C3��M��%�[�J#�Q����S:�����*4+Az���1���Iz 6�~?@x�ZA�#�ʉ��:�C�E٬C&�Ɗ�
�GL���Ó0t#WJsK���Z4C�k���E5�=
P���!4��Q`w<����L�EO�y��q\��A�~`ā�A�A�Ǔ�2[�i��?�N�NA�3a�-���1�Rp����̯�e�`�a�a��=�������� V�����`�@��V��
�  �x8!ABVBvI�c�lo"j��E�-U.jy�1b�6q�D���9��i�H9[��H'rNx0�O#@���b��X��|c�$ɧ9���ŋ�3���!���L�0fO��{���ۅ��&�ZK&��:ix�͓6mݼ{�M��W� M$�� -v��7�-Y�]�kJ�('�,��Ihǟu0�ܙ�k���ɳ'��uӚH��@��8pPp�o�:f�~"x��4/8�z��xx����y�X岧����{���bF��N�?�)�$^�����������73��[���F�{�ч"}b�Q#i!FE]tň#R��IB��b+�B�ImTe�UT�EEJu$�RJ-U�Ko`�S)5UE!��-H0Q��0BN(LTP(ؤ�x ��K"�	��?=�Dz^� �:~镏=��C� (�3�\r<�	���7��I�-��>��SWe�X�L0�E��
@�fZb�6�B����Y���l�أK,w���'�'q��ۡ���x��]I0�t�='m|�O?زrM><���}��z4�w(���I��4p^~ނ���� {�!8�< �>z�AG?udD~�d�D �Hb�U���A,��b�T�EX��ET=%�N��aTi��1��4F ��=E��e�3U,�Ƈ]�$-�&Ek��Co�.���h��<����{�_����5j���f]�1�L��̳N3t�m�^���e�c�&X7�M#�3�O�4��}Ll��CK,�r����u��%畼`{�v�����w���p+�b<� �h�n{�}��*��g�������m�����g��`xI	���HGm�ET\�!LI�-��
�(҆�k��I#)��GA*E���O%��M��I3��<`UڙP�p�Lb�P�$�ཡi�A��HPr�TE=`�[�ԛ� �� F2���b�*�yfX�o��w�)�op0.�p�	4��^�F7oLn�B�Y�7���;��iC�>E�V��E<���w�c�(Vp�j�'8��x3�jIKt���V�B�"��;����C��W������Q�1�'�S�SdȢa�������s����& �L�CD�p=3m�CsX�80��P�b.ZA,�1�Te�����,X��L�FI��G�T#�xa[�V��B��1�Yz�.�,8��@+�f�^/������$J�@8�8a�N��P�B �	AB, �C��'u<d{�c ��?lc-쁩y ��K3�D��e�tkb����uP4rH��w"79|l���b�>��V���h�;���T&g�#�S<�C^|��*T~����N���g=���B��`�ah�y�柨�w�zAՃ(��	����zΎ(laa����4U��*X��Xa1���%Ҏy����H�HȎ&/8ֱ�,��Ń%���L�IPf���7C��`���j �� �^�$�|C��4̕h��pS
�\'Y"�IZъ ��l���9Cq�\�JN��C:m&�`h>��f�M�`�=���{�E+/�8*7�P�X�[x?
7\����G��R{�#����ќ�[_ۇ>V�瑥B��O�q�xP��������Ս����L@��e��!�����pű@э���u�P��tʈ Mh0�"�3����A�j=f@$C���-�aW@f�G�ʺ�aGǔr�����+�AH�����	)Ь�b��΢�=)$� ���^@C�Y��i�ar
�&r�<h�.�V��P��h�1&��=�3�WLg�w�F��5θv�.���[D�����0/w�9��fn=�eSE�2ޢ>��_	��(�4���C�Du���nD8��"C���|���ѳ��p*P}䓉L���b��&��O��3��52ԇ%lcr�kD8d4��k�5`V���&� XxE����}�XN�/�ё!@�]�P��� N}夌-�/�
�Qj�%���6æ#:a0�a#b�(�Z֢�?*g�Ҥ2���i#��LXx���4S�U�l;�5p��oρ�i� �>�!����Եeci�i�p�:�!�'��pӅ,��R�:>�������w\�ږ�6K���?�������MU|���������|m�PW�K��j�0��a~�s�Ή�iM�G�ᡏ�c�X��lx������kX����Y���  � �r���Eo
9B#r���,Z��+�f�ß�5_Hf�����4cg(a0��ghpWO�h@h`pT�A��$�&�O#���R�[`�p��qj�h':�PV����iaw͠_W�@Q|bt�'W+4�a�l�5��D����)��;v-� �� �G��wm[a���g^���-�f'<�q
��	��	�`f o&�	��5�fP��\tt�`	��
וO������j� �p}�P�נ}�G��hk�d����}� �d��"��
b�wY#\�S1�3��d�#c-��E9�3�vf#0	}�}��@�k0"d�M<�#�>Qg[_&X�p[�! �,��!`{4��V��O*�b�%.Յ��w�7tc�p�P���`y� ��F$�k!�w�V+X�p�R49؂��wy��`�����5�@a��_^�l��y���@af�G��p��`
���.�P��jvX�kpCF	�e�0	�@
��
��
�P
��3�� -��U'�k`}���p�@?o��HO��k@	*`  ~q�6�"懋-�~��" �"l�~i����f�C�=�*@`F�C��u@f@vxb�� K�p��V�Qa$�tNG�N��WG)Api�#Xp�!���o��\���)I�%.�)'@ ���0��� W��f!{�i�i�@lH�Q|g5�ƃ!k��
��_^tm�P^$a��BCr�^T�9aV�����p��;���{n���	����Ro�r	��o(o��S�z�f�/�`�q��pbyl�j0�@JG��x� �H�k@� w���<�f�����KE�.�)��yy�=��k�f�5WV�7s'��%Bag#%P* hp@�p�xh���x?��A�sAT���O-����u�#�#�L��" ��%�v���3�ȴ_�+�2�Mi�$i�PD���k@x�x�C���9��@X�n��_�|�E��,�z���BTD�Gh�[��p�����	j
��� �=��j�8y
U�
�vbؚ�Uy�����}�B-*�h'�e�h0^0q�p�� �@#�jp  @:z���f�77ws��>�����-RB c2F�fP0<�s4�%H�4`P �Z9e9dZ}Na�gs�>hb$QA�2rZ�2�ʰ���e "X��@+" �  �y�F�
�]0$XI�31� e�#z�tk�{�Arp��q#D{�)���!R c<�
�2�n�-zwm��m�@r�{��A�(�m
&T�`�e�F�l�p�p�Г�:o��պ	베��.�"��ꭨ��1Q0j ��0�P�H;®�`��g�'��Wp*@v�,�'У��~<p�@�-�-��7���"2��M��4ѱ�]�]J,���pLepbpV�ZV0�IL��2�J^���/ʐ(�2<[W)C^`$K�1��1_�?E��^@\+��Cp�8���P}�Xe�Q���W:�%�<p�:�<4 3 ׂ��-�n�j����Fؑ,'�@aڶ`�%��+�S�p�Z|�&��и�Po���:ٓ�[@�	��.�+�������k�L���}p�[|��
d��
`��4�� -����f��K)���>��Iۗ@0�)�usK������k�L XZMw� $0�\�+P�@	�c �U�E�A(q$��r��@�
��t�3�a�ѳ*SW�l2Hrb�.m�����/@�\�Y���pW�"�n�B���,b�Uȃ���R�v��P|���za����,>|��`˦_�P��&a���
��P��
�f��P	�0�'�H?٠]o���9����j	Mp�o&p�����Ȯ-`���Z�0o�޷Ǒ Efg��i �
�W1$�dGm���#���Ұ�"�������Qs�,�𼯀�6�Z�#0$@[�``����@�Z}f&��.q���@"͔���X�[U�?!ҳ��^�#��L���\��
> Q0�h�����iP�p��YF������T�3/�<���@��l�E`�Ia���w�ĉ�`�p�;�z婂��z&��>	� �]om��8i�nx
��
��	��
'���@�C\|נ��V+����=}p��{��*pDv!@��|5#�I�Լ0�fP�~K`C*���f؋�Q��Z��A�
R�sCAM#��`��b�� �j d� �bPE�&�������u����D�I�$4~���K1�*��(��LK0;�o��z�8��XPI���0 吂�Y�G�
�p3L�M� S� Sg@g�H����g��b�
�`� ��r���fP�K�����ڭ��	���ܑ�3)@P*�Q��[�6�����q�� �@JG��;m �m3�
d�J�}�-Уʛ(>r�ܼG���W��S���.C�sYJ�dP$ �v �o �8�@A'�e��0���@-�
Ր���0UP�؈X*3���2R��Q1����T)A`(ƒ�e�x��@���iO �_eIY~/�q���� �b�Ƹo�l��,�O�0��vM�T�5f��*���z(B�����+0]��o!��`۰�����
���~�=W�ͮ' ��Q��2)P���ɛ�(A��O}�Lp��f��^o�Y�P0L� �[����f-�fpe�C� �?�U7I��-"_�2� �]&"���46.���e�{0US�����P�ي��0�ߐL!���Υ����TpU��n/�?��
)5^��-@��m���
��둨Ρ̊Ŝ�	��Ie�5�7 bI�QV�Q*.�'v�e	�^�@O�Y	-p)�(�?<P�Af�q�߉�f~�"f���Q�����,r� !��K�D"�U� ���1E#(P�x0�	�#X��r�0`�hy�2��7Z��|�-5�h��ƋB-ZU�T��+E�9ԋ��L�2�����9)�xѳ�P! �x̘��*��7���+>|����sa�+�c�����~�d�C�X�bƍ�����+X��ٛ����.;VLH��&'��x�����Ԍ��)S&N�L�%V�������X�=�D��I3\k�<_s-։T7`}E�5eʜD(�x=x�7?�k�$�`����)x$�?=~!AN������0�@�L
 �" �x�R0�3��Pa�J�&0�0I�6TR�&��b��&���B Zh�+�B�ǐ���+�d�,�J=pb�Ŧ�@�Bi����
�Ta�����������zah��.�p@�48�������A����m1Vn��V�Gy�Y���,�~�a�iZ�@5�V#���N���O8�/�XS��˫����keZh�EcZa�Vn���*c��� �@�� �;���r/�z��҂�?��ONX�Z� �@ �VA�,�@����&�I�"�,�_���#3�P#5N©��PI�W|	�]j)��X�&�he�T�"�8��(/� 	�'i�/�؂/��"2�;�B�8n�O@f7�˃���@,XS�:�M`xj����
;�O���'�~��Z��X��yZ�ei��yh1��u��{�Z�i���T�MS�8�-�Nn���7U��~vՁ �K�(v	Ƙd��ct���]j�re�M��gϓU�ދ��L������������&7�@��=H���@(��j�� �Ia�E��ꋰ(~-2�0�z��Yd���ߨ*⨞��F�߷�c� ��+���x��� 2���vф]�~9N/8: �b"Ap �4���\2�� poL�8�q�b�q=m�2/��g.S7��#���:���3��m�X-Z�����n[��e�!,��@�Q�����L�p���Ȳ ��Jihj���L.V@��.z�9�ct�(� Q7@�5AY@�ֳ$�:	� ������@��Jι���&�K@�+�
4�(^�"ȂقyM�z�ކ���}m*`	U�"�Y�&4��P��� ~9�%/v1�\b�/� \�2�[�5@�/�lT�38��7?s�	岛�)� ���X��|��<`X��cR6��<�a(!mn�F3�ъt�m�T�:�!�DIc�F2E�'�-O<�_�N�6�!�8�ǵi83�_��3&�g9S�؄VX����A�0� ,AuLHXq��� v�[�}�e �� \�Sh���� �������]RA�'l�/#��_�W���
)yIJP�"�/b��U$���"И�;AJz�]� ��!��k�_���H�TZb����g>H�<����E N�~��K��M���Y��1�=�ZE�6Q���:�6*1C���4�!�fQ�l�4�1�i�c�mr���)U��AD'JQN�SJ�b`Ǜ���g�	�1xq1�Z���ꢰd�t��l�QU� ��TX��`��5��}���E�	J�&�!x��2��29�4�	���D(b��L�� ��:�h�(�J�`��u&NaJK�R�V藽��w9̃�8�-}X"G�b�p�ɊB�n� I�8�%4)�m�JK����\����:�ZyH���5�ۤ��4��0T3��G�&#�HF+T�}:��]3o�	��BU�	��� 5�mdÉ,v���_P���/ ��_�1�[B%qփ%����x�)}wi�_��'���r��%���$�x0���4��d����j�f�B(4=}xC���C��!��K�f����eL�B���/�!
�X/.'�^����+�1w�3��.r/���^Ӱ�`
Rn�2<Q>�C�Or��&-f��d�bj�J��-��܌d�\�w&���q�c����%3�a�"C��8�3R�r;��Xs9e�5� uG�EM7��[z�v�	;��7&��Єx9Y1���^�`��P�_
p	�4�r��/�f	�QU�vfG�GP��
�L���^����,���J�Z�{J�B/!�\���
F0���Rg>V�^O
?bD��Q�q��y���	g@�[��4��O6w[kB�2Y��3����r�^5C�r~�����d�,g90���\s$9|���i����y�I�9�'sf�<v�9��/��"u)���k���:P	8�#&(=�I��;U[/�k���)Y���#��5ia���1�偂V���Y��!@��x)��A����X��Nj�aÐɗ��	��01�	=P	;@�fʟ�Ü` �(h)�i�مi�`H��rB{�h��� �$�K �!&#�4��3�� ���f�8�ظ�C
y��8�-�C�c��3��ۜ�Y96�>^�^�>��8���-���-As>��dp�d�?���J4��� OPNHEٰ���"0��*'z�= `�ә��X��R�����ҫ�+)Y��ʵKꁦ:0���pA�ʫ�/[����<�B�6�:���&P�`� �P4�2���@�2(S+�^p�.�=4��*l��B;�D~�Bғ������0$�1�=���d7d����B����)
/������#���>뻾�[98:7R��	�]�>�K{Z@�!���:9;����	c�%��)�袄OP�O��O��Ҵ����V(J~�14��˩� ���Z��K5����d����J���oiF����¤�������D�A�"0h���������b�9�b�0��˟𘒞�<'�b�b�BCZ�%�t�tB-T��+;4�1^bȋ��5<C5�\<�>X+=,�(�C=d
A���+>��%情뫾�d9�i�c0�]�Κ ����-9"o�:+MN$=5��������yW��H`;ں�љփ���ES����7���,�^|��z@�z� �Oe\�a�����Fy�Y!������+�Hr6�1��F��o��ʱ�����]�׌=z�G���Qbh�a ��#Jؔ0d�`��lHl����$2ڔ 8�5 ��͑t
�:>2�C�#N�;	����
	/c�uȧ����Ӝ�S�d��sD��Vp�?2d �Hx39+�V cx3;k9��Gy#&�8�B���Ob �D��D��9O( ���أ���T��<��ILs�)��������\��x�+�й��҈A�1X�F��A�Uo<����/��:?��+~2��K ��Qb��(�~l9,��#�"��cK�	&$ER5�H!����G�	�D
f*ӂ%�1rՂ�@5@�5P�5��x �"�>�����c@�f@�B4,�f���3񣾪[�1�`8�~<C�C@F��(h=ٴb�D���|Q��V ,�[=����%�����ca�0��E;�i�H�@���$�М�Lb*fQ	�%�l�Z�����9�	y���a���_�X�K�b�\�a@Ȃ�؈�B4R���L�`o�H٬H����s�s�/ّ- �����\
v{(��y�Y �wЇ}؇w؇x�CQ-oЧ��ӝ�W�X���S:e�M$�3�3�#=V�	d^�``VSċ�C�֛5ِe�����)Km��Ae����N���i���#d)^i���&h���K�YUK�����c��W`M��EZ��Y�[<c*��8��2��:��4ֻ�^�͂���)&Ԋ�m݅n`z�Vd�*a^]�2�2�M0����2 ʜB�V�]X�w�-yR�E�{�|e�z�yp�B$X��D`�>��Ι�NVc��]x�^PD^h���&`]�,��ı*d�5��)HOۍ=�m��=Gű�h�E=#3�?��������r$ �J��m^�M�_�YV �g<dF���f�[h�k��/�U)����0��U2f��9`}�24&�^��]dC���#Ë9��]�!��傳T�-�z  pQ��f]��Y*Y�C��*YZ( `x�hF�3��ay��^1ÌW`Fq��[NGdfF�I�T)�D��<7j)/���YY�)�VH�'�b���֣b���k�X��.���4^��5�W4��Y�)�q㞀cfs���X ����}�fH��B~�EBFZ%� >6�ɤ����n>^(����H4l_nUC6�MN�Q�[���K�TE�֗N�zk��_0`�}��0��\�Z.��G�#�R�i�a�yb��J�ef�c]� ��>n`h ��:�Ƭ���_a�#R��_��D���ݮ%&��k`*ibb��_3\��ZDG4\`c�����d<��ٌ��حa(�%��V�{� `�?f^�'��M�X�%mn��) c�,^bJd$MJ����\[I�Z��ٳ7l���嚎�` ���i���zCR�.�u�T�mV �izk�dh�W`�^�eah�_ x�M,.ƽv���"�'^�j��Q�|fy��%���F灃�&�+_B�¥2�q��*VTr�X����]gl`��mb�Բd����f�	�9���a��`�aᕷ�@�mF`�fX�D�J�>Ihz`h�{��0=.�K�apb�O@2򏭽����������k��׼ȍ2#;#UO��d�
C�O�-O���U�7��R6`�+e~�_���K��z x��|���B'<�W .x���>�˭2�1�f�Eı`��K[f����m��M������+�6p�Y��7�>� �"�O��d�FË�I�v��|��ޜ/�[Q�[A�WX��ql�8��̙`@���~`�\�ː\h�wP�˸Vx��!���acQG>�T�%�!�H��!�p���+~�5DiKM= ��ܮ7^8���i1�r3?�.��W�t0�/�i�鶅�3����kZxjv���&S=_�059�>�Ĝ�&n6��A�*Ę"K��V���pgGwR�bb��)H��,����3��\d�\ip��V���d�n����æY/a��N����uuhw�?��[��|Pd��e���"('��SOk$��TB]ba�M(���cPVrUO���4d�zOCk47` �H��w�.�z��8 ��7�_�/�7;Kx����ih\X�xR�����m>MdS8�>z︥_��.��%��(��[&���I]dQf��[�62&����Xi)�˳䔳ժ3�cD�	z��pf�Z��Þ��M��H�X��6�w�z˰����w:��3EY�Y �)��'���k5����_�^x��	���&-����׮�Qv�R(%��C)2I�P��^R~����^@Z5�K�Ɛ[4)ȱᯌ!3ʌSJ&'m����װ`Ē��5��0`)�;Jt�:y��ٛ'5��dǶ�
,Y2V��;�l�1`����Ֆ�B�j4���@�|��E�}�L!l�!������Q�\bR�E!&�"ʒ�N������%����:{��6kֵ��d͘5[׍nZ�h�n�pVLX)$��'Z�����>{,�E�j�9�\��ʓ����cӚ�c�*�4yҺ�����������o�B>����_�B��U�A��[͵�C� �F�}4�J�gRL�/wy$�O2e�a/>�Iy41��O+:��0AӂZAMs�.ҤgOz���S�i$0��0���VA�,�Ћ��t D���W���F��d����$a�)��C��r06t�C(%D,�8�=��BuT�&�1�ӌ1��V�:�0��.�Ђ-��
q����	�1�t��ӂ;ֽ#U��ҏU�8�J����i�#MTRu��^0}}$~Q4���.����%�=D�|Q�C`
-T�Ӈ`J�b���҇'��K��DSM�$�d�H1�@ì�R��R�-��+� ��	� EZQ`��XA%3Z0	+���[n��VK|4�c�uWB'�T�A�4Y�V؝L�ً���,mC9g��2�RI'UvP0Ҵ0�<��Cˠ���j���(m��f3� �Kיr�)(�)r)��\�ѝ��j����,���L,@(#[m��ӝjv[�1G�mP�+P��ҷW��7��a�pQ_�I�a�=X����"�2��J�"�HP
�|�M(��"/����X=��4���Z��1�Cp�FLZ��9|Œ�L+QH��.�/Tз)M9!�]�5����&c��Ą�*ad���5&�/�WA����4���*���X�CjS���Z�(c 7���0%6N	�.���	x���":���:X���f��4���-C6�2M��c�{t�5Z�SP�,��	s;����(<K0���x���e�suA�Z��Э�D IX�&��n���2#؍�Di�0���X�C��9
ŀ���8�	;�0NЊ��x~��P�b0��b-hQ�Z���|�}	�^&2��0��F$Ӌ2��%=Y26IF ��KC(i2�����U�1���cU���jf��0��j��,e�Ke�c�K�>�Al�9���>�э���*4�	Y!d�0,@�Mk��Z�U�:���))�qʚ�ӂ l�&i±|'	iN�0)N��8�>z������r{�(
79^+*V:�݋&)��F��ѸgV���'b��x�F2���n+xE��R_��њ��P6��d}�I
J��&�)�M%cɻ�(R��03�	`��]����Q�4]�*����Ҫ�]����M�xc�е7���.����4��3Zp�^�QE�MY���Dݪ��GU�Qy4c��SW�R#l)d[��
Z���3�Kg#�D�$���ؐah+\yк������E%�3(M����]/�Wo{��
�'�H2P
8��#�8�Q�� �"�1F.��	�w�C#�7��pS��G9���z�pB�-0δI 4��i�	ڢ�غ�M��Y� 3�5�f��_ňA(�X����K]�������x�Ko����+-^��Y̢-6�P*>�9j}�
t�i���hs�R`�$Qf��y������;������0�/)h�������;�	�I94�R�HT�u%e�K�� 9�㙋}����z!<䆄ϰ��}[���I*֙J�!~��4F
T�/��k4�xr���>c�q�=
�Q`��7u���1��/���Gh��.��h�{���X����Vu
�s��B���HM?�=@�5�j~`kh�i�f��0&-f��߰u1֫jf�*�-��V���v%�cJ:A��������lR�ċ}/>����d�Kxi����a|��P�峐0�8[����
�h8�3h}qƋ��_��	s�񯂎F��Bg��<:j��,�mRn�B���F(�0�k�c���<�"�v�&��)+2��4<J`1�%#.](	�-F"z�J��6��&jJ��Z��Dw�� �3 '��A�I�,�4g�f6����Q�
kۘ��q��0��`7-ţؠ�-��*�8�:�R,�0��q�:q��~�7�hՄ�{fcOίh7f������G,�!ڂ��j�Ɓ"�R;)&XN��r��k�-� ��GۡT(�@�x�����_�r���WD:��@�q��/�5o*�)�=�3C��Z=6W�{�TYVQ��=p A0@�eP�����d�ҕ�He��D����pSe��܊u<�`ᝲuP��J[��)24�20�24ʵ���Xp���h�@,s Cl��-��-C0 a��
ppL!�J!��.�����$ž��A`���-�hx�3TX��+�
�_3���pț�μ��H���ɔ�yWw���+��L���tH+r�Hy���A��0LC&��a���1T_~��i��ﴝL5�4�C>@�v�+��=x`d�G3e��l�]=�WtPV2�ͺ|�1�
 �d�V̬������G�3N��Z�LC[��y0�d9�i��,�C<�F7��7�- É-���m�8Ơq�`*�qP��-,S�B���%IZ�Z�+�
bLŨ��dO֓��KJ�Gw�y8ƨQw�[Tx����1p������!����0��l�9�1���J�0�r�Δ�y�����QI�d�4��\
x��шy���a,x`�����Tk��3��U�<�E���]K#UD���dh��TO#�Y�-�Q�Q�#�B���",��z�R��R^,��:H
���:,��aE�+� >�cN(��� r�J_�1,�\-�ZdO<YZ����G���3R�$��P���	�&JP=��	�B=��:�C���ʯ8�:���A0����&���,�z��%[u��4�Dv������ՈK"WL��0�dS�F#�&�dI݈x�Tϵ]t�EIi�4�RB�[�e��<�T���)ݡ�gV�ĸ`��R�4��]�m�G�Ɂ�8���_tU`[�G+���=�sD«x�8�3tC�������I�(>U+(T����� +��L�
�[4I!y�Ǥ[X��1����%	/�#��Ei��La��W4ct��d�ùMV=��`]iӬ���!!p����Ĥ$&��pxï4MU�Iee��o�C:��荄/TX �Pt'1��v:�tr��]bx9Ϭ���E��y�H{����ZG?А=�y ��ǎ�MU�{�p��m��x��ɀ�~HW�N�f�x�ݘ���%3��N�k:t���,��9Jk�`���`⓲�H|��$^ ��r��^6�.����c��KƔf������E[d<rL@Ƈ�"A:i�F=�+,�.H���fl�ʎ��BVdV�ӟ���)���= ���R���C-���i8C/���6������&^�#�J�2XX2� .<�0H�P�z<�=p
Ք�dQ��z4he�h�l�����8+ �A������������&�� ���dyX����hk��:�Jj\�>��,��uă,���@[��c6�.|p(kfp�bG �m��
j�õ��5���[�hNm���W	��Bɲh�|DA FD�Ek��<�)y��BT`�
�Ҵ%Pk���:�Ȧߜ�F���'{|��NEܨG��,h��ݞ
����agL�1|$q��GJ�u"E���E�έn
(�z���,(Q"e�������R֮ �{�Ў��<��0�*�	�OP�N�F��F�B��˛����4Õ�klaՊ���y`��N)���:@�-t�.�X<�����1^�6A+AաhWu�>H�5^n0Cd�s5IH+,D�x�H D� �A�GA4�\��jO�����V� �|X3��+8�Ae���Њ�v���:�W��
���P��G���<�!�6�Т����_+$ÿX�yEC��Rz�ih�Ww^bI�3@�Z��C� 3�n���{�'|̓�y\mccT�,�n���(JZ`L����Q����������XQ}C8i)6B�֦S0�_�3`�yx(�a�
�/]�1��`��^������^$s_�X0�B$�B3o�y&5�+^�v[���+&2�!0�s�s���+ C>�B���
��Q��J-��5҆R�4�B����?��	:�U��R�������-ʥ��Q�B���4Pm�D��#G�M��O���찥ўt��4��nӜ�5��D];���'Rl�_��tw,���o�@;0��0� !���m����/�Du�@Uo�WL0�K$Y�U[��2�et��00!8�(J���X��qf���~�+�صa�o�ݒq<�q0_5>�C<�C`�C;�C>�C>��>�C`7�`ǃ>D�:�;��:t����`]�|�.�Bh�v�L�iK�.�¤��.�vkw(���,����+<�+�Bp�B,�!uph
΂�a۵]�`fs��B,�.�sw.t�.܆f�A.Hci��9��0��%,Y�x��@K�040 쿦G(�L��TZ���:�l`E֕�@��� OEE)ZG(��$�z�s7�:�?�DaLE��	RT�}]Z���]x��*�j|�M-���5��8�5��d��R��s8�m���.��7��]�`�rJ_+s3 ;   GIF89aj�  )"#2%%#!1%%#53'420$($)''7,1)+295('3+594*768A%E"B&(E'.R*5G+8T2+E3.Q6:F4:U2:r:D8@LMx8ET*Gh+Jw,Pl*U{;Kd8Ey<Sf7Vx=c^<bhHL&K.I8-hw"j/p2+D<EA>ps:AYAPG7Se9bCjO4od:HGIFKUJSIJSXTLGRMSXSJXVUCLdFJvFUiJYsVLdRNxW[eR]uXjOFdiJdvYchVdyZsj[sxeYPb\cgeJhdYeuIgwXweHuiXuqLwsZhfhhkuithhuyrkgrmuwtivvx3>�S�3O�5e�9o�GR�AQ�Gf�Ek�Lr�Jr�[h�Vi�\u�Yv�Jw�f]�cl�dj�gv�hx�ul�rl�vz�t|�n|�o�:t�Qt�tU��j��j��n��x��x��v��z��u��X��m��x��{��%�1.�74�=D�N4�i8�S3�d:�UI�qR�yo�ZE�uR�xi�Z6�k:�\C�tM�{f�xN�xe�|��z��}���>��Z��v��r��W��s��xŌ;҈VΑqЦ\Ҥw�U�p�Z�u��z���������������������������������������������������������������������Г�⩸֪��³����ڷ��͙��Ҫ�ϴ�陇뭒鶬Ƽ�������ĵ�ŗ�ǳ��������������������������������������������������������������            !�   � !�MBPW������ؿ��ؾ�ؾ������ؾ�������ٸ�پ�ٸ���������ؾ������ٸ��٫�����������ڸڷ��ڥڷ�����ڥ�����������ۥ���XWWWWWWWWWWEEEEWEEE�E�EۀEE���>�DD���DD>>>>����������ۙ����������3!��ܒ��������nn�nm���2�22222�2����������,,+++�%�   ����������������� ,    j � �	(��4��&Lx�a�l԰%�H1�Ņ�i��bGj� v�&1��&S��M�B�.3.d��Me6�)�I�X1e?�
ݩL'�b:��\��MlEw�6l�O�ÒU6Lذ]�z���/�hy�R�+�ڳe��5��0�>�Kƌ�Mh� C�F�Z7l��YCg����ȩCG�ȏ/S޼9�:��ҽ[�������S����װc˞Mv�}�X�ˇ�^���ڱC�4gɎ	����2D����س_��{��%W�R1�F�[�L�6��[�7O�=<���g[<��������y���z�m��|&���uU�E-��Fa���F��A��`��$"MԨ8�μ�"�E�NM�D��� �4D9�2����$s���)���$��1�(��/�`y
'g�A`<�ud$�vh��!wم�]x	E�Rz��ޝt��MH؈�<��Y҂��g�}�<2
���2�g��=*)��iR�ʔ�uZф$iQ���5$M�I����'���4aJ�k��Ǟ������Ù��Qq�4{X7��q=:ӌ��3L0����g�	F��Pu�Nwݙh~�f�'�*o���:!��JԪD#}��I�dߟ���h�cꨪ��w)��}W��~����I��Y��4���5��j��ND)x�aE����y,���~v��Ұ���s:��Z,{�r����5�L�#4�&���gp��e7�B�Z�5�h��	Uk������|����o��*�<u;Z)���2L)�w�1�v̱B�n������,�˴rj����/�trN'z��3�@�tУ��p�1��qIC��1Wn���]�{ft�K�}(�؉{Lx�*#�v��SJ1�
����	C
��-ut3z
�:�˞^縻z(���_D+������r�Vnk����6���3�� ꊵ�e%�Y��Ӑs-+ukUH4�6��(�w��ݺ\�vi(|���"r6�m�U�z��,e�G�A���V+́p" �P���/�f�1[��f����{r��6�)[�W���>ӿvX�t Ģ�X@f!PZlF��U;.�.w�]�6�!툍|`��˾'$6�T����������N|��B�ǁ�{��X�������qhR���>zM��򜫐�!M��< ��y�3�I1����ȳ"�8�g鬧�Z�Җ1�u�3�k�bB�u�ȋ�M�� u����ђ��ީЦ*A�=�ϡ�W)?��nӻc"��/�h�q�;:��F��R���45�Z��g�sY�NbxT�=���<�M_�Q=�+�Jc!+����F�5Ij��E/�vFu���\�������%�\߇ַ�|�ꄦ2b���6<%�?�Z���a�?;I��K٩s&e�ǖ�<�:���`��>9ӈ�����R�O��Q������O*���a�.h|��\GK�R�8�T�.�ŉ^��(;y����p��`�Bu���	��2��&=�Ru{@=آ.�S�y�C�,�����#�u�I?ز�5r�#�5w*:qV^��{�Z����eO*��x�&��i�qU�P�PYب+qSQg00��./#�Q����,�ku����#�P�bEB��Ѳ����� L�t���)���X��=[�dk����~U�m��	<۴UȄVEdnC�����<,��z�ǳ�2��M<ֲ֡�24U.���\d�Y�A 4�Z-�&G��ҷ	ܥ)���>��A��7^�ʓWk(������R�O�SAI�l.ĩ
�˪ڢ��]v�C@�Cj'�˜���ė*����J ��a!JQe���b�5��#�B��eŒ��I��l�$zkKgܚ���I����%�ͫ�n1]r�ܓ���Wz}B�U���N���d'$�>g�rS��!�=�y��*��*�axX-9P)�ٶ��%&�}�;��/fu��4�6�J Z��c
z�w��Zo�X9��q�ۤ��)Y�Z�>�o��]����x��5���.�i,���a���b�3V��jW�C�a�@a�)������eeh ��fmC�X�~:f���$�Z��F�iI���^�2ۻҋ�����H�F�0A�뎗����<^��
�Nڧ��v�%8��8/5K��ϋY� w�����L;Y}���2���":�1w�K��.9�V�1Ҏ�;0on�Q:�g�x���g֑^(c�LoFM~�:©N5�M��ȵM���zb���Tm��L;��Jܶ�<)�M�F�g(���W��b�C��te�FFOi����I��d:��Z��Ր�	���#������P�j�D�<j�TsC����޺��Vx�O6�3�V�ʬim;V�:b���GRT�RPXJ��\���Uwt�� ��V@�;��hم%��Q�'.d߁L��]Y�]��sI�x��`B�/fSN-���N�b6�e~��~U�I�gL�`��Z��Zu`?��9r$?�7+�c7�G{�q��Cm�Rm��:��E_�ű@6�K����%Ush�A��Q_R7�L��Xb!�P�u�<��e��'�Ǉ�Vy�Y��)5�<j�<�	�HCu��)1�n�f3~}�'�s)B{2�+ $׉Dw�"�s�:��:ۆW��-9�%�с��1��&%Xit!�#zkT<�p�Hl/U�Wt�p<'�IRB�H�^И*�7p�Xz�B9���Sa�!��hTRX��ӉH@G�E�F-�p�-D���Vϧn�gio�.�buhbz�d1	��5U�Ҍ�uu��IO��$�p�u�f�ͤ���}�WC�C0Dg���uF��#,�|u�����qS��@����Xnxn�R��\c}8*e�1iN*�~�gCD�e��oY<H5��c1�T>�s�B���3>g�<�7��>*�[14��g��0{BQn�,ڶ�`�$Un�%�i���5o�;D&h�F"H�:ً��1R�~��ǔӤT6����<�u`��x<����C@���L���2�3�h��,&Y�*Q��:2fKǡ�w|�#4�5�y�e�5�hAd���8V�>oH6��Z��.�2�UwH�	��'��O��!��u�vR_��j��޷���z��Dr��b��I,՚��VQ@b-ɀ���|�pX�c�A���L7�.u.�a��W&m�]�9���;"(�C�x���x���Lo#UM9p
7o�Ǔ6f�y�FXh�2t$o�6��7C �a�:TX�uG�	K�b�!���I�]���id�LjVP�x�j& ���b.��c��;�3�#�T�Y�^c�#Ry>.8p����1��+5[�#2�%�Fx���Z�	�0����f��L�1uH$q��X{�94��:��h(��%a Y`FP:E���b��	�:�U�;���	꥽٠T*2k8��¥삜�D6�3�)���_*�v7��}�2d�� �P�s郄����&���3�IZ�iPiٖ<�\U��ˠ$���yf &Y`Op�j�`PF��Y0��#�
�cҪ�٪:�ZAY
��W��7�!�������Hfs>�j�ⷫp*9נA��#fk�n�Y�iDI��T�i�D��� {r�%i�[H�#��]�zIj�L
&f@�\��;k`"`�����������J�
�������Y꓅�����T��1��Z���y[�>��& IX�6��pB�����M���'� ؤ�Q���DQC�
A�`b d`�{���	��
���U�\bb���f�Bk����	��k(��ZAM[�d���c�g���e�����4Rw�����v�8�
2���w�-3��.Vy�cV�M٧SWD�3��C+��y��`zd��ÖPA�_�������K����g �⺟ܻ���3�%g@�
g@��Z�5���`Z� k����ix{���d�1H[L\ӵ
�V{��$��M�L	�Dx^j6^�Zy���<�*���2\IP��8�3�p
�P��K��-������"��W�ʧ�����%��>��ĨJ��������8&G|�H�T��;��zAӠ��1J�Ȥ�����i�m4����Lj{�KR�>��xsD��j�vKq�H�à��K-��t
�������jȾP��K�d�%犩>\D|�� �KK��z�H�]T�����r�z��A�z�C;�'�f�d��2[���Gf�W�
�L�dUQ�Y��E(��8 9G?D�n�it|?rl�+��� �d� � ���-���[�IJ�_�Ȗj�j?�é���J�C[����L�h��RZɥ\����L� -��m&2���%��"�R;�a�1�4����k���׈)fw���Ä�D����Sq���1�Ԡ� ��U1'���¬@I��J�`�7kV�Ӑ_�F���˹�L����<�������a��˫���;��G��5�um���s��T^�16���~��}0J�p�X����y--����L�ӄ`����lY�Ç�ӎ��E�?� ��[���T�O��N����ϕ��]��A\��,�Z�1����-�M�1�ؐ�m�L���
	U�%��5Mt�6�CMj�s�$9R���0UP��Pq\���4 �3���^�%4[É���K8}�U���{���E�CP����=�}Ė�.���cR�J��5W�i�߰M�P��9�u8ɜ]�f��~�NʪlЋ�5����؇k�8o�ID^I��$�~QU `��
:0 0#��o"}� s� :1ݼ��2��K���Y�7��-�	���MZ�-ߝ�A�]ٕM��ڹA|����՞�Z|A�-�̯������Sзj�^��]�Z<]K��H|���5�zk�x�]���*B�}}և%9�0�E��� 0@��C<�+��+�C�K4K�����Æ��b�ذ�����VN뺎����b��	��c��.��;K��j��Mm�A�f���~���>���:�2EhN��Z�e?��`��e=��0�� :�
܀:0�#�BD��2)�ݠ��P��~����Y���` ���E0A�T.�����?���n��I��in�����m��Y\��Ź�;k�`���۠��]]���C�5Yb�I �_�I�� �  :�?00@�Ϻzw��9���2�� A�~��
���N
�/ߗZ蜩T��g�����Vk�fb��9�l�"����.���Ǟ�jN���KK�U[�� �_��*�~�J��˝�~�W�G�U��p'��0F� ��Q�R G�Z���Y�+����K叜����>��M��b/����Lj?��.�.g���������J.����ڿ��*��>����ŷJ��4���(��lSZg~��y��V?��F����� �6j�eÖ����٨L�-�Ą�Y��^���Нi�0YJ��b�$�,,��LiF�̒"�1b���"V���TZP�D��T��jC�Q[�jT�U�^]
ujԩZ�NSlU�O�6���j�jP�JC[VmU�ז>�:w�5lV���6*ۣp7\�aP��7~Ha����E�0��Ț=#�4��3�$h���?�#��`�j�D���a:j���H��Ŏ��)+C��,f��li��0�Șau�N��(Oi�JJ��Q�\.TqҦZ��E;����kҭ]�6,���".jƒ�,���>���K����*���
�/�j˽��L0��b�ć #q�1cqD�"ڬ��B�F���Xˠ�z!��R����M�l�h#$e�(J���a8��c��3����� S�]X�Ό3������ ��.��Ы��S�-��3���j�@���N��B���*���/���¯Qi(�K����/<Ղ�!j:u�j:�Ĵ��L�;_͐(ɤ	�S-k���"RR�i�(�)O�`~���!����]�����d3h"&�����6�H�n��_���K,�$��Д��_�٥�]�<�&!�X�8�8T��jO��`���Є&ԫ���KQ�*��*,L�l/�4��R��j�U��<�-;�i"W�J+�k�3�Q�M��$�s,3�F;�HN����`@M�� �,��V8��M'7���v�({޺�KVv�r�.��r�q�lx�<�l#�IM}�����"@���s�C�������8� G�lՎ��C޸c����=S��*0�
s�@O�y���z�um(Ek:!��q�M�1���a+`X��n��Zl�e^8�0w"t�)��ٖ����<�]a�'?���F�{)�ä8��eO�J��`��b��U�¶JT�忋U*�[�\�"4�L)�t�����D��.d�[�"�IFP���A,#���2��Lh.3Ǥ��0�,|+x��Ё�a��ID7X���ĵo}\��0�!�/ �v�^#�`����a��nk����	���&��B��?�K���0�eb�ORt�]N>�R���:�D�z����%.�:̨�"*QE���KDl��h�g~#�pC��*?���BݰȔ����Ւ�Pr����YRI�pbw�����L��"�Ƈa��LiC�)����i����g(���pu����t�mV
����R���.��>50XT�J��:I*� U��5r���W'��:B������&.�	np�8��R��D�M���e,ӑD`�n�PĚ���f2��h�1��:g '�� ���P����<�q��OQ���S�]�r�!&A���R�
�ӭ�/VM��
&@I�T��K#�!�bBC�Y�C�ɠ�H�J"C3���Vz����%^�v�"��e�4��J�j �1�x�a�!c�û���킉g��)��1�ac	��T�kJRk�P�y�o*�_*R%D���/��*%��F쁪B�]�<��(+C�Yc%>4���.ld�י�D��Lf~6W�#d��+^�
O��� ��Tk=)z�%�bi5�$1�/v�E���b��	����١u�0�<�L��)�������r,���Yڊ�qҭS�I�Ȃ��B�\�W��Ub��zlL���5�|PD#�\�b0��U	:кb&h4�]D���	�`�p+f�Y���Y6�Q�ƖY���/6�q�5�y��(�gwq��B{�uNݶ���IM4%�j��2��z���o�6�K埣��<V�*� ��B
D���B�*�^�(o�'Q�H� �dO9kf�1�aP��ʠ�b��<$gy��I��a#���`�.l�W~E��1�[!�ʈ���5S��	�64�������/�6v���{fC�4o5��	RX΅S[�9*����l�wT7�8Ď�OY�	�F�K!�u��ʼ��0D1��!�C��`�)�j���0�BU_p���:׸���ުd��W�Y��Ё�f?��0":Qd�Y���&A8#Μ96��bx�f���j�8�ݖ�'bn�^���1���k"��&�1�Q�m�-�p�h�/��?U�n���F�!	��0���A�=�}�Q�=�
��\�R�!Tߓ��T���[�eRs!�2|��d��p�v	ՏJ�'d��0 �h :��/�A�W|��0�$�� A
��6��"���:2$�n'��Ţ0x񇮳��i�Nrb�堖Mf�CS8���� T���A�x�w����>6x����4��B F �B �5b*ѧV#�����Dk1�X�b��Z�jH�,9�ۮ�����$j��c��ؑ�`� Hv��Y �w8�!�	����=X(%��#����n�
Alj �cP� �@P��DH��مS�30��	 0�� �")��)X3
�0һ#��ڦ����

x���n�?m:�b��0�	@�"H�4̴��8|�8B0�*�4�� S�,9Z�K�+��Kj82g��'<�
\�%�$��*)������� �� dxR�(^�� �� ! �)�T,��0x#�6�Xj3�AZ�"^��a@�u�3�](�"�/G C�8�1RC1�7�0������8���h��w��u�j��8��;>�[K���Ё���"|�l�B�B�S
ؐ1(��丁8�[��c�AR<�c4�Z+���뺮F؃h���Xl���P�W�o`�_�����!H�!6��:ƚ���:�Ӥ�3^P��F�Y�"�"8�fJ�"h��'�v\0h�w$ �
�H���!���vxw�r0Б�GۼcAi2�&��� ��#�S@B`��S̡`��<L9՛��6L̐����`��5��6��<
\�[���2!S�ax͝��� �<���ܜ��Y�p����8 Z|E �a6��h�2�hT3j����]P_�B_ �0�0�dH�bHcOah�x#��1؃�bGxkK��:�7Ļ-�H5���qtxqӵ8Z�D�<�I�=�3�3�dP��Kø'�=Q+�C��1ȮJ$�)�BnL�a�5Q"���R+n�L��đ����@S�5j(�MQU�0H�� `  8 	` �Y��Q���� %��ЁXJ�|��S7�����J�L7�)�0�d8���_Љ2D�=�71�wt�l�;��ǋ��S[3r�Ot v@~�Q{,��+���3݅D8��[+oTCx�O(�Fȸ:X��S
_ >�.�B��,��3 _`�b�L\����]]QC1���іPh@IK
]�^�^�b��lR@�&�ܴ��[�VK����&��#P>ֻ���4Z�.�$AepBi$tP�a &_��l�Jn�,��,����C��#p�'�&5���	)؊�R1	q*��?E�;;Q3�P��y�t�w��������R)�lL�M*u��k��b�O}��4�3��̸+�3�1�,��.�/�,Ȃ]8Q�*�a�@Z�W��5dMe8=jhM��5��#c9�FU�a}�0؀�h�!���MH��n���(�d�@:"�5��ѳ]Q^x��!f0=o[�ƭWa���D�1(3H! �te(=��dH)�@���$���j��{�؂��G9�ˡH�D��w�]z�t`z��cH�Zk�S��M�;ue�Ap+"���YjP�*@:B�Ĭ`V0,��Ё�x6.���\eE�ah9e�Zь�3�W%��8-�g�BM]є�F�U�O ����@���@`8  ��[���> ������E���_�e��+�"��pp�'$� C���?(���EH���$�����	㫃�b�2`K0��us�&ޅ#D�ǁ�]FE���zx�}Ȼ����5�][���\-
"�b�*0Q�"�}�1�8EHC�*H�+�:���M�VȲMhU2؅z�\��BOf5[�����c�Z����;=����-[���bH7e��`�ԸM#E�  & ��Q�K W�͟3���M�f�_a��Z�%��]��塆��>���aj�y�B�JPbZ�c���)�
���j���à1Fʊ�K:u�rFFm������&"�`h�^�ܢ

���K]�D��F@��`�b*`��#* ���8C�1~����!�^�e�W0��2�W��.�c(d��I���MfO��I�Q�|Pj��n�bx���Z<_|��lD�f�e^R%`%-ej�K��o��
�E��1 �}V"{V�Sb�.=G62#˵���\�������3hC20C��	c�R?#@�&0]5T?�k��]���]y��)�D}�&�]v�$M����BNh�M���`�ԊbH�0��NC�*�C`��D��Oe��U]�~(X�'���2��N�!n�s�ms�s �W�+�V����x�����]������=d����_MF�D�U�.����}I��O��Ԁ�dQRRf`R��p��\�H�	��<鮅�t�O���"s9bu_��e���u�N-&<5DC
��6���(#��k�TH3b�x�>\�2���m��Y��2�wx�{H��]��i�3 ��g�c=�2����$�'�����O�$�3��-��-���x&�H4&�b�1ڮ(r����rs��M�^�B7F�F|]Q)|V|���,[��U��_�pZ�N�lV�E)��%"K7E`Z�n��IA�W��2P�X�R&� ��L�N68�$O�U_9��b L��a0���F[���-�FX�=��(CB8�)�5d�ԕ�'X?0��N] �����]BiQ��}D��]thu0�]}XY����0(������n�+�����8�<����3.r1%Z3�0r&&7r���켉W�h���-'����/؅5�i�.�z�__ �?P���Q��������-��U�b�\C[Z�C8О�B�IՀ��<8   j ��gj� ��͟ۅ9��:Oa�o\��nVc�嬴x��=���72`��
��1-Ek���<�B-/l�����Co��a �#�nHTD%k膶_� ŵ#�]u�[�+ ���{��g�{2�ڠ���Z�8U�TBP�3�y߂�er|'���AXm��������Ǉ�6cX=�`�i^x���_Z��U����Kwi'Lg8���}_�\�܃��}�3��'y"�=� ��#�+mۯ��0a8����d�t��Z�*~?ws�E7tK7,\�DPY�2�Z�1�yq����S����L�0��	 i,h�@j*��P�4�	#R�VL�!)F���ĉ�.��~�*IRX/a*�+F��b�|�����:{�йi��8��H�O�=/��EH˓AO�<��j�,Ov@��Jh�b���-���o�ș3���[|o��ZUX�a���/�����"��/ą�AG�dԠ%+F�r��d�)�YY1E�Fͨ��`� @�LhЁ��	lD�㇑n �1��b,�����!F��V��/���J��p�?��)��H��3`փ93f�3�ԇ�uf~�`�Hi"D
�a�FF\1B	"$AQ��B=ȐK��bDf�Efl�QI\<Q�����.���/�$�"!�l��+�x��+�6^��%�\�'�0$�/SI�0�"VS�@�;l��V�HiƈO��K1�SD�p��Zn��[m���*bs_�(g2��Y'h�`7Ls��d���1���PZ_���u�0�H#����huRGUbH�6[�l0AmЦ�L B�L�n����f�r�"�u�Ys챝w{$rl"{챋e��ȣ�䇟`৒/�L{F|��B$1�R��a(�n�J(�����@IS��J��H�C:��S���QI%��L"b��&��2�7Sˍ����H�DTL#aS�Ecl!F�_� ��ZD���l�+�T�+Ep1f7�)8ߐ�M[��
8�4�^y3L^,���/��x�f�]��|�ɧ/�&6g��,�u@QH��0��:w�@ jD��|�0��6�Hh� k�a��a�!�1�HBH>�hc�a��c(�Rg�y���t{�����g�q���q�`�D�K!pE�E�����RČBF@@ 0,��0���1���1X�'�����P�Ebr���M-9���*%D�"S�K.񒈇`=��WZ����-d��`1��cL��F[�g���>[���a��p�9,9��86���{���wxќd��2R{!���B� �;�����)CD! t�   ���L<�S�c@o�ub n:�����MkU0�� ����r7�0x^D=c�[w�?�.u�9C��#Į?㚂:0� a���)<D��A�b =�}�0�A����������r�=A�x���G���O���.QfPdM0�E��^�*O����a���&��2���,cz&7�4��I�g��F���^��,�"T_����2����v�d����K���PzQ�p
@ �8ā
�5���0���S�ҁ��O2�=c�!�(:/e�lɺb���|��}�P�2౥�b�z�P��+	P ���#0` �l�"���y�B��HlЃ��=�Ab�TWl����K�a�a*Z����I>n\�G�8�&�@ˈ���ʋ2ұ�}�[�� X��,Hie��� �BR��A5q4�Ll|o1����f/Ũ�9C�,h@�̀F�������)1�Z�c�Lox�D,���Bضԇ=��FD��?���zC*��bP堻��L���QD2|A��Nǌ�$̤��Թ�=,m)y[g^��v#�]������}�_vI��B$D\2+hB���6�i�[�9��1��˥Xb ��(V>U
�;�%8a���"��=�b�cZ�����>ɘO��V�c����L�XU)��E�R7��4^g��Ze�Q�f�{�׼ӽcq�:�;#�#����� D� �*Ta# �j�Ȁ шr�� O�� ���Ћ���ҥ�>�e�0�ӌƚ�����bY�	]�Ơ�����;i~Π���4)a��F0��D�w�]�Q+kGO����4?������,^�d�(�NWu�&1}�B_�j1�E�IF>V�m�2����^�poa����z��S���c��!�3=�L�8�0�&�{^�N���u�c%˺5D�����*ZwuX�h�`��,��X�*��"֙��@��q� :��|q9~N�͋{O�%�8! �	1_�����t���:�=��:n�1E)B�.N1� ArG���_��z"y5~"��g]� �����W��ܘE��t�呉���V6����F�p�d1NH��Ƕ�r|u��$�_����!�_��&X��1M�d�d&c��2�C"�wEFp�:z�ӱ�[��+�R���!,.q�S|���
!�K���C�����`<cv(�H Ā$E8����L�r0� )\�lͱ/7�{JKY
�ڣ�q��A���`"Дv5��v\�~��W��BI��˜��4��,L�������PT��+�7�B-��7�1�C=�C��	���C��3�L�D�'�ϨRϼ���O=�1���l���Q���wlG�)ZL�>��d���R���Y�t�t�� �^�@@	 �O0� �j,@�t�!��A�9��L��T�����t��\ΟF��߀�q���J��[�t@ � ��� 8�!��C�L"�P5�Ǵ].UQ�2�6��&��'�]H�+�3�K�v�/����- Y��7��,h���C<������!��,и3M� `+�	>@P7hCό�8Z�3ś9�=�F"XN���`�hD�́���]
�a
!T� P�DQb!��|�Qk�1�@�, �I ܬ!X���`A��0T�"T�(ZYNE�!�$�{���g�/�G{a���$�~^����i���hDX��؋P	�����W�<�E `#]�E��24��t�,���t���b.*D*Z�"d�/r�6h�b����B1��ݛX��9����D��,+�B3A0D9�8t��д��@К4[9$�3p��KH��\�I��IJ�D�mQBuE�f��\�M���n����Yq}�D�f��@�XITˋ���Q�Y|$1��$�YN�צ�K�$����i��|]�'�ݒ� ����T&�K����̽t݈i��\]Ap���1p��,��} ,��-槱��u|�& �,��7h��\L-�BdC3 �1�C�񥊡��̠��`7X^0�A3(�vCc����%\V��hC9��ó(�I����D��u��5�t,Bm1w�����0i �c��=�hfJ�4o"�W����!p�pA�(�E�a<�A@JA@$蕋gn�|GvKN��{ȇ���w
0����|M\A���%AR>��	��0D�#D�6�4�}�"A(�h�@F}^ġ8�c�C=�C<`�~��q�|�O���7���b��f0�"��p�+蝄&X坝�]��'d�3�����>�+����8�4��IC7|kҬI-*k��O8FS�|k7DC44�=��l��x���Q�t)�=!�]N<������\g�A�Jm�S0�D@�\�@�� + CIp���{��>�#�)�$�y&J��e`�x��|�d"0�BN6Ǡ����_�d �6����<�4��(���Ҋ_K�ݳI1l�Z�g#\ǩC8�C>t�=d�xL�PH.1F3X����̂��H-��)J��T!��Y�' �٥�B�q�&LP*<k�~(+��bUL 4�B!\ٷZ^�M�҈�����6����G˕�>yG�NA?n�"��ō� �A<�Y���ӱ�t ��\�2� C���T��jTl�@ �"�8'�d��h<J����Z�@Ap����$�5�F�N{�J	]��A�A����&�Њ��> ܋�b6��E<m#J10#�5B!��t`�K@F7D�1|��Q-J.Y��(B�4佂'l���� 2C2@á�eP�����.��u`�J�1J�p�'>$��!�*L^�U\B#<�#��0\�5]�cd��5=7��9<�@�Bnm��N��}���FmQB����� �M�J	�1땍�<�j��k4��lik��E �
L�<�3$�K�)���Y�@��$�Kܰ������z�'��M���׀h��!M����4ѳν�uT���V�C!��!h�W��"��@\�9C�v�6pp>��Q��A8�8��4�2<�1�@��mUU��pBOF�=KF��,�m��B�B-Ȃ����E7���9����ñjPX�߁4|+6%9��>S��Ϛ C-���4@$4BXQ;���te��n?�(�I�^��(��8��8��x\�r���p�Mk�}
�t@�� �2@FZ˹�E�(��i)��L�B���P���K{���H�,�΀��Q�C�4���@0���f����u(3;�(	�M !P��B7�b7x���C:�C:�_8;�3
��:<C0X*���,�pA�Ұ���������@��,�B.�B�\�u�=�s?ws��>H�>4�>��s��c�e\P�#N8d��)�+�	��9�I-�B4�24BJC�# �#K�N:a��C;���8��&t�P�Pa�M��wuj��oԜT�9T@�!��FmL�1 ���0h�����h'�����$�.�4�W+{�zp�.��|��G��K�6$%ю@����^��6��Z���T,	P��A2\�+tC;�X>�_��$��;���x :�kK��4��
�D�"�#���!h����'���6@�BH>�Z��8$�9��>8�9L�>`7�_�><7>P�+��\�}�w8��6�[4�f��z'7 ��P̛(-E[1J7B4T�^0f%��!1!�5�S�XT;f�����I�N5�.\��A���@o�Fld;��
��Yh���2�ݔ����@�I�%BX�g���c먎}��[i�[q��&A u@`���_$�a+h�^�4@
�t(STX�W�2t��%�cP=C4t�1 �Ϟ���6/��jCH�L-�R�h�R�!2��0$�8l�pB+N��A?S{742}��[��>8}vO�t�;�[ �G�A!B�A#�A�n5�B��$Cz������3�b[4E�#�4�Đ0<����B/���XJl�{wV|�=���a��N"� @A�a� ��X� �
CU�� 0���
]��$)����!�I��{vH���N�y�'���{�%�=M���$f�t�$�/@��a+-� B#�WDET���ECk�mC;�6d5�C8ޛ9��9� �1p:t�d���	[�L��XB��0 �3t9�@�9���!4 bE�6n�jɒ8�[-n�0�������u3Gn�;|���ķ�;��Ƒ3��N�B���Ө�0>uz��3'٫W�6mr��#7��~+T�U�� e��3�3�|�"��Pٲ��I�G��Do�"��-]_�|�M�k�"_}���/)   �@�ƌ ���&`��!B�@O` `�� �����/^�y�q�(̘)U�(�=�ذ^{��2\
�0a�8c�,Y�dcƜ9��۳g��$��'L�#�{� � �<ѧW��C��ڱcГ�O�l1�m�������'�n�1�Nl ��P��x�F�dv�0������WfqE�b���f�I&�$����`�#���(O<��"���qX|衇M�r����H#XZ��d6¨�a�J�B�p�6ؠ��;�P��^Q�(����`���JG�+i i䐸�!��D�0�,���c;�芋к�ㅗ��m^N)��h� �*���H�1�.k  ;�`���4b��0N	�]�����C�)v�_��+��� C
1��3�	)
}4_��iY1��3�n�2�0��8�����E��F��*��?�	181�����}���V��QX<: ���H����d�$9�D���d�y�a���8�������O��e���醤�a���d�'I2gdV��hr���(k�h�F�h���ظ��Dl*đBְ�j�5
����?g�t$����m���30���,V�-���뫱X�n�^�/Eh�Ew�#�Nȴ2�[ �#+�2L/렁�X4"��*�6�]���a�x�	!F !�h�	*�C��0V
!�H�u�a	O�3�A�u᎓��]wvz#&x��t�-�;a�y�?ݓ0"_c~I&��Ȋ~��m8�G �0�@ �SXa?��1�P+��2��б�d�� �w#���D��
+�8	H\�� 0X@tЃW\�Ё��`0�F��1;��&V[C���D���X���������b�8djP	8��#�4F��xD#x��?�eh��"hE8V��e��\�a2.|y�)��8E�!�9@ .e��L�� ��Ҁ	|t���ׁ�DV�A���&/����9 }��`�'�BxB� �e����H�;�`4�
e��3�1��5�Q��cQ���y�g��5����3�B"^; �;���!���"�nl��@�sx <� �<'�\fK��~�N�.�C���0��� <���}�0�6�a�d�C+	26!�M, 9��}���B*�K����Jp���Ć4�a*i����d苋\|E-�1R�;y�4"�FE4��q#O|�>Q�O���o�ً=��}��v�
Zq�V���)�j�04`2	q�*�XKj�2��'=�ځ�U:(B�P,^g8AB$�'� x�l��0]6��!2��G	#��x��2\�G�Bv���0L![�5�����؉��D�sqD0����NA�  �� `"}��zE�w�]�apo4�$����N�0p�H�� W1�	�@Gp�!	�P�3h�n�Cfx1�ᒤ�0���k`�MԴ5R.�d�����ְ:X-�\2ĺ���u����UAd�c�wx�NШFG(b]i�V�D���m 	�G���wg0�X��_h���/�X���K���G"�1�I@4w��4�a �@�Μ �$� <���°]��x��h���U�V
�¥Q�1���r�/���_�".�H3'4a�&�!	���q�@\�:�	@���4��1"�Q~�{�ke[�	��.X �xn �@8��!�����?;ڀ��`�p��V@	�C�`T ��M��0�Pp�0�d@#�(89��3��{#�q K���4�C��
Kh���&.{0�Q��԰8T�KVc�T�Z5;��xF���ccJo�ɗ����$�Y��V�d��t�˽�U����_c�T?�1���c4��hF7���;#>�bP�l`r�s���n��U�  @��� r��`��.Kc�gy�Q0�B`h�i�P�\�`X���K0!E��.y��� cpq#=��w
S�u.{R>�{�h�!��*bT�&�����7-�D� �y��'�(5P�����g@���T��9Y ,`� ��MH��'x h�@v�jT�'XF��4�Rpپ�*���fF��)\!)��� 
b,��@j,������ ����
d_|��jr"����h*!���B0I�~뚁��b",�
�.�0��4�
p��^��\ �b`^���k�c���&`@U�g8V�8�$;��P+� ~ �r�
� ����������X��x>L z�8�EP:�����c��a����a�
�^j�g�6��.@�2) @�Q1�.���k����
.�(����	bd (`�3k `��*��������3� N� �o8Aj���)��!����� N���hL�l���@�7�!��f��&��@' 2^n*l��� ��n!}������a"��!�a�!��"�8�#��"�#����v�R�>��m �p���2>%�n�P%	��;���:�+�����x�� �@�v�Z����!g%������`�`x�`�P�����h)�A����O.�,Xa�ˎ�	
ʢ�4�v��S�< �<�+�����` ��@�:j1Q�6 ���<2�'�)���ѡ�`6 �A|��b�C`P�C�lV6���	� �`��&b,�Ҁ�!���`0������A�  �f aNkx������ځ���!���A���IQ� ����� ��8T@W�;�� �ۊ���Q��9��~`4&� �+l`'��`���*���w� � _��,Rނ���aVv����/���g��;� 	L�<C�C�xq��.	��JJ�Z�n��ޅv4`�(�30i2�P�����/ A6��< ���6�/a��4*������"��� �P! �t�?�$7�bjA�r�7��R���s*��(�.09���:�A0��A��kr�r"8�SyBk^�!�` a?�?�@ �Qe�X�u a��IAY	bYa�&  ` "C�S�nI��s,Es0�1J�2�s�@
eQ�"8X)�,�J-�!z@����	+e������� X������-va?�c\R�U�����Za4Vc	�c!K�@��L�/Os��S�@�NQ�=�3��5�k������h�d�4L;�	�H� �f/ a�D�b��"�!��#�h�R�`N��`8�@���$l� l�9�a�
������8ـ��sV��&���ƊY� $�_�X��
�-�LX{u�zu?���6e1F��, �N ��Ƞ��n�
 C! H	��~�-� x���rZ���l�XE���"z��Qv����A���xc���?�@�j�p�A�tV�z�w�w�wZAd`*�LW� 3�+� ��, }�t��t���l�&�P ش��h�g����i�)�<�蠫�o+��0Oj����l<x )t@Z`Z`5U��8٠���>p��A0ቐ�jV8<�Vk�Aj��A�ۢ��UZ����"��-!zw���8z�`p�	 2 s0W�:w�  v��&�.� ]�,�6�v����vAV�J\���M�Q������w�XQN!�ʀMjx0��`�V���
t��z���+d19d��{3��6��¯eY�`�
`��1�k3�� Bs3y6���K�Ϳ�� 9�؁�aAX�f��-{��d_̆"Rh{��=�t@@T�N S� 25�R��o�ޠ&$n�S�a�XQ�i�9�����N�����X7�97 ���7�W7�����렡s-�F@ @ �[#C Ư0 �+��~ 2+�*j09t uY� ��f�@x�.�MC�_��qRꦅ�����z�wf�Rå� �`4ac �4�Z�A��#�A��� �gє�422Z�ǯ1g�L;Je:@�#�M�3��K�AQ��������Y}b�N�w�@x3�fMja�dH|(�HG�0�"VS�'��V�>��S��!���a|��AV�1k�"����փآ���ypyBt{�~��lو�|�-�� <j�|��Ɨ1���c� ��Br3H�S t�̀�����!�Y0��y�/�lq�A�Xx��̆�x4�^R�)�9j��n��O�`B��%|�Z�9Y���3�Z2(�L}qL�t~9�~Ya5�t@����g���ǯ��W>YUZd
c
���j�f�vfH"�}� {g"`�=����"����K����j.���syX�;� �&��a��扪�N
���m�j�U�\Ο�ε���\��|�����]����
��A F�F@F� B`1������S4��u����4~�j��a^����Fy�[Q��Q����+'.�BN�Ю�Z̴���b�`<w,��O�Ժ��CV��{ai "0WV�A�1��}Wֿ:?hf��v�����M�}e}�gM����Oa��`
�1	�`?j���Ew�)u۴��� kab�r����vH �� K��Ȏ�K������������Q�p�����^���&N�JKx�\����<��\�h���������}�����|]���
8����G�Z�

�C����{� ��.�2�B� nJ�W�_�4ݲ���7�t�x�Rqgpz`�Ze�k��j�{�@� `��b�,|�	�c���19�} B�����=1T��/�v�3g�a��#�>���>L@� }�=�S���Q��	�`kk� ���a^�t��$,�HȄHF�V�u`��J �ր����s�i�D� ޠ8^�!���a��;ۑK���-�DK��m�z�7 }�o����}� [�b$ب�ADP��`�!(,Z�hQQ1D*ZD�"C�=æ��p� �
 8�`�/0��#�a��)��lX1^ʠ;��(0^���r:,�0a�xQ�kש]�΀1b�Ad�0@ ��l׊�E�� B�}!D�-�-u�
�RW̃� (�  �8rbǎ- ��s��;x �^;z�ҍ�i�$3b�xQ@��,�@���+Ǩ�S�$�%(��1ª[7s�������ap��㒧M�>�X �ǦK=t�02bD	5i�$��НF� ��?
Ih̠�_�L2�ӈp�alܡ`"���P#i�H#�8∃�8"M1�sH#�$4�AcPB)�x�,�H �)2"CP���C*\E#@c=:Ă
P(A�eL� K$P KL0�+�$�V3�;�Ӣ/�P�eRe6��0A�UUe��)�A����Zm��^s��gs�[h�E�]O���0@�]�ҟ^�@�m�Yt��Լ��;��N'��Z�$5�p �QP���!pE4�3�J̱BpF���r�0��7�� =�PC�l�'1���0�R���	j��.��5I�H#M#|���2���5�<�3��a���w��QH�h�QH#�@�
���t��!#&cB!rH�!���B���@$��HC� s�$�A�yc�� /`� A�M���10��P�.�г5ɴ��Ǩi��SU�Sm2�V���)g a�f��
���k�!^��M�.����f�+��BL+�L�dk%@��K֘��f�����G:���9j�|N�$���۔Qp:��+�p(�0�VCβ�8�,\�-�0а�+�l��J�n�1� Fz륡��"G!��ka4v�GTE�����n�.� �����#�T/჎@I�Ō<bʍ 22��+��)�12Q�RT�E�G-P�̢ $�
y�E�E��1@ ���$� 3a� PNj�hQ1����4�*�8FPx�A#+e�ʜl�L`�1�Y� @nl�E"z�]��b2��d<c�π�1�4& P@K 4�8f6 �e&G9Qy s�Miڑ�nX��{�\ �P`����za A�C~ �n���%�c�-�wܱ��Xa<0X�� �kѓCB*d!i��oȃ4�j�Ԁ�!v�LA�eؗ�5�ob��D,	$��IS!��# @��h�&R�/.Ґ���b)�5
���"&���1�!\I�.q�t  ��,E0�0���EV;�/�Va$�(4l(V|H6C� f�hE�4D��O
-2������T�B�c4C��0X1�	X
,���0+hIK@Lc��`f q��Ot�&U�j�&���I 1k�@ .pU�`�G2~�x D0;�K2�9��`�	N|�VP�۔ƊUlbu�@F�t�Kkh��
YΧsh3Rh����	3���ILE lB��T�oc*��4�7��]�� ��X�K5���mK����	-��GB )@W@\cB)B*A@i7��� j�#�
Az�2}�%�S�R�6]Nd3[h2� F��z���"�U�l�%3�[���0*�0z�_���0+��Rմ ��+pA���l3 Omf3�z��P��A���J�16�2@n,`T�A BzF 2(��L�9��M�@%.ВT�ϵ������+� ����a%��p��H#�h�|��!�C|���|.�>�a�T��՗����KوB���0F�@�k#T�r����"5
B�Y���1�a`�S�� �@���}���8Av���x�k��1��݄����8Ý �1H=w��Z�˖{�P��>���dӃ!��1XA�hwE �� `1 ��X�_��
�PG�\�	�8��H�������TS�L�1��f,�ծ
#�v�#k2�����9ܰB�욀�%-L�+�� �`�'�����;�a����"z� -c�ʈs��LL7�}�`�#F�!G�Ҩ&6WvM��������W_�,�*S�A� �p�Y�yC:b���+cC�4O�p��Ҭ��f�#� uT|&_��*r�Q��&�rw)����U��Խ ��6��?%�����C�*�-~���0��3�A�����	H�
�\(3� ��Ì�H'�4�(u2zC�OXBV����^��r�W�FЁ� �	��7�,n0�O�rW��L��`E�A� �� z�*g��
B	�ݗ�V�/t0#!�EW�ϧ��(��JD#t�:'1h��1s_!��pE��sM��?�p�02��?rh�[�303  A0� 0qWNrO*��%�`��`R-�`B�0w�>� ^�P�U6sb'0Dw�4k�6��U������{P���"y�G&�EM � C�^ _�l���}:Pv�����pD���VFA����4�{/P:�SnkD����5F�;&}��� �-.4�B� :�}Āf�@�T�gew #0�p����c���K�#Xl`n�3'�9���sX�UZC@1t
�jP���?�uv���X�"v�#T�$#�0!pc!0 O O ��uw�6)�t%�%��|5(B������!��5X�>t
e�6I�Q�dN�{��{��|p�s0S0M����F@Aa���:?�O)�
2(@�qW��6*�aK�B�#b�l��9�V.p�j�F�A��M�#������p���!}m5d�tW �DPY�
�l1B0�pp.�D!�3�@��E��|�0�X0jP~ĤL�LɈ���6Wg���xG��9���(2�e�P�u{!"�f@��넎#�R�R� BwU��EK��ư���6��0{�@W�[s
PQ�Z��D x�QG$�m����ّJ�ɑ�P�x� G����O�	��	�y`)�R�6T��xxW���9��l���T�3	3p�WH�a�2()cxc@�	�;Y�����pv�4���I|��'p�P0�h0�E!�0c�(W��K����.%����y9��/vЌ!2�xMֹ2�U31�!x#� //��YZ�/$� /�PbJ }V�� `Ab R� a� ˣ4�<1�	��
���X�����ɐ�3�5��&�QFPD�wҩm~B�K��p�|P����|0�1s�u@�À�&l�@/�Cg�3�l�)�������w8G�0˶lxd�	�b�50�%�2��p
� �*�|@�V����d�g��$+aJ(����   � BP.r��n�.�LR">������jt��w vP)�v@�q��ƈ����w�`J�10R3���r��	�1��՘1#2�8��xgA�P9P��lz�`��F�71'D -��
��� tBQ��W��f�À�[���y필C4�z�D{�'{ � ��?�XC�sН��|�Ő�z�P�����)�'������)��7G�0�*���
�W�	�8�F6p�I�WԊ|6�	η��Q��	��Wd�isJ�Ȁ��0)[���x�& �W��$��|p_��0YEf���%��J�4�h�ͤsh0?	�I �i4C1�h�$2���{�/�ڽ�ڽ%2`K��9"�*�00 ��y�#� ��@��8�yB��
�����"5���pJ��Jq�IC}��APx:�%��UQd� ���մ1عN�	�1�ʰ|��@�&����0�Xu�7���2*G���l�7�j	�#	�S:?u)��FKpy����|eV�	��
����1-0�4���i�c�� ��;к1$ ����� �0�wPr;�ݠ/'�S3 K"v@e���:�C�
�� �uPsSP!C�c��{`�z�$;��;��H�|���X�j��&\���!���m1��`60H��տ��-ܰ%i����]�G^�PA�P�_�^:��+)z�� px���&�1X�!/$�{��%"�`$A]���(R��`�������Ù!*M����*��b���0	E9	D� �AU�aet�pG�# @�������
��
��Id���l��}��}�ںO� �kp wp.���rq�yD͠r�O�p@��X+�e�)"����r�>�½W]��0� ��Y�{ �^��s��l�2ӽ����;������!S�`q �F�`pR0Oq't�dY��`KQ6���jS1C����w``F��E��C`'I(�g�G��+{�Z��<&LH1�l�l���\�l�`'��+�QtX��"A*M ]%��&��J:	`
 m�F6`*gp��� eh�C�I:��#^	f���Æ,��Ȁ �`b�|(���8�/�0����PEȠP o��o��li ���Q����>Um�������Cy�S`�����X���l�`���<'�1-�!�:c����6Q� �h}��c�1�2�� �`�����8OA\�N6<TW.�P��ٞ���u��#� �-G!"����c@#��4��=y��� ��U�t��@�"1*���n#�y@t�o��G?4�u�r 6p
�p
I�*@*F #P�0D:�
X�	:0B:���l� n���*��_�  P�`A@����0�0>#֯���<5c�.�kp���� o ���*��"X�1f~Ϙ� �ـ���� ���!+2ܫ�������4��+��vY"ex'�Zk�0#`DqAC����4ʣ�`��p�����{yt\roRMY��%��������_C�S:�mI#�BICS�]7���/�څuNA0צ����� �����H�Oo9���.� �`�VP ?@F�H&au�T\21�I�1)���*�
�����l� � T0.A�+�������~b�T�������
�=�-p�+ rt0�4]��pM#��^������aP�y`�{@�a��R�?��B�a�"�!Wz��	&{�#r�*;��p8J8��?�?�"zBJS��0�<����|���@R�����@l ����%�0��`�$��G���F0��\�T�:��)5Z����c��O�r�v���=�	���A�\����2X�p�E�A@���
����۶c��S���gԊ�AC�@ʜ�u��	CX{ŊU�W�X!k
+j0dYaeE�c���0���6iӮ����b��Qk�.]8g���>+�7Q�:oL� A�Ċ'�ȹS���D�
�u��#H���K�ή]z�ҵ�W:ܼ��T��Z^eǆɦvL�l�����ƻ�0E� 5&|��FŔ3c���5�hd�`��C	���CY�	3��X����O�o6�֘-KVL��|��|�q��!�N8���0���+� b�!~����S��oї]� �0�c�<����b�A!��3�"&� �&���0�@� �
*��
*�� .�(	�T�
m��ƙ�v�
�p�_�*��H�}�A�	tp
���bE�������)�@�DxC��bc5�p��a���vXkF�d�A&�^zc�J ��NA���p�BА��Gɬ�j5�V�n=�h��&ۜ9f����jV��0�P��x��W�:<E�?��E�a�3b�&�A�1���2��ʌ"�� ���
���_�	��f�3�|��g����g�i�]fa昡�����}$>�w���3�+�0�BERV$��fc^xqq�D���zl���w� j�  i�$-�`I6@�����$�,PZi�!m^��q��g��N��3m�`��K�+��%+�1����3���{n��b�!A�a�5C��+d[a�y�d��g�1&�`|�BJ !�3�Gq��&k$�H��l�b��]���SN1&�T ɝ�]~�ex`r/�x^v֙�	'cU;F�v�y��J�=��z����y�#vQ�5�`��z���M�Ёh���=�$����8j��r�A��!�X����3��c"Y�~`���h/Q ӱ�}�� ���ALH���	"�j @�\ �����:jـ�v�O�C[�F>葎n �V��C�p � j�<��tp�L�
V�%7��Os�� � ���r@�Y���%��5�эt�� �5���fl�/{HB����%h�#�b:ɰ����#�@q��R�q�K\P�����[3H)7�qbj�&�@��Jw�;�0�їf8���G7���Y���x� �]`���D{���ݠ>r��e�@#��0�a��7Ő>��=�(�n������adD� ���覄5J�5�B��F����>�!��G/Ԁ�0b�z�6�@G.`�(Y�$XI*��	-��p�0�`�c(�	#H��Pa8C7�G=t�:>eO����F��(q*d<O�����,l�C��g$ˀk�!�n`�5������3�AM�B!W��I�hp�[АG`&ҐFj*�N�#���(���m�U���K ��mh�Y��G{�ݝ����*7��-����,fqm����'�0t�c��&��v�$$a5	�M�)М���2�C�d@�
;���n�Of�Љ�Єy�̄��g������L���n�A��F�x��`DHC���V��R ��1j$  1�66ўp��J,��	�$4�V�&;^KB�̣O���S����){�J��$b�mP*Z䠆7���(�.�Q�d@c�XDG8�� #��ێ�8".-o5]$
�8X����2��� �`
�k{���_z#���6��!jY��a�_v�=��,+���\�����h�Q�Y���u�;�хn�A$uF:^�}D`�L��hs�3F�P�0�!h��z��%���� c`}���\����׮������
B�t� oB!BC�|�KB�E�D� ķ��o���T�$�
� )7>ў�t#Y�0���&t�r�&8�9y�%V��P7��*��z�J�� �@	Ke�N�b_,C� _����1ACsA�W�TV+���s�˘�-����PH\�Ӿ��3j�� ��+��;�p����؆��_����6��nм�XV�gQ�#���0�L�n �_Ω�&z�����m�2 |@:� ;<�o>��N�q�@(C}���]����`�����z-�x� ���0�y��k���w�+P��M��JV��!����b�i����J��pB���'�t0�F`�0�p��Y���F/U;c�i�6F2�Ad�GF��To��(8���F$�!R�j\2��g�5�1�9[?�p0��FBV�h�e�P#눇V��KhP�PڹHc�#qع+�+��:���R �۲�"jc�`Y��Y0���Y�p �73rH,`0�ڇ�!��!�;H�4p��a@͠r0�|j�@��9�<ȃf @t�20�xr"����.[���=�5WS�zȵ�C�ڇd���8�bC��a���==:�  ��" ,�Y��<�d�a3�ap� �c@��J3��t��W��W�ļ)#X<	����" �|S���>6P�9�ax1��s�f`�g��LQ��@>["�����Ѐ;ȸG�˸H��@�Gi�X�s�+T9���+��+�"�p`�����[������Q -��9n0�>܄O �PxGϚ�&o�n��o�n d�+@��ٺ�xAF{4���4�lx ��nh�c8FT�S@�-�hH�;��B���هu�'�0��2�.��j���5z�5zH������� ����&a��R6�B6# ����@nxZD3`��&h4S�}y�n�
��W��y�;ћ�c�"�$�"P�EA6@��؅9�1a�*�0Vd�Ȕ��\�$�5(����l�btK��4�ꆾ��j�4kt̾�)0�3o�@S����P�S�2{�����[P�P[�Y�[��Β�\�n�,��)!lZ4���:�4�����$�q��<�@��}1��iN�;	��y�ɕ�#ʧ�!�'6a�c0HO5�I� �#�/gs6d�O#	 ���܄W�w��_x�b 3��2p�t$��n��Z`�U����V�
X�P���a�,! @��;�A�a�a�Eh`��D��8^������Vd�d��
���8ˀ�bԌF�EP�wP �$�@3Pz&�4|�L�|�����n�M�,�[([��X�Q�2!b���.�0���,q�3׌�o�����чC��ԇܹrXұ;e�;3�q�:jp�@��a�dH�]�N�|�����NM5�|2��OR��`�����0 
E�#x�1��R���U
Ѐ� �
�O8��`0v�xHc8�8�� �c�3>d�ŅUp���;�
X�ְ�7�!K���˴\�E��ؔ���e���4��q
V8���``E�� ���!�Q~Ōhx>���:�%�����mܗph[)4�a �+`
S[ S0��m:c�Om��QxG6��PPM�G9Y[;��W؄/��m:�;�sT�%��$|�H#g`�@ЃG=�c�>��(�BL�T�N��B�]�qѐ��&8�D�W38�'�39�#H$���������(���"�ۧ��<��`�	0�)�&�`�������2���Z��X
k�>�
��=�P89`9�:B؅^ /�1\��d���,VȗMY�3�(���}��}�܍�F �'ۗm؆p0^l�F,��)�$��NZ��I���{�oG1Y�-��y3m�+2���T��|M2%���9|�M�Wp-���5�s�\�;�YHN�m��A;�=�%T�1k�����J���'��Z��w��сЁ ���	���ZQ�Հ�{(� q�R���X�I�\կ� #�ՠ� "X�J3  	�=,�[���[
'&#2⊮�,�����8�;��A��w�1b�K�u]V�'�
x#�-�b!�cH��}}�K���]
��	�)���:@?&��ݹz���uM6��Rh�\p�M0�{A`RM�]����/��L����M�/�4��=?=���_�3s8�ӅV�8+�8�nTeH�6�:h:�a &mȃ6ȃ���L�'�i�%?h���ZF��ka	���Ȁ�E����<ƈ� #��\����/���Y�U�� NЋ� ��  +�+�y�MpW{�|��2�7�,#(�����bF�m�5�@b��C���g��'��+F�~�~�;���Z�|�+�+쉲)m��x-Z����d�M0��Y`�H�κ���3�\��������9q�.�_"e��_T6���:�3����eZ�3�]' f'h�>��9&@'8f�i� �5���Ɔ�k�38�y2�� ��1�'�fh  4BC&�-�rv��$J�L6&q��g�*	��H�&f  x�Ё老�X ٶ�h�9�܃Fh)&D" �#($,^�;P�_���8��[�@�*:�H�a�)��FȆi��k(��8"Ԁ����V��)�ҙ�iqز���p�ҕ��\h
H�O��0��:mS[��@�M:�̘#�U�O�OAd��YT>N�.�;�����p\�p|��c��w��vB�F�p��cP�� %h.d�` �t��b��>�,��'P�"(��Pl)�D���1
�`  ���h۵=�aKO� �0�����/=�h��)ғ���SH����	#А� X�;焮�V<�m?�W0�'B�D"��+�7P_���.>[�v(ύ�t���b �=�͐�iȆ�~?>X�"8�==4�
�>������o!����9��^q�NZ8�]��uS�]�8&��Ys-%���`�A�-�=�q@�rZ����]TD��r��\�&r�6 ��0�6@�H$x�SH�8�cF�K���,0��"�`�w|'r	p @	����p�] ��O ��q[�� �r������;��3���(��O��[	 j�Kc��8  `�X ��)�y>��홗��0"�P$�RD2T��?Z�M�7�9 � �?��]���$H��i�]��s�۳S0�:���)��W�O�^C�r�,�ҴYS]Ѕ��Y���GؔGs��L�U�K؄�׭��viO��Y:���G{�f f�V$p�~�@���]1�̇�+ K%���"x��ׁ"��,8V 9��3� �Q�d��Ą� �����
6��8���dÀ�����Ϗ ��+��ibL���ۅ	��` u��?�y�>!N�� 888d�>�j�d����'l��x@�v�����$�N���Y8jԤI�D��F����� @�4i�oc�|�<��(�Hp���C	n%K��N�{���-R���q3���Y�lŲ�+W�[ܾ�5�T�Q�n}gn�ԩ�̥����,Y�6a�.$٫�b�g��r�ж%���[|���A����L��6��y���I�0W1�e3gv	�:t�޽�G/]gdO$8�2j��kGZ21?8����$z8�����\�P���/X�@! ��өc����c�QKM�3g��튀�YX{���1�����2c��cFI!�+�q�k�1L1�<�4�M8!�:ݳ�F턄V�t�1��#�4�L#�4=�	d�AMf��SY#�RJ*��L2���7�H�O����TP���-��T�-��T�9+��8ߘS�7����%`u�;c]%�T琥$[d~S�T�CV��)�������!qL2K�^���N;���:��Y��@�L �.��i:���0g< ��#��Xp��A(8�(`��rT�l���\�@\׀�Q�5�$��3�XC�/aQ-{��
,� K~�ы����+���I���k���0�$#ބ�T3�d��FZUV�CM#�HS�0��l�t��V�HR��̲I+����+�$�L+i��>k��M��R�-QV�S����V�%�Zwֹ�'_y�cX�蓎2�8CQ�f�y�sJ�����^���(Ay��}|pD�=vEFq�����t��3gL��g�"x�A�3�� `> o(�zq����G#��m ����2�\r0� �Q���l]�2�`��F�����6 +����o���g�|�p�#p��p�� 2�����G�>�C[Y�J�CM4K���8#E�@?8Y7�d@��&,Q`�H"t�۠I�J%
�JI�З�t'1ELb��+.�4�e$�G8q6?��LD�Qt1r�}QC
�1�>� �J��!��p�1<���z�߈ 8Q}*���9��30` 8�0��j�� ��3��!1@� ��
����8�qJw�F|쏁��栄C:A	S�C���e� WXB� �=��cОE20����x�1�W�T�zԻ�|Xa�# Lo���p�<
^�Q�b�#C�`G_�-]�,(4G8xqiLA����l@�L@'38���o~S19����lcb)��-Ja�
���c������	�czK>k�
K��je�G:��6�ᠨ�ٗdH�\�� �1t8�6H��pB
�$8�Ԁa��p�n��	� 0vq*V�3�@6�1�t��o���.���݄���1򝭹1`��0�� ��"�S�����* Q��5�t�x� I$ �^E !$at�"�	���?�����u���� \��F�# D�a>�tނF1�Q����.�[7jrp�>���0�1��e�Z��~`+|���7����Ζ��4�I�1��"�)��r[Զ�W��Z촥�t�N�xE<������S����ph�&D�EDˋ�Z�#��@R��t�����g�0J0�IO�?�bUg(C��@ȑ��� /\E#� ���0ԡ�&Àp&/���	�u8�炕���X4����ۃʊ- 
��F �#��m��p.P�S�U�}]�}�<V���� �1���(��*�Ee���;���c�d*˔u�@�?�	��刀���'ӄ��ro~C��mh�	������KM�v�4�.K|$?��4t���7��o���Y�1��0P��,�;���Nn�o
��t�	A,A
��5`��qF�P�c�a�yKz*�`s� �'�ӟ6�����x�_�Nh  & )p�����<p��a�����1o��H�r�Հ��
@�
�pHX\�G��p�Ї>�K%��X��_�����#4�`0���n@����G�T@4w�CSل� �_���M��jv��с&f!��B)��6(m������)��<ib�yR�%]����b�Y4�-U)�,fуW���h��݃�!ݘ����c��Lөy��4恾%PB3�q�E-�>�0ƀ�0�cK�1������@  �� ��k` ��Vh�4a�@�'�� �S-�\�]�a�"�Z��#>�@�/����3p�
���!��lo�@ b`x�u>�t^���׋C���u�e�	؀5��5��=�$W93OB��t�1��&#�z������� ѵ�lU;%�7	άD��T���Z@�7m	JPՑɔ����8�DT�WX�+�E>8�pW�AT �C>�Mh>C�M���z��GB7��0Q��1^�@
8�1,�1pB����I��LX ���JL ���|!�d�Be��0�Rm��͈���r q� �	 囋i� ����"4��,�̕Dp�
�@�\S�X�0C}lb�ȇ�(r���K(
'��\Q�U���90�5p7��.�BX��&�[�h��'|�� '��"D D� nGn(@D��@X�&�֟Aş���V2`�Z��9ݗ��#��ST�IQŘ��Q��Q|�t]B@�5���J ̃G�C7h�8UT��A���C�E{�:��bC`]�B)�Z
��aJd4��HAdA���a��pR�ۭ  ��'=�ox �N P@qApU ����� ���LArT��l�� E �L�"[Io|�R�����)����*�"�=� �_�d�C7�C3l7�b.2S���&l�%u� � ��X�c� $�� �@�e]Ic��'�;uRt��i�,�m�fm1`m�S:b]j�5�ړ�EQה��,ܟ=��ٽ�\�C�_���\���B��A��0܅��NʅC �B~ 0�30
�G��:dT��@���}�]��`N  �u��d椮�
oG�MD�-W���!��5����HAh��X��8(W�FE�����^��*���8�`=O�q�� ��Y�L ��B^BT.�H8pB�@�%�(��WLB1Ҁ����F& @d�FDcAt&;Mڤ!�-h�)Ij2�6�&�%�8v�<�&=U��<��&n�ivڙ��+&9�C88w5'u|1�\��3�B��wE$5��2'�%;l�t"�0hC`����1h�|�P�����g��j,n��|��|�'������n4�'���=�� ��6���^ ���A"���>Ռ�[� d��&��l��A��Շ:��΋1��Z��<�e�ǜ�Vj�*�Y��� >4:��&L�%਻Z�$� &X�,�(��%H&TB��c>��F�ff��&��-�B7v�)|#~��5����jfZ�A�Z�Cҭ&
zI�Q�Ս	��[􄜲�\��� � H�C�tC8 Bv�Ev�_�/h��,_�B�QC\$0p2p�/��6��+�AL��e�nH�&������`mN�n��QFL����4f��� ���A"� ������`�� 6���[ň�.k�pEH���&n"�u24C�F�}4�}<������ �P�n L��_L � ';�C7��'t&`�'L&��(�B(�nЌ&��'�@$��=&dF��F@5��(�ğ���Y�lm���q�>�P�a�6�I�f��h��Z�-�]�v ���t	��C5'_�A<��G�D7�C^�  �^��Hag������/X$
�+��3,�1pC-X!�捥P-� �e��\ P��	AJ�$eP�ҡ�mS� ��*��A�-XqX)�"�Ap�$,����oX�d��]��P\�Vn�`���p�$����zL��R�4.c5�XAXB�*ŕ��(��(���$� d�/)�F�[�'�B<1 �Ֆ)�B�y�78���g�Eӭ��r�󂩙J�Z�T8�<r#�h��N�ɮ/;�C=�_| 5ԃ��x�]��5̃���!5���L4p��A0�1�&,���+�(����đn��Q��37���閌HHAc`��0�P8�/e R �D��AX囌� �%���q��
�Ȉ�P����d(.�Z�}`�тh).7A5� �n �����J#@@$�fJR,�;�O����)XL1�*i��.�R0��:Z����R�SM[�7�)�r)�v#�Y�J��ҵ�R)ױE.���	��G4���,�v���
�4��ϢA���Z -7��3�AG��'x�+����*p�\�sc�3233{�33#EC@�4��	4FXm��X��Tǔ���� ������� �A8��s=����\�J%Ap��0 ��T���� CPF��(����
�z��`C ���ڂ,(�,p�(tL3ESԀ�FL1d
��^�`M�S<I�h*�-Ȃ-`�SLw<�f��mQ�ѵf7ѓjb�T���~�GSo���	7�u�&
Q�N���0�Ú���YT+G�@���(�/L�+4�)��,x�,d�+����+�'�'��27EG �L ����Y�GF��$c#vJ޵�Ai�����BR�U!٭g��*/�Bc$R �
�@D�v��Tu��MFl��m����_9O0 L������]K���2K&X�:�y�B$�B��� �BzkC8�dFf�lP��Ӕd	7p�A��>�H�;=����ݘ����&�F����;�Z�u&��`-�Sł�'���C;Dx_�l�p�9t�F���j5��pL�,p�~|B,��,��ř8\��K�?�gP�� ٲ��z��$J޵� v��;4���{�IC4�[ �m�����W�*�*�Ƭ
���(�
�0=���z �t.�e�?��%~,��[�D�3@nţR����4�^��� ���ՕT��7tCwg�B��� {)�B�}B�@d2iT�& 5�@c��q�;��;�2�RD�z�-��,t�z"��>4M>�8%r�,�b���6�˚r��"����>@d+�z �2�C.�P/F}A�2 �)h���B-��f��	ɂ*l'��4e؀IA�@AD��<�X���5�cH�HJ�~�Ac�~�[4�{���
�Q�-�{vX��.�rm��Y9�t��H�uyE���H.�LO�s�>C�En�!(��㊵$G�9�)��4cԀ%`�R`B'�R��(���?G{�&�tdn�dN�^t���RЄ,����`D'L�d�5��,Y�r��v�7m�Uħ�}���3GΜ�o�0V�V�\�}�޺���[�h��ƭ\�o�D֚���+r����7�M�6n����Wڌ�A��Cg�����AI�n�x!	sl�-Z�jq�4j�'O�j�r�*Ӧ.`�\1b��6lX��7�aŋ�S�̘0g&O�Ly��E��hXg��@GݨQ�@��i�5>��Cf�H�(F�8����2G�Ax�#~1v��gёU����Xv�ͺ%{֬��Ɛ��D�F 4hp�B��l���/bX�4
P0���P ��@PF�
 ��$��� ��:���QdɅW>�K.��KL��WZt����m����|��g�{�1�r#�82��`���h�I�Yp��Y��䕏:�g�@�J�a�!���1���@b��9	4>@���i�Yd�%Zf�D�W\|AU�d2��
�~�a�	"`�0��R�&S�RL�(�P����"�l�=�&i��̴h:{U�F�Qd�Z[�^��}n#�)��$��#��@�2� Z�:��d����ȳN��X!���³�V6����=����E � ��@%�V` 	4���W#T��z���K�L޲@�T��M.qe�U�Bf��b���q�pd��UFgh�Q�Y>�d�\�I&�v���q��71&���DM4�<Ŝq�)�`P�@$��d�n���6��&�t�yE!�>re�Z��UHN���hB0"��0�2� �Cq1 �rRU�1�Cʦ����D��_E-W_Q[�V^�1�<)�h�Y��`��C�v�2�a2�I�w񄷎u��V��y�o�˂ �( ���~� 2��a}k��`�d@J}�hP8���� 3�}�-��%��=�$wqE-x}��x���É_��<��^Q����`�+FA
�x(.��)na
�4���z�
sl��0�,��n�DpB�֔s�B���9x��̡V�F:�q���F4҆'(W��n�`W!U�b�PԢ��� a0�3ը>�8P=�1`(�W*R�J1�F2�A�l�J�`Ugd�Gxf��C�z�:G*B�@ !�e�e� GȤ�3h�X�
��@�dh�\�WYq��4�:�)��(�� \` �������@ +��r!�Qs{H'h��D�]x� �.������7�RtA���%fQ��h�娋9���"�cy݀F3l4�qH�.<H�č$�����B\R�[��&=�W�5��(� �Ίr9}�	��F.��^|EcG�ꁶ0�͟I���H��Ԣ�_$��P2��oM8�4cǞ��qk��aΰ^�b�P�2�Aj���`�2���atV�l�B@0u�\M x�}��
B�P��d%�ȒVqP��h	�g��w��
��[�ix\Y�`X�]�z���S�d �pO��� �Ä�K�Y
S|�ټ)<�0�g�z�<�2>��.���)FA[�z��(,Rvv�����pa�
a<C°!�`��>w(�>�B��b!-�P$��Mhmi���%�֍�hx��o��\ �k7�Shv�#쁉K�C��d%=��܀ŀk!@���_ #�?��Tl��)�X�]�H�.��h�-!��R�qaɝ����Va�HYy�V��C��0�ְ5�>^�L&�	@֍����UI �� ! A��}E0��
��\�4F�^��@��U@ P����z��W^`�.$$$�	M1��������g	e(��������2!E�2�����υt�>��_�B�X^u��#O��އHn!P�|H���J��KpCk[s^M#~�EC
��� ��7�
�p��y0F�z�
��B.���1
��U �bT����S�<J�w���9�d@�P�;ց��C�� x��
�*��<3H�n�j bs�a���@F��%(aG����� G	��a���0$��i%��<��6�<)��+�`ĺ��xWnـ�U�z�K@4�	�x�#�)H�
R]�:\о�g�
P@2d�'�Jp[ �	N|��� �3>]��n(�G8�!�_C�fu����
T�P�A��	+�0�����l�F� �F�2a�0Ld�/3�������1�Ln4�l�W9a�/d�gP��fE�71��t���x:��`$B��\� �S(����-�G��4���j��Aټ�5��jx��CS ���p�+Y�#�p�Ӭt �0��u�t�<���7V�t�_<� ��E�h_�%?�'��������Bf�AfB���N� @B"�  6�� �~�x��9���¡����8�F��Gt�G�d�v��<xP�$����� ����B����en؁F����+x����AN+�c�f�a�a�(;2�벍�@���q�p�
�@��c�Aw��NI.�8�r",�R�ތ��N�����Ŵ�UF�4l%4d_uv�!Ơ�0��8��2��rG�� ʄ��冧<R�+\�e\n�   ��\�a�eP�N��"  @�Q0��	� � �`
�@
&@B�� �' R�� ��p,���������8!�h��$��Ρ�X��*ZI��NȡӺa�LM�DJt���� t`�
�'�1��e��+<���C��l�A�a�AӺ��`��m���TG3�p���/���A��A��������!��!��A�q�v��mN�*�+���U�
V�4J5� �ƶO�~��a���P 	�L.咭:�h�7x#�����ʒ���
Sx�C;�#��k ��^��="?������ 6�6S�@
��&��r�z"�>"���t@�06=���
f\n�i��3	�킁������F���Kڌ��
$��V���T�ev�;�A�>�>���C|�F����!~�9aw��*π�(�5��j�S���|���j���6��!��΍��%G��ӌ�*ya��4T%������/4���t-O���P1�%	�C	P�Ȕ �4�.�v^Y:�9v�<�<�%1���!��x��$���' �z��JK�ϊ�d�����X��� �a��EPBr�� ����GN�t3'�Ƞ��
�� � F�R
�Ā�a����������NO<a�)d�嬀��"I^�t`;;�F�������0h>G���P,��(ނʍ����
��'��Q����ͨ�mX��(a=ѓ����~��I�O�8㪰��^td%4�jlW^5dLuRr
���@	���Z�Ȓ�F�%F��X�eX��AH�;��c���x��	�q�. ^�Jӑ\.t�O��b�/K���@$��a�"`! � @�@����������ܡ#�:B:��Qwp������rg}D9��C<P\&���@b\� �Hbl�;������	��
lJ�)�����R �S0�m�Rt�o��t��+��*������x��.�/)��ި!�@��H�UB#D���pL��X!
t��6���b�vj'�0	��X� �y*h<�;������`� ��^����t� � $@� $ �a�T�:6~�zH�d�@3Y!� �@L��
���	�
b����3��
�aXa28�����zF�GJ�@��!
�nS�i����2���o����2���(lTH���V;ES�(rSpJ1���|�b���o�ކ�o�Or�m�04�0t���R�5���"w4<���,]#��O�6WFn7���d���l�@�Z);rq˨L��H�':�c\� ~��(� ��akn2#�^ Ġe����843�3�� @��T��MM��� K���t Y��ҥ����!H68w�����vA�����~��(�� � f�d�d�b��$��f]�"{@"sy"�3���Q�F�(�G3 �����VSr�/xU�l�Qz�n���o����@�yr�9���S���oI�<�qcet>�5 ���@�8�fT.w��B�8�����	���01���Jy�cӚGNu@���tM�E�*:d�`��b����5�c/3̠z��@BP�v%:8A�x� �T9!O��ZNO� ��G6DM���2���4bh�j�N�R+��e����`"{�:_�V�槻�`�]�Q �/h
2�T��ނ��X�R���Lx:`r��Z�/��m�yaT��q��٪����R 	,)�NC6Ht��5\�@�-��s�s[1�`t
�ɺV�@_�'�z�y�cyd�:ʣx@:���QM�E2׸�g� e��P����	��	��� P3���cKv8c	�8,:Г�����v(��v��!�����K����A�`45��{hq�8�-Dl����u ԁ�`���'\`<C`@6q9S��0��
8�k�`QTo�.8T�Z���S�sm;x�����M�;��'���Yɚ��q;$c����ת0�n�j���U\���YWL�5�o���� �R���U	ފ7..Fk�� �� 1�e劫:b[:��y2�	@�qzTa`{��  6:y�@�`�@��`�t Rh.瞴a�"`�K�K�T����4�@615K% b��!6b�R����a3�@���"�?M�=m,�E.b$����u���S��
�a`��`$ C&�­`����T�>��M��A�q:%��v�����Cp$e�!XS%T̽�/7�R��Ϛ�m��M%U,��V�|��9�X���R\Q�����`
����U7�7@i�x�u���Ե����=��_���� ��o�z,�^h�w'��K:Qw�X���>��O3 <J?>~"@6sceӐ���j�{#�/[}3[��e<L���b�3'PL��z�Ӌ�H|D�K>� fi�����@X���O�@:k�����<S� 1�F��s�+̧�y?1q�VW�Q� 'p�=�z�var@��!��ݓ��p��:����G��N�t �t�;T2����X�`�L�r'Z��u��9�[z�Cg�	b�[�/��4)$�w�'^�: 2�c�W�1��C�С��'�E��w!:�!^�e����X9�x� K��~����a�H�*�p{ t����̮w�СC���>|����6pܾ��41�]Vt�aM�.�8� �Bb蘩ÊM+]���I�g�3@��9u�Q�F�*�S&�P�@Ü)��`�iB��&D` ��=g��HQ�Dp� :���Z@x
�԰@����(Z4i�'~4�q�F|�B����e@{���<EI�(J<'}$ɈӨG�H��ɑ&#�3���mc��k�Z7jݠ5{��v0V HN  �?~@8����ko���0Y�<9�
��"b�<1�%���'w����1���^� �Qm� S q� X[y`�/�ēM<��"E��X|���|a��PCO:�P�̊�Ӎ3�pc�6�H��+���J0�d�C�S�\|�K��, S=�p�M`�Q%e��SQfu�eQ}(՘S�a�Ua\���^��AXa�q^x)��" ��]rm��\�4��`�2�`��a�!��a�X#�6�Ge�Zv)_tVF�J�
�)q
����hS8�DI a�0ކ,���[7�W[+� ��,�dV�� T7 v�!�] L �yga�:��c�HP��Q��}�g+��wF0��kg����1�6�(b��2�j�aސ3a1�[ �E:��I�� D<�آ1�t��>�`�]3.ݬV[��1@��R��SOc���YK�SY6�%TaPE�UVi��_�� Ys5�t���z�a'^j٩��d��v
�^�6���M����Y��N&hx��m$1Gh��*Z����7ф��f�+<6�Lo� Xm�K�9G���_  TwAP��S3�D�gL2� ��/&�ʐOBA?<?|FA�D{E�K�E��2f��n��l��O�g?~A����~�������.&��<���O7��r2����;��a�BEx�;|��ρ�>H��L��d�Y肕�/���a@J�2�9h*E1�
�V�\!+��b4�Os��.���i{��rX���kv��ۢ1%ƍ�s��7�Iq�v�B�Ϡ�4�Q�V���,�G�"׌����@�3�1�n�W�aE|BG�,����w�EȩM���xĐ,��	�Bxf҃!�`:�c�X&�	%��{�2��ğG�9�~�����w:H��D�~}�E��`þ��7���9�+��V,O�8'8B+ac!�`�)��!%c�?�@� H��� ��M`֞,lK`��T��v:ȄL�gҒ�43��*YQ�������,t��֐X'��%u��B����JPKT�$u�aحR���^��M��T(P�QF�`'5i��4�؜mD�9�(�3E�G����1u̡�뤵�\ �k@���1#O��� U���I@��,�1P��Ŋ��<����!O^.8�����*�"|�3I�2hN`��Y(k-�W�M�/��H�0Lg"��x:��L#� ����U0�IMzR��0�\�JE��;�͜!LabJQ�i�z��ij��V�iF 3u��e�BP$���r��7�Ql���"��D�<*1��(`*��E��
DhP��4	���iP�0ve��)mL���T��(Bs�k�`9 > 6���;�ΰd؎G�(f/iJ֬��@�F�KVܕ_��FD��a��b���
1�xy]0Ƹ�աe����B����.���� mr�Kc$#��:ڑ�t�#�����2 �\Egk6�(�,'9�IO�Y�>��gg��lYO&�*�ŧ���|�M�Ś ��#�����@yhD�j��p�b5����n�b��.^M�TsP	F��zqM�8���jcA��\�W��͏u�Y���X��}RW�8��c�L���i�	��*�d��,��p&�XQ��7� 2�z��|e�%S�� ��(f��M���u}��2�LN�hG�iY:�1e�c��1�a#$��*��k,�N�X�2̸�1c�'Y�S�2�����\!V�I�YaMD Bt����Nv��8s:�ɠ.�!�(j`�1��TQ�J=�nuF��*1�׼!5M8Ru��@ ���_���������f3�uWE�Sԡpo ��� �@�t ��@؂M��������#_��xa��L��� ����ȧ��	a�$�̐z	�J&�2�aw���H������)rF8�!p^C[��BM��p���0�2z��.8?�X�
��6�T�C:��0���U)�H�5��*�ڛ^���b��Unr�{\��	�{�S]�7&�mRL�)�G�Q�EsY��u"5��*�*#0��8? fgv #¡j��S��$��G?�Gn7Tv� 0O0: ��/�S��A7�e!т1�-y 41�3�-�bC`V ����B�1 �U:@U:@n�I�Q��AV!�p
��Xgsd�@����?� IϠ>�_����:�'f<QX"P!ONa&2T�*~Ls&�O1�rp�5`Cs��P��P��	�Pa��s�}y�Ps�D��DK�D�{�]�"�b(�|0���eR�2#�^�aFP�B ��+�1o��S�� �L� ��}t³G�A-C�:+8�`Ap�A>�-F ͣ!Y�W8CAԂEpw	v:�<V�!?��V>�s�3E��-��-�:�|5a�Y�!��Wv���` �p����i�d��c����C2I`��MP%0�30�b&~E�gfC�}iq�B�$~%gW0��~6�rt�g|���8��ȉS���0����7a��P| ���DJTQ��PE��v�)��6��h���〦�'����1.�35L�v�)�� `<v ���   � �� � o�&: ���!t�R� Bp�wW9�lx�.��-��-��!�r-Y�y?`I��CNX0Z=�0c"I]�J�QV/F/��p���� e�$��pd�e1%�4ZV@qY�U�ZS14^�%H��@�NK3rU�` �)'�aQ�s�r1���N9�'�M��5��(�F��0�����1z9�����06N0�8G0GP8-��iJ0���xHme�����F����jF��c��pH�i�SS��� �7������"}d�g=�	] .�F �`�aZ�YqR�?j��X7���b�>A�	�A¹/:�/f�b�hnÀ(B��� �`dfP�@%aIg�d�e^Ƈ7}�q���@#&>I-t[#�4�w�Q�i�N���̅��P��\��}�����(z]ٵ�S Jp���X�PsP�u�8��E��0F�!�8pcF�j �X�_b�#����T�:�0@`# �0�� ���'1"���ΰo�@�L'��@q�/���8���8RB�`�ic𣡇@�dð+�����π��
���>�>�0�dX� ���p���j�g`���0��� �E:N�M:�3���U�B[�R��ا%��B�O�Z�0���rP�����H�ZU)�s*ڡ����ؕ^)��P4�����B�26e����hS�E�fR#�E����Bpw�=�T�	� v�Ҍ��V�0�Ss�@ �35 �˂:t�:��&E`2�sV�N��W�Q�|fp
� iؑ������&B���Pa�yҥP�� #�����@b2v���ݠ��������{����dg ̀� S33��U�AKhVr%Wᩋ�}�T[p�<�N�OF9���5����j�+���8c&j�0G�M�(�������)�'s�\T�ESЋ�(����(u@�`i�:e8�R���` ��� �K-��@ ��K��T`o�<�������/��."��`+"j$X�cs(��i�1����� �� g�@p�0�P����� �P��"i��P� �A��"�@0���b���P%��HHk�:0Q3qT���A ��`�}gB����X�Vq��W~�H�|9 ��W�*�*:��8���hk����_�h�X)�07�q\E�;7]ӵ:^�i@^�ы�I�*8�.e�m��z�1�`�m��Q�� �����T`NH�bT��c���ԋ�� �)�	X*�(�L� ������#�/�K��h{����g0�Ѝc�k,���������s�ӕl��@l�C����`����/�$������Tq=��XB�R�N,�|�M#�'wrڢ���,'�ª���*�ܡ��֭��N`�� ��]"��6t��ܠ�08{�h�b7v36�e��\*��8��*���p���I��@9 ���ٽqGBl��Y��<<�<����y`�?�݀A������)�]�l�'ќ����bЫ���@@�B�Rj���qH��� /���An�#��@��{"�(��޻{ðR�@���ʠ�V ��M�*3�N�,9cZ�ZVB�4�g�9�.
�&�Qpb�&lB��$h=̊��y����O� ���g����tCQt��Z@��5�'�~�*�*G *-5�#`���+Wp������v�hd:uN�_�q�� B��3 Z�T& c�P�a����9�)�ηLa
j��Р�p(}�o��ȝMYd�-�� �S�`�<bb��؃���8��荾@���A�w^��`��+=��p�v�2�4x;3���f.��j��[�V�[��b���ޚ��~��^��\ƪ\�^�m����eۖ*�͏��zs(�7)��(� �����\z08�|u-U �G0�8e�X�a��]��K.v�E��f]�_?u���:��3 �``aQ�U��d���L˴	)��B`�O�P���j�����L
cg���� �W/�@ˠ2���	UMP��U����A�9?��%ɀ�������`S`�#��f��eNU�,���Ba-�[�[cmD�PO��^p�i�\�\uѶ���m�������,�U��j����ኦ�D���'��~��nn	��m���R#0Ξ���i9�1��������j�0���S?�v �������<��o����
���
O��I /�R0PС*j�S��#t����p��`�p����%�/��C�`y��m�DáS��/� �?�V��`�`���NI�8�K�Bd�BX����'g���~YO���`Pp��� �C`�	���'Ϝ=s�̙�pʔ�y"N��a�=!Ι�dC>|�Ektr�ɔ*���C'�h��=B�H��9J@*��H�N�E:b�JG ��d�2�T�%K��Y��ͼ6{��+c1    � � lݶp�Ct/�m��C�:����/�`z�
�C��+2�X�r�i�ɜ���0BH!N;ҙs)Q�H��0`��0Bg~AÊ�d�8�cf�.3%��.�>vϞ	c%���f��J�,��]Y8�cG�G@� !��:ܻ/2Ċ�,V��i}�
�+��7q�&��@#�0B
+<�@0���&�0)��"� B�*h!�>�"
�,�H!�@�C�=�z�E�*i����&Gi��F��h�&:���&�t�)�)�p"�)�R�	��B��2�r�x
���A�d�Af�2�jƙ��kf�` (� �; .��
 �����z��
���3[.Q��٥N�L2V�ؤ�>Tj�:��)���L�ͤ`�T1��a��ft�i�h�ygt�f�3�8�]z&�f��5�n�!������,�8C�p#�f�1f�! `�������ӖB+��2�#?0|P���Ј�,�7B�}+���&� � TŐ@)��`�� ��@x���!����ƅ��?^)�F�)i=<n�H#���'���	)�2*�&�P�̀�  �&�㚑+d���d�2拴�:˭�ڊ:���s >/�K�
�|St�ĺ�b�G!{t3��a��&4�.�˃�2�R!N"�
Ax��w��f�QF�d�%�z=C`�
'�vp{���~!#^�cܙ3����c�� �< ��c�\��Ȣ>w�[�v�+p�3������z�7��
;�)V���6Y�I��8$�2n�b>F_�hNJHb���I�a�H����Hʙ{R��tR@ 4�r�x�1���`ib�W�c� jQàԤ6�<e-/y���@��,�#�Z
���sY�dx�c�@,��S�nx�0 _�[�2 �
�j p�;�z����J��8_�"g0B�0�?Xn%�H�>��c�"�/΀;0��P4�A3��y.حo�G��A{�U��sէ@�|Ѓ��H#��>�b��!IRy���@
F�D�z	��.���{���f�#�H%;��0��"�/G)��d�&4ɗA��Q�����������q�63M�����
dК�l��րt�p����jt#�@=ށ���C�0���3t�/�BZ��A� ���y�Ԁ��ғ��s�D�����4&�h���@�3$b;�8F�dc�@��P�,�p���}�#�i�b.�@��=ڒ!-!}!ҨB����FB�\J�����!�H� �D�DPy��A�"��D���F�����h���dJlR��� 6�e�`e
�

��4e�K�q��̭��Ϩ
U�Ɋ��U�,����C�f7�0��Y�J�1��r@Co��$�{u!tռ,f�R�<锠1h�vx F �p� �D$b�5`1ư�b43�� )4Lz
_��mY ��s2�b��T��'� 2��cB���?*��x��5E}䂲��K5���d�<���6�ES���0�%Ĭ%+a�#5"#>���B��j1y�B�`���IS����)��.�o��V���nx����6��i�u�%2\� ���p���� �,��``��v��L�/E C$��;��<ġy"@�D����� 2 �aP���(��_0��]I�At0c���/��3X���Zsv�p#O� @�M���)����9r?��O"[��F��˩Q͂� kG��PA<�Ո�$�/ʥBh$1�]�b2bȈ��[�Hd9�ߓ�d���D��1�����Xfi�1�7��@H�v!
j��h��Ԍ�P�˴�5��[<qv���!�� �D��Ձ�'� ��`@e+ $�߼}K @�����ߡ��4�?P���J�xtW�Fw}ы�������Ù��;Y :�+� ��dG���v:*��u_��z��E�i!��_F�����Y�K� ���C�q�S�䎰9��y�G�:WhI%��C ��@��a(E��U�4�C9�u���	0F���4a�X��E+��o~w�O^��׸[�6��]�-2@ H��KY�Gm �����ԁ#L�o$4�Ӂsl`��PF"� �,ڦ�2y�v�` Oy�\����� Y�Q���rPR���Kf��
{���#���	���Y!��H���Ъ������7�0�R+�𰅩%h@P��\
�3�@-����K��C���t��iNx�ؼ�B���B� ��7�ʋ�ʀ���k=�فNC��A ��,ݲ&�8��8�#Jt;�&hR�  -10Ȁ�.��3H}hN��%|H h���#ۡ/�k��
�A�	9$�
�Z*���z��
 XD���� �@������a�LL�M�'��Yi���]�ّ���\����+�r@)A�`�\
[ ���?��g0Ȃ,z��Ȃ�,�hBl�Ai2A�ԛ�� h�M[ �۴�a�c���C�;�=� p��и�K��#� �#�p� ��B�/��/(�gX't�$�9,���O���:�#$�*��$����D@����!@����\ӵ�c@����#9��eC���GH��]�%���(<�RE���H)y�(����-$7ΐ/�<��Σ���{�j���j����!��⓭�,�)��:�9A��Fm̠��,���(�7n����` x��s�	�"`�n �`�!Vrx}�h�8�4�����4����'�;�;��*�Y��p���$�ҿ���Ӗ������;�(I�`����ڪk1���P��[�<h�:���"1�@E<3�&�%��d1������8:��iq�B�d��0A�_�F�0K~�,�њ�졏��������L2���,n�϶��B���։����v��u�wt�"�	��֙��G`��k:p�������+ R�5��PC���M���+�������$��@IT�;�z�]��D�#p��d��ٰa�*U�@(�FΔ��]�6���ʤ �� �fZ,�X,z3:2���d��!�"S�\��RO���;�2{����B�ML��N8vX�w �f�0ȣ�D:��E�p��C����s�P"`�;%�<DY#�!p�I�Ӕ��T�L�(��(�6�������B)���	�ّb�6@��F �\�*	���J��DN`
J웧Pم82�4�D�e��d�9�=������ �zSCeS��=8�O�GmDTC%�j�PG�2���p��˱L�Ee��"H��/��H�P��xJ�����M=�����|D� ���񉌸���9���𪠸QtW�+�Ȱ�l6�@�D> �1��T�&��)h�P֣���MQ
�P�&2����A��
z���&r�'�F�k����h(2=�sU�=�t�B��F9�V��״`/�m��rЧϨ��o����:|L�����2XR�TA�;�
0�zXMM�x��� H�����S��e�<Μʣ�NR�@`���	Wz'X��4��٫��)��,�c�(�����#��OI70����
{
�
d �cB�[�R���j�܂B�\�8�7n�,�K ��x�8�-T9ߵ(��	�W��/�j!�k�.�����M�T�;�=Q�؍|\B9�c��ATu@�aUi;֝dο��H�P�P�6N��F��N� $���)���X`�ѡ@���<�x��&����(T�۪Y2�+�"۽������  �K����V�8�W9q:��(fLP?���(���H�P��D4����T����� d�Nl�ʝ�6��L<`������$���֔ё���Y��ؓ��� X��ڨ���T�cz<bb
 r
�=a����n���
3�a���o-ӼH2@��=K!�Om�O��-����!#�%t��Ɯb��u���/�`_�J�H1��;1&��_�l\4�� ��ቅ�\�YßU
Uu��4�X���@�"Y�X��&�嬡(N�eU>�sC&�1,JN8
x�gp���g�1OF�3�P&�}�J��Z�Y�r]KU�^В�k�k�8��S[� &LT��[)V__�۟�b�m!�:f�E憽_IZ��S���G��"�&���*A8����6
�٢~]���E���ɿ���o��� %�gsC�	&,&�*�� �c��?��n(�0)�Z���8��ʩ�ڇ��� 4��Uf�&|��l��Z��양�������`~-�L�����s!$f��D��D��������P=�{1:��ٜM�x�L���u��͈a��XIV��a�Y��!�f�X�~6��V�(��4����+ɀ;&^@hP�c�VnHh.�%���=Un���'�^��t���nB#��%��e��eE% Xǌl�l��[y��H��b���T���?O}�A:��Nu\Bi�$��& g� g�
Y:�鮂cٞ��I�i�E�A����	���<	 F
)����X( 7�� �a(hd��5�1��_ȷ|o2��>A �䧳@�}KL���6ԉcOةe8|�9��C]�	�e����[GX+�R���<f�L���'ьsm�T��'F�_By x )�+)8VVUUD?`��*oۥ���'��6X��<�?��'y�=y �Q= �n�r�(�]���r����*��c&�(r ��,Ǔ:����̋7eϹ[[����kZ�F����C���<���o��6os*�]��^�A����mo�� �����8ϥ�D��n��I����B��I��0m%W:�=��(��&������<�q%�n J.�kE@�19�cq�1�h
V`h҃�����֫��v��k  ǌ���F�Vv8\���g�����h���P�~sj?f;�?;�v?�'z����fHtH`�H��Rՠ(N��c��������yE�0�i�X�c(�Aߔ�� Z��c
�_�Ҁ���NQ
H���
���Pڦ�/��/���Ӧo���X�,_��do��^�L�%<�9dT)��4�e_�l���s����ZKf����$l*��Xi������DoU�mmۮ�:�B振`%0�Q�V*Q'�]!��o�J�'�rC��DΛZ����"O��
���e���r�� ���G�)�ޫlr!N�� :�ƈۡ� T81b�(H��=z�1D��+: Z�X�"�p�h���!"��b�䏒S�2$变1'ؘ0�I�LQ�3ʔ9>y*q⤧��N�.M⤉�#I� ��$	V�G���hX4ib�;6�H!j�}�`��:<H���$*Ph�4�##FhȠ7Ɉ`�A��Y�Ȑ#SN�+dE ��3�ТG{  @��Q�Um�kѝI��6n�r�&M� @� ����	@ x� t�SwH1b���)��Ǔ#a�B�����dOD�̚�&<`Ѐ'��Q�E�4�Jd5U`TY5 V��U#|81ǃF9FM\%�`��[#ܥ�^*���PJ�8��6#L0�4�c�5��3�]��1���g��Zj4p��l��i@�x[iNY?��Yh�ǀ9��r�5]48��I'�D1��E:d$�H�$H#���I�$���K1�0}�MaR=�XTj� VO=��IH�USt*`ON�bx�U���AF�[Gzp
"�0"V(���#d�0@��� `�r�1݄�d�82�\2Y�&�gT9�i�id \P�ONI��P.��A �����Ks�-0�<���1�@�C�)Er:xy�GF�'qy݉��y&
SL5�t�}	.�DL-urQ6�_�+K1�i�!��R%EVQ`�+����ծ(����5 �p�`��SQ�X����@��a�.�D�I��>��3��Y�mnP��j���۶�u{��v�6��)�Yg���D:QD��� �p�9WCB�
��uY���U�������wqw�qL|?�1Nu�싞|T~	���Sخ^>��A�,�Ё@|XkbWÈ�g��B����8ѻh�1�q�j'���376s�0>^�7�x�m�\�=Z�R�%���x� W �,@:��D��� �* 2	$Mc��.9e�N%� �~�9��`P"�Id�B!&.�I����pQDq�Q��C��/�P�t8� M(b�:`�T�@� ��u��#0��ؒ��k ����E/Q�t ���
g��0�aN��cW�V� ��e\6A�LH���I�# ���]�( ��5�10^0�%���I =�u��:y's�JH)x��� � ��B�As����>����R�E)�	���w蟤,�?�� ��Ё ����`�4�	L14Q0�Ȗ @����p�@�`/,-`7�T'& ���&]M����i�91�?	���f]�B���`DF+I�z�i �$HvA9p����Z��:A,b�	�KO��!�%A��KZ��%'�Y�D��&(��bO��L�-S�k��e��aET\ה�Ȫ�Uuj)޶00d!��`�L p��W/� 6��[l�����+�n÷���n �jDsC�g ��(%+�F�5ǣi��'!7���s�Ȝ�P�ZA�{X+O�[�4&�%|�i��d ��mPu&;��d)I�]�o��h�c.1<_24[ݪ��0�&f �w���-���>	�4�gD�Je5m���&�6�lk��C�����d E����lr ��DDi1R��s*%+֝��$b�4��s���Қ�!(���$���1�O���,ՙ��b|&���h�B�7�
�i
!jB�Pd�`OY�0a8��8%料��@��p�_�B�6�tf[�[�hbcX+�F[��h~8�p&���o]II���Ñ�s<J�*�;%�h9��Ԇ�$��X�fi���rP.�iL<<۶�'	%ә1y����gD:�-%u[ e%SjIÙ )�H��#Ϋ�EF��b��	��̐��^1��+�0�-���p ��l�X��悗ݷ?���ol+�#P.w5ǀ���G��O}J`_iJ���U��P$���4�J����S�8��ؠ���<M��zRS�-
�����V�W+��&�4�կ��߶�2x��e��f�L ��Y�me˵�,�v��j+To�e�����o�U��]�|X(6��X\GˋQ=G9��h�b 9P>փ �FT:����n�XMc+��v�S���-.��&�'~�Ɋo�;ݙEE�5ɥ`��FR[g&p䯎�{��� �N�� ��0<_�n��n�[��w�>����U7kE�ΉMp�����_���ڎ���[�'8���{�@�	O�$�������/����>��>�ѵ0��H���;���K�� Pj
P0��p'���Ф��.u���j4�g0@��� 8������ !��Ӎ��������=�0�$�Y������&��&|Yݔ�_"%��A�$��-�l�v�D<[��K�c)�����)e�x��à���K���q��p����T}��!����e
���UP���]��_4�������-!���$��S}��@�M�A� l@���˼���ƞ��ٺ����u'�'`A|�	�`��� Ι�spI�i�w�`�Q�@���	���(��JM��4�ĸ�0���G�{����GN0��	Hƙ
�YO�P����%�� ���|����it�ōg0 {�vMa/�+�B<T�@R� �U��G����g��^ņ����
��pB��)�Y*�YB��<�p8��� )qĢ!��!�� ڂ�b�=FL��,����G��m�N�V��DV�J��E�a�JUl���P}��%V��`U �O@�S\$:�W=���!� n0�=��jy{����Cf	���DF�9�ƘyF('n���'nF!=$^�r\d���陝@�Gڀ�!�����)Z�5�iA!ܹ���, A2ޔL� 
3��t��e����P���mI��ʯ@%lX��m%'��V�{�W2@��S��Y�h0�t(x��+t�nt��_^�[@fpg GqL_��c]�+�P����|]a��'�iă��i=ZGDX	�VMɒ,]�~r!-����O-����d�~����k�HR�F6�~F�`KpZ�pFTpB�]�'��s֓vVg��ƛ��@�"�����K�h�l�}$�g�H�(�IwtN��T���pĞL�©Ve��ĔP
��2~���Uc���^�
�П(�0(B���� Q�̈́��T \�]��"�h)�W"��Yt���+h�ĩ�1��֠��sH��� `��:�b�� �A��y��i1��4�J�JZ�%�L<�-c�[�f��ޭZ���
茧�^P�6��ԡ[L	��S&��4@8I(��\���k�@'��lB:'�B�i�ٰ��[ݙ�M�xb�(���a"&��I�	\�=�X�4�\��k\��R�⼲|�!7�Jm�䪝j�pq���L�^l�
�ȂF#b�F@�Fæ���=%^EO�ZY] �$��i(���&x"'�+� �Y���<�ƄZg�]]��˳9ֻ!�eT�$��-��%��@�b6߄�]���#�'fV&|Ն�M��ꀦ1#����ڀ"S��j��
�d܈�
�d��a@�@
h��@���m���(��͹m�rRlsf�uA�:gɢ����C�lm��"��l�ͻ�˳ᙞ]T�(��]�!�0�brd��J&�K�G}zjL ��v�A벓2R��:� �DR�L��$���]�m^褤���H��c���
 ��� x�M+@�F���
~�\�Asb�ɢӹ]g^��l���Y6 	j�.�E^�J��+YH�l��l��+x�L���\�����Z"%B������Jƙ�T�Eh���hi�Q�Jl�M�@�H�e���!�ɯDo^b	[���MD�v��q+��)���%�Bd ��}-n�>.U�U�0�DTb�A�@�c�QH���$���dj�Y�NH���R�Pb��/�*0��-k�꿂�تV
�@T@�[	p�d�x@\@G��h����)�U&�D㞙��V�D�\(`���p�qf�@��:�� �Y�5��Y"qP�-��J�D,P-���"Њ�0>�J^�iu���Lj�0�n�"p���m\�Q���.����F�	����:�4�0�8o�&V#�pG<sؠ@�K��đ��X]&c�9�K<���f�����~�۵�`y���XN�D�΂]���8X��$i�DŘD�Г�0��/+c�!J1�.�:��j�Hp^�4�R��>��J󚆋{ ��Jbh�S\V!��J�D"n qlvGr���m�0@KF"��v��9�)�l]ul��sv�Yv&��ss8�D��$���w
��w�"H�i)t��{�pG�XX�p��
Y����kȁ
�a�����E
D�bG0_D��j �ӈ�
X��F� ��[��0��hR[��nԮM��D��p�2�e���>�/m�)~e��p	\qy)�DH�F�� �X�ϒ�F��hឥ��B���m���L��z�g
 7����3�����P
�
a8�f'	9B�S8Ŋ�<�6{����[Y���/��k�M  i�v[耗h��o�Ҝ]�%�W%��o��4j��I5��]�A2w�>�èV}��W�Z�-�𴣾>�m��ƱZ�_�3������6��v04� R~0k3\r�)�}���G��� ������v�L���"#a�	�/8�(ы�O!� ��ޢt��\)U�
&�A�u�iǒ�φ�$_w�> \h���
k����� �Y����܅�T��_�Md���.��[�k�RℛZ��拗Dn=�[r$G&qƅ�v*�`g$�rh�k�o��bl�ի�*j����+~�yu+�9I���S�bnφ�����m�g�s����	z�VOa N�/��fb�Հ �4�  ;GIF89a
d�  $!"-"#'&#)&0.I<d03I*<c=B;C]Fm6DU0Lm7`x@.C.)C+9E8*F6;S<-S9;e=>I9GD>ca<F[HRF8jQ:vg7IDIHGUKQKJSYWGIVITYSJXUWOUhYbYVepeIIdLTgUJgWVqKItMUtXItYUeZdb\ss[cicJidXou^wdJwfXysIysYhfghiukriitvvifujuxthxwwV�a�.Z�4g�;u�F\�Oo�U|�nw�c{�z�vY��Y��u��r��~��u���p7�\M�\V�hK�iX�uH�uY�mJ�k]�yG�wZ�ke�ks�wh�yv�mb�zh�|w�|J�`�|���8��;��5��U��j��x��z��i��x��l��y��Q��l��y��i��z��j��y��a��y��S��q��@īVɴp��[��o��u��������������������������������������������������������������������������������������������������������������������������������Ŧ�è�Ʃ�Ӳ�ĸ�Ŷ�ҿ�������κ�����ß�������ª�ɸ�ŵ�Ӽ�Ѷ�ë�­�Ŷ�Ź�Ѻ�Ѽ�Ƽ����ѽ��Ǐ�ª�ù�ĩ�ƺ�Ҩ�Ӻ�Ԕ�ճ����������������������������������������������������������������������������������������������������������         !�   � !�MBPW���������и��ٸ�ٸ�����٬٬ٸ��٫����������ڥ�����ګ��������������ۥ���WWWWWWEE�E�DD�D>>>��������������ۓ�ܙ�ܘ�����ܒܒ������������������2�22222,2,2�22222  ����2�� � ���������,,,�+������      ���������������������������� ��� ,    
d � ��$�'S���p�#F�=T�%G-~��*H�.���ժ�"U�j�J��U�N�R���X$W�����U@����
W+\�p��t)/^۸I��m�7�T˕�&��8t_ˁ=W�k�r��vE��޹�^���V�ֵv˪EGؿl���go�=���÷�1�ǐ#K�L���˓_�`�ċ%Z�`��E	�0F��l�E�[�؁�v;|����G�^�!2��!C��[H�2Cz�q�����cv�.d��5�k��Ǝ8H�@�A��&Z�hA���&�kHѾ}~�`B
�� �&h ���`{*� 	�� �"� `
v��)���
(�b
)�PD
?���
?���
)�6���
A� �
/���B��c=:A�ALtPASL�P�S("SHB	
%��eh���Bk��vZh%|�.��B;p�ݕ�q=pA��^��'�^h'~�)m�7B���8� �x��Pi|�Eʥ4�'�!h ����'�
�*��ZB��(���*+��P@�� k���Z����:��"�겹.�l�p ��0@ �F[� `�-��Nk� �B+��K���:��h&���%�Ƃ	��i�,�[�`��f:���:x�ep�D���q��v:��3h��f(�0���,�7���x���j����f/� ����J��*ꧢƪ��X@A�<�������H+�몪=���fݮ�܂{ � |+v��v6 t@�ޒn��Z����-���-��%��lg��.x����,Ƞ�v2����\�@gn���n=��x�YL^
;�(�)�>rv\lL2ɸ=���� �	��p'��Y�L���L�B���
�<��	*Э+��qc.�j�]v��{-�i�J@��M���@����5��g������� \; ى#��T��p,`	����I���x#����������"�
f�D" �H�p!fP�)�=N��vfp�`�A�	B�� �-��~��S�
z� �����VP�dU+�mZͣ۴ҷ���M��޳ܵ-g�+{w;��g.jM|�#W�ܶ6:zq�k�c��F� (H�K�iJ���j��N���@N0���6F9�[�$���.��b;0C�	R�ɀ�2b�JW���0�#� �h��7��4�2����
Z����K�	���O�@ă��Ne�` ���4����m�s+c6�i>��qYPcֈ�7��om�L[���.��^��#���6�}�_;@���3Q!��◿��,�c�A��0.8lh��'��0/�N\����R������6��Je����)H�	Ul�6}D#�D�������|���h:�	�T�*h��'���y�\��<$VST�;תt�,7��j����-�ѓ���#>�:�x�|��k��7��� 	
��/�.�]�`0���^�q*��8�`c �{ 3$�� GJc�\�����&�@�4�Ph�#FKRj�r����da2P�!}ƪ��f��h���%�����|��O��Uŝ^��xũ��lyԕ۰�6x�|p��扶��o���� ���ku�~�'��}��r^v10ե� 6��W��"�P�$�#{�������,�!������#�@���jН}^ ��#�*c3�)�l/���J䗥bv̞ի4���	���]5`=ի�g���W����w�u�Ϯ�+����>�ӝϺ���֗ׯa�Zv_��Gz���"��zU\8KTUk.ֺ�*a	�BH儆В�h�7��X��E@
��W���s���0JY�0>P�R�ٻ�@y�Z�S�z�]���rB���#r��X������3�[�ڲ��W2��y���I�;�՞׮#\�6�/�OW�
,7��lb��ae��0��y����P4�!���7���X���	���)?e�QU!�-��Z��K��T/}6P(����@��~%�!��x&;Yhu����Mi)�~�}�>�\m��S�`�#���h{k�����ݫg�SC�b/���7�tE�@;ht,!�o��ähgC`E�=��m�~�����?(C"1�ߠ@3��~�[%�^�\�uA��W� R~U���M�7T���8�VMpֱ[hc�m~�:�Y���/ؚm��M�x�ܸ�ŵٷ����1���x�4�;b�y�>)l�=���&�8(@����F2H!.�@�k�?�<��c��  �p����;�D�<�V���Z�x '\�p�x ��3r� 6<'"�صJ�?�W7a�<�y�W l�>��_�F�'g;7m9w}׆g�2Ixf2D���Ac�XcЁ�Dop'�������K!�6|�su�smOo�m�_�P�E�x�D&PTc�T� �3*��G��)gT>sU�FX�MiC.` �Ry��l%V� ��O�����w}hHg���ht/00��0p!s&�qIfPohb��tЇ�F�VR��]���2`E�vWo�W@�z�tE僀p�Nдe�Cp��K%D����q�Dp]�1x�rxW�=M�*r3r
h�5m47�sb�Sf�C^s�G�ws��m/��m�^_&s��6��C��@B�2  2 b�p@k@y��u�n�x^�w �`�ߠ
zp~��c�=h7jU.�&��fG�7 P�N�.���U�!T��*O��*HU�bM��8KD�wl��<Y�k�47�S^tE��xG�6�*냅�}hc�-~e_8 ��>�W��6 1�-�e�${؇�y���vo��^��@�G-�G�ku>f��e��m]$�g8Op���9� ��L%�'@�"n�r̤ �q<��3J�U��=\�l�T�4N3�Oa�=�s�3-���n��޶.� *9�xyn�,@;c2�)��uJe �"H��q w���@
e�դW�im��Ewv���6���4�e�bK�����>���qxw/o'e�EvRc�cy���eTC�^I4��6��5�fy��i_�>�>��ExT���m�3 (����a�q�^h ���h�th��Ў�p�PU&G&96%y6�'6v#g��l�#>x�v8@>�*�1��C2�;B4���&�YqU��k�rr�"��e�*��+�V-��7т7�U}s�VrU^�T^���՞�.+�C�H.A.�tPt�\Z�Hq�|�����
#�2��ѧW8��!����W��N�)^�6d�g�S?�2, \���[pC���58=�6c!	RU�"e�r4�4��P��e�T5�T��%f(	�6��j(g���^ád>�H8�E;��2�T���`�u��������5.�rΆf�l�%^��e�eOw6s�js�����qm��H�dTf�3���EP�*��&*��m���ؘ5Gs��-
���f�6�}��I��a�Ƣo�@�z@��e uqПt@
�˗9��bz�7���_X�fH��g^��_�)P�* �/30��Ϸ/�7?�T/�j�g
���W4�7<V�z"������D7��E���m6��^�Em}% �
��:�֦/�	A|3B����\�u�H��q@�@��
u@���[�E�m�X��J���-lc��Ņ�U�@%� Kt]C�;�L��j>�PEvC�k���Sl�KNV�+I[���e��z���U��y���X�';�[��7�0u��z��j�| y �J������	r�h����^|ʞ���l�F~ռ�����#�x����)�w��ĭ3 �1�W��3�k�-FS<ʃ"�4A�]�Ҭie  K��[�vkm�Ԥu�f���2����^�	{�|�t�	t�y�{Pyp���P/�z�ԕ�cyu�-�U.j�=0�s��Ң !�;�jdn�;n����9�J�rP�D�sb��+YCO۲��w���:�_c�i�p#gZ`Q�4X�_�{�	|��f�	p �	+�z ��@��>Pvi6y�ꬣ���w���6)Лc	�ꉃ�8 ����`q88�'3=s �Vy��P�,Ԣ��C-v[���_������_f��?��6�Ǵr��Y���0^ �Uu�	���0�wP���_��0��P8�Вvx�E3\�ɫ���-k&C���Y�W|�Ђ:�Yp23���b.�;CE���q+-�MT���d�7y��,V#lm�F�6���kG���K��+�_�Q��e�l�"6�u�	�	����u��	fP���yy|���Y�:�E�7�S�eT��e_Ϋ>h6�IO��42�ٲ��X�C�bL?4E��ME��3��.�BB*�M�v���Wo�m����YG�"m��6��:��u�w���\i|�L���ë�P���舏�Wײ�~ձٗG^�e&�6w �GI.��Ҹ>�26�%�d�F4nK�*���F���F[��!�Vv%�tԱ^ؗ�F��^h�h ����	��ۚ�	��	�}#�f
��  .�݅�b�J{�WP_�Y���Q�s;Y4(�1����R�G�*PT+LfD?�_贋��v��y�3��_��_��f;�W�U�4�E�&��Ƙ�	�#����z0�u�	����0tsE䂀����;��
Ȍ���d�_�V� �>�3)��\F8�/(��%\�T	RM;z.+ND���`l�Ympf$��͒�f�.qJ>�����ne���	w�	t����\�q�r�� �@�����_�X�(i�QL�'���Xxfz\��z,^�s{2`�VE`�¢��/=Ģ�d`~�r=�6,��+�
-Ճ��B#)�N��by0lO�UG�K����7��u�]cou��#��r}ѯ�ѯ�垠� ��	/0���G"������Oܡht�\�N��+Ăr���s;�*� ���n�Y/�1Hu*YlK����D�4 
����
�	EA	� j8�-�w��z�E]��[���	��	|�	�	��	�m����N
� ��
��H�-w�*� ��]�N8��^����m�y�S>��,ɢ )��P	��
܀���
��O�(6�+aT B�����
B/��0D�ؐK*�*hH8=����
��
�ɠ��
�l���;��n���-��s:��9yW����h�	h���	� �i|z0
� ���ݬ��$!Q�C{7���{s�N,3O�	��hA���`��P�_��Q!	Bo,�d�����8�wB$�%�CE����k��3����
��o�?��0�A�f����2t�u��ҕ�{�l �e����]$ d�G~��,�|������z��`���ɍlѬMD/F\�� @�@�4X���)���㇏?���Q�7�^={������]���ʁ��	
�Q�,j�\��Ń%X�`q���z�АTi
N��z�m�W��mC�M\3\��5��@�d (P �ZԪ=0`m�@/� �=�aB�g��]{j�&M�cz��ѦF��q\�ճm�^��y@ m�� �=� �]��Ț`��g�>� %r)��hO��r�Lr,�O_9{^oy9���ấ(��N�x�b�x�-\$�`���&\�8
?D�&P�p"-�Te�^)SFp���XTqER�9F�^j/� ����"M-���`�� [�÷��G4�C=V|DM0��;X��e�Q�:�1  �pC�^� �p㮰�n+��tS~�1�q�*�mp���W����#p��?����U��`,�L!!�����DA�ZH!<LxAP! �f�ye��Q\��ŕW`�d�F!凱:����kU�<�0�Dd0�b}�.��/���M:a�X@�D@Xd�5I�?e:�a�"+@7��"���Hs�'OM�ɸZ"�I�`b�r�qy����X:�eU����E6ad�V�jƛ|���E���X��SU`�d6�d�m8�x�r��Һz���+n����m�n���eeVq�M(q�F 1�3D���I��D��-»h�4���0\�n���O��S4�dOHal�G��$����G�\�zI�\z��&E�\�x���u[ܥ� `C�z��*LYA�fU6�J�(�� ,Od
J.�%�f�q��F�F>Y�^n��"��ag�o�نqH���9u%�S�UŔ޷��GU:=�S��D�:�(#�2�`m�Xc�k.�$�k��ުkj��=mog��>�ح�f��G�x���c��`)Â�M��<�.u��@����d�%)`$�0'T"���@����q�z��A��x�#Q�DAEpB
P@� >pJ�0qC�P�T��|\�J&��W Ի��.�蝰��O���8F�^q
Ux�W���\���!f BҸ"D�[��� H�� /�: :4���"
���5�NlbY5��P=4���j��ڹB�AҢ���;ٓ���-vM���F�1᠃N�@
r����l���"v(3��E0k�(�C驆?HD"A��q"�G=��T�"_C�&�p�!G�!�7/���)�x�؄+6��M@"�fp�/��!A?��P �̥�t\�Z�g-=Vo6rA� ˲�W��.pa���b��0f1e��&b#F��1��E�\����|YB
��O~R!�q.$2|d$�P�S�A��X�7���6.	&�4�
\��"�A� �|@���O)�
Q�m@��aǀ��d�s��Le���4$zH&���G�!�pD#��6("�h�2�}Q����ᄩ��mpD�B#�� $�D�,H�HG 0�J��]��+Ox�X�bd�	Nd�l��D,8�Mx�H��')!@��-J-�R�"ፙN$K�0g4 7'4ë?��D� ���P:�02�s�R`�Jbc@��zG�vUlX��@�D�!E(C��K2�7�rp�+p1�m@�A�*̙�f�	E,^�
	� ���d��һt�zz���2�4��z�kQ'��=H��$$ɇ�=h���L2�P�;ކ[u��D�n�pS@$|��zP������ ��J����=^��!�@8�j�P"���r�
V���C�+�Y�We�hf��Ј��CЯ~�0�2�Y*7,	HHB�P#>�EHP�N`#ؐ�T�JU�:���.p)�izAu��G���J}j�m���Cc��H>��\�4z䁺�Z4�Kn���f8r@SP��������(���HF�c��`ٸ��!
e]��̐��E���i�(B`1#�N!��e}�*�鷅�`��1�@�:ۀ�/i0�����@��T�g:�-IV�~h\��=Y/�H/�˫h��]Fy�̋0��$B�EH�q�@4dq�_
�.� C�@k�mw�[�C��ԒT��_��>���s\=������J� ���*�Z"�Oބ"̠�R�}����x' ��U&%b!����@b9x�Fa(ж1�$B/ ��P����%nᛒ@���ĺ�����i�z-g`��*PT�޲_𧇜?"����=�����l� +d ���27N��F�b�,7n���A�U|�G�Ї7ء�؁�"ԾR+f �ɭ��ȇ���X���Z�"H6p�:0;��4�6p�|c�X@�NA�K�U��h*	6�t�J��(�X� �H
�� ������,�:�s��;�Qz�� ������X�M�����aI>M��;�5OP������Pj	�����X��R ��+�Xp�r��}�8>�ZQ�
�[����D 2��j�)��"��3��_�"�+4Ӂ6�<x�v�Nلm��h�&q@���nX�)�(�\.C� ��'�,ʺ0���PB(f��fK�TQ�">����SHB��a�b!�:؄WhK)�^�"��Xڧ������6~P�2�� Gt�e�`��x!�o�q`n`���X���Cp���i9c�F6� 6؜U�t84� ;��\`H�����q��W��PةT��t��K���kL�1lE��6Ip
����ɽYd�R9�����-9�X��)(W1���a9@�уg���	��(�[95#�q��:��3 ~��X��p�D(�}$��-لd�*Dy�!��1h�NPU��M��X("K�0 ؆�*�m(�qC����PL��J"!e ��1t,r���*�#UlE�T�!�n�L�tą�!��#$^y(�'>��$J��A�)�Nx�ZJN�I҃:�9(�S����1:ӠN��J��Rb:U) P��v���=��fX�F���2p4��(��OF(�F`�^���̚r�z��Z:�ۡ�3A��xLnX�S���EǅۆXP?��� AA��3���k��t�m��QF�3�k!(�k9�,OP���s�($�9�ga��9�#���0�ƵH�HR�;�(������m0�! �"���9Ѓ��P7B��2�9z��ଅ��4Qw0�D�g��D+ѓ@P{@w f�-M����z`HCU�U�+UpU@�U��Jp�)X<р"؏z���8\�'0 ]A����Z������()�"d��N(-�aa�R >�($Oi�@�SH�'Y'��o\Ò+� @�D��r;���2�hF�SUp��i���^���q�;eP�<�D���)�;m2o��K��X�?B5	��r���JX��Q�U��P0�.[~i�fX�X(�[0LtȅU����`���Q��J��ߺ�`$����R��ɑ��B������5% �����"k	&@�(1�9*�� � `�[����C�P����(�Wh�NC����;�!��r �Zp��1�!nŏ��9�W`��0�q���Jh!`*3"�G��2=��t�M�X0Q��q(�q����AS1�M������@ ~"��*qY�nW���O8aAʋj�N�H�H��hM ��JP�%��$���h�I�3@����J�P�X��ل�Wp!�����hՅ��z�\ q���!�mȅ��o ��C�m�^�u]U��hr8�[J�����2��-�4����O��X�H��ɣ��^���]�ξ�=���,QӞ��A*O[<�8]��bуM����(LP�ѡ^Ț=�X ��P��c���@��=�H�@�X��@(�⽮�)��� xq���s�bd ^a5����0r����!y��5�Zp0[[P�L\H �,�C�D['�8p��#6b��L�P
ۄ|aJh`\��^�&�a~���?�$JP��iS*U�,N��i�$�F0>`]i�A�	m�a���Vg�Nt�q�^(�U�E<���Z �)GpPP�h���x�3��^����K�p�G�)L^Д��KM`�M���3�ț^Z���X�-i�����RIXZX����\�[p�K&UZ��	���ْ�#"���k�8O��M8L($@8ZE�� Ȭ�־�(�-웖��'A��dZ�UX�eBy;���U�!U�,<hZS�X`�	�
���E	�+6��2���v84�D�zxÈ�|�._^�P�� ������J�F �Z����Z�*���*�p�OƑ���FߊAX9=��� �S�S�Q c!�N ��9�D(� U�Ѝ�p�؀�X�Z�� ��( �����	�BI�S\���d`&~h�M*jKX�K��+H��i�+Eu�2Hn!��h2	�`�|5���ȕ��<�
excR�D�}���m��[�OE�싕�X ^A9�8���[֞^�"�֞�S�c8��.���ϑ�!���-�$����V6P��&�=�P�����_&�nS PI�!�P�%q�4:_qͣ+ 
�e�╠��ۆ ��H�.�ۋ�P3)	�)���V[�v���X8�㩅M�gHP�W��zrȅHP��k�ޤW-������9��#�ξ����p>���p�Q0�NP�Q�(�^Y��ד��P:�\�`�`��k��W�.�--�P�Tܧ�	�z�'A�ߋ�A:�- � ��p�M���1�m�4�L��kL�P�|؇�B�O���Dh�-= z�i����"��U*O�fe��!X�-)�#�P
W��{�X��0�5%a�"a�L@]�1+WЫ�
��֐ � �X��X '��A�0P�������(�BQ.JO�E��m���a�]A��0���p�Xq��q�WH�U�|��p[�[k��a�)Y'����Vp�Dt5T� �һ���k+��+��򛐅�Z����5�e4��~zMX�.o���"%�p�lII�V%��P E(p����wA���H�z�h.�kG�`�}��;�X Ԏx̍����
`�0�U(�-�S�"Pq(��j�Y�a�:i���챋�G�n�|� ���8J�'2ݮ��\}匑D`�<�[	�eM ���+�y�?b&����c
�Xc����"��Z���p�K�o�r8oX8|�UXC ː�]|������n�G��X%Ƀ��IQ�`��T���T9����P  (0�����N	��IS'S�Hu"uJ��F��A��@�"E(P��"	P��@����$�` �:uAⅉ"h�h�DJ- �p��6\�J$�s�ʁ  ��5 �&G8p@ÐZ��y�ō�zู+����f	¬j��N�sP�7��)��B���^"`n%��k��
<�2�ޜ�Hh5�ΪfW�YU�h� R	3թ��Q;A��&Mu��0[ ����> A����r�u��&D�АB�����+V�Gu<#ٵ��^G
М��I��<H�R����8�S;��7��0�_��@�v�j/�$)@��%�ؓ��d�@aUxl
�PN�p�f�f�J�\�#A���+�h�C�x��&�`��)��a�d��ȓ����څ=��A܉�y���3�|"�h��R*7e`
���v&L�:}���7ʐI2`L}��AonEY_a���Y�*5 0��*� �"���8X�Ps���RƉd���ZL�P��B�&t$R�#N
1�R,������UUd�Ҳ���xP��+�����"'*�)��+h ��p�N9�8�r�~|`@f��Ҿ�rEg �0��  �D����_`�VU�a�6+V
P@A�g�4+W�ul�@�0��)�0Đ)z�!�|�A�G"LQH�vi��dYС��kb5�T�%�0&7�4�B�-j�)L�"E0�/< /ސ��f�P�L�����nu�� J�~j�U7#́t� 3�]p����� ��fQ�Yj�
t�+��"�+��R�*�$�HE���-�0k^y��X{��-I��e��9�
7�x���UdN'}E/����%���"?8�?L��
:�xヹd���u?�Si�Z� ��ձ���f!��ZLK�eL���*=��婤D�0�,���t��;�`;���&�"'*U�F�kXf@�� �0<+7�Z�ZfBX�i�g" G=p�'����� �r��
�i	�F��T�B}��VF���d�J9��yH�*E��0��~�:Z)�p���"�[	�!z�C#Lq2S`�%H�'����t��I²D���c�Iq�4���j"jD����PB�A
R$��S�$�A�"�mF�b~�6�Lȉ��aC�O!,K���n��E�,
H܎^�*���+�+(��S����&�`���JSTy�?�%?[�O{x�}��gH����j��ԉW��!\B�xE,6q�OHB��
���gw�I>��}���,*M'� �,��̌ij����}�ژ( ���J��5S�W�K��tuM`�&
~�
n�#d��WZ�E �|���O��0_+�/����:t�Tt�ѱ.ԗ���>pD-T1H\�S��7�,Ǳ����P��](�����|$V���S�Y�+�I��2N� ��S�*�1nu+^b�/hAp!w�C���X�ST~�*
��7�a�� ��Y"OA�����+^-�̢�`�P,�0�K�b���(��U�&�[	���=5�i�b��j�$��i	٘� n�%q�U���,�l���XH_O��d��e��	ذ�z�W%P��t��S* n�� .���<�7����g��K�4�`Bs (@��E�V �Z���H�n�#��h�g�[�� ��$p��$�C�����bMxZ%*��(G+�Qe@��8F#�@�Dp���8��C�M�DWI�k��.�A� ,�8���م��&�~R�D%����Pşxa��1�9�t�C�t�+#D�h�� t�K�ҊU�a��r<ֵ�0f���. ]�0<ʐd#~�.&ƀ�F����+T�JA
r33�mHFh\�(�	d�R �W���� QY"��>���:��6�
Wa1Sh.�p�Qh,��)>{��}BP����&�2��՟tM^�W�
<&�o�VÒ��
$��& �iax����f �f�#���8v�,�,=�.����L :�
���d	Yt�o�f��9@-��`
�`PU1�C�����g������B@q�z��Р��JD�j�{������6�=h�@�4��ވ1���pG=�q�O�[G��W�ns��,����F"\��,.�+ա�8#�ɈQ<2�"����@�	��.�q	*P�:8�9S��T�Vz�'�g#����5�Qi�6���b��X�(J��Ϥ�Y�G]��:�� �;��r�`���)�+�.v4C�n�"ʚ�j�Yd1�W$�Ў!�fa��D�*L�Tp#�Q\ֿ ���B@Uu�{
R���?�?�V�ĺ�O���3�r���&��1�i����7��؝$0˷iX��̱H�8�C3�B&� P˻��%
N �C����M��t�$�%�\������@����@�Ј�������Y` �PK�L@��x�x( ��'T��1�CX�&D�&x��2�C=��؁
�P��D{��B�TKL�848��h@�@��P�I��Wm�$��,t�8LB�������|�N�N	�ж��_�������_�j�)NT-�V�l�'h�&lB( 	��1I#|B�$�'ȡ���=8���Tu���ؠ���*��6��*���p�p@]�ጛ�͊�\�PL�L�l͂�鍉���ؙ5b":�����Fu�|<����uN Ω Rg��(�QdזD2�&�A',1V@BpȁAlB2|��!>((R
 ����>Y	�À�+|�pSYXT�5G&:�q�LP���Q��Jk�
�4^Kd�
0P���W1Lb�oL�8�ތ���L��V�M�,��@N0lB'�� ��E�2��8��4�7���8��@&Z����-��&�]x���+�
_�
+1
���u� �J}�$O:�9���)G�Q���L�3ݠ(�)	" %Ƥ�j��T�Gf 	,$��%-��'�2(�k��$B&�7��;�=�$�N
�>�T8:�C,���� � i���%s�	O��R�9��0�}�>a$��>�_=�2���y�ZEQ��|O�l�v����Do����+��&��'���%��<�)��4lZ�6���<Kl�Eި`*�,�C3�%�A��@	x� ��E�bV�b%o�h
���X?ye=Pz'�X��4̾LgZ}�j�_}�"_�'���1lB��V&�2,3�Ր@�&��4�)��+lC\�!;A7�D� �|)�-��+̂#� ,���h
hp�@ ��h�X>��VP
Kpd��Ǳ�:
t������PΜ͝�޵�'�f�*�V|`��ĩ��)d�#l!�Ƃkފ&@�x�\�'�jm��=��%��nF�6$4��!ҩ�Hfh�
���٬a�����˘A���I&z˧$�:*j��ڬu���u��!ħ���0ѶaϨ�jo��j��~�3T��JN2 �JN�I	���/��IW��� �6��#��	hǉ����i�u���?���j+��鮞�2�t�bY�l��]���K�8��Hw����� H��2�1�Fp��}�^������=\M��NLf(��-$�a��j-��K���R�����hp����>�?u��������"U�ڋ'R�H�r��WF�,@3��'4�2l3(�)d���)(C2(\K� %��2�^��	��J�FL��t�C,$���)��d�ю�c�ɧ�y���_��l����R���'��E�/� 
wܧ�� �*�V��kR骶����� )<o,pC9��Q��.SJHk
�. Ch�G0���L�e�A$��	bҲ�=�̏>��\�eR���	�Σ\�(Q�Vx¾H�$J^NT?��X��2-D� y�2�P���&<�*�$B�H/���*�j��h	�7P�p�!p��~��L���ԧ���<KB(�5����-����l��l�(`0��e��@���2T��,)����'(C3P�8�&(�+<�k2Ñ���(�	�^+B/��6� -
8��e��*�T����Y��D��*�u6zg��#S:�u��IPS�\��^��fb���u�����h�0�����E����G�	�f2HC⒱2���l�/��1�&)��j�&8�&���� ���#hl�$���3\�@B!��ڬ��~�n�.�(�gobO+��q	��n������4�B�O�XHD�k���*�Q��(C�h�J�^�FEtB,$C�)E
$©�0.�"�J<nX�-Ԃ"� 3� X�:�W�����|�}p�����r$:/��/A$G������'���ml�H��@P|���3�)�*_IC�P�\$H�J"�&P�+�T9܂�Z&)�U��'pV�"�F�����nU��j�^0��e${������z�=�_RU���t"����W����Eh�����1�B(T��~�6|C�1�/̀q(�l$��&��.`�°t�@-\���x�x������Cu���؜d�}�W�� <⁉3�O�<���z2٢oBhTm'y�[��@�V�P�S3�	�F�SS��
�lY
8&,&Pjs�3$B�T�( -Ԃ���[PP���$��cQ�cy4��h���i�i�J��&��०�8��4�i��{��6z�tr���ƹ��Ÿ[*�'5��7|3��)��6PC8�x'��j<�@��A-n����L��B#�@��G��H.PB.8A�A -��a�@��#�� @�a����2PF�
^Er�eH���О1W	�Ϋ�sI� 
0�]rVq���+�f?�6(޸�+hǊ� �$�(.4�-���\��$��d`@�:�Y� #lC$�0�U�yS�ҸF\�4�4ќ�n�U��K��Y������_�}2�_g`UR}*g��v� ��f���BJ;(�0�S�2��H�#<���*@�'P��I%���Yx�T��)�*l�Cc������% ��}8Sڕ �z�$?���b��a7GڈY<6x�@��J 5H:3,�)��(��/�7�C�zB\�	h:�(B#(�*��
�\8�7����Fs�r��
�/�`h�`(���cc��ۊ?���7{}�0����<���$�Fy�;�����ckm�(�~8�BC,0�7�|�$,h�{���A����.&�RY	/$B��(x���4�V���.(��R��@��dw���g�Z�����~��]�O_���h����S�~@��IzD* 2H����CCȞ�:����P�
  ��<�%�@�|yȀd�O~���J� .��Mt	��k<[�<>_#��&���gX&1�(��2�6C�cs��H��3@ @`A� ���%L�`!BÁ���L��SȾ��̕�M�^}�n�/���M�2N~8��� ����Q��&e8 �Ř��P� S�p@�֪��@ Lk�@���;��B�J1,�iS�����7oR�r���x�na�y!�����-B��h��c���)n�2X�`)�F�QY�A�đC��'��X<@�Q��,Jh��B�H�Q ��jѸ�0A�ɝ�ݻ0����F���d�{��EnQ`��	d_�W����߭�_~����"�8Ꜣ�L@� 3� p���o����IG7:�S�i�����9� �!|p"�� ��N���O� ����j��
x@��3L�"��>��3n��p�) p�J���Q�%��o���4(.��K�?��"�:���k2���K�P@��f���a��o"L&eTZ�Of`!�R��(b���!�P�!v0�4�������ȅ�凹�+�=���,Ύ�O��n��1��3L%���?Ŵ3)*��zEj � �Ͽ��o�h1�H۴k�fBbgCp���g�YG��>&��&"�L5X��K��_4x1�j��,��8�8R�������2,	s.�_9����Ƌu���Cōޒ�S�Zhck��/TZ`�3��9%���0�p�IX�z�t��cāP���2��Dx 9dx��J�@���k�d�.r��$�,��&K���slI���X.4�չf�ۊي3��a9��e_ǻ(�*-zM��; TO�_����,�B�����a�O@�I��55 ""���~`D�\ *���	-�uc�)�^j����-������#|Z �2�'�$�Wj�˂�D�e���+>��(����v06K�抖���CfPeD7�u9����3�B�A�Q�� %p�	�S��!e/ʣ��~���!(�]�&�j/��`��R�^g?��^��v�Q�=�CO��U���J!�\� ��NwQ2��%#ԘP��G����%/y�I�� ���)xv �� "`���A�@�*�˘�4v �h-![^�Ҕ��&	y���$�;,���.Ɣ��ni�G(,V�JZ�9����( ��P��F�B��)�O���7\Q&�����F�,p�-Y �Q奈_�^���`+��r����A})��6&HVpi�3+^b%q �������
Wi*�VBp;(@w�5R���������@Ht�P�&\���$"f�� 	!pa������跲�g�mB`���P�X!��Y�0Q�G���E��5�f @���	Ca�@9�J���0N��v<qs���8�Љ�f��x3�����<px`(%��V�F���.{IQ�$!V)�eb�X�4õ��qh��"�S�gqc�iL�d� ��=���U�Ԡ�d,+B�V��n%�
 ���R��oP��¡.A%c��8�1*+�`u)�`���u���e��@�`��@@s��+�E��Y,���W&.n^��c�3��3��GG�\u��"'b�
`v1�̟i���c���]zݫ�X|#e��(@/�j	X��aN��[ �� W�js��9�a������$����.��J�\����g9J�+Z��q��4��R!X�Ti 䩌f�Ô�%?���g��S�QH� X�0��|-~�[��xU�(�	��n��L �-o!e�_�ď�t1��W�4V�lXL�l^p����t0hz	;�gZi,�
]=�)�P�]�7�Q����0�fpK�T�QY�C�	;gυ� �R��q͇��k����)��
Ð�C6�������,��!S)�cJX>��7�u�����'�����ɀ�;�imMؗ*"0�#ʀ�H�r�0L�����XKb��Yg[�~[`���<RY�A\�w�V���r�bOʙ6@7��RY�S. ���訡�~��G:��pl��*��G�(�Aj^�s��]�1���� a�U���,�!�b�חr���d�K��Ӯ��J��)�R��v�ցf������\���#�Vt��aw>T�qBx�ԍ�tM.��6�רX�;<��2��ӻ�ָ$B���*��Z1���e qˮ`�<�m�:?w?����wv�P��83�D��#�fG���s�Ёv���4��;>A�) �V���UZ��$�)بEA=+�� $���u�i�˱
��t��d�Ŝg}Ξa�+��g��DP|�(��L���)gf@7
�	�@d�d f@AD@ܡ�aR@ AD�"���Y�#-D�����\�Ϊ�ʣ��g�pE|��=$#�0NE����D�M�ɇ�丨���n
`A����m��!x���M�.�B.���`_d�X`�/(��A�A�j�	�b!T��@G*��� �-����NWB�|*ƒɄ�����U�oy8�ބ�L�=�H��)��ĢC��i�� ,�
�����%����x����h���A^�`����\ �H ʡ�������Q�a��V������Ç��m|h0�#���<�qJn�H*�X�<����,?�J��
���c�.��"B��<�����\џ��䦰�
�	v�De�^@�����1"��"���$y�* �ު��AGf學��� ,(�����n�x�=&FY�G���*����"T���҅��A��k�M%�hfO>�t�>��^@k(����A��+�A����,�Ʋ5l(��� R��v�1B~ W��{VO�G;�d;��m�"@N�T� �G'�a�Δ���D�H펺q�
�>�D��zO�����O��P��L���LA^b
� >�b�Y`4A��d]�aO����6��S�!H��#	3�P/�f*)���&#�Qq�
'-��<(�>x2G����2=�Ä&fbք*�"'Ϯ�����C��"]v�,?���,�AC�A0kv ��A�X ^A�!^�]bA��.��!8�!�AB�����'�#��zxi�����F2��̀n`^�yR�
��҂Y�3	)x�d@��1�!��_�>C����ߩǐQ۔A�D v�{��J�a4?L7A6t�&�6��A>a���2�*�P�����-�B7Jʥ�C���e��<�K��:��b^Od��91#6����� J� /\q�,��S�a�!�M�d�O�P�AjG�`��'�^�N�6�F�����FAH�ZA5���ѡ(A�K�˨��(�R�@䛬,��(YR�~�/�H��/mH��8L'�->h�"PSa&Ʈ^��� ����B��\Qy����
#�,�!�7f �N`L�vf�6 �̠ʠ�����K��p�(ò��������Lm/~@:\M���`�)df������,���@&a�����[�2
�x
��Ȋ��JZ�	��
�,U���?m4�2��m] �\����$έJ!| R� R� Ra
*�L_�ā^�P�ט�H���@yfj�ҨEׇQ=1WNŶ�%%�fC�͢H�I���,:׸�I-8�G�D��?�e�����!4��J�@e�*]`<@b���<6����u�@� ER�wg�apx!B�p `��&#&�5���^��."��3+|.��b��m߆�e��^�,�.2�b,sol��΄�� �
#D�D'u�!x���!��B��mf��@\ k\�!h���m��j�@̠̀� <���u��D A��	 -�`�B-�  ��p�o�&ϒP}���s�Z��:RjJ0���ĮF��fi�	�b�N�t,�2DP����41A�`�`�@kb� �A
��`�.�<��@l h`^�6 �>2��p ��%Γ*A�&�D��f���O�i1�M"�;/U~ºFʉ�V��x�t.�>?n]B��x�_�q��a����ANjv�t`n@f�����v�u�9�`����H�N�D.E�n�:�"�,�6����u{͹GL�J/��8x$K0l�ʬ'�orO��"W�pGm���3J�� �Ep!?�T������exo����h`t@�Y�ҖN�� �z��� �6�`�WT �_ L�Rh��2E2�W]B@!���5n$�Y��GdQ�#)z�[rP��,���%���{�q�B�[�&}`X` �$t�B�A�a��n�?�8��C%��`�f���V{�
�l"�Ox�`��R�^|�x���B�Rl�2�R�bm[Q+��wz�\�D�i�8Rg�1�%>���Qy��H��;4�
B����-������������ae��O�������c�V��B�� R��� f�^B���RR`m�!�/j���,z�2��]����t��t�b��fmq��~K
X`@܎EG����aB�y{��Jt>5�A���`��f��fH��Um� 
� 
H��@p`|�|�SR@���� >��bz�<�c�s-s*V��b�iW i/É�ĠsJ-R����%r�A]�h���)���h�%Z���3���hp���&+ ��Zxk��Ox������x�l h�!��)0e�@T!�!��K�1�H%)��A���/o2�x.�dr_&���;;'-
e�� #l08���_�D?�Ѕ_?����^aV2�> �F�:���6�Ű�;E��^��E��o�����7|��|T�4��� V�(�F�8Z̽ͤ���D�=�K{^�e�@�Ta��T�S��a�]�_	�����\�`t���d�F� :86�=��i�!R���阰�:-j:2Jt�6�J�D�|��)��1�ҳM�1�w�����I�Q��Y>�WH���s��!@v�=�aD��6�`gO���S~���I��M�`t����q K�XB~�������El"����_@D�"F�*Z�'4@X���H�9����82g��n����W�'��=R��'������O�3S*IS3B�PΫt�8��SM���A�h�`Z w��= (���<��C�����E\�P +��jj쩶�� C���`�P���7ȟ:͖lL٦�gL�E��?N���1��^5y�8� �+9 ��mྐྵ��*�8q�؉c'�!�o��Y�5j���B��]v�p�#��,C�CD�8P̔��L�4Dl�!2D�CP����E�/4��p j   � ҧ��J �j �W˒ ��j�8�6 ۸��J��պ��58���h�=xna�bՂe ��B�|�nU����ª�f�Tv�U�f�ʱsW���oʘ5cX/���p]q�!�.dȰu�e䘑Ӽ�!>�ȉ����2e�)���Ѥ>p�x!"T�e@"���=Hp�6ku�b�l���zY\Q��`�� ��!�X\m�UVXq���cuM��cz� �5h � �P�&�H��7Ԅ�� ���J4���<�<��2�T��n�h"%l�L��Br�M�B#�����u81�f$R�X�QDE!x>�@�I5Ւ �Y��RG8��� �%�r�0 l~5@ra)fg�f�u`�������MXلW` ZQ�E@f�5)c~-�h�N�n��8�p�"Eʸ��l5��N=�Ì4�Ԙ�Bup��Iº��w��!@Q�u��l�Ѭ&r�QGf�ԥP�Uw�'���
@�A
�1B�#�|��6��)��]X�~���b�7j��v�x� k1x�_���b��X��F8���x�*ۤHk=߰3�8���Z�ԌL�7�lc��ʰ�&ɀ�d�e�zܑ��7}G���AG���c���$���!���d �@�I"f�R$u��ߣ��g1,b�WUʩ�q�iUx�հ�L���\p[�uƗ_x�B���L�*��M2+n��:�#���n)#[̠C�/d�(���T4,�H�8W�/��D#N6�m�54��B8�`B	
P��XS�-$��h�U�1�o���xgx��u*�^�FZq��,��W)lWe��Xdp}F�^U�0�X����fe�*`�j�9��C7��;4Q�,̀X2��H�ш�d@(B��#�� fh�#bw����:�A
R�����\:�J
4�	:ؤ�؄�d)���?K����0�(�^�������,hT�P���ܩl��T�&�)�.(�7��p0$�;`s�z��"�Q=�ш-� 8'y�Iʠ�$M�~X�ϞE�D�(B��	M�hr@C�@�1N���J �� zP!�m�2M���@�2a������(�ra��ϝ�d��	1o���Ù��80#�ء�񆍙���9����G�*J�.|@���)L%7���k��D"�e���r��&6�Ɂ'd �l���`)( ���:�.�],P;!/x)P_���`��~В��d
 ��]�����1�:�}G���;��7��6�cW�r�:2B�����Ц2� ,b��&�+b�.��>@�	���Ik�*�&4��
D �B��T�\!��Ta��H2�Ml�dn�~�d�^���lo.}��"0�m�j�G����\`'�I	�F�8��XHi�9^�"�pG�A�-�@2�����m�㎧hkY����Ђ��H�@&l���!�!�/p��ǔ@�3��pP���L�ۡ��1n�_��X�) �^%?p��b�WF	�-��e/�K� �Or�� ��Ф%E⠈8��̇�j��	�E:�d��������m`��(����Р'|ֵ�C�Aw�;.��8mCQ� Xl�i;x�	L�����{,Y����&6��N�1`�e�����+0��!#�E1�c� �/����,u:���##5��E�ډ2�L�v��m�H��@D�ү����h�#�;0���$��;�a�&@z;���0����j�mM�{��-���]D��(�H��B�H94V�ggFTKz�8I	����zP�/ĳy���h�7.{ �9c3�T.��t&�\q�C���
��G�awPw��� � 11a�����i(��Q�f|A	��X 9w����؃I�BV��Ⱥ��rC�9rb@U�b�z�
C�^"h� P (�<�m|�6��F3�q�V�CN��62�x�:�.Y��rNԈ��Xm� � K�h�bg܀�P�qNp@� �LPkJTF�5���&@r(p�4��PgOx�Q�:/��_���So�z���V�ăQ-��"�>�޾�|I,^t��m$��)�2��p.�P�Ѕ1����)�av��� �PT.�D:J���~����h�X8��P)I"�4�6�`-`�t��\O1-�CT[��e!��Cp��mIuXY3�3�d����TJP��� ���:����!@T��*:�� Z���v�#�c�c@gH��>B+�PG)z�$`��{Y�{r B�6��Q5B�4D�ԁ&0f�	��(`},pۥ Y3U��6�GDD��)c��7޵X"g6�GLj�jJ�'�F?��J�7)r)�a � Zo�@���TG�>���ۀmtV�ct�0[�!8���'� >6-�@��4G͑4c���ؕ$,!���P�e}�pp��t^X� ��R+�7�C��Q�R�&�$>�a'��0��_�� �1 �W��dfA � ��EG����@�� 	U:�z^�^��PT8[l�`,�ӊA�J!� C0��P��#�S?0E�q$�c����% j	�5�� �Lj�]��KR�X�h�r_����K�u_��
� � � �Q 	��I ��W0% ��B�`����z�����@
e0t��y@���ш������J�dE�'$`�S�T)0<> &C5� * ӷ
P�pi�D�&�!�L����YT(E��B�Ҋ�R��~�3  +�L@Q@��T0	T�I H 	 h��A��)�)7`D���5Z>BZ������`�9��ɓg�-���0��I���@2p (Q! J��BD� F�%D^0���8��Ec� e�E��PaY��:�hL�#�����/"Y��~t'_� 0�T 	R J��i�� � ��i?��,C��D��a����A������pHea�6
��ښ��4Yc{B/��7��]RD�)��D�E@�fa@���!�����/Kr?xS_�%Q���#79�F�) H���9��H� 
�� J� ����
�)�Iu�^�m����	J�8�C��	t0t<铍������ QQRF�w VYVC��1�����`�q '�&�.�3j�y/kQLV�!)}3���>��wcCHY?%�T� 	 �� S�_
p� )٪@��� `(�RGE��˨����P�Z�Y�����#h�f��v����|@�5Ԩ�����!$��=���Y9s/���h
P��~VƐ�k	�Wx�aR_��o����0� � ���� ��SQ��Zr��WUZr�v1�!G�JG	���9[� �P��
��@��z v�� ֐��سؚ�/�pF(�yr)[�A�*�I��)�v>�k�ځ_D1w�N�q|�7ud�E-�=Ъ	��`� �
�	�VZ_	+����"�����)��!��	
	f�#E�@��P������`
� ��Ps��
�P� 5'غ�Pҥ$L��QXZ{ 0O4�|,�/��R���q�)y�&��)l�! �l���e{�Rڋ0K�Y ?���Q��!�9���X�L�I�Hp�W�_��߰	�����ۙ�r���	? ++��)�#Ȱ����i�����v��P,0]urQ�rF#4c>��;��6�������IBx�yi�f�>�(����� H� i���HЋ
����j�)ÿHkL�R �
1�P��"F���`����OE2�r����@
�����P~VC��E��!'@)�\9��K?*��:*�RE~�K�L�k�5Y�u{�{�[Z_�� i����������o�ߗ>�1�;�
��E0��r0k���-���Pߐ����F],�=;\�Ef_I�[8� � ���$��Ր0�1��=e16ydB��{\
{�j���� �c���ɶ)ّW:�����ܫ��_�ѨвB������\�@���zf��M�I���:�]������X��q|d7���8 %p���0�gn�we�3x�t?��r��KP��Rk�=l������3=�jK�[J���C��v���0��B��l�a�t�
*����z����������Ǐ"�% Q� �R!z'U�z'Eǫ̻�/� �	�uK�z}��9��ٶ� �J��x b��c �w
r�Ԡ�z�j�p�[��������uw ^��!$p �x��V�h'�F����6�j�L|�0Yj?���'�0�_ҫ����j{ ɼ� �ȇ}(7R�������]��s�p������� ���)�J���վ�����'��:dh�RYf!�,%]wr�	�0%�_���ˋ�4ݑ���9�p;� ���Iʕ��=�
�����`լi�`���	���� e0�p���a�ҷmm 	�CB��"1��R���vb��:R��'Az?'e'�M������R�������i�� [:��
� �M�I���[׎ �������}�D�
�@
���К�� \�u����<���=F�T�J� Ax���>`u��� �_Ht_#�>zG�4UU�xt��)��w܆|�1m�؊l�M&�sĠ�����>��P?�_�C �� �T���p�>����U	�A]���jx�=L\�!#��s����̖	�{9L8�0�ف½ק.���b~Ӊ������F+�	}�m���`�z
/�����0������ٕ���@&*��.������I�$Kĵ�5۵�p���5!�k�k/B_A w�?����0ٓ�Y*�Y����p+�
C��͍q?W���@>�
�������{�#+��X|��^B0��϶n�����c��д��LɣoADExQ�(l����!�}�!C��h�3'%1�c>��@pP����'���x�<�<ߥ����� ���Άi0��`���� ��u 	�ݚ��`���;0u� �bϬ|��Y�7�_�����7eўߑ"?�$��r�� |�>P�7�qx�ؠ� W[r�u a�1�V;�Sa�9�����,�r��N@	���4��
��c���^ ��	a���X� ?WBl�0S��C~�������=i��Qp@Á 8  ��!O�<�ҥ� F�lY �� B�,p e�!{@tg 8�,J�S�-��d��ꀕ-�tm�>mx< ;`�ٴi��o̅Kwn\�����L�}O��$�84���+�*��M��Qs(/A����c����i�ǯ��z���[���)oA���f_�Ls3-��o��<���*լV_P��S�ėކZ���B�΃6��
i��������7��s������'빫wR�F����TN�g��
mAi�@�!�YP�|Y�F�&{�!UN8@�(��=�z"+'ur�E�\�I��t��9Izn%��:ɦ�@B��ڀ� [I� ���d:���
 ;K�C��G�B*@�,`Ͻ���K��h��|�I�p��p�a'��0�0�|�A�vv�Ke��д�N�ǝ3ʇ�ST��G��R���p(��>����V��/��O�o\5��  �6 �ɧ��C	��[�%V�{	?����B@�����;�+|������8<�����k{jrȝ�b���y�Q�J�S�o�1h�4vG0�o���:v��p��f�G��(=�'�S̘�A,�(E����a���1�#�yM�z��W�TZ�7�^6�V��� B�A��=J��0��m�`O�����R h���ؗ����|��s ��=����I�D�@G�KD�~���O)c�B`O̞Z
0�p�騼�+���k�qɖ��1f�a&&b�����F��6�J:˂kA�G�l�� �k���D�c~�$��������*t���}��gO{���bg^0��r�9��~���f�l�X��Ll6���8�}�����o�:ơ����k#R����%� �i�1��x��(�!J0�.h� �@O�򢗸�E(D������#kل�q��c��j��	H@�\؁BP	M�!a[7ꡊG���ٶ� 
��{M2Uq��>�<�)I<�S�6����)<Kl���9룉!y��:`!�Q�w��-��@�Q�(�X��3�[�+M��+��t\fw�C,���l���;�щ��F�;H2��M8R��>t�zd��rxBp�P|}�M�اD�D�8+��z�	iqL�
��WE��	��A�q2��Z���p6���p�GБ��nn�M �#h �8`��P�lp�+&�:�� �8���L� 6h����xH&�����f bn(��/����Tn�&�2��Jd3���I��`%�#�HD���N*"�w |��ǡ
�`��0=భ������@�ٌ`����We�B�D=4�m<�4�P�+�	v��a׻֡���A���@h�m��Hh��Xܡ78A	���(�'s) ���"
Jpj��Kp�������d�@�$2q_�� Up��J�d�s��@��<�c����4!��UIU_$Î2�@��"�=<kሡ��T�S�@qH�8�Z'~hɵ�snf�T�p$#ep�L�C��B�^D��&��4���bg�H$��E&6�����+�E� X�C,�Q�
$ � �F�/@��k�]�0Tm�@	|Dz��T�I# �.ylb�`�):����+�]Ѓ݁V!vRE>�4�=�� ���C�y���}ݖ�`"1a�F��(�^�$�ʏE4:�tvs����dK��I8�7B��<�%xtat���[��AnԁX��=��� Fu�۠LΤ)�Z=���6ZAe��D���=�j�Eȏ�<j5�;�ASkb�;��>QX��)!!��R�M� e\��M�2�1��Mr%�@��(��8�Q�gD:���qm��^�x0��=��p1�A
�0�{�4��zh�4F�+�=9�X�N��7�oq�)=xA �w���7<

���3�tE�`ފ=��)�w
[��HTE����˪�s�G<�ɎRE�ن�yk����J����-�*a� ׈���ld�8DA��"��)�Sx�����w_��&@G8��rX�M�M�����h)I��d�3͂<	�:��Ҁ���0�*�B<K���Bަ�9��ٯn�+V'E#61Pv	�Ë�  ��슃xh��fy�n(Y>P�(H ��C<�a
lv��=��	��}���(;�ӌo�I��8�!�	A�eh�>Q�m���?()C��Irh���Pm|T�y [��36�U�L�r�
*�֊N�R���`��`# p�E�op�:8�0S0�.����k 2���Bh�����	�	ݩ�u�\H�q�m�v�'w��c�1Ӡ1�9�x�0x�"��z��ۤy�v؆Q �8��+��ES�`�����
R	��(۰���%��Wi��B��s�� ,�H-|`��A��׺�'ك�Ӏ@���j����x��{�ǂ��R�Sxq��mHi�v�D���`�f��s�oP� hK1��p Xh/x��81���8����3
,1���W��J�1��� �3'��( <(H^��{�	Dk�>  �w�� �����wXmسi*qˇC`�r�#Z�{Xv��u����x3~ ��\�U}8\p�O %�!BE~��C:vH>MI>(8Sj�����E"����ak��J���?�R,��c
,��0 ,�9X���X�B�>t ���C؇x��jy�x��v��?��0���Q��ү�0x�x�u�q`��zP�s�l�;k@�iH�S�"�H�S�Jۓ�K}�+��9�W@!(Ѐ��+UJJ)����k�3c9��� 0� 
��&�K����	�"^Z�T9	x�����9� �7 �uJ��A��h��B����B8І��/ �{�r)�8��i4vH@���p ��q��X�x����ŠW��؁GpA��Č���9qH�M(�E�IL�iL
2�����6�5��P)=ƨ@P�3��0"_�2�p�;@M|P���Ph�m��&	y<x 1Q�=c����@��&9(��$Y��	��xX�|�ALU�HS�Ѳ܇u�̘v �d@�ٖ�GXp0�	�������z�Dv������(�h�[\��/1��2^�YA"`˲ �5�����"�K)��7��L�8@�8 ��?ۃ-�&T ��<R�?�<	����l�X�!T�u��!ك��CAM��O!�(հA{���x�}�wdh4��0��r5�
���B�������
�26ɪ/���)�����	�0 �:�.؁)(�B����B�s�/2�@H��Dm���a �P�هQ��y�!�A�Reypu����R�ꚉ=�9�.hT� 0M�M�=a+����9?内]+�p�m%.��и�K�6mLW
F-���?4��q��D�	6i̳�5�b0��$H�{��X��88�y01�e8�k���,��y�A�{聉ʵ� ����(;��F$��;~�K2Y��Jw �G(��Йg�)���3v�|ЇXP����e��XHw �d��J�dЄ$�Ж"�i����I^�b�L1�n���Q[��]��Vp� <h0 ��)Ⱦ����Tb�t���<�6 �d\jy&H�<�J/Ο�(興���)	x���Jt1�ʚw����Q�k�pp���;�"pq�Ke��z��[ج�Ni��o8�z�;q�s��cpŌm1�e�V�I�� V* ����L9�3�	Y��)F2Ӎw��hX�T ��@0X��Ƃ �t��+�0	h��z���y ��A4���C��X�f1j�,0�9�����.�-结J���1�:x`�8��Cx�A�3Kq�q�p����w�qH��46>wh�S���R��ٲ(<]��[�\)��a�i-�X��爜�����
�H��=��E�q�^@�8&�BH-~��h�|��C`���0�k�:���DRKB:�_u�y����d���X�A�m�,X�:P����s���.u�8n΀��u�M�����'랐P��r)Ж��A�`����\;�6��W��
� 8�q�0,���zx��ɨ��09 �@[m�G{@K(��(���.�_vx�
8J\���Hs�9\w0�f����Rpy��"8��z�I�7��7��㾄8x�NA(`���E����	.*<�`�S`�a
�?����l�3�PC�pTH����o�j�h:X�n��k� �kq�j�Ё��Fm-|�^��OmeߒpP���u�itɝu��"�/�� ұ
iЄ8��m��RHdARU-�Z�����!9A�G|9L
�d�� ��=3cf��)�� �]�*2��ef> W�PcQ�d@>hM88;�1T���p���X�(�̈��Z�x�w@R��#*:�~C������y>X��MH���ܯ�HpP��������2@'���uɚ�s�x����f؆����$�:x�´����-�.�5}-{�^�2��
�5o�J�-�P3 �A�b��88TI<�̸�k�JmH�6�{�Q0D=U�><����D;��ѭ�,5K<�dXj��_]+x+�f�"(�&� �4h�)�{��N�Cކg�v���;U�;�;I�;@��S�2�����sTĂ?2#CV�ȔLW/���?�!"��j�� 5�`A� 8��(��h�|u-qs#�Ӂ@�8�b�����̽���.�s�|sq�j��vS��Ѧ�m�R�Ж���<������\eH�z�H}��|س|�A��f��O0�p����P�����x��h�%i�S��\46�*�q��K`�� ��B��dh׋^�ӆI���m�9��� ��F���/5;9�r�����/599�8��m�����S��P�0!htr�E��:�z@�[��AVWx�DxE��t3Qo��	�A��aܢ�����§�&��(��̪��,�seWXy P�/�W���B��w� +9 1 �x��rp{��g�-�p�$,�7	�/��7�;)5�Ozp��{d�R�z����lp��M蔇��0��f̐ꊥ�J�J|>7gi �F����~�/�/� �E�VO������!�O"G��H��.P���<�pX�ԃo=�h(��R���N\4
(Hx��� 
1 E�   ��o߶��ǎ�8�$ŉ�eGq��}c�Q�7����}sKY�M=�A�"����(�(��z����/�ӑ(I�d�/߾}����e�Suv�paB���V< ��\��@a��#B�(Qn݈/Zܨx1� ��(y�]Ē�`�����Wˁ�`�䡳�k���	�7t��Ջ��¶�V����;��P��ꮜɒΏS&&R�\��)e�\�2���#=iO�PP��3�Tẹ店�n��_>~��S��SY)�I4��[E�P[|��Vg!���PE~ѥXp�96���'YE9tbE��)�r�x�����j�!�7؀�Р).B/����;� t F&:�a#��4�)��:^�I��QG�t$t��AJ,���Mb��NG�|�JA��)h���|OT��ǏW����S\q��=]���P#�[��^h �@AgB�j%� h� ^�X�Vz8\���HQ�I��`�A�P��U�"@0/�xq�%�LJ�j`�.��� �Z8�xv��1�`c#,���&
gY�uGJMW�I/�S8��L�Q#S��fu8��"�.u
c�Q�'��W &��~G:���(�jU�QF��Y`s��D�����ݪ!�%V����� \��X��
@���Ë7�x�5 ��!w0�� "�S���[��G!�Ѱ;=�C,�+��	�Iv�d�U7��:)A']t�KS�7� NM*|��z議w�N=����~�wO�\飏S��	�+��@�"��l���]����
��h��?;$�D=�"p�Ʌ�\K���J)�a�]k@ �RD2I,��B�,���;�ЃN9�`��o�?��� �C�SF �)#�p�I�ƒ1}�K�(�Mj"e$�r�ȗ���B���D0�(e��������Єc��G��"��*
�p)��J(�L��J0�[����C� �de���jV���D�8a
S�B+V�@4���%hC^�o�8���T�F����=ۈF4��9&LjR�K�����L-�E*`q
X$#���Fb��j$-|ZX@��^H�#>q�IQ�qJ�FY�W�Cʐ���q�O�Aq(�D���();���r���	Z�_�Ɛ�i�DXZf�nV"�\v)�F\��0���D�:/LA�X�6��=�*.Ɉ��i%(&#��6�a�f|�$#a�:��o�#�`�U�r����P���Jjr�8��%p�nP�8���h
<�2)�li���4T��j�#�I]�v��ߩ�^p���a
�%/�(@�Oo�2��(zt�Y<�]I��?�ن�2�<�IK#`��" �(�p�YH���F�	=��V�&Z�.5�$1��^;2�4����ܧ�B2���^�P�� m���1���"���>���o��K)I>�񍪤R�8��^�������[ �+�
= ��.)֌�t��;���x7��`p��K���H��d![:����)L¬�8���6�Q�hÛK2�BAex���FJ�蒕�q:�2#�C�}���,�7x��G�!����h�� 4����7bq�d���Z�S�bB�Q%ė+	,6x�(�J�]nv�A�"(��j�i�1�.���^\�2тs x <� `�Z�x�B.!�VT�}�1�F�aE#Q@KĒ��b=N_�P�c��IK�a��(�N)�T��>%38�H�"����!�����qJ9��U������*v@(@D9ȃނ"0� f��x��3X�; 
 ���
v����p��^h� ��O����A
S����$�]�R�ϓV�����	BY�o}�oI���p�kF�9�!ptC��|��W�@'�Pp��E��4���+���F�T�"*dI�*��m�@e`�t�����}y	�P��
0j���Ĥ2�J�m�tc=M���g8�_����(���7��z�"b�j����8�G���uDg:�ɺ46A7M���	;>G�����|b��Dp�}��`3�a�pGH�џTJ�b���|����*��P�*��
E��h a�S`�(Q�Cz��2�A.H�;��G���V:�p�"�u+��5��T��h�S����M&�3��; 5S�&��p��ů�d���D#T��������NT		��B��6��>��;���A��G�B$~�S=6�J<�S���1�Q9ЊDJ8�^ł&�Ac���Q�V�� ���A��H��L��P�����m]�4F�`�9S�5����bpE������1��|�+��+��q��g�B2��+lB2<�&��)�TIń#�	C�D*�� �_	� �AlB-�����m:�B��8H��XX���T�8؉4�Dp�^�ԃ@�1i �̠����� @�4�/� ��W����^��^!U���<QzE��U	�����M���E���1hT*T����������Q�6 ��D�M�Dr��e��@�B"$X
h!
�@$����5$���5��=P�V(J�d��YJ ��7��(0�@z(��EĘ� 	��[4�ﴘ��˙>�� ���d��Pb��\�9�o�`JD�)�b��P�&$�&�$�2CM�LX�`�Ŀ��E�A�;8�6��6P�'��@�� �����#�lB��!;d`��p��?8�m�'`���
�T��Tg��0�,�oa廜� �)�EQ4E�'VU��4�^�e⇄��>�j�#h$����@��TG|D�ǿ��+��N�T2��&4C(�#ȁ+�� ���@�h,J��]X�$�8J= �>4�8p����&��#�C"4C�� �@�΃��C(c#�X
�[�J��U���EhtњE���0�TM	C����ܯ�
p1U��^<m�4͊ P���,�A������q��ф4�B�}�&�$��%t)�B"xB�i�ԁ����d
��UA#�A�ى��j�;؆*��6�j�$�j�.
,hD� �@�X ��e\"�A
D�a4�u�ty"rG�(�я[�#i��e���3�
X^Ċ���TD2�5�B2�B��1�Y�"J�B3l�)l$��&l�+<S$�%�%'
,�J�D`aݝl�&������@@AX$s�C9�C>�<�B=N,H<��8P��I��U|B)C*4��� ����H�4B#�����DD�����@J	����� �|'��\�

�
�剴�_���
-���j� �N5A@�(.�Ĵ��8�`��Dq��+h7$�2$)4���$�0H�iɩK�	C�D8$5(,���T0��F�L]�]�]�7lkL��S 
i�=�N97��6��� e�[��B� #4�"��� 	�5�� �I�p*��(��
����ls���Y�&�j� 4��(F#8B,�� ������3ȫ&R�yD�m�D|8��8p��J�� 	�	� ��,x<�B�։�xM=��H���>4E=p,4�eq÷ŉ&4Z�@(�ZDʄ�#@�#@���գp(dΜ3az�W�Xf�s�C��C��@*ӕ���%]  M<��l�
x,^2��6�D�0R}̩GHC�I��+��}�B.`.��8}�#��&4��i	 ��A
s��X��SLE�|��XZ7��S�Q�m�"(�΁�N	��įȕ�"��$���<U]�������T�˂ �qb�E��@g��J��ȎZ�q֊�H.F@@$��B�F�)�B|�*$C,��Qm���{�*���N�`���v$jv�B�=��L�p�"��#���]�S�
�C=0<��;�B3l�o���'lCvȧ�x���ƽ�Q��:D	<�*�%��"�����\d@S�6ח}Y�l`�8� ��m� ���p[!���@0��"�,���*��lBl�" �En�i��@��#��6��*�+����$\$xB�^8��2���n	Յ�=�A�Aȁ<��Ջ��;@�CB8h+���IC>�Bً��e3$�D���5���#�1%8f"������s	��.���3�l�I�,��^w�UC�^?~b�\+@� PAdpD��^��#xN�A#p&MX�$��5hB4h�B4�A+(CFA �B't(5D�N��J��7��9���:�%�74C3��'<�n?؃qG=PT$)u�����B*h,��� �`@�* �)����hzD�]�J����)QW�1>��4��]���L��\g�5��o�a8O!��\�,��A3���c,�N��&$��~i#�HH�z�K-��L�v��R3J��/����)��
��W��-�kv(�OpE=���E�����]=y�7��+��&��8�Z�R��R)o��X�L_��B�\�d�3V��򌵩�̵<��ur%_?@���%XH4�aKƼ����\����Z�o�Ô^��q ��mC#��;��+P\&�v��"�flf���Q�(ܢ����WP0r�+�\�2@�M� � 딊z@�&�.\�Dց"؎]� q2���X0 �ZU_8�/�em)0������V�o��S�f	��$�/��w#k��@p�1��1���lB�}�:���Ȁ�8t�F^1)�2)o�40�|j_�肪�&h��@2s]j�O�=����?4�'m�>l��;��:��*��+��Yi������\.$d�ށ�(�EoԜͰȯ�މ���� F�Yb�v�`�Hy���8g( ��*4	/�B-�Bx�7T�ʽ�L�@$�D2Hi2�A���>��~D"�5�B(HC�[��}�#(C]Røʔ���d��l�4 ��*�2��=����_�9��E�Ź�;�BJ	�7��|h<)��ƙ@	��T@]��@A����ZȆY�<�L2��o)��ӊCP�u7y�H�B��ȝJB@�L)8�M�-\�L�A#W��o P�&��{���M�G�$߰�+��+@&�%��F��2|�#��4�B��VB]���n,ԁ/s�h��o�'��847d��)�l�&�C2[X��K8@�X�AT3��#��n���D��#����	�s�|w��\���ʮ(p�XA���qAy�}B}���9�O9�B.�-\�zg0�# X�.t��O��),�$��A��8��OX�*Ɨ0������Q�D=<_�Y�_.#���"��#LA��U\]=^������D�z���/_>x��m�%.\�m�6�����%B`��	W,\����a#&4(8p�@�   ̜I�&� 
<8���
 ����6e���ӗ�J-Д*̦[�fU���&)�\��\9t�ȑ�ōW�KT�0I���d�T똪:q�$jdɕ2=��Q��[�u�%sLm����R9��-�7e⾱�(�4�m�V�iۧM�PIAGN#!4�Q�H�*e�\���.�>}��c���>~&���W�.e��m�����7\p,!�$W�c���
)V���f_�5��O� �`B��$q"� �`�&�"����� ����+�����` �ʪ���"
���dqЩg-t�!�^�ᅖK$�+	$�
P�b�c`q�G̠��O���f���r�q�!w��)K��W>��!n�i�ˈđF����FRTIŕ:\l���#9�C�;\�fx�Qx�Ihт�cR���WVIFeh��m�lF�M�(#��Q��U��e3v��Z0*p)DkZ��� ��d�	'���ZfQ��)�xv���D�)Vx	�������v��\:�RtB�X"�|�a7Frf�E�I�h�Ǽ�0��� UNq��:���65b�XPK-M�$���O�a��m��e�X QF���LF=W��D�c*�f�dA��2��G>%1�ET�|�A�9��N�m�;��d�1�Mĩ����$�1^(� 6Vy�M\�P0�(h�lm��)&z��� ����)&��\x�%^�bd
F.QE.9T�	�q��B�L��ܢ HAZFie�s��]w�]޶x�e{�� 	$z�7�  `�M�}%�c\�D`H�B�f"b����!4X\�EX�	�R��oNyĕF�y�p�>��M����c�G�$�6;egg+��Fnbi%|W�w8e��jJ���jH3�p$�u��N� �B��,Bb� &A�����&7����7��	T��,jq�(F��.�6C^��b�pB�R����?4�$G� �(
�`+v�z�㉜��=�Et�����*t$��.	L@^  ` 5i�T1���P�1���L|#X^iHC�o�F�1`�d����L��A@��c!�#�.�S�)7��)N������(g�v��xָ�:�!��-
!A�;؁%��&Tu��	^��WlM!��.�h ���O_�C��B@@� �)LA���*j��r�#F1�GZʁo0���/Z�V�����/�эo��!�xQ�ȏ~�Rt�8�-�B.���]�%|���:�T�P"@B��E!�aHSh��q$C��ml�K� ��
.qc��a�i�ԥW�	����p>ԡ>��)� ��m�ʋFdN�z�cQ>㇡"���� ���iT�X�B�B��Z��U|����v@�h�
�뭢	�f�0'��
Uz�"b9�
�H7��tt�`׻��X'�C���8G�V�sә�@�Bn�"���,��&<��
Vx(�UF��M�H�+��%A�_UF2�$e,o�2`d�\�,��S��w=X���K�p�	\�"�H�� �"<�e)\'*e:�9�t�;����J<$	���e�N��m 0���tP��)�t
����1�Bb�v1�`(I8V$Dk�Z�b���7ղ�wQ����4��̾X�+���Enx�9��\�Ra
JHB^~�/_ v*��������+���#2�P�I#e����X.56�BÐ�y�	e��ː�)6��:��w��&<����l �Ho{��^����;���_R�\P,��*Od
>��Z��dj� ��VS�ҫR�-&�bB��b.bK�� �����^1���Jh4� =�Z��Z�HTpB�5+��	*�\�� �"rؓ���:4�M��C,�>q���x.��OĢ��, ��F�O�P�j�q
qt�ʎ�RC�
M��|� �)Rq
X|��J�ϤjU�[<:��jT<�F�����np��$�@�v��5�!"�H�?}	��C��ȁ�-j������#.�c�u�+B�׿V�d��ώC����
�p�Y|�ٳh���e��=��	{��vCn��HD,�p�'��4��lU4�vuP�#H�
�J�1c���<ǣ4���`H�*ޮ��7͜��w�%��c��u�!���F��� �#6�����(�4pLP�z����` ��D$����b,^B�D������u���j9l9L�t�Z��͢g�@�*�,��,l1�Y��.�s��0��ؤ�m��2�W�a��ʠVfye��a8#���
��1�k2"����R�LV��apATA��� ~�| (�`���!9M�f0����"5�G���:J�a������c>2��Fjm���
`W����B+�b\��&�%�&��h�laonD��b���-hdŁ�P-�B��p���t$A��oz��������/|�_�Bxe ��1�$��`�4���BjJ����KH�K��XJbާK�LCD�a>��a�A��� ��N@|�4��n4E�~�Q�*!2n^)� �h����8 A^����!��h�z�(z�+�p���Ԫ�C F
%�4�B�D�ȯ��@���fo�����z�xa?Ko���BY.�&!���p�ơ��P���.�������>� pS�`p���/�.FA��Va����&#�p~�h������F�4!SH�P	w��2e5!��z���x�l�^pQª�ѽ�ѪB�r0̨'��`��J0P`>���)m� �ЙD�&�I'��[��V�b	���R!�)�ja�Rh��fotl�&a.&!٨ ���C���!�N�&2@���L(r���M6���#٠�A�z�� ���+�ay��T�y�!N��c�����d��i�H�8�D5�j��� �M��+�a�e9`� ��9�A;g0:�����/6O�F�MNj�@z@.hl��B �����	?���jb  ���Y��v�	 �/''	�@/�\R$̏��
ᚊ�X"�@�	cdF��-Hg.��X�	��Pǌ $l?@|�4�� �6�c�D�A�&�T�46�;H!���G�!�,C#x2�ʂ2>�K���'P�"1Np��|�}�1��C^@L!�g��ܡf��
��"��S`�����	���>,�� �� ��m����%
T�@د�Q.�V`,0�n��$!@0�A��X(!Y�!��	U2e�JG��o�� u,��<DDbb�!c�a��������A�`�NCف�4۔�w�(2 ��L� K�w,py�,|�D�����!N����A8<�alb!��s�z�M�tNQ�Kz�v�A�!R@�8�%�P���Pg�W^Wk�)L���0� U����`�Ё���b,f!A�����NVFhd!=L�F�	�hu �NTu"�'�"��-+��$�#N�,#�H!�`N!��ܪ�%�6�aT���4�4���8�_�G��H�R� �`� ���8��Tag�Cj%�&��8�P"){6�n�xdTbA^��\@�D�Ӑ�(�� �&Ռ�?j� n���Bb���~l�X(��$	� �.�	*tL�d=Ter���L�D'Vuh6@��'�r*0�&� �b�1xP����^�f���$�1΍;�5"��8Q
4��;@c2 b%by� Z�1�G��(5AA6�f�b^A^�^b,�%R@����:���}��͠��%`Q`b%���%��0#�0 �b��q���0	~n�`
�o�&�
ba-f�n"s�p�=�V��TȋrW��	��@�#�xB�Zt?p�&��ڮwJs8b��b�f a��X����8Q��j����|f�j���� iR�� �`f�\GA}M�A����F�B|���9&��uN�[��Nh���d�po��Sj��we���1W�i�4ٓ��.üPE�e�ȋ&��X�/k�$���	Jt�&g�f	� _�,_z�Em�/�B�fb :� ��`bA;vk���e`�>@�dx�!�'�yx��646������4��:�b�>� $�W��0���,G��]5�	���)m4�n�s���PϾ�;b4��G$b�AV@%�Ȓ4�`��m�  �z�����*�&�q�	�@W��US�f���H��ǜ�� S�~,��@
xY�1��Ad( �D� B����@|�Za$�`Ah�8�7{�8F{5|�f{��|�;P
���y��W����@��� 
̠ʶ��<�x[����`��C��g�L�!�;|�)M�#!^ ^��b	ˆ6ȁmEC l��H���BnD:�w[�@���V�
�^�� 
����� /��s��v���LK�S4� @Z�����j��"=a�@�~�B�2T`a�NO�L�l�䤾,�����M*=�o��!`����DF���O�x|"�\I�^�n����*D`�S�e�l���}@E¤5x���x�5�� 6Z��mr��-��?@$n@�$Ap��G�i�V 0�Tè@ǂ	4�P�
$ٯo ���	�\��%f�����f32�O<J�>s}O����@�HA��{X��[�(��=|ˈ}K8�nXiG����` V�l�G�� m�*4�dRJ*9��P )P���1P
�ba"`�M
�k�1��V�.�>D�`�ʣ�#���I�[x�u	$�.!� X�[!T�iǎno�A�z�!I�2��	��
�;�|Y�Y�I��[#��L8a��5a
� =w+�`��'e�;�a�vT�aAˊGˌ� �=:j�G�x�S�G̀)��� �`�����q0G�A�
S�4P���%�k7�W�xج�F,{>�r���ƃ�"'L]#�������q@X �^� �zbu�	Va�V�Dg!{�!�х�� �����Ǽȋ&��[�n��튙��/a8������
�MDn\�=��OlGTL������'��\A��@�O�}��%B � f� \�^ (���}+4��K�ʖ��kUj̤��G8pA~�V`tO:�lbNP�wE��W��+��,L�p���5�DK�Y����
f��	���<_Ȩ�y�' ��E���* V�P��N�ߌW72�H�A%�a�1�AE�F���l�����rJ`
��vm= ���V +~ J�ȇ|
�*V`� �(0 ��!�&dǮ�8���}���7i��)S��ӪX�V��"E(K�аR��
8P�@�7q0�@��@�� �(@� �PS@H1���L�:��d
��]�HɚB&_�H�$	Է �LPt@L�; ��H5���6i��IS���4=��HC#��N,Sۦ]����2X�R1N�43X�����)U�\�r�j���e�8��ҦK!�� RF�9���.e�4~�5� M�ą���w��ح��;j�Q{�JY�D(|�pA"T�x�AA3�4N �dPM8�T  @�t �< �QF�e����HD�ևI�p�[nA�VZhe��[<��w�4 S�_�8�_�(��|��+��M3�|�5�$���1�+�q��4���{%�K2Z�v
+rJ)�Xr�
)l��C^+�b�+��a�&�4b`�D Qr@Q�>�T$M*#�7rx����P9�<�<�#�;�T�E�)�+�� >���	!��j~��_0�S�3e�<5�A�=�O%�
��C�To���YQE�VZc%��[@@WRM�W 4e�7�u���Ա/u\��%ll����~�Xe���+>*�,	�Q�M�",����)k��(���H��@
,l�q 
�$R��3���E�@Dr a��Ĳ��ې���S�ǎ��:TO=i
<M��&���(�@(�PB�&�PB�*��5Z 8  �0 A��
����u�\N%	/�P7T<��
TL�`IQ"�P%p�<��R���eGh.�9FM2��R�2r�B	5�,$�t�K�7
7fgX*��b� L'��%M
$�@��L�Rr�&�@C#��p�M3���B�aF#�@
�H�J��|B�㽤�^�ϥC׃)��l'͚���	%&<��I�`R
(� !, ���R�Nl� �h'v�
Р()pSV
.�r�b
SA�Q~0�\k
m��Zd8ɤ'1q S�b��e��I,��ICH�p�'b��$)L=5<�&b��1fH\Z̖��XC6���tg�M�"̬�	��S(A�nt ��h��@�V V�����Ǻ� eX�g�Y=��3x�h���+6�H\�>�V`?��?1� n�1��7:�]b�� �pp;�����^�����$�P�I�[M`¶�r�Z.&t�` ��
]c�$������BIH�c#�Q�m�#�@T����!�����b���dH�4ɀjNq
W�bM���(4a�M`�F��&H�M����C�l"�`er���T  
I�H�64`+H������ǟUT���5 1�M@�+�_
^@��d?!�$%k�(�JЀhBS٤&���\���\b��E-f�ʯH!�w�������E)/�^Z�Ra��������1�1�	�74!3�B��+ �(Xld#�������X�����'N�G�b�u�RX�H�#!�|�Q+��d�@� d� )�EE�!����QE0C�X�{�я}C��p�����"~���*)P�	��P@ �,��EG9�'�
�RLx�ZB�ES��W� �AK�Gi ��fB_��(�B\�����H)Ta�XP��F8Z�Х�ɨ�2LQ�h�u�ے��Ԟ����P�*N�;U��l��	MP�я�A��B#BS�� B�� � �p�E�đ����}�(ig�ǝ��8$0��ߘ%
@	J�����.�Cjܮ�j&&.U�R����pK��`� �<�B_�J\�8�pJ)�A��_�X];�$l�5�m/�ʆU4C��8��_�|����&6�o*Q�g7S��R������+�A�c�h�#6��:hX�#���� �L��A&��M(ȣT��=RT�|��>H+)�}c�HDJ=�<�&�K�G^���j?������,3�
��RӍ�Z���:E7�w���'2-9��}#vRX�
(�"�IҎX+�M>���P�F�#0{='�h�]�J
:y]�sq�	�lB�S>���8qhD�xol�h��.!Q�a�c?�E�1l�_�!	�+B2G�L	X�$�J�`�I^��Fłɹ� \��y{`s�Nvt�+�YW^��IL�= �[L���4"�pXyn��jr�X�x���*u�Y�!�D<(]كH���rv��($F@���`eqh"\� [}B!�O�l!���Fv�.�-�xD�!F� �D�Nr�i �2	���&�	���Ey{���ǥ/�� n�xaJA�{�Z�&	=X3��m$b���D�A�$an�p��a0K�%�0�!N/0F9R @	�� ��
��	�P	�T2�8�!��O-�?��A#F� %�3����s������?�s�Q�"���pf0.�[,a%��"�H �[�c(gS}q�b��In�Iv�~iva�f��@_�n�59�R&6�r =�P��^>t���>b�M�M2`�!3����h����x&PS�[ ,�s
'�c&
�@2b#�	����\,4@S0��p���8h�8����G�۱	P�O+#�!I��IWG+'$a��+浌�ǌ�=Qo��@+dn
P,�V~�^I��t�A�oи#�#��#�`9��p������y�Ј2s�q������p4`�tY���/S8O� �N�`�b s��N�@~!a? C�G� �E@t�tDW��wQ�'ʰaBP��4�*�a���H�H�5:QI��BWh@8�#AvO}@҇o�Bvx�nl(���KTu.���$1M+*ّۡ_�N��
��V�'3\�0J��[�	>�#��$F5���V �q�PN���� ��	�`���G�	��|=W7X�$��@�Z�{��e �������H�S�%I5Y+2IY� !�8
r<��iчB}K!���3r��f٘�����+���I�6h����I��q̠�x�����i�(Ҡ
@ $ N��z��
�q�@'��
��85�a����ǣ NP� Q�&�
�X�;x�?7Q{44��`jc�<* ���)���`�Im���s@/�$���Y�o�kX�m��+b^�(a�e<�����L2_~hl��5:k� ���ȏFr$
�g����
YT )�D� ���
�@
��&f�/70 ��pBD
|9���s~��֟�w���s3�G��3<S)͐c >�4�[su���Y���٨n�T��z1@���t}a�8& 7! aw�@y^���;���PB�6��q�3'�p���%��P���s
T�Y� Nr a@	
(Nɀ9��z�#
pr��`�`�Np)z�J�ܰ�5X���k�i>Ah�0�PS��B+�d���&�L-E�cĢ9�(n�]s<�ZS��g8�ߕ+����mD�y;�$��(��$�#V�i����`"0�qC
�PԀ$z������8+`/`�H�ve51�^|�E
@�G��J� �� �j���Z�!V�c�6�/@5Ox��e(�E��u"ntS���R�(�����7#�H��"�9�P+04����a�D*$�0D��P�s���8�����+
�M���>� �P�޴԰%�� >`X�CN�����`>���� � t&)�@ǃ�Q@����
u��4�?&�@&+�K�(�U� ��򶗧I���c#[�<[8.4��&�BѶ�sS,CPlp	4t�I����g���$�������~����� ��2�e 
���w��8
�R !`X�i�d� S@Z�pE����;h��Z�P�Q���C(Q{|��T�Hv��h�	��\ke��u/:���~��8>�KZ�o�g��R���s ;�-@N	�0�A_�߰�0�@�ñ�B2��	+����
u+�:_� �
`�'͹�`�F�
�rNm�D� `�> �X��l) 	�Ń��ۀ�Б��)	h�� �Q35�Bu�!+�;���8�h�T���!�ɔr�+d��E�N&������ +ZU p
e�2P4�����#���>�_���J_as��`�Vl�2�=���n�x��hI�( �PLke�N�P
4���<Q�P�[N���`�`�0?��{@�)Ax
)P#q|Tc��/�tԔis�g�RүoV�p~vvs�\k˼�h.�HU|���\@h`i0�@	3t$ٱ���w�	��[	�
$�q�����!5� �`��`�0x�Yĥ )0��1�� <砊|�8�H�)�L��d�`Q�Q�c�E04������ ��u����+MY�g��&z���toڧ��̌�˶�8���%����"� �`
�p
��hP2 x��(��^)&7�=���� !���G�\F�B	�����J�	E<I����3a�4�[� �I�֭���s�L�5{= > 5"��D&+Ph��U���=�i#aqx�gh��ȶ��Ge丽E)���Fe��A�����N^0��
G�/5�
�Gh��s�p�����	�� ۀϵ�|� ���c�� �Rg��9r ! �`�Uɀ��d5���t�s)�qh'�= �(p�;u(��-�H59+h�d�����I'�+��os�~���W(��U�S�iEܼ#=��jIN��
�AM�`��	��	ӤӋ�b� )�@�	��N�48��)�� �)[]x��%)���TeX (��)�sE�ɨ�!� ��Y;��  ���{Q�[����f�DZ�B�-� Uh�L��c{ ��1�mɕ�(:�ۈ���/�Q1������c���
�@%t0$E��;K4��f���3�Pm����Y�}X�N�21^��6Z͙�+�G��50 .!h�Dlʠ��  x�8��%@'Q��)ok��?�d5�<C-��hg2��C���k��F@��۝��1���#����1t�`��p)�>Z�pET@c{�
Ȩ�0Xn,����5�='pD�;�Z�;�d�e���k�`ɹ()�p3qz;�/p�Χ@�j��z�0|����v�h��t�$�����a���{<QǣE�MX��� Va�$���@V��	�a=����"Ԯ.��u��Ͻ&lr@v0� �`>w I�d�hn�e� ^�A4�@��D���,P�`Z}��!�3��� ��$@&��/��8+Q��?� �ʌh+��
O������Z� 1�@�$h��� ����p�Q{IZ#q�Qk��5v%K«��[,p�ġǮ��z5m�"/~�����/\	E5��W
4,dx�� � 
D��	L�pÔ�S��m�汣���h���Դp��嫇W�̚1c������)^�0!BD
"��� rc�N�P����� Z��G7D��C���.]��T�	N�Z�a��Y4(p  ����N��d��%���[2�%�[�J\�d��1�	;�(QʫG��Z����ד��Nk�SZ�)�طq[�E�X�q�9∃�QNQ�T\�E��*�餚�	%�P�K)��cǯp�q�V��Lx�E��쁢k*31㌪�Fk�4ڠ"ȶ�t��*�J+誆psI�Zs��&Q.4�> V�	���I��e�YG�o&2�\B\Ǔf6Q������l�/MK��K�A'PI���შ�:h��4�&XbIfARN!��7q��d"�F
KJ�//��<erG��<���JTLL��j����4K����Lh����"�|C�Q#�65=+�(�|8��+��M4Q�M�9�p�Q�mdX������#p�9�\h³��� � ���P�Ih���(`-Ȍ�d���X0��R�ԕ���f�3-�&n���z\N��U�DI�:��
�`A2htJ���j��r����^�4f�R4⊫��ϴ֚Q֔U�8��j.�X���\v�<{�f�U\��h�[Ƙ�If�m���}�A�;�9��ƚ�q� 0�a~��k0y!���9a�F@ep�UNqœMPwE�X\���:�)%v~p��d �d�DʘQ�Q%����Q%qb��ڦz���H������Zдn4$I�o�f=�:4���!���j ��s�7ݖ�Mك��ƚ�t�&`A�d�B��,�v��"��4N�	q(�&��	��Q�( h�+Zc�����_�X�5R�>����6��}���!�t�
H��?PW-���DHC��,� �"Hc
�P�\ax<�<�ξ���E�0H�c�A�F<�h��5�Q4Tɚg������N�l�Gg�&��� �����B�0�*�SPC�PF�����2�X��~u/�0�a,S��!/&�ǝ�����w�q�cc�(��6A
.&�xA+V�
"�b
!�F��:h"��1`i3v��/�k`GJD�� 4EIfܨ���Q!QCkИ$�L%HVqggvc?��ƝPJM>�H��F���N�aHc��6�񥴬�҄4��F$�,$��M�c�N����,�ƈ�zxG=��z�?�GJ�olc���Z��DP�>`�a~z��D��!�d<"]�Q8�1��dDR�<*DqldsV���֨���{qt�����1�֪��NӽϘUI儍�v�������.�����G3��.qPc��^��u\:�8�yT�q���5�1
>�!���	K��z�����4.�KL�̀D|0����RЈ���X�&,A���J-�jELR!�Jת�)\0�5�U}qE# �?�0����#9�����X�ʵ)��&!òM���9��؈��B	il�� �&,��|�#��;��p�&��P�X�R���=-�	.\V�sp��%�����4��ȡ����Rt�P�)V�
i�Tvi�]�cRq��/n�r9"�s�#r��=�����/!�4��x�$/�T�/X�߰����e���Gݠ�iim�)�1����#���+ ѩ�`:�<�UX���S�4zB<q�eN
^��t�"�pD"�@
U��h�#� �ot�C��wpI�Li�R�F���#�Hy�]L�7�ymc(��B�I �3^fvC��lٜ�J�	���>��� ����<��F�A���Hxd�-Ʉ`��i�ʠ�8�!;}�[@�HD`,�n|�C��8.AR�B��>�J�dNu8f�Oa���4�<��æ۠��T�	U|�������}���i�i7I�Dxȣjc�A���w�m�C��F��!��k��T�x*	~�Ac^������$��jHC���4�D�cyҸ8q�Oq���hkm�Ѥx���dO�2��1U1���i(�)��E�pܔ��ťN�LR�a�� �&H��������;�Z��
�p.��	\09D�PE�n�n.�7��no�������J*�xߧn��S�Y#����j�7�y�$@扇��� t I�Dl-���).b��j�{��)��b�K�::��\�⨬V���A�S��GxMH@��X�E@.q�\��Z�v�E��V��e�$K��X���`�c��z�n�zZ�=�s�#u#&�|��0>r�/�����(�~*$�q��][�ox�oh�UH�o8�MXN��t@4��a؄z��U�U
�Ck C��(x�0�CW��6��#���R��O��M��S�J�3��"PG��D��Ҿ)�FX�g�W�5=��Xخ��
M��r�/�y��$	�/�+V�>6�'G�⨳di����@��`;X��F�=��]�X�8(l �sQ�]{��(X�<a[�i�F��~)��[�U��k�]�XPX���"�"("�U09��X��*?U����j��� 9��px9x
��o��g�:5��z�/�¹"�|
���'�1�d�������zS�d�rQ�N��S8L؆�[�XB'=W�MPj׫	t�N��N�	NR×QC�p�A��8q8^��V@�F��"X��@�)@��F0ep�I���:�	)���x0�چfP�0+�ޓ>6�ic3��+:bk�H/J����P,���6rr �)ѫP�Mpj`�M(�	;�eP�(�Be�%_R��1�S0�YJcЄ
Y�#b��)�<"��*���T��P�@Q�n�nX�U�� p"v�^�^"F�F �D��2(�M����9<9�#FuQ�X��c�#C����,3��4ȫ��������ŧ\쑮�>���tHo���"���G�O��o`��
�Wx�V`ix�K�ѣ������ӎm��������8���4x@~��S��k�z�@��]p�|H�V�t`U��:��u@�mp�]J��U�I�d9�^x�VPJ���mX�m��Z�@9؆Xh��\�S0Q gE�p'P,ł0 ��Z��RĖ� ���>�ҫ�O1T�SmA��MHeX�˰$X4W(�Uho9���&T�� 
s6X�d��Q�AwhÜ��z =hL�rX�]����z�)G��X�X��X`��)�pp�ɫ���]0Z���Q��S`eP�A�Fh�J��d`H AqH��mP��n�\�	�X�z���!7�	$�4'�)1��,�7 �Z>R��P�H!a�	#P	��S99��eЄc��:`ɕ\ j�Gq��:@�u	�V*�s �z�,i`�GV�J��,Ņ�e�����iп,
�PI�Rnp�K�\��U��`�DP�"p�q�E�;�R(�VHE��"�)z��B0�0m%�6�+5�S+����@}���a���7�h�3��:|�W:�E�fPO0DM�
��FX�,�H��(d=�VP@@�XHY�@��PɍMU8�R��z`�-��Tp@�x�ک'c2)+]n����)X���I�-��"6��|�"�)P�"H�G�9�9�M4�T{�Ne��_:�K�Kj�O"��K�sZ���"u{+Q��	[���+����>w�M�9P�2 X0����dP���^p�A��D8H�U |s$�h�m��yÚ���i�m`2 ���4������,o�$���KP�qd�E`�D��"�iXRP�MP�24�����9��kb��8��9�eX�2�
�{/��
9K�"�&q��`�e	Lz� ��8�/�E��\�i�+>���hU�e �d`�{{mEcfR�Jx��A�s�7�;�W<�X���:	����M�ē%U<�rX^H���)��D��r�M�Y�u�X�U�/�sT�ؘKQd��R�0\�R{��ƓSzF@�Y!W��:h�/\~\����'�@��$f\��5'}���:�e�O�
��H�f؆W�U6P�D�0�&sg��dP��:��8");͚�P��*�PJ)��s�th�䆝��F�U��)@�v�e��V`��Rp�<9n@��6ؓ�UhW���Q�UFi��ph�U��H��"�I,2s&R �)����y�'�i:��1%�m���tH�`#�Ⱥi���*�0��
�r�hTj��e� ZG�38�F�U8(X�E�W�{m��@ iUv�=�(�G�ׂn؅���V�>H�F��,�K�R�������P����I��G\�U`��V��T�L&�R��F�)H�][K-�ZS5��A���J�\+��ub6/k�����রA���f�7
�%"�LуGMfX�N�� ��T�G��o�.Є8`e�3�U舩��VA�(���Q�U`^�`i�W��m��3?U8�� �`�n#چn膛Z��.e��V�\H 9����R��QH� ��"�=EhE�lHh�S؅d��]k�9�)� �}����&�h��\��a�>�A^���'g+�� «�m�k��ބG�76H�f����Vf��]SU�W`_EH�)��>�i2�S؂�톝#m2F�&�o�H�X؄�i(	AnͩZ2�ԕ�Q��fn��#�@E�`��Q�3�V aB,?La�!F.��Ǧ7R�ݫK��\vA+Q��;>�M���O �6߸<�4j��X�3�00s�n^��f��]�y��,j`��q�-Pm嫂��z_R����7�K o�F�D�^�Z� �ln��K�8x�UP���X�fFpj�>ܺ�XX�"���D��a�"mM������O ��Qt��/>��+�=!�����bσ(��\RTs��GH3�j
�a0� N�����Wx�:`��ԥ)��T�׹c��{�՛�"�"cEm�"؆ ؆)�����I�QHA`D��q@x#KĜ%m����s���Wp�-�� uO\�9�x�ȠvH)9+V��Ac��2�F� �	P�M��LJH�C�����r�p	��^��"�%�E��s��X�p��p��1���Rq��I���==�sk���~F0�Z��f�b��-�V\x�5�Z�ʆ�(��x�FPI`�U�����K q9h&z���0�6�>���� '.㚬������4�x"W�^7��6!��5�������2e�6Qr�,���F�\y:�FY�mʾ}�F��OEVm���:�N��#g��p�'.�6�߼��V�g8e�]�Dj�&I���j5�ۢZߤ�QuJ�4R��E��1��X�vm��a�X�j�jE���)l��]q��F۶ɩ%��6E����vI��XAD>t�+W��(   �˘3g��3h�}@�h�L0z���Q���4��W����7�֘�s�:����x<�R�:��i2K�4aĔE�-��i�+�ǉ)RD6=�$�L�d�ɖ���J/q޾����*��s�%�8��"�\��0��%�ĢJ"�0��4����7�ܴ�8��$�6�hċ2���+B4!���
,��(����$r�*{=��$�p��#¸",���D�H�n�iZ�����i��Zi�0�k��@lɑ6lh���f��	Zkr^��q��y�2�<�lb	��Be2�G����ԥB�4�P3�A�ݡ�4��_7>�#������*���x��B�4����.ԈcMM��D75��XN{���'e��.���
.�@�
$��B�%�Ĳ�b�8�7>�	$Ҩ.�lG��MZO�RЫ�4��j��֚l����	$\��u��k��v%q�a&�p
�� k� aIl����7�<r�&�,	&����(B�*rĢ�%	m�A�4�+9����^4��&qd+G���
Q�(c�))�K�^{A��8���M��.�KԗP��R�<���L�J�0��
��0�ց�+�����l��L��u�ۀ�Kc�@�&>���q���;��
כ�ry���w��c��z�K���I昨��&%�ti2Ā'�+�*�	1e�R�x��2�3�2�1�%��R�4D���(7F�5ޘ�`馫��{q���	�D7�P��8P63)PH�C-B�҈%E��m#P\�$�`�T��P�d`q
\��R�K�l�HxC����4"8�Lh��f:��l��x�R�RC�5m,Lb"]�` ������Թ�)r�F0R7eԈi`�:���Ĩ�1�q�m$c*�H�+��D�B"| �+�H(���5F���%|���:"^�b|�b�.�U����$V�"�\��"�A�A%#�h,Zq�]�J2R�)ba��u�0�+R.�q@Bm�h�7P��-
���0�^�M���P�/ӜF5��
OG��e9X�����o(�@$��I
G4b�E,Nvi0��(; &�-FĢixBd��I��؄�"Mh]�L��]H���%� '�
��`�7��9���H�%6E�"jX�r�%-r�*Ru-U�TJ!].��M!? �/R�n|"B�EԀ�­�*P��6f/Yr�qa*M�����!uL@�P���8�	�U�������&�S<�f��4��elbv(K�"U�R4�@�+�P<iP��ߐO(6q�Q��1b)��ֵ�!.U���+.Q�Z����#�v	��Ő� H�a�і�e%j�����������)$B�hH"�����K����dH��o/�D�ܦ��Kd"�Pk3}����ۘ�0cL�x	69l��@h'��CՍ �z
'$����N;�lG>�P�(���6���+��E�x�805\q�=���x�+6�U
��0.C	\�`NP�R��
El���H��z5}�xS�FX��h��I��E,.FԎ� �
(Xb���*P������FF�1�m��i�O�4����b���+�*9�_B���P]ԬF 8$�q�Y]�`M ӝ۬��5�LF�M@  ; GIF89aW� �  (!#0!"+'%211B+O8n"D-1Q*8k:D:Cy-@J*FW:CI:FV;UG;UW/Gt=a\=bvE;5n92C<LB=fLF:gO;ya<GGGFIVJTHHVXUJESKUWUIVWVEJeCJvHXeFXwTKfPLtUZeU[tTeXKggJgvLqiMstVhhXiuWtjYvvjXMd\gohVghfejvhuhfwxujetktwwhvyv?�&=�J�V�a�*L�3Z�9d�7e�:l�GY�C\�Sn�Nn�ck�ck�fy�fy�tj�u{�t|�fz�Vw�d{�\�xg�Zi�ig�yl�zw�lx�xt�nx�y{�v\��X��i��j��k��l��v��v��w��x��i��h��l��i��u��u��y��x��{��|��Y��_��n��d��w��x��{��|��71�>=�W>�g<�[N�_h�qU�jd�mt�xf�zu�le�yf�{u�|[�yj�{��}��|���Y��u��y��X��p��]��xÛtŦWȦy�����������������������������������������������������������������������������������������������������������������������������������������������̍�ϋ�⩺ƣ��������ĵ��ӓ���г���޽��ƚ�ѱ�ƺ�⻓㼡�����ʶ�Ř�ϭ��������������������������������������������������������������������         !�   � !�MBPW�������ؾظ����ٸٸ��ٷ���٬ٸ٫���������ڸڷ��ڥڷ�������ڥ����ۥ���XXXWWXWWWQWWE?EE�E���E??ۤ�ۤ�ۤܤD>�DDD���DD>D>D>D>DD>>>>>=�==����������ۓ�ܓ���ܒ�ܒ���������2��222,2,22,22122112,������2�2�� � ���������,,+�++1�++��  ����������������� ��� ,    W�  � ����'U-�E�9X���x��S�`-[�lQ|U���<���/]�r�jhKW*Q�D�z%JT*Y�؈��G��.j����̣�f�|��S6�ޤ2J�7h������7_�RɊ�(7p������8j�}[Ɖ'@�8!b��
F�,!�#N �Q��c~�Rݑ,玝:��܁cN�g��8�c��[w���lGN,RH�"5I
�,N����Bʋ�/�L��E��I�N)Ou���V�Z�u�X�e��E;�;t����s7ޝ;y��ͫg�߾��˟O������7�HQ��Zy�b�C�,�H��� ��-��`��\�G�bDMm��h���>�P�	0�@�0� ���`DLI��vDE�(5��h`��/��l��?�C�_�eC[n�EWb&dЁl�%\� B~�)�.̇��QGp�!'��f����١
����U�bG+��RES8aA�`
�Z`�ID��|t�����G|8��3|ԱE��E�YĚśwԡ�3y:C�3���g*���*�E�/Ѵ��t�Y�L3̈́s6�x�:�c�y�SϷ��3�����Cv�1`��d�A9�+]�`�-eXaC�<8R.�pd
C��5� >� �	zu1>@�<*�(��FSj`�U?�"YT��$\�դ�jE��4�5�0�]]���t �^"����|Bc���&��'wj�f��̡
��⌞x�r
PTAGF��V��L���̀��:������i�����)�|��w�~������4�3��G��3�,���|Jj��:S���������RG����r�=��@�>d�A�ې�RSH!��;K�O�E���?�p�(����;܁�ԋ`�n��L�q�rx���E�Q#�VeEY?p���T��Z4�0C�1�W]"p�ȥ-�g>� 	�9����0�*
ځj8ғT�U<�Oac����;P�7(����� R�ʔ�7��S��[��6���p��p�#�g�T��Ci��v���[qȏ�,� �HC�,N�!Y���Uo_��E�l�T��C�	LP4��ӛ�(��b�I.d���Fn�"��H5d%b���~�*��[[rF��ؠ5����-�i\��%���=�Y��(x�8�a����dU�|����
ɩ�����	0���)AX�JA�J�������7t�8��)�1.��{\:W��Mq����N�BQ�U��]����<�"c\	�@� y/���-�U��u�'00�<�q���������IƘ�6`�x:�QS֐$4(�, ��R.Р-l�AZ��Ӹ�  �^G�������(�<��j�����:�����5��;С28J�/Y��*H��Z�H�.(� ��m����{g�ٷ{>qp�L4�89uB.��C��q��}U`�ٌ�u/���א.^�vx����F]�y`hM��0  ��Ew ��;�AH0=�|t�x	S��٣�({NId�N�����h����򅗹̺+��d��@�#��(;�ף֣#�5af3���Ц:3���S�4��g��k�TE����0k5LbB��	XJ	ZЂ����z*��[�&O>��q�x�����.Q�{-'��a�u��G2xנ�a�c;�c�@($���j�����&n�@ �hI���AeKd�X��>�G�������C/�8�jX�:tF>D@W� 3���~�c�No`��@0 ��3�/���X:�x����8<U��ee�F�<���zb�N��A��-l�^HPjJ
�Q ^�
Ew�
��@\��X�3�{M��H�����E�p_���6��(�����u�%
e��+�P\t�$��j`��e�Kȁ���J`"ﶲ%*�!䶼b�B��7�(NQ�S���b���Ju���/Pa,��Zn � 5a�Kwk G�P 1�y��G���a�d.�ԙ���ϸ���k5���OY�
���� %(A\C��4 �|�!9s�X���uu�5v׹.���\��ق�������ov僌u�m��������wl/T���� �������>��
�eL>�F��-����� e�B���W`���s��\J�%,B����7������P^<����m��L���
��h~D�VYc�U�0��B�r�C���=UM3`��i�Z��P�::�7��4�	H��:�C�m]q14���w⺇��k�������h?{<C5�'�j7c"q;q;wam��P#�m��c�@0;���4]�nSFe����x��	��ݐ	��GS1�%
��=��=L!
�"�TIh�o�Fd�?pQB%T3?�gn!* ~THT*P:�0�3�Wr�0��WP3i��w�i8bT�
/Gs��'D+0�T'��,�/�B�2k=DaĆ*�S����)��*��)S4E)�W���)�u��MO��e'8d�v��P�cP,q!<³/
H<���p�Hdߦ��b�x�@��Q��0e��n��	�	��	�	%X^_�!��y�xP���HL$��>@"�c_`m�v�8P?�W?�3G8L�3h �3���*�$��{2'5YPq�&��_rP��KZ�� Y@�`4�|�QLi *�jA�!Q'�-��Q�D~Caf8^'b��C�Ǉ�z�7�%E)F���)���T�����#�k7$v�m�/��F:	w?v2��/y��ޖ� �`o�5@y����� 	��	��x�	����`	U4,"��o��n p�Ԍo`�@�H��Y��2=�nTiwi35 �~���g���{O��c5��Oe������QVUP��i�rAv�6�VA�jA�(�/`7�Sa����t�wl�DX��D��*���S��FN�X4	�<Tv�8�`rFn�/�Bd�##1?����/��� ��0vQJN��G���`x�����Y)\�Q�En (UyF�RL��V!�o�/�\ �2�� �z?�zM��hL{�{
Z{:��KTX9�TP�q�_���Lt+�Ri�#�&�Tj6pjAW��,`��2C4�C4y}�a&FD�Yb�%b�7X$�9|EW�MY�v74C��t~C�9D����<��; �cq� ��0Z��F� !��>��91��y �5=���T��遹�	��	�@�"@&�7�H�w��QA+$jg!�\ R�K�$\`�J?1��X�6��Wr�	j3�q���j��i���6N ��*��5w�a�
!*���9t`�r�s>�� M-@�[k|S�L�*b����G9�u�l��tO��(�7Y0C���v��)a7���mW<i!�&w��A�(1<c�y!��
W�& �L�Q[=�T���	����9�\	1t�0;by:p�$NA2ʕ�?p�?�$����]6C��r��6c5��]1���T0Ǡ8~0�P0ec+V�T�*s��
��P@jy�rp P'fuB=��! M) �[���M�Ĥߤ�=�t�8���sX�Ul��u�RW�t}�I�C�F��4E�$a9�7���� !�c��~Kdg����m����Fy�J���މ�@� ���	��x�ȋ{�QH�Q7ǃY4r<hF#a)$j�>.E�\ z���H%i!80k�gc%~�~��#�q�h3R��P�S���Q2���5V�s����D�T�U�Kip6�
��	}�V�i��µ�����)7��*i�!vl�Z�$VDd+�c�D�D�K$8*6���۴W[��26dãwA&�D����mjTj�%���G' �Z"�e�h�����I��x����G;0"`�ܓ
��yq6$�$���:����73�X���9s���3�G���A���Q0��5pss��
feT�d�1%t6�0;Ra���<W.�Y�)�r��dW�[O ص6�8Ob�!�8�FDbDC��,a&��;CZ��Q�u� EL���I��Iw�!_JyЀ�L��������b0I�@L)e����Ke���0�Y�	��x�������eb0\��hID�H��e�8�$w�{F�=���g��3I���u:��ӼY��ҫ8\#��s�q
P�*�d<fȄ��#o@� 
��p���vli�78J�)�6~�� �O�8��N��9�Cx�b��ȏ(�~�Mu@_�ܤ[�]�F�CPhT���������jѠ��	'RJ��Q����n��n���Z�����9��L1n�1S���Ea�?��d�Q�2�sy�Im!��gM���|��3��X9��'�XS|�ys��'����W����Sϩ��4� 
���(���)QP���W��M�"s�i�Ss
�@9�y��u�r��G����9Y���}�Y����� �FcĮ��F����B�)�޶�L���@�3�i	�%���ӄ0�y��`e��Qcb�U_ ���#!Cy�2kY�58��%_ ?���<q7�g��?���é�hG�͏��tkt��A9*�K��A0�+U`+�c��S��S��6�
�k
A �)C���NT���~4$A�ڥ��(�"�M���'�DS���Hɋ,ڟ�O�=�~��j'dfԉC�m�/�p��rw
e,<�`0�1@` L)\	=Q�n��n떹v���`e��x��	��[�G[>���w1I-���`��
'$f%KB%(��'U�32�gzy������`��Y+�s��s�� =�R�|]pR�� �
�p�>')/CY Єc8"9X��|5�t'i��+��`�B+������8�'��&u�)u/v�磰mv��M��ڌ����;£5P�����ܮP���	b0<0Q�dG��x��&���S�n�6�$�	c�xݠ�hnG(R>PRb
'����FS0%$o���SI�q=��I�'�����5b�'2�r@_l���d��
R�L&�f���`�@��j������`��ϼ�7����XK�)p��)j;�ViUMjC�)G�b['���\8��t��7U�C���d�K���XZ� � a��fZLPщ�&�FL>0C�0�4H�V�x�������[p�({Je�������[�e|�ea���H�HEa"�_��t�8��m�2d0���1�g��3o���/�#�|sr''b����6Y��>6��%T��/�
��� ��/�;o)1�����:���)q��ߵ�K�VҟjJC4i���t�8��~p�~�'8̺M�K�T8,ۧ� a�V�Z�pą�.[�x�/[�*�2&j�9: y�ÎR�,e����"D����!"D��mʔ	$�4Ov��C'��p���ҥI��q�OT4SӠ���Z�p��Ǘ�_qܨ��m���Q퍶5b�P�6��*����g~�<�#'Ν;���ᣊ�[sT=Su
��l��iC'U�S��N�TTV�H]	bC��CL�mAw�3||����hϠ9�󬏳g[��f0�A	�co�΀��	����޷����y^��?��tM�5h�|o/^�=���yϷ��I���<P [�%���С�Rh"[x�%Q>�J)�,Ƥ�RBĒw@�B�d�nn�I�i�D�^v� �@8��j��6�p�$��
�2�ZC)��
�/���,��¡˻`����`�K���!�!��ҌA����g����C:Ҁ�g�xf1�s�U2��j����Niô7R-�TZcE� t H�a�@�m�3�>��8.9V���	n�.6�@` W	� ;S�O==� 6���p=b��F>��?��O�3Nu�X	���b衃�%�\H���E![2��MI$���$�G�%Lz�)��ɩ`a6���^�1�L0�"�|�Zި�<\I�*�`#�+�"��p�ᬳ�� L0�+��lk�5%��L0E��@��{f4;���1��#�g� �2:^��;�`�Q�D)M�\j�4Q@9ŊX(��@8@;\u�`	��@��W�,��,�@AQ�v�.m���]C`��$�6?d���l�}F>Ƶ��r>��#
k{�6����Q���B]
B}�mɃ��xiw�sj0�Zn�E�b�E��z���y��� �1�n�!8Ǆ��ax$a(�M 3*Q0�Cc%��
5��J7�x��+������`���a�h�˚h �-d���x|��1�-R�5��0�30�Uܡ
/P��T�*���ԥ"
׀=PAmr��9 W�� �����)���P((j:��@�҆+]�;�i@���de8��������x�/' ��	����8���!�`� ���x����]�B�P.Ba���#>� ,ыa�%��D���"D̤ΛN<1�q��㸞G��@�0@Ò��$I�x��R����f���p���e,? �Yb��21��.wQ\hf��	=��`;Pa5s`N�P([4-t�B&0	H`PxC�D��ST*RVsa*� ���`�t����!�7�іp�㛻Y 7����ة\)T� a@p�S).Y{��y��e�<�1����6�=�ڃ��3��[A�D BGہ�v1��nQ:ܽ���	�D�`F/i	LD�:i�Q�Kz"a��Q9�Qq�a�$%	D��,)*�Ԙ\�W$e�L����`�,�B����%
T �0���23��ef0X�ӊ�p��|�a�S�m.mP΀�A�*T�GZ��)
:�k����d�Pg �Uw��*��-=��l'���@p ��v�6\%jG�J���V5vQ��Ⱆh�����I��Q=���8PD�
��:/C � �<�]���'8Ի�U$� �2J���H�(DOrLx��d9>ٍG� ��3����Y�bMژ��P�+u8+!������u�3@�\X�,goa3�r�����gL�t��4�AE��<P���wb����k��$$���C�ْ���D!W\Uzz�8�@.(}�Đ�M�I,��F�$�>��h�(R��t��v���A�Tl0����z�.�Z����N��ck!S�B8�GB��_�zjN�ړ 7�6)�M��I�CqX��$U�x�CS��ʨ|!J�S�`W.u).`�\����%f4�Kp�픽阉�Y*���`��������A���F�ؠ,\��NaB*�@9�Pz��3��
T (�S�|�B��С
5���S��.��x��`�@��_�.���,?쁣l��y�S]�z���I��,��brDnq�Z�A�b�E�H�TH��4D)"a�<�% �T���;���8q`N̨���7�WI4L$@�*�;�a)�ð���Ϯk�
���5��K�tظ���3��L��"܇A���*�1��`��P(;@�a�6��3�P�����0�t��3�m�w�X 
K��o� �&a	���*P;��7�����*1~��sbYc�+�s��}����&���)����ZT��"^�]u�c�Õt��t�徭A�����ȁE�:Gr�X5� ��C@���N��M��r@�np�Q��X�5�;+'�%<��6�0.��+ԥ��h�6f�������1 �0d�1:Iɲ[��"9��P�B���j(p�y�ԫ��P������3<�P�Ё�r��('�Ѝ%�>�,�эp(��S8� �� �{"�N��>7ӎ�-(h����J9�#?��@��?��Ź�P��	�F������h�b�z�k�sٱW`!��� %�rT�LpG��IBC Fb�C�N腲{5˪o��J"%��0��PX�7��<��Zp�Wx�2��T�*�4�*�.(1;��2�
1���@ ɳ��<ˣP� ø1`�7�����(p�7���'x�P�p8�34́�)�����s���9|�8>�����(��)8PA�:{�-������9T������3.��a�(�����:c�9�kq�G�<�?��b��4� �LÅg�w9�\x1��t�i<�XFʗH؉H�	D b�H8�Hn�	ht�o �r�F��8��I$�FK��Ry���Q1�0sD��g�~��X��� ,m;����(��~�V��;��	Ƙ��x�C���1ĶˀQ3�= @�ȁ�� 'x3�-�-&��3�YA�͒�	X(��8K ����I\)��Fl��x����<�(��k�"e��ᐏ ��5�<@�(`� �=>�9��x(�����Is�D<
K�h�Z�Y(1��0� ��R�H`��X5� �Ch�F�J�n��n00O�Fr �W���D$�����a�5 5��PP��Z68���H�L((��\&6x�����/Q�c���q{�<H6�z��4H8� T0�*x7�!@�` ;�Qi�A�Z����4�8��	��L(Ђ��� 逭� ��!���ٕ����PO��O?��]��C��[k��I�(���d8X�\�� �3Ъl�e��ô<�#ס�dE:څ3�E�C��\ˀDP���� �H`�H�J��p5;�Ѵ�qȵl;��;m�Ʒ:���;�A�
/.�1`��<�Щ����������,��S�P�[�/p4��6-2�h�� \�ɇt�H�8C*�8�Iu ���B���,�Q�ٸ�	P�+ F`�X��ŏ܀$Վ+��s��k( u��=���%p�����OL,��@I�T����]?��!��ʄ���k@r�?�Y�!����B�؁�C`�����	n�p�s=Wr%����n@��� �����T�7�;<7�G0����+	&�4��������7X�[p�/��� ��m��3�ǻ����V� j��P�0Q`�;��8H�*p�	�Xj0
��@�!`B�|ʲ��Z�7x��9/�D������R�K�E��Eؗ!��xH��\	���G4�l��g8��~b�Ñ����X%�L������'�q�w�p�)�ZE�ʄh
�J^H�\�L�	��	K�}A�J����H"\�o�pP��ƅ�L��,�#�^6��x,9��
0 �����T
��cǸ�`�+��/ �x�KX������]���T 	z1�c�D�42:�)���	�PC��O HԪ�y��_R�	'xÍF��)P�8�1�7���O�Gx�^��L�FhC��Nie��U�]Ĩ(���@`�k.@h�lʎ
6�8�S؀�=܈���3p�nD�<pr�������]����#�ԅ��L�	��r!_��L���n�a�l���F �Q�5R*@���� U�P��*��g�+0����ɔ�\�v�e�6m�<]7
�t�ؐq:PT�,���� Q#%;�� [��������6p���
0'|�d�:�,�`���`�*{�`�|關KH����,����l�kpZ,��1�	�'���撏l��(Jb��g������e8wH�j�:z�uQ/���q^ة\`�O8ˎ	(�(����q��+����|�Fht��4�I�`ld�k��Z��+�^�A�.�b����/111� ���`�},�7��d�Ө&8H[��S�E���QC5�� dp�԰%Բ��q� ܈�(r���8�6�IӁ `1���^�LxK QJ� � 0�6tCQ鎥��@�)�) C�~�'�	�g븦k�H)m��(P��4s��E`3�k�z���#��7��J�]@V�����숳9���"�؁D@���Jp�Jp*z.;zv�Ҷrh$��Q��(+x�����7P�/����f���ԍ�0 n)�L0P<�����9�ƣ�f�h�n�{CG��8�9E��m;�	o$Gr����HIPp���3T�Pk�8��= � HeP� ���K��=�Q�Q�F���A������(='C(��@�]u�a�؆`���&�3�����!밀�s�k����&?�J���ҁ�g�qwYP]x�Oe�����fN�[F��F�,m�+�E(L�:5�X���ƥ G2�K`��͊�N&.���� &L�P�1n2�Kఌ����<lK��9�֤&�~�K���TPDO�l�\���Ŵp{cP��+�� �
�J�U(��,�� �YEЁDX��̂ؓ�_l@G��Q�uF� �HXn3��g �H�}ԁ�&Cz'XT&�S�k��J8��0"�M߼�v������J[Vع#�y�;��dp�܁� �����z)�!7�� &�	{��H�@z���w݄
t��+�qW�H
5@Ҩ�0pW ^60��Ӽ+(�!�T!1(�ᾁ.�
2����%�,2A͛I� ����U� g�;ȓn�g:8�V�S�F[|t�ށ�^8G��6`8��	 �
&X��%J�*Tz�H�<�̒��J�2ez���"��F>
2��	,,9���?s��YH�ʕ!=lH��d	��~�]�v��?J�*�%�!B��:UE�*��q���V�[�j���.^���Օ6�.��p����\]�D]G�':8   d���Ȉe����nݾm&����n��A�C�2��_�Ц]�6m1nB�r#���0.����4?������!BĄrui�0]���F��c�_��8j$��޽*gϜ�yv��6vޤ���!0 ��Z9��C�Р`.�`C�p�RI J0A� EY�Ei��DC�C=HqPqT�������F����H�(�.�`AAg�1�|�q�UT!E=�@ŕTP�K(Q�\S_�A�
DU:���	.��
�`�;��9�-z�ŋ-h�E�[����.�$��Z��(b��� � `��y�";$���	'�q��7�r�f��A�J�k��p�n���Ж����d��s`w���?��b����
^xх���7�wz�U����6��cO*���-yؑh�A��pؑ�#hia��Ё	9��`���_.�P��S�ȀQIp�
/8a�$F)Z1���U�xF�THb�(e2�%����:el�
I@12�t�BTH�(mЖ��P,a�P�xm�I���3MV��@!�0B	TM��UvB�N*c��XgZ��s흖Z�?�/��e
~� �X�b�u`�d���	G�d���l�Mh� �ڭ�Fl_�!,n����z����!�08� q7�q���##�2�uat�0����w8</�1�'o����)g�a�3����5��}m��!��@3H1	�7�C�)AKPXP��EAe��І+���`�'�8�!w�C*@a
`���A
�x ���@
p����3��m�R��w�#��� �=(�(F	����?<�gs���M�SAA,@u��,���(��܂]�ş���]�����.^!�Oa:�	Xc �  hͧQ�H "��ƪB�Xq����H�����o�����b��Wn@��6H�5�k~��0G@3eu� =/�@z8��(�3��K=���ybЌ��A>���*��4���T '��C xfx��P��A$x��vPd�R`��@��D�D%�B�`(X�
�`C.L�
P`�t�C������8�2,�F�d�BBz�)P�p8���/>����F<2�Qld#E�Ù�V�����XJ�`����mU)��b�#�-�`�Y��.�/��iZvA���B���DCܘ�gf � �0�4���#Jő�h.4���<@�;��@�y�z�_��7��� N��3�� ���l�픅;.0/�8(�l�.u��;��A�u�����*� �@�U���I(� +x�3%�M�bHP�$6�� ��BD�i!g��`l��H�.���h0��H�� �h�0:*�Ht$�!l3��)�R���hd#����y5�ю~ɇc*���/8� nTqi۬2���)�z�/���A�E��ڛN�j�­QB8vp��F ��T"'79B�*2���P�Xy���զ�ʚ�֮&��D�����RC`q�F7�`�]�R~��dC�`	5 �\��w�����C���s{��hV�@J�����~$8�l[;1C"����	F��5�S�I@`���H�.�R���\肹�@:�!�6������<�C�-:=�����k�(=/>쁏��ף�-����?�(J�Ƀ3� �<��!� ��I��),W$��
(-�M����.W�TȂ��4y`���Ѭ���L@"����S��q��A$k�ݚh��uvX�ր}�����,nWt�8���@�P��̊���kY��P�0�.��g����%�Jr��tmA:\�
��`N��i.B2���	DС *=? ,��(��|�C��8��zyF2�!�l�q�E��aO)�`BX#��m�cCА
kh���]�5�ՋQlX#�xF�Q1A��� �����j;�U���#�g�i-��S���-:#�{���������=�f1�2̷	�#���eX��nd�S�bLa�Z��lp$���I�N�V�n��V�8���0e��>��+�JPk9ӛ%-g0\��^��G*$Z�ю�	����`LK��L4猈��<��-����
ȑ@E�P��(�E.P��U`6DC��A�T;�A�H��Y�'�B(���)���\�ɐ۝�=�C�MGѝ5��5��6��R����p�T�K�	V� 
��
PEX��X�����.t��MOuQ�H�+��'X�� �������� 둊�\�e�X�|N&�b`����$��$�������rD���N��@$�\����0p<�P���d�wD�����G=�G��A��8TʬA.�(� �	��4�d�����8�m��|�` �38�*����'0�]`C�EuE�*�B���@(N�0�2�c.�'|�%n�g]F��FuTGY�]�=��A�u����8gmI�4c�ҹ�Љ
��9�M佂�Xў��QN-�EZ�(� #����39@�UcP�Z"`�\�&d�W�
8О���j��	� И�M������hGq��)eqt��ł%8��4�qX�+� ���x�,UOx�:�K=t�}�$�A� W� �!�.<�)�"`,�lE��#DF��F�O�T�� ���H�%;b;F�]\�	A�D*V\�(��1��fnN�\(��-�W<X�m��6\k�3����5�R��	� ҠL�Ĉ�Ho���-��9�r�B���1!Y�ųi���.D^-��}# �R�
Iz�IB�VeK�$W�gxF��d�Њ��`� ˯�~�A��XX�J�� "ƕ�B,\����U���|�wtG��y���@(�G=���8T��
�Al��AA��3��3��4�_���-B^.������	��m I�ӆ��X�BH�$\`A.$C�U��b�.S���AL	��9��9����A��B��ѣ���k���P��#lz�@�Rp�
��XK�dɧ'����GCv�m�A����E�E�]'����)��� �-B_��5QY5LcLS%���dΪ��gxF9t�7 �X��Y���������������\��U�%�]������%��4��,Gŉ��RĉGyT���HOX�b*��3�A8�
�� ��� ��]0+Aw~a"LS��("x���J3y�����Aw*�L���͐b�]���YD���BX��I��XB�1P���ZP`��<Z�Ǫ�G����R�AM��pi�&�ڀ^�@BV~��A�E������J[.dg�a�`j��T��x��n7lՈ��
9�C9p�#�U|6��x|A���+�����+��%xA���܊K�F��-P5�,L%�0��-+���m\��b��z��;��)H�?BA��ȑ�@J��ȁ�`�F�) F`x �@�4I�!P����On�  �"$�#Ԯ Ai�/`�:�CFC5H���=�S�@�%���є���W�/T�5`�:�E=��~,=ZC��	����%�h	��R˾oq&��������(��R�$ja]��.��.(g-� ���@鵆Y�@�M�FpD7l{L���j�7x�7D¬�^"%��� � O&��
��A��������%��ێD8L5T�4LC&LC�˲0K%B����X�'2qXkq���b��h���� �hd�EE�4DC+����]�"4B�%�U��]��x��%B"�H�),i�����f�=9C4ԁS`�B����E2����ZP �:���c�������n��ȡ,h�)�A�Ԍ��,���5+q��D` .k����`'��-HG�H6��t@-���d�/�4w�0l�搘glm9ls/��N�J-�@|�$�����O��,��%�m,Ђ,���e���4 q�s���r_�����.+)%n��Wr\��;ć*���Y�<���lL�Z��C*�"���I��� �HϱI�Ɩ!�0�'�B.,6��F�Y"�:A�,�f���c.�1�1`�Z8�GK�E��ZL�3L��v/ p�6�]RD	�� ��*k�B$&�$x4C��P�J ��%�$�`����*Y�Av�Q(hK��À�"x�%�B5g�K
�'l�c-�x�֒�8���������觱��n�ھ-X\�+��,���E_$��>O�8��%��4��Y)M���(����˕���C�C=T�8CM�(,#�T����:�1\AkQS�@�Bx�"�@���L7Ul@BlB",���(�B�:iN?6V��S�l�B.(�r5C40i2(�.$/�)k��q�Mor�ro���Q��h ��K�u1e	����� 3pA4��[/�` �A���������(�o�����jvØlP�,�B1Ts/7@6��fhme�C7X�lv��϶���n��H���=+��mk�B����Bm�3L���1q��T�рP�􀇻(4q��rӍ2��ft�Lw���HWcvsIӃx�cL�!��i���V�kl@U�@A2�!��!�����6�~c�.D�./�dA�h�PZ-8x�/�4bs�E�j�:l�:��S�Z�zv��3d�6d�Q��h`O@��L�S�#Kͧ�30O�] ��/�Ӏ��K�����PzG�4mx�@B,C1�C7�D�WU�W�9��00C)�@��N\�5�
�Ҍ���[��]B���#b�*�B��6L�%�A�B^��me���V2k�Dq�R��X�����*PH�ۜ�2A���c����%�
��V�=S$ю� �_�BZy�i�z�����'}8C$�A�hH׮r5@�4b�cj<�;t����i��`�K���K02��)�,�QZ*��M�8�0CZ�K� ,A�>P���W���;�AA�5��%B��mgȊ��y�6����c�0<+|e��eC34C)Ȝ���� 	��	��\mRm���A��A��6k/K��N����mOC%�4p�����p3��Jk�:���C*@	����`�@�8Q�F�g��eÆ-��+.L���!GEC*�0A��Ď@�b� 'J��Q��=c�0Z�d���¦˙�8P� �2�N�7�x=K�+�N��bS�MjU��YÖ��gўY��-޶l��>�'M8Tڴ���MQmش�B�^*=��2��.40@Y�J��f��i;�q����G'@l$11C�&��!&��W���K׮]�n�<�����7��ʕ#7\�%f隱�����'r��dȗ/n�dǃǍ<d.�	�ǒ��d�Ԁ�F2n`�Vm��i��X���������f��b0�@q����GB~ܡ�;찃�\paF1�T�Ö��aH��\A��:�ȃ1�`�B�@�6�!�T"DY�!�C���)�&���U�j�,���[jIE�gt�)��v���IQ��d�+�x뚱���,?��#:�h#�ڸ"A?A/6�@�
C���f�h�.*�a�ǆ�b��d�B�Oi�!�8�!��L0��2 ��`#YjA�v��nĹ��zN��6ɤ�t��9'R���j���r��iv�ŕW^ٮ���/�Xo���(���o0��/^/p�tR|�������j�gB~�9��;T��<�����6N��T@�A �HH�F2��"F+Mtđ\X� b!`�ɨ�i!^l�Î����-�����zR&�d�8�1�~J�l��FL3��f����T�x#E�"����/(�j�/���.Ѐ2��2:��op���j��HFƏL;�!X{�]�lYm��Gz�ᵷχ��L6)��z��G�fX���rIuh��xl�f��(�2\��W,q�2�e���#�j���2��|x�Av��@.|�aAR$���B�/l�a6&)�TP�]��B�!F|4iT�pdЀ5 A�  �� ��Cŗn��d�d&����p8��]�O���$Eg�
Ң���0-&2Ɇ֬v�:m�k���@�Pv`���T�R�A�Q/�������̀3@3����$8����9�	&���	��؅(jQ��Tc!��=�Qq��s��7:�	KP���?�����b����#!�;lT�[ã-f1<W ��Q�dA�i�n W�̠A��ށ�g�� 1�����U�Br�FF�`$�3��*0��f����bD���  At$�A(� �zxP @p�E*bw3^@�!� [*������E�`�$eLOc�?k����mJHL򐇱��,j�`�[���6d'�I�~���"�Y\�P���i
�@e���5�h(b�v���Œx��]uz�=�h��
�N7,��`�C��L��Z����,���.��[����z���a C(7�Y�A
r�[a0ْ����>	��g��Ȱ�@#h�
j�0	H� 8�c gB3τ�P��l*P�G(�	B�.HY�
kR�T��OE	[*�F�T$-Jk�ٙ��O܂%M���L��g<�,yʐ���R��օh`\��\��^R��԰4��kh�谆��@1��-	B���c�d�N2������z 5���F9 IN�����8N�HD>u!U��O#^L�����������
7�KA8`e�TY�Z�`0��J[�w�ۥ���6�� jib��B��i�E&��|5 �g9���9�Y���%��+��+V�4��}�T��e��+f�J�4�,�r�E*lQ;�a�e��Z����>%�PT��s����r3Cv�������3I��E�p0�g��60��Y�O�����78C��o<�	KTc�sj"��`�>2��J5���z�3a]��7�_Xk�
m\ �Q��j�b\@:�S�zl�>;�8ʥ�B0�� . �3�Yx�ʀ�$3q�BpVX�@�x���l�Rj���\���xC*�b]СU�2��&ö)�x2����ք)K�Ŗމ�i��ـ�5l�׺P��@"s�����h�ƀI�"�7�)l}
���E�-=��r��O䂧q�!~�I��cxΰ�ѹX8�^0�!�ȴ�%�f��v*�/� =�1f���}1�;* m� {�@0 �T�!b����t���q���=���= ��PM"?� ��@e[�L'_3e���*��C.u|7h����v�K]R�\jP������3������&�K_]��@A�q?#�7�4���6��.H���2:��:Ko`C�i�������f� ��y ��*�x�v��*��R'��.��n��(*a��!��R����,z�1P��bj�G���:��Ė`� ��dpD�`��p�D��o"� � ď(�
^@0OX�6���, 
�    � @���:@��Bo�>���j 
g�B���~ X�r�XALA� r�.�	�`(��Ot��BM�,0F^b�&�jAN��RA��B�˧�����/�(n����� ��p�
��
%����/�@�N�O��(*�/��A���U� �*��Lr�,����e�n#a�,o���B�Qq�,�!j������ �n�(���.��OD,��O*@&�& �$D�����
d@Hd�� `     HOAbG�5�D �i#��@�J�~�
Ɖ��`9|���X!&M�@AZ�H
�`t2�����D��Iڌ��c�\���.��(�����n���
��a���$JQ�"�$�L��Fl�p�n./ހ� �(B1�p:�8�?l�N��z�vnhvl��/��$�넊z�a���'��:p!̩"w�Ax����8�h �`!��/�  +��/D&`�qE�������� ���T�&�� G�
`�3�`�F $�茀`v�p�ez`9�`v av`��>�L�
��z�pd���`
��=� �,���&�+b'f��`�ESR�g�$j!C� �� *'���宒�H�l��+/�0���2�n.P� �-۠�;���`x@@؀�$��̂�ꨎ������6����iB��#�*��8P�p� ���g`�b��Q/"   W�p�* H&�4%@6�!� �@� �Ԥ\ ��gȆs�$� ��L�D����ʖ�pt�p��qz�YNb;a;�`�@JA9��9t 0� �&
��"(�@��D�bM:��j�)��ONn@��D��~I�*��.��V���/pR�
JHtD'*�� ��++��B���c �Kv
F�	+���B�h25Щ��̡�t���p�f�I�"�2w�N�fצ꾬��n��2lI0``0�H���K�1D,�gp��M��ڀ���<$�I� + @ ��
 ��#J��)"Ntt Z�#�|�����;A�Dk?� v�gw�z �H��&	���S���a����aG�$a�b�2D@�ֵ� V�nkn(0�-���"JWن
�E!��u�HS��
�@3���ʏ�(
��Dk�v�]m�0��B!ft�j�F�N8�qJ� 1�G���&���ɢ�"�Z�]x�^g�\��m�J��o��u�a+�vB�ab�	��g(
�ҭ��4"#�!�O��U�9|`9z` F��` f���`�f{�7Nb%*b9��-���&���
�J)�I)0H����$���<1�`+��2��dbk�Nk�6l�r�/2�64��
B�Ҡ�>j/L0���un�����`j�fj�a��B+�$�� ��u[�j097�1�ur%�G��r�*�"l�]uG,�����m	��Xt�`'`�q�& �"�&��~	�0��Mߴ�4d�t��-�H��������v@*�f���f!��"t #��
,�=��R� h�WN�&zKv�aL�I���3�ڀ�$��.�.����,�NS>�ԶX�UB��
B��(nq�QqQ������&;��|�Y�N��iVX*"lsqGeX��[mC��t�r5p�~�E0	x�^xLx�Q@�
ھ͖��,�K�YDO6�	�	Ѵ��~8d�0��� �N��4`�gw �)�А`��!�sH�7{�7;s�2���wb��=��	��� �R�ig�&X8�v?K
@!���,�fqw�`*�v��F��lPyP4��b/�@~N��2ئ2�/��5��_�oP/� ���8��ur��֚���6���vI�x�;Pk&���{*f4�0� �h`�fl�H�D0�藍�mb��d3&	�8.(�~x#kN�
	  ����OA�\H�頻{!{!��g3"#v�>�N���R��1��~�)��)�,�b�I�,9����(F�@ҶO&DY�o�+�"mƆ�)T�.��S�
@���/���2����jl�B���L�t"���Y+nH!�sq͙e��h�a\���5H0�.sP*�澶A ܄uǄsgU����
��
`������"/&vM��`�:�a��� �78��$`ya�O��gEr�p^;��7���yV�+��{v?gw�&�GZ2��D�i��K�LA!�@����l�o�����M���al�BB/��
��w�f��+�@l@Q�"3��D�#�`QD�%L�]SX'l�����cx��92��A\�kT��6so�.{� ���Y�N�"� ��_�V��b�t�E%~�v%��-���9����>��K �L"!�4$$�:�h��c�~\ȇ�ȷ�f{6#!�!���l�7h� 
�� 
\�D�jb�I@���%'�'*.����΀���|���nt�/�b�!X�,�m�B��^�����@*���X΀��p��֤Nz���������;sB��A��e^Hx���.QMxa��EuG�� 
�%Tn=��hpT	A,� b 3Ε��ob�(����H���yNA�೛W����c=L �����\��Z;�+���<�R� �@(� �
!dg�D)|kKT��:�Oز(�V�� ������`�"�ݛ�8��v/`y�2Ĉ�֊���^Gi���/A�+xa��O�T�\�uގ�5�\����%ikbk�+��V�(u�B,�@8S���0DT������
"�碤�&�2 ��^
� ������X�ٞ��}��C�b���D�#G�
sʡ�!6�@�����)P�@��
8wl={-�g�V�s�6@���1�G�6�ؼicg��7?1F���R;v�P�B%*�4P�����VO��y��V*;D۠9��,;�R�IUK�\����ʶ-[6l|��|V��4lz�m�_���;~��>{��k��1�ƛ17���r�l�K��M��xض��f.\�V.�m[��iy���f���8pȈa#F�1fĸ��Fl�Y�ެJ�4;�s��ň0F� � ��(@���:�� �Ď0�� ��<x;�`�~;|@P&� ���M���b|�Omh�J}E&�bK4�<�/���y��(C�B��m���[o	�Sp�ԡRUБF�T��$T\AуW� UT�bGp1EQo��+p�U�X��y/����5'&ÒJj�FZi���ٝ��S�e�݉�fy*V�e��͡���jy���4����-��R6��5.�ܒ�h|��80gC��`*�� �u�]��Nr�;���
#X �
!H����^�-k��	���p�	
�7�"�X� �@�7�
I4(l\�a"E�E�SdA�$��K����X��$�%5���l�҆�A�NAa�N� �TNE��VOI%TXH�#½A�(^�R+���G-��rf�e�bKl��FX46�ċ5(�e�g���'���çd�j�dA���a���h���&/$�R��2/�`ִdYi���e� ��7$g�tWz�2B��tЁ.���N?� A{�A8p�} �_�?8��;�P�@9���B��-C64 ��R���wdQHAa�3��Q�-��m��A��C�o�2����V�Ul�:� eTS��8@��O��WUP�F	E�O>��%Yq�|&.����^r��3a�f�YbD�4f���٠w2�'��	QO�^4��(�%���R6?ְI0�`	/jᖄ��_!�tp �`�91����!$�%)fHj��ؠ��\ �= 8	����0H��j0 ��Z����1���B< ����C��(:��#��Nr�&zF2l�\A!�`���!0�
)�P��6�X�Bt0�0�)���T��#*1�8�Q�Ԓ06|a&3AK ����A|C-ހ���D_cI�^�4��)|���ƙx��h�|L ���l��� h�)�5A*E��.t��7�l~�S`l���]��-D!���P��@z�1�u�a%:TaHN�zP*���x��S�j��  A
g��`��)���G
ED�! �!��~| 6����:�8���H�b'�g�M�B*R�	 ,"v��P+��q'7�Ax���Q!GRyRT()�W��
e��Z�T�J��_��&?I64��T剢����&�(M-�F�\Ɨi��;�����ci��L���	�MKvᩖX*�����Y���b�^�����}�:��
��=�u�x��L�
*�`rABN���>�4��x�gŶ�A��
.AZ9 	rP������L�
9���@���g�N�ڗ�[]�B������Z�-H�	��0E�D-jT�2=ZϑQ�bZ�JR��#��SX�1���5`�y��(���$�-+��9��cC�ذ:��õ�e�bα'�9�2O�G�MC�Ґ&�\�5v�[4�S`Q�6�Jk��-�E� 6����-hX�}q`QT��=/���JTh]��PDk!�.pM@T(Z;�\�dۅZs@ȁ�U��ٟW�N��E6t�'i�Es�]D��wH��Glҳ6�{�0<�F�DW${0�Y�b��Jìg�(q�mC�̀����rA��4�@9�^��j�Pe�2I ș[g�E.R�]�.&#����͒=[�E.���d���օ���A]D[g�6��n^����S"o��;ef[Pt�Z#��i`����"�8i�d6��,R��1̲��P/�
�P�g��CbqU�>�l���S�;߶��y%,�#�'Z�@����`bX��R;���-i4l�UT��P#ǂ"m�<�bN�/��Jd�I�P�T���7*���C}�۩�^6P��f�npU%#7`�tT5� �n�J.�t�Z|�:�f�\D&�Z8;��|2��n�A[��΅(L
c �>Ƶ5���B.�W8��v��}���&JV�B3� �Q��&�tJ��a�y���n��y�,vXN��d5T�:��x���$��m������9��+�os���Sn\9X趶� 5_k@@8�H���g>�Q�Q@G��\O'uPpidhqRm�m�0��]92<a'H.�Us.3�:�0�=�S1W1T�f�̓!�H��B�g� 
� k�b�
�
>pk�����pt��&��
�`z+Bo��p��� ��	T(���L�z�"c�����Ȱ�&��@Ò�m�M��M"�m!a�$���I	�$>ok9r��U8N�rC�ks�2k�y��!SY� =�$+���`h��:�'`D��S'�|� 1�`��;�B�sײs�Q-��[~V�p.�b.8�P�yP'F!Rq�\YRp ��Uِhl8
��]6@+��S��7�4� 	?�SQ"��wR�B�w�
W�p2�$5 < Wp��	�������
;0tl�l�uX�� 
l(�m�
�p.m����
U�y�f
�Pᆆ���n�V��0x6���Ȑ)�P����ՠ����
����?0�jd�grP@�N�(��Ag�$�o�*2��8P�dd �v �eRQ��$X�miY�
�`6 Ւ[v�E��Z��r9��g�h	a�a n69�Så5-�EB�TS�i ��UPY pY�q 2y�3iz�P#A���"���ttVB�A0�%šR��'�#U���w_0*i����C���5`�p��eo`���l�
Rh�B C`
�0x̀���`)
k��l���26��� Ɛ������p�m��C
���`�V�6��
y��0�`�T(
?��P���Pn�
ȠI���p&��e�
8��F�PIf�-6��p�PY�0Z�=T�m	�1Z�w���Z�\�POZX�~��'�z��5� 	W ��!7280T��Bvȕ+t�!g�H�sR,E"|�"��
�Hs�/B�$��$g!h��	WP#8�w'�0Y%R�Y�1#
�$���E�T0T' CP	b> `;���	>`��Jٹ�̀!�pra
� � ���`
��z� ��<�	�������P��� �|m�
� �0�p�����@�lp� �`
�*�^ �JU� ? � ���0ୡ�
j�� r_����d��Pf�)S��dd�`��X l�P C�$  C�3@-'��&�Z&P8'�,KJ������C�%,�Z�"�g@:pH�V-5`k�JI<�=D�!Q$FB<H"[�
҆\�"Z?j�67:�=cT�	���wY��oW�*C.@=�
b��*�lP`%`�B�7P���?�Ql�yk\�ʷ�4a ��	����	��	%
[;0�
aP
l�Q�*����i
� �!9�Юb
bpz�������	�p��	Հ���йn���0
�`	> E7�P
�Ѓlp����i
���n�oP\`}`�A.��ZQ�',�� ;�xwO
Y r��Nʊ��[�e��Z;`
!"�Z3;\��Or#�O��+>l8w��*`��P �DT8B��$CYqjH$C�_��+<P ������(=��^c
ONTEk#`��xE��#�X�n l�!z;6 X��wz�%k�
��l�m�f��f
����uy�
�0��@��+
��QT���Dg��йT%@��?7��a	��@��|?��Q5л�v���_8`}Fv&)S�p���0Q�$TЖ�"#PB�0�)gX� � ܲ���5���!8�ɥ��B@�B@�9j�$��g�zf��[-0��f����q�:N+��!P`��ȑ�;qC 
��;�գ^$�kQ0"�1 ,(pO�7ȑj�FwZ�wbˈR5U����7 8�n��2��p	�Z��aw�_�m�WЯ�l��2/���p;�f�K\�\ly=<
o0���p�!��_@���_D6�jC��8R���������>�{��WY�W@*�<4>����{�>����-�����h	?g
����	�`	���F�����Y
h�͠�0
���U
����������v�`yt�
p�:J!��҃#����w�0U|���َ�!~h 
��#<1��+*p���! C�Ԩ�����Τ��zW6f#URUU��-
;�	T��F���0�f@<�)��b.�!�U�2o�y�2,�p��nP��l�P���xUE*a�C�TGg6Sel<�9H�N�V��x2�0j`��=D���a
�~T �} �b����'7P��P��;P
�@זPC���� Y]��0�_�`����@-0`	��p�|4�b���y���1U`t�
�u��+p&Հ����
��v����r���n�T�&mP�#? �� �+x%,O= 
��#��T��w���"�jq|�D3��)���j�)j2Vxa��AK?�v�3��3&?QC���(|�)��&��n��{��T�Hǆ��*�ZN�=v�]Y����%0p !+�1b��C �[>$P�L��W@\9�98
<��� @�ƀZ[�v� ���P
� �?�z��ަw��6 9y�l���!��������
�J���3�@�U��詝���R��
�@�o��ݖ%\1���f�fћqH���	�ga݃qX-���F��|)z��K>S?�q謑�n�N��٠�C=�(� ����'o)�rB�,C2�K)y�lv	�I��|U�j�� ��^���!� � 0,�r6k�1��B8; ���C����O��O�p �:�'P���&@-=p[��i��;� 
	��\�5xm�;x��� 
� 	�`�`����Ȑ�<`�Ѱ��`	S���@�B���
�=ڧ�'%�M^$
e�k��f`6dC�j.�_`��"t�@(rz�4֐)�B��3���b�A����� L�����:ѐoTd��5�f5.�%�W6�\И��� �6o���ϡCQ.l����F�	1� D�$�Á�3�\�`eK�:8���D&L�i��ʓ$:�8A�'D����O}Ԡ��T���D,B���"+�	'F��Q�"T5F������0����Cze�(�����*�ڸx�*/r�؊
g_ra��
6h����MA�ojE��-�ml֬a�=��i؞���x�m��-g>���ك�ys����3/9�mܗ�_�-���ƱM�f�Z��آ�ƋW-[����?/\v��<�0#��4H�T��vy�!PS!�"�J(*���a�?4��28��@p D�J"���L A(r� ��f<`�j�AF�����A2�ܲa�֠����T!�	^�
&�`�^p�+�ಇ����r᪫�,��~��NPˣB���(�)�^��4l����pM�F�p��
rm56\CC!^v��6�<�M���;�6�[��c�:�[N�wxCn�O���9���>k��f�g�ž����hm�O�JC�4�u:fB~>�r"�2����~���@��(n��)����vAϫ�=�_t8!O{A0����1�O�(��~����n�A�������0��
�T��ʒA��ʍL"(���ɐr��"cxs������I�R����AmpL"�Yi�d(�Hm���� *D���ըH���n[=�����#[W�R�n�骃;����^��u<�q�͸h�=v=k؛&�������O�5�x�5�p���a���`C�7��
�T2	$��#��+�u�`��^�` ��������m�)��a�O������
6>i�6���E��QjIE?3�H�:��
���}(�x#�\DI��2I�"5]��H�j��IIQg*�KG:���,,�d�b��]�؛�J���f[��:���цl��F2���΄k�yz�{�n����]�j�qa���7�������p�Z���������iJ _��@Pӆ���ߚ�'|��|i�����=� ^z��M굓�h(C9�P�4��9�N:P<��k0?�=,�W EBN���m�-R�;�a|t��V �*ܡ�Sp����T��L��̞���Q�j���Rq�<I�8s����@A� �"&>��z�#�x�2���x����9Ё�f�0��M6��f$���E.�yN]�3�H;���ڜ״&:�q�w���PG��Û��MU��!�6*�S�a\~j��P�5B�ְ���l��SVx��M6h``� �KH�ۉNjRG��d�o,�u��$;(�����LA�EB��I��U���Ta�V�⩪�Cз�*(�|���ȗ
;4�zX��+1�(P�c�AN�
Q�UPfi�[z�40�\$f_��W�6����2MeJ��갦<����xS�&�1���dd6��lf˩^����\�.r��t���x@��!�yõd{[(0(�hH��GXij�`6(���vIفv�!� {iNf/�<W�0�i�r���Ő���GI�����b*�T;�7磃���>'T!
s!�>J��l��g~����r��i��XV�:i�5 ��l�cV��p�=����"6�2M�Xu�פM8�Y�b��0�9Eq��UEM�p}�"F�Hk����Qq�Ax�����@b���'��I|�\���9h�NO�a�l����T��X|�*�g��ٹ�iH����w��E��jh��D�4Z���
�XZ%ʌTiJ+�^�W�Zԣ&u�MX{3�����u���hF5j݌p4C��hF�sa����v)>q�!$۸�mn�s��8�)g|
�}dVZ0!��\{�p
I
��̏-XQU����j���g���H�B��m�J�W�̚��1jq��|�Y̒&����BCN�p�G\��x��Q�|�ڰ��G2�Qj����G4�MpFò�dE*^�cQȢ0*� V�VO�TN��x��o����(I�P����d���]�[��z���H,L�A3r�����uٱ�Y�C�95�Q�y�]��L�>Rm̊�]�{�{���WcRG���G���iZ�c�ؐ��)�c4��Ѕ,R)?Q���h{;O�	ʒ��D+�,mW�(M�ƸT+'F?6H,�X���'1�]!b��(� �O�;f�)XA|Q�:����Yv]'�쳦>:��w�c��pGǗI�|��}'��~������[Mw��C�Hq:��Yc�֢��i?��ٜ�H�+���k�	�ٚ�p�$��ძ�a���3�˅Z0����97
���tb�˺,dȅĬ�:�d����5dH^�A�3!eеs��cu����w���{�dʇz?�KB%\�Qk�TPl��y�
B��?,L����1�I%;h9���8��\�pk��y��J�hЅd�]�C7�m2���C�R�!;��'s҅7Ȃ*�D̂���)0�*0,@�1�T8�T�̋��S^˅^'�C9y ���St���;��&lEW��*�3�EZD�ӈ�Ш ,꿣-R�S�3P C�ƖK%K�T���q���6PVH�g8v
-K��>\�� ���ڬv��m��hxu�8�w�-pG-`wt�8�NR����7Т���������ə$ �����Ё(��x�ҁЁ �� I`P ��4>V�_8��\���p���u�w�I�4ŷ3���}x�Wl0ϙ��+��R�́����3/왠��P��T>��W��6�*'p�'H�ED���6�@fQ�Ӣ�͢t��g��(;<�><�x�t�UȂ8��-�G���x�=��-�Tp�g�3M�$�/�9�/ǔ;�3�����|` �� ��L� ����`����؀�\M�� �@��M�L�H���H��͂���)Ё��䔄�$�U �4I��ΖlIv8l�ib&�CB����9-j���M�4��P:�Hʙ��h��:����6xF��Jw�J(pF��(�3���>�]HGrró�F��l�Fq��h��(PD-������(��,�x��lx�=�;�GPb�48�E3Hت?��h �� M�L 5M0�5��<M ����L�H�"m P�%UR$�R#5�%%�)M,�R-�M'��$h���X�pp)p��Ϊ��+H�OXN�,�e�������NvX�&y`�3E#L&���_4:HZ�Gb��B�ܳB���F,B�{J��+,����(�ϭURU'�*P��+��جp�=��=��;�����P-؂x��8L? QUy�x{gPDw�&@�%��%��f�GztG&x&�L~����� ������oMR"UR(-Ws��t%�u}Ru5�$XҔI�y�Wz��$��|}�|�W}��x��)M�!͈�� �tMX�hM��l��Hp��5��P���e8�E5:8�����d��B4*�=P��ə�C@*��-�P��'��$P'P�'P���j}�dE�*�$�R�W�C��u��g��vlG��^�P- �x#�8�ژ�=Vyd��zT�E[-P�&`���������QͼۼW��pE�vuR���'\%5\�*M�`Rp�$P ���؀� ~X���~��̅��ER�e׾���̇5]�d�@�T���L` 	hX�h�$,:�* ��J���v�ʬ��Oz�1^)�O�I(���|���O�����( Zk��( ����U(�X��Z�U��[e�x�K�P��н��l�']���4H^(�G�BVkU�� ~� v[ ��z8�ͻ�!W�T �\&�`'m�v���mRX}�\x\���WX��{-R#%M�\�r��[����͆�]�]�H������Ҥ �(�ٜ�Y#^'H(H�������Sub�^&�^���+`�����
�8�G;P�V8�Y}�tT[��8�Pxt_��G9��3�?�5V3y��(�1�NB[�m>d�` 6[t��z�[��L��IFM��Ҕ`s5�$uR#���d\s5&�ɵ T�\#��ͥW#a#��W��"(�$�e'-�%�	VR6݄�a�l���Qc�L���O��/-�3Q�G$Fb��Y���b�M�bS�ޟ�Y����EdDV����'�GC�KE��:c�ʏ�%�G\�g�U]�G���}�mx�60�sI�	� Ȉ���y-�%�EFgV�G~�zp���ۏ�` \`^�fW%��R��"����`�%|��Z�\(���ȍ�\n�Y��*��Rp���DX�-M��	X�k���j8��\��l�#�ٶ�jq��%���٬�>b,�ⶍ[D^��m��;�c�<�,���]�^�,�Հ�˽�Հ���;�U���Ӎ]�h�Ј`�][z��E6[uv[&8H.����dH �� Ei �R&��F��&js��]Vm%m��U�}-��i"��|}��N�O�m�~m�TM��ۄe��d�ƞa�ހhPԢӹT�G;PDkgrg,N&�Y�>U(@ĀvǬ>狾bD6�vVgU竽�^��8�k\u��~_�No]݂=��3ph��p�a � ���C��~[͞��h���^`����Nf�_j�5�r]\RNW��m|�\��$�i�6�\�eT�RO&M�|�^���Q�En�ff#��	����$��j�ҤEV�Mk,�#���<oDT�g��g؂#��j�l��l&h[5�o��c��н�c^�k������Ǽ�)�c��̍8 	8���t�X�M�V��^�(�l����\ � ��K�֔� Xh�mQ"&b��`�]�O>�Tni��i�n\����]���"m�x]�M7W��V#o �q�<f�l��O�<����:��'��p��^+�@����TH�T �0W�5`�~o��P��Z��=����]��9�ռ����P=@D*p،ݐ��� �	�tOR f۴�l5wo&��le�t�5�6���W��j�XІq��L�j �b(�X�/ �T b�v%} ȝ\�n��ne[��Z�q��ix�`NW�Lz�L�-ͤ7vd��"Xf�$����3C�wD�̦^,^bz}�t��* Qi����b��hG_s��oz�s}��|���w�������-��)���� �h�߈�@��5dF�K�n���w�lg`�V�mD�O�N�M�LȄ�߄L�M K��`�8nnq�Wexm�Ƶu��������iUNW�V�u�q��ۣOR����[�H�$�%����`�/:8��G�U��N�?'tA�������+Ѕl8����&@gt&�v^�9���Ԃ7�s�k�вE�z�h��'��-Y�la%!������@|4A	 N\d� 	�(M�4a"3
-J��<������ޝ` t(Q�	�6P�t��	@����DV-Y*T	k�<T��D��4��V��$e��-bd��$n�X0b7�#���k�]%{'Q`��J�f�Zx��CY�*c�L����vA�6mR��CG��rڤ��N�2�0��S	�(J�X
u#�B�1��Q��e��I%/oZ���v�D�� >ˣ��G=��"��b�,4f�����;N�G�� L �J3Q�Q솓MOD��O�U`�QB!`ԅF%u�E�A~�AU"T �G*=���e���m�U�a��(�^G�D�z�W`��#[1��XEH�e)Pf
���h�5�YR͠�
m�����u��t����MJD��+lT�� �4`%��C��AGt�)�Du�ٔ�v�E�C
�'C�A��3~<㌦�q��{�-�)�M����	��p��'�X"INp�K/-Ƞ�9�$aO� �G5���(��Qf){l��^ܱ*!y�dk�u�qۗ�z�8�G0�8��ycX0c[�Fo��J�oZ�u�@g�AͤJ���iG*
�I��ZȔ�M���D�*��A�s܁	'��RL�`M�I���	�םw�N��4�dci6ټS���G�CY���C�f!�hoP�	%�DB9���"�h��T[K�m����?�a��:;�D�%T�g'wY	<F7��Y�	�]֌x��L,�,��`>�x��Q�q����4xKK�b4�Ed���ko�i�d�eώ� :t��њ�tآpka�1�t
���O8�!�@���A� u�P�:B	.�0��Y�`B�*(��\^48��N<��4�uʳ����sS� ��@�@����1�0��V��.5Q��� '�mB)���ն�-eY8J���xE-x�ݐ%�ĨE^i�̐�k��62����$	L�����l�!
�[B�q(̡f���0(F"��C���/t����D*5 (��P6��hС
��ސ0��f6Y�Nv�lR�'"b�	����`x��V� 
P�ZKn�(FEj<�y�iP������]D#�@�C<�N�g�u�2p��i³��D�4� xBi�ָc�[����̶�g5�l��۾x��H�V��$<�^P��3쀍g��њ�V8�m���0��V�(d�i�F<�q[��ذ� ��8@!	G䡥���'>�H�b6�@/��k(� PC8��5�(S�Me�a:����2�9@��J�*R ��"!�� mc�&�je⁈@F�I�@�T�
z���,�B}س�	D=���D^��S� ��,^Iw@���i���B���dOR+�]=�V�:��lJ�:��d1�E҂ `�S� *�� �"ثIt� 4K�(�����	��l��zx4�=Ȅ6vxG4�q.
�LE:��UD�mXl���,ԡ>q�����,�I:<�gm�'�	P!�b1�(�2!8�u٥"r����2'0Չ.��1Wᑪy$�HˋR�`R�ݮd�(�� V �?�`�ͯ
*�S��<6]��ܳ�,� &9�T��4Db��A	J���H�Y杖�2
��;��ˡE����%p �2<��>R�I@�-0қ�����E���x�7��ٸ�5��9\c!29��a�c8�%�M:r1�`�C�@(��l�#��E6Z���q�{���;���3�g�pF}����e��C>��������"щ�!6|�hm�� ����		�	x^����$������ 
�rw><���<�B
P��	x1���� ���V*'GE��U���5I'L㧕@�~��9���,>�������5�(�3;�!�x�c$H�>�,��	�qY"S���H6H	���7��ȵ�4�j�3D�~Xl����sBA��8B���Fc�@�(D]�9�x�/�t���8g��^�"��H-+��Y��x���� l�BBg�f'�AlA�	��S ���Ԥ��H�;� .P�pu� .h֡"vy����$&a J,�S���<�Q	��Q��
	�""af�פ��N�p�p
�{E���C�&��� ��H)Pڈ� ���
"�
�T�a�fA�3� "( 	r9�)��:��r��:؀�h�!��C¡�gQ$C|��->A�v$#�}�.X�s�B�x<U1rV�AcN;�!
~k�pg��T���8����%�-�w`�jVs�;�*8��LK�D���8�
`D	H]�eU��QT�� *��
0Ht`���GM)�z(ؚF8���J�����W��>�
� ��XXAl��%��`IM�TEթ�R����,HDh[��U��; ̅,6�QP�� ��x�T&d�'tC7�"��"�	^<�Y�۽�Eb��_PA�.�����:��3�����A-��6A��9ԃ.������I�;��+��Ƚ�*Z4`C+,V$�C8���=�;<:`C��;�B��A����FDJk���B�e��Z�x͂<�H��ԩRHU��Ƙ ��� pq�L)|��)>��F�-M�� � �$p9��](�����! #��N؄���Ș��p9��C$�DAY�:<�P, Q޷i^�1 2&�U@B&p�vC&�!"(@A	��\���.@����6�AԂ:ă5� A`C;��;��;��Md���s�A&��ͤ(��;�AT��I_<8Cj+p�V���a�*��Bebt�!:0�j��-
�*�m�����vH� A��3��T��=��<�Z?N���GR��|�Ϩ@�
L����I�c�0����J�� M�](Շ$� <�!8�#<�i��l��%[�@L���Zw�mfA=0!? $兛�@����GH�a$`'`�'8g&x@�`h��H�-�]�]<�b`NHB�PN��� %�9���+��$�
��i� K�+��HD�%�=��A4RD�.�d��;��)����B�@��e���A`5@�4�9�At�Bآ��<T\�Q��
�S�E]鑗qz �|Eh��@ �Rb.~�Q� (e�� �@f����8M�f	��e��f~fy8z��)���"�i",�I=*��@�Xu�`���DdO8���fp�0u�XUtz�U�!'*f�&8B�����,	]��}��X��Bا}څ� K�@H��bX�
���
`@{:�pȀȀ��@>��P�D�1�(�T��]a�0ZRV�P�)X*d��lFg���������Bم����0'��y=�x�O���3�)$��Q�#z���G��H&�:�d�(�.j�niy�*H�$� �A",�8[B^FU��4�ǣl6�Z�)?��@�4�C�i�БU�@@X"(gBB!d���	���&Z�^�K`,�4N�&a��]�bl'a���K��_���N�^X��8�<�*���	R�Zѧ�@����
����el���b����@�g����Z���}�Յ��iI%B ix�bϠ��.��,]���H��<��� 2@�N���R��)�c�UUY�	fX���c{��LJn��9h�o��"B!���|@!��Eb��zB9tCq��ݏ���H�Ƹ,Ώ�a�wFY-1�]H��R-^��9"F@���AQ�Vd�K۸Ø��jD��,�C��B��X�E�����U�Xy�+�B�I1l`A���+d��l
>U��#r�	O2�kfվ�mKh�R������B���꥙:c�A�.w��@�y,Ҩ���&$��,p򩰰����X!� �jF�C9TB`����^I����"�� _�K�p_��K�]��bd'����\(F�zNT�H�Jp@�.L-4�s��jĆ��-��uI�
���y�%X��>�Ê)%P�*�(.ՖB�3(��\����R ��|cV�2��4F2�����IH�'�%Jup��3�k��n���y�2AhA}؃؄����S��Z�08�	X���W��@$8��I��
��a�l`�В��������b�Ō�N��K��ˑX��I��	bX�6����E�lF��C*���^TPl����("%�� 
(M0y�(�z �B"��#P)����.e}O<�>���,YLڀ �,�0�&2��"�үɉB���*���`��+aVyZ"�����2�Gy(���>���:��U�[��B�i�8 �~@� @x� /�"&|�',�7dB\3�4Ɛ���5	�����`+����[��a��^,6bЅ�4 d�Eg��s
%���V�l�AA��抦�O2�,��=�!�#�B"0���::4����t��=���D�@8�36����jCX$��ভ]l.�&eJyX�4<CT��G�(�t
�eh C���}v�-���/�D4 ̊0PS���ȳj!t YCX��l�&1'|'����H�5��4������"v0��6`D�ϖ3_A7�uc{Σr@D��Ø�I��]"5� HA����h�Hx�,G���4y�`$$�$�*�rt��͌�=�6?趒��7y4|B� ��"4�L��!@�9#h]
�N�04�r�X�b�)P���
��8���@�� K�ݝ�y�g�>��2�0Q\u�T��MG���H��VG�,'t�s��Ө �$���"�\��ԁ�n,��lK�X���P{���a�ņ�:K�z������Kx�J�b76�Kv�̱j����\AH)H���
T�鯇Ն�`��X�Dg�2�����$�*lS�� yn�4�19�G^<@9�[�"(w���v#(�#Xȝ>�ǖJ�c
Μ�1�Bf�����V�I���7pz���x�&��;E}C$C�H�D'���筵��55�uT��s��9���)A�n0f��%�-����9��0���$����7A}�C�
iήKQ>��gp�;t\�[���X�'�#�e�52Jpͫ��H�}����"d�(���?��@4dC<𶒗4o�O0�7�(|#X�r;B�e�j�!D�ky��tmk�$�<�C3|��@
��`� ���ɚ��Cw�/�z�M3E���C��g�x� @�5�r�Zw�7�C%@��ҷ0�/A�xɤ�Ae��A����>���A�?�&���A48��|��t@��tg���!\�'� �6i��D�J�,I�$I�4X؀u�@}��ŏ!�D��J�8g�l�RR`J(N&p�2C�<xȑ�"G�%�D��S���y-Z�x��ݻ�*>~S�J��T�1S��l�("C7�m)� .RXH��3g��8;��6w��1��ĉ9j�p!�!<x��qB(p�V�{�>p�Nm� thѢ�n�@�ӪO�^��  x�c��q�D�Ӈ�L`h$��#ʓ,YbPl�ء�#J�;����UET���A�
;��@AU�Νg�t��EEW�s�ޱ�G�|Ɩgxy&�ʞ�o3��[�>�H�!�8�h�w����!x�A�OL	�zx��,�8)����\�逘:����v��G,��S蠣(���来�⇩�����Θ�'RFYdG���:�,���p���/i�y�vƱ��\�!���`��|���������L:��l4�B[���Lc���j;� OqCD7D�0�-��2x�ܢ0	%�K
.b��*��C�4$s��T�hc>4���4r�%*ڨE�T��#�h���7tI��,��Q^q�Y�h��gTх
6t�>U�{Fl�a%�T@хPx�
�j�`�Pp�SL����Ee�+v��8�@4�-�hQ�	 ��n$A�=�-G|�dV� �.6�Q2J|�''���*|��Ɣ+��F���H�F�^X�	D�#ѩgo,bv�a��}`�������G�itM��0Ì�Ύ�@�J��0m��4U�5�&0�S�8  :��B>�$O*��%\j�B#<��W-�`t�DG(�,��ub�p`(t]b�+rE3�h
:|���E�,��B�6l�E�6�8��V���Q�1�V��e�+D�'�0�P�GQRy�"6��!��8ř,�8���8! ٍb"�x�q "��OLl2�Q�A���gN�
���`���ЉѾ��F8Bi�Ȅ$zp&DQ�jFz֌�5����0�#!�aC@CH�0A�4*Bs��$ՙeT�o�[�*U�M#0AL\B `6��"
��ot��1��< 9	)B�`���
y@���7��}T�B��*҉�U���0 �Fb�uZ$i�CAa	N���j ��D2/�n���/p��p&�=�`(�P��3�O�r�E*d����UA�n��6	�!��E80�E��]f�$�`���G=vF�z����
�hG "��Pg/$�I�Q��MU��R��S,�8�M���P��a���*��M��4�D��AJ�`� E)R4��hgs&.ќ 4���T�(G9,���d��#��8�QIP���@
��A�O�+����J�s�B��IHT����-m	��, 8�p��h���[���03�q{-���^�xG9J�Mt���|�� �*䆗?��ՔJT�MlnsIL��;��Kl)<�
�!�n���J�\b��l$����`r�O�3�C|��/��?pC-*�3�6T��1,���X��MvPḼ*s&%㩺a�9��%���(ǁY��+�����)��v$�zg
+Y��s�Uj�����7�i��
���� h6� �2l�'BQ
S�B�`�7��YX"P'A	6@���9ૈo���K����6�\���53���b�K�&8�a�D|�T�C�:?ˊO��(k��4�ld���G3���}�J�"\�����l���>��� Q�i����V>4�*�0FKS�l
py�(L�8K� s��  �BT"��&,�xJ*�jBF04���e�~ͫS>�w�լ<���R����o���j
)`�+�f<D5x+�hB�P<�X�c��K��\��	>����&�>YX�E<b�;���b?L�j���b/�Ax<g�ƙ;��e���8g�U��a�Gʆ4�h�;P�G=�a� ���XV�%*����3�!���!��n��^*�0>�Q�z��rQEh��b�D� 	�@�h�(N �   �@>��JTB��D%�a	��82@�N/miL7���� �*���\�>Wߙ~.V�.�L�j�&ԕ�/D
ƀoy����
�X$0 &�GM�-?�$(��M�h�*,\��! � HN���f%٣��D��͍����ɰl��m4K���H�cQ�{���,��%�/>���n�KK�Y1bH[����7��QsS���LQ S3�n�	��	dq̐�7�9���%���2!qX���l���q��5Ս�S�2�Y'�QK�9���Ռ@V����V�H�f�!B׎p�θ@��"��\�1J ���v ��Os�7N�Tj��� P3��.�/d�f�i������������Tf������)*�,����,���D��oK��kv�F�e�χ�m.N��� J�� ��.��.��@#��@F�����&*qN�`/�A�a�AD�F
o�%
�H�`���.��*��. W9
��`%��h
9樻f�aI�.�S�l4t�\�(b��kv >ʘ: s��K���T*�G.�e� ���`/x���A�Zg$�@O�!���B(0�ކ�A����0�)��z�.KKK� K�1lt#������~o�/1D�,��B�2Ұ3L!�*S �ʫV�4F`�!��da` $�%f� \�
���Oơ�,at(��!������h�����k����k9�
쎠�Ϩ��-��,�gm������JAW놘��,C��Nubht�22�$�d.�
���(e��B��.��.��@��d��a�A�2��A�!Α*����a�F���v ��W,L,���\!�J!b+1 ��4�ؠ=�� �!��0J�J�!9�o4�5@C�!�A�!���>� 4,A�$�a��!%�pL���!&$�z�����j��/��xR({���P�ʎ�@)�@)c�)��)��L4��!h�p˶l
� �p���,�,3AeR��En��"$Bl!�2������A(���J���/��*�b�qz�'��2��J�$o7p/�@��ꟶP#�`����
�
^H䖯3�4���4��VcT����8M�F� J
x ��Q%�a+,a��)�!����֓=�<�(��������T ��=U)���䳿
���X�������F��x�o����*K2,I�$bf�E���xkC��.Q�U=TC�D�A���A�I���:�E�a�A���2�|dAK�Ar����G��F3� ��&��.x�YAJHa
����6-�6�tp&��Ɓ�zP+� dN���D(��Q���ݘ��)N�(�0	�3M�;?���s>��@	�`9X-��nr��Q��4�����/4�+?��"U��v�Ry�N |`v7z$�z�T���,{��-*BꢰD��d�6g�R)<��C�FAkK0�������E]t��LK�$�"�,#n�,�>�lZK��)��~O�@W�@0�5���k���B# N�I��l �A�
|�����B�L�UN�����)�����;�n��s��baWec��c-�==v��`�w�>�rJeQ���nA����&H�H��X���
t@(Tw��� �T���"�*(H+i�d�ZA�!�\�O+^�i�Ar�v>��6WlŖ�E�Ak!�+�1����$K�~!~�	��5,A�JL�G%N�tj@
%p\�p1jq�0S�2 ��a�a��~٭��L���$��������!�A�b�1�� 4��P�l��$�*v=׳c�(֢.�B6x��L6J_�QE�}Lʔ�^,�N@
���J�>��6�d�7����/��ɐ�Q�"T��@^1��r�gx��z���P2���w����GXK��A��,�~a~!�G#.�K���v��}�P�4cp���\ݐ!!R4 ��}�H��a�[�_��,�Ԉ�؜��g�8�F�c����=KQ���P�)����WCJ�4ڐ��a��+q�f�^�	���	rJe
��}��ZK���I���������n)��71�.+h.���!kvu�tu�Ρ�a�����2-3a��6�G! x��Ъ�{�J!F������℠J8
7
�uEAAq�����x5$����}������E��$����!���z�&MVN�������O��("��H%1>�2�XS�ظ�梤H��}( �6ۢ�\���@fTH%70��
���!���jX�
����L�}u�M�R���΁��^e�ކ������VKdtG,A��Dy4n�A��{�?H�o�����ˀ�j'	��tAJ>��@Co���B���j���2�����A�q�D?�E�E�q �1�
��T��������Psw�����7��R�8��T�(;����#LAzԲ�?oB�D�yq��7S7�$��������;>�Ch��Ta���]�opCA�����ᔋ�k�a��aW?�	��Y{b�za�����<�[��5J3����nc��U�|��&��[J��+墤O� �Uva��P���A��a��E庱�X�� 񔞭x�2ы�o���Ң�����5�Px�ZqWظ\ D���؃��Y{䆶�4���q�E��N��#����p�^�A$��@���p�#�!ͨ�.�|~A���-lM�����}�{�\�B(wK�j��J�a��p�:3�~/�@��J����v e@2���;ý� ���]]4*����a/+�����qZ�¶��\��@'s2�N����;gmÙ����v�>���v�R�,T�{4�(��p�}RAa��Q�^;��7�q78�08(X�i�!�+����ie,�dnۡV��U7t�34������������0�)�tㆶ$� ����A¶g�y4oCho����|kl�&$
r��EC�ߛo��*E����^����[tƲ���1�����&���+��Y���P/-�����2-���,���\�;�T��a~mz�nٶGP�I X���fG=ٓ����!�ӡ�a>A��2�`�|dX�ɵy./�.5 �	Ħ����Թ[(O^�q����[�n�ze�4�"D�v�@�#�a�J[	3f�V�*E���P�B��&�6l,Y��h�=V@iůi�+H0� ��Fݺ5ׯ[C�wo_�w���Sx�]�}�Y�4��}���[�)�hQ�$Q��H��$!�8I���@�DA��4P����͟$�ܙAӦO�޺@u�֯�j��@�X3���b�P�D	7e�T��A��A"G&-��C�"K�~����XBU+F�ظx�L1b$H�����d�T�j�ŋ/���t<�,�;��Cς��O9�"�/�h$�%�8BRH���K/���0� Ì1�Ò,���%b�#��b�PB���*�p�SMq[VdE[T	$y�jQM�F=�4�N=���:��SO>�hԍ8v��>���?���a�	��c
8&X�)`DFX`g���g�����()j��� kGr� E6�?C�
(���
q�t�&���\��0�4�J��dI��`��0���8��g�t�0�"� �Ȱ�,��Ds�~�e��A������<�`KO<ֳϷpQT�0���&s	E�H<�*�$2S����2S�B�dI4�X#�=��C.Ȑ��b5X$�hW����8U>g�s�9�3�7�l�M]w�K��g��m���˂i��fIX�� �5�sh~6�@h�y��kQ���nU�@ۑ=�C�"
+�gʥ�t�n%���H4�;bL�^�v��[+��(��#��gl��$�"���L~k�6؜C����~��s�=�8�<�+�&m�H�t�a/-1����
���3
�>�k�H:��Ï@Aۢ�F��W^zhT#P^x��<���:��JQ7�صW^��C<>�<�D���)s��g������֙j�1�{�^��d����(6����rJ*��>AOi� D"�k��ׅ,���1�1LA�G0bX��H#�e	G$���E3������bl<����=t9|8�[J����JC��ڂ�D�V/B�v"�0�1���t�*��)�;�������+��i,�{ 	;�:�f4C"�^ɨ׎���H��#'X��ьe.s3?�	4�a�P3I�Y25[���;�}wEZ�lvÃ+�O�@e*��5�\h#�c`�1��դC���\HT�t����"ܳ� ��ʄ#��O!q��U8i�'Z7d�㰇&�Q�r�(�0.wK\�&��.d�XT�Bb����0�_5
EPb$ρ��@�
P�Dʓ���rGء/z�6��f(��P��D&�����(C'D��|�[�T��� �|�	����Kn�w�3��ʕ	P�:hf(����	�o@�(�c
e��ƈ�S��h�<�K��]�B��V1���G��X�!`:Z��UZ����A��C{�Л?�G](R."n�C��׌��K��>�3�Z��;Q,�x�f I=PX�p�Pڜ��X6�Qt�#�8G\ݘQ=�%�K^��;@�0s��d�[�z�}R@�X:>�<�KX��p>��~x�t��3$���4�g����t5W�>�
VdJ�1 �p
;P�B��P�3�� !�U�fB+쨤D{�4Z!�Q�-buD$A�E�;��F�b�j�po��ֲnz�~��C�!oP�'-��&���]��%�|�K���N�y����>H�l�<�aM[�jz'4�A�ߒ�<�1�zP�d�Ђ�юv�cKz�G<�'�n�0�� ����������l��Y���Z졖��9����(l
P����4�4*�4m�ˈ!�B��h���@;�"(;��\47cy��CXv�靬C�@�; 
S�˄���#�U��tP51<�i
���&���M{���H7�!���V�p�bi*�y��^������������� �F�LÝN����*�ϸX=�ŏv��5t�=��Zz@4�Q�6��[4w�o&�$5�fI��335�����>:�g�s�Y8|����C�_����!XS�6o�DaUء��z��e"V^�%;���X�#|H�FrDd"����֍ьkiը��A���!��}�>��c�h�7>�o(c��N�⻃.,��B�BO�0O�\�E7yQ��`�/0�: (�2� �j�H�SW�g�4��X+j�IM<ߣ���[�ȏ(����!��fL���D�������٨3��C�%��#��B��=���`8̉�Il���t����j���萆����m�%b,�����*A�q6�:AD$0�9G �<��&˂��Pv2dMbˆ=��,A9uWw�b3��R	�R	a >��un��X��L��	��5 y@�Fry3� �yD��P�<Ր%g����Q�ze�Cw�]����$��Y��>�1����g� ~`_h� ����`r�G%�9�r��F|fѐ���������Tsi��}��5� 
�@p�X�K���!#��E��*m�	�ѐ
=�'@�	�Ls�*lx dWɀ��Њe�V��,i�m�m�A!CKuW"yx!��0�^0E?A��Xn@Eb�?�#;$<���I�]k���0��0
1-�`C�F��32������=h�p�(��E���w�T�g��� װ��װv��ְ֐�P�d���'rI9T2rҐBx�x�
� 	�!��'��
t@U�/�`/���;�#�Ct�t��/�,��T�! &_�fuNe	�PbfW ��P�`����Њ�sy�g6r6$AA�##3�Pw��@�1A��"��C�EQ�cW�?>�0��M�:�;�#FeF`?[���Wr����� W� �@��!�`���x�I;���(0�eрZ��C~�׀�	��i�����]�ѕ� �z6p�c�P�M�
+``�A� v؇��}�b^�&
�~uPP�# < ��"2c��:Xt�,�*͠Z�p6�#��9Pu�	�0
�`\�vj�1͐����p]ي	 ��{v�&���π�Z8���R喥0w��9�7ֈ�:��n��CSW'p�uP������0�sܐ	�P�!�� CS������+!�s����L��A㥭�]y0Z r�Qu֎��	����|������~ x�g>Q�������a�%����a����Ũ��
�@Y *��$@'���1c#O�4c�'1���� 
C�<0$`�ru�	��"���`Z���`�Е� r��g�%]� ]��1J�p+]f"�pmW�KB�X��0�;�:2"#��#8j1' )��(�� ��dT������:ma�@8��-��	�p�bPo �q��(_ʏi�{��	�%�@v
�}�
o�o�v`y`tV���Y�	s���u���-��qwh�(i^������X�i� Y *I	J	�@+m���b�7�d"�6"vd8��W���	]��V�ذY髃��k�	J8d�g"�|ԥh������1�Xbs'�ri/��Ox�@w)�4�_���S�7�� $���2X@FJ�^���0�ju��	�j.�d	l 
�P�PS���	��j��;�"]Q�]�԰�!(�P�S����	�}(�L~�_�cX�
�Ў8t��q�%�lQ��W��-� f��f��v� ���
������i��20Hi '#� �?)_q9"t�ś�M����d˯��l���`
�`�`���C���b�֠r$l��(\��</�*�&��9b��dB��b�b�<Y�����_!FNS��{�g8Ѓ�P,�k��9F1��	MN�� �'�f�RECq�	y�R���]�͐�� 
q�6�:��`P#� v��8�6[���̙-�K��f)���Ǩ�kT�� �
p@� Yq ����*,!AD7A-�t/��Ӳ��`�`
��70���h:8ϐ��5�0�� 
>���,��q%���r|�wp���*2��(A�&v�> �d���Xtǃ�?���YAF��D�$CcN��hgPi�P�5�9��sQ	��	��8�~m	��	��C�P�]��>�d�������}@!��o�� ��q�
q|Ҳ�����.1�%��ntWWjpV���3P=m�vߗ�0���U��
�@P�� ��H+@Vc��*� t)��9-���W�]��7A~c8���
[Y�[��Ǡ�p���g��rǌ�n}sǋFH2R%�*A��9�"q7,O����c?�#��@�'p U���� t`@qT@9��;��B��r��[���&�3]�e�8��8b�p��#��{�:aWM����9T-Ȱ����Ͻ�ݕ� ��-a�s1h� P� ����N��!���he^���~� /`�H�  � J9E�p	�V
 ��6�q*x�=X���	���"�pC�����X�ʺ���p	z�bC��|w����1�(�w!����]��:O"�@T�D��O�WEb���C�����6��y`T�X$�]i;E�E� ܡ�� ӡ� "����P���8�k��']�ƻm�6R
�r-��\�ЬX����
�p�Ce5qlr������m����=�I
�%��&4�h�&�D����B�[ɝ D*� !�0 p ��=�D�P~t�/a�� v�������
���ÊZپǠ)V�у��Ȁ���aW�wF]�<�p[�<u�1q7���.>��>��5O�ns�4E�%��l ��*�s[�dp�4��
���
B�X��P�$�W~��e�	Ҝ��8t��Kh�~��Hi� #p4a
q,�p#bP
86�F��k���Z���7�(��eWzz�-b�}�����EPN@N�Aͨ��}޷im`��ɸ� Rm�_��J�"ʯ�B��faA(vu��������s}���
��}�p��AH�s�g� ��,�
�`�3Qk)wז��)UuO��7<>�L9�x��W���UZQ 	�d�n0SP�����.�D6Q�)�<�0�����M�]&]f�7O�8����	brh�r�$�j��n�u#AA90�"#�
��+��ɰ� �`�P�{�Z�%k���
P�hI��� �OB?��|� ��s��H� ��R! �Ԉ �W)K4h�W�_ŀ�c��]�s��a�v.�d՚ak����w�ށ�e�.c�r��+��d1jĈ�Iu��m�4�3�Ϣ��4�<y嘎����۸a�X�T,KnĈF�B���$�S(S�؆r�&M�w������0 ��;vt��0c�daE�+�,fժ���9���Z�t\;y�2ã�o߽{�὎/��ز�Ż�Y�$"��J�pS�D��J�'K�٤�a��X1a+���a���
�t�ک�WO�;w�ڷ+��.(I�8)b?I'���g&_taE&�raEPppa@�`� a� ��a�e���Œ_F�e�b�a&)zԩ�"l�*¦�d@�h�s�q�\��e�WJ&�df���f�)����&�&�y'#��q**��i�b��f��1���
#��y��PJY�O�x#w�� �+�	���j���1cr1�1��)F��^�>x��r��<�4� �*�qL�'����d��x�'>����^�E�Da3<6CYK�!�����1v�./�
c/v�AY��+����s�i�#m�M�"����ᇕ��a�@Sr��Fa���� ��+��ǀ1!E��a���*Kǝu��������hl�H���9��cRa�c�@��Ɲ��m���gT^��g�j)z�zʛ�q(�^dAH�R�2����HK����7ژ�zN,��b �`���`b�Edk&�tP�G���s�ŝ,ҩHm�qL_`RĲ�\{m�P�I6S���(,�A�Ɇ�%�����O�r���Ye���!x��e�69�.���sƹ;�\�ܖ��}%c:E�\�A���/@P���Xb(DK�Q�K*���f�<_g�>����RG�&>�hz�E�/�)ƝP��������i"��Y�HǱ�UF�/��K+,���8�B�ȝ��4���w~9@ P��H���Sb���3���:�!>���A�F������0|�(O܀1�v��l��̣��7F��T٠�h!�}������"/���Yvp� Xdi�h1x-u��Ly�A�X \E��}�~�vʅ)X���`p��� ��>�D)L!�a� ZY��H0`l��B�F6�=�,,m��E��N,d��EM�đhHlb�daP�'?��r�8�T6���ei*�	F%���^�"C�SA�S&�����a��A�zL� �63�W�R㹚����f)�F:��k �~���fXC���4��56���5�h�Q�x��2�p������ɡ�X�B.��]��Ø�f�`�0E3�IQ*wX�~^'�8�#@��L��
Q�����D��
�`��#�w���Q1,Yl�T'�S�SC���3dCJF3xaJSj5$�0�=S�$���I�D�:��y�)�XD�ae�*;��нd-�8�ւ���%��_� kI�G5�!Y� C��`�9�AKw6i���C ��Ғ�~(J���R+3�r��P��Q� =n�E&�aKqB!���+������b>�G5�E/���I7�ClE�)�����	tL��hG(c��5��B��m ��H5T�R����P&b���X`��T���ʉ�H|,�	6\���obX��*kT����$��3ҙN?X�1��R��Kz`	�d�0�!��Lr���P���!G�%�'�@A~�#y�S`|�'Ќ�O�43�&�Β~���hI;�-�a{8��Z���	+�l��(�����Tl�7c'�}�%'��5N����,@gqy� wӁC��bm��x��'��ɨݠڛQH"�5��; ���E����L\���FEވ���5�Cj[�*���R|���V��U��HFY�ȋ���Ғ%�B/E�Nu�3s�v�J�3HƢ��3��&S���ʙ�]�Q������d/��{�E�A�����k�ߵtg ��.���{��PblL�ti��g;�6�3�I�C��<[���\(�zY��5�jI�2�ԅ��Rq��.��H�� V���? uHM���+̐�?d���8�޵����fdal��NzbC�)���zH�Ml����(~xvi�+��D���_j��Z$�x��4sۋ,�Z6^:�C^@@�=�TA4���ol �ذ�G,�@��.��e�l�d➑|F#ۇ�����Ls� ��I$�������Wb��X��`m��< �~
c���偈�t�p���D0�K�ђ�����+���:#"���R�b�	d:CV�Ju� T�����$	�A��
��H�g ����C-���Թ�j�����0�m#��X��+�Ag:�Q6 <u��6@oH�jH��@e d���ۆ��v
�1^#�Nj�-ԁ��y�Iy���w��ۂm�] Q(���3�c���?����ځ/����苑F��8��p�Jt���h���x@P��������ѶR ��0q?�X��@D�����W�:��%_�WR���:\�gx�g�<���˸o��0�hfP�`�
{��H$����� �ۻ�O�HA�8�b�H2x3!˸ 
&fh�Ĩ�۬�"�ix�i ������Zyå��A$��x���IQ3,A����
�]���p��7�+�(�c9�!���!��4.�:�@�>뫹:t�˅�Y��#Q �6�)�BT�8��2�T�R��� [�Lh?I�?�A�E�5��4S�:l�{��_;����cH�]#I�W�<KE���W�o�!zH��q�(&6!S�o;���+F0��Xgr�R@A:i��ē��P�AԉI�JDC�RT,1@��g�!		˨�ʘ,q�K��y�˘��A��x��<G���(����Y���+/�!��
/0��@��y.����kY4�a���9����9i�ف���2�6��*����6(��t���N�ِaP��R$S�)���\�0�ຫ���;���d�W����SE��*.�Xܔ���KDSH�I5�d�@�'�xYaI��c�d�K� 1H�ԑ���`�'�J�[��g��R�Ꙭ�Ad��Z����P�l;W�f���+C�c��|I��@�]�Y��P7���(���9�4�:`Po�͇D��P����p�>qIv�M�)YH�؁��!����X�r�Q�K�!؁��F+���K�ɚ�]��������$�N�*�䑫J�T��+E�iJ�,��1k��i���.A�|�c�
�d��	eB�[��Dr{��s��`F�P� �uho�Eʹ+�jf��RQ���ZT�8��&&��K�Q�ӻQHK�\U�U��\��� .6�87y���(a!�D���"�)���ψ�T�Hq������+�؁肁�p@��(:ʡ���,2f�`��Ȣ,��d�����0Y*��h���T �V�c�!a�Z��9����	��t%l8�T�%�_]Ւ[�A��60��Xl�Gr(�!�ϤQPPvhPf<��5�9c�����
���PAA�2j�[\�ۼ������Y"��
�p���G"7S(��@�Ppr͵`�P��(_��5��/H�00��Y�k�c��T`' "H:T]'�`5�`(�����	PP�	��!�X �S��X Ӌ�9����.���(�����^�uyҽ�����5��]Ns�6HY_�4@38(8���_9�_;HQ`�h?'���p��A�J����1qD��������U�������p>w�2`xV}A�]x�] �ꉲb%�$���ɨ3Y.�W0YX�O8��2 Y�T���j"��x68_I�6�(��C>�[\�,i�fȅ;�7R��h H� '8~8�eh��X0�P�5�]��^QR��0�X2e�;9���E0��lP �10Y6���D��х6�H�c4��5�HYsU_6XN1@O6�+h�4��5883H�TN4�_�1�I;���u�.Jc���0j���Nr��{�U���`<dX��l��6;{��H�7Ul>a����r��H�څ]�+N(������m[����pUV���3�IWqJ.�B�f�q��vp������h�"��h����K�X08����/�X��S]�e�)o���bKe0Ke)ōd��P��f���S�B�7x 7� K>�nMf��f�hNcQ`�7x4H(�_[�j:��6��6�".�! ����#���A>��(1��R��PPlXƨ��	�e~�)��;�� ak&�b��Ĭ�ʰ��t�3�ַ�E���~�L5S�3�R טFZ@�Xq܍S��#.�K�uEd2��\c��fz\�fƟK2�*� p�"��.f�@�9�?�0��m耢��(I�lS�( ���f� HI[�1`�!`����+ȅ! W`��:.Q�q�NYh�T�\lؑT@���Np��0]�7x�7x;����7H����e��a��I9e^:m^f��n<~.UU[a�O�~�	`���^	d�j��}!lk�[@���z=�I9��ilä����/]I������&��`n�̽B�}F��"�1�"K������lc���`�oah��H��JR� ���
8�(�!�ᵄ�f��]��R�)FȐ�2��	��c��ƍB�37i�j�if(fP�!H�r�_Ʌh/Sl��\`c�c����܅iH%:xd^�Z`�Z��i���O  M�U��X:��?�� ����Q��;�h� Z]F��	�e���,`������q�6�t�'��zݮkіV
	��k]Y��h�t��!&���j�,�(V���~���0l��31�.1l&e�7 ���h���C7��x��5��8� $�� 	� �^96�(��yw����6��;�X�x.�"���O��B���^�P�� u`� KY��!11��f��cx�ʅMY����z�h��E*����1-j^α(Ιh�g@����6�*>�f���`q�F0xh��
�r�6t�m�fnBu���]	�`W��Ez �A��LgR�!���f�������`���UWҌ����hf��68���gh�o �TׁP��G~߹��t�X�ށ8����c�݁ `�͍��!��V��7�j6��OG����H�\�� :��^�����7�?l��Q��S���2fi����!��!��>`�ร�G�w����R�R�B���%��1�1`���pF��l�8x㪍K7T��W�d�T��UKW�\5�ؚacV����L�e6�(��D�55v�ە�d�\j�T)�z-�}�<�D�5C�˘�\�^�M�2�1a���&�6��|��)�)f�F����4P,Xhк,�D�ߏ#BL bp'`�P�b�@6�،*��Q��aP7��:G<�K�݇�0})2�b�k?��|��1�_~�R�F*��3���d)0|�f�p P�A`�T���=�IBXt��@P�y�v#�X�%���(.}"SL�䴓N5%P��67V�S���3Az�2���1�0#2� 6VZ���0dŲ�,���e,���%����e^��D]h��W<���c5?��1K��d�E6��\�YIb�1�KRݘ�9�y�L.t$��N������Oh�i*��	1�`�}��2�4c�)����'�!+_�Yk�(�Tʕ� �XwS6�$�L5�$�X.f�'ƃW���E>x�ld&�b�'�o���P��F�\��҇���F�Q�<�@��yđQ���4V(�J
��D#�<Y�27vc�_�̗��5fY�y�ebn�	f0_-���e\)�&�t�U&Z��ٔ1/��F0���f�p"	斮��F('��IgHG���F�����NQY	iJ+@���as��m�M B!�����P*�B��2���J�Z��%����/��T1�0ӎ:��`?�8�=��9�$�K2�䲋�x�y.��q.�|��1�(�.r�qFy���r���n|!�K�YbEC� |��y�/G����f�YIl$|�k3`jώ7�3�:�q�[2	�ba�B��b5Y$S_�v��Y�I˘/嵿�)���b\R(�&�
T�R�,��y�Tg63��@F`�ETHc�q4��j�Z)���/�?x��	�@���6��ANJzz!���2�«Z��R�n��0�q8w�#��x���!Eu`#V�Ea��]Ct�K�E���)ˊ�8G4��k����C �Fk�q~�-\��H�E/����iV�ZD(�x+K��ʉdJѧ͔bz;Q &/9)7t�{�J2�r�/�l)X������,aY��D6&�\	�d	��4�1�"Mz�e�����<�PT��W�������D
2��~��h�b��FU�h��`�T�R-��& u�0m�AN�y��KYj��E�Aa����2�W��N�~�/�1�A�Q��:��,�)s��"/X��Q4s�R\���<�Q��8��8\4�^���X�*�BL�[K�'���3�E�����]R���(��C���dQ��=	�˅1���\�t(�ۯ��+[����0������J�,\5�����G`C��1O��"ô�	�$�K!��4���
\#Y����
��uJ@�i�j�� *��>A'#5�W-��`[_�� F1�ьv��1���o�1Eb%Ê����F�����r��/�q��ZjdcS�Fk�T�y�^��c�K��tJ�$��cDY
Q8G`�H�/Dh$�`����t������,~��}�I�,l�`�=L�%��b	.�KY�)]拖����(� ���PT#wmS�'�0�fI�JD�Q�9��\��=�a%H�"��׼HR*ЬB��ϲ�V��ÃO�	=t���7����\1'�t�V&���;�a9e�q����yQ�j๢��+��)��5��]8�!-E�iA����j�)q_��I����q�ˢV�&	E}�+����À�<�Q�Z���tjҒ��S0�d��y���J��w2S�N�%����^P��jLX�(�؅�2��Š�3�kZ�c�'`h�(���WZ���]�5׋�6���s�H�]���!@�ߜ��'V��Q��@���&3���M�-��_lB���>X$�x���w�8�r�Ёq� �h3�%�A?�h㎢���-(-�%k,�V^��ܟ�>3X�uÖ܉�ʚ�:�TG`d=VZ�d+;-���8������Pً9�$%x�a�V���2���
U�b��Y�qݚ���V���K��G"�3�hFQ�M�x0T0�d+���`ַ ;  GIF89a��  ;31<9B9B>;BL>LX8Ju9xKM95f72L<FF>zw8Xp;m[D9Lv;jI9pw:TNVOPtJ{OVgwpSOnPoolUrmr6O�3Y�<b�9l�<z�P>�KU�H[�Sk�Lq�hW�no�lu�H{�dt�<�Lx�8R�UV�fY�Y[�dk�Vm�kg�[n�k<��Y��U��\��p��o��q��w��L��]��k��j��m��m��w��v��y��y��{��m��z��|��8=�59�:P�<i�8G�:d�L8�t8�Z�G9�m5�ON�Pm�rS�nq�MP�Ri�jU�om�1�19�5�):�7E�9c�A�.C�M�F8�h�n3�I8�u�r6�MN�We�hX�pm�NM�Ze�hX�qj�U��s��|��]��w��~��y��y���6��<��:��7��V��q��V��r��R��p��T��o��t��Q��tؑ͎8ѯ9��7���5͎QЊtЯOΰm�L�w�L�n����6��!����5����+��P��t��T��u��L��m��I��p��������������������������Ć�Ƅ�Ӊ�˅�ٖ�ė�ӗ�ɖ�Մ�ᖼ㥝ǭ�ʮ��č�Ƹ�Ȓ�˭��ݚ�ǖ�܇���������Ԥ���������������̽�����ё�͚�ө�˲�瓌雤驔쳭Ⱥ�ý�����Վ�ϳ�����Η�ɴ����������������������������������������������������������������������������      !�   � !�MBPW�����ٸ�٬ٸ�������ڷ�ڥ��WWWQQFWWEWEEEQ?����������PD>DDDD>P>>>>=>>>=====���������������������vv��vv����v�u����o�oo33333!!!�������������������t�t����tnt����ssxrssm222�2���,,,,+,,++++,�+1+1�++++%+%�+%%%   ������������������������ ,    � � �f�ȑ#?*Hl���
�H0���X��D�)�H���!y�HD�&QtIWDbCP.$B�fO\m�4xp�ÛH�,��aQ�3'6R�!�"'[bU��+΁S�i
��˖���$Ǉ6�C�gZ�_�
��R���!��U�ӳx}�4l��a���=�k��,c^̜�r0��p�MZ�,X���V��7�ܘM����g��M!�,������
E.Ό4�B#"�d���"%A�M(���%���t��+E�&�ᙐ(��J[�<��,ɳ���}]�K�]5�]rpHQ~At�SF��>Mf�a7��|YX\}�FLU��cN��QK}�Ox��W��2��,A����DA�&dlD�66Ѐ[�e�G�h�M�9�_}�D��TE&e$_V#�$bWcu9�JEqJ&A�Q����\�DV�7�t"YP�"t|yX�N6�Vv%�gXf��,4��Л�!p���==�D�3e�.!�!���
`���G�N&��RN;�g).r�j����=у�@�l=���=�p�.9���~5RTh��x4ut!`
��%�,&�`E����Xyz�yf�ey+>&����)u���E���G�qE"ZD�T�k:(�f'oA�6vS.ʹf���a�GwQ�qX�Y���:1畟{�T�^�tR���{P�=��.��H.����2m�����l�7��,n9Р$���W��xsG�}|(����r��'�Ai���	!
X����C�x��+��h!҈v�+��^�4_Q)m��]���A��t�|���B_hLt�K�eY���~�W�*�a�U������,�^�Y��{w=7K��A���!u�@���Yg=�ַ-�9Z"�����Q-�=�C5ɇ+���k��Н{x��9w�BBa���t��j[tdF>%�v�:̉PԒ���]�I� �Y�e��"F{x�b�d�R\q&���i]uY�^�!�q�\x<u�[$!?���v���i��3V��"�@zH���v怎#2P �������"7���!Z,��{EĆ�ɟ]�$��,����:��D.�������愭K�RIMB3�0뒉L�4�!gV��p<E3�x�L��~���ٽ�v����F+�H�I�ֳ���dK�cӀ���9�y<("����%�>iQQ���@������w�M��9Sd��#|'1�����C�)V�ف�3���
�H0�!��0T(��)�L^eGN����6SHM�+9���y���)��ƢL�}��O�h�A��]eb�(�"/w�? ^,�4K!J�Y��%��U=�\+,l�T_�e���e!�s#(_r�ĭU�z���\ 0��[8�ųE����8.;�D�QIfJ־�H̡%æf����l
zϨ�*I,��"���_�bD���J;W2�iҭ�K�]g��2h84���~�Q��D��"�z f�Y��eը�5�D�!~b�At.&e�E1��L��!ȩ�J;��dJ�Ah�DP��v���v��"�:�ԕ�V�~%�k&�w�Ei&�Vs��8PA	Fx?�L�9�U�< �.�8ᕰ�9�y2��m��6�Am4�a�y�)ⳎH�T-7^h���TEby
	�����!-Z	�
�@���������K�Jōn���l����%kW���|��Cr�D��-��
aCB��5�{��-���r��R����R���RȐב���x櫞i=����ŚcI6G�����kZE^&�\'���γD�JB�yEQr"54�UQ���X���ֈ�2a�D���Â�2����&�UIR	Z�L���h}*d����D!&1�VT��p2��d�b�@�-d�@��S�P���:f����Q&[6�褤���[�Ȍ��m�寰�����{�.�5�t�"�fT�c�>�"�S�v�?	t(}u��rMl�<e���ޔem��\�3�+fJ��@��f�I�u�xD�(=��C�hE�%�IH�`C���+�!Z��*ъU�"׮p��Qd[���h�$y��G���mr_[)T!�yEwӮ��P	��}%��	��+4/�GABV����i��.����d'N�^j8�3���M]�=�E�J��<a#Q�[��XG[�D��9N�%�f�3��Rx�I.M�����vy��rHB���І��!Y� f����|��F+�a���<��pE���ka�RJ�QW�qkn�T��l����$<��`��h�����MK!��y7����^�9� ����-&�� �!2�DU�SG1"d("@�3��o��qq+g�o�2!զ1$!Z�&#.b(�'Aa�6�Bj!vޥ
��r��z���0�@k�0k�	� 	�{=	l�{Zp{ZP{Y	77{W�sV�
:w
��s�t�&�0� ��������[F M/�E@g!��(5~f&�X%QvGnH�4@"j�E`n�D��D/!��-7s�%1���t6T	S����1 tE&�=OeL��L�FCl�7EC�W*�S�UG�3�GkX%�a"DN�a�0
��	��	-
�V� ~ � U�-�r��r�v	�6	�	�Ѓ�Ѓ2�W��@��$���8�H@6�*Gk��-�� ��
��](H7�(��d_G,)yq�3vsf�n�fh�v'�9 |�$J�$$��C08�aI�dEd���\Gp��G*Ur�p���]X�/�G�-I�8����V%do��&!�Z�5\z2�v��	�p�� 	�P���K)P)�
Ǹz,�zX��h�̨��P��Px
s�eY�V��V����TIk�P�p���
<�
� t�'t� l��0J�S"�"U��6��~H�	nKD@��̲,��KI2g3�((a�%89e�Sa
�j~�e$IM|�61G���(>�6LA"����0P62��6�(�$�1����0	qɋ�0�0��`ԩ�i��0N9� �@
��r�`������| 	� s4w�Y������ǈ��P� �-G��
�`{�7s�g�Հ��6�A�|ʧ
� ��@l��6Ny�/B���m�F���4���n�n�C��DdZ"c�G%w��� ��@�j��W�^TU��8/1�+�xA�./�#�E�eQF@F8gt*��~�l_);
;���Х� ������� ��p��@$0���
s`ҹ� ����P� ���0�0�z�� l�R f��l`R��fО�*	�		�0�Ӡ{HHs���w{m������|ķ
@t���F���0��'�=�DC�cg�9"Y$�fQ3ѳRUS-��BX�^Q���"�qSF"�Ra�&z!tUW�3��2&�RT�]9:I�*X0J/g����J:�p
۰]�� ������b��ۀ
�0�`s�
bJt@��
[b�
�0��`����ٱPY�8	0���		���ꨆ
s��s:���8�-	J{�ws@�mS���?���h� (7���
K��nnn�#,8���f��R�KHT56�Y��P0D���Dq���S^tM1�M�V��(�)e��W#�$��&�uZWvE�ζeNw��@�p
�p����rd�j�q�p�ؠ��K�:�����	KՉ
�@�� ���@�`��0��[[�	��P��ڞ������W���ʸ�;؃�:j�V��
e)V�p
i9���r�0	�P���跫8R_�Q,i�f�!mׇv�P6�A�Bw�lA��!Ф�y�BФ�&�P۴���?�).�9T�%�F@\^�ǿ/FK�22�A�]81��฻�
��	d�g�b�^�\p1,\�n�nmp���������@��b�
����
���`� ;�ej�jگ�;�ɧ����y	��	�k�p��w����{�0`~�
��VP��[��8�x#�nEs#��~����o6F��5��1�x���fM��\����q�b%U�%*�b}��v��I��G���P~A��\#��
��
��|���g g��^���pШp@fpc��n��b j`�j����א�������@�j�g�����l��K�V��V���)��z�y���� |����r0����r\i	�`�[��ᡒ)���#��v�4�5p��2�x;���1TG <�!���qa��u�!#,4;Y�շ%}yZ&����*�\u7q�$M�����
�i��@��<}��U0~��H��`	��n �.�:��J�:�n�Tm��|�נ���ݰ��|�جͷ�����k�ӠW�I�N�AP�
�P~PO �I�@�+���ڋ~�rx|f��c'��{�� ��nw$��D�U���D���v�qo}+^ͤM�C�'Z�OǶ�=W=]��
�Gr�2
e�vM�!^1�X� 
�M�f ,��i���0VPj�y�`*�؝�0�P	�Kp��p��K}�����T-_P�j �k�<���,�a��i����0SpW�Q@Z�s��
��
N   p  � 6�
�p�`׭��d����r�׫��m�n�ȕ�,�Q$q�$8���1M���y����)��ʰs����¤ͤ�4ζ�"rs�\�6���E�Uj�e��E�0�s�`���ܺ��`����0���j���P	�	� m0���X�ݗP	�\p��p��K�J��.|�np�U�oP�Z�����ڐ_�� p ��p
��
��!�N 7����m���.g� Dd'G�����9��A�ؑن|IB��L's�Ng!���)On�GA:O��]�Qο��*��N���.r��O�˞ae�-3M��P���
�0�`����-|����U@
D���^ 	ڐ׀ݕ���`�l��P	m0�0��09\����p�[��Q����m��=����H� ����^� � � VP  3�D������L�p4	�8,���Q��Q�@��"��w�e�EhtA�!�/c������˺��Է��ٌqa �Ж4��a���2-Z����ʿ!���$���
��d��\@	�ZZ0�~��P��j0�0
��?�����p	�P�p	��R	~>��	ڠˇ�˷��&��*��ɬPY��:Z�4���"P��P�0��@
>�E�Z��f 	3W����=@v�a�9�#U���Yg�a`�q��lF9�������Q��"�� AL�1�Ă<b���#Y1"Db�&$��H1�
�t(1�1#!	�\8D�Ņ,�|ɰ�ė39��,R�v�Z�J�'N�����3Y A���Kf�T�w횶k���˦/��i��]c�M��Jn��Ѧ��8mi��q#F�[/H�xq������V��˔)�"=��:D� @�F�
ֲE�����<+x���4�6qȐy�F�%s�z� 2d.\�t��{V�Y@rG��7xܸA���C̈i�eL�S>TX�F��(
<&�XD�0��T���0�#,X~ �!�Ӌ$�;��馐��i!�"��&��Ab�P��B�Y�]X�K��ˍ6��g�zF+�5sC�¸p��J�Q1Ѵ�j��Ƀ7�q�q"�$��Bj01�L,D���ʥ"9`���g�$����` 	(�@ǲ���$��p�A^��"5�P�H$����B7�x� f�e��d	�X���9唃�n�a�BXP����Y���t�����4<���h;��n��Lȡ�X}� ��Y�%S�Ș��q��V�5�XTa�UF%�V\ie��@tC�}�f5�hc�6�H*�t�����Z�+�H��J�8��c.j�f�Hl/�
Cl������͘����l �

H��6> �
��"�i"�&KV�� x@e�i�0Id�f�R8�;Q���%�I����B�!�Z-��؛)X��ｩI�i#��fP�ZY*�$_�Ӊ�_�zj��c�Y�ҾI�Y�}	�!\E�VVi�P8�d�/���}����T�5�&6@dq}�Y��,��\ī5�K
����/��ÛS"�`kH�ؚ,  �'����BǴ��TPs�X�l�)�ې�s�4�|��7�¸���a�戮N��h¥Ubէ����#B����m"@e��O�֩��g�P��6��-�<S�PL��-`���+��
Ix�b�?�1�5�Nsd��h������6�u2<�gX�ù���n�as��-ea�pN*�-haڨBp�i|ff����������RTH�l��$�W3 !> No�X�Y��8��(E4�؀$��b�b�o>�	GT����@�	F�ڳ���*?�)�J�E�8��C�ܓI��d�c��7bDr�]ˈ t*" c�0*X��9H�d8�5���n�Ig�
��!�/���Ȇ�0�~�c-$Q� f�l�a/E�C���p�'��⢴9��a��ÜC��Nbe��� � qd��8*��Hom0�.VC�X�j��!	�Tc7�M�z���쬍���)�,��#�̷ ?�R��	�{9J�lm6�J*���@�Y�sJ6T�R"Z (e* 􌒤��(���f����*N�TH�o j�>6؆�l��=�����A��B�q85D�D���R~�c��(Q^�����at�G5���3�DI:��@�I��4�L�5�!�UC8@>��A�
����6L/N�Y*19H,4�{h�г�錢��F� �� |�t��4Q��'�)�NY �jA$%�þ�amig3�Y�P���\S����ҍ� �)P��]p#��x��pz��,c���:�$$.g��,���̣��K$�:.|Qmxp$�q�i�pڠ��b��%�\X,$Z���D$�7�v����Fg+� �+La�D5\���4���"�6P"�{j�@{ �5⢶:�M�����ѣt4Z�����~Ssiq��ܧ�"��d�R�4��_>)�:9�R��N�N6�.U!M*Nj*���b�P�,wQ�Q@btd(=�1}��ڸZ��}`�qkU�7S�:�� ==�zV�����X��0��ih�
��k����i�]"Q���w՘b6Rl�)�n
�B09��4Ax*�Ad��I�P�l��F(��PTZr~���dJ��a����t�TJPQ��?��&t.�ғ>cq#�w���4��]��Ep��Y�Ħ��n�Rb|>���$&��B�b���[�85\��{KF�n`����ǹ�0���a.���+}50a��4|8.x�����5��kӚ�Q��Osl� ����y� [b$��5�P���ָA	DP�:��>�+�t�H\@O�|�F�����.���(*ǣ����Ӄ�;���N"W�I�(�z��qO��v�>��_(G��/k^Ay�"�q���ב��x�<���u����-?ԗ�D����/ѓjX�0���帄!�w��b�<Lcpb��4�a�f�z��hM�v�A��\�>��h4�ab�Q��st@�
�������h�U���ɂ,��X����9� ;P,;x�W��j��A �I�����!�����B<��(��2+c7K�H�	*��)�Y��@�ڛ������7�A$���`���cP�)��$=c����8���Q񈗨��"8��+���K >2�S�z����iX�;�;�-�-X!���q.h:/�&2����}�l��~)����y�>m8���/C��k!2h:1�&PP��p�q(�qq�'�̲ � �~��U���� � ������M�6;�6�(A�	U0#�(�kA��X�XX�� )�M�.��<]�$��$.C*]Q� /}3.�{B�+2�A/�)+�J�7])����~d*a��G�B�۸`PR�4Q�j��kj(�9�H�VȜ�&���0����X�C�"�q�{��k���H��+�1`�$��V(1d8�SȐx�1�]���i��qpW�T��Z��_�`M�Tx�ɉ+�A��$�ș�2��X��*�-���K�<
�x�:SY/!{�G��S��r��1/���ӳ/L��B�D����(��#�`ѽ���̛�[y	���R�c8C%$[ �K�0�@M�6�W���{;V`n"'�ܡ�Q�r�sA����lp5��X�mR
���#�M����kb$ȂV�v���A*����T�8V�Hʠ����̀� ,P�+��l`�,��,��B�1� "�`�����2i,�Gi<倅�#�<�1��(B>�a�z�=B�k̪��l��k�Y���˩zD�c�������[Q2�G�[�c��=�G�H�޳�2a@�z(ʢ$���m`T��m0�y��6p7�����H$ٜ�h�#	�p�&�����K0&q���å`%�W�6{pE����@Z:�9Յ �m�T��Ȁ(�(�@T	�N�W��Q�����p[�qS�Q��\���t�2��l�K��S�P��P�)Hr��7K$�QU���A�L��8)<$���?S T�	�ڣ�Sњ2��8��s<��	�����T��*V�B�!��I���P	�{؅m�*�dR���̆�X�mj\j�*-��p8p�|�!/�h��0��J��A�/P��4Kz����S@��]��Bn�S@���S O:��=� ��$Ȃٱ3�=�P`��Me�L��*�-�:��L!�U����̓Ҥ����*��G겜@B�3���:̌[ F>�b�.<=y#�0���՚J'��2����#`	�=
�d���O�K��K�4��a/��MW0�L�y�A���ȩk׮��m2���H$��(�&y�&po�����rՅB��Z@^�Sϥ�VP�m�@ �l�@��:]�$��+�|10�
P�W��?���	�[Й�KD	�BK�XА�-�	���:3�h]���=��V�-�M*t.C��TVd-�d�)���%_L�ot���a	Cw��0T�04��L|	�]L���ŴB⻅_�D ���z�x�z��mP�u��{��J��{�R3�������\i\��H`8��;y!Ǳ�;y�;��>D�AP��(:��M�W�"6�WЅ9��:uX]����XP%p�(�y0��������@� &�D9ˠ����F,�<�{�.��L�`ZN֤r�@2��b�\�PyK�˔K!�8��eB��74/Xe*f�.�	�}Yc�_�-�[ Hd��d��d��ʅK^YP���SP/Xx[@JXb�u��yhR�[��1s��4�[���^�es ʟ@U��t�"dۛAX�A��j���,KL'��u�$=�%�  �%F�]OnH�W8�Z��)P$p�I(k����a>�bP؅�- ����]A�l<5��)<�\5V.`�3��Ѳ�Z�;���V5_�E$:���l�K=e����;��#��=}P��E�a@��F`p���UH��>fZ�����v���Y���F�YZj؜�8} شVX��C�d;��UXj�k�i볦��~�8U/�B�=��%T���te�� �WN�VH�T8�T�����V0d�:���*�W(��r��9(�Zp��,����B��a�Ѕ��
H�j(��/�-�sȁ�O�$c�U<�HPǻY�ɔL�z�Z�N�8�iM�UV��PU��ө�ַ�H=��^iE�C�=���a�_m��a��I+@�*Ho+H�[�i����;��;����""���N@���l��:I�ٝ�,����5Nd��c&��)@o+ ��Vf����^j��X���?��B����S��Tx�v�o(fx�H{�#*q q(+�X;E&�'�S0�9�?��;�H���/kH"�V1I��] �ҍ�e��7Y 3F^��(�l7�`K��c�+�Ui����-��4������_��Z�Zam���+�hdEߠBs~�G<~PX�Yc(��&p*(�H�+���0���+��+��A(�6 ��$���5��Ah���iD��sy0�T�~z1�k��	����kP���o�*�pZ"�,�V��H�FUX�kb6fsXs��z�fk��B%�T��]@�dwb��񇍠<� sP5k�("�����T�(���d�\�r���-G�i�-0_KLA�B�ǎs�R"��.nb���Ts�,��5n�vQ�>��Q�b=���@;���x�&�Y�H�ǂ+H�kk0��I�#��@�,�ct�U���'�y;O7"�����:�k����{p���PWM	�\��9P
�������L{��pTpv�~{�2�_WW����p`P�_hqc�%v�wm��o�fq� s����"f�xb&v���d?��T%�`f�֞��V��HY2���O���)�.g�5^^�̔�c��	�n����d�#�8�ެ�c:Ss�%�����C����P*yL�^JF�O��a��Кݙ�"�C6x�ٍ&��Bp�_��+"��', z5�o���โ���Oς�oJ?�{0���6n�*���.W�P�2xCD�#Ft�J�.d�4���K�/W��REJt���3�j�	K���j׮W����jĮ���qs�.T�PћWo^�?l�@*���=���2kV�aƾ����YX�`�Ŷ-�@r�Q�^4� q�X0cǈ�%L��a� 6�x1`Ēf��ab�W�8�`Ò�ӌxq�̢S�e,����U��|���׎�9�-��0ϯ7�}�թU�X��j�H�M[U}vjӨY�fM�5mܭ�/o��6k�ɛ�6�=���\�7�m�<d���UO�9�C�9�h�"�:���9��RA����*����;��·:"<��s�B�h��+�DX�c�r�@����
��R�� �H�	}�*%4KN�3j\�G]<��^�5�-����Y���,p�E�\v���@6�Bb��o��Y`}bY����a�	����f6j{z�a��gh�y6�l�M�ۦ�y���9ܤ�ܲR�z#�(��R�)�|C ="s�6��+����k�����=��S�9-����s�:ŪӬ:��9���뭹�뭷�{������!���3�� �*��N�贃4Ȁ�4��<�̣E�����7��wKV:��Ah�C4�@�>��C]1�x�����Ȝ�cA$���=��c�>�\��)xը,Cx91���3�k�"D�Ks�U�]8�0_�C�_톨�W��(�~�Zc�Uڧ��u�(a��[b�چ�b��])hY_ݛ�o�:�1�䳋=ߐ��B�'��R�9�p�K��2��R��cN	���R�ǃ
	��st�s*?���t�T	�4�+���#l<���:������N�����"�0�3�� ��H�Xb���{=���<�\ae+�bN��8W<H�V=P��;h�UVYըe��m���-,��`�f= ��`��+f1�p�%3�%MGkK���79MN!������P��K�Lx(@	&O}R�n�D;IM8za��ޔ
T��p��c�po��2|8�����'
q�?q���t�
s�$��6^A�pCt����q����(�:�����q%�-��h���|իc�X�"�9����6X���A{���%2ǽ��=|A#�Z����
`�b$���ܧ���+]Ғ��-��b��A��B1ZR,}5ʊ\�B���+=2]��iL_Q�-�Y\Pq�;���{:��xC�e�nG̛Ƕ��&8��!`Hu�A��o<���I�����[=���ox����0`�RH���J
{��s+ITdk渁.vդ܃���6Pq���B���B���e��q�P����iE����� wuܠ�ċ2�0]�+�8YI�|�` #_ك�;�ц6\�E�PYG�q��t�KA������}
��W]
����%�y�Ѐ}�ԙ��)د�b,�=�,b1M��%HC�C�9�� 2U��;��'Ȩ�u�պ�'vm�yT
�:�32��d�y�a�Mj�5"�h*|���'��?�����)��1��0=�AIN���.v�{�y�ʺ��nK��]�h�G7�э��>�x/|��^ktC�/6�!XthI�8Ғ\Q�3�`tDJ����b���j���H�� 	$���Ug$�, ���M
�YW��V���}���eكE��l2�!�a#_�E4ea45٥iy�_��іMɏ�m�����Eԝ²[�ē7���:]��'��x{M�~c�+�ɝn~20J�	Od��\�O|���6��kzx��)܆Jl�ю~tְ�8���fC,�i5���津?m%5���6u�Sͅ/�$���r�WԢ|����:�A�� K*A���C�� F��a-D"� �H����@15�\�B3C�X�R����A8k�R���pɬ�b
+�\��1�TX0�L��[��X��/�t�f0�j�ɖ9C�r��k��Zٮl�B�36��m�lD��������t��d�p7�D�K�S��
U���P�*����@	HP�荀�% 	KXg�C%*�l�Cو�5⠆kd��٨�8���6D!ڸ��1�l�aژ��8~./�]v���j4�8X
n�$TD+%8|@P�4H��C�/dЋG8����\aJm�+��۴=b�;���.����xE�%&�13�\��1�����q��9����[�
�e��}᭘k�e%�p�߬��9��s�*l.�2i�ߛ�SY�d�g�������/��Ó
X`©M�/�=��o�(�P?����\�HBp��]]6L�5TC5D��]=T�YC"Z5��Aj�C=�L6hA+L��ԝݙ�tЩ��/P�*|��J�J,��2 �/�/���J��H�ص�B�CHmÆyE��it��
�e�����Yh�՞c�b,S2��E�Z0Y�cmP^@��8���P�J܄VlM���h���>�V7	b������kY�%���2�$��,��'�$\A	����`tA�q�#���(`��I� �B�i��IǄ��4$�RI$(TL�t\�� ���iUTCȌ���'Ν�
~�� 2�����/�N�;4�h �7[�����C>l���9�Cd���+��4C�l���ӧh�k�^Xh�Y�ϼFۀ�����
�����Z�[X�*�ō�E�<���?9F!Q8�\ٖ�PܙqY	� ��9)�J�����kug|MQIM�/��(�П�՟�����' �X( 	B#���yV��8��G$h%�}`=h�����8\A��8T�6T��pAމ�'Υ�Q�A�2��
�	|��(@ �Ix@	�Z#��T�>��%�0\H$\C́�N�@Z��an��,��Ըd��k־=���P�D6�,�d	����m1�Q�L�Н��Em�9b�,������mes���F�1$�H��-�(DA	eQ��	:c�=�$`�*�|��A���dC̬�8D�$d�6h�S�1���1�N����Ȣ�v���Q���}�� 4� �$�9l;��O��:�B��.�Ѱ]<�C?�C>�JP��hAd��R�C3�>�$n9ʡ�ȍ�p)�Оc()2�fk���PPm���H��	G~�7�V�V�iLg�V�-'�<g�z�w$֤
e�<�
���u�c$C���(%3Vh��(�� `�$`@ `!X<E��5L�R�5\A5�G=X��΃��5\�]j$�<`��0�e3���3��
��S�N<�É�H?�jK��Fh�.��D�B�p�+�9"���]�*�HN�#�"�������Fa9j�۶���C3}E�c�f\ddY���G~'Õi�Y�Gʻ2�?���u�VM��
�	p��GzY�իX|�on�G�d��)��.�'z��p�AƒНA�U�EB#����%�A�EB�_$����t�4��`��a���0A�]��|���%�~��Z�@Q�;L=�j?l��/,IF�*�����Q�
��XG$؁����d$C2�+�>J���Yk�f�k�El��l��V$nӐ 7�V��Γ`L�w�HN�r*�^ͽ���ɼ��F\�}ao��G�	���$�������韒A\�V([^�]����Q�A$cy��a,�����`�%�A$��ʮ�m3v�@3�*�D3�������.���������D���r�B8�*�˄i�5Dg6a��d��3,\��ch����޼�Ͷ�	l���؂4�+2�ŉ@��ŵ��.����	�.�z��͡��w^%Γ>nv��9��R��������+�.��"eޝ��x���A'Ν���8�?L��%�1�z��8�<\A�z4�Y�`z��D��/�C��;DK��������0	�B��;hO� CÔH�5|hJe�=z��r.�pw�IH>���hB���5����#߂��-c��]��
D!/"�u�of'�+i���l�*.snr�&i�+"wg�)�F��n�.�ڟZ%σ�,����� �A�=���_�U�P�\����T�A3Sq��xB�q�T�6�����5d�
Ν�e���0����=��N,���=����6��B�0���D��A���ܣ�1�ឲ�So�+����"��k��4H�B4EcYӺ�F^�8p,_\).Ek0*�k�^V?�)��4��e�aN���}p!oni6���.��J�.�A۝��<��>\*�Yh0��VV����5Xu7\��A%��S���^�����CS��S\���6X�>̃6X�AσxQ�L=�6����.��
��9�	����T��xoNH�:���~�lo�JLսЀ�C �tD�*��6�B��Y1L�L�+*.Qs.O~)�(�ᛩ��-��-��Z��DZ2`r	�q�n��
*{'��t�9�)�kM_���tv�t��4 �뽪���4�������u?�t"��TO�4���C?��%?l*�R�����4hA\�hA=�>�h�>����&A��k�v�>��շ�ւ*��M��F҂N�;�N������*H �,��qh#�;$<��*`� ���D� ���Ww�Ű0m�p�U��b4n��7$���H˂��\\��	�|O�2S.h�	�r�2y_\��9R����2r�0,�np��/O��by%�nQ��1ǌg1\�>��|�hC>��6pA�C|_������?�Y��wt�7�È�ܵ=��ņ5>�W=<E��X��8��Z*��+��.����=\N�����CIa/���:�D&i��i���ĭ�vS�:���9t{�Ec��[kF��F3)��&l�$/��O�a*�J�Ӡ���'u����9P��O����띇��_��@,%��m:'��^��ιy����,ܲ'��2%U��>�"��z�wCh�>���|�'��kîNC�xٗ{P{?1>$���u>��*�,�i�5zr��B*�@-��)؀��D	X��.��N:؃J��C:d�=�r�C��P!�
(����ЄR�Ŀ��aܫ�G|��R��+�+o�F�S�k��$�tm���sP����&|�pN�p�#uRSbM��n#�Ǔ'ncg-o>F{���p�#����6��k��"g��;�<�>XC�q2��ü���=����+s$ԃ��>l�϶�5��T���.�w�G��rA/��.Ђ*{"���@4 C�@��=^�C2�@���ٮ����^;t�ܡ�,G�v��E����ĮW��!�kV0c'�K�,eʕ�R�ilfJ�'Q�<���N�<y�̕,�����3����[O�=�uUZ�`�R+�* 7T�+�FY0i.&��a�ض=�,ܹoY¬k�fK�paµs&ژ|��$�R��(�Ҕ��ʾJ�S:�X�����՜7�/b��Q}m�>}��!�FM5���[���k��ͻ҆�=�sO�&[={��]��ϸ6m��ۇ�K6}����F�m}��e�vM�<5p*���%��+�u���
s�B@g�f�i��uj��[^Ig^�Y�!� BF�W�YA{V���]v��]�9'� f��&��2���l��&�p�Q'�\�(�J&���b�)�n�*+X`�ū���,������:�*�r��B�k���d�%��J�0��̮/U�2˺Ԍ��.!;�M9�4�11�T륔vY�AŘ����G�y�Q�z\��q��ǚ+��g�i���z\St�yZ��5�ه�zX������n��
1�P��J�Coڲ�c>1��c�`��%$i��shi��d^���j�kkA����a�D@��1g�lژf�>�Y�U��^dQ�e�m�_�2���)(�v4J3���)�������
�&��A,T�,k1��j,-*�/��"�3�K��F���ʸ�Jl/����.��f%{6�4�Z���sk)��kZ�+����n�f<��b�i���j,������7��P-�G&:U�<־b�5�0�6��H���`B-Ը"�/��S����Uf�d�jIs�9���aI��Wb��^�܅�u�YgBx��p �]���]9Ζ!�H���>�%��<i��n^��i�y�1Ȧ����$��*{��ˆ���J��<>|1Y��Cf��*ed�ʴV�Y�=�L��3e�J&2�$i<�E־��"0��K NC�/��p�K��05�!��F��15x�׈é԰�V�n����<�fX^��wC2XpP_�2V�����@�T\,{ΰ�-��)�K,����*�)N�xX�.P�
� �@�R������1�`	����4�J�|�����gJ�Ĭg$��"+L*T�=�eJ ��=�Mn�#)y'�Oh��S���0ey	��#�T�	��8 5쀇;��v`E"�+ƥ�q�9[v9ﴊU��Fظ m�#��D6��ldÅg�T%p�C2�a�l�D$ q	28�j�2B�,d������b�U�P4B"O_�c�b<j�y�@�@�A�A�jL���zЃ!��'�{3p�"|��E4y�aH����F���R4�R�	I�������ä%1y�h�o����O{j4���K���ѐ濞Arh�4�3#�8"L�+���g��X&��5J0�9�a�t��/����4-��bk��5�6ha�:��ơ78�q���ÆFa����7#1�Hb~(D!��I�І���*\�,�Su�@GH�(�� ��.�Q"gp� �����s��)������a��f,C���e̸(�����|E�g;�Q�|��?>�D"$Vy1�<	35�XB\J�}�,/V�7���2;����U�/i��&-��1�O�}כF�jX5�q�nlc�H ��zC/���g�%<��D!
���l@�A��FY`É!�MHL��
ъV�����7
��B#׈�5*1� �b�/���Y?��E�@��f�,�p�/z�x���BV����w&�nT�[�G��0M/)["��A/�=�sS�1�@&)��=d"o��1T|��5�xj^�����ej�{�ơ�7/P�ώ�:�A�)'�����*N͊mXm=�&4�kY�5�U�G{�Q��|�֌G5��jx���!&1B����l!�T�����[mDD6��p�=̡�]�#��p�-Ē-d�C��+�+>��Z��#`y�*���ؚ���.��C�� �?��1|'^LB��uD3&�����'>�u��R[�*�KdX��1�D���Uߡ�j^�3ڼj\oz)�����ez���B�_6�7��֓|���]�D�`�.\��u����`��ak�cN�����sh���z+��X�?6/��\p@8���+�p��"ǅ��a �Ϯ�(�����B:���Z���ET��E`�#��5�;������v�HD%*�`��+'oN_��7�@���z�<=W�)O�?�$g��,��� y��D�өGy�i6��dv�YG}�I�7�O���� d���=��op�_�㯺=�QWl�h�/���_L���!�@`.x�� �������
�`�\  �T�ɐ���BH��^b��d!fJf!r�-����c� � x@� 8� �(|�T$%�j�h��iVF�y�/��'P*�P*3����H��+M��+�L�������C_�KM���#�Ћ�V�GV��Nf��f������f����&'ϐ�*TAIvA\]���.��M��������� ��n cl������ٜ�۲��`r #+h�h�PP�z o@r�r``�t@�x�x�f�h��vQA���z`���x'|K)�e�,���	���͜�%b�G,N�FH�Gb�QIp/���ͦ|�|N��0�R�嶪��,[/%�a�@m�g�0����L��R")d!uO��"el�nR�\aN���\@�<�4p� k�@�
�@A&i�v�P��+Z�u�P����1������@	�� z��q�ʈ��Rz���%�'�~��l%0���o�J�Z�	yv��z��6��<��tO����a��qK��:.�����g�Q�����!c��k����Jd���'22��	D��#?��-��6�Z�0�N���#�����U����fr��&Q��A~h'���a i��t�*���*�1"�:�h;Ɍp��@�:� g!|`z���E���ȇ͌f.�K��ȍ�y>�>�P��Ҥ��-�r!H�"*6N.�hab�ǵ�+c�"/��B+�Ӏ�{n��'���/��,��0740���VT3���:r�&h�� ��H�<�#A���GK�p��؎�b2&�M�n4�������\��D�8A!*��~�5�-\����L��Lϴ<���&�M� Myg^Γ^��M�%N�E���^4j����qC[._�3�0j	�3-Ө��	o!������qp/��{�"1-�P-t/��D3T1U� ����k%"�%����T�EC�\a:rAah�� ؀�ڠ
nTƂ���G�����5�m7yz�7���NA !R�p��AP�P��A]�SK��� �5Lw��M��N��_�u�$�O}�Jb��%/���LQ��P��U����|�,�eR��R���J�I��hʦ�pZ�D��J��`��`6Ti�_nfc�怮1�2���E/�rt�EYa:�W��
N����l�L�F� ��m �ƪ�Zc�kwS['�Sd�l'���!�7]@zaR!�-�m�A^Ea89�A�8��$J~k��)��FO)�Arv���4�k$s1�',�B��ЊOT�KP�3��2u��g�Hd�bT.K{ؑI���z�f{6fW�e'	�ng+TfEW1U5 �BZV�@Bb���AiW!�F��j���N��|kA
A�
! �7ϗ�Z��Ig�8���!~��l��~�W~��X�9a�6� �\�m�8}3~��DaKE!.A�`�\�\�
ADO)47���$�2朧�J�?g�&����d�����F��p����Q-n(�h)�Gvg��4UeC�c�+x{�fa���ww�����f����fe�f�V-z;D|UF��^���5hT"AƗ��.�!f�HŎ7y�&v3&a �Vm��7�W�!@!��@��@
�`�\��b@�A~����� a]��NP�p,&
�0�F2ʆ��,�C�P��Є�Q�x�/g�t�Q4u����z��d4����c��b�wWLLtVc6nw�׈�y��b�x�Ëx����
��
��}�F핱�u�.��α`�$��j�[�֏�!�` ��؏���!���ڡPL�>����� z~{]��~��DAK'!
N�H�]]� �i�TCՙ{����Zօc֙�a֒�ᩪy�%s��gc�)��z�Rb�b4���l��|��X��8VMu�ovѪ�hf�Vt֦� �i��!|���"/Ƹ�Za�|��'������&���:����A^-#��6�@��^�>�,{�T�
"A[%���w~�9~3ْ'���V�\A���RA&����o{�_6f�0I�a�{��׍N�i9�+�Q��c	�y�����~�S�Ij�S͛�y��몥x0��gG�0�%�V�٦wa�f������c��A����Ԏi����9�	�'��!�{=l�6h�T�)�ʣ�W vV���] ]��'1<�����F���v8�6��0@N����@��gm��;�ɚ%vڷ��]��]����u��ax���n���-�)�Y�������vU�Y��f�ZV���k:�IW�uC���!����[ƚ�mC|�����y~�t��x7󷐽����"!�`�,�֊���*��Z�V`V`T@` nG\��A���ӷ�K���W[OL�a5�5�SAF�gf�А�o��m9�]v�Z��\�q�U���x��c5cdIVv=������jx	S��x��ȏa�s}%t]�a�ЈVW9҂e�#��֯��������-���q�AA��7z ��)�4���\��z	5���h����R�Q�&�]�á�n�����$�ޝ9E|�ep����� �el��b�w\ǻ�D����{z�<��k���!�};�g�,��u��c}�gʇ��mˑ�ITA�m�ڭy����:��\L���#��m��v8�9��U����(�X���!���!�����]����z�hc9���уi��j>X��T��Q��6R�����$ X�ݙ�n�^��0 ��ÿa�ɶ�۠���a�m�&6��=�Z�e�ݻ������ɼ��X��|փU�k��Q�v�������\�lP��=�Y�z�������s<�z�Y����ۦ�A��!�aT�2 ���������}���6jd�&N8d*ͻ�F۴q׼��1�ƍ7^T3�ԩ+V�0�*�T�H�@u*�TȾ�:��Ƈ6\�l�X+`߾y;�M�l���ڥ˕�@���2�l�2gζfu�uY��Z�j�u�ge�v���V�Ξu�7�ֽzɞ���Y�Y�֚�pVd�7n���a�nU�5��-[�;�R���*X�T�0��Fj6޺��5�V�r����-���f�����odZ���wo�+T%���IC���ջ>/�8m�xW^5q"Uʦ-ۥlٮ}�)N�z��hk3���.1��xQb\�E��1I+�Ђ�ӊ*6ܰ�*�H`�;�|c�)6�p�!�>��J*�Hu$L���)���J+��"V�y��Z�	��y��\���]mF䏻�1�5F�Y��%�c�3Y2���f��r�.��b-��"�*9�v*!zؚ�q��W�C�\n���_B.�$\q%W������l0H!��2]s\sMq���U*�n�{��3S�d#�5��q�<��1Mjhр��Qa��5漠�
��0��N*���+��@B�����+��$c6���u�D;i��\���47U*��B�Y��d��
��"y伅:Wo;
ƛa�G�cg�Ld�y)�1� �Ş�	K���'*9��Zkw�f��Dڵm�����y��>��	� �$�|���|B�$�R����Z�zK�W׌c�R��:�^p1���M��yqx�� ��_F��3�
j���ߴsb*$�4bi����,=��+���?&���P5z3I$L�B���RHU�H���Z%�L�i[J�5�席�2�}
��cQ�UÎ%C��`c�ś],Zƣ�yZ� �I򝲹%��|/���7)$�U����0��2H��$d���F5�0uM%��ê���M5�c~E^�1O�hs�D��:6��j�_��:�6����Y��V�\"��!AY		��|��� ܜb�W�bJi�*zbsx����-�"��0�������|6��!�겧���,5t�Y��*%�u�a��$6���i3f�-r'��[d�[��C0�,*�ޞ^���ɋ��	��Z��e��@�W�9L��(D!F`�&�!lQ�8���RUC�����TE�m�BD���Ł�e���	�@��az� �* V ��,U c%#�[;2ȏ|���@�=�a�]���ȇ���!sX��`���8�b�� ��S$�;iS���b����7�ycW��/��p/����V-�2X�ņa��y��)��P�;/�fx\�8I���$����2�(G�I4.c	�2�Q���
��`�RA	E�.��(.�F5�Q��V�sU�"����kpí:	���C<�� *�|�#q؇>��D l�F8NT���0�0��v����X��]�Ö)4�7H�HT#CE+�`�2j٦�tο��s��]�7Cr:���C���������<Z���4�K�؅=/�1Д�c��"��WP�H��)c��82���r=4���<!��ɠ�p&�	`�����^M���Ӳ1�yhj;�l�5��5�g0���Iz!"�ȇ9"K�m���>�7|��*G9�a_P�⪷� >�z���i3B�9�C�t�)$0�a`x#�h�$�a�T�#�@�,��
����Zr�/���� h]^3X��e�(sh�G|'��+͸�32�ĆIf2�L�p��ϔ�&в���$�j�Tfh��ڈNy���K���0H�	F�O��A(;m��6��lL#>��x��݌�Ǩd �O� �Vb���>�s�+�G����q�����(JQ
S�c���j=���{X��`*^��W��a͛�K`�s����W%̆iL���h28���CJu��%�b v٩m��eƆ�s�=c�+$x��c��]�%{RQ�Vd�?;��.�0��'�zP�"�gI�j�RQ7�d�sFN�\�!��,j�N5T���W�HB���z �b��4��T<7��=҆H�������@"���(L!fy�:�`G	^�
V��7��+\��@�t2둏{���D�"a�?ؠ���*&'�oޘ/�36ak�w�X�d�2hL�iK����^��:k;-�D�'��]L�b�=wi��v+��coq萱�3��|�::'˹�Wt/�X�|eԌ�<l���� �\q��`�x>��J�O}�,9��k�z�84x�a衂g��p�P�{[��.��2�	Nd��1��a������s�Sd�#o�F���R���� E\Ix?� r���4�b����>'9e�(狇x�q�{c����<z1w��mܶ:N$d��%z�w��d�c!�aZ�l�[�Vx��	eb�FQ�����+\��q*�5x�y�! A�P^��m]����T���rZr�`����d���v�?Hhm�hjp_����@,l�}���
��'�}Bw��s�����t `��
E��p-���	��� 
���90WQGy��P���l/Y�vPXe�Dh��m��0�tw`�Y�QE�m�Er"#���b���'i�.�e�[�p��\��hڀ�2DeIm�z�z�0���^݀j�mP{�PIdЋ���󐅨�����A��� ���]�6*���
kU2���}� tB�}y�s-�@k%�p�r��a��@ C0�l�؈�u�ā���x�	��vlY�[����v�Y�U&�8���O��;1"^dP�5P9	F�������wl��v[1�����U�0�U4&���\�Ӄ/�]q�^�p{Y�����q��>�kpW0�6��>8�q��@���4?�W	k0#�6m�
�aE� �`�
��pBb�
E15�N���
��/`V�Y�G9!=��`mё�F2J�9!)eɚ���TQ�6y?�=�Y�HdR����w��1hv���hP�@2��l�ȓ�V�?�l��x\��� z�T��k�K�fl0���X`��`��^�O5*�J��Lp^����Lpq߈I�)����X �H�����?��X`gj�&�J
TN1K�47��
k���
@�`ne��0$0! t@�P j�� �= A0����[��$�6���l�ƣ��e\f$��m	�x)�0��0��an~g!\x`$b��);	�)F\*v���E��Xv��n�q�b=j�^¢ڐK���6��5�p� \qq���d0>8\0�Fl`Tg��p�א׀q��ѕ�>XIb`]֐)�5 I��%v&���1��¤}�@��Cko#�V��0#�Q"�7�
���FG+j���=�C��W声��̖vˆ����y$�鬫H���>	�������w��O��1#!�![ɥ]��i�ʩ��f�GB���b�ڜ��oVh�p�1�+�@V�g����{>����ø���(]Q��`���Z@TG�)F�S^��+�v���dXE1��`��R�P��}��s0t!@��q
�:�I&�W����9��c
F�i��c�ʑ�ze�ʑ����b��m6�0�v�.˛��J�w��!�)�͆��&FV��W
�=����$3�U^�`1x5W3�1W�yZs�Ӑ�Z�_����hgzF5k`�Ij��AWsn`4!J4� ����t�����I}�P�
�����B����V� ��p@ = ��#nm�٬׊Pȹ���/rk��F�֚���_F����'�0����:���1�'R*v8)v`$vV���Zȉt��дL+�x���fm�q�ر�g�p}�l`�/hgcIO,�2����K�gD��b5n�G���A��gK���!:���� ����2�
�`GA���7�������
F,������B��b<N7����z��Zwk�*�U\V,���8I������e=�e��n*Yd��U$Z�X����$��Y�Z�V�ʉ���=ɣ�Ik9������{��^����A�ag^�V���hv \��p ��cT"�\	_3>|�)H�y+%�<H1�cH���p�@0�0���,@�
�@W����7�̃x0
�x|�vZ[�Zc��l�c�X�F�_�����No+_�l⌵T�l�K2l���zw��wuL���4	et��W�����\v���!i����`J�ch��]���`�E�&wg���Q�Д����5_�y�m�T��)l`�װ�p˷�9M~B|� !� �
n���Fĳ�a�����< C��U-9�W��ͽ����P���;���b�U���<�h���ʀ�Z�-YE����b!��� �-��V���b5�p�lb�y��$3ְTQ�Ӑ�m���cJE��x@�Ax@���	�W���������P�
����=���0. :�˪��̃h9�R70�[�ɛS=0*� � ��.�bc-���$c�ݜ��+��d��L��l��`��-���د�q-��V�vGE���v-z��$؀�dg��,�f*x���)�ꝉ۰T�jJ�{g���+�K�я�q�m��@�m�9��� 1Ѕ,�
�p���9 �or���0:�<�C�@.@ ��Z�8Vb���޼�[k���4��ʙ��ְ!vY����p�����ΰ�f�cp��olw��w�P;�A�0i7p��6 �w;��N�Z���)�9�Ʃ��d*����@��QY4�X���&�����6=�0��ڲ-�⠐� 
(N~�/�/�-�	�*�4 �Q���;�����{�՜B����B�J�B/b� bDV���[��8I�e��-�]�Z�v�8��ݿ�I��օE�h��(	��%\¤���O
-��E4s�{��}��P��W�� ��N��-�bN��VK���搞Q)�x`�`v�
� �gr�]��*�e��p�F!;��[���ÿ��/���,!-��6,�3 ӌ��-�D^�Ll�E~L|�>� /P�0��%���Q�X��`��tkPY~���n��>ʿ���ٻ��M�+F�e��.�s􍛶�cl��r���^E�����t�4��{,������P��[��u�b��e��Ԋ���ְѯFu J�?%@k��j�)���T;��䇻��@�9+o�C��k1��/�8�
/����]���:��ܲ0E��ѯ��B39ל�$%�Oi��J+���π�{Υ���X�nl��Q��M�W���X�N�[/��[�\��{�˚%C�l 2�Ȑ�څ̖�]�\�ڥ�"-��PiTe�t!��s6�:w$I��%:�)GF+��$L�%U:�	SZ͒9m�4I��͓!a:[欍.mX�re�ڶm֬�7�\�z��}'�]�_��
�Q8op����_����E�*Ջ�/^��!���o^�:�
�.b�PQ�*^��D�Y���26k��a�N�Ff�5�e˒9C&���ٷ�=��&Ig?m��wQ�Cm>��4��l:?>�t�?�
�i���侧�.�<�џ��D欙{��!,H_�B�)����UF�PA�#@Bi��T
	��Xr��|�N'㾋�'�V��$��&�"
k��F�m�Ag�]�م|��&�vz�+`��^�b�_��曽�Q%�#q��l(̰�����I�R9� ��2b@�X@���ZXAY@��\#��apKF�����=ݎjʙg�Z�<g �;�N�з>�"I���S)=��KOR���B�;Ԧ�R��F�#o(t��O���o��ZuϠ���odhE&"�T��? 7:冏D'���I��#=�Ab�!v�Ci"���F�ó	c��)d�ч~�م�sJ@e����T�ڋ�v���`���_,�A����tR�� c�W��a�VXa�-U�!�P0�K��L�V�A��j�3>�J�M=@a곤D=D�<�`2V���s6�i��9���M�䘣��*;�~�i�
E�;��C���ҫ��-���^m5���S衇TAF׊*�#P	�7�L�ۺ�u�A�����&	;�z��p_b�����VD��1'�|�e��m�Y'xܩ�s�Q��U�!��V}�s�@pD�H�
{�2V�=wZ��ˍ�D�*{��sX-6��R5�ĹVB��ͥtBb�O�w��Y�'d�Xi<�:�Pkb�p�-�N��A��C��i�?�g�e~�e�<�(a۫^U��jiE���i�W��N���$%���<e='%G�K*��&��%@dP���G<����B@H]�:X �KjғVp�<�1�]+Z�0�E�cd�X|׻.}�s�
R���Pf6�B��ā(CZJ��,�������*x������o�e�5���w�xD�-�����jIK�u��m[ʩL��!�gl�)���Ƣ�D$#��	�*~�B�P&r�	y7k��A&��a� ��Z�K/54�����Erp���A�(��@�;X ���+M a���r�;m.1e�"�T<�	OcO�B"s
�S5���n�8��g.!d��U7j5�|+Y!�X��B�m��d��a���D:��`>':���z�
�@�H���fLGDmR�A9z��� �S�x�����D&�6	2��{�*9��mk���>�����
�`@���b�$�0�-fD �9��j�� �BuM��
^�k��1pa^0
<����� ��P���]&#W�}�Q�B�0F�B23��.h�&R����S��vsdQE�HCv�@�)�D�J~ЖAU(x��Z�aԵ���,}��R��<�Ѡ<Ԍ��G����}x���W��NO��W��%��ɁV9T��Hqk�@�� ō�$˨�-ҔUX��i�lЂH̬/x] ��d����b�V���	�
Z���UwKT�!�$X�d���
(p�ɬ �hJ3���t�*��
	I����������S��${��W���DV<z\!��I.���$��D�ۥ���-�{`��[E�"i�EH �yĵ�m�w�T�0Y/�[���e�f�xU��a�b�-�$�d�f͝8�D�U,�fe�Xp�.����A*Bf1L��ݣ3I���;UA�T"d�8��Ԩ"o-f#.�vG��V�����Lb>mG�9��mԧ�E�G���8�c������g�T!e+�qA�"�F�#���jo��7r��i���B�RB���R�,��%�W��C�_������/�(`��l)3.'_U��9:2�3�(p�¨ ~��Ƃ��-��Ȃ�t�_ /� /d�'RF�ӄq��Pi�Ac��
=�A�\�[��,�K�n����'?syк�ѣ��5A�s��=aSm��"�P�?8ݶGfI�}��%s�y�rb�8�Uг�� J!TY�*�`�*T�މM��\��	T�L��b��|����A;��`@�)�|�t�߿N�b»�! ��l����ppp����t1���.��e��wlan�a̟K�<ToIV�?� ����%{�0K����$*퐪
(\�_��5u�+0�_u��[����~���EjRұ�!C�&\�9�a���}�h��0���&��7׉<�:��⫂[�����/DQ@�(��XX��<���JІ6��TpW(� �=!>�s#�[�	z(�2!Y����:���#>�y#�=���z��Z9�">?ʨ-S��ҭ4Jǁ���&��:�a�[�2��+3�,;��KC�r-��#�s�I%x��A	�oh�U�>�6h6`�,H�(�.���"�rJ��p�"��@�� d#2كP�C��+3ѝ��8�ټ-r�,�5����q�P�2�	5�9]tAU��C�]̬����(�2�
��粮�*Ƌ��3:��(���᳽ZSB�z½�-?q��.(�3.�Z�����?3c,�^Sâ��1�����ᨗ����~�-��i��6h�,�{|��7��ā��+��(�B �ЋA�/ @���������40�80�N��@�)�E���#��Z[z�j!�z�_�C�K�c,�`k�b3��BƯkƅ��a��K�e��{7�	�R2�t�>/$��%Q*Cn;�c��#6�=�#�eɠ�c���Gj�;-�+ā��U-�zk+t�):'�0 �Q�1�D�q�@(�U�6P�6h��l�.݁�F���p0�4�p��^0Q��Q��A�8` �#��0#��j$>�(��aNߜ�����B�>
3B�dN�lF�"F�F�� +���,�L��G0%�{_A%���AOx��n�Ik��s��U����H�BĂܿ�t|��@#��0�Ck�1�"��V��A��5P.�1�5��"1H?��D+Z�~������p���c?`5ȂH��` ���őA�3��J�[��[(�4B"--8�N��΋Z��N�sJ%mR��#ܓ���E��'����B��q���j���2TH�n{˵\R{4:^���u�OW��A�GN��i�i�O�,L�d��X���+jL�90.���B��T5���/�L��?��+�2�Ɉ�_0��8�v�s8QxA��<`�jh���T����T����T=���+�v\��[��lKպ�%-R�|V7}�j|?k��Ϛ=� �.���0�j#K��2���3t���SϠ:�#<�h����!�Q���O?����I-H�O{;LQ5�{8�ؒ�1+H}�I�P�T1M�T��s�o ��? �2Y"``U{�|��=Y�3���� ��@8�,H?��_���԰8�?m�,w�G.]��c;pcKg���6fm�w\�4��x�E�=�2�'%�ְ�	��!t �i���CW�
2앴t�x���d�Ǡ=7�9��X;�KNˆN�Ah��ü/Q��՘� Q��x�@�B�̜�K�����M�X6��P�C��p 2�{ixXY�MY{�| ���{�s�`����*ІH� T�PVt��ۋ�nUW����9�u��R�*]�(mZ�NkmV�]϶����6<6a��.U
1�sPѹ:�t��s5%S�t`�w`ׯ��؃Z�m>s��+��i��=��[�Bh�Vx��h�*��1X���@H����?x�i�L
�\̽܊���T1p�l �q�����?������u@��{����]U� u9S�s����4H
p=bS�ۻ!j�9e���'��^������j�Wm��%t�,�f=ϵ|=�Z����p�ZҾ�,�3���cG�U��^�#ڻYk���k-h���[f��8���x��a"y�<Q�4�q�V��H��
�\˵؋��.�d��T���6��z�'�90P4�qd؇]� �W�t���T@�{�`��� �H�S��Sā�C1u=Z~2^���.3�\�^��L	v���MCy`�l��0���KN7E�i�c.�L�5�گE�������R��Sx��)ޠ|�`�b1	sX�+�Ǿ]�C�zK�h A���^P���s�ɰ&ȕT��02��8�.�R&e��L��O��z��v8���zA�pT ��W�����(�P	 �]@���"	�Z���i�-�;�	k�����x��5^Ϻ��?u-��R��a�n&�r�b�v�;VZc��of�]�á��x*��p�1%S�e��|�e�H[�`����%���~[O�?f�U�7z����S�F9����`��M�j��A�P28�JȆJ��q�m��0��O���V�Ne.�WU*�T�#�i�Ԉ�e~�z@0����^V�H�j'5�0�؝�Sk�ݛuu[*.^�V��_*v	��!��_k�f`��Nk�~��]B�vK���Ω �� �%�g~Jl\�b�?W�V�G��g��9�}�_�8x`�����uX���0o��_x [�N�VPӎ �� 	����#΋���V��k�ё�8�7�؆m�#�8����L%�ʥ�.�T7@e��̋�V�IST��8}�~�] d��@��]h����Ȃ6��+���@����΁�hgl�NtOou�!D���Vl�Vk�~�K��������\�nFk`��
�o�.t��,�Gƶҽa^�Q�U
�R��X߱$���t?���&����'u���P�p�CAS�π(��e	���R�?h���` ��j��i�A�05��8�mȆKȆqP�t�8�
6�8�Pa}�mL����i�PXC������`��~���`��0Q�+�4�?�$�[��Wt_WB�!TqwX��Fo���E�tLW�c��}������p��N���y��t�y������o׉Z[�>����ڮ�O�
'S� [��4US���7tn�/�Dv��Z���l�vi׀� A =�L ��v�o���ow�/�7��׾�J��q�i�1�
A�l��6���ܓ�w�eLm�{1?p�T0I|Ax������t��]|���輀�6Ȇ+�"$�E:�8y��t*.�S��awy����jL�ye��T�y�G�NW������v�?p��pE������f��>��5�u���u����S�%[�B�^����)���]�w�������Tx� ���4d�p�E+?V0f��ϧO)Nj��B[5n�pQC�ڵJ��*�q3�R%6��]��-�/�zj�7O۸6b�zy���65P��aS�Z�/O���̚)*R�
[T�P��`�F��Ⱦ��oo*W���j�W/�lU�h�M$6.T�x�#ǭaȜ9C�"Ċ&�W�ҡ	&4�Z�D�	4X�5��#8Q��w��v=��o��Z7�ھw{al��DcGGZ�h�޿�sf�2s�Ο7��+d�v�R�^����N�8<�n�h��CK!�2I!�L��DP�$��ч�z��'X����M6�d3QdT�P�����V��T6�LsM9}��<��s�U\�q�R���O=�|�T��L+jpq����*��R�)��W=���>���`��K w�,��BմQ�
+� ,���w�)�����B*'�AֆlED�@�
��~��p��Fi�=*[�=��sM'ꀣQT�:١��w���2ڙWz起�+�R�}X�ۥ���@�1;22� ��UH2� �	��|���WnL�b$!fs͇K�I6��G%C���WE�XT%���RS���Q���<_\0�X3p�(RH/1�U0s��<��S�����q`l�RrA�bpƐ�4�؉C{�Lf��Ш�-iό.����:[j���ڥ}�iӟ�#j�R�Fjn�j����_i���]w�x'+��ݪ�Ȭ��.��l}��w��w�&܀�N��,XH!S�'�������	!Uh��Jr1���\S�6���F$��T�q�!�W�t����ER���MW�1�>�1M6J��M�E�a� Y���$�b�!�����!�%r;��a1�_8��oV�U21�-�4��f�1��g����>+��D�2���ww,����~���
h,E��6:�@b��l&V��UfjUg�mW����
U��nVk�%� ��%��)
Q)@�� D&2������ր�.(iF6��8J45�H�R�U�����!�`�<��5Xc�@����ťXX���к�p�D �
q��c0�hБ�i*
&��`-�!���+�"B�Ee.�'�ď��B�$�3�i���F�(��T�l��H���&iL�\3�M�;�@�,�c�\��m�y/��K�T����� ���pB$8Q�L�!��D�*TV"��!"%V�٨�4��ī\Ō���Rk�G��8���6p���G?�q��Dd�N�_���x0|�����)Ya,&�U�d�@�!(ƖTt��pj�b��
!��26s�o�����}�i�|���H�w�d�8Y� ��R�2L�,PU Q�� �e�O����FK�*<d[.��˸yPt��QM�J�!p6�q�0
I�!� D
��!?L!r^x�� �zTBr^8w4��l������H��<�UXyB���>���k��_ TԠƺ��/�AZ��t��E)jl��1�9�Đt�$mAN�
 ��|}r�f�(�ǄB�A8�� �Ҁ��B����Tz���)AER.���٦:6��-WZ�o������ k~��һ�T?�H:�"�� �G"ǅ�(��bpb�~��tj`Q=�R{^&�KJ��A�y$���=ZbNmxQ�Ɔ�Pa/���P�DU��@�6Ⱥem ����� �Ň/�ᶶ9��*d�[�BȰ�f��_a^�h�j���~7�h��,T �ʥJ�Tr���7�����p�+�����2.Y���R�F@�d:ģ-s}�6��7���+aOm$��Ї6�$tC��HB%Fݍi\�
m�?���6�v������@)I�C'"�@���+x�
���Btm-G�L[h���m�=� af�52��f4d5��ڧ�����L�K��f� �X�;����o~�����g���<��]ZضK[�2�[���������Xwn���|/'l�h�#�	B<�
L<CJD$[�S{pQ��~��v�8L�0�~�c*���?��~M#�yІ5ZR�.��n��&���//paߘ��Q����* �b�=$� ��F*l��)�3ˁ,�^���O:�QM����;h����/��ؕ����һ���	��y�5G��{n6Y�cϷ�ez���^}U>rs�EX�f���j�vڠ�R�a�Vh��»qP�+�n9��R��������y�_(촑��c���@s|��O���3��![�]�zb��Z����OlwX �@P�,d�eY�0�a�f|��ޛe'��)y%��������<dW��U�A��S�C�U�R�銟�G.�X�������śY
�h�4��(XA6�hu��h��,�����8|��Z��Z?��<��œ���6�CvC5�]��`����I�1Q�B5���T����qݳW�y۷���,ԝ�� �B2��2\F�}��!+������E+-��� wa`<L�N�VbZ�}Yv�Ou)�BT��M	�-�.���J~��X�^�&&�O�F&�RB YD��;��̙QT|�/"IŁ>�����L_��C6�����h�$�K\�(�M�7Y]ϱA�TC+´y���� �ab(۴̀�P���,䟞 `rق-�0�B .�wDM*-��@`���%�^'j�彞,fDZ`��۫@�l�:�U��U��9�CH��z��{�}��S<�s��Cj`h�6�aTxA6�C���S�A�A$����K%L�U�ha��C$h�=�����SDc%<�\�<N �TLxN9�c�������ݛR�q[��·�ݩ,�%>�B&�!���e8ҭ��v��C��d����"zE*�b��=�M>�D�c:��Pfv��n<P��聞)F�dy�d���*�k���aWA"J�4�9�AթA=h�A�=����4 ���6�|A6xQ�yH�\�4�K1��Md��@ecTC!|%X��	x~���LDy[��[���ɂ*�� ���0�y C��%V}��x�kVf$��=@���Ma2�9&<\f Q����,(e2�c^��.��A�Jg���Mh��h�I�~��e��O���v\��f��M^C��Ȱ]�8�C?L�KgAV7h�F�Lè�qU\CU2�!)�TI ����'x��x�gb��������ڂ���]��4C2�'f܊wlƜ��x%�D�(���"&�k��`Єfc��jvI��8h�
�nb���^&�[���:|�v�vf�������(2 Cŝh���@&�6��nP�~䩤6&l��4|�5`�>D��]Z/��1���D)�		�0�9�8FB$xC+x'x����X�]k��Z��q�%e�����]��y b3�V��w��Q͠���V��"�`�"�
��i�꿲��V(�F���憡�"
�:��tf�Fl�~*�ڒ��B
���R+��)8�
ĪB���<85|��4�����-�5�6��}:yAx��y��)\�W*�o%y>�H�@������d��=��>������ �y�]��e�͆N�r�&,-�a�=��BF^�(�n��m�����+�&h���.$B���B,�F��*��(y�{x�۠�*�~�*K*eBf�6��6����L�����i�>��6��阎���Mų�#�Y�%� �%��W���Z�����a��#,�)W������P�ë�l�R�`ګ���J���v�^��V�����m����-���~`��>��^j�n�IPH��x���l��x��G��Rn�Z.��-�ޒ�lZ�L�4PVk0�$Ϣ�S ɑ��W���B�ŀ��Ү ؀��]��\VYr��]�$z`P3��9�)�JP�(��
Оv�K��#n��p�
no��F����������:�D�k����j�zf��h�
0� |��Ї�˪�������K(+��9��UX0L�S�ǁ��B5�E (����
�wO	��D-"/������.��}�0ג~zG��vt&;�;l�F����;(�v@�����*�b*i���J+El�>U�`jB�Jض����.ߏ؊��R�@���!���a"�ǭ�""��1�����^Ė ���H�J{�q�����=LbKR.+d��=P(<@���3,�j_���.�~aEc5��5D� h� ��� �AAW����,t��L�*��)�����+dtG��G{�.��{P�.l����J��9l�K�hHn�:Ĵ9�4=�C��CHf���O��:�Pw�:�CNw�9d3�uP*R{�RK��Q�'W,�v*.ϒ��Q�
(�2Gz��r��2(��Y�C:��Y�[�2(�W�Tī��S�r)�rf>o��W*��t�.�m�x�Um�,�o-m�7�fy�ǎ3*xl~�3K�C�b���sc^.;DKN�5��W���r�TTg�5|C5�B+�s2$A�n'6�v�v 7p'�q�0�r+�r'ws'�s��tc�r_�X7u_�vc�Aw�Aw_�`yk�w�7y��zkAH�{k�h�*�w|��}��*��~���*���O�����S58�O�*P�4$x�K85XC�[C�o50݅s��o�6��Jw�6�xK��<��Ko�״9�8M��N��N��O/���tQ�M5MӴSu����8-�$H�8�-�d�G��s��Cf�6;Wy:�C>�;o6�^nj!G����4����7�E���*d�6@�Q>��$�@E�@���GA��@���	�@	��nGA�mefe֝�DAI6���T�[�ڦ�֡��Nd� ��@k�hA�ρ�́X � 	� ��`�`�0�7��:��r�Z�+�`cv_A�S`A�C{��z���[��W�����(;������;s.�x/{w���;�w���y�;���{��;�ϻ���;~��4�{���|���7�60�	2n�J�8��*�Wcb��[|:c���l�W�Ʃ�h�f�\+�����7��9������@߶��Ĝ��̫��F�������L$�0��Gc��<c�N�xV���5����8���|�HKic�#kz>��y��L�)` 	`����)�:	� 	�@������@���=�����>@�<0AoG������ٷ=��}�?~�7����f��{�G;�[��������A�SA�	!�k�f��gwoO��ڴK��_����o:�#;�#�d,���.�ï�G/�
*�~X�<h�gg<庳;_9dN"C�C��5�.��+����9����)�>�s���<��3F$�������MC5��?юD�HZ"UȦM�q�F���5W�Hl	�H�e�a�!ۨQ��/(S2����3E���D	T%H| A�NO:��$���NKi���2��-*�|�r%K�A�&u�t	T�S�~J(�!�Y�>��y��rT��nT&{�P�b��);�P�Y5��
f�hzc�U�h��b��P9CMB��U�S�fղ5ҠiNs�z �){7Qc��ĻW�!3��wpdȀ�2�
�+��U�:�j�|����.���|اK�w/�t�ҫ�3ϝ;s���3w\�.V�����ŋVU�0!�@�܈C�ָ�6
�i�y���Jj��Hڰ�@	����.���i$bC��$�<�Cm�qP�KRc�� \i��,�)��D(A�D��'���)'�F8�'B� ��$A,���6�������c�AZ)��UV��R�:���B��'�TQ��]Py�9��b����(T�{ϕ�Xie�o�yϸ��ۥPTP�Ɯ[hh�:K�.��@b�PC���,����)�p��,���?RI�V-�PB���`"ת��m"k}�=�|3Ε�\�t�����������x�AǞzܩg�]�AET\Qԕj��⤓j��%5�H�޼p$#W2(B)	�$��(��L"�&2ɤ�r��$���*�~�b���J�6[  �|�q�@0٭�R���S�a�9���	]q̫S*�B�9�(��s��t2k�$�\v��e��f�U����b
H�a�~BŨe����[�F9k�>)Iq��*��/�D3Չ)�Xek\+�첧�{���$�ķ�F�,f���+���6��.���M��s�S��NZҵ���kQ_�<w�1'�p��&Y���-����]Q�`��S��j⯎�ޫ�
p3,mF>`����
%,X���J���F��˷@�t��J]#�K���`u�mt�y�>�iZzf�ߘ��#5-P�*��P�Kq--(@ 
 ��c1qEЬ�9`���H`�Цn7����(�FoZ�»�����0+q� �p����3Ua�,�+�]�X��9�s�yN�����iEK<تֶ������>µ��!cR�BU�ԧ/l�	��
��`шDw�W���8�Hp0���$ıbn`�����?�[^����@�W��Kh��&|�j5�ˮh�ጒ�\������]��)�f[�k�Y�kӱIr���L> �b��� AH��m��
�@L�*�E�(!g ��)x�4Q(�U�R��!=DH�D�K�!�l{6��m*��ea�X��\�ȴ�\�U�꺓k}�<_4G��c'T��j�y�)5̑Q��6
 Lr�? %$�͈����4�ґ�KbQ��IYI.�~\�K�&vSG�k�z���7TMEy�$D���(A}�F"�=R�T���ȑfR4T�������-��{�#��K ዤ��B8��ι�U4/�Ȯr��^"Lg7A[���+ 1��\�<�T�"t\���#��au[�N<���܂�X�5;�'C��_���6jǆ�d����MO�Ɲ"�34j-ɯ8d�$utÌꨆ�ᨤ�m�Aޱ��# �PG�r��\}#JL������w�#t�;Տ�ˑ�l�
�gG�A��d�]\_��D�y�Qmx'1�Z�)��}�С�65Z0Àe�۴�n:{Ґ4�z��IL�q��'ʟT舭���R�:���9v���=��+�t��G�^W��,*��dS�:L���Β���TϨL��Wo:R����e�q$���jm�}��0!3,��3�V�$I]�ͯ��`�@�8�1	D�؆5����MT#1�5?�y{�O�7����
$�6�����������}6V��-���Z�R1:�����b��V݅Us��5}�O��(��Q0� �1 �����X}#+�!���_���e�6��ŪI��3{��9��Tb,l����>� ��(�7dI�zcE��^�Ŷ���<�Z���J���1rO��}ʢ9��@�6�]��['6���4�QLq.J��������T��!GuD�saiHC#�N�b���'dN�[bթ�BHNr���<�x�I������[��[�-t��NOz�Y�>��>��9��U�����U����%�V�����I;�i�B����b�+�����w���i܀�_7c�<s�ᾞ��@�0�"(��m�+`vҔV�.���3�0�d?P��#�h�w���gX�%b�	�{�}��(l߉N����`��y�s���a�x���#��;����s�C_�]�9�G�t��������~����w��ٟj���I߾�a_T���:�I�s���BIn��Pn,`	
� M�6�6�Bո@��n%��,�ؼ�Q"-��`�o)8 &�-��6$b��X��8샱�H�D�K/���a�̏��oXn�n��<A��  �|�mO������`���B��<�������/���P������>�I���@�n�Ώ�O� �O{��`O������O�����O�o�^��O%h�/��< 3  4�����Bx�n NP�l	n����������F�t�G"����T�b��(`0Y �iRn�M/�4���鶀�x�>�`��<a.�� &� ����A3��΀f����v��,���Ҡ�H�a�Ҡ
��(���p�O�1 ��=�㦐��/��O"Wq����1"K���o�L�4�����݊�j�`{$q�<@�BVr-^R8 M@t@j';�Cqj�,R�6��8Nb�)HP�N0�6#����7|�����+K�[V������l��� p����
���`��-`�|����1��
��������\�0�Ց�A0�1���q��0%����1�o�0���z�3%3!	r�DN����@�.�A�676g�c'�D4 Z2\�%�"�r'e�&oR8k�:�
�����N�� n�2d�2"O��->�rJ��X0�X~��@o���C����Vnm���
��:A	A(&��qO� ñ�,���o�����3�@Z���@���\NAͰ��Q#���t
S�/9�r"�/4��4/R:?����=1����B61��a8��AG��v!�tHcab�p�7+�%9�&i�&s�l�8�3'y�^a|������2t@&#O���6�-�R��]�<q�����A=m����]49���������@��<�>(&H`����`8� ,!�Bn��=TCI8AB���7S�1�s2yN��(ې�ڐ�|���������o`�Gu�H�u��Y��H�5r�j!I�4J��&�T8��&�49����J25���@d���]�"M3�&� .�M��7kN]!B/��t��	�<4���@�8�2AP�`.�^��@��B�>/ c���
a	�N�����`[����Lv7����.�B	�"�,���D� ������i����h��X��l��3���㔏���p
2Vݠ `�����G����ra��r��!�AZ�6r!��I��&��J�T\k��5c>.]�5L�Bϔ�N0�S�&9�8�Y�����6OIO��r��* b�Ja�� (�@�8A
��H��>��� �������3O�O@VQ�� eT�� ��buT4V��wAs�O�f��j=�'�3rh5RFi4!~��c8`��@��d�L��x�~c�n!n�um�'i�j\;�&��o��~���S�@�����^�"0*N*�<�Ct0��Dx��=�`���@� -'�$��.K>�U"@�@>�
����͐����������0�ŀ��Qy�1��;e�8VAz�8�ǘ��،��㯉�F}����$�@%�uS!N��l`&��؏5@:qf��wH�v\�T\����VK�� Y��R�w)P�G��- -�y�q!�P�CY��S�HX��aۀlO��	� @�P����0�. ��l��%� ��j�s�`� �`@� ��@��9�͍��*������������Y�+A�ǹ��y��9�����ݹ��9��y��9������������y��-�������ٙ�8Ͳ!���H�`���r�F!J�B�@�X��ւn�[y r��n��mq!9�L9\��u�����XQL#x3�>@J��x�$,g�)���v��O��`�:�!�ﴪ������ցu=�0�0`�EH��/ J��IA���`	��Ծ5����Xs�#��@N9�΀�W�#���WMh��;�:�����X(����3��5���س?�C{�����H{������avuQc[�cl��xY�i�~q���� �XoM�Ji��u ���w:հ ��L �eJ 3�g��%��cY0�(w9N�B���:�՛��۽�����;��Ĳ��>- " �q���$`	���i{���W���y����Pm@,E������ٹ�H�I��@{YTG|{�(ϱ���V�7�	����%��u�2��9�t���2^��C�d�6�d Ji2y �6V:�!�u�[�'�s�SK�����$CHP��wb����[<]0h8/9�f9 �k�۪ۛ�����[�Y٫��$������>�
" ��R�W�0i��g7�$4�Aأ�����A*��ԯ�B.�AZ$�!ơ=@}�Aj��o]� �a�!v�Fݠ�a���� ��H=*�2�i��-����n�^�K[�T_��M��8p ���ݎv8���v�
aR�AH�:@�u f!ʖH�\�߭4J�'���8K� n�|b����	���=~�>�h���Z�G>��ϯ:�������$�V&!>ay����W���p�=2�����_���׍��u�֏}x�� �_�կ^�Z:�곁��,5	����� ��A��د!����A����";�CRz�P]\�N��R�f̀l�"�J�2�C� B�!<�^��=�@ATRA8��9�<ʛ�nc���[�w 9u�.�%�6� n$@R> (z�n�iX�����\֥�,7�����ܽ��v�[>�> ~� �:aA�������(S2�W=�+=��`���aڷ}��ڷALq�^�!��?�#A�z� �M�6�Z=~����5y�)T�f�6m��7O��q��uY襋I1b�R��J�]4	�dˑ8��$�eΖ^Τ�ĩ�GiΜQc熎�<�J�jBCREа!�ƯQ�
�A�B(PNEt��AG&�ꈅ�Y��Ɍ����ک��1�J�_���E�.]�B�p��j�,�����
��ϠC��f�2ӮR�f�jU�|�b�~6m۷�����{��y�ԩӧU洊�ɓ)A4�"�N�^��d�}�I���Ϝ��ͼ����_�m��}��]{9R�8m^cc���6q\�M%lhÏq���85��5�̣�>i�P%��G=��1�J���y/��u>}7RK+�T�u:�4TQ���2��`Rq�\:��S"�0�[��\�%B�m�%%<��]��"�Sqɣ;�@�Sa�i�abp���\|a����+���ejh�g�}��h��b�+�Z�Zj��rJo�5��m����n���9�W�'U�"�$��"�PJ2�XS� ~�P�6�Wv��>�`�+m�Z zy�G�h�F6�X	G�h�F6�^����\���N��^Tԑ��c!P���M.��K��X��(]�.�1�t�#�x2�%7���NEţUe�U�2�@C��4)V&L\�[$��4��]y��^~�#X�YbTB� �\��ў�*i�ms�j��rJ����ѐ��=����(��$� R�'~D�ĩ���w=բ��^��^���<�`;O�oc��<o�������1M=!�푮j�+�5���8N�捳Fkϣ�x0�;�J��o��]':I��z�N�p��(�Dr�F��8��D��א"��A��%�Z&t e\tU���0���^��֖���Sc�Y��i�F�v/�K@s�qܬF�:���g��b0>�J+��ô��0��D�;��������Ȕ�:1�OH`�av����Kl��NxV�"�D"m�F@��HhA� �C��7�pam� ܲ����!G� ,���{b`C$�
��n`�4��7L�$:9�M6�ҭ�XT�Mvb�4<�(ްF�Ġ:�.0Q��ޡ�x�=^��OE8�d�� (�W����ţ;n��(�%(,�C
+&��`58�TxX	B�ɐ�fW�Hb��?*~�q�.��U�OR�S�����]���������g���&�"xu���Z�fEM�dCB��a)�Mmzpٴ��p{P�[��B ��	���U���I����+��p�y��CX�ɫ�9/yE�)bU�X�-$u���5�H�9P�GU����r^���(;�A�H�@�3�v�^�����9vA�@��M3i�|���H�2�܃+e�9��t��I��`P����M%�E$C0�֬W_���7�Ҙc���Y��g4����	&�.�F�� P���'���&��'�bI�Pw�&-b��я}��[a�d��+^�׈?�YDǩ�Z	�6�A!�c"�N�N��g� b6�)7 4�[�ODwA� ���,wΐ�GD���2���K:ȀSwq�#�1�=@�C���8�o��T� �v���q{<�A�3J@�`�0�=�ьs� ����TQa�i(�T��.V�T�`�����*K��o4����~��S,�h�t�[e�^2�R�09���)P��	[o�5�W�g^�\��a+�˟1����[�A'���5��N
)9�҆5ԃ�i�G�!�H�"��'5�@.dÞ���uЅ�8�E�@c�/6b�X����� 	��rwT�c���.�!��ϼr<�dXQ���w��ΡTT��7��=J���h4��:����p�lTC�s�/*\A��a�܌��ھm C�>3ԡN�?���Ξ�r9�9-S$���[���p�u�p�����.(�h�>��䪄��G�΃+p��W��G?6��}*Y?� xF�x�]�׀��\�l�!��Nʍ��9sR4]M1���0�4����� �4��ͥ�=%�XG	���B����@4`�d@�Y�+�U2��&�:HShl#�%(�"�T��ȣ�#`1I�L�\�L�Ё��Bb��J��4�Y+X�
����xϻ�g�{�#S�N�.ig^��N�J�0^<1j�>�lL��2��ԣ��}��$)W%���H+�����`{O�}��8} +��P2���D��ZȈ:�9T��;�)q�Hx�;�S�(��m� XM��
TX]�L�y�s c� ��KP�?�c���˛�t4�N�';�AjX�da2x��s ��
t@��y�b�':���a�A?��l��
��
{�"xw��w�'A)�x�"�':11:ED-���,d�o�o���#B� B��iSp��d��np��o��1��e�BZ��CK�g����0hr:,�*��"�.=d:6Fc������d �#jU[�Ars��FS~X^ss�1��� s"pEWh�@Rt�
tpt@j�j��$p�]�I�7; ��P��VP	�
�p_��}j78�!(�P8(�q�0K�1wC3�!�z�?� fP8f	�.#g@g��|�B.��^�x-��� 2�,�6��k�6�07��!t���R� �8 ��O��Q�L�*3�&b�V�|�*#���Sgi.�@)^b���it��5^�p<�lu�`t��G��������P��] ��t@��U��
"pr�6v��`�C!�
Vi`>��vm�v�1�q��a��(�x�ø_m�&6qx��n�\�dI-�3�2 n�+�9l�&�B=��7wӖcԖ�5F��:�n��n�=.a[��[�����S[=؃%g�rn����E�|��L�u���m0	<C�s��!��tPh�p�F�G�U �i�q 'P1�I��۠}3�YW1s�
�p$@��2� 2�3�#cB��pxHuB3�F3y�=�C�� ��a�Ƕ�l�
{GWØw��w���[ngg�z�V����2��d�[<�8�9gP.ߤ-���Z�w/��[��L�B�c�/cYP*1g��q��	n� \�O"%A��52$]�.�h��Y71'�G�p+������V7'��i�`�@�x���Xꐓ��Q�`%җI��7�2�ci��5�����J:�6?�������?Ζ�|w�'VZI��l�mP.񆨞�y�2ԡ&h�A�&��,w7y<��Y|j������o��V";��I�-�q�)ک��q�#i�3<~�7*X�
<�}����HI]����Hq�?��O��`��cT�Љ%�2Tː>�#D`};�I|s2jH�ԁ�&��������q�!���
�a�����)��4m�*
X��\p-�n�w+���lC��I�P�9���X6^` ���ݠ������{���V�W|g��j�lh|i��Y/i Yp���H�U=Շ1 `�ۀ��5g���E�i�������^O����@�'(�Ü���0�2ǉ�V�_:R�t���pj�U�vif��7�Al�
(֐���ʧE�K�2�'+�}
�	k6/�2�^ �PNw+�ek�-q��e,\�[����g��'��h��JDc�ǌ,�PgEQ�!�pt�L0��U����A�g h1m�2�r��F�zH]M����ܰR���0jcKp'� �@����� ��8~�@�*�Q�`waw+BӀ-R!��ېvAm����>f�H�u���(�z��k6�{ngPpb��S٢gm�I`�I��7�&9c��c8�r!�v ���X���[[��!�M-��2kc@kcLS�����+��.;Om����W�GH�g�s-�u9�\��
�P� �1�O�g���  ͠ � ��[�j�5��ȉ�GP��jF��@���
OTw�%&�;P2{{j�x�&�p
�&3�'a�d��>���3um��(�;� ���谹 �c]`f�p�����4̼�&b���&��_�E��yY�C�Z=v��]�f:�KAmI>�%���{_��#�:���$AW��0!���@W��ֲÛ�����pQ<oA>+'�wH1x3�I>�fHEbzT$�H%p]i' �E�j�C3ǃѰ�i*km�%��A�s�URQk�o���}$@�f3���O����(��
{��'V�x*�~�_��c.Bg��Jq�&_��[�9!�B��oR{��6ِ>�f�!�����mDlP	g��r��O��d
a8�t�x-d��GT�K�������P�Җ��z�d�r����Bu), �#0>�cGUh�<C>���$yt�����	�V��͈�t��e���RM22R��@�hL�S!���qsP�$������YV��G�
����}w���i6f�o�+o
��l�e��{Mq�=�X�C���$Q	/<�tӺ̤��z���y���i�cf�Mƨ�8!8�������п���c��h��P9>!�d��A{[j{�����\����s\̙;]�E��& '��������~�#� 
�$ۻu<��Ơ��	`R%�� B+���$ ��&A��'l�<�a�Ӹ�jw۽�G�ݚS/�6F|d���?�9q,���67�8�2F�5~�X�r�6�����D����������y�=��v�1T	 �,�����E�Q�c9K࣮��;lP[y:X�и�F}�YQ�M<~����G终��A�k*%���Eǈc�F6��$k��A�<zQ$�cܨ�I�fB"@h7J=�Jk�������������
�m��)\����w��c+��ޑ��A؀k�`F�a����[����ވZ,J��@-�{ �@F��6��6��ؠ)�N�p�w�s��1x�m���ZP!�6� ����Yl���ET�FL�(���Nn��s��1��~������3Ⱥ�H~1p!H�4��=yӪ����7}�rpH6���^i݋K��s���V�;wl~�W�� �] �x�b`�K��2/\�!�n<��J/��ܑ�.oѱ^�!����+Vm�yw76�Z�p��W ��������Et ���@Fj'AQ�
�w���o�'�QYlY̺��_ LO>(��-�q�<�/>�{�E�;���z��}>祛��;�]$�hAnSEb��
1 �C�	:
K�̘DpqN�H��!"D%HH ��ʗ/j�\ٵ���z��\�-�5s�jZs���*U�|�T�P����"�K/K��s���O}?�ᒄKR/]�>m��z@��[�5	�/b�~��o��yj�l�2/(?mn�p�So�O~�ސ�$�6���'1ޤ_�$q�dJ�+W�|���ZU���iS�D�8�V�W/���$�D��x�i}�H2ab�˕q���Ӈ6Iۤ�Ũ�A��G4�=bF%N��A�D�X�J��@��t�^e��Q�A|�e���>����sj���t@�h��hES�����

B�d�A:0;:Ԙf�HڰB>6a:�F�$5Ը¦�^�	Gsd�ɕo���Zى(#�4�u��*�.�x*���/��j�)��rõ���G�þH"	���+m����`������/6�����J�.�V�D�y~���3hdk�gGmT/�LӦ�B+�*S=ݰ�-1Zs�8��1
5�P�Pm��ɸp�+�>����t�A;�8� �H`g�_�/�h�Ǖ�P1�� �A����v��{x�`v>�A�>�g�>��h��C�
"�[���L�W�9֘F�j.26�6@�:b���]AƜu\�8���1�G!���1$?FҨ6��*�մy��Ǵ�b9��\
�������칆϶����q�yl�z�*4���K�5��i���T�ɞ61������O}�����2M��h�5w��<�h�x�G�q���n��aU{�k�+3�����B]��T�}e�x^1��D�vƳ����x%�]ιg�]����]X9�T%��B�G�W�s\[�!Uhh^pQ�~����5�f�6��\��+�`äs�����|W�ҕVXiE�<�'���ǜٜ���U��G����ꋘaf�W���$eb��x5�(������\#+Ih�^��lp�M��ʝ��H�N��>��{�m7�b���ⴷ�9b�G=$3��4�o���=�6D��U�#�s��ZI'�ˀ
b�,N#�؅Ρ��##�U��ϡ�� �x�]l�$8�.����������4� X�P�6t��m��(�/��� d!��@x�/k�Hs�+怊qH$mhÌj��Уb�Qƾ���������>��,M�Rp�?��MX�.Ws��À������F}�����L�Ø�xC�� �#�СeT󔞝A�������/p�o+$Mr�a�kL���4�����m���5r��t�.lIJ`�&D!�c�phD�P}@��JM�V��/d�VP�@�����t�J5������;����b��=����x\��P*���jL	�A��V���@�x�("4 �1p�!0F�$�Ɖ����	Փ���`R����2~4>��oH�%-�S�mT)��B������cz:UW� ���.\�[���4m�KK�>�a�2�i�$�G7U@���.�{Y��P{�&L������o��k@Lbآ-�����G:�@!�VyH���И ��h����V���M�[�$�5<���H�#�k����)*�Q8D�PL����������	�Q-P���=Ⱥ�ruk��JQ�� �@�!Y� �ƨ�@�e�9�(�h�Z�
�@��ftq�b�1NZ�W�B�7�	<�3���f��>�46l%���J���n\�\�DYvc@<�j�Jgo���q���TC�X6�&�B<�{C!�Fk�-�P~ט�IT���nz6�B+U��$����h�5�	|���:0E]M1#�*Kp/r����.��s�g��輸m����i�zA�-s�#��8=t��}���(E)�q�@��x$�,ă:��D�ÂH��S,�� �V<�M�{ߋ�5�A�!�8�8�:�p� �5)�� a�q��u�2j`C����x�w���R�/�P�\5�FB}p
�R����]\�j�ʠ�C�7̊�|x�:��j�U�фH���e̫�Rs�C��R6��@�/|�`�
�x"�^��!)A��d��'�A�@`�Q[�:��s`q��EW�4p�L�4�@Ǹ��|��X�@�E��� �j�*���jLc`V(+����d$b�ĺ�#�X��`�Nr�
�[�����d�̰@l�qꁷkd#��M%�֘�vW�½+!���j��]�����E5^�%	������迶�iX�zɟ��q.�!�"���Lc��eU�
S]:3#7�۹B�H��3�l+=ۊ�{��wҊ���d�"��C�� �b���r�}@ˑ6T�),� ��4��rBR�^�HO@���������J�:(	mh�$T��C3	5P�S�	����7�;�T���[�8�-��?��y8���d���ً���)�6�,��.p�����-�p�n`y��C��Q�/����nH��Їz��q8=m����L����5э�I�i�;��[Vپ�.7�297P8ȆBĊ���Jp�?�%Ȁ�����"J�\�/[�Z`)�x)�ɵؗY�8���n�Z ���(zh�4��A.��5�����P(QH���A�Y���?T ��'�_$�<B1�P�5؞�i	�+qs�m 7�a��
��	���
aj��*	<X��[B�S�HBX��|&J�,�JP��A�e�7PB"�x!R���X�HQ-���-�<�ب��a'.��<��*"{�C�aG��L�7��k��k�D�N���"�x����a�tP�рZ`�uaz��K�ɣs8�	��A2r��̀W ��\06:�f�;�����R��O �сYH���?f�
��5��xYp����r��v��T7�i�V��x��|X�4٨`����-E��b���n8�-� /`����3 �	�<�( �  2��H$豀������p�|�����p�7���,�ꆈ�!��.�����ə��.�Y(�="����6(�H �?i�JIÎ*��H{x��h�0�(�� �s�`	�R���4J#����x"y�HK;	���P�OH0jc���9𔴩A-��E#O�9a+�8G����	ɣ1�`��	%i�v�%r
����+�+��U���
�����.<�l@�i�<���(���5�P�<p����$����DR&5	I�m� �[�/h�iX�Gqm��T�k�R��JA�?Q�|�7ņ�:(����,""�(��-֨�l�ɞ<�8��� �b��Ƈ �8��N� q9 �s1��8�Z����/ZU�X���@N����T��[`:H���-�����D�|5żƄ��7!!�Ȅ�$Y�X�Ѯ����֤���)��(� rҚB��� � �G�x���ʒ�����9��,:Q4k����ѿ$� ����h� �غ9Ec%J�=��>S���/�ќ�I�q�&��'� }Y�Q�)��]�^�FQ�����^S��؜^sٌp��*LH
O ��H�\؜:����H8�i�6 �X���k�s�A�qc�VZ��1�x4&���j�L�'	9���n�јq��Sɜ�9��Q,q������Ԃ[2)�+Ђ� �8.����K��W�Cu9Bpĸ̮�[���ڰ�'�/x���ľ�Y��4J�����H��Z��䗙͈�����]WmL��KՎ_��@��
c����)�J�6�6Hb%�uy^ĸ�MVdV�a�,�S8�)Y�YbXb���x8
�����ՐW���j7%Қ�:�-q
km�K���2p�ﬆ3|�0��[���,��]?X���h����Ր��k��;BQ���+�8]1��X�G��H�T���ZY�]kD��]ޅY�Y�^���UF����x	ex;����j 1`���E����}	�D�t��(�Up�/��諥�˟|P�5@�Ƃc�}[��v&'ɒ��ߘ9�[�5=9|9��M}��"\����]2خ�J(6��5(���D8(?.�.�5��M�DJ��H,���`�\��Q. ��  #��a�"�P�ٕ]W����0܅�`v�]�T�;YJ��L(�SXb��[`�]����'�V�����%�rd���A��+VH��}ѠXH�9��s@
�p7�M��H��3�dM���0�\xE׻x2/�_=N���c��c/��� �z�,,��q�8ІB�SՋ�����
7��6�ɮ0�l�8��l�I}��8��m��k�i���K��K�d�m
�3h��3m�đ}�&^���I�P���]U����L�]x�a]���OHb���Y�ͫ����HP+�ڃA��u�AP�eE�T�	u���+�z�iż�3��J�ڶ���FlĶ ;܆m�m�ɦl�6�ɮl�X�� ;m b��`���H�2CM<�5-�=�q����i ����ˆH��u���Xq�8�6(?���B��J`z[8��)Ľ����"W���<�4�p݈P�jl��ց P�!��Xp�X�Vo��o�W�X����������Ё�T�o@���U�.PR�1�5�f��N\C�2&%��!�!�6#Y��)�f�i�s��A,C������k��q�q:��BW<�����"�ߧ��"��#M�%m�1���Њ�V��C��y7��X����lh�l���C�����DMԜ�2��7H
6��JPn��\;����/� �� Z��N�U�)�dPc��[��Z(�[ȅ�x�\0b��d��f�^��Kw�Gw�dX�\��\��X���eptd@�]�Y8�d�H	Z�^�g�;���]�b�i�]7�������yL���tc�㛾��i���O�2���0*]�W���#]��5`W`��k5���c,�[*ϊ�8�@���_,i�g�8����O@AimH��*C�i�֨�6��Է6(Į��D�d��I�́K
&���� �f�N� �ɅK�����XH���c���x�xR�uX�۹T�R����� ��h��煄���@`���Y�Ђ��u��tg�	ǔ��U@�i?�c�3��>=ꮄ�nlj �BMB�M���9��c���_y�cxo�� {���׊nŊ>��7P�/?7x��&®��8�s�8n��̏��i�LL���I���5��z�8�i8����܍j�A���aj�@ٕ���� ~Ac�u��uT�����	��w�Z��q��y�_���J�(�t��`5�^��V�aE#2�z�_��g���k)g�5���s��̷�Hӛ�K���J�*Źfg�.k��M[XmZ�7k�豢6�M7�����#H���!I�$G7&K��s�e�3dT�qSS%N�^l��R��̝4q�SS���?e���ӧ��d���J���3N�p�ƩӮ`ǂK�,2h��-ܸpeĕ���]<�%3��V�%D�x嗯�e|����D�W"n-�̷��/n�W��Z%x	�ly+5��iYC�0!8l�ծ]�̙[7�]=s�ۙ�����m澹2g͕��Z��g�:��ڷs����/p���Vo��k�.E�-[�z�q����L�<z���k>Ϛ>�貆*%5SNI���J�� �,���J)y�хj�Gvġ��(�!v�!����"Aq�X"�1�x"�'jh#xX�A��\;��]r�0.�)s�-�� �a���}��^�$���Z�UK%�P�}%�Y3�}y�d��"%2�P��j�"����� Kn��f�9��c\p���͢�8�
sй��+�Yz�����f3�6�,d�8��j��d���첏9�Xw�6���=���Eb��z�Y] 녰Ď%��d+,�N1�+�^�����i�dM[XbpQ�S��+-Y\tչ��bqe�Qq5o�Q%�M\��kipd�C�P$\ �@�1^�r/!�@����" ,<K3Q*��`"�0Y3����e������b�g����<x`AX�����n���N=<G\p���r� �Mtе�J+��)Ԗ�COӺA~�h�C��3�����T���Q«�Г[~}�kap�Ŷ�r�lY_x�7_|+,}�=,��2;����";-��p�����Z�ug~���f�-�����b|ф=�5���Ճ��P�-ͼ�Kgg:����΃�g�Nr2���C-��R3Zz����0s�<T�
�̬��|���x�IX�ߢ:��s(<���~q���r�$g�tJWg����z�>����e�މCv��~��٨�>"q��5�:��D�\��]�Bۀ*������ax����$ .	��[�V,.���+t��ҍZ���~��e=+�]C�&W�$b���c\�*��n\����W��u�X��U�����p`Hf���§و �iF���Q����J@��Y��^d^QT�7@�+�H6����l$*���M�54�,pf����P��G;�G(��OQ�qs��?J���T+[	K�������j�T���8�І��Qi�.ء�]L�#Ѕ�`��![�L������~��ŀM��]��
7�ky�X�����d�3�`���4�pa�q[�"�9,�s������0��Q`�]V����Hn��	�ъ�I��(4����@ �@�F �&`T��*���%P��K��K�@�%�~���Z�,}���:�Ǿ������������5�:� wdy�Vj
�n��>��j��!Zǧ�������+���]���@�6d��ưo��&6�f.sve��K��F9`iч�["��)��S��,he��Oύ���M�>��-�Qa<X]�8��Р;]�TQ�e�w��D�ԁ��4���1�T�0�$��K����v������xpd #}��'�k�������.�')���Z% z]i�6l��po6*Q�kd�ը�5�q�k�EkX�0�2�����N�o^�k9���	����Ֆ�X�-(n�E��Y<��x>ns ͬ;��љEZtc'�X���.U�DϨZ4N�-�-]���>���"5�@ �bʭG��\0��n��h��f���e_��J�ҏQ���s�#���"�n����;-.����sR�5��fq�徰�'?��^�ݾY.��s,-�V���+xK��kV���E�ns�]�����OS���(=+�&��9�1�V�c3��}� [�\�.�X�rsR��Z��V�t��u���إv��C9u~�r�9 �4IQ��޶&�:�m�j ��@C5�;<4����x��2�^خ�J�,�F춾UX�=:�a�"�
�J/�q�
-@M���R��*&c��bϊN\�ÂF�xH��Hy�k]W�*]����xmz��(E]s������R�MfE-��ۆ5��V�b�N/<汍�X��V�N5�^ujh}U�Ft���iP�"���nv<������~���"�{$�ކ��A�}���.�6���}_C_����ݍw�/y�G~�w|�'>7�ݝo�����S���}��ȷ�"��n), ����rl[z�#�--w�F@* x����V�	�bG\tR,�5�k]x����>У-��%��P�_������7�DGu�w�~s�_��A?��O���|��G���/�������.����rJs o4G��J�- �8�!]�m�P`�A�s��Y�*E��T]th���� ם�DT�*��ב�5��Z�C�`�ՠ��/MC��M�*��]aO�ڑґ����́؁@᝴D���W^����U��M^ ��%�=������v���pW;4U��!�`�sܡ�����<��:����'�:�!�!�!�O'	�"��u�:"�'"�m���&�'vb'q�&j"&�b��"%�C3��9�_!V*�b�8���3��3����".���/�0�_��u)�&���)�!#�c�([ J�#24uY�\�nXc5�5�8n�n���%2�c�)�.��!n V��9�.[��>���Y?���#)u�)�Os�פT���=�C<���=����=�����a�CZ$FZdEbWG��D�G��=2[�ْ-}$�E��4[J�$L�dJ�$�ɤL��M�$O��$N�$PN� 6[%�"�Qib#�bR��(*JR��)��Tv�&Y�+Z�%�b-�CWV�Uje!�b,^�U�eU��*�e,�bYʢ,�������\>���C?X_��vՃ= �y�!�I��LJB.&v� 5�vܥ �e>f.�+e�fPg.de.�C��]�%i��hR�=���a�hR$k:�D�&>R�h��EF$DB�lf$H��K�$v�dR��Mj
q�RR�M6[N���4[tF'MV'L��uV�f�KV�NZ's^'P�$#�OK���+�c?�_�%?�c`Fq��]�9ʶqc�'b�j�+�cjG�@kb��:�f�Ҁ�O>�'�@� M$���l��v9бa�u�V��&nF�h�&e�ge���i�&h��^����f��v9d����'���_�`�@Z�a��t���e�gv�W>)����^�c�_��R,�C���=0P��C>�C>�O���j>��eb�z����e��阎���f��g�����y�R�nǟj�*�bJ��1�g_&���:�z=��q��c~dwD���ZʀZ�j�v�bǕ���
�c�*���u��u��^ZU>r�^>v��2P_z)?ә��_z����f���:&��f�ͪU��j*�&*{�h
}
�@�٥.M@  ;   GIF89a{�  (#845=:A<B:;HK:Wk<pY:mpR0G;8l:7E<Bt=DR@OE:kL:ve>QMINYiVhSTlmlTLlZgokVrqj|�8m�,z�&|�?h�5y�9z�~�)}�V]�Sn�Pt�q_�nu�hy�G~�;�tZ�^S�p^�vv�[s�no�z����7��+��*��(��(��8��8��9��9��,������
������������	������������	��������������+��(��4��-��4��T��Q��U��V��i��j��h��e��x��x��w��x��p��q��t��F��F��G��G��W��X��W��U��J��S��N��k��r��o��Y��S��w��p��:7�8<�<E�;E�M:�e;�K;�m=�IF�LS�WI�YU�IF�KV�XI�XV�Xd�eJ�gX�tI�vY�eI�gX�vI�uX�ie�ku�vf�yu�hd�ht�wf�zu�UP�Xe�mV�si�:9�;G�:P�R=�f4�x=�SQ�Ve�nW�rj�WR�Xg�oT�wp�x��w��v��z��w��{���=��Z��g��w��j��v��i��w��h��w��v��W��p��u͂=�<ҋWώo֪Vҩt�X�u�U�r��^��u��Y��r��r��������������������������Ì�ٌ�ᬘƯ�ʧ��Ù����ŷ��ܖ���ڱ����Γ�˔�Э�ͳ�萆㜥籍浬���ȹ����Ŕ�Ƴ�͑�ή����������������������������            !�   � !�MBPW�ؾ��ٷ���٬٬�٬���ڥڥ��QWWQWWQcQEEEEEEQ�����ۤPPVVPVPVPVPVbbbbbbbb\\bb\P\\\\\I[PPPJID>DDDDDD���DDD>PPPP>>>>P>>>>==7=7�����������������������������ۓ����������������������333!���ܒܒܒ������t����nnnnmmm�22�2���,++,�+1+�++++   � ������������� ,    { � ��A%��*���Ç?B�!QbD�yc�@~����!��(;�e�'|,��͑$C�$Ƀ�ʓ>{� ʲ(<�}�r�ʢW���*U�?{��z5�O N��T�r���D��]�6�!?�>�J�(V�_����u�^�/��E�ܷT���*ѳ=����u0�p��{r�!oA�"�HA�2T�0"�3RlXS6m�Y���gL���*]�Dْ</RyyR�]��A|�r�`�^.ʽ�w�S��r�i�����ßW=[�[s��)מp�vo���d�]au�v|U |��_��J���Y��g�-E���E >�D�hr"�Bm6��tM�dX���M3��O<}��zNG�n�Q�PH.u�n���݌`)u_z�ue�T��R�EP<�z����\Xn��{q7^x�Ȟ�U�V^x*8�����a����[�B�fxh�:[F�����ъ��x�L��T�R	udo7Ά�;�ƣzLM'�}�H�v�I��X�}kNu��e��X�yެ>�d���uw��Ԝ̺�ݔn���}XR{-�&`�xٕ��q�+S�Ixh�)�ZA �����T/���[M��b���xi�%Md��QG��&��QCF̤VG~���'j�H5Y\�Su+��[T�W-�*��z�&�)�}k~%����_�j�����b�k�uh߂hMF.bL�Ƙ!tX�D�"J���2z[r�QGiOQ絩�����%Ħ:��w��b�7��*֝��X��^´Z���Z-WP21����i'����\Pk;���Lm��:�Z²�4]?#�֘D�l��e�X�RkxQ��Rz����ЊJ�T)����\N��$�ƫ���QG��p#�xG	���HV��_c����j����l��̸x��i-aJfG��Պ���?� �uؾT�����.
�@���B�a5�
m�ůHQ*x����6���G Yyb���1�y��Pl���A�;u��ה��{{Cƾ�2��M�~�ސ�*�YCܮ�e?�1.t{�"�dҾ���J0�
{R��H�uB@T��1f�R���j�F�Q�9���*�"�D9:���6�A�!%V[���v���hp�Qۭv�B[Պz�Z�����gM�I=�?��i,�dV�~�6�,K�]��e9�@�Nw�"�zո��%c���1V-��a`�,h���ˏ"�IN�ؐ��*�p��ޔ�;�)�FLRyN��+���Ba�6S�C�lc�ɡ�Pv���sjR��2J�u0[���b�Z��Hv�'�\�?דś�V�V̒F�p��*�9�WCƂd $�+&1k���I�w'�D�v1�j�ԓQ8�<�he{�ٓ�)=�φ������#ѯ�v�Q^�2���#��\Ϯ�?���<[��:�N��l��������U�œ�S�0��'�׆~���kl~��l�I�Xq��^�I�FB�e�hEB��t�vB������ofI��j�Rzd�8&����T�>��PZ���<�B0P�%gKz_Urb9�6ѴXU�%xjY�� 4s�
��D��FC���ښ��˸������$s�c[�72�4�k���7!Q���Wy41bo�8�q��=ZUL�-���TJ-�N)g;���^f��&k�P��}���l����zj�ԗ��2��˺0�������Pq�#�Ӻ�Rج�X�就��+&HO,�Ff�n�%�wyؔ�I���t�$b.Q��%g:OvY�ىgN؀#���!U�}��k�`(��=�LI���Ӫ�?��b�����jQ;�
�@L�9����.x�������P�yޙ�ou��Ʈ��Q�(�o{�D��Бݫ� A�]��p���i�hÍ򎆳�H��a�~�˙g����8L��n�dZx.X>T�L�5��C-ʭzDTG�g��O�����z)��Q���R����H�fL��A��"|S!�y��Q[�F��6����zp�Yj�z4��oL�)]'w
���:Z<�,����/c�ek��B�$�
��=�o�Y�(|c���b;�����	�� ��i�E�I�c2p�o�������{�s��Ӕ�H$ukvF�d��<vc��������d��M��V�o����.5A��mW�T4=�+I͊�X(��P�ȇ2 �y����m�Y���s�v�Rk6Q�A��p���H|8��o|�g��s�IR�)�Қ���u(ԙ�v�.��Ѩ�:T�}���댠N��7�BYmO_�C�2.���5m%�D�^\�O|�*����y����=J�
?�,�N:�W��)��94����_~��#��0E�ԗ��Z1M֯&������V��!���� �"Bo�8V?��t�Y�dM�E��e�c3��-{g/ť�q��G�7v�/{vW�<_H��0�t$t3*/�)Hr1�fX+k��W~� �w~�w~���rmvY5F$7�$C�9�>�&:U4�xQ�l�m��xq��q qp�`cH�dX���c �dU�S8W�-W{G�JY�3�3e��qfU�hfq�YS"-�vr�v��F<R*�D*�X��$+t11tAQЅ�G��@�7~�8~�� ���1��YA*�7K�I��D@p�~��~�p��g�g����x�@�Q�6Z$���9s:�V2z8.y��`�F�GQ��!�gU�a~t"�\r�B�6��/"cC��y�82�C>hx����'~��x0~�pQ@  qDV��$�=%��_ڂj �����ǋ��}���hً�(n@Q!1�ui��Y��4��*Y'p��z�@�dUv"H;�@縉�q(vq/XWR�#�Ri��X�C^t�<0 &��x�wx)~�(��x釆߀��m�3iO�R�CTR2��jq�dr?��������`� �z	��g֠��1jI�t��Y�ew2I?�6K\�ux�[�2.��
a��.��BQ ;�;*"".�}/i�tS>�H��8A�v(8Pq�pP����B�ّE��G�ٛAx�0��$STCb_Y�d��Zo� h������~�����x>8��������D$�Dt�y7W���y�u%��E�Ӈ�r.��mE��H\��!fL��cW�c1�<�I�j�9*C)��xq�x����~�~�~��F��^ɋ@����iS�s�����[��g``�������};�� ��ycx��*���zX�P�vo��\�O��U~Hp��U#�V�
4#Eg:%xtn�B��G�E&�h$ͥ"g���7�9j�ޙ� :~!j~��~��@xl�����9@�M^UP�e ~����~���W�>ʋ!i��hf�a�(�UR�t��`9I��n�b7���Hpu�K��(��v�i|�9z�)<@5+z��&�+�%�	-�~��yɩ7z����J�[��X�h��gV3?�3}�~-f-?�����~}�Е:�x�7���x~���؋���6�+�q(��d�Ȣ({U�{u�hEveVaY
/f�@��c ��8�ĪWs=:�-�B�׉���i��Ӛ�����J~�G�}J�0��0��dS���ꢶ������r�����zY�-�i��~a�W�VU�#,|������&|�q"@�W}��$�k��ƥ qzQRz�*���>�j�
�ڷ^�>ȡ)��j��Y��X~���G�"���
s�V{�3,�%��?�q a)�Lk�9�j~0��6�0Gxi�taf�ڂ��2�f���#� �k�\�kfF"4�g��msw�$6�c[�cDV]kvY����ةvj��W�������� �>�r1/.�*�ˀ9٤!�r5{�
��K��G� j~�Z��7~�~�[wT?��tqX��ZKL���5@G��j���!"�2��VQ�;&�<���'go4Ƙ�Q ���T�"���{�M���k�)��J�˩�GfD�	C��{��9��K~�� *���5;����D�c�P�UNtŉ����V8�mkc����J�zE]-7{D���ZU�nO$�b�^���ȯG|�>������i��� ��)ȉבrІ�j��T�KF�DQ`�[���������|ꏧk��K����?�zپ��G��;�J�Jk��kjF�p�h�Y6��as�G:BX�<$4�	$k!U2CYi�6J��ً6�sJ�7j��l��)�~	�����#e�������q@����������7�I���[�Cȿ�w8*�l���J�'�zXŗ��C���#!T�ţ�l;�T�dj��J*�M�=�u�O"h�x©x�K��~���!m��,��i��/�zK�Jd����v�5���kΓ��C<�"��K<�����ʏ4LZP��Ϧܾ��?&��m��
��1�)l�;"g9j���	��A2�����>(����"��Y��ʯ�����xh�&��3���ï����B<ğ;ψͿ���1l��*���mc��]�I�TP����S��3'U�V-��r(�,Fw�a��/c�SCF����Sw�Z�s�>x���
~E���S���pp wCӥ,�T��l��@��z
��k݋}�=]����}��(�����Jm��,�C����e?�{l�q[��f�Q"��٣R�6�����E<�u��$�&
�Y��=����%���zU�������=��n0�� @]�Hk�'���z���s����B��������k1���=�d��(�b�!���C^`a5,�y�=�t��O�[n��~���,��T����֖��9���mH1L�j6����<��D|��{������������K�q����(o�	��3:^�N$�Vz�]4���E�!_l/B~���c#��%i�f%{_�}�nM��ݯy�����ˑ�w�p���ղ����Wq�ڌ�)N�{�Ù���Ĥ˛:
�� l@�]�j�\���а��=�!Bn(K� ��2�}'hjd:���QЧڻ��ǩ���]��y�����닑-h�8�.���@�4M��Q�}Gs���Ml祘�N|��:�����h(d��<�e�����$Cӟ]�ھEm�.�ᙜ��
A+��)ݎ���6�Di����?�cn����@��^ě*�K��8�x� ��A���}�	oC ݑl�t~�/�<��o�,~�����m�I$�ܫ���蜭�?��	a�
��g$QQPfP���H�c6����~�K-4���z�֪�6��&M��x2k�����?���륃��M�o�ZnЏ?H��.��|xO~?��\�jM�BJ�_�n��p�Umro��o�m ���g[�������f ��'*��䢬�S}Y�E8��,����rJҮ��*�w���}Ҍ�~��I4~����V?�+qV �C��(q�y����z����bą':dXq�D��%2��oq�\3y�8r��qM �4��́=}�X(ϝB{5zT�П@x��(FD�
�֭Z�h���Y����#,W<z�u���m�;ԭ��o����Z8m"E��08p�6�R��p��v0�kެ��$��r��!���2��N��{�̩�M�C����aC�->���⾇�{;\|xeKk-M�q3:�6BB�{u뿬��)4�Ҧ���	D���A�H�v+��@��@c�L٬ Ԫ�
���z
������뾛�'�Tp��kl�k*K��2s,���L��	3ES4Ѯ�D��H�;�d�i;[�q5눈�B��Q(8���8"!�h#�.��DKl�$9Xj�%��`���j�Q6���.��r��K3��(��᪬�+���.��@������?��b�����/�j�K�M���B#�2<(��,5L�z�2��4�jL4o��$KQ�GM�s��ЉL/��ͧն�M��hEc����";rI%�k2�l0�,J�Zj�%8�`#����QF]�51�DO��:�5���˾8��+����M8�ʾ� ��'����t��
��Z}���+1$;�-8P-5$M���\����[/y�u�i!�8Y�((k(�ߌnI��]6����lŃ8�s�Z6��V�6��.
ּ��F�s=�q]p`3w�r���w^�2�z�ak��"��M/B���!���=
�T�4~��;u$�C1�k<6Lp�HmF.�>�F�#�|�!zt����y戀KrI�E�3;��k^r�7�]��`j��[&
e��.�׾��ݿ͚֨��o��ڼ�><�x>}k�|�_j���>W(z*)�SkN}5�ҭ�fIT<żM��C��0�8�yVߟ���Jƫ� � ��F�|cb�l#9S��B�
�$JS��j�=���Ed�!ϙµ;���Ma�W��;=�+ҫ�S�!�/���~����$'?�H��Yp�W1(N0oޘ��*�,���,�k��rG��6����4�΍��r#��$��|�$(qΔΰ�ju�o�^��6��ŭ]��!O������I���Ire�[��������p� ��y"$�"�҄6�8Ù��bZ�b̀4��E��i�*6��5���}��Z�J��3�� ��+#1R�"���ĬD�JUK��9�aJm����G�g�e"�MT���	�xMk�Rld+D^y�������!
mz��)���K=��ST�"���k�6)%Pr�L�p(pM����\毊�I����Q�d�h8�
X�G��Lbd� Ov��)Ƣk�o�#M�2k2y��V�A�u��B*j����1��a�4�ߕ�'Q�kz��Xݰ^��А�7����ʻ��P�1�AL��J�Yq[T�02��4bt�T$�b\Y��-z�QMLyw����q�Ʉ��!&!�Q ��)�d��R� ����M�vӛ$'���2���$����?�~�i`*�l̥�9y��=Ý6��R2*������~��j)��\Y�J�`���l��Q��u����_ӧ:�p���Idx2�>vV@*�`�>ck�C����*���$1a	Q�)�q�S�h�]0כN�5w�*z˄#D.�^�@ޕ6��I��4K���'v=e���a3O�<T��O�é���ѾyLwA�1���q���( �����1�,S�l�[�6��u��yL^G�Y%S�RL�0��6�ℏjVc�ײ٘�UdS��۞ː�P|������V�҆7�8(B�����=7|�[,A��Q�Ts��1�g��U���.�a��45$��cɨjZ�W�$��0"[~Rg�DRL��p'�����1�[�F*���h�v�%c���\9Tӹ�"����f�(,s����2���gO��dR�@S �����r�P�SUTҦn��e�@Ok����^#�\���(�Sm��Ev�dx�M�ؗ%ǯ��7����R�KbN�&l�8�����h�Gv�z�Kۧ��r��#�"[Cy�U���}�F�'��az���GA�����)gzE�RP��+�cLkQ}��p4'����|��L� F���V�<fJ#'q�:��Fp��`k��ӛ��;=�S�j9,YCK4Ⱥ��!&(G9����=;��+ T��hK����7wB��-�H8d7�m���^���Pz�.e6��W{���T�~�q�s��O9�b�L�2�.3�D���2`�����cY�[���zWLi�;E��2�{ՅF=��,��ׂ���/Nac�,E>�j����KN��\)���:�-�ۭ�'�P&?�
E롫�wy=��س4(S����w
��R�I�5�#�J���ۥa1���Rc%�(��xsJ���@�dz����ɉ,��"�9!,]B��j�(��/m�=
�����l)�T�?�q��#�y��!&B�4��`z���y$}���}�$ ������G�����*�k=)x��q��'ZČ��'�V�%�q��0)&J��)�A1�C�A����+E"��`"�@�	�Z
��hЈh��Р)��� �,6��?1�8�q�$������5�
�6���F���@�v����=s����՚.vC�����#!=L
�A�(x�ȍ�B���O�7Ɍ��U��U��R36���E��y8h���8 B�!9��_۲QCB�o(~l�
� ��'ǻ±�@��RT2�Ě���c-ul)�`�s�_҉�:�6���pK���6���C@��>n�t�CDs,$Cb�ˣ�(�Ka�˵ƀD-�1&)�QB�0���ML�)q!`�(8�tm�$G��V+�S��7@	Ց�J��I��p`!�� 	�ɦ�ãp�H�	�����J.���˼��	�J����B�>����ؿR,���uCM�kl�F �91��	nk:���o���ʻ����5�}�.!���L�M$5/��YK:
	;p�5;��3 ��{/��<"�ّ���N���I��$l��5l��i����?n��,�,$4�CU;�C�`�?h���"�fTFu�Kf���7��͠[��H?��H�X:�!���H:��?�ɑ!���MT�F�PɺΊǲ�%�$���R[O�P����؍Ā��c�}��q�
1�%J���D����#�V�VA;��E�ta�1{<.54�E2��&3&�|-#kI8�4=$�@ ����=��?DM�Zq=�P���;
<��Y��=3�΃6u4<��.��(�Q丱�ڔ�k�I�O��	)Z�UR*��ǃ �e�Q�Y���/��U*<$D�9����,��0	�VasB�A6���B��?i;P2r5Us0d4C]�Tت<���P.��F�<�|/ ��H״9���0k��B�t�I�Put���I�E�,�%p�)+���)�H���'$�SQ�aV�� ���UE?�R��!�Y��@U�2��HI�<�/��/<���1���TgM�Wc�a��T�̿��t�6U}���[sV�|/	�Dk7nT�7D��B�a�C��X�k -�L�K�M%؏��� ̃(�ͨ��E¿�X��*�#���,㴇�D����WA�S�[����O��/*�Q�)�O����T[�b�L0�c�9l����W�,�V��5q���Z�û&��(�Jvᶜ8�E��G���$��C�T7�mxR��"���&b[ʸ���H��1��K���ղ�;������TG�-�#���k�Ѻ:��8U�L	aU�o(�}ţ6��=Ê!p;.�_ �5̈́֔\����9�9�C5�����od���M�#���ݨ8�#��	��=�]�����u��*��b0�j��H3��![��ИZ%;�8��������HT�	��mX���N�\	/SN<	���˔`\~ՆJ����"CE��ˉ��Q�KR�0��ǉJ�c݀\��hC����`Je
N�G��$�#pZE�`�Y�&��=>'#��
��E������yGL������3����|�aN��!�Ӹ:5�Q|�gٌYE;&j��(�ғM�#f��E�����T�@�-����C��A���C�i��������k,�x�������%4)hWEc�
F�n�E4#�`q�	
���c�{`0Dd֝�����- ����\��M%���"��h����N=�ʸd>�e����+Vdd,Z���W��"���TE��"�R+5m�������68:��D�_^��]B�j��m��D�����4�;�S���(�Ow��v��k2��]��}I=��?d��A�$��{*�-�OT�j���4�\s���L���5�4<U	/�;a�����O��	Z��a흲N���Zf��2�z�X�ՐJi�;ۺ����!(6�_��� ���z�_�V�@�kF�a���e]i
t�'�Ċ	�v#�_�V�mꀢc��
��@D�j����EE1��YS�~k��'��!�.Tn>��|��`h�=�ʐȬ\%��ۈݨ};k����d"�\\��e;
,RCl��)�#�*�`�a�5Pg���f�~��ͭ�\�.Fޝ��s�g,�G�=ԧ�D�)x�ۍ�D����* �ڝ��b���ەc;|P�z��m�EE��g(�2|Y"�')^I|o�.)�.��Ԁ��B��U�:A����Ø��f��,�ϠZoUI����������ou�-��2�b�6f̼YVc��XC7-P�02�#@��um�)�æN�@n�ʜ`������Ԙ���dD�d�.̾?��T�ib���M�%�RN�ˎ]�|��3��ȈշN+;G���[�kD!���2K��üU��KWQ��v؆9̳� ��j�c8��,ni4_�1�t.�hWSi�l��c:��-#͕�ԭoiD{Eû�g�>ZǋM*@4:f�b��h�����A�ƾ��t$&�N!Ku��t���JY�����:G��B �����˶��b��.ߑ�D���(�Y#�A,�:�H#&\26��b0Oew�e�l��w~,�~v�|f��ֵ�y/�¼4.���)h:v7Z���dGj�#H�]�o8)vT\���]*X7=v��m|�?#)�	�m����»�R����"��|今gM�}V +��w|���;���4���%�[�U����DL��ƣ)xy8@�d��1s�/s�aT�I�AI畄TZ�&���3�˕9E_Y�����h�ڍ]q�cl���p4��u�fe7W�fzq��g�wޞ���Dش ��V���`Y��tT.<]ݱ'�:X�a3���V��!U���G���!�e� �$���9��V0��?�}ˆk����f�2"C�!�`��DY�p��AX���>2"�(��ňZ$b�)R�P!"�G�����!B�ID��)T��|��B�'{��iO�*DjP��T?Fa��Z�o��}��MlY��֞���ڷkg�ҭ[ܴ֚��ѭ���{����K��Z���x���lɖE�w�ߵ�ʆ�&����n={�9.�k`=���W�Y�a�GN7r��~#���6GFt�l'R��?Jd.�jF�Yv,��aV�
��쨑���G.��S))�=�Ҩ�&B|����{�R���h��IG��QAt�V�A��@�1(�o��7�UX�7����l��U�ZwYca�d�e��"_ ���c�	`��C�7�j��`��m#ơn���W����q�U�g��5�mqTy���&א�H�G�IQr�Yft�I��v
a��C�`НmDU�uNE�vD �_��=T=ѷSO=4�eOh�ASS&�OP�d�U���l��oR��i�q�Y��jZ���Yl9�%�1b�a0��X��9F���*k������Je���X_Ys�jَ��[ݪ���U�FY��F�ȥ9D�Hŗ�.�ܚR�Wf��M�ir���gw�W���"��Hm�~=9ѓhD唀^.ד5�hc���a��QGmt/V����FE�[��R�Yg���hY�V���U�X.�c��t�0>�+�H�ҍ5k^4[9��o9nV��m\�h�Y8n���5���n�Ơ_~	=�DA���-gr�w׫f�	Z���Ii�Ux�����d�Q���h�18N*K3�9p���Hѷ�}n�I��k&�rQDQ��Z��꩟m�Z��kYpyfǵ���+�~1F�^a����5��='��"�N�6���q�V�і{�t}��gC��[q���h����Q���)?�}�]�
��?�<2t�E��$l{�J��О�ЄbLA�M�g�#�h�8W ��c�
���2��$
��M�B3�)IiHܺK�b����0���ѐ��\i��ѱ�«�&�TӚ���k�F{I���⼸L	�l�C�ހ�.�)Q�G9��œ��A��V�?~٩?���tƃ�:a�N�y�JX�*�
d�Rq�g�CbH���m��������נ�Pᄮ�R�X�&��U�#M�v��fx3:匄w4-WM�)�$.��N\� ��G�ލ%[-4�n��7��>��;�q�2��o������$p�A��y��}pd����x��$*�r��U�`A�A��~��(�)lb�ݠ�1�'R��ov���i�j�z��H�!W�h��#��Ί�6te�fyĖIm��DM��D��Zt�T�.�!�Wx$3!I����n�� �ͭp�d��f���P4y��Ǔ�a�8A�%��mH �Jc*�rP��y�� �"�U�Iҫ<Y���;D�񎗴T6�0���ᬬ�k5��m:����lJFd�@�j�.f�!i�G:ĠO��S��T�H7M{���>�&��'��iP�c ���z�$ppF5�@�y��N`�J����v�=�T	@ucP�Ԧz`��^s5�hX��!*s��QwX�!��P���:�zh��S1IJ�����7L�LX�痌sCFPo�FY�u�ڜ������T&7����d���D`�C.V�\m,o3��~�H��8ǪBm��Jb�a��!R�����)�;QQ�0����T����ZŭM�4j�S}t[�M����x�q�G:�ASY���7�_�~p���ƉG*��]Z�}pT�f@�8W҄9����2c��ᆬ��g\X�}+[}y.�@d��UE2^e\횫���C��d�_�-�E�f"�̳~#�� ���3v)��c�fʨy�/,�X�^���f���$
��I���(r�_"�!�2{(oP`��x笞�ʦ.���p}�z��Yb ڣD�jx��+��!�G?�1�&�2TiS)�3@"���L�'��
E����^%,��15�5�1i���姫�*�΂v���f���,D�
�OK6U��O#�*Np�#$ae`}vO~�=A���y����>�y�����y!݄w5�-D���D7�H��tם����,��[! ՎIh�����Ѽ.,q�CX���'�����8�Q�����{5qjF��'��l��2�) �&�"£��#�@'8	^2��*�	J���{N��Y�:�<�y�����B�d@�Bq�ȹ�jV��t�D����Ca����L�w��6`�$T�O�n���p�g���6x�پy"�]�G0��8����Ž<F���#���%X�'��B��#��o���V�=�~,��܅���^�����1���S��و��[c��\=Z�@������lH�u�/��e�&UQ_�Hl�����xXnu��a��\r�@8�C:��	X��yށ�����D�q����%�	A�� D�� ��@�I�ta�O�՜n	`C������-���֜�	 �����[U ����dh���^�y ���YH�]ڶ�F���
%�,�r�K Eb��}��!�����5D�C:��&85ď��݁��a����Y�ܖ|�@6@=X�4������PLA,�}ac���!6D����a 4cn���
` ��Q_��7�a������c��	��7�A�������Ez�����[$ɨ��J�\ ]�U����L�C:�C04�9��b�)6�Vyӟ)�TQ��ɜSD�34����`����cTҨ#��#��!�a��%f�ͽ��!���͹� .� JA)Y`7f������8��]�H�
��yu���L����, *��ܒ��JE���J�C9/�Q4���я�9�j�����=��ݖPU��`��	Ԣ3l/F�,��}��ߜ�d 3��͑a?�e�Q�v!1��$�N�22���7�U��)��z�ԡ�8�ޱ�JS6�7|�T�Q�AaQn��m@N��c?�$�Qf���hA��C:�d(�Υ��$�����<}�$��b���5`�`b P�b�C=�A�I�/>,����͠K�!i�dfe�Mza��b&ch�i�N���v�U=�HQf``�!�N�P��p�0��^��jT��W�ch��V�!r�aWR"3�����A8\�>��1�C<�C5�&Jc4��܏�5cC�%o��`h"�0F�*�e4�gaB�CL
�d���~^)���䌂�f��<�i���QAg��"�NF`�0�ϴ���!�r�l���2�\�����8 "ROn��U�oH�m�i&�4R�����D8��>��8�>�C6X���α$Xbص�����%xJi���y�a��4�8��9L�4�'��i؜AHc
�qR��e�$Mڨ����P�b��H~��b�N:h��)��nc�mhlF����a,�RZC�� ��L�M�o�oP��TJ�a��(�nX��h�F�>��>��;�>P=\���$��J��%��P|�P�d���?���� nq'�� �gzf�� T�%��zRC64�bLY�Nh6A|�8�AJ"�����b��:�{�)@���#J��K��iv����5h�e�$F��+��v ]�E�����
�1��X(�d��F��Ʒ�J<K�d�b��� E6R��=�9�=�;ԃ6�c�(R��j��R�o������j�N&3��&G��Vd�D�/@B`�*4�\�5dC���k]�b|��Ή��dK��v%e
����,_����A֖�v�2bkhRAY��Rv#`�^t%�jh��ێ�m���7lƉ�m̐�YY	��֔Eho��)W(�I�zg��Q�E>���v�(m�.#��=�� @}��~C�����,���dޙS�e�(���HFR����P��
�N����=8e6��,f����0�R�8U��Y��Ł0X�0�bk���H��o왒�=ݠ&�A̜Ha<�YĆ�$Y<K�VRMF�
 A裶�d�j+n��
��B=�CS8�ArDbN6�nY1RMA��F8D]8�����%�ԝ��\���%
�6h
�����6��@�c�pHƈ���1ifk	we������ �E:���AC��&h*A'������k���+��R"Ɖ�c[���,�h"�E����i*�ˡ�r�>4.=�g6H�5�3BV�iWN�Z|=��8�І.��,'cr�bb2&G�J�%.�D�fh��b�&�u�L����ND	�VJ�fXC�o�n�B��.`OTm͔��R�:ƊX� �#ӊ$,^�(�f�-8�Шj�匁����*K��N�܎� ֕��ѩ�,�A?��3 ���nI,5�C64�>P�5�Fern�.�j��8X>��;�L`��Ж�N�K6o����M4j���6vc�Y z�Nc�,e?�LX���5HLe.� |J���ve;��T��പP�� �K�i����,�A������c8S�����Po��l�
		�m��نo&b��"�k.�)�u?)fA0�<O.=d��c���iF���ŪF�|v�mਗ਼2 v%痌�Ǻ,EK�%�'��"h81�)Eα�=�.�l�m������pv��H��k��mRF��ʕ�h��J��F����1]��E*U��v�8�]Au���a�`��������m�`¾�2+�F�HxC�_@n=@C�>�A�!�����
=�Cy��nLz�~��aw𚛮N�%W�3��.&��E��6����D�v��/߳�un�ᶩ��̊�NڒL{�S>2&3�' ����������U���c��]�2	o�P�ٶtMk��f������&PA�5@]>Py=D���dA�va �����{=�aL��Ѻ�)cK�%~�_iC�%�v/fK���q3�t?	��.��U���*#���YWZ�4�+�{C=b�Yf2��P���g�bԦ5����f��۬�EXВ��]"�xT���F��Fq���\�?]�Rl=$�d�g�|�\�g��5:�w��ŷ��dx���C?i~ߨ"h�#�;�qKk/cZ�S�m�f��&3'� ��Ϩ �H6 	8착��{�x\H��Ʊ�̢����7"Z*��?�޳m�������ر�;�
��8��T���m������r/3�{�vg�A'�F���5�r�_��<OV�X�[X����U��V{�Zi���}E.}
=��k.��xf�����(c�8�R{��Yx@.F�4d;d�2?M���<��o�i��O� �����-�z(c����8e�i�!�q		�xChɖ��̋��v'@LYRd	��	�CB�9�̱�hNZ�}��1x�$N�%	*��Dɔ8ߠY!��
�D����HM� ��T��Ϟ?_%�� ɝ��,xԨ҂�Ip�G'J�����G�	�RY�I��P���*5�3UѤE37�.=v���.�8�|�}���Z�k��\���͛)-�4q�U��yw��fΟ=�z��}�Ks>}/u�{��Ɩ=[�7oz	[+|͛�p� ��f��p�rt#�'���̙7n�$mWʍ�V�����Q��7~|��q�asd�HM�����YoB&�,�d��~�j�������'�
DP)�$駝�駧(��*�H�N����n��d:�*���Ϡ"�P��K��$�͡f"|�a�6������C�7���%�"�	��q���DcM�&Ms���Pk�$��2����-/c,o���p�Ü�f�+���`Í(��	1�n	�H�s$)�!�#'�r�%Ǔ�D)�`o���"�8�k�h�S�PT�fJH)��Z
)	EB�@yBp T���[��0�>úp$�j��86q��M\�I�r�b|��i���
)�������� #�#��K��FKW��؍rJ֪�������^{�ʫL��<��n�KG���W��8c1Ø{���p�P�����8b	7�$o<p��J����
�$�Y3
'�piCa��eRc���cM0К��A�rb�����OL�W��i(1ǐ`Jq�Xd�q��g��gU��'$�k�!˒l+q��b/p���$׍5x�2�t���,o�;I�����3�N�3�+���z�������7�2��7����k�*�Y�z��aC)�и�&��{��B����Ы1L��u�T�&$ph>�J���@%U;�)���G�U1�)d�CƩz�q�v%��aG��UqF�����̡����\�۵s�t2o���WK�H�s��/{9So�&o p8|��394X�h�B�&?GQ���@�ñ�ԣtԨG6�*\abUq����qGC�	�A�B��jU*
�|x�(i Z��n?���W�UT�Ll���G*6Q
s�c��b�JQ�L��JW��D_	A��Ҽ2#&�k5�z����$q�p~�Kl�(���L�,�mw��c��(G���׀�u��k��o�Ғ2E)�jc8�HN�C�}����vuYVb?�!��7�'��B�	TjEK'�/��3�v^"�R`b�0G)�HudY�`+��Ld�Y���xY�nR"��W��3v�����.+��q#`�.C77��6d*n��3��9�c�A)�'�e�OR���$��P�I���F<9B�������p��Qa
���ؕ#��
g�$�2�V�A)������]�M�u �%��X���Q�fN���`E���Cb��`�� �&�����h`S/ҼJV*k��67=��~nDW#0�1�G�y�s��9�頌���D!X�%��G��7� ���E6ꑣ|S��7����W���d�`	 �gIvDS����É�`�ˡ��VE��f�U?$�S�$����j��@4� A�Ld����	d۹���}o�g��&O�n�nh��g��]��E��y���&�'1���^ـ����}�K)vpx�o�J�� 8��Q����(�j�	q�	ܐ$;8a2��� ���60sT�!�N�yB� �;���fO-,�(X�uL���p�R���D*H���b��F, ��b�S%D	���b&1x�/ �yG�.j��[�x%��I�H�8k,�lx����W4 )
��_�;� 	Hq���� �K)J�Ql�A10�o�p��pX`?��4(�{�#nH�Q��zܷ)��,_�Ÿ��Yٍm�'!4c��hD0D�
CD��Y��|CL�8�P�j�Q���� o��n74L��f��ݿ��5��#_�z�k�0�yd��9KA���;а�8\Ak�7=�tq$�4��F=�`YC�y�,���`������U<#̀��Pa��zI��F�����$Gx��J�\�j2Q&¸�b��P�&q�Q��HL� �!*P�l2�QVu��y1-�/�체��]�b;f�6 �6�� �L���{��8�9�ECG	K4������d4|�{����[���C�uF)mCj��`.��.P����X42ơ)hfI�X�F����ܵ\=�ɯ@���X@!CmM5S �� D��J��$� �{|SR�^�v�O�4���e��դ%�JY�Ҽک�Z#��\st�-6ġo>hZ�b�l�q��A��`��η`�^��z���V4������`���$ ���������4l�^��LN�>z��΀�+�@�J!�h��R���bo(���� a�(McJՖ@
c/d����ԅ��O�ī5Z������7x07��ܠ�ଂ�����(����䇱N`�H��nL>	�����1����6)�+~��
X!^�F��G���a��a�����!� w���t���#"1�� ܀��� N�J�AR0aΡP���6�+�2A��Aʠ�,0,� Ȥ�p��"]��۲��ċ�-/�	�g�xĽ��z��.����@
���p%����N��H@�n3�.J'�b���H��,��RA:�F!���a���a��@��!RD�/p!�t��l+2AH�� j��F�I�,Ȃ�� � a,@�,ƚ��aS��A#3��Q�Kܪ�pr5����c`t:��$�������L����!�A+� ±H�N`����܀$�@���0��)Q��i���������!�����F�!?�	&q$��V��~�Ҡ�4!�A�A*@����,N#!�� j� �D!k�g�`����O�j�3�nv2����L�0�dL�2����lr� ����O� ��Z(
�R�!��"��/��F�r	� ,��L���h����!����3�a���������� ՠ;��/�/#&0#f
�K4s��Pa�a����#Q��A`�a6�$+@���Ȇ�C8�7 ��-���K��O6�k&�o��m6�+q�D7�L�f����0�*�@�!܁
B
ĠN :O�7,/@�0g	����L i+
��=���A5���!�4Aȁ֡�?�S?�-����ȶ��0�4�4�ҡO#�L��+�6a��" *��|*	B��n�CD���i��n���~��z_�#(MG��7�
�ڠS��O���� 	��9�!�A���a��$��@�ӊ �!�4�`V�G����A�����A�����a�Ү+�4N#q]��O�]#���,	~�N��5|l�&�`F12aP�j���A�^q"�B �h��2���45pDcI�o������o|�TkC/�$�9��9v�U[��T�8����!�X�9M ���A���XO�$�YK����W����h���U�a��5��!�#�a>����`V�V]� ?��]�0��m�������lE��*1�!(!�R��!�a3AE!�3�D!(A�*`�T�F�+ �
zq^�+��6GeTY��|��DC�b�d�0�����
�������X�
��\Nc�a8�`��4���*��$HG���#<�.�j5�G� ��Z�!��"�Z�F��MۡM���O�U�m�v��:�n�V��m�?��>�O���2�!�A�!�DL�w0��HaF�q[q�L 
$�_i	� ]�%x�KbL<UCJ��\ȋ��Ώh#�M�N�,
���(��,h8�ln����Ѐ�A��Y�ty5���c�AD�������lq
�@����a�$=�!����,�TAP�
�w$��?��}�n븎Y-�Q}1�t�����A�4�ƵXFq�4�27���6AL��@w��J"/�6DP�3��7`c��mL�/������H�h�o^7��7��4�G�`
�C�~�"&�@0XO��!�7S.b������!=Kg<�x2��X�azy�*&�,����A���7ڡ{�|ׁ���@}��k~�V��N�~��}闟��:�b$�C!g	� �`.���L#?���a��G��.����+Y6T��z��<�36��D80�O�K��̢/	��F0#/���
�p��(���1��N�fi&���R�9H`X��*KJ�u���SԞ�#��t��4Gt�f�.��x�A�ᜣ�|]��WAN���	ΠZܶ	����}����Y��S�)l����H
T!4aj�!��"�f�!��i���E!�K,�,a�I�? a�d8���/� CSCxKRy'J��c�
K��sE�g/Z�p^��P����.�Z����[&�O�3�zX�rI��5�X��h���H�Q�A$:8iѐ���zš����AX�Z���Z�YV�V�Va���	�:?�%�`���m�}<^<~��9 t'P4!�-�0s�2�!��`I�q1����I��EA2�*!T�b����s��K0��˭�i4�$d�d����KV��L���d8��_���ެ"�:��Ѯ�#,�6	����8��O��6��ƻ$��t�������4�،�A�\�|��Z��{��']����
蠞�m�����}_��bґ�P0��Ap[/�P!�x��0��`��@�2\[\���!�9���A/��8n#���su�\���K��9����f�7ƭ�f8S�m��l�
�s\�*� I6�*�A�3��͙����!���Q�a'����z��B>����ܜ��ف֡Z�С���[a��L@N ��W�����x���~#�~�گ�����"��A�5�!�AO�Ȋ�	�r-�/��_��19։���T���;v���=�Zx��?��x|6R6�Pd�,S����۞]/�d`�+��M�͂�X	�
�@/��ԏ��!F�!�F`y��{ݜ�{	F�L ;��H�V
�A�[���A����\�������A
���3�>�����/<!%��
� 0�������!��u4��72��~��f�)��)��5aH����o^O~���jfO_
��\!��%�#���Y�a�w=_�^6�m�&e
��$���7�9f7Ou��G��*���
����M	��*�X����" B�����޽zٚ�{x�	�8ٲQ��-��%;.a�c*R����Y�U���j�/�:��\�Z��Ī��I�WM�ǎN�q2iҡKA�ʴ)ӏL�N]�$�43M<.}�$
=7K��m�DI'S��:V�*R�J�*v.X�r���25��&L�5i"�7�(T�0�jL��ȇ�#U,X����9����%�X��*S�p����f.Z�r��ѳ�͵��p����ċ�~�xpq��}{'��8׼Yk������M�v-zx�ϻ_��]�*H��G�T��g�	H��� ��	��;�dS�3�c4��5��C�in�#�5�8��9و6TI%��`ӊ4������B΋J�
9���*r8�\�R#N%�WD:U$UQu��l�T��YGP!�S�=d�&�q�!�#$R0�Cb��&�T@�b�5FX]���)~>�I���Y)�hR�\� J��9��T1�pd#�9�hc`4�L3/�DSA4�[3�P�oŽz�pƵJ�=����7�Dݮq8����ޮ��J�y�ys���9�GP3N*Έ�����Q�c�@#�0=Q��#8��sQE���5�P��8�|�#XRt5"����r\�b+��hB+�8����C16b��dY�\c��MY�S"&��C5�8�D�J�.A�v����a�,ָ1f�g���%�SL*�n��(D����TP�]�յ(a������Z�Y0�vYuZ�!����&��E�fܶ�:���&� 3�<4�K)�D3N3�4�L6�~�o������v���s��J���8�+x���Mxe���R�S
0�t���T`����N6Ό ����̸��Ca>��n���#M3���^�hD1�HIt�4/�rB	&���U��:렳J�6�e�SYU��g?�����h��l͔�/*�!KG��Y���(<�,���0�	R��W;��0��`l�PE���'L�OZ3&�(5��P�8L)�
��8�!2�qd#�F>С
t���<v�X,c��F:��	D��0�r+�jq��"k�C�8�Wb���*��o�C�q�u��98K�l`C}8��R�"�x̊�:؁
Sp�A&4���^�8�����#���%��P��pF^?%L�+"�^?)��D��C�$ �3�$f*�*:��uPB����	�*��T@6�8��6��%��X��q�H��(P���L�悉Bh-\!#�
�1a6a���	P�����-j��� f�
��jС{�W�B�E0�aD#&�I\<H�r�c� \9+YG��9�O�3֡��R���F�f�W��u9��lxC V1�3�0�HހT;D�z'�s�d��`�A�!<�Ahx�X#$%y���e�OqJQ�"0(E"c2�\)	D��eqC6\�
�R�q���̐�L`%+�U� �s��/���	
�_�l(W�B}�x��JG1�1<i⅚Ћ&�f����1�&�FBQ��C�fd��( 	X@,`CD� (�����a�h�+DQČ�c�0"<���r�#��0�Q�����M��E\y�XǚΎ�F4^�9�]xr��5�':`Z@�k�8D��Y*����mt{�*�m�#spr5g�dE�A��������C&�a�s���:x�+�t�Ţ�K&�p@�˘���s�<��JV?�9�!
���&(A�
$b�����֏d�	d����i��G��!�BF���"��B�j��S�H�����Fk(MP"�]�"phDW�P��o� ����b9�+R1
|Q����;4��%�y��2���e�t��uU�b���PN:ɂc��s����Y�r�t�tٍl��D$��%,x��*>�{܆6�V��`sh[�f����S��F@#��P3%��
j`Ï� BB�)o�U��	���հTf�b�>$���&�B3a�
d��D L@�(Dv�NI�3\��hf���l)֜�T*��@ Q
��Y��Z�7QA4��/��&*�[DG���mx!�
D �؄BЄ#��QH9NQ�S��h�"8&ds�C��E9��]y(���"�zӪ�����i;ǼC��:�l�5�6VF9��uQؒ���qE�Y�!�GN���X�=(����o�4*b�p�z�A�1�1���*'+rT��~/A
f@kR�R����J�)G�*������80��>F� �Z�0�g�a��s0<�T�92�H&F��Zj��s:ԑ���#�Ќ`�	|�& 	p �y��$�`�&!x�C�`E<]q
����EW��Pðð	� �p\�Qe�*h��F��Rv�3��,�,�!��,wgS�qR��P'��	�7�tH��ΰ
�S�RUQ� <�U�T�v;!F0�32�W{K�KƄL!R0%9�oP�Pd:&����d?K�N�py1c�	�Pe�TP:#�q_Gf�p�p�f(�	��Azt}�P��?�W�3��p�� ^��7�`	��~�% �spR
� P� �P �O�0
��m��U�qE�@�P� 	�����@�sE��j�`^ر^��F���u+id3�s�9���]u.�P"G
� �!�KopU��� &0.$.�.�r'`q[�U� G�t � p`t@O@V�V��YM?S ��KjU{�cVA��p�4�>T�7?T?Fb{ԃYͰ	��}��|�	�@	�DH�O�͐A_�M�0~p!}�(�P�`��p�V�@D�W�@�`����0�p�'�3��[G	G�	�� 	w	� 	js'�	�`�0�
�X
����QRa�p�P������R�����x�k䑂�q,8Ә�F�^���T3	 30�30˶���$  �1��'p�\u�qqP/9(�P�@bU�:�P��o�c	�o�{��9oU��:F={9V0;LL���dFL����g� 'c�X5k�Q�_���Y�pA�(��N�P{ *N�R��F�����dj��7}a�� �g��0��0HI	��B���ؖ�%����p�����0�`�\7� �ġ慁��i�#�+�1F��E`�F��F����������B�0<P�q	9�H� ��U
bW���������P:��&.5(n0�D3��O F]oM HT�Vꡜ���T0_ɩe!{��1�6L )�^!M`oY+L�	^�	�9E>6�BQǀ7�@4鰔jB
��f�|}�5�P������چ�*�Sy��������ВW�0��A	�P�|�������B�P�	�V����� ��ˀ���cR�!��E���;�+�J���(u��zA�����%
C�	�ǉ
6{�qU�w����I�p<U��x��V���4g� ��I�Q��	l�٧x��:K��:&/+��	�R1o���J��_�CQ�(�M�A���q(� Ͱ�d`��`��g����	�T���B��	Ri��l��`���
����|�7� S��� �7��	�0Pe� �_	5����W���0�@u9
���r��� ð�P����ƨ8���E<
_��k�q�_�k��s+�Z�q�� ~ŠG�c��_ o���x��.�{.��;��l���p��b�
{<u�Dn@��n���x���F~ڛ)�TP���}ڧ.��8��93��z3�JPo(�z4~���(-�	����ۋ���gu���'���N)�c�lb[���K9~�{~��{t������	 ��`��p�us��	�p� ���
�p��xY���ɠȰ���`�r��8E������+#�F�v��`�����9?Z��B !�|8Z��2_v���n��T�y.�.��ئ�� ț�ΐ��vt��33� Y�:�T�	pU���[�KU0{�/�����i�9ۑ���T�KU�K��\�Ҡ���:N|������U� �	KIrza�� �%6�R	��m�8�9����g5���Tl�P�H	��X�����+\
��Gh
�@�0
����H�Z�@�.���R�Q��3j��9�R��q��p+�Sf�����!UҶ}���P;��+v �x���چa��`�d.`J��P��� ��a��*m��,�3s�롛�ȄT��4mȋ�)M�K:�o�첈J�֠e黤�)��;�-���Ј� N�E~�PO'�������	���0��� � >�����}����m� �r�����N���LHkj�	}`��pcV��	�F����	z�
�0
� �\���0�ϳ��q�F�CZR�a�͑�ł�euЀ�x+��^��E��(��
��	v�i�@���xX�I����*������͝c�x!���UqPr3�����o�3~����Ȁ,�`2��v�3�7-�4-�MoqZ�9�g �3{Խ��>]�b��^Cϗ�pA���	���_W��|� 	��Y� ��'�� ���TI��c�a�	��(^�}�k������p� �$W��	�𴚐	�*
�PF
�O��	�`C
������ø����+��JϨ�E��E����.0	����}��AİB!�0��(}�!ߠɑ�W�U1aao�;h�.5��w9�I5� ��Fq@b�9HE�d�u���%U�t�1}�4�1��?m�ۻ����x��뽾Ua�p-��.۲�+@�A�O�&逪�1���� *�(�5X}`�	� H�5޸0b;�ڶQz!��̪���� �������̋�}����}XS���P��A�O<^�0������ZR٨�1��x������E�8�3Z����WwR�����j�~ވO�W
?R���`�b.F�|~��0!������ax���Q3��93�T=��;�Ա᭛ i�>ӯ#�z�i�x����.+U_�bB���-�T ��A�@�d�����P뤺��ߕ�T��A��X(���R$�|�e��L	���Ү��X��	 5v;NC�� 'u�6"w�q(�p�^�0
��B�1۷�ׁ�xy���S���������H�E���*b�E�-���E��`kys�GH�	k�qı����o<��k|�C7�H5L��t`H�I�TH%H}��`Ҧ�~�1��=OS����5��E�޾��*-�K�+`����� Y��r�i
vlX0R�ҍ��(X�D�6�ڤ�b�]�x|oX�R�
	ڴ)�`�P�D%j�֤��� �ɐ����5�W�q�ʕ��/^<��4M���b�=�0i´g*�`���tHO�s�Di���(Q�R��)S�Qq7�k�/[8k������{���n�=q��-f��cƋ���l�q�z�ӫG��r�J�G9\�z������q�6�BU�)M�0a��������#�=v��勗N�:���.���l��9��=;Ιýq#�oq�ġ���7v��y_��}�q�Щr���9ޠ�
:,���p�'�����B�*������c��������C��ADF�	F��ʱ$�`�	��,�d������c�:h�@Y��rh#���DQ`�q������*�I����)�މ�qJ*�J���MH�-!�Jq+���9E�M��䶄J9��C���QDyi���.�� ;�0p�1m��3�1p����J%�zT��3.���� cLR�2;�b"��bPQ�Rh�d�l�h���u�z��ʡ������Iͯ{&�F����泃�8�#�8��<9���Λ�=;�c/����b�7Σ����CDD��dbB	� ��8����z�O�8�9�Y�	͂M�A(���x!e-��t|ܤM���h�D��b�e7Fpf  HNnd���,GK�ʱ��r��$��KB��B��.���0I%�TF��$L�)�k>�e�RJQgU����l��k5�(�,�Ic��
��gW�,�{�9�f"�`6�5�t0�3%��@�3-S`��C�d�
�9a�ǂ�/|�i��fTa��`'��p���.1N��v��o䈃[;��o�I�:�A�]��[�>:�Cz,�����@����
�
��B|7�i�{��k��f,ª�c
&����H���H'��ň��/+����f��?h"���!"�$���;���v�#�H�Ҡ�����E�Ⓧ�I#�&�����p�9�ᦴ��-�8���1�b��B��3����T�n��*��F5&9Y��u
G���
+����2shF0z[��0��T�fs�B�mv�LT@�p8���b:�PG'@�	H�$b)�:���H��w��;�
�9X#�hF~��L���v����Rʯ\�yC������`_�����?�C%y�S.9ȡ~��IBHQ�c�D)q���))Q��L<#���J�TM��n�#2(^��Fa�:����#W:V�J2��i�B�G���s�y��9ԁu��G��B�M��i#&F�VT���4�h��S��ǤC�ǰ����=��ҕ�SK���@I1�F` �(�0�Qk-
3�!C_c����iB��jZ
AU�pd68���0�5$��jWO�H�X'���#%Iku�[����lPԠ5��hd#=�C�HY��ȡz�,W|�����^{C�[\�0�.RZ��RS)VBQb����PfM,�"IC�<p�"C$"��&0f��?$@�����
h0vŀ*��h4"�8�P����P���H�6��}T䬩>R��|1��8Xq�h��MmA"i��L���`�5�Z�P�j3�9��*ev���`d�!�r���C���H���F ���O0��H��qh���D�i�)K0A�Bl"�D3,i\F���6�U���:�Ȍ���T�;�@�U�+p�v��;��z4�{��_GI��AϯX&ea�W��U�j��{0Q�W�i���׾��� I�h�$[��VM�Lb�Gl�#���~��Q�L��JƂ��
������� H`q���*�	���P�O1���B"�9�F�|�#�MH�Ps �� �<sM�
t4I;�&X!��@n����d�XpdԀ�
G��̘�0J;Sa^4����9���T8� <����o�����0��>A�4�*zL4���`����Gr�#:�V���ˌ]��f��bC��+X�up�͚ьiH�ݠ<�l�����(�捿j��u��� ���GA>������mϖ��0{��WoXc>��4Dsb��0�W���T�"
^�GH��D���!1F����h�<1�Q�� A=�!� �NG�<����D�D#X�̴�mѐF���#����5z]��}��қ�uD���ef���9���V&S��F���z8#͈Tf�!`vp����R ��ix0��,A�Z���1�4z�p�N��PR1�ܥ�N��~Mw��q�#�&(�4�"c�#���	�1q�h�:�X�bH0��/��8��H���$���i �`){��f �:��l �#%��97����r�+H�$s�C:����1�+{:op���h�b8�e`�de0�_��YI�T�
�[�M���(x �RH�J�AЄ�S MH�M0�<�S�Q��� ��ı�
�wz�,	x�q �@(���ي�� J� H�ü#2�q�(W�v@5v@��"ڄb;/�)!���لh�6m��@h��n؎퓆π�� ��s�mSz�[���{xH��w�M0�^X�:I��^(�N$�{�>hP�T�g�BC�s��(�+��*V��� $�8Br�z��j�P9`��z��$#�{#����H � ���[�} �{���G{���L*q��Aq��Ű���z�Gp1p�$x)��+����l�F���R bP�y`y(�캯R؃`؃Ght�yF�[���#AX�
��� �@� .�B=P p 8��ʃ� �)�i���p�p��Oʆ���:"r@r@=x2�8�}�rPW��<�Cp�����= ��;&L�L@�M��B��(k���ꆹ��n�D'1�jxz(�Mh��J#P��\p�X�w��Ş�h��:RP/OP�����{�=�G2�H���J�b@�t��f��C��p�1��F@��xH׈�}���2�*1.�2`Gw�Ǎð}`��|�@aiL�o �o��:�iS���o�6hP͸
���k�}�ĉ������|+p���q��Q�e�y�}Ї��Ac`	[)�B(�
���
����h��=x�ڄ
ǌ�i@Qj�j��=��><��p� /$����lP�
� �X
��
��@P ��-��!�/��y#�v`ۄ���"(=Ѓ���/��b�˰L8���\Єq�a��n`9N��g /89j DCУH�#U�^؅^�]H����Dp�m(w8M�SJx}b-k�DcqR�s��8$z`?TH�x0Si@���f�G�C�lH�}0k�1�?��8�;�� k������1�+p���f V�`醓��8�2�NaL����o�j����r�}���>k��M����h����f@F'��f��\�Af��y(�A��::	'��4n��J�f�����
HK��w��i�M�h�N��=��K�?AN`J�	��F��} =ڃD`�M؃!]�R B0@��Lx�Rp�sh=lQ��su`C� �$��H��.�*�.%^p�
����+`�u��0�8�[8	x(�B@�s���j�]��]؅�� K�}��^p^��D���`0����U-�/<2���T��9�BX�U��T��$�xU�ʆ}HFkx�@ꠎ}���oX����19
 �g�+�t�}H�1x��u�g��1�x.�.Ђ.Pm�G��+��g�0�z^�BWt��j��q@�� �RX�]0�"܍@ ���
k� @�FУ����H����j�-��; �Q脍S@<`����� =J1������
p��@Y�R�A �
�B��<�9�RPJ�t��z4@��<�c(H�Q��/��C�Eh�apx����g�� ��pڑ[��Kh�j�G ? ����}Lp�j��D`�#�>ɳM�8!'p��� 1^�|HC0�p���2v���]Θ��[�l?��a=/�輂1`�X�.��.P�1X
� +���r)}�+z�m8�ۥ�m(�n��m��xXu��֒��zhP)�l�k@�f�>'�8��:���z���(��}�������
�b���3M���� ��K�f���#=bQ���L9�+*z�<8R=�BQ���� �p 
'���2u�:�as���K���R�(U �@�sp|�J���s(��xJ��`�`����h��s����I���Q\�^s0���i���0JT�w@.s�6�[�Tp�`8T�)��,u(��� � V��6>g98\j`��8�b� �P�*n�� Ȉ ++H��)� 
��$���w x���7�{x�gx�Ƅ}�0���].@�Ұ*g��{ش��^�A1��[�D{sA9�p ġ�x��Әt�x��d:��5�� ��z����,s���4��p���`P}�A�
V�v6���<0'��+�!���j�[�}�7����3��L��ԁ���-A��Ch�cڟ�y�H�>����)�ڄs��D�D@pI��-� ^����hH�Y��eY����4LH�M C�r�̉r�s��cR0c�B��\�X�\�u�o�ꪞ�0	���a1�6�X��8�f%�s���0]
�w� (9 ��+�Ȇpk��g��y+�P�6�u�x�p�1p]
�]x�p�L�U ��C�}�s_�&k��t����H�7�P�MHH�G�@�XR8�x8�������.=Ҡw �E� ��WL?�����?� ���	֣A�AHu ��p���R�`C�J��s���s�u:��
(��4L�`��b����A�@���	�O��h� �� I��<J��X>y
I��e�F(�wx��t��q�NZ���Ds`��<u��Xa.s��5���??ΎC"}��h870c��XI2rv�G|��O5�pV`��+���� p�֥r �  �������1x�w���(��}�1`sXz�HE�1<�sj�lW��4Ⱦ���pv2&���p0����-�*�G}�R8���y�t^�'f��ѣt�2q�w��h�SǶ��Ǽ�
 ��>�����#���6���aS��H�s�!��
����|؇r��*}��)�1�Z�X�<Q��لQ("@��th�q��G�fI�O�_z�fjn�R
xCwI(�hh^u�rwH��wi`�y*t%�V;$"�T����x�����q�WIR�5Ű�50c�<<���[N�`&!��'�^>���e#A"�+�\P��A��(n���ZB&�A{��ųZ`���5���c^��ˋ�^��գwo��:��&�Y�zj�Q�FO�>|��Q�Jmj7j����7��cÎ+�&C��1=��T0^l�Ƀ7,X<^�ʙ�T.Z4K��ٛ6�B�y�*\�7��bz����Wo�}�rҳ��¦t��$jO�
�I����4t���S�O6�S�Fa�Ь��I��Q�G� ���eiР?�J�KW��9L�F�sL:u�N�%~\9�xm�0K�%K�,�b�(Q��(	27o^�a�0\ 	/��L:�4CM�FI�
=���8�@c9��K9��
)��w^<��5
� �&dN1��8�8�c���4�S�5#�p�q��c�	#d��5��W�p8�dXa�d�!T#�]X�4ٜ@����-��2�-�P+���
{R04c<N3���t�5p�p8pdCM3����;�<U7�`@�=���>p��)�lRJ:�Pc�0��L)ä��#絥	/�P��%�TVc�b�=�X=���5􄓍b�\��9��3�9��c�(y0P��&�LC	}��3Nw��Vixѩ�8�����&����&�l���zt�&�3�9��g
)�3�t��+�L3�;��3ǒd|�����4�`P�^渣r9����#� �$���=�����Xc[��[�����1��c�7q�8�sN0�]�=�`ڪ&H0�c@)m��93�	�8v�A�Q�N4�@#�5q�a����ΰ�3�S�0Ì��65��BK�������5րccW�(T=��
bԓR���n�x���,SQs��|z���s*�	�QN/��2.&l�&��j	$���=&�>�T��O�>��O�^&P>�4[Y6��V�!�D���bN����T*�͋%w�p�U�R�(�(P�	��"��p�&&
S�:��)L�����<���-&Ѝ���L��>�q�#*��#j�b/�9^�MTl��E9�Ѷt��Ј�3��@QL�-�p�,&Q���k��G��s��V�>���V]�K�@N�d�"9�[�P��쵏|� Q��P�h�8C��h�jiΐ�W��]ԤN�p#�T��	_��dr�Ѓ���L���U�WpT.i�A��P�	0��Q�^;1G9�r���"#
�vBƼc/�YU9�`���T4�q�c0_���T.yIzT%29ч$���z�#`�&��U� ^.!|���虮�G��F��<=�
�m��Q&�q��5:�� SYl����,$q
` *�`J<���z�����8,��f� ݍ�PV^ c,m)�4𡔽�-��W0Da��Ib���B3�k��b`��E�f8&���% ��(�&�G>��z�m��p�@�'L5�*ΰ�4TQ1���pF5N0�g��x�d�=a`� ����1��LE������p�A$xT6���!�b��5V�Q
� *���$*��0�T� �1"z�^f�[��X3<4�r���&�)�.JJX�^6xQ bz	� �\C��b���	u`1���:�3��R�F]�8�'L1�s�"��m�&�Q
=�!���4qR��-hq�R��8�9�B��*�m-s�L ��R{��jh� 0Q�[԰�h�5���hdP:ԁD�g�C)Q:�_~tŏv�)���4�!�X��^�Q6���H�*���J@��x��09���W�n]�*�Q4e��U�a����$����H��+Xc#M����@�
$�����B6R&؂UǺf`�'�J>��-��)�������2�M�"�{�6�����(5�3�h�(�D��_��:\�]��B�o��������8G*�-�	��W��'Ju�"�ф(H��i\�yrA;
0�C�x�;��a ��pǹ�A�h@���6�p�3�b�N�;L��H��q�)>$�q\���02&�	a��0;dS���̋��TA/�=Fj�R+8����jHR�)=+�����\�	��5��GҤ6��zq+���O�yj�#G�r98�0�s��(�F�)�㳜	�i�j�G4.� rn"����+�M�OX��%����Qc�S�<J]�.b���+b4���
��0�5�E4�E�C�U:0�
i�= D 0��q���@�A1A
$j����#�#�R���H�9Б	b�
�*PA�m	O���E-���]Ԫ�
�E/�����[�B�;�33^,�ܗʆ4tk�C��_����t�	 Ŀ�)L�\ ᰒ3L�
���|�_㌞P5��9��p�X�E�4�U���3�]�Ֆy�	�B�HX=��.� -<`�8���@]��'� 4�����Ѓ��M��(�A8�� F�>h-��U�%4�ri�<Ѥ�B!8�&��GIӧ��d��Nܝbإ����4h��M�4e�n�J��B#���Č/�)�Bk��(�B:�
/�an�8��8�B�|^4�C�,�$���@0L1@[)��ÜC9D�ț�D@3�#Y�4�C9��ë�;`۷͛��B.\�~�L��F��DC���IC�<C4hB"��,�a���)y���n�H���_�C8�A��	��G88�e<�@8��K=lKA8�u�����\ݜ���f5���66C�1��7��=�C3������d��=���NB���%��AH֨[9tC �%0�0(\�1T�@G�M\�|ڽHa�M�@hJVAIq�e�Ѳ�\6�L38EŤ^�H)��1�1�B9h�)��)�8�!�� ݆ĠO�`B��B����
�����C��&α��I� `� ��!��,4�:��]@,�B,��^ �Z
E��$DT<P�;胅PL9�CU��}L�,L�
`�!_�y��ɒ���\��8��5�C�����d�_-���A�>���4��x@9��ژ@�@��"���X%�@�N-�#�iAR�=�H8�80�B:DP@�UU�6�	Eޔ�	@�%��= �44�-4C4�¸u�y�S�pX:,�u����t[<ZHBK�r�7�K����A�@��RƄ�d�j�n�8LC�m�U�b4lBt�q*�)|*��:B!�8��(�Z9|��4.L@
�� {�K44�&��r�%���`� �8�%T/Ȏ�L��,.\�Ղ.��"��,<��CT��1���0��8C3��l����`E L &|��H͒���Ee�D�E�LE��@hM<�q�ĳx�3�Q���c�D��f+��Zq�
��L�P�
p��M�"�	8\Fe� 4H��5(FP(�ZݑR��dC0ģ �?.$R��H��A+�RÙC70�4���
0<��j�����t��.@�&�ԇ$��U�@<K�RSNd�~��i ��ɐ�@�>D�dԑbQ�5�T`耼"&M��0��&1N1ò�$B%HB4�HW�tH��B.`���
`���B��Ov�&h�ZBX0[���e,�����B���-���B��݉�4X@� �4+��*�B0$����B��` 4�NF�c@��LpHÏ�E ������A��J���X�$�9��*l�1~�3��+��
d�
�*XA�LÀ� �Ab}A8�78�� vR 5�?�@6\A8�A��*�m�	�`ba���,�|��v)�!b��l*���.C��a�@�̮�
@$@���\��iMjTQ���d�/���4�R�P��>̙��H�@ÏACY�%l^�nC<�^pX#6Ⓘ�%T $��g,a�/��{��
������E����FY�XB`�蓪��B4�
��Ь\�~��qu�����q��-,����*�B�PB��3��+TB��Bh��	K@�a@�O��Oq8�Y�5T�*��	U�N�e1��ٞm��J6���
`�@B��`�3�E$�Y�����#� p��P�zf�>KPd�!Uԥ
� 8䒜IlZ*��`4ܸ��&��j���1@kGơDE�  �>r���"c��nk�F��8lo>�C�mB44�&�P�J�>���T|ə���	�T\%�ǁ|ŌE�*���p#�G"��/�(���4�,lb���
��l$̾x�rBDX�9@��i��	β�=B [0C�`@!vaTsín4�@��P�0��nf�
7R�Q�E��ς��Q8��M��\�8���A4KE¼FI44���C����_HO�}5lm���ڈA5\q�LE<#Hsq3��q���PR� �ުB�8|���� \���ׁF��E7�A <@},�)<,�2�%X�)# $c�	L�#[���k+�V�%�BT��b\$!BY� �E�h5DM
ǝ�5$$�6�im�������vŹCb{�X�B�%�V�8` �D@�B&��,^�4B�0b �s[���8��^�&���0L�v�F�
<i8��0�'�C3���a�v�3D�B*��40��MX?S���P��$�L��NX�YEIܸ+�ST�%�F>��w_�M�Ԉ��b���\刃(�*�4�>��ج�DH`�����dCLC�@�j�Yވ���X@��C,d�8��˨�Ɛn!0�<�e|J�����Y� ����HC���@ �#�8�n���: �NB4��&$",����T��_HʄHC�DC��&ZJ��JL6�&�2hW%v�/v���#�J!�^87 -�e[8���.�B�`B ��4_@/��M���B��@.X�"4)�`���.��4GXЬ�5i.�L3���X@3�E4��$h�q>V�sJe�4�B��N4d�āVw�E�M\3LY��`6�k�l+@���z3��3�H;'H�-��H*��%覘, Ic��BI\�>@�lAK�����=Jv~ReQ� Uei'8� d9x88D3L��G<�[:�;�C1ø��#�1�b�a�J@��n�?�kG�	H��22#�x W3��Jr�޸�!�<�ÌG�a�^"W[YP)DC�a�	��-�E��T�s0�)���/v0�i1x$a��r��i�&�������W�&؂���i&2�!��~�Ij�v,(����[0�+Z�5�R5C���\��B4|�=c@T��߂�YFa�B�8x�=��>|�����X�� �Є�'_lB_X#�I��LÏa���P]n�&H�UH��-$Z�B.<��эgJ��x�7��EFd�RI;M�t)A�vZ�t���^.����Ǩ�;$����(1 l�:�&X�̸�+#c��	�x��;#���f+%#��i�����������h�^��^��?���$@xW�%0����gy�/Cb��{1ܻ!��!ǃ- �/.��`�B^�4]xv!��6�EJ�
b���`B�D�Yr&`�T�E3U�tp�,z�p-j��K-K���0��,�`VLp���f�(��f�v��go�>��̕�F-�4s��Q;�_�h���%g.-=|��Fk&MnYs�C{F`E6�"��r3h�^̪ �^�0��u��81��[z�艱�8ஐp}\'���ve��pW��4�CST��+�.^�`�i����*bǌC��P	� �| �H`� B����� }}���;H���M�E�cvI ��L4)�!���B�4i�C6ل�I��c�	fxJ���Y&�aNL�x�	�Z�Q dYd�[4�*[,Aj�0���d�e�].H(�
�� "2)��\�%��E�I�`Fx����	k�\r�E+1�ᥖ�\p
ϖZ�Jʔ�k�r�.�
�'v#�.��L�}�Q�-jcG�i��	0��"��P�0�(8�K.�����ΠgSg��oJ'���&�lXsM��ĸ�2N �XL�&p�0A�/��
TX����"��0p�x�!�F4����;�PLa�
H�<���_}����̫/=�ʋ/K�a��]���`�M���>4�C>��pFD�#�@*$`�!%�`�I'�aR1��A�xj\�Edq��T���X�%AoAj�M,���Tz "����*�)�xy��FId�0�u�ƝY��l�8)�Y��qۦT(�?]@3�|*������i��9}�"��f�,g [�|vr+ȩq��z������	�v�9����$�f�p���fj*�W�0[�q� �4��8��+�I��ߠ�͚+�C���ƚ��E 
��`|q1��b����
01%;b�#��=�G���Ï���'= ,��� |�' @�BxA M8������b���1�a��mP� ���2Mt�e+�E<�Q���h� �1b��e��(g�x�;�1�$K-qʕ"@ ��`iHi���D@���E�Q�E�M�����(��x�ш
4��1�&��
7#ј�*�Q�L�)�۝�R�;$i�x�Xb��(%%�x�>g�y�c�pF30 X�,�9�^ꑏ��$u�Ȇ9�9i��.�`;V�-։��\��M݃�h=��&h8�3��Ǯ����W�k�kN �/0+_��3��pt�
؊=�QP�U͸�4�I�E���HD������;���v � �Ч��*X>�S� �O`��O 4�����(���O��
}�C� 
P|��1|��]C��D06�%��������4g�*G<��|�)�Zl1�`�ba���;�
�d���XXAֈ=�aV� "����ThDD/�옢��&U�0��xJ�	�`iecIVr��������8�A��-\i�K3�J��y�(%bY��h���0+�9d�T��@>��hl
���9������3k殦wk����C5�E�l��X�;�3zC+P�
#���1�[�B�x�;�a��v�0��� �P�<��h#�.B�c�T��4���)���Ǽ,�@��>Ъ�@�BEa��*t���|IA�2D��0B��V���ps�o�$1*�1^:�`�)*q� HRH>��ia`��ÈE/��w<b�x�#�ڈM�A�Z��"�!� "�pچD�	O���F�2�� 0�Y��]�*�cK.�3�$O�h}�T���r���Ɲ����屐��Z�B��0٠,_Tq���"�bG=0��fxArg�Dg���l����]0�"a�#[�5�A`u������1��Ʉ	N�kY�
�^=����e��N��?��Q��~����<<� ��|�Ǟ�e�x˓0M�B
�đ�P�t�"0vac�b��D 6шD\ �>s4r!��<	�鸡����a(x��� ��^<C�h/܁&{K�k r�l�f$� ���0�8�Il��$r�j����4xQ�M����P�%���xQ&@߲� �<�F��[���,��ow��Sz᜙��+8U]@�f�E��Ic5JR�Ct�ۉ*6�^\�R�8%Z�!�MX��x8|G�Ib���`an�k�f5�	�5�1��MoɳF�r��4��$(�	�E�pD���<N���c��!F1�D����)D�!Rl�^��_�v/gS��O�׊�	DL�{��S�Ї��{��� R�b���1��M���R	Rna�YR� �g4�c$�D�E.��*\�3�`D9T*�i,G��Q���a�#�/,. ��6N(jԅJA��K,�^N�dAȠ ��,K��I n�F
�h.�L����萦��",��.%��n�b()TNI.ȁ��́�-BE�B���B�N�,�!V���"��>c5"L ��<�@�!��:m���z��YYl�Y�a՚Gz����%��!@*��%�>O
A�`����mc^Ϟf_�)�2�<�<8р��"���J�0@ᡎ��<H����-}��%*��Db"��"lơ�bgP�r!�ʁ�������!Lr����!�az!��:�ԥ; �L��K��M�)\�FA:�DC(`�h��ʢL�Z@h���F�!�d�&@Gp�jP��*�&K\@��vi9��Q���x�2��"��XI�F��T�Ф�r�!��ơ��3���Aw�W���p��г<���'o�)z�Gy��1"�(	�Z��A�Lr�))��d��ѝ��0�.�A����I�4��h��r �l���P�P �B jf���A��d��������R�`"��a�!�DV���g��9�3g$`�r��F�pz!6cH���|���;B���-�"Dl�* ,B8a^V� �
�F�
�⬲$�"�h�Z�!�D "7⦶�oNef���&Ap
�"$�%v�,Pr�/Pr�R��&�a�!T0唜A9c�фҙ�@,`:M ��� 2zIX��C!-�a'~)C=�{��W�A(�YP��s�!����E��s���k��*���A�����Ȁ怚t�Fb���a4���C�@� S0�bv�A����1��~�;a��)`n�ހ�VD�N�a���f���߀!lxax!@sz�A�x�h.`��4m���a.`FZ��85�>�Sy��eN4��u�8���jtA��F��`]5$x0����'��*K��+��!N2�@i�V�a�AQ��@�b�A@-@��0T�A\�A��oK�"�L��L��г��x��n{^#������v.�����*-�`}�!� &�� ��h
���}�-���
�`�I�T.+�@��Bpe���0���F���2��4�H,3N�M��aE�fz�6�a���B�R3��@5�xAd�z�;[ �a ��x��6a�hơe���C�p�T�rl%��%��h�h�
+^� 9R�ͦ0�z��x�<���(��.�)��T"�@	0� Ф�J��5\s��sdr����6Xc��a]G�(��`��ܵ� )�E���T�]ٵw	�{�Ҡ�	c(Py�!��)�q6G'�:��b"��`�0a��^�R��?�2a��4!&����c����`�;̍�j�B�A!
�d!"n�{=s�-jB�)jS����!��M�x��b�߸�����z�(�e��b���6A $ aHL��Z�r�d.�P"$���V�� �Z�fA
g��.�k��W����dA�(#��(0 �*" �b/�!�"6���rl2*������@(�L`L��4���-��v���m�z�{�aw��/����$a��� ��+�8/q)G-���@*����&�j�$`�#_��|5�a�A ��dC!��+{�0A�cHA�����ܚ2}���!b��.�cr��
$��hn�RbE��4[��
����������t��a�4�� A]�A���l*��!aHZ����NՌ�$$V��
/�np��Z`�!�x$��$���w���Xp�+�"VR�-,@$`�#,�-@i+ۡN�,�2��f�?T)����8�^MX|R(�P2���*�%+(�]1@0 L�(�ȇ�z9Ϝ�!Gol�O�d�GB�9����_��*@$�H�}�R��i�G~6�8$�����ddd~��n$���� �Daa(�W�����-�66��AN���Af�9�������+j��.�{(fA!��IxAw.� D&�AFv����ȇea��ʡ a�T"o��N���e9��̊�L�fy��f�|�&aL��a0&a8�?�-p���P\�֎.d2Ұ��42�ǎa:OE��wݕ=K�X��v�Wx�(����c�U�0`8|ft�Ģ�b8O��-���Ʃ��a�����0�uO.�l�~P��M}�7���j�l��BF]�UB#&Bl��C��Lx%`%pM�@v�hla@�[�vA�z�l�A ���"qv�L.X.n���!�$a�A�c!Z�bW����e�xAh�Vi��hL��Qc0$ ��L���:/�h
�
�Af���!���#>@$�a���	�.�/��~�[+�,@�$�ٟ��-���ګ��-@�$�T�]9�E��a"M+$g.�,����Rpt(�,���Q(�ǭ��dի��ddJ[f�$Ɓ}��8A�d��"�B-A�.@l�F8��De\Jex���A��B%  "�$ѥ1�#��$�3}�������1���hd�_[�G.�L�!�,����hh��mAZ� ɠ��Z(�\�F���A*��%�[�Lݤo
+�D$j�,��@�����nַ2u�=L] `>D"V��AT�
��zW �]͈F���X��c�����@�{w�T$�í����٩���!��-�s�}�(!1A�B��>Y!��K=�C���I+`p? `�I��6f��w�'d!�@���F�����$��Ѭ��\�P�ԡ�Gf���a�y� `�(�!�V���.�kh~�zFPD���qb3\ ���UnZ<s�ܕ[H�`�.\h�"��Xc�C&W�i"T�������ѻ@��
�Yra�&D���&�f0\�%��hH%BjBK3U�LD����;/��*=z�TE{e�U3iҨe3a���Iвm�`�41b�|��; 6�EK�f��:�Wm6j��IV��L��/�M��"&�h�0`��s�!*��At3j�jT�P��Pb�� �^� ���(_�|9��"���B�
�HO`I�@���iDJ�y��P#�)b�iJ�k�%D�x=Zt!Q�D��2�0��%�8�B夳`�����0$�/�hbɆ�X�I���bT3�4c�$�(��	.��4��B
������;���;�D�[U$�,-�҂,�$�~8�D<����SM��H;@��`5���L@>HEcN"�D��`��UO>;9�5v��\�S�*�\R�:P���g�	)�%���P��b�;	Ś�L�N���jjH`�h�bٌ	_`:�&`A ���0Y�b�b�&�P2$o̊���� �Uk�r�)W��<���x�)�Ӈ{�i1��KJ(����&�����gK� �$�8L9� �!/��Ko��#���X_���� :l^��T�-����;������"/�I���#�@�(��<���<�ăO��$Q�ZF�0�G/�$r�4���%�������HM8�dfp��,�\�/�$ŭ��B�5����=�LS�3�p;٨�L$�����@��$x��\������Dd.�80�3�t- W�k�����.�|+@�T �7a��h�z��k�|��&�PB�`�A�[o�}����f�	�`���L2�!�bP�%��#�	��#��gLx� �H#�0B�&�2�����J�Q�%���E/4>�lP�F���^��$�@I�=���E,1�h$0 �r`��8<�3}�#*�0�B��Å�;t����c��(�#h<*łh��"�Mc���DfQI��Y� M&�\��&71Ӛ"` ����8�-ԁ�qT`ѐF4�רf��`s�,��]���Z�Ul��r�4�'U|!`�s�����TK���<�a���� lј�N&P�,a� �],E�MD�����>����A��P/$A;<��U�@2[�)v�E��*.�ѨL�)N�§� hS;��^4�� ��2O0b�v*"�x@x�N>< r�˗" ����)`� "�Ss@L�S�0[(?�f.�՚Kd�,1�h��������i\�(�DG1�'F�Y<�1^8-ǐ�F��trJ�Aa�
t0�E՞$��D k�!��Vɵ	%��-�Q�T N�����vL�HE�@-\���u� �ߜ!�M�M���k^���\"_fj��Y�l�A�&�<l0�À��I/D�Yl�(���Jv0� �4���"`�!m�
[�g��)
�@��m�@��&�` r����i3|�Inq�3�Fol(�	F8"���"�ca�|0I��?���B3z�0�#�l�x�-𱈅:��/��;��@K��"
/P=�a"�(�* ����bN��"J0ID��؉$x9�N��$��},��͕� �4�$D�$�Q��}�@�8Q/�_A>�?[s� �4 �a�$���QxD��E�_��G��gd����h��-?#���$������)�\�^��س6C2��h\@�YDfЀL7�э{��� �>M�B:�k" ����:� �� ��v�J���Da3Ѐn@�+x��$� 8��(x�*�,nq�K\�tnZ����vz�p�W��W"b�]`hb�Є��Q���؅���e�wH�XȆ�� �"L
I�4��LB'��,<!-Z���,����0!�pG� ��EHb�8�H���t�7�0�<ʡIl�?��H9 r>���E�/p�  �(G"ʡ����{`��Y�b�珚Ժ�hY"�$Z���"�~{�*�	���.Њ�LWP0��+d��^~�+����C{�����.��C�.(�F *e�RY��3nr	�pj���~��@�@��p��!
����(�
��)m� -�m�k7�V���b�l H/ (�x������<��c�H<��]a�ڼ��1x�l���4ޑ��c�H��/�¿���%OR�c��D9 �����-��Dd!���4sB���p��+��D��C��.��ny %��p0� X-v��Gq�7�k":a�&�Pw����7�SeB�l�.�����A�pu�Pev2X����@��K���uw�)"�*l�v��)`�6�zaY�+�F; |4pjQ@(�iP(P�B�xʔy�cJ8q -d�-;&��p�`�7�2ǐ%3�0�%�#��#�� -2 �	��,_Rð�p�Xf!à|ѐ������!��8��8�������P�0	X/���0
�o`� �R0 	i�q�]� /�p�.����4�0D��� �| � � 4I$��S����?�UI`X&Qk���RvB�Bq�nqSG�Fh1�u4�K<�K�`h��KԐ�I�@h ������e��iYh� g����[�6�2��AK�`�E,� &T�A�X��W �i��b@`����h��������y�0�.�S]�@���(C0�!�@	�{�������`	��4�������zO���!$$�z<IS"	���pZ� 	��,� �S=�p	�s��| ��F1� R�@� � A��X���0Y�� �ƀ� �h{�p�g{����8���ITR1l�V@�(�\@2i'$&�@,j���7h���<�������1h�Q�=�hI�@hx���;���h� �Fqh��L0� ZJh�P�*�@��#	�|�8���`X�xG`Gp�7��Lڟ*!�L�	�`갢��U� �0�0/��	���$�`����>{��i	��~3	���U� � _%�t!��#)���qIT�������tP	�0	#�#�`�6�=�R��]�U��TUS/�0���Yr��_搚� �ٚ���z�7�0�К°�����R�:�@	�DE`� v��7�0u'	�u_�K������v�xc����0h��u�у���������0h�P�'����Q���Yb`_@xb0&`:ɃPw�6��hIHdB��QPg`���xQ��*Q j�r �Y P �:�[{��0�p	3��,����]�P�0qQ��7������00�wW ��� ^�����U�P����Igt/-�	��"�V 	"�o��NUB��A�44�I:!V�`^X_^���Z��wl�P.QNJ
���Ǌ��}��	�+s4c��U��5)����0�@ϐ���K�`�ːI��� c���$���;���:���ّ�h��u���9h�&�[�]0�@�k#�Z g��<��)#�8Q�D�B ��j�Q @��D�G��4�[��[�[V�[W j7p3p80�%� 	�`v��ic���<�xWP!O`s	� �����	Ǡ"{��@a�00��U�tC#`tG@sB���� I�O��S/�VI����5�"6�� 	���0 3� 	�|$%�P.�� �	�P
�0ċR�D;L��sr-eg�S01v6i�pu������]v\Lh�@�8X�f|�^|���Р8i�	��
��	��P	r,�t�
����+�{R��J�D�D��Q�7P�2�[�v�ً4���i����tw�h=��Z�C;# �{Q ����� �^�P�@�����1a�xYGr.�qHq�0%H���	�@L3{mR4U���%d?�xmU4meYCS��0aqA�3�����	��!RoVK
b�b0�	6�	� 
�P�ύ ��&	��� 	�@E����K�p��`qqkL� Z��� d��� ҙ���� ���0��e�������c`8��p��@=@�	�ĮP
�0
��Ԉ�>P�G����N0Ն|N��40��Kb��īx�
�Qpj��#�vVp�e�a���8�Q`�8��O�� �@�� Cj	�p!��  �^6�B�8� �o�jU4-����n%O�JBA$� H2%H��I�!�\5 \4B1�s\�b�p����i	� �y���í���q	-�0��%�P��4a�1���qUgH��`�Z���] a@Ғ1]0�.d�dpYp����o}V��=�m-�1��`�d�����ŃF�[�� �;�1��;�Vp Y 2�1J���S
�0
���#^�J�W��|�X��G��k���x4��g�x��x�ʡR��e�㍗x�8`�bX�h-���	��]r�н���GDPr;[/5l�b�0I�D0F4D�tq��&GĀf���p��$^ʀF7���@4/��р�`E�P5��P6��R64gT�}�	��്`	E�����ql�׽V�����G������dW|C:Ӑ��+��+`��d�a��Z��J����~n�V@�V ��Q���YP;`10>1�1�+ 5�c`�S
��ԥP
���!�ԣ0
K��(���{�M�K�NP�M��Bp8���j�ˋ�
���{�dϡ��G�o�i�2)r!��I�P���������L~_�����\4'�?kr��}�l�����x�	|��$=E4Z��c�&S�V�vh�I̗=Q�IIT�=����E�[?���F�>�p�-6g .pm��ы��0Ѩg���n>�3-�
�b(����0�����[��� ҹ�ސӨ�.޽Y���}:p�Y 2��; �/7���
��M��L�Ԫ?�N��,�������E�������/D�|@�����	o�e�xkp8���wH^��,�� �� �� � 
��p� ��@.��N�ڵ$b��Jh΀Q�,E��?���� ���&`�0ႬBpq�EJ�H!b�E�49P����X-ba�%��-8�lȋ��\�t(K҅�[���PWY�d5{�̖���r�zV��Z3ͬ6�o�`����`�Z=z����W��Z����s{���p��E{��޻l����V�8p�y#CڨS��J��)Q��2uJ	�%J�8�zI�&D�)���ժWArDʙ(@^�ZH!D�D�%�ܵ���Y� ����W�Q�F%��]�v���7lO�`� 0Z�^�2L����^�Z��&��;^+D)��	"Hȁ	NB��*�%�Xb�ZJ�"ZL�Ñ"���j�ZZ���.X�!�]t�A���%�
T�(�a�(Z��in����0��]ʱ)�j�&�r�y�x�	�cܹ ���h�ڇ����뭳��.��ʦ,zںk/����}�$+j��ƙhF)E���1�J�PQkH#]�%�P�%�8"�#���5"FMmT!�p�8v{M
!^B
W��M�~��UĠK�ߐc�q,�; �d�K �.x'+5���
�;��G�5J�䓯^����^��%^A��ar� # ���P>��.��!�[$'ci�����;S�W�V��#]��!J� �l���H�6*I>IxY�#^p�E�E0��!��'�t�9����t��/�f�9g�h�"����ʋ���꫞|��:�b���ܓ�l���i\q�Le�F-e�GEyԔ�2��4Q�(�S':�R��ެ4)��Ɯl��bTَ���U��
2�9�V��%��΁
��/�c�}�����D����9��Ynif�����[�9��D��R�a�H� ��" A�jQ�DcQd\�m�_Yz�
.� ��R�%E�P|���WHDy��@Z��)�ZZ��%Yj�D�U�WCla	K<b�(�H,q�i�����1����� �L!�x���9��l���UsZҔ�4;�jq���4��O��46�L��Q��D�6�(˜�3�
�j�p��qf5�i�p�I��7@Ǟ�Aؼ&8k8ʰ�6�qT�\,�(� 
?��ޡ�a4�<�]"h#^@"a��"���i�u�@  ��u���I�b1�X�d�x Cnq��xٟ��Rvc��.l��a8�Ӏ�P$�|M�`�-N�-�d#aHwVt�� �!��0r�YA7��Ih�"�$���*%q��`	��&!�	�,�$�����`�Z�L8�y��iS�Za��;ݣk���AXQ�Q��Q�j�dJq�T�Cme�f�H%.�Ep�h89�!
�ը���=y��i�rӄ�D�l�M�G� ͈�3<��1���)A�
B�ĵ`:k=��W�S-�!+ W5V䣐DȨ#��$F� ��rt�|�N��	T�ۼ@�H�I�"�]W/RID �+C$�{1e=�JC�j�[�L}ߺ�$�5��KɌ6b1]$"���.��ad�A���&�ɳMhµ��&���m���m1J�	CX �0G
��>�S/� 	�a��3��H�q��'vD"8t>Ա]t�e+��1�qC�H�8*5�"��T-��T����5K,�o��1�ƌR|H��2�rp�� � $��� �fx.a	� 5(���	�KU��xq��,c�9VyUO@�b�D.�q�[$a��N.a��.aq�(�3VH����@@�G�@��r��Uʹ��tTe�0 �EC�ٹ�<9�I��tY��,�.dNjH"��b2��L��S^�խ�(�lI���⵰�mkK�NM��f�*J�seYG=�[�d�,��t�T�l��O�>�Ӥ�|N���>d=�Yǣ�PG�Q)P��4��ԥJC�zh#
���t�HD@��	��8| �&���@1��W� 5��+�
&��~�eK]Að�"�ac%"KÐW���D��� ]3��_ �i�,`v�"�x>�1�������K�d�P�� ,Y@.��.� ��y�͈�:�,�3�A���lի^\ a���u�#',�C��$.� Y�hD#Z۹�c�|�l��
M4��Z�Q�;G���B���B\}����84�5�#�r�
5�1w9����Ԟ��0�#��;>z�Q�q�4��L��`�Da��u�T�8>��pF�M��zDјD��U�hb�"��]�Q����i���}�*��<�X�"Y�I��[�$�q	�5��7�λ\�Y@�Aѧ]ǣ� �8�����d��x��8V3�o��Q��P�$ !٠���rEօ���KL��͈���$�� ���.�9���c���X 1{9�h��x�`�E{�FЄL��@��=�YS�X�w��fP�LAP����ҋ�;m��u�"�}ȇL���l0�!K�;�����;d�����z�&��k�k@��B���(�	� �@z��=	�R���z���J� H؄M �ܡ�h��	ᅳ���=�[ �H` �c��>C3�HH�iȝܹ�K�%�ۦ��q�� s��4!��i� /��D	��2���f������+jh��(EW��\����ڎ��21�9)s [�+O�3�ĎK�  �=�F�	�t*Mx��*�@P-ja�?��xP�MK��<JC�h@A\�H0li��26�'0�P�UxA"�shv(Bv`��a�lPH�|�� ���HԸIa�Ոm��(0��(�#H!H��/x�2 �hX^Ї}sȇrO��KP�E��
��[�?#�ʙ�F��x��;"��h��R8���D�0�@��r(�� K0���E����ar��r ��z�K�+eB�`�\ܦx�Ҋ-^�^0��#��Il�@Ӫ�+i�K��iر��o9�IC���ھh�Y�F�^%�A9���+��K� ?e��� X ���h�t4MP�BR�L� (��	C�q� ���J�.��4ƄG��GzT�I� U�Gġ8I;��U�;"ļ�ǂ|��$B!T�
��؛�B"LQ)��u�`��b�p�+��0��,�X���j����vA����K��j���#g(��I�����%�K ^�J> ��1;�g���ڷNr�G��I�4�I�����	"�R��w������=���sx Hx@��C����!띟�2󫷊�h��q��$I�q8-HP�M��.Q@љ�H9��I �
�L����X�� <��^�9k<G�ԄB��t�-
��܄���q��q`�M�H��H�Ux�9�GғGMU�I�N�d� mAj`5⻩�AzϩxA@	��.i��$z��h��" �8�s�8(8�1�h �I(�̼ �S���ʢx������z�h����y����J F�o\*���!�8D�s+�$���4ZL�h����@H4��L@�ĝؤfH�σ���!�Q�x ��4�i�f� j�K��D ����� j�ɒDY(R���)C&X��!���Y�FУ����>��ϻ8��o���[ ZT��MD����b8�n��th4�� V8�t8�sP���7�K�0HP���$[��ԫH�d���e[�lվ��|���s̉պɔN��N�M��rz��Êi�T^uxjH�r��K����֫�  ��xs� �LT���rP�hx������D��M�bH'�7�[Y-#S<��FK�$�M)K����2I��݄Dx ��$��<�̆h�o�\yl�MxRL^p�"�:�����L�P\��T�f��i�.U2.�2������e�?���� ���S���ix� � O��Gh����
X`F`�AK�s����q�Zt�����:�r �h�J�#h�H8[}[��;v"9)�������s�l�[����N���("��8h��kUP�Q��`�FxY�\�� �% ( +��f�Ý���J�-u��G�k1�j��@�F�7l�\�	@�����h3��mZ��� ���RڱёX���y�@(z@�q:"�D��k��+�
 `2b�D�,H@@���� H���ݤ.i�f�֬�Vߞ]�������>qq�{�C�C&T(�y�|�"s�8����`j�ZjV%�|��!�G���v�Ve�n[�T�#��'�������:�L2  �9k��@��'�܎�bz)�8�Ѐ�n�t`�x�i� ��H�-�0kM(F�-M`� D��Lك ���Gp$h��ֲQZ�8&u���v��x��ꉮ�ž��w�\���
��Yp Y��u*�%��-�f�ؗ�8S�I�EG�2�y>� �����"c^�[�������S3���Ī ��
p]o<�@���Єb@T����qX�g� ��k��Ķft0Y��ŀ8�<����;|��|@��av@���qI��]���_s�Ѐ{~b(C��*�mz��>�ր����iH@K�����]؄G����&�@��hMP��`KU>�ɷyH��w �X�2�����|Y�с s��Ex�X�X�����������
�h�k�fL�J�5�2��$�%�b�=T�̗�2����˅}�VV�QLT�cc�8���h-�i�GH:�vL�.�c.}�H�=����V�b��M�vhU���@UzP=�  a0�5����;�H6��&��]-m�P�#�	�,�

�CM(���m,�b�.�Ѐ�0�7��f8�xH�D��j4��]H���K�s od�Fʵ�3sY^X�H_xxq��eH�a�y��(��x��dX�]��I��؅c�$e0�eR�2�����xPG��؄��q��
cD��l�f�����3�cʒ�a�Yo�9w�X�����E@?�1�
P^�u8i-k�ҹ#k, ������R T��=߄K��l!�?�Z�q�w�N\�d'KC�(_é��M��M��h���;��(�)���]��½�!Ѐ+�5'�ӥ��Fh�ր+��+��ɡ#��(��	x���]hu����0ka9�Ԅ@`�X1D��Z�ށ��x`�XX���^ �e�Wb�a���؅e�%aX�� c0�]x�=aЄyE� s����q�c ku�j��i~�035s�J_YC ���l4aP�cXw�1�Hx��
px�t.�a�txp��|�K��w@�)1}[L�g� ��<w�\�DxMX�`@f� �JQ%I���:�������u�jlY��f��%��=l �f�ڄI�0@X�����L<���K���X�������B	��p#� �������x���� �#Q�6��`�x�����0R+,x��A� $Pp�B"� 4n���A��LVp�kY������QѲw.,��KY͓b	��kٰ~���0a(+T���`�M{�5l�iX�[)+�{w�ѻ��V��nUF́-^�z�����>����]�.8�0ϝ��0��XR��%��W<}T�7�׼i,�rǕq��`pA@��J}Ԩ�b�U�T��BUM�N� e�s�M�N���T֣O/U}��q���{W���w�(Io��=v���ß�4n�%N��_r��J4�DJ N(؄ �j�A�D �Q����_�GL�BDab>��C:l!T�1�<��&�CJ0�PE�fe�cd"������ @�/����M�@ (A�L<P&��2�<�QnD���T��0f
��1�З	�N4q>�	>&=�>CųY9}yQ"���> ��:�XbIʹ�-椔$U�9 �2<Pd\�@��ӔS�X�R��c��=�Sf�#F�	��d)�@en$�I��B�&8��C���V"U}��s(i�w�Vw�t�C]x��C>���9���{����|���9K$HE4A����k��!� a0�@"���w���E,E�q�q�B\Q0�s1��/�h��0����r�QɑF��H =�K#�]�$c�S�,3�dd�e����Q< �2����bH�2nV�_0b(J1����K����-$��9O3�bg:`�N��b�$aF��<��$s�E�!��$T-�<��Y9�5�[h;9ɇiJ%P�;=�R�F�8��LC�ӈC�D�I (�MU8�9�X��t�~_{�[.��C�8�#?o��hw��� ���9q�NL�>D1?�b�L�qG��A1�g��Fg ň<��]�$F1��2[�E"
��$"ǐ\�DH#I��jtHPEZ�Y�2x��jq@,���(-�L��M$��4-"3�2n�Ȥ#�U��F�c�J	p*�w �Q-���s��xG��X�c��F1E��A�LU�$�e8�sU��,�����p@<�!�
�C���<�E��:�Ha$0����!��$x�CPd��1�qP"��]nq��������%�x\�0W�b��wD�]���t�u�W�^j��t %��C�:�~:� D�H�x��M�~G B���!�cQp0b�bcd
\�"�u�
�"Q�(�[ ��
`$Uj�#,�G��1�tD�R�4��|�	�dx�)c��"qI<�Jɸ�v!` +%���;��uL�WB{ #�!�0"OӰ"a��-��x ��1�8 J�D9�!¿9��)<�!G��4&��&�@�L)��R&��x��4��G/C�,�n�aJ� �SJxE/^|[rg	�Q%��E"�������:0=U�[���9�r�#�ꪥ-�s��ao�������bf�|��5�)gv�~��gO$!H�ؼ&6U�M���r�&ι�G��v��L�&kX�y�37���G"3�P4��d p�c,#F_�HQ0"�c��kS���[�d�0(%!L�Qu�c������q�$��S��g�d���K���(�Z(i��@�f�!�d���ٌ;q�^$�n� ,��)Ū���EdF����t����J*JlBv���(Q��}���;,	F^#�Մ!K��h�:�%����m��z�μ��K+h?:�
��˒�@Q��i�ǡ�
mm����f��
�u�3�F;F���< 0�y�B<"/�1�t�h"�g�v�����]<�K�����E�Ҙ�D��x�fƲ3�jҞ��.v����(*�
/d2z G*���T��hq 	V���D<�i��u���0I X� <�B����� J �A��I�U�T*`��sX�#hb��Y��D�N��RG9xa�ܦC�h� ���.���e�衎�!�[�9,u*��Æ9@r��D>$��_�3B�3�`L8���{��2Z�xS"��p�h4Bd##�WV�=�t�HF:Z�@^�JH���9�[%�1F)���Z�����}$�������dX�`vN��H�%H��
(@>�{7��O�0K�a!N�}��oT�Mf�ؘED��8��X�i,�8��D^$�E��晑��9�#�4Kh�UA�4^Ga����၏��C�/q���4Ym͓�98�d�V'�؃W��a�}����A��C�" A|����� �kv�~�\m�� �G�
%
��@�+8#x�!Dm�C(����.D9�H85D"W�`@<�-dF�c�M��Fn �9�d8Gfl�X��Zx��HBadF�D�$�L	�dp���NII`�d��)E/XJl�PK�]f�N�?MC�I���Ռ�=afЌ���E �Fc�\1�C��#|MP�ËC L����^�1��K/eG*���q�:�C,=_�q+a�d���ы���x���������2���P6u��؟������i�;յ	�!��$�$�r�I8�UB�&W͔`�/��-`�,����Q�!J�Fc �G=��	���B�(�%�W�D�G(#�\�%��XL$HB#��G!W���;\�FDFQ�y#sT�L�r��7F�rX ��m�]�5n��a�cf\C1,��	U�)0 /Đ9BJ,� ��}�E#@� l���d)|���!��C:�R>��`M_+q�d�\vdY���x��8� ��8A��GP˭\�!�Q�7�����D����> �0�H��9E$D�S���%�A��Ch�#��:��-`�$�Ds=@�H����F��!UL빘Yq`Jhį�DfD�TUL�$�6�d|�S?A�@ʥ`c��`��Z��#H H&$p�y���ʝ_R�@�QI0>����Z��$4��lB��U�@Q��Q:,C:��I��^q���!PH�RH�$�>gt���������K1�� ������	�����_yS1��x�gEL�D�Ơ�XC8 L�>�>�[lVfe�1@d0/)X$/�S8����dؕ��J�`Z�ffTI�E���S�]_��N��C����U z�J��e��b䜠]�BWUXB��e3�D��`���%@g.��4Z��E�Ƒ֛�F�]1�����[�_U��eE9$B���& ��-'�B)<'��iI��8��Ktx' ��exbO���Ĭ�S���E�g&&j��\��P�3�óXB4`��ATZ>��&i��mB#<B��%�&@�jBK�E�P0�f��TH�T�$d��Z�lԊ���aZ���#�g�#�5�$l��p�f��� ����e~)�`� o�j"�Ï�F4�B8b@"`�S�f�ވ�Ƒ
FZņZM<��b /LB��E�C0��!ĵA���c� P�
³�C9h$H�ٛ�i/]�s�;�RcQ�n����+x�.}'�Q���\����,� �,���,�������� PϞ�l� P�Ld�X�x��K<@$��BL��"0B"��1��Y���4§؊�D��j���jnD�ݫ��
���}Mzق$\�eba@��(FC0�D���x&>^�9��_:���X 5^�};r��*�
�u�-�(	zԔJ�S�ۈ��`@4��7�kJX�-L�%PB�����i�Ë�%B�^�- �!tU�S�u��C���)���>�dHf���I~�tX_@  ;  GIF89aZ��  	
		"!##(%"-%''%)+2,2),336*%5-285)8759;A9A<B3;CDDC"K,E+%E-3G5(F95U,$W+2V7'V;4h6d-%e9'e<3s:'u=5I<Ae?BXCED+FF7NV/KS:XC*WF7XR,VT;Sb6mEgE)gH6iQ,hT9vG)vI6yT*xU8td;RQJY\c\cS\efgJCeOUgVGeYUvLCwWFv[Td\cmgTqrkz{�w�X|�u|��y���?�=+�<B�M�I(�J5�V*�W7�J)�J3�W)�Y7�b,�d9�q>�e+�f8�s9�Z4�m8�YF�eF�gV�sH�tY�hF�iV�uG�vV�uk�WG�Za�tL�yi�q6�YO�wQ�wl�|���;��:��X��v��W��w��Q��p��O��vċ:ƐTȗmΤZϧt��q�u��v��������������������������������������������������������������������������Ƕ�Ļ�·�ª�¹��˵��ƚ�Ĝ�Я�Ǫ�ĭ�ɵ�ǹ�ԫ�ҭ�׹�ֺ����㸒㻩Ȼ�ۮ�ս�ѽ����Ŗ�Ũ�û�ҩ�Ҹ�Ĩ�Ʒ�ԩ�Ӻ���ŗ�̳�������������������������������������������������������������������������������������������������������������������������������������������               !�   � !�MBPW���������������ؾ������پ��ľľ��٬���������٬��٫٬٫��������������ڥ���ڬ�����ۤ��D��������������������������������������ܘ��ܒ��������������22�2222    2�����������,�����    ��� �� ������������������������������� ����� ,    Z� � ��'P ��D�����>\8��Æ3f�1�D��ћ7rd7��Nv��m%�h,_V�-��g�n:s���U@I���*R<y�"e�檥;�-����ң�@i%թ+��\�vm�l����U��Q��d�������N���͚�j'��TF
xgRgH�y���*�;�u����U�x
S%�sea�6��ib�P�LU�Q�F�E{T(N�81�hQcC���w�ѷ��ȓo��7���/��[y������ǽ{�������}|�����;��<�����2�����U�����j��g4;�Fe�=�Se`}�
(�p5
X���_���UZ�4��#i%�E����B(:�Gѵ��ɕX݌#.T�8�H��7���q��>��7O6�!�M6�U3�4�8�o��x��6Jg�t?�Ҋ#�]�\vi�hv	&H��#^�����8MNe�"J��}4\E�m�%�<
��i&��>JDJ#�7;�lSi5��9�3���%��}�b����苁�zꪬ��(A��i�y�M�M��VCM5�P#��T����Y�Jj�ºJ���6�,�eWO>��wRLt�J5;��(�4"��S�k�\>*�x�n��L:	 �j�Yl������Kϛ%���~M�7�R6
��}�����F,񹢂��<��g�հS05BS��Ò����<��,+��=o�C��GV�&��o=���r?�,��e�ϿW���j��#W	d�S�L��X���<Z�}�p�4�:C ��1|lA옶M��Ǔ���{ig5�����uY�pqI�߻�m��ɩ�[~h�pq�M�u2DC8��<��(�l�N!�3?S�n��.��-γT_�|��Gۃ4�q�8�!�X�'������AD�H* �rQp�lNAԸ%�A�1D#��6�`��)0��6B��B��nP���5�#N��b�����̼6��/q 3��Q� H����2� �����G#W�n8!D؆� �� x΀����th�č^�蹀���;��vD�)Pi�YI�p���Q0�^:&�t/�D=R�	@	�M�h�P�!dA��MU5�ԃLcT�'�!'�����(D�m�8G#���qd#W�����7�@�h@T!�y@�M��<��>	�_�#5�3���	��3�a ~�#3�
� Np�,��,�=
iJ�����NQm9�� >@
t1:��]¶��U�	������SZ���kز�a�1
Co=;S#\w�����C.0��g����ƯHآm��x?��B
:�X�pB�v(G5��QP��[1��P���8��g�p��U�)b�'��i����Q� �� �����4�	7�DA��'��AЩ�0��z8a���։Aޙ��(�"0����g7K�>sT�����Q>�ш9æ0h�BWaH�N���Jd���C����U�ӈ
L��X�Zs0L��c%a��@�S�
!Hk�	zĒ6m IcDv��=�_�I�'4�e��S�8>&Vb)H�4v�n��I.M���b�A�E�H�5h�#^�ĀCXc�����U�m�Ry[�y,�m?�+�S#�h�@R�	Rh�"�h�����j�������lr
��
D����Ws�W������d$�*8=
G�,C���@�z5)>�����di����*?T $o�q��"��=,�Vd�;��P�ͤ@�*.R��"�Sf�梲J�5��}e��BIM�|��w,40%�x��g�>��)U
����|(}Dc�}��@�׊��ن�%P��̎��wYK,Z��+kW�!�j�$���'�]��z��t� F��g��xz�lsjd�*��dj���6j$
R��;!��	G��&�kL�զHur��j����0��� ��b�ˤ��@�d���޾Ӑo�	���y�,���jKm֤5���9�+��	�C/�o3��^���2�`Rd�Ԩ���T��9��nΰ�
�8A
� ���J��F$���e�����,���dA���7�>L���Y�.ʇ\�a��NpD#�����΍�]��nG�C�D5�A�(�)����h\�'lDƍ��[�ZH���^q��սq��f���9�C��g�MƂ����EP��/A�)�
�|�0�8Q���i5~ؒ	��n����r�t��pO�s<�4��ׁ7S��Y�����_j��� �u�C��Ԟި�	�B>A
�"����C:�b,vxͭ=��?��o�a�}CU��8�Ϊ�C
d�`:�{g�F�p���l� gE�%CSKԣG����J~@:1
ץL�#	�n�!B�o�	5�L�QA� � 8�g�G.��Ro�4עf��i�&5���p&�7E]4~ Q�d�u#Npe��3�{��O�'��aA�fQXi�~�|A�lqB f*(ws7$��4�O2uQ�'��a�4
WU~�aL�"EEx� ��C�p�/ԅ� ׳�a�\C
��W�_#�J�iex>� ���h���B�6�0d1�h����g1��f"tH_. �t�A(pA T� IP���?��Y/ |�wJ�'$~�g#%�QH�}��� �x�r��U{H�gi�Vo��^uF��Ct/��?br�4=O�3�Շ�(M �6W��$F=A�e�p����f$H$&7�}!�	7f�8.�dE�4n�w�4����a�h&ԠG|��#d�'A��LM�I[�
�c]�C�}��FrqB��E����'q�dk���eS+rR0��|tu��W�����d�#_�|��b
!����� _.�Qp�p�CW�{*RFƒ��(j�dA���(�")T�aVl�p4_z�>�Hp��j/2�7=�bF(mb�g��_�p� � 2İu� 2����r�`AD�'$=N U��&��7��.6o0�?��~�Y�	�	�T�#� `p|}�G����'n�I5�p�X$W"�B:?�AM0xӳ���d#���U��[m8dl�05��a3 I\�ΐZ1
d]APz��� ���U������a�҅��lQ� ��@
���/@�y�gfn��*X�1�h-So�2V�w,� �Y���������M�E�4��dK��R��b�d��_��2��0�9	&ـ`X��m�a���.�"V+�Ҧ����rTG{�E�������y/�L�0���7�1�08DIi�!i4J�Ab�g>~�hJ�A��@�0��o�9�iI=~�t���O7z-R��f%�gSz9%�TV�`�6��c���y5����3�C��⦈�Hp(��h2�g�$�-2
Ί��	6�5���Gt�2�`�g�Q/�~�&����6I�Q�����Ɓc�A����J{æ��+���4�h5�W�a����bD=����D#�0��E�ǡ
z�Tت�%!�CG�Y����~J��(�3w�Ʀ�b�yo�&jt�d7�.BM`�1
MQ#�� F5���o����I2��9�z�3����A@p5�S����"ED�A)��)p�a&�J�e-B�P�7��C��EJV�he��5@��C
����cwṅ�3���s��YS(+�0��o2��X)ժ{�
��3� .�oU�BZ���Qz�@��%z(Q�C��Mp�/d�s�~A�@e�6�Y�ɋvJ򉺍B"ę^x[�bEl�x��F�u�^����QT�42�@U�k(D�e�t����	�A�7�	׽QK���{&��&\5��s���uh��A�N�]�A.@�!B.pV:,��	H�8��C�pxR�5&��'�J����:'���+���d��c��RG�)�!s�25	~��	�! ��$"� ��b@��VM�� :U!t4%�p	uAQ�'1$�Q��`?�'e��ȯ�C������������@�qRP�(�`N<�X+sV�@R�D�:#��;D�;�:泸�36�&\���	�Ѐ)��uN���+�p�N�u#%@q�q����[��I����P����Jy���E����A����� �i�F~�j�bN<��7H��w;ᘏ�����+���t�@:#�T�|N�E+�|Ƿb-��<����܁��~�	����0
�������2İ�0�@����:�\/,b�`C0�7\|�#R�����,��H��7ysӧ2�2J�4L�2\L�*B]�f"
d���d����8-�-3HSw�u�&�eSd��GM�w3�A+J30R}�r�������@�� �@���M�@)��+�B�|�}���������ٚ��M����1��ؚ�ϒ��D3)��n�\�>�����1٢+��Ϭ�B�� �����m�a����-��Ӄ�,�ԝ/Ľ��B��Q��q����l��۱[��=.�}�C��e�_���!ެ�+��������p�}++)�/��� ���*���=2F�fv=��+�+���(��C5-A-os�]+N'�m�ܢ�>�������Ͱ���p�@)���R�@'�  �Md�#�J"��r� ���F�2��������p�p��P�p� ����F�m)ڐ
�`M'�lo/�Ѡ�z$�	�R��� 	���Vh�]��pN �P`����;� 	s�"��0�.�01��*�w�^K����������p��p��P��f� |p�� ���]��@Q�Ssb<�V��;㕏��	���@��A�;�/���^�a�Ѡv���q��F��0�Ԑq��`�ɑPn�8.�� �.�������p��>������`�Ͱ-I%�>�݃���	~�J�0^��T�Ц~��#�	���@����SԪ�R�� �<��K8�k.Ɉ��1
�΍�E��ʁ�k�f������Ȱb>����+Ƞ
���-R:A
C 2��p<+�S��@�A��R�C3��Tp�Bj�!���p.b��AB2�(#/��A�^[�"M �w$�!U��c������~�pȀ�� �ˠ� A��H��0l��0]�J���O6M�(��s��C� R�_�P+�� :�@�ǞD|I.[�lԢ�@�"����/�������������^^�R��p�0����Y��V�0T.4�P:,a�/ 	�6��6���� ? aϞ3g���CH��<�~���� ��9{�꠽yфU�f�THκus���UWV���Y�y��9l�R#N�ItX��<h��9!5�*�YnÉp�H���d	T_7����b�o^X�ق>;��U}������bP�<��i�m�{c�"�l��|�F-�.غc���k�c`ǖY�n0B̀���Z�jҨ	s&���Μp�Yo��z�R-�7��R��U�7_�m�֫�H��F��Iʉި���tF�S=Hx�=y�Z�m�56y#�М8�W��X)��ek�h[�F1�����!���p�(�rB�\�˞N���&�T� ��$T	�O��ǉF�"E���{p!�B�)-�.��N���R�&�N�2�Ʊ>Ao�:�O��~�Q�!�ʧ	bXc+�us�πy'�x���eāe*��̀��uġ�j�A��T���!hb�$�!�`���b'����H�O�#���!���ǉG
�-?I1�B?��G6D�q&a�٦	Uj$
�pH���D{�hd ��RQ�l��q'p��!�IR��q"�j���(btH� �ʽ����Y���P}���	a���l��&�OV�'뜁�F}�9�5H���'"���(�.�Ä���F md�N�H�N�ء�|�t�0�*[����d�)Fv֡f7�FjTk�Nm��ӡ|jhT?�'|���I��[������S	L��O^�K*7���Y*�h}Q�"��=U��ȉ��R�	�;��?��%q�Ɖ�:axU��&bh^�m:1J!��B�۴��i�q)R��@Ij U:��&i/���G��g��b@}��K'��&S��'U�������J�{�Q�y�'�|�i&cd��l�u~�g�n@2�d�V^��|���W��".CseGq��HoD�Ha�u'�Qzdm���gJ�����v� �F�sHQ�u�r��]C�
���il�^X�r*{��(�����RAIE�.� �3<ф٣	�KXw:�ͣ	�"E���dp1����►H�:����� 4�u��@Ô3���c-[�4��'R��>2��(P���&�v��}�_�Ҽ�4�`�4���\LMK'�@���<E�
���΅1S[��&@%'C0�X���:�_��Y�"I+�p$
}�ְx��Oh�lA�=���h�(�p�y:>9���b����C��x�2I�x�?�p5��/{���'� �R��*�xD��qjxO~�
�b +>L���:Bb}1����Xr#�tp%�8�iTF��Չ��м��$G�r*i)�A�I���?��Z�T��O�7QyD��aȰXα�*�m�tYsB$�*�t#l��Y=B���1���J�\�BJ��W�s^T���t"O"Jm��n��
s!L�%�j�#<�؆�p�Ѧ��	���X�.vA �G	�q~Bx٤�%7Z�|��*��5#D�,��f����X�9�jdc 1�>�A�~֩�P�<���j�"5i�)�1I!�=��v�c���T�(,����Ī�BۄͣlOQR�1��
�q��F#@W�� ���Q� ��	Cz]�j���aO�����<R�ʃ���*���	��և~�q̓Q)�=l���p�� �
e�����	r�ib�2�ar� �_�Ր!�"���҄��C�*g>D1
�� 
`;�t��oJY7��X��I�e���a���ő�@pC�a��Pc,��F�����v�&sFq�:h��!�PE��Q2vx%%�"lc/��%�&�3�QX�@�"Q��
g�ĝkPf��Nu
k&yDR��	U�6I'F��=�"<5��\�8�c���C��i0� H],���� NѴ0��>?"����0$2�rJ��V�bB,z�`��_�9�òi�\�i��jb�u�^�"�g�XͶ^��]���ؐ����D��p����^�-<d{�+�>��W���ׁM�<�AXv������m�l�߬	�������Gx���:ţ��b%�W{�#��-�8~2��"ة6�>r��<��4y�U��$f�8�j���&�c�	;���`P�M�!�b��r���GGz�.%�Q,_ĵo�nݜ&;?1Ѓ>l�w���N���>v����;�w>��|�#7���8��2d���%��Y&�C��c|��uj������8t�sc\=�F0���՛'+�i�����}��G��E�r*��=��HL;�W������6,Hڤy�v�v����.�������U��tw�c�c��=���7�i�?zt髼]��}��N,����7˼	K��LӘF��<�[��>�y?}������u�;�,�?�;�iH4�໅���s���?�;����?j�(�ȇ��9��?���2��S?<�v
��Y���@#6�C�Y�@��>�S��sAv�A��AT�~�;�[@�S��(b銙CB�;>�m���s6�)�'y�������U��x�h�/�Bh��1\�3�1C/,C̆U���*��R	��8m�
-�(�Zs7���l�l�oH�n�<���np��JD�/硇n �L�D�¯��>\딮����;B�0=��5����
�hw�9��k2�v	����5�#yr7t .��;t>8�;~k�j
��
x���
� ��x���
� ��p�� l�F��#h�  �	X�Gm�� ��	�=�D��*{�G���>�EO��F'$H�  ���D?`�&h�!����l��8h�t�8p$I'h�,I'��� !pO`�y#���R�D܊{�ܨ�6��PhI�=��F I'p&@ʥ\JX�AJ)����G�9��qP�y�	M䊓�1z�iE��
���p�a�N�J��N��YH�^��i��Y�Q����M��Y��JG0�N�H��I���;�XKǄ��+qC�rt�p��t�
@ �L�<�
P�͔��d ����@��� �h�� @�` 	� &��$����� �a%��P� � �Fh X�@ �4H؁� 
p�� 6��5 ��P�8�HG��4h���"r�
��"����b�p���l�H��8PȌ"��� x,�
�G"�L�!$8����Ng��i�/�C�T$v��Y�A�8�A`�>��\Y0�PP�8�����`�8x�8� �� ��&8�!m�Ip�7KƚLi���P�� �8 �t +�� P�@�+E S(�ȁ/��%X ���p� ���АS��l�j�&�4���MpNB8A 2�0��0�8h��4�> �ԁ��� �ā&؁�x�h � ?h��1e�p��Y�J�8PG!=�"������#��
8�����#�J������b���%�2H�W��)�b��W��e�8��u�@
��	 �W8WA �@�0��h�~5�&`i��1s�A�&ȿ3	h���� /�� �X+ 8 �] +m���F� �5� 8 @Y� ��<D��/�8?�0؂�h ���0H�p]�WHC��@2�Y��>x�K�% ��4`M%�  ���(G
@��@�ic|��t��[����%0�0�� �eUv�Se]�,�/u 
�S�
�8��q���|HjQ}AP�8��K��J0Ax�7��@ �h��x�%YN���7��4���R5�������؋N���D���S @    P�  +m��E�������� ���R-u �d�A�A}0?�2�T�eՑ%�D�_�1�e��E0�@0/؂�M��3x�NM���P��� ���Qf}�	��t���Q[���X��u�ĕ�F��U�&(�(RVu ���em��X��X��^xD�8�]�K���uʽm]�t�N��2x��D�W:��0�h�:�7� �]�l�+���WA��h: 4�N	p�6�����Y,&-��k����/=��҉�X���( ���@�,@@5�h�7����(�"� ��`��|��A�Z 0V��0x��]�U�i��Ue�@�Ie�0 ��-؁X`؁R@Vx��L��v�Ϩ@b��a��Y�M�Q��j��t�-�d� V�bX 
���pN ��Xp xX+=dxՃM�Q�؅txf�y汸�םe�}�Y��68��ցp�/N�����ZI�u����Ih�(ڼ���i+8��@�8�P��V�� �M H�8 ����84��,P��,`O�D @� p�%Ѐ%��A�Q�Y�=�5U�m�08Dx�Ysq�V^�bP�D�7�� ��Y�Sh���Pa� ����ŀV� '��lݠ}5���]�J���&�a��C^��^ ����`?��<�*���+u �"XN��!�4�����I��h:؂-��@82�����n������6��@h^���� ?0�(�'��"�7?�L8j�� )oH�
xh����-Xk2��-ȁ� ؂h �M/�R�%���
I��#8�@��-gHe`x�w�0Yxx�Vv�U6ZD(�@�8G:H����h�����ͬ�
� J�>������H�ɾd�FЄXp%P ��b��5�8����:�4�d���<�(�L省
�� ��F�i�_t	(��{,}%�: ���`s@�ڎoٌ���&n��:p�6����S�N��{����N��� �n���/Mj?�c�>�8��,�sZ>�/(��.:�S `^���%c�3��?�a�@ePu�	��@He0�w��_v0��^e?�38�0>��@N@� Y�^�
`���80�NȆ@�a�4pu�J8H=p'ȁ>hP� P�4���W�w[(�W��WXT:��v ��������n�����g?�4�Q�ȂH�h 4�s4����H�(��_ O8��|^��RmN�g�8�«`�&8Mq�R�c/m� ��'�P2��/(h:0.��xN(�4X  �X� ���(�]�c'ޚ���hh %@�~�w0`ǫ_7sX�U�e�>���:HZ>  �x�-��q�|�/�a|��U��̥��H("?' ��n��8���W �Wx�e�VneNx�\Hs8��z��ҕm"
�'h�u���yP]B���jwr�2 ���4�� �6ǁ�� x��ͬ-��4؁I=r���ܻ���Ӣ;Ui�M^SGuT7�*% X�@:(�/h4�,q(�- �`���
$������'v�7�bD{��Y��&Р@?H,� A�e�މ��^�����3�lܲb�^Jt�-|%
s�NHt���R�p�Q5��,Z��ȕ�D�Μ��3cD'A$4	�Ѥ8FJ�j�2Y��7��sƖ��u�U"?>>, @@��L U�*Tp��T��%r�8��W��M��G?�
tfn�8r�ȁ�2Z���A�Ç>����&�>��6��4gڷ���ѻ>j$H=  ���X��@�9N�h��DN�ѡX�� R� 4�	xmdOW�E�Q|��4�� ˘#;���RL���<*���2�r�	%�!dD��$��#�Ԁ8@�U P�J>�!i1iO4d9�J�0C��	��!A�p��2�&NK(��<h~��1��Je�d�!��T���h�id�(���;�P�F�A�r�hqYdу[�q�+��A�N��F����a��1�<#5�9#L���4,��d��T��
Gp�<l�F	=�=�� ��8P��Ͱ����M���#�4�P5����%�<"�";&��f�`┹S"� b����`�0�$����H�@�8PA���]=�X��M4QC[� ��]�	$���+���1m��=��ctL� f�9ƨ&�=H��R9���p��� gؿ�#�,�����7��/ڕH"��1*[wFm<�uH h|�[�H�Z��F�T�����:ђޅ��T	(�@ X�A=��F>�@����	Y`�_�>�m#�p p� 4�>IzW���̓2���T��2˨s;DS����\�_�f�2�@L�	^ba�8T #(q1$�����@ĮWa1Y�˚���� ��Fx nD%�ԗq��z�(�K�4s��g�\p��T� Q)�U��
p�X�3�U}Hc��MF�GmA���v��q#
ϒX*:|V`C �@�2h!a��1gP#;���D�7N0f	H p�L�JPB�,��Q]�uD��J�J `©u �Jx�e<6��*d��@@`,�4���6����M[�a�>�&q�� �� [L!�@��(��`W��уGp�x\pI�^��
@��½��@����]�/9<�������
ԀT�0��'��/e��-�8��>�2e�\(�.�qq�W��ư�-j�V�m��@A���o�CdA�.jG�ʼ�+�*�<� '� �-�r�D�o�H�2�r�-,_ ' � �(�=ĿD�EAD���|����8,�A�ͣ�ܤK�!p�c'��iM�����d�c0� ̀�k�p�`��;^���84a2���`�	K`@$ڊbF%A[GKbR4k�`�4X1n�6�@ 	0�iOkZ�1 �0GɎj\�.<H��TŁ��n��.xf�W �{��q�H�R*d�c | �7d4W^��@*Ƌ���(�ף�`=�B��c|(�;�)^��"��-J!  �;S�JE����0�F�m�$��W�3Q>"�����,^�q`��c��	�`��a8�@�Հ4��h�ZL�8A���ɴ�zt[��\`���� 
�P*Z,h�� M��)�cD/����@��= (mB �X�
B�H�T#5�x�fЀ� 88�����~��˸8���@���0�@�S)A�!�C�(&h@��)l�p��8  �K��p�/��PK̡�e�Bi���Ns��+p�#�h ����G!z8�}�C�p/-�	�f�!��c�Վ�Kā�[��.E<�B�3 ��[X%���s����@B��_y,�v�4�
� Fz��f HTA,4�G�o�/��=�!h��'*��J~�S�����`�JD�!Y�63I8����v�sN�![���8�2`!���ahN��Nl4W��.5�y�aj8@���/��	�����3�c0밉��ڜ!$ �J�<[I��F������: vpj�d����D�u��%�-�Y�*�-��[�ь���8���L1�Z�Pb�@�cK�l�e�&^� 
P yO�D3���0)�#�p�dsնH~���$����8�8���:(�0�@ ^�D=�C�z������ ����P����<���2P��0�4��Q�bZ�L�C��\)0���],Ó��e[J�Þ��|
@�I� ]wlWFd =�C#@Qtq@H�8�J��Ig���� ��8|���Bp���� |�A|��-��	IUt@H@�P�G��.�K6xBĀɽ���|@�dI� ���^�`��]��q��ψy��VTpLU( �@��ݍ'B|���{�@�\J.�B��-�\��I����B.�!BT
<ZLQ�A�Xڮ�_�Op@��@�@	ࠋ,��)�M��J��4UR�@���W.�TA,����0H���VC�T� T�_�UO�����L��8؄-(�� � \A
p@���H�	�b�@U@E��4�(`�wL=�C#�`�Ǭ#^����Ё���q6�����f1\4f��� �� j�^) f|�4��=xCD��j���P_��\с�Aq1"���A���a�-��A����B(��u�HA�*A�²t��M�T|U#<�D���2|V�x�2$�"Bޜ@�-�*��Ow,��E�,�Am4�<��'��3�C�q��4iO`��:8���(��	�Th��E���N �|5��'rD6��|��iܽ��@̘̔�IP���KL[�1�
:�=�Ó�j�8m�X��p=@�>$҃Ȃ ÜH`��%̈!�T"�s�YB�Z��dp��@��S���Ubvd�Gu�vO���pA$�"h�|e��H�6��ML?э!����- $T�l�c0rBDbt�0���� �� M��
�e`�C/����D�<1n���)d��A!��@R��_I��]Ix��TC'd��G\PIL��� �?��Є�&C�fL��8<� ������ 9q��@۩��8�j��������%"�A��a��N���tH]9E`���ʂ�%�C�I�8P�+�A�@�Y���������`�ɇde������� �B,�4�v�LFB��
t���X��p�C�ÿ�^X��%�A�@Dڅ ���M|�x�m��O��������Ľ0��5A���؃3D���|�����A��B�܅1��PS�U�f�%�IS_ }u� ��0�	@
JG*Ս�!�����1 T�(��2�BLA�BTr�%����		@˝F'@���!�[F�C��2@�	�F �]�Ԣ)Y)�U~�9����I�%·=R\� ���f ���<��<�@����4�BpB���3��2��|�G�e�p@	�@
�@`�m!ߜA!��]XB]ԑ���L��h|�=�� �T�ό��gJ@Ddr�)e�5���� >�C������J�-N�����jMU,lU�<���s��B��I���ƊF10"�2"�*Q"`�[YL��Ȁ�� �A$D�x��'�C��+��! �@-�"4g|Y��eK���;���M�Q��'�@(�er;��(A��Oy@��� <�+H��C5.C�����V��b�܍�F���\�t*	��1�� ���Zi�%������J@�XA��\��^x,�,`&�B(��8��k���Y��.i������C]y,@ �FN��z�F8!��@�:���U�@Q��q�-p�,�\�
�.\�!�F�Ȇ8����8Cv<�Gay�BpaJ��?�Ԟ�����_4��- ^(�M9�(��xA������[�E<�_�^�ٞ�AC <�b�=��� p�����h ������|�X˲UE�Ҍ�R0ΣF8��GdM������lN0(��`��,�,�Ay^�,PCk��'Ǆ4��8�����Z�T�U�Y�PN:t��A���H�4 \
ϰi��a�$+Q�\J ���/l�D�l����,�q��>��D��MY��f
�^��;<��M}2�|��2�1����k�eZ��.O��}L�/��+��
�p�Y���p�0�Y�A
�������xAhJ6BQE��A���0�@<H0�< ��y�^5 |\$���.ls8L�8��C/H�/��,�B�A`B/��M���L�C1�M ��FF��qƖ�.q"4IN�0�@���T B͜�EB�$�1Ŷ�A��a��Q�i�X`�\&;D�2�"�@���b�^�_�D5�g5*�9̱ ��M���2���8
�^=Dr�@�� ����6�<T���+��8P�N�b�A��&�l���M�W��D�<�A��Xɲ��HHWT	�UA��B_�K����<�B@� �B+�-�<�%�'���C:��F��li�U8��k���-���FQ����e	�
 ��	�]��2�?E,!�3�� �.���� r�4�v�D��8�A�2 !����������8��1�9�1E�m�ݰ�.� CZ��۝�>�Û�@��G	a�{���|@�,��Kme]�S]�2g������z��	�e x�b6�t&��/��w�C8�Y�T(8^� !��`C��-�o���%�A������kj�4LO\��
I��<3�9'R5C]����@R�P���mP��� ���,lq�9E�@��܀@�v��Ѕ'C�lwPl�.0���ȏ
�w�6�C��6�p���.�����,�عJ7�:�{4@�JŪ����0�@��@��<�CL�"�h�]��9��κ�����kp  ߤ�{a\��V?�wt�kF�	-�����C1C	��,4vb̳�����4d���u/�,DB\���$P�ca��e�|���c7I5�M"��MI����� �%���1�D���TB+��ߘ㉯t���� L�,]�94;�M F�¨HU
(|p��A��
d�|}�ˡτ�`|���d;�A�%CT y\M�ԏ�A" �$�A���L�D7��_��5C3T�8�Uh�M��@�q@���!��G6��G�Ed�Z��D �D@��?@ph��X�e�ĝ�޲I���ء&N�V��3����=�����S	G4hJ��d� p�� 	$X�>�?��Go�D�^zӦD�(H��#P�J��Ѡ��6H_ٲ�� 2m�h�CG�!�t%
�&ǉ�h���� a����pa}��G�c˖%JT�@>�j��E:g�B��X�-Jtp����^]*k(Ѳ4:�l�uذ�y�{�c�fˈ&0X�ू��b[���q�>^�������n4�\g��AفÉ6odp�A�:���A��>�㭚 p@�8���@�@�eq���2gY8�X�
�鮣{�!�:�x�ȡ�\b�<@�^x&8 �y%BȢC�-zX�	:聎D��@j� !��@j�W�9�A��A�4.�/��8h`�4�(�0���asƘ��7i�؂�K4l2]�&�u�ᄓf�9$A&�,A�*�N��dL�eKAYD�xCApz���&N�8	��  `���$�q,��:���ǘpƹ�َoK�3�,z聃B8��48���>�@�
��'~C��n���@�@�[�)gBs��B�3��W.��d�yG�|8�:���4��I�p!���;ߗ
��>��`B4d$����rx�4�
�	'`�%	8ryE�3A#�8���~�˸\�A�8��2�X#�P�L>[��q=�p��E�Q�e�!e�yes�A��W >��ȐҐ�W�!-&�'@���U��θ��HX���=�	���b։��u���ʈu���K4�4��r蠁8�v�p #�>��@��Kr�}�&@` ` h�@��nGp�9'�0�'����EYg�6E���̇6Ѐ#�Wf���s�i�/�`����W���h�����=8hY�Pc�b��Y��6��E�����ErQ��X�0�Ao�� Hi8�k`Jac�7la)J(�e@�mʘ�+�Q`q���`�MH���[��?�)c`K=�� 8�G�C�P�Wp�Pi�D�[��1�ǘ�<�1�w��I%�\�
z���-�V'X�r�� 8��*P<1�^�s� 0 )�z���-p�|	�BΑx�b�Ї8� �mE	'8�А�#��%$ ��, >�0d���=�9�p��(�z���d7[�sK��&�x�����x�%� d�1�Q �k6�	=`ea�!F�C8@��!D��!:KA2�E.�Ѷ\4a�x�| �|��q*��d0%0�3`#s��A�p;�>��:�!ք�!��QHcID!�h��@�@��5�u��"'�� ���w��H(�@��L�� ;G�ёs\�D�bHxl!7�a#h�%"�l�#�* zu �	 z*Pz�#{�p��J�:�ZلPКH�e�b��8��[D���^�0��	S[�A%s��@��-��3fHCސ������Ϡ^��s���$j��E��@8u �x�v��'���|Î#�g ��ӀЫ@3��8L$7c��#yL��j,qLkF���P��0@M>����#�A�$��?�P��@HCX*�c�#Y�G8B�a�#&�AX��z�tˁ� �+�b��� ��G�pF5�qe�6�(���ڈE�j���4������XXfkB:������&5	atM��u��/�A=����1�����D������-ѺhE,���<x����"�'~:��1�@؊�ƚ�5�S�ϡŁ��^g�d"�~�����xp�
�h���H�q�Ra 4�i�u�jP1�5��P���*gW�x����,_q���PC�B�$3G2a��滣 D6V���<2"D���C"�x��i�p�9����8e!�Kj�)�@xP�@��Qs��p��8��ef 6���D\4�K�����R�8iv��\BF@¤��-�a0��G7�G^w�C;`��ˀ@����q�����iy�H)�H�nr1�g�Pi��X�N�S@�H� �awq:yp#��#���p�*GC��xbq��OtWx<fm�g+�%B�`�@@��� QH�Dٿ�Aq �T��\���!j����X
B��J��b��-�������!��Kp"OyB��[e#]�d�B�*D��B�4��f��+���E��`���7 0�a��� 
5� 5�"	n��z@��2�:�B���N2��N���%�� LP �@�I�F TC0�@ *�����0��zBI�  � �4��(�ےE!(�DZI�:�����,��f˩r�� ��.G��m� �<B�"�6��a���^�l���֪P��e�P�ʹ�j0E��x q�`�`��V�d��7d�����$Р6+�n^`�n���"1�֦{"�q�N� ��x ���l��ƈ����z�)�b� �G������tlp(�u C2�������v�c$@5��Q*���xcږGfߨgJ���V맺�Y@�!�eŇ!<b��A�#<&�����A�A�� r� �P��e��Ȣ4��$�ALE,(��d���da���|��B�Z�K䂝����BD�A�����T�'Fa� x��`J�-�ht!���f	2�2��t!��8� ���8 �@-�������f�p r�F ����N��H��a{��|BYJ#2 D��(��=�@v�ѩF ��J��8Zb�q�yf�u$GVo>��qB8/!��;���2h�檖�C��2��Z�)�n�r$
�����Gj��LH�B��(�n
LhNBz�1NiC�"�$�*��`TE�@Dd!����L
@d��`�`Ͼ 6�6t� �T�z��6����%)��c`�ʓƀ�`cq����|����F�P���F�@��?���تR�cR�)p�Oݪ����O�P wZ�%����V:� ��(�	� ��v�0�A<�`�MY�c�4�B�O6�Ʀ�%p�!j�@b�.2Z��.4���?��%+��A{���bH%� T0��^"-�`a�Ni{n��v�i�i&��iR����	����۠h�� �0E���a�&�n�(h^�Z`F��`5���~�������|�%_Af�`���8&��pb��&A#~JbJ���� T C�l�n���̀�`[4`��W�e (�VH`��� 0;�=�.��gA�� p cTL��_�G6��b&����!�j�B�Ϯ�l!�%/z�z.'� ^�H;$�a��"��~ �ΐK��K�,������L�+>���8�"�/0"U�p�'d�̠��>�
��*pB�B��a�%�&$ȁ�D�@�?`J`�@Z �`�BA�}�x�mR��b��\�
�r�W��r��A� ú�!'����D�����6�`�` d
W���> (��j�q��%��2 ��ڊ�%���N#aaDb	ơ4*���eQ���'��GY`A��AfB�  �No�H�f(~bơ �),�Fn���*���n����M�Z�(�T����k2f��P�8�,9c��F��c��s�њ�`����z.�$%`s�F���,F��R1(��CgP���fp�v�k&ter@m���#Ř��*�"VyMV� +��v,��b$ �r@5> 8�	�D^
d�gDA�?lƜH�a4�,(���Y^�ܠ�'���bA�A�G�� W �nd���
�'P�x�A���Ua�
�*�F�Icꆊ����K\7~w5���*&��*VMm�y�����`qL@�`!#f��d��"Dm4��(!$`{@���p }�`���
��qL
���`�8��#W�mD��H  �G�AC�Br�w�e��x Rn�.�@9JP��B*��X��7��%�qW�Cs�Ec�mX��XfK���'O���X�,Za���,Wy"�/��*�y�yGt�����IK��,k_V��� `�lh�*TaE^����fw��T�>����������V�%�����Sϴ�-�H�/O'.n�$�-�` z z �p`��ej膺A�8���� >��f
0W���z�@,͇/��� ���hVzk�M��4g "����r�m��G(�!d��GT́!K�aT)��L6Z�\�U��a,��,�`E!��FeA	$�z�n�t�zNO�� y#�#3p��!�K�i�-�D�,N,d+���Av�^4�S���S%���T�8!h���@�C���r"�2�̏��AY'R��bJ@�*+N�5BA>�o?��a� �p����r��1�mq*�d�d�Т���Vg��@ظ�� �Wsd�m��	���=,�Z� G  6k�
�1�%��V���"�A.S�`��@cr����&�osCs�/� �͢���!�30� @�p�6KJi3��I'�i,���J��q`�1�ހ�!�y+9���9���)Р�
6�A�J'�|KH�(� `94 n@~�F ��q��0�fA	�;z���� ��@Z�l�� ���q-@�#��hV� � ����&  �N鍨��?�@0��@xG�^��BCj~`AY*�<���.��� �ab�K:�Pc�A`�����GW � �W�X�<��h4�8��a�h�6��� �\㸩ʿ�T�=�ɞ����s��m��	C:� iϰ L�4 ��f�Al� � �� 4d?���@��h�
��>�f`�Ò����@%���#�� �����"��(xS튲
zV_�Ճ�{� ��`bF��*��$@G�U��0 �IFY1@��� ~�E��E�!� f��!���K�@�@fUT��� )���y�E)G �Dl��V�b������Q&�@��|��*��Oz~����������������l�!�ʂ���`�$��2!��@�t� ����`h����AV�"�A WP`+(��pȢ�F�Fa���r l?�W  +�2W^@�6&�;��� �zM�b��p���P@A� (H� a�=}�&R�X�[��<@��@�9D˖�[�����8q����inٻW����b9�:�ꕠ80P��� 8p0��D�6�؉��Z��.�+F���c�-�%��ΡV����u�-���JtH��	t� dԪU�&͙f�b;{�7���Wd����̗�>84Xp2�+e���s���e��1�'�lDg��h�Q�	!4��`��%|��/��n�;��Ɉp�x�!z��ƿ6p`��W��6V��<g�-D�8p��
-@�
Y��U� AL �eMdQEi��`e�T� 	����-˜�;��s�9��s>��sO��ԑ�h��u�u�1`�M�	\ŀV�]wf�f�>�,�l� v�q�!�,�K_�GGt�Ga��R�+���
R�4�Ao@fZ ��c5�:�gVz6��E`e&����h|�94��	t��2� g�!�,c��*�H"�i�	$]1�����aͳ(X�`�Kx�-��yt0��P 	�
��K�L+G�A� ZЁX"�u��$H��H0A��V5 ���} �A\��YU��Q�@"T l�]��-��O>�3�5�SO>��!p��l��9H0IV��A[��G[aPM��`iV>�ȒT�p��![�q��t�Q�T����t��-ax&�p�H�� Sh��e&M��5:X���-�UQi
'A X|��2w.��(c�z#��.��A%|�C�'�<.��ˎ6��8j�<τ�X��H;�A5�����@u貌.v���\"vFh�� ���$ � t� $��<��f�Q�7� U ��=�@��b�:/݃����K9��w����I�#�-��!(C9��� P�Ph�X��F�f�%>��o��!!���Uw�.r���;�BvjQL> n��d� �a�q��ʌ3�6���ȁ �@3�at�B����k:P;��o����Z�*t���ASr��w5�)���1=�Y�f��F�p�X�J�	N�t ���ԁ�0�2�!v$r�3�xp��@�S�w   {ɻ� ЀP:�@�0�J}ph$)>��@@A8^.a���}/z�����\��pE���CÐƘ�@P �0o  8� Vf���GAA`�t�z�C�0d.��B�)%x�l�']���Ať��E���P�@��۠�e��p"���<�1�"@͉?������ )�МEp��T���rA�#12�!�%���y��J�@~UiQ� ���
A�$J�0H*E8�0��i�H�"��c$Z(�	z�T��d��>I�d��\�<��S��z�4�G"0���!�[��%�����s���lE�P��c�P�k��jVr x׻���`6ÜY�ac��D�!s,\1�xS�r[=�J-y��2|Amp!2*�j�0Q
š*3�>qp�A:�OVh.`�%A	
Q��d�8�y;H�W�]�"c q��	��	\+��,ĢƲi�M��f��T	hOH���!@ڀ�sȰ��em �z�-���"�� P���'*[Y��2�Ɓ����0ġ�(�e`� � t�j��-�!�����F�4�`j+��0ҸE��m�KD  ^��� ��]aB͂Nv�B3�F�ZեW�&h�x�"an��\��Z�r7ldi���@�VF��s���j,��x�8�9�F�kꂦ�P�*T�?��s�	����tȂ� /x��Dp����@��	�K6�癊�CMh�:�t-�5�� $P� po9�>k ���-���A>��>f �\� (�U��H�A r�� '��#�ֈ�[� �)`�z���;5D����yi#D�#v�DR*��8����Q�t�<�-X��t�vdǴb��D��	A,�2�{!��
�}a����2��7(��K���9 wk2��8��\Dmƹ��*��'��4^�,����X[�[�;�����TN�Q_�T�-D"��2t��B�8��_K�E�ůh>�y��	2�@<p��~�#h�U����&�(�;��-A�7Xa��p�,����!��F`������Gل~i�� ����|p�D���p�I����x�nb�*��� �ЁT� H�J�W���#`N�}jvX�'�i��>� � ��-fP4�B#�0oV[PV�&b.�
��1TeC\�*5^�i��Dr�)���A��g�m�3�iV�,^��Z�^ pY@DoR"�3�� ,����JPy�.�":��+`�tJ@/�����Z�qY�R`@aP�J�4 �y� X�Vұ �/��l6fc CJjuM@<�4� �7�,��	k���>��0��0�$� �Ձ�J
0~�"� �,�gk�� ,�BUeH��%� ���A��;� K�f�t��8Ye�e���\�p�$3(pQ�&Y@DY`Qv�*�^�"��
���^���r�R.478��{*@W��5�����F���f@� �f�03��� o@��*�Z���18��3�TM��b��<Gu_��/§�Cq �<� �?�=`J	�p� ��d8�aV�U�
� B!b0�U&�M F(��4V'��*�jQ� t�����";Q�"�)�Nt?�k���KXՌWW5�x5�#�T*�,�D@�E��\ Pw��y�"T�
��_`v`��jyr��ő���1vs*�@b�7S��A�<��@J ���˱,p�2����Z��fB i<c�<�S��Jx��v_M�!7Vp��O�x� �a҂�F?_�
��"/(��>��aࠉM0I%w{gU��=Q<p��pG�M݁;d#����EN�`��rv��W�%U�5;� i�<X�i�B�0XVW�P�Y��hЈC�`� &�>�)\ԑtp�!DADv3�((�>� $����Dh��BQ�u~�)S�9�""�>�Pc�!f�(�ݰ�p�t sH�Ӧ�� �B#/�d0.�	^���r`@x�s<)�"2 �CJq_��%�TpM�0K�{"�{WA9p?�=�=���
����p� �����0	� ��bդI�Z�ܳ�t<�� 3@/G��(� yl�Uư$�B��*##����g@d�*�j�`%�p�?W�@V�Y(�R�	iK(�`��& $`�v��B�ňh�q*(2*��-��,P��h&�pg�@�1�mq�@j�i^{*��+-y����/�J�2�BY�R�S"P �� �hy��
�08#p0V����2$ � ˒``Je���/� ��+�W!9P���m�8��h��fv��	(CM܃V'F�hE�6�e5 �M���1�q�h9'ƀ	]�
Uu5tN]�@��G5��'�]e`&��sUu��1�5�u�\W��y�aYP��f�iQ�*�2���z�p*��Z�Z��ʠw��g��DG��`Ia�p-!�p*�h��Q����	wM�B 9 J��8��3���%� ��Y��b���t0m� P�p�r��5����V�=	G"#�g��Z�W� �p�`���������,ǃ feo!@�	�$�BP�A0�0Ԁ�����@2IŀA�@� ��R�P%�<�nЂX V'��q�!�$�����pu�h���u3��t!0�t0�Dr�]�x0tbRE����)u���hr�Uzt�7�P7�*]�kLUk�H�����(�a�0��r�c0�lx�k �!�X�3�"Zem�m�:�v ` 1s 	��<��Ӓ�C�~�� ϲ=��<�m m 5dP?m�u��k��� �P0�s<
fh+@�� �  L\�`S;��l�Z�:�%�35l�i'yj>�%�)�u9[]��`(� y(�0�Xǹ6�@�!px`*07�qf��6ADC�&��_JR�q�Igv�A�ْ�|3w�`�`��(s6C������ )SX�<���#'�B	����va �ա �|���b!�#���c�����+�,=@Du��o0vt��i[��q���<��� s8a��!K�{ږ2� C`�!�52����w�bIqy����6��%��9�zr	V�����W7�U���rJ� -'`盺�"�vD���� ��P�u�-�w*� *n�s(��g���PUԷ"�U��7��@ٰ
N�֦� �!J����$#pǉA�p���H���`���b �ފS&� V����[|��.���?��8'�[`?D�o b�e2G`�� B~f;MP�Py�Ԥ��:����0���L�-o��g 8�h�w��c�&��]���t�`���-��܌^�\h�B��#��>�%�f�7��O)#�(����� �ttj�"���pg�$31�YU�1b�G� ����`
��	���B��=R!� �į%P�%`p�|p9c��X�RH20DETG���S|"T$pT	�$��K��Tp ��
.h�t�#�L�	M�<� %[V�E�\ �JV�� �@�`Q�Wi�e�@2ڒ���*@W7��*I��ZzBB�d�@.�hA�S܌Zg�%9"�xp)��� X�Ad*�$8��tg�s����T奨rq�'��� ��am^�0bu��d����M��pϰ
� p
0(� �CF�K Q�,_|����F�c@xx��S����F��FƇ�Ī`8�c�   -[�i@j@�P	� ���Y�N�	N0	p<�CV_FV�) �QM���<�C�Ϯ��� ���0�H���N�^;~���*��v�$.�B��j�l�hq�D�5�z�\'ר&�%p8C�g`_�E�e%u�\:�ǻK����~*���GR\�7O�km�Hg�(Sm�xĿ
�!��w�IcĲ��,�Zf���D���(�+xh�	ʇ��� p��(0,�/�� 0�S��'#�8�i�nb}5i�p��?>4�ȅ ``@C�
�8 A� $Pp`A�m�D�$�o�+�Ō�3��U�D��D3С\���\V���W��	E	��-[(s���rָj��c7j5�ըIs&��3g%Ŗ4���YJ��cƌ�3f�ႇ�;��j
]�t�2q���w�8p�P&Z���Wu���%��2c��;w�:ϝ���7V��n��E���Y�k��e[�̙(�`�{/ `@�,g�1d(�c�C������	�;8�� �*,hPA�ӥO��oې	��D�}��P�#��B�-�(��x���?�hႅ(��2d�"@� .<��#` �yL	�Db|ŖQ.�C�@ޠC�@9#��tҥ��Z�E1���H�PJ��Y�����j��
+j��+�\�%8H��JH��/��.3�8�
3�#��Q&�C�1g�u��*q�gp�|����9�E��#s��C�y�K�y�K�a'/O�&�n���k��fnb���:� �#	�n2��� m	��K�-r a�8���(<	��$hB=nKS���(����F< ]~�� �@#�/xȂ�xءNP�(�h)�p�E���C@ ��
h��P��q�>�y�%s^ɉ�������e�[��ɖ��j�&9yE)������2�P�'�BP��L@��,��:���9����;�lq���/qđ�s[jO<����<_�Te����!��s�Y�E�P�ƛUgC�TW�� R����>	Rd�����9`�Y�W�u;�+z� ���n�p�	� ���[�}�I\����N���v�xCAn�!4��"�6���YH��>� Ć �=>H��b�uD��u��&Tjdڂ�:ڠË3�3�x�Q��W���0GRBv��D'�h��8!�j@EgY��π4�#90Ax�d�_��p�3�@P�ZQ(s��1�_�&���nt{���:,C8J.��
e�c���6E��&5�H��f��n�&���3�� p�>� ʕ"0� #��蠈@���b�S��� ���oP�ޔ�Y
p@�#;�e#�j�-
4��s���0����~��-���; �3H!AD`@��"�9 �Hn���hZ����P��P+���4��xH���9�h�X1�_|b�!5�I|�,�A�a�g`!'G"%� '�A	���-��i:�	OxB;�,d1l��F7qT3{�Q���N|��uH�{�S��j��R�&��h�6
SD���> �5`DOJp�+�!4K�1���A�!hq���   ��. �� R�� "i�HΠ�S]T �Mz�������&4�qp�`�?TϬ0���F���B ���!%` X@�!����GtP�����@O��G:���D�P��Z�����$EN��-���Lp�\�9�!�v!m)(A���:��m��	�2>&o�!�=��=��P�4��똠(c�ؔ�����#�cA�<�V���U�A�)L
'4�-(W�<�wЂӞ��H��\P����7�Rc �0@0�tcW;�'�Gh�^p�4��O��C�B���1d�%Y����!@��'BR!u�U#yH�"��E�8�/,�
W���b>hFO
�+��s��5FK8���ps�������ҚV��A�*� E6C b�O�JYa&6=�%P�	�9s3x'�r��t�e@W�s�[��ao�����3V�7�D:U�Z�w���p�� H�r�i��Y`d��&�h�}�Ȁ�:K30�yBe�y8��A��$_��4�\M8�F��]��@2��y���  �q
o�|8E�Є��u!`H����
�Dώ�4A�M�_������H0"{���BjHOBJ�$Yh�Y�`I;�R�S�B� ��AN-�m�����"<�ԥ��ð��3ˠ��`��9�(�c���=�A�Ǩ�x�J���0�Q�hDڻ�pBp��8��o8p�8��U.�R��,	��Z`�}�� ���p��J�����;�]@�r������?�hTF*Rq
�s� ��ML�A��-�k�o��\.N]�Zt�>�!j��Ѕ�@���-�!3�\�΀�8�)Dl�(%���0�".S�g�0�0�,t�
i��@/��]@!�t!��=�����:�Q��n�����@2�	q�20%��c
�9�`7��nX�X:TIo���.Pp)p����5�P�����	��	c���
4(�	耦b�x+� ����d������r)���i a �a �`��cH��S �!`X1N�� �I��5�!#@����0�-��&8=ن>0 ��ɾ�V�&cȇu�	:z���[�A��ɲ�,c�.C�����
Һ �h9p����48.��+��Z�D8��s�w�����_Q��P�'sႇ��0O	�G����V`�U`�C�n�(���h0H���
@�����"��3�=4�b �\@�A	`���i;^���
@*� ��H��� p�(�u�Fh�` �SضTj aH�!Ԇ` a�}<�6�!���
x � @�H��!$�	#�{Ș�7��Bh��\�W�uȇ��&���A!�:�:t�$��[���a�cp��DADĠ���/8���/����/؂6@�8���� 
[v �r�:
��\�CP.�P.KI4?���?���膋�k(�)�kP:*"����`�Rp'@6�)�;��	��ި�h�2��W�~J�\H�Z�j�� د �P����yH�p������	S)	 � ��f �SH�o;�a&�a8�cH��$TN�$�Vʞhq���Hzۈ0�ޑ��8*14v���;���� �|ȇy��&�(�� �����T� ɉ�jc�
d��-�.3c��J�����L,�*X&u��^AI]ȉ�H�h8X��qȇqp0J\�h&�����P���Ep����i �,��j�4�$LG�E_�o�԰�y@V0�F�(N� 'hґ�p�ki�e�	!�[@
A؁ͬ�H
M(�h��q��d�c;�P��?���?x�XA ?8b�jh�vІT(�a��v@�aPN` 7�;�p�h���Q��B(x#s4�B>`6D�B��Cp�Wp�Oq�'[�[��D
!j�,;�&i�q�~�ɟʞ,'Ӓ�l�3p:� C!:���҂:h2��68�A���1X��AH�Ȃ�<K�C 8�����f �>�?�[�+e:]�U�ѿ�+o��Ry��r��� P�!p�l��'tM��� �@h�)
���(� �Y��!�nh�t���Xb�?0��p� �D`A�m8�f��dXbH�La@y@�S BNPN�K�(p1�r��a��1 (��i ��{�	7kD8�D@CAx�F	�0��)�K��W���ȦW��$�?x�'-��8DgP��3jH���1@!ˉDge��z�0?����W��VP��c��Hx�K�-���{:P�-��M2*�A8����s �Έ�q([�?���R��"���Q�y��h�#�Ng�?'��-Kd��bX�� ?h�8�Np����	0��cҠ�ek�  ؁8�� v0� XR�}�u��!dbp�m`�u�jXw�m@�m��~D�` ��G��� �&� ?�FP�Pi�$��uH�AH���8[`@�@��� ������c��q��I�[NP��	N�6Ű�4x�6x�&�IRp�F �F�R�g��-	3�2�4�.����0�|9�/��,@�H(�W{5x�x%X[���Y�C�}4�7��& �e8�Kh�W`��I���[�j�Q�Q^�eG�'p:HK��A��+R�jxR�`+J�t)�88�:��w8�c��b�9A�q�����0vPY؁��>�����Nx�"��� �5��~dk�|@w�dvp�tFm��@�Tp�v�m``J�^'�Fh�!� �'���p�������� �H``�F�_E�8�5�?x��^AHb�A8(&�&��8b�^?h�F�� ��'�O�--s�0�<p�(c��6t�6&h��8��� ��h?�*` �@��6�:�D �PN�V?��J������Q�x4z�k �)��-� ��`��X�g�ah�U� �OaNh���8� A��g�u�F���ȁp�����XA��&8�h�N �л8x��6�#�]?�U?�|v�y�y�d`bȇNw�TTa�bhJ�	����^P� 0�!�N�@�
P� xhd���`D��xڧ'0'��vi�&��� i5@�:nj?���mch�Ph� x�
8�X�F�sRdБ���+�'�9>��ْ=�<�܃ϳ~yX.����3��F��x��P�Ř�k4���x�n0�^��)��l��o�(HC��@H0�s���9�r����#P�0��}qNZ�J�>8)b� �� ���l� ��N	�>��s��#h�d�fcp!Ԇa�j�!d�s>���sm~L�!� ��� { '��y���
`eG3B0�@ؑ�a#�Q0a �Q��P�A0�`�y��zX�?�8-&�N@�v��k��8sB�4�-(28C2�-h�22h�7X����a¼-���Oi�!c�#B8Q����vcx�)Y?���Q"� ��1�Q�Up�U��'X� U���?\|�U!��eǽ�F �
��Cn�5x�A�cx�+�cH[ p������x���-�hf���=��<`�h�w܁4 BEH�,Nd8NX�$T�o�sN�?�n @h���H� 
x�r��0rz&�/��� �y`hh�
n} ���?	��	�/�(�:�)�8Ypl����ي��Ǥub �P)gbA���<����dx�e�c`�h`����f����dx��
x�Gw����qp���ɧ�)�I1�q}`�n��-b:����zw�h��Y�Gh�Y��̝�GX<�IH[/�>O�k�[�o8m~?p��}[ToB��W����S��~��h���sl��,��{��z>��!l��b6��������F��P�u`�t�x%P�W��!��n�<z��I��5i�!3&N�,q�f�%�V0Y�8�(�ZEj�^
s&Lج���fӧs'Ϟ:���Ӟ>�B�"M��Q�E����^Ԧ:�����'ЬW�Th>�fϖE/m>wh��'�ܸv��eWvmܹr��c��o\�]�λ�<xz���g���������{�3sz�8q���C]m5��/_;��r�Ъ_ook5ln�O�&�U���ýR�-5�p��}C�>֬��ү#�����z:��+91C�獛W��z��"��T8�ɟg5�{����qWeqNaE`P	�_O��'�uBxԃG��>�HV>�\�C��3�j#V�|�D��P�X߂����r^��n��؟m�i�\�?��\�ȡ>VF�x�1�z"��R��H�␽h ����uZηՌ��y�sZ(�~^٦�mzwde�u�dz�����'͛a��Ԍ]
��*�e�6ކ߁	��`�RZ)�F�i!e�5��!�e�&�F)�=z�5h����.������z�N�$�兆��Q�tbt�*\�/ҪT��j�m��((��6���ێ�Ϯr��.d��&*�Ui囨6Js��������Z�壷Y/���*x��9�=M2�)g��Yl�c���d�ٯ�:�ۯO�b�,���o�N��s��d��Lq{(� ��-��Wٸ�}*��-UT��?�	������;����G�(����I�ʰ�x�3Z<��G���.��"��ZơfL+b�{��s������TSY7�mC�6�uC��E�c��EQ6x���s7�ZHO��\h���Sㄧ����_H�Zg�c�����v=��������t��-=���"�a�j[�{T��^�{�C�l�4�d A(`A<D�K��AX@}8�BH���G/D!�L��L5�4������,O���3�d�L7٬��Ѓ�! �zD@`�����zN��0�!Lp���"��	B�`��'��Հ�6�Q�nt#r������yd�8�a4�Q�V#�b#Q�FH��Ct���Qtb��D,v
M8����,v�YP"�X�6^X�jl��r���t�2:	��Y#W3S����E�A�/:c{��C���4Q�,����'? ��6�� 
@ p�`lrȀH 	xR��fЄȠ	$`�> ���8�0�
�]ډ�@�2�9�	E� �� <��@�3���	8@/p@< �
� �� h�z�����g3M���D|L�3� @@	8 	p�7U	�� �A���5���(��P�:p� ~8� ��;;bJ��U�e�XA
�"aE�����FāNXB���aqX�4RS"tQ)�JU�R�(}���$�� (� p�V;�|�We 6 � a?P�p����4�.��4ґ5��mM�K7:1�L�����	�
<��}�( �|S�
8 �X ��p,P	I�©][mhӉ"��/ A(@ϊr��L@�9�`'�C��
A�@	�؂ ��	A"?�'J*��%H�� ��P%��hP��88!+�)l��F�#x��\�.��L����Ѐ� X=�p � h+
~0����� �	,�h�B��p`hhEf�J���N`�5��tb�u@��٭r�0pc=i�"x��,7`� �� -0��΢�[p4��n8�۔j^`�
H���ͦ8�5(!~P���C�ai@n!�7�M�k��Z�%��  �Z��t��$a	B(���	�S�Ai�#*j��¸�oj�&'� X T��  (�@���N�-L� %@�8 �lAth�9&QQ���I1EGLe�	��%��UP��U=  ` @��`�r��~u� ����X�#y'8 �h��@� `���?ú��2Dx����!�A7$"-`�,�E�q4���$�5\�:��&�m��r�	}eO�F�R�{�h��'�ap`lU58 ��[8�K>@�%�ټ6 h'p� Ё���,V��8d#� �6��ŬjDa�-��: �����   0�r����f �
$�`�0�� #�C���a
�����ZW�S`�ꪍ�u�5�M�~�
:����ʐ�C� �����R�VeL��0��M�n@�	H� ��3�(�[
�8@��a�R}�_����P�!��6,�M8��x�x��J��J��86~����8�C��@�B��AN��C7��9@ϐ���ے��^ @��� '-����Rݑ�9A_�UYR� ����t�XaX[9��A]-��� Ж�A8X��lBB|@DH��,��L'�S^�@$��q�3ma�I��pZ����XX��̃,����@�B+�L�+�lA�U' +q  r�	L���h �A+�8��q��9�<�X�@#L��N��~�CHW�������V��@ p��ڐ��x�)u 	"�I@�m@��� '�����b�W�|��A��	�����p@d�t��@��@l�h�A������
m����Y��V��y�5�ҘT 	8�4�M�aS�Q�h9@,؂,��8��.�.>_8� ���A�A$!��!��<H�9��8(�4TZ���S�]��J=����-�@���q8��l�&m����� 	tX�A!��t@\�XA#�C6X�SX�u��&m� ��	�� ����A� � pQ]��	��d%���A� �@��;��;�BOy dSh�����g��?��M���a�ql@��u�.�6��+C1��=��T��@$a ��2�� B ��-܃i��=@�=���8����ʅL
;PB?�V	�A��X9���# ( �=��]65�	"t�!d�"��!� �@h�)���P�NtS��@�, ^!������]��� `���A�m��@�@��� �Z����~���!E58X�T%\�qt-�QF�M�8��l����λ��9�4E�P-H�-��rFJ.� (�!��2ܡ8,�-�C�.�8��2h� ���C���b�%��)9���%��ɮL�x� �Ao�ȀxA%@� 'U�����i+��-P� �A
���u�ȀU6A&d�.MT������ 	ha�q@r����!�`A�����
� �� ���� l�h��e �A ��̀������K}��-V 65 	�]� 0b�9��(}''Q@XZ�� 	�,�L��Đ�N�X>tB#8C#p��M\J�1��80{
���-�d$�C J�9��2��$�8��r��r��h�� tV�0H4��%��aD(��$4��AXh@�m��A�� l�%Ȃ1��-Ȃ x�	<�6��5��$4B� 
c��4��d٘qd� 
l���z��\ց��{�@��n����!B "����!Tu�G��D(�@�@�H*ߦ�A���-@��:�h�BA$�+K��*$O����ܖ`��,<j1��:��+��!�.���C$��b$g�Z�9p��Bi(��.�| ��`B��dc
�JmB$�� �BE��1(�p ���wb��@���-��KCQ� ]q��@8�U4%8��!��5A��0�m5 th ��A�%B",�!��j-`A��A" ��&��.B 0� "��4A#����8P��OA��DI� T�Du@6�]�qS��R-Y�B5�4d�T�G����
�b��,C+�B��+8�p.q1�&�2��9,�9��i��2H",�A�*�%����� ,X�4�)�B-D�8�B:�C��s����`^��A�
�.�8Ю4��,؂-�A�2 w�@r�oh��6����(��~�\I/�A4� �p��+p�B�!��!� 'B.$�+,�(ߞl�%�-�
)�(KI�\�%�%�Bo
B+\���)�+��"
h��~ 	��p������$-`<U�=8��b�M@������-����-pi��n��C��$<L,<��b��$\Bl
�b�'|@��u.���$CE78���z�GQ=�4�q��%�f�FP�i�i�Cj�48�	����aj���@$P�4���m��,�Ճ� �!�2Hq��.��r"(C,C�R+�+��B�����0�G)�R��<8��$� s%s$(3Y�<�Pޒg�R.�R|��@,%4LDLn7+�Ƭ;4�h1Y �����
 <@�2L$���8�4����*AÃFЮd_�%�î8蓚��N$ /�D7��$��3��r��!�èmDk
u�itZI�ƚ�Cr��!gt+��7."��� ���Vȯ���64�36@XIoO��!@1��4�6,d�iu)��+��q�x)�p@���bG�V��B�AWB1���Y�Bg�Y7�U�g^�Yt�̆ 
�@�I���4���5��C5Ȃ��&1�Vա��s �C ء@0����b,�<(��ֳ8��%�Cl^�b��\7�B6�CG+O��$4���dl�tD��D(v��C:�Cb����C랃DHC+�����dj�ATB(t�J�B�=4B���� ��2��CJ�8X�4��F���oDy��@B� Uˀ̀�R��,��$tU&s+(11��@$s f��#Gz�<�
�����U���4���`�m��4�B!�D@c=�`�.ĳ���q
����2رd��%�r*��#;��s�^�28�=l�Ī%�i�Cߜb����,�i�8LClOCp�C8��:���w�=��:Xb/�B2@��J� `���.�^pS^1Th-�r.���s)2p);,�1,�:LqQ_�'��h|sl�Q[RP�l�7.3&��҂Â���+`%.A]�@?R�Zq��@$A�A#DY5L���5~�zQTC�6?B��9m������n��3�~�i|vr"��/�8��8��8�ò�C:`.&�.h�6���D5<�,HC�0�4dF�C����|�Ff�9�{���8��N�-`���_��AX�����Q�oO��S�X荀��!���������1бElv�C�,Ȃ�5��4 �@�¢K7Rān�@��2o�%�1�'p�-��2O~������6x�A�Eu����*Ui��,t�R�V� 1�-��%^�+��-��-4,8(���gF����C>� �-q��ݻw��8r�$8p0�Q/}����wcF�1�c��^=v���V�9q�Ɖo�q0gޓ�0�[��ǎ��z�GP~86uʑ��04xq�CC��z�,�b��-#�r岕�¶]V�k�@a�th�� ?q�=}:��E}�L !a��V�0��dKV�[�`ɂ���%Z�s����~���C	�:hT7�%�Z5jԤI&�Yn��ͫ'��
 (@� ��tEҥ��Wʼ.{�e+�8s+	|ǎ �{�;gN�2x��wn
,�j�<�N��o���z,�g��&�q�)0p�9'�'�V�i��c4�pŒ8.�0�.�h7'�G�#���
��4�8�+e�kK�e
T�%�d:G&��G-c�z%�6Fh��Zhb�&�	,ŧ�Ab>��c�V^iL2a�eNhy�V��N�r�+�%��=#�ęj`�-�B��-�n��D 8�=� ����΁�V���[�3�=
��Cx�i����� �,\��ꠈH��&���Ȟt�!�����i���'!uRI;s�1(�W��3�〥/���u�P@�	� �| ��E��U����%��1f {�bkY�J�C�p���f�&��T�0��	�
$@��N�l�Xb�%�XҔ������N�4r��8�سA�d��f���B�H0���  ��>���E�^����1��wV2����9hӰ�KV�5�	�e���%B�ȯ��9�F@�����Q�@u���k�ԁ��s�QuUq���L�C�{�y�zR|�� "�`�@ ��0��x�{�H|�1f�#�w���,�	�48���ʕ@,�(⦦A� D\L��`�24-#�MN�P��-���5�"ibOJ��$LBTP�E��n�q�`�
x��8����9���Ϋl�p��Y���L��`��xA�x��C �|����_*��z��;��ڤl㐆Awj��%����v�"'0Ada��9�x�;�>�p? !p�@4�	b)��Q�����1�z�"���E`[���P%H ��=�����cb��DĘƴb��,$�Qp�1��Z��$�!����H�L��m����W��G����5�h  ������+��8&�,�d ���.�$����@'�Ҁ(L;T�S,���|�@,�8"u .��e�Vee6lU��� ^��$n�[q�[��`��[X��0�D �l)��Ӡ��E�-�b����+g�AiT���9�D�����1���+.�ƀ�c��4д�H\"�S�*A�"�uhD�(!�A��>C��1�8�r
x@P� 
�@LաA�3�|.ۙ�yb�'!19ǫȓ*h�"\�>�J�)u�����s��@׼�\�U�� 9bäQ�J��!��R4	p��!��"]�;ز�d�G*)\Kz���àL"C~��С�$8�8�䗉fdu@�H �(�-f�[8���p����q�DK��Iā5�DDf#gc}J��,��	[A@p �7��%eˇu�r[�"��L�+a8�8��K)�,�c#(�
�IL�)jM)8!�H&��;�l��H���^��]�H�lS���h�"�p�%A3�A	D �1���]\�iIKd��-�c�+�P6��
��ZQ\X�L�qX�����3�ⷁ�C�P�(��vj�\��84�f��D#BA(��PMQ��Ad4���p  �b���;6u�,�� `v�c�|�bp�� ��^���u�&ך��%�塍X���)Y�A�-G8Ա7�#����n��V`�E/rBv� j9ǈ��e6l�hB^a2���@!��Y{�ekP�W/W�cx��%� ���5��t��+H�Z-!� AHP�XJ���^��P6B,��z �:,a�{�A���F"�7���ԧS%ڣw�sF� �`;��+�b���b5= C�ˉ�����a�8ЁM�xk:ԡq`B���G]SB	|�	%0�"�*���, .��h�2��L�Q�p��zP�?��^c����ٛ�����������HD�v�c�l'��Z�;>+�f��,Y\�.��T�$�t�x���9��qB@=]�nX� (�Zo�m�1!����G��r0%�k��L�!��"�MVщ�3S8��  ���;�m�R�����@l��ր|����X�Q�Ԣ��d0�cC��C�m��4(sA�A��F�EH$u��~�T�]�!vN��I�:�$ �/��Ě���A�a]!�:�"р!Tv;�!j�B���<�t��j��� ̀�,�Nb��(&�2� �lA�n�$��P��`O� 5� ����ά�@j���,�ا�*bH�	 iȐ �i����"�zҠ0�v�Y�E�����>�`	n�'���
�Z�a�̥!&@DM�اr!&��@܀9f�@���An�+���� �v!A��j�Ve7�-�8�P`	�I� r�@��ޥ��BڂP�:tq�h��+Z�r!� ���4���&�^�1l�1l!�~�j��	�`	�	WN�5p�.�@���Ks��v�?�:!�  ���an)����������"�|b͂.�@ �Al��v�T���!��&�����@|��`	�@.��ao�!��*�z��� �`�%~�%$#Z�$&�/Nh#D�p ,p�+r�"����s��BZ�+���u������8��
�)��	���@$�+�13 �.A	��>p�O��r����b�X��k|�6�/_Qh���@��� $@1l,���p�RJ�:J��x Au`@�z�
r;6���qi�`*��B�N$�a�����	�@f���N�¡�\O	�a�a�`РnArV��*����r$jv�I~�:����3Lk�2^a^����+�h�t�&��f����Z+�)�&�:���� ���z�`�	1�N3	�`(P�FX4��	�0�j��i�ҡ,�� Ei�P�#�0d8�+Z��Z�v���C;z쀦t b��t=�fS0���!��8ه$�A� ����v�=�Af��N&!�`E�D��<�jU�-�"4��F@�l�!҈N�
F�@F�!r@uQ�`��J`΀��ȩ	�B5��@:�G���$��s-��f:�`�{�B 	�"P�{ �L��:�r�-":a��� � �R���ʦ�b�vJ�t���l2h ��b;�*S!h� �ad�\�����$��&@���&�=z���2)� 2&
����.�<�������0;�.�� ���Ҋ!p�+,tA��`���v���ʀ�t����)"Ԏ��$`* &A��ЀM�d.�ŒbO��8`�bئX��eH e��	��	����2�Xc6A� ��&��h`^!��j��m��!7��u�%�N�BZ��^nY4m�`>�w�? 6H"e�ATm���o�A�>�%h�%$�U"��#��� @�a���������1��Q.z���(���+^A��Cd�J������0@���ŤZ�_b!��dD(�F�H �vB �8�lG O"�	jZ��g�#v	� 	�Ji������n�;P�SJ�,�Ʋ��+�E����ʣ<��i�D%g .a>����'�xN��!��&�!?h� �&ơ����OGHP��y����ޠ+����А�rA�co��#y��f�R�d���8��b�?���$��8Aj[���B��kքMN��7f��(��?`}�dO&��u�tPIaJTi@  @���R�V�u	�+j��=�i��AXBִI�B�:J&`�b�A��(�_�c�h�lHò���兴J%b�kb��fXc�)�bd@a��b����"���-Ș�l+��ws�v��ZSU#4%�� y,j-�1Z�6
�`�~��� ��k $�D?�I�7	�f��nh�َH�h  @ ���  �y&��UD�HT"���ޫkL�'��k2�H!.A��=b �6@�a��L1�G��o�aot�	p�Ƣ�ao�aolȭh���f.9�� 8`tb	,~�-�	�qG�ј��US�%�8a����?��H �7W���%Wi:�1��J
�F@>�k�ظ��mp�Q���k~ ��� �+�Z��N��jC�b�v�'�CV��o�n���F �� ��.:�z�L���#&"D�h($�hB��$t�)�6��D��I���Hz$,\v��:�"�wd����]�����a6G��1~���D�,lMb1�2��F�k%@F��\>�!ۚ5"�������,�B��+~���I <{��: ���'.豿"�"H;ܵ�B�%~�(�@ �hS�E��7V�v�`Aԡ�Ex�D��ա����X�<-6[W�Ԋf�$` ^���*,�xS%����������/��H@H��ˮ3j�n���L�4(��C�A��;F�$�r�mA	�U�*`���� ������ �A>@����ĳ@�h�P9&$H(d��‍*CY࡮b����h5b�%
<8[�0� �pTza�*&�!=�h]|U9B�>��+\v����yT"k�����+��y�Ix�<@&�j�ea��n���h�	�F��Y@ lg��?H�E�Ol�lY<!�Vi���t9Z��m��~@L<�u�½ҐY�jJ�H���� ��&�p�*( .�f���=��4D�!`1&
�aC%�����í��h/=�����!$ ��G��3�l쎙����25�8=�Vn��Ҽ!��J\ah�1��2Z���΄���V> ��P�I�`
&z`5�`2���W:a��1�#[Ͼ	:%�i� ơ	�Y�ǂ�_�2�B�0�Z���2
r◁�a �>�'6��$��16��afa��ʛ(L�n�h�o���!na�v~�x�����g� z����狹/,�+zW�I����� �L�za�*�a��0޳9�VF�����kCÝ`O����A�7#D��]8�F8@Ad� �C:�=*��j�-TB��<`_��L h��%^�|��37�C?����G�b�y'Z�h1�<v�`I�5+ܸ���%w�4p�nɼ��\�r�Xr��Sƞ@96�!�A�r%ʵ���e���R�*�2eM�[��PSc�"{�,סD��r���'�{�$Tp�A&X�h�|���Wq���&Y��t��X�$@Pp���C�flqG�,g�f�v&�'[��:98  �    ��ϫK����^�#8�ie���^�z��p1��ĝ��2�ƶ�B��ֲq���C���#3B��wZ��{��2f�e:|�ޯ7.����ܲ9�䔐{;u�^{;�"�̐�!�X�YYe�
V_�s݆d�N��r�Y� ��z@�C�d 4�D,��rK(5bbXu\��%����8@�dF6@�4��?p��H���������  �� A]̑s�A����.���r���-���.��b�10�SN/����!{��� ��w���8�\�,Xb95�H#N:�4�nꌓL��Vq��R�2���n��S�j9�B�M�ȮL9�8�0ה-PE�ˉ>��Y�xU��r4O�Q"��3`��@��h�����-��r�-�вDc�d4@�t��i�Q�LS%hXv��j( �
���kN>,)�B2�`",��I��qH+��i-�4G�2t��0P1�WA��"�9g&���A�H�Z���S8���=҄��/��:�0�4ᜣK-�c�:��Z����H�� �6�K��([V.�8�l.�`VV�*r'�R{P��e��N0�J��J��\�
%�`���8PAؔ'Фe\�Cu(ш4�3ڕ�f��@ 4 �!wd�8
�DJ��F"��%�A�t��F��bN9u�8��",�Lc}����_B��Wוf�.���4�j�<���.uԡ��ƨSN:�%�O�n�wE��Xer@�CbW�hT�A�e���`[St!:�-m�R�!�&D�g<�G��5#= ���..�'L�bw���w	X	���� !y%8��@��$�A�"�N&r�8 �I�,��4K�Z�\�� � =CU^[С
y��+�qc�F!�h9±i,�/dL�4�A�u�I<��YΖ�	c���@А�}���t�%Z�
=�aV��.樐[�C��E:Ա?,�*("T4^@��!�@"*�+��P�F.�@�EL�g8Sz�4�BqB)��?�4s#����� I��/�hE%��
�q�.:*B�9�0�oLbLF�%ā���ſ� 4��8 06�I�;���:f���!�(�-f�<t!�(G��Ql�O�8��lAAr�c�H,���s��Q	�Gg�3�?�G-�0��(~qh���P�:���XV"���K\���(��H�~X��_�-+��ʅ�:��B�B��Z��	x�K}Ң���pB�,8��Jԁt�i܀� �g����9 �bL~@D%,A4��Ts���   � �R���:�4��4P�ʅ�=��p�/j�
]���H�4�1�Ip"*�J�Q*s�C-��VF쑒�Ѵ��ͧVr9�A*+�I�1���C'��!�؈
H � �Tx!ed5+
<Β��H0)�@� ��lc�I�N���(� �,��.�O������D"E����7�X&l�;��y�J5
0���b, v�`#K�;�NŁA���(�I����H��91�-n��idc�P�+�Wv D<3�4�ⳉ\
�����r�"�pEV˅C���Q;�!�|�#h��UR{"����~��4� ;^-e�1��ȡ�\<O�n
~��_�	�T���ֶ�e.x ��b_h��+�j��( �9�t �5�#P㰄P�� 3ME&��0� X ��v�
���<⠄t�[��|\ /�Y��i۬������(�Іs-#'�P���S���,V����
9�AV�Ø�}]��?��rDv#�U��[�F͉yL��(�E�5�c��pq6]^����	�Ѐ� �"j�H#<�7D�(�s\<�/e�nEVӀ�պ]��C4�ړ�W�#N-1@�>@���[�D��9��� <h�")cc�bXÊ�9f7'�A��E�J�!(�8��3vH��������;8�y�w�1�[�w��G=��=�uӝ�[���y��N�������
Dt���:��Y����A�9:�@ 6M�|E���2�������)^�"��6���^�'*��#�����aD�4Aa"E:�9%��{��4aBo����6�!e��|�)� *V]�@I�$ذ	t� ��x� 1'z��� Ő����� l�f�tp'/sfg��y�g�T�`^��	����tp�s'�rf�1n����Wp�C�U�+��0M�~�"��|�ʷD� H�:R��H��M�@	��	E /O/�Rt���XDDD�1S'l�&9��c�|�,Z
�:"� Zn ��� [�)���p�QA� � �#Ձ��]�A\��e�g�A4�1'� �00q�Љ�!���Z�6�I>�p�PJ˄'eq�@x@1mK�NA�<��
�TV�hgsxP!d�O(` V�xz ��$#9��B�q(� $@�	�	�@9$krXNL2C�j@D�Ƈ5(��r��|�dc�����p�p	�xuWx�-�0A�q8AA�0�0���}�0T�('q\<���@� '�
�@�F�3S�`Qy�v�pA3�@�ag��),���p���|�ag\ɕ�0K��A�JM0ApGpc�K���T���UQh� M�P�x|��D/�PtO�	��#�ƁaCq�t�&/�CXn/O���c�	|�؇j�	30;PJ�9��
Q�(�g xރ�����s*�
�@��!|�
�a@n�]~���0
ːU�u�)�R6���g<����e$����*1u�41�Q�(�����`����3Ҡ����Y\� ���@m;����Ch�@V��g�w�A]�Rit�` A�p��,�|�  jsQka�w,�-�qv�
I� �a�E2�����D	U"�7I��	��BJ�2�)�y's6��72Wu �P*�U?h	p��q$��0�@�0� �4����?�@j��ēQѓ�9� �a�Ұ�@\���Q���<%��
q����8l6Oa� M��U�&_p�%t���rK�A��֍rt �Q� � �!��XW70�!	��	�}�@�:8���i$"z��$�J�qE	�e~�ϐ�Q�p�&q��ax�8�0	�0	�0h�g_ a�u�(A�P�j`y�CW�E1��ĕ�xRZ�L�������b��B�0� z$4�xy�5��x��b�!*�H��(��pǡ\�07X��P���@� �%�U��ˡUv���h͂��=� �1� �9^ᦢg�F2j���A%�5��9� 	� 2z dK�tE�j��Y~�a��R��Ӑ��
�l�(��߅��@	��qi�Kd�x��Gxet���HI��)��������і~������|�'�p��G��)V����+�H�7g��V�(��uR(1&��|CˀK�!�u�q!` M� =K�x�˛�z�[8���A�A�	�R��7���4jv`�� L C4
Kf	6Y0�P�0R�  �sJ�o�N�c~:�D��`�1�`7����T�T4��#�p	[�dpXp{_`ª�Wm����
� ֓� ��o4\+�Lȧ���v'��n7�����
cjy�0a�(� *!�(q3(�0�����,e���$�A�R"i"�`�0Dp =L_�,�!��^LX�#��r�M�	ʵ�@���0� �\OL�~1R�w3Dh@PkOT�F��I���K��0��Ȋ�b=�4��
o7Gi��6� :��0Â (��*�_���Wx%[�
�0w!������G�@\�pe1��=�Tlq�g�"K,� �Ҁ��	��p��v�P9ᕁHv���B�,A�JC���]w)Y��U�ʫ�a�fi��L4"˻�ňt�8�1�0���l��@A���q�$F��#EDwq@	�p	np�h�7&/h�}�D�I�$����{H�b���̷ZvQ��'�T�8�S{^5d���)�u�D����@'R�`v�`����;A�K0uy�,�s��� �G�����
����Pk��]��7��m�9*����3�̀P��B	�� ����� �[�g0�Aˠ@�UA��%�9ER;� Q�N�
X�T����� �0`ɒV�1mPA��
Vkh��l9������Kp~΀%鐭�P0�P��F� S	^֣4� �,�^�t��*Hd�����0
E��/�6�]�E�z���PSyE�� �l�(���"QT�)� �����
xGY[�(�{�� )��U���� .�5� �=9a�L)�q�SAx��� @��p0��P�
BI�,.k^� �SN�.	�ͱ�G`B�b���1~��N`I+��P� 
�ԡ�(��<�b=�`#%x`c@��,�Y ��-����txF���H;`0����a*Ȑ��@W�Q�	� ����@���L�6#�G�3���p��3�0�OQ��J� C��2n�#pip�=¼D�� _���0��b.NC@
N�K�-ln���F���s	$MP�N�WtR��G��Э��:�j���"�h���@�0	닅;c��p_�KYP{)�J�|?:� � a	�����f--��=��$��4������Y
��� B����^�H�ѕ��gZ7���0�Q����@��r	�C�N N�	N Z��[ {>GZ�'��@��~Yҋ�N��р[iot��WN� /��5�|���up�~`Xɽ�I� P �c��9j)L]����P��f3i���
,��k~�W$�x��<P����2�dz$J�6v3�y� 1�q�M�[�0���#Q21"��?�rM�t>��`Q��0h��dx� o���Aa���FQ��� ~�	��A�C 2�Zp��@�@${\�p #�o  �b��1��h��<}��5|�C�2�8Q_�	<P� A�,(�@��Vu^	���4H����
vJ��!��%q��qb��Ra��K�n�\/YŌ��
��8x��ݻ�O�=xS1ݚ�n޸t�fݺ%�U2gд9��:˪�r&L�3i�����2c�������=�O���wO��q�f�\�^�nq���]��I�5�W5���9�'�;�^}{�
�kX�`��Ky\����C���e��	�m�!C�D	� �GA�4�H�	�w�� ~�5@0 �B6)��`� ��4:�2�rf���S�>�` ��pC/���K8Y%8 �M�i�F��5��1a�R��%J�j`	f��9��ؙ�7���DYԒ*�i`�%�[Z�c�6�C/7nIg_��2�^Z�Y0��:ЈC��ƹ�����3����c�G���+i�Ei��f�y�y(!�jT�~���q�QG�q���ѫ����:�đh q"�F�p"y�}�L�@��@(�$	0 I�
�� H@Yb� �VM����=�ѧ�Y%�@�[�J��4�)1LZ�$���%Q'�[��x�	���1�%�ggd��qĩ��.�K`�S�j�9,�pΑ��6x��A�e�in��gf��`��Ϲt2���f�@���Gc�F�A;�F�C�-��K,I�����K���L�NP��G��e�F� b�h<q�<'� �Y������Fh@:
��V!H`N����"p@0�a�g3�g�R�)�(�Ȟn��A��$
�:��0�K&q�����D�vj������_�}�idFg�.k�����i^	��ܥbg�م9�����[�Jg�\>�WƏ�s�a��I�<�y4S��sL�Li��=����͙G6&6{Ѓ���p(U)I!'"����0u�y��P�x�8�!'�;N��)� �4/p@HP�l`�`�i���:!�#�h P@N0p�!��B ��"�Z���m"R-o�	p�G��-z�$H�2Ē:�� > ��hq\	8	OHP���a_3Z��x�)c�B��d�"��X
,Ƙ�1΢�lܣ��z1JN����3v��q�"����.z�I�� 5�S�I��@�Xg>����LOư������Ԙ�R��(lU�iP[`��㴇��8�+�� ae�pD� ����S ��d RBN���ŀH�1�q%:�`p[5 � �#`�FD#"��H�lx�"��$$ �PpӡA�K`ъW��q���<8& % pi�K�{�8d���#P@*+���{#�(�4�#��"z��&�Ǚi�����T/`	?���8�	)��ed��
�v���G֡�2�Q�[�c�`_7h�c:R�:�8`�B��H��Z�Q���Dh�#�0(HA���@�0$ �x�(��
< p���Z�"�3�h�0�&��	�lD�0�6"պ�m�h��i��b5$�����@ Of��-��'H�P��^��J�`Ǔ��$'�l ��4��{��㿨A0����p���r|���D$Z#�td�z���429=MVceU����­���a��>
57�8
��		2��)���:1R�J&Y�*��BӐ�H,�A��P���N#<q�(����������
B�@�(<�
qxt��l6���3[
8��!�p$,�!��E���ңјE�G���>A����p�m�C+҅�J\bC�(�NлNv��9�_�����U ��U��276�-&�jH�e���Ek�Ca���E's�\�b��'��G�qE�˔�S���h�����4����}�M�e�gI5R�X Y����+�8N����,����
��PL�QPE�9��6�7��lv�D ��`���	M�����f����8��/�G�)Z� �#�XE5��� ���(@�#���wѕ.��!u��$�� �GAz��8(��8��1�!���C�l��=�;�PD"�`R�S��q�b�ؠ^�L�_��h��I
N��G�6�9�@��� !#����}G��!$�ԣ�u���m}DF���Rղ�Q�`���r��U<��Q�DۂP��� ��M�(�&��I^`�!���i�f���04h ��b,��h�&ʚ����Ȇ3⠆�>ѭI}>��@���DA���i�h��#|�Hi�~�8,�鼫=�cp6�P��z}Pc�4�`�q�pX�ix��ؤLB0ِ��Y�P�Lr�[0L�q�����*�&���	�ˍj��;��S����<Ja�h2�Ly�[j� c �b�?x��xm����!h�!C�h�0��x �c�����p�h�)�� ��Wq�~�C �8[@��8ch�Q��9+ k�lȆFX�آ�b�1T 
��	(��GS��ѐ@�6h����O�?���/ʁ&p�鴃�Sq��a�|�P�L�ˆ���i0;���ꡞ]Ȇؠ�����Q �]`j��eH�l�e�>�����Y����6ʸ��"c�A4�����i��q������8h�hT� ��F�������	  C���-k�?@j "���jN��9���`�8�\�\�8���l D'x�-ډ�z��<	X�H�A�I�J`4�It�A`%������(� 5ش��yЍ��/�<K�/L��Uä���]��Lr0�@����R���0�R���;�����y 噱�Y�U�)��b��n�A�:�hJ@��)#<���N�� �7m��'�-�'J��j�� 8���&��mx�X��!@T0'?�C�����,j��D�cǑ�B���(�6Ң��� :�Ā	����Lx�H�Ip�:��4�N�����H�J
��)�"<���:�0$��E
�X��P�
B�Pr�l�Kz���q��.q�[�g���5CY������tt�����Ay��y������㪌�(L�x�{P�\��G��{(�u�&��&p�,�n��3)�L.���T ~;���2i��N �W���� N��(� X�'p��H�W�Ð�3�z����l��L�B�ڢ�����	��V��F��K@��NJx;mAh�8pax/g��� $T�K��t�At�h�N�@A����S�;��*܅{���N�TiE!ux657��ƈ؆��L1�s0����+	��(���������,Eyձh�OYQq��&X���O����&h�������Ъ
X��ɀ0�
��t�O(Wq�F=?0�x����[s�8}-s�?tz���zp�.��#�c� ((�H��H����P��I@Np�v�L�]0�g�*j�?a0'x/~�4��)��
�3��p�X�X�EK҄�脐�@MڅY���I�'X��\�������K6A�t��q�Z^��(يLq�09m�p��FX��J� E��Z��q8' �O��&h�j�V�� � 0	H� x'�[2*\�O�G��нp?  I��s�!p��8��Zp�8���qˢ�kx�_�!�"ϲ�zGp&pSȄ @F{����V�*4���
�I�Q ���$ihG�Nx�?[�<�������P��7�= '��[�Y��$ gЗVZP2�s�p�(�Pb����8�5(��p۸4�{�P�2����8��Wd�Y?���������FЇz0f[�y`i��P|��Fp1�H�My�P����m�)p�)4�
 ����V���m�Nx��%'�U�?p�N0���N � 2h�����Z,c�W�;]ˀG��h����{�q� !p&xO��F`$�IA�A@���K��3R���}���@UK?�' Dũ �jx�A��%v�Q�R �P��8��8����W!��u�Phi�P�& �X�� ����M�����&����O���r�BJ �chx��8`'؆mp�9[������5�p���v�'��z
�j�be�,|����n��!��gq�� g�!N�F8����aGh�U�!H�k@�'v�<\�ex7�n�=���"o0 ���
@������'0�F�F@5���i5��H (M�M0����*�rR��m����
ghj qa��jpjp$H�N`���'�gx�Y �T>��i3#ȭ�P���uL�N��B����砠�a���U���Yh�``�I?`?؆O��FH(}��F(a��?`�8�j���phka؅F���r���&ɶWr��&��Z��@m y&{�h�Fp�`�O�g� Ux�n �� v�Tx}�Ok� ��xV ���8`�׎�

Dʆz�'�"��	�ViS0o=x�V)�:h��#=��P�O0�g0Y���@��P~^���эC�ʓ�{��y�a���v�(@�� ��Eg���}i��{�P�$$p���<a�B�� �akM>���zP��������Y8Y�qh�``�nj����M�=x���x@���l�_��yp����Y�������s�����gI)
����� �NP�0� ����&PPXU�fQx�y��N�g13�����e`&���*Jo��.�1��
�
xR���'�F &��:%��H�6�=l����E6٥�)�_�HԯA7���y�/jq�
�N�=����Pr��}GMЄFЄ]x�͖�T������i���M�v�+VaNV}P���hB7q@ge�e�x�xs�mP�U�f�G��T��`ȇQ؍PN�)?��Q��8�j�>������b�G��'sr-0�NP��^�Qp�]����
=�o�m�y' �7��&�y݁C�8�I��h(tz�} XG� ����~ .L!ݵ� 0�'x�җN�I�M�tG�o��J�o�Y_
R���h�:��Pâ���Y�HI�N��𝉉[d^�� {��8+zpp�Fʘ�sgO�-,��ղ	E7���gf��y�Y�u��b�l���a�(p��GP��hx82��g��
(�(� (��tX�q�z'��ܖh�h�mV�=pt�,����Ѝzz�6oZDHSt�-Q0�����w��P���=p�F��ԑ]��W˥(ЕmYY�@VoDVv��@� M=���(R�f9#%�Y�Cg�v5*T�Y�B�X-ćҤ���n�{�����%L�,Y�c�n�Mv�l�Ǯ縝ժ��Wt�=��mcW��S��捣��Sc2]���r��j�Ɓ�i�Z�!Ml4���ޣ C��#F�1^����j�p��^Di�aNQ�O�GQ�8�0��&~��^f�4iq���5�͞=z��e�fʉ"�P!(O�l�*n�x&H��3nʓ)M�M���aH�!
k���J}���%�,z���ٜ�o�{��,W�<Y_����j�4I�;L���N=N�D<:��;�P��6�=�t3O=�@U�=CA�!mT	U7����k&�PٌC��"'�p��6�<�'� ��#~�d���ad�5y����Ǒ�42e�l3%����'P
iL1�,&i�3R��t�L7�DS�3qzJ#C�YND�	)�<��'����(�J��rˑ�I��u�()����B	��P(A�&���ϧ��:*���z*���j�/��)��~w�{.��);��������OW��SO>��������{,�S`=8�a77U��PيEv�n�+��;.���kW��u��/�U=�~�=�ч�}7m��S/n[�Hb'�3�|0�	+�0�i��Q�ԋ��F)�R�3�m���+��pۑ0�5|2�)��r�����T��>u�� ��L�&��3�A=t��*3��iX�S�>�m��=5�U[�0��՚^�:u��!�L��e�}6ڢf�=�Ԍ/;��$w�O��m�y�w�0�T����v�$5߉�+�����ފ�4x�OS���3�9���������u��枫�:�/�
����Md�^�����;�;�^��×�M/6=��w��;�͛m�ѱ�7:Ut�����k�=մ�����?N��B���4�I�/>U�:�4����Slq��ܛU��-��`�; Ʌ���?�9����<�0�� ��b�yT(xj��(����&<�w@7��ŏt� Q(C��{�Mj״��.�3�!_'�4�>:d� �5�n���N<`֤׿��/������-��u�I��gē�-[�����5��{[C��FGF��Oyjd#wg��8��k!K�Ĩ屐��x����Q��4�Kh�I��{{���g�Av����\�h%�E�1_�{�����O�Rq���"��S���i%.�?�,Rx��a�`x�\�j{����H�!�¬`1�iL�(2���Q9�NJ��S��d��S^ώ��&:UŲ��h��W69A3�\E�n6�m@��G'db
����hMc�n�$]� EL�1�Y̝s��=�̞$Q
�	�t����{�1
e�eyi�f&��m�(	��*Z;}l#�1yY�jſ��'���RI0Iz�~���K�шOuB��g5��<�ʄ�p�6H���8#���#�ѧjdc(�\�XIj�j��Հħ�� ]�f��؊~�Q��P�m*;:�|�T���TU���u���(�
jT��)�2Nl� �D�0׈y<���p	HA�ϒz��Ԁ�(BE+JHb�����p�M
cCb(j��YJ=rK��`γb�(�!�T��f�rFg١�|��� �0\�ո�C���g'�����ĒXۚU+�̤B�!a�y��"�U�:0_[իv�$�NY�R��N�+^6y�3z���pʸ`�=�\x��$ 	����
fZ��;G\����6N�+yy�"
n�L%ro,�R!����/mrb�qs�N&�1��L��O�ڼ�C���-�%b�<�,EwDY�\>�(�,�^N9�9[R#y�9��<Z���q8�%�n`t�3A�L�)S�tp�v*2Ǝ$3��;|��;j���E���Dc]��
���G	-��	%ݻJ�Ƽ?�G=:!a�<&��S�i��y��Y$=DGK>,Ҫ�x���D�f���@:��4��`�4���OtcтF�V�gd����)��g<�ڟ�x�]��\{�t�Ƨ@��R��k���|�]{��Ff�h\e��#���p����#�+H�C�(�<�!�ՖT�X-L@
^�6��h��KD��j��� �C����%��~$^��04jj��x�6
j���@�9�����!�3T�,��b�V���
�jh�e�Z��ؒN�Ӏ|��O��WB�u`������H_,�i�SX�ŵ|Yf�Z%�(K6,*��4�)&})ɲ�����},�MT<b.�~?5v�2zdU��-�A�B'5��<�4��-c���������i�J<9�}/���'ڑ3l��Y�5�]�:<*��n��wxm|�����8���'?:A�f��o>4gEv�Dÿ�QbWM�� ՞���G���WQ�����"L��IN�4ߏ=��N��}����^���N��S��n��`X������M5��K8Cn�>dCBY�u���]�BUC���M�G��^&�N-�qXͦ]��9�*�3H4`�<=�S6��>�[�B��ST��?�\��K�4�B=0I�><�dL�mJ�����vL��܄X��Y��K�B4HJx=C�I�Dfu�3�!�5�6�;�D�!v�X9�K���	�'�U�D��0��uu>�_�D�Ƥ;�� BRJ�!�x�Hux��OA�2�c���U�{x�WQ��AÈԶ�T�5�V4��<�1@�gɋʙ5tB��-T�MQ�Y�ȉ!O$�`0���<E�X�6dC�є��ˇ0M��CS�s9���>�ta7\�S@�M�
�a�P���� |�I�C=�#�qT>��X�`k�^��E�Hߊ�TL��$�t >��d�ѹ�`�EN��!I�C:�U��E镌��$E���L���TL��%MI֣��0ne+Ջ�P%K� Z��,�Y��Gא���Y>e\�QB}Q�d qe��e+�d���A��&+�E=bR�*�qc��ž!�I(f��e&�ե ��ڧ)�dP���M��&]�*�Rh�L�D(����?�B�P#�f��_��~������fm��9X�xA����L^��$�U4�MR\��T#�l�`K��Ⱥ�`�����Q�g6Qs��q̓*D]7�c��4��I�å\[Y�gDΕK�)<�*�H��KAB7��yB;:�)�Υ�'��NS�	1�|�L>D�kq�'��*��(T(8�6��*̂#�\�zH5��84�D=��=tH��l9CR���WX� �'��ˋ�_�9刞�����?�]���-�CEE)F���(��WK<[5DCD.�țwԝW~%<E�ӌP^��i*LW�����P����N���q�^�i��i�äK�zE�p��� j�y�0�B�:��L*����C�����#Y�q)DY�*�H꠺�H�O< g�DpBU}*�j��2�VH�H����i��j-LY���QvX&�Z����P���N��eΫ^k���Y�]�L�*��L���)�9_��仞P�*�����X�2�K�Ĭ	���V�pf<m`^l�pHL�h��?ɩ��K�)E�M��i��L�B,�0��1�� �ꍠ
�W��ĬɤP���=��ի�����,�<�>4�aVV4�UN��uDKi��D\��}�i���G���Q�g�,Ϛ�=�TI]�>P��uB#��|��#l���j��W}���&���"x�������,e��vm�tB7tBX�Hf�W��Hv	nMy�ʇ����y�Ճ�H�ĬfZ �ѷ�������w�� ���˳D���K�M����������Y=
�S�i�RMGL���ӱ�v�̸��d� S�ejg�3)Ͻ6��6�Y�T٘���������:9N8���뒕ɐn�r������$m�oi͠���z뭮���$��䰦�q �0� ,�� ���ie�7���8��dj�n���S��+b:!*��Ȓ0��)�dkC��W���r������썚��
3o�p������A��t�ô��E+�P/n!���=�fn"i��DE^�1Եe���H��nV��v�C*@þ��S���DC6K���T��.�-��o��'@��1�u���Z����'8"��T��=���*�'@��UW�B�>�*pB7��u�螾������R�,h����}B=44@K1$n5ےct�C$�D4�2X��}ΡKP�}�V*XF�l�'�.�#7�)T
zZz|��r��Xq-��6(�^��[����.s�2/�;K���(�EF^^����Ks��X����>��.����r�� ���̚�&�@O_)lB����XTf�x*D�M�ڔ��'�Lέ>��r���sH��D�jY��Q�o1�v�K덄I��A��s�m�����وjL��I�2C7�Q#5ژ��d�2�R��¯T��@�U0����$Ww5Fo]��0���5Z��m�����\Ϣ�d\w�
�&l��Yw�݆�Do.��A�Uޟ��/vu5�*@U�x��vk�)���z��=��g�Dp�+Q'����BT��Gd���w��7w�fnM�\K'�KD�{��܊�\S�^�.�T��Va$x�ᛈ(��St���}ޠ���͝��Ν>�b��ҕ�}�(2>��KɊƊh�C=� ���>�T��Aq�HuCZ7i	�T�H:��&?2�>)�s{�CI��唔ة�)�mVi�2�V���(����4��i���@Շ<T��'��DV�K��*���a̽)����!�ò9d=8sgYƪX�RA�h���i�^�����K�.�2���K����
L⮚��h�)�\Y�&y������D'��I��_���,�(�����F�K�yٰ�����k�d�M��9��M;��	[�� �z4\�� 6��� =��Y���M�#ӡ[��C��Du������{��P&s�:�G�"q����a���z�������r�b��z�Hp2��/I&�9v�{��t,aiW/ ;�_��5�z3�O�z�����I*|K�[?��'�C��PYL�އ�x��{��K�ȋ�Gc��*��z{��KOS*E�c�B�R��|����<��1 io{q�.�7���κ����F�d�R��3��;��e��[W_���Sf�|�c��𤺘����X:��/����@��ڙ��Z�αֺ����flzԋZ
�����:=� �Ø_�/G?�R�<�+��D�I�B�{Nc=��k-&�B�R��M^�=ۏ��q�6����r��;}a���76��;Z�Wq�d8�y�
)@>Q\9ޛ��Iϼ�)/�0z�;*���KL�@L��AL��6�#8�䃠2Ю`>��������_�9׃��<8��9��G>�@�d�mD:��;�U��d<��4��O	$(T��/���4Ȼ�
�ӿ��<D-��*��D�ZCk̃A*��*4� ɎpB#�E��0� @��4�ӧQ�8�����>RA�8��P� �(�[�o"G��@�Ɏݸm㪱�V�Kj/�9��LXMaq�Թ�gO�?�:�hQ�Gq�k���&���k�j����s��>��Ed(,����2���I�!�Z%�Q>z��}�8Q�Hv�ƕ��re˗/�%Nl�+RǏ!G�<���jC3\夞�m;��8U�� ������6}��^��hm�Q/>}�S﫰!t;�廷/��*Q6,M5ŋmV�>�zu��/�nJ��F_u���uh���*�(��&��ȱI�|����W�"p�|��+��V*���{�1Ԯk��!��Ovr&?j�I���$�F>�DP@!%���z�	?�p"�F ��!Rb���y>I��<��#z�#����l%�cn&�j�E���R�)y�fhv�f�(x�Gܶz!"�2��	�f|��F�x�F�T���:kE}Fi� u�� C%��
|���dR�I*m��/%Æ�i"��t�S6R���OHڣ�!4�)UF{:y�-H>Y��F&�����qz�$\�")�k)A�d��c�Mֲ���'e+v�!ŅY�[�ӆ��*4�8j$� �b' j¬�p�q/�x�5$��3�@j�L�I�2S���lbxb�\��BMR�H%#g�hd��0@X4P>d�Ƒ�jġ ��H>�t$�T"��C,6��a��Q��HE�NHq�P�	M� :	ng�x����"x�G� O��O�hdhH!�U@�ȉ&�a\}��©�Q�9g��]u��d�5�a��A����Aj������[�R�&
U�`Ӊ!�O'��Z�F����&�\s�����]ȷN�2"��p��B��m���W�VF0����[��)35#�ʇN��Q�~6��ѱ�R����o�1#pII����g!�^o[���U�%���&��oJS�R~���m��%�d���Q׽�'>���^�����>�-�:�a�>�����kE�F�76"4h�(�CKGJ&�劄󊝯Tv/���-�L5�@Rp����>��GXU��D�փU<��x�N�b�Ye���\�<�0����ZV7n����*@9�q"
��*ꡔy�c���ر��`�'I�HT��'�Є)	�
��A'w]4�!{2�Ft"
�p�**�G,dM~x�Wq�jt�E�ПN�ѐP�E-{Y�K8���$�
��e1DΒ�i�Ŷ!�O<�,NXE=
2�zdRR�F=r-P�H��=�����mo�!��(EՒ��\�RP�Epy�1��av#�z�Y舖ڄ�Y(o�L\��8�#�WKu��ݤ C�'#�Ņ#�C�F��5 ��#�揜	>�00�)6��~�����'�����&�u	��xE���(���@���=�ݤN�ɯ���(-%G��Q nT��Ց�BǧU��2���n�k�!Eg��S�'4	@a�*ЪmmP7B��^/#_�I�엖h`Dh��^n���1Տ��bZ���2�.�:����$���Q�~��|�$�5��2�K��C�{�h�H唅�umeW't�C�gZ:�	/ѕ��=rVJ�$�#�I�yB��k�k�XA������s�F���tIQP�s� 8�
vt�i���#B����5�pF�^3
H:c���3�щGh�|)�F�OW�9��It!��4�&��F>��wx$��Ǯ�3a���!qh���`4"�n�G58��Gn�E>8A�O���@4<f�R#խ>9�zV�O=� c�2�F��ul;��j$�9�W��^[�9�i�/6��^>��N�! ����tVbMR���܅/c�݀�{ӹ)j�Uv4Nڌad����&��bT�;o���,��|�I~p�6�3
���¨VE>z�N��Ϥpc�b��u�Ol#lM��Mb=�����jĦ���S�IU�-������(�:���kYaǗ;���ix ���'���|p&��h�D;A���W#kdY%X��*:Q11������b"��F>�Q��Q#��4�,�|�b���]�Fv���q��/\R��!��/F�$�m��pY#o��k�5�X6t�p�%~�{(�2���^gts���qLmsY��Y4z����Q���"���d�6���w��Ψ�Ҫ.���S�52��%i��eB�2�}����&|}�M�tsB ;GIF89a��  "" *#30.W(W2u17W$+i"*v(8j(:x3<h3<v>B;8DS,Ck+Cx6Ci6Ew<SxM4F85yz%s13F=DA>mv=BZIKD;pRlJ9oi<PMLDGgCJvKVjHUxTCgUK}VYgUYz^aWWeyjUQd\hofWhffifxo{{sjgsmrwskywx<�;�&<�#=�2;�8=�#<�C�C�C�F�)D�%F�)Q�4G�3J�:Q�7R�'J�+Q�(R�2L�4S�7T�A=�CJ�CL�GU�FW�TM�U[�UZ�GZ�W]�Mc�Nb�Lp�Xf�We�Vv�Hc�Jf�Yg�Wk�]q�[s�c_�pu�ku�Zs�mx�~�|y��x��w��}��'�46�)�16�;A�;B�^�A<�n�s1�D<�JJ�jY�wq�HH�Zb�lZ�wk�%�05�59�:D�;A�A<�C=�KK�\b�c[�kn�LK�}����|��|���*����1��/��W��v��P��u��U��o˨ϰ2��'Ô|ԻR̶p��~�D�����.����-��U��t��N��j��^�����������������������������������������������͌�ᙦΙ�砛Ӡ�멩Ģ�ש�ǩ�ױ�ı�޹�ǹ�լ������ι�������Ǘ�̝�˪�ɷ�䚐쭘縫½��ɑ�ø�ϐ�϶�������������������������������������������������������������������������������������������������   !�   � !�MBPW����ؾʸ���������ٸ�����٬��ٸ��ٱ��ڸ�ڷ��ڥڥڥ���ۥ���jXXXXXXjXXXXXWXXWWXWQXWWXXXWWEEEWEEEWEEEWQE?E?EEE??��>>>�������������ۚ����������v����v�33!�����ܒ�����ys�����ysmml���2�22222�2���,,,&,&,,�+�,��%��   � �������������������� � ,    � � O X�@(P"aC	!"�a - l�be���\B����3mΰiòe�[n|��u+W�^�r3fL�1cΜ-����p�ܡ�:u�ڹ;z��i�Ǖ뽯`���'����hӢ�Ƕ-�~�����O�\s��맗/ܿq�nK7.\����k��ŏ?�L��=��1��̹s�͠�qz�h͢3{�l:��װS�^�Z6��~?�&]9�hͥUW֭�o�'�^����ֲ�����9q�|��@b	�)F��B�C	H$�AB�H(QH!���:0p�`B{�t A4`��E�@�!����$L �!HA	�0	'�`�	#�P��&�`�
&���
.� �]��Eg|��<��#,;�!����q�,��$.G��d.I�TS/7���WbiL/=���2A�	8�3R��8lRu�9S�OW]�SUXx��^s�Eן���`���Xc�N�a��e�d�:�ho�RjYu�7j�Y��n�1Gi���fr�Z����az��F$1��A�J4��D�JDD���í��,��py�A�D8dk�I�7DD�7�yI$��z�V�жC qĮ�i{��24�~��gDC�'�yt�w�IP�A)TAP���3� D:� B1@ �D�p���+�a��L'Lp� ��r	!�,�]�hs8�|�_��c�C�-F-�MOڤ4���%��l��3����X#e5���ϛ`��=����_�-�;�ܳ�W���O܋-j���Y�����	��߀�
�k�I�*�F�+J4����İ?p�����A(�y�J��8�7���7���A�D��!���˞8D.�;��츷.yE\�y��ߪ��/����������_�D��&��:�Jk�@@K���8�o��̮?DxC��C�[;DC���`���倃0" ��Ԟ�H����{D��G!� E,XAf���\6�)��&�B�p/�G8��N�!�HC����$)%�&���Ӵ��^��K`r��x�6��*]��(����Ns�G>�h�>��O�)La��A	�PdD���C�mR�A��z�+_	�s�������qksFhB�@��%�u�S���8_�/~�I�?�$#�WI�*w<$%O�9�O��s���(�"��ryt����9��W�b^,��G�u��e�x��b�u��$�	��5n���0��jJ�Ys�y0"�ΛE��-��pa[���.��*]�ԟ��C�w�k~G��Vxx�?��k^�A����0��A���ǂ=H/Hъn�%�`Vv�
��'��<����Z��R�B�(�2DÐnx��4JN:Z.jbb#N1��$���p����q�N�GT��tS��L�bWJQҏ݄���������?�Rq�Z$Z���K��u��[2,����^Me4UY��岚�Dl�Z��"��x�#�W%W9�*�s[5�'��-a��4�3E�H��5����(�9T�q����i�F:A	��`O��e
֯��ծ� �kJ�z�Dޭ�	��.WZF�V��='`{ۣ�vu+����BB��;^kŋ~�yg��'�~�K<9�A���ǡ��T��B� R��	!�Av��� � D-���|�U<VˎBȁ��U!V�C�c_�G�`;�R���5�@h���
:⭏v�s%e1y���2��1���M ����X��-����[2�׻�&�]5�_%�L�^���,�0Q�2��qg��!E[�2��ʵ�6�U�͹sK`�FY������W�����Ouu�fn!=2���mm!���hN�ҷe���\mW���bU# k��,u����Q�{�̖��K�pQ���Ζ>�5��^ґy���p��4n��ܰP��:;��f�N�,Z�8�@X�落U�� ��1���m����$���"��܊����9m*����]5[ۺa��/w]��Uj�w8�6���Q���o��,�Wdl�u��\�$��f4�Y�w�l*�q�{�����C�۴>��|<�a]���AW��yFKiC��y�u\1�<i�������p9������
����s��.YMA\빬$�:X\�kyI���^��Iw�ݬ��+ЊZ��p�<�[	v?�r����q>066��an�u9�7���(�$���-JOz�s�C�b���A+ł,n���ǯݚor�V�G����ߵ���c���t�1޾�w�so���j>��v+]9�t��֯��s4��j�5���]�)��K�����|��&xr�����%�e@�1޿�7)]wp�ߴ��c����J��J�J��Yy4=�SHG'gm�WsVZS�8��x�9SVI�s!� � pHwd0`� � �w/
� n���� <@�SI�sHQW��.@p ��g�4pE�nt�f�'I��&,�&>��]^�G��uUp �M��Mt�U�D-�Ca�ӆ~�8�wtw�x�{��qWV���:��pʄ:��r˴}�c���d?�d0�;�+�xaRV�'q��=I�X�{���%q�#L��[�#~���������E���Y?�p��ght��H�sfJ��U�9j�I��VG`kp�"`	" �XC@- ��+<� -�.@��-�+Ѝ�#���80'� cX-p!#@�� P� 7p Bv>�S����sH>�9�39�S>�"-�w;@+��w}LGn�&,1�G2Fn�"I��Wطq�'�T�GXe:�UY�{�7���%�T\ڷ���H%6J��e�Ȇ@�ev��SH�6lǕ��L�Z��L�Cf؀�4�b8���[�#:��;&�8>�e6�J�v��MД��d���t��F���
��
��
t�# ܠ
�
��
ڠ$�6�� 
t�
��W� Q��@� 
�)
����@�0��+0���p���	��	�`��p	��	�
)hh��Vc�R>�z�nه�mhX��UI��79gs&�IA�e�(nwhZ�����9�u�����pe�U�(Y������-'JE�;�U?�S/� u�~�b�JC7��viLY3�fy�b�Wxay��䠻�]�4���v��v�[�(��
��� 
{I#�	���
�p�	� ���
2����`�(�
�p�w�
�� 4� ��
���w�1�	@��	�� �$J>�J�dH����9��<�RM��:Q7�V6-u5G�ly����x�e$Fq�ْԩ���y���^�Yk�"i��D9>79�o�Z_�K{�Hi�o�4b{ZL�#+�/^p����@��`w;���u�3h�(i6'f��u�v��~M��y
ׅ��u�M�Jc�RGƵ��G+o��-�� 1���
̚
��%� p	�����
� �����
���=@��P��`
�j
<�
��
Ͱ����	�p��z�;�	0�`	�p	���>�M8p �W>�F��G-v7?�TO��=��M��+��OPM)V,��;��>��kum#�I�R�k)�t���u
Xز$�y�u}T6�Tv�x�r���jMMFb�F�W��f��$<�Fh��F��h0:p=3�~����9X�Si�H���G
:�
9�J\ĥ}�y\~Lܢv�b���+���z��� : ���֚
����
�Э���B�͐��࡝@	��	��	���;:�/0��p���
� 1���-p �chPTP�I�I� b [p�!Y� T �K^0VV� Q�Q@�� R��0�b�UN�F� � @��`w�:� wgwJ���O�W�x��W���.�rćq0K��
�3[[Jef���U(y�e����/ˋ_�y�{��2� g�(�����q�赹�t����8`��;Xy[f�$X�us��uƚGhJv��d�ģW�4jp������!�� �����	�ڬ��`n ��
��
;�	� 	y�	| -0���/깞���0 �HQ-������������ h��p����������������p� �S �C�� �P���h� S � մ�P��b����@PW�� c@G�q�#���sV�X���c>�|�,��G��*Y�vG�����c�ˋv�Z��c��l!7�*ǳo%rM0/P�R7l�g��,��*f�pY?�O�3>�R?�O�ϒ8H�]�b.|u.��G�tоr;J�<��	�d��+�h���ŋ��9m��^F��;���
{iP��b,�c,	� �3#@	���k
��	� 
60ۢF� �j�b�	��� �m@`����u�f0�p�{��Vp��h@��\����Pc1� S��E`wx��{�g0]]h� V�� %� l\�u,��\�а�0Q�WՋ��U,�tYe���{�|�G������K��|d�V�r5ɳ�W���q�9�Y[�gV�C+��h���g �"t�4L�� oM	Q� � �a0��
Ã�0�@` p �a��0�0�{���0����.��k��/�^�%�f+�R��B�dU{M��KG��=KaC����z�����
+M��
0F���P��
���6 ��B��kӦ�	�`�jԬ �w�dg��9���p�l�h� m��<��`gp|�`S;]}h�ϠP<8`����@c���Jm�F����h�lA�˾��p� ���xH[�dy�քt�y���˞�Ibȿ=XL׳���J탾s����e�#�HH��d6</g�kth����Y�de�dʰ��C��M�SAT;�$/�$���FRSF�+�#��)�3�#h�BgCq#[P��#�������1!S�eQS
P�Ӕ	m=k�pS�4��6����	������
�0��R��G���0���2� ;� /z�x��P�(�	 ��I�l��1��'1���#]�\��`�P��%`pu|�qN�GP���%0p�c�� S��O��<����}��$` ���@@ ��׆,&�z޲���'�>�/J��Y�!�a��;y���UĖH�w�o�b��l�F��
3@h��q[��ydJ��Fj�����f����jq�P��_����������Q�R��NG�TB�DIe�@�p���Q],-T �mXp�#�^��#+�>Ɯ`���
k@ C�<�0*ݬ��	x �/`�*Я
��
�
�`���p�����
!��pU��Ő�pl0+���`F`g! ��3���rF��3"��mH�%G�[�)����Ɓ_���񧏌��1c)��2fn0+��X�K��D(P%Dp��A�HѡF�>�#�9����3�R�B��
5�S!J�b
6�Z�B���kV�\-�U�V�Mw�K�ɐ6�
F�k��ԇf��r����bq�=���\���{��]���С�~�Z�lڏ�M1"�"I��V�4�_:x8��
�r�D����&��SM��zsU0&��2B�vT�Rq�T)��KQ�p��*sT�X��O�S�V� 蠊� �	&8an���3��G�xJ;���B�y�#
+��Gt���
}�9A`�������6�9Q�6(8C����B�\Fx�E
�	�J��0f�v
�� �BK����ꯡ�+����J/���1�B+���R��4�z�,����R����+ϥ�ҫ.%���+����!�)�4�n  ����#����#0S�b��s�)H5rcm4{VSm�������ǟ^b}l��h]5W�T���}D+h�(8���8�
x3jP�t�ց�3����.�Ra��he>�R��� ���3/W\1�U6)]��3EL0��q=A�VDqa�t؁����ny��i�4 �gX.��/��Ё)\����X��$��B#�@�}Y����H�����9q����������88�}�9�ˬ�
�٠����B�0�R*/<�D�L��X��4��k���rJ�:´̲��*L�	�nM��Fgp,%(�`�R�2s��t�%����]�y��f\�e�f^�Ş^l��}�e�^����^��EV[��ݟl�)��G�&[v�ǟz�E����jf�'Pq�54~R��n��5�9��I���"e�l�6���5o�p�^q���T�v>SRY� ���G\�1E���
Kl �)��	K�`!��| ƮV���;��`�c�0JႢP�g$��:�@�)����K�q�6� ��	"�3 fF �/Np�x�b9b�J�1��� bFJ��.,@|	i 	�T�*�CJٶV6*��nB��t:���u˙��6>�ɏu����AݥYz��V��AJ[��gRu�K� �8l�0f�O)�1 �_� v��\8l��̙��G�jq	Yl������Y؂1�+^��q�����?vыY���#G/b�
r��<�9q�{����E��G�T�C1#�E${1�I-�	�J���,����X�(B��P�b*0B&�m� ��*x1dbm�*X�
m4c#xA5�c�� �jE+6��B�8�+<�� ��6Za���ЁtpP�9�/z�7�W�ǉ�Іm��a�t��t��5j���SHDR����1��0�8Cr��,��X@	�Q=�I=��B��,z�K��A��F�<��vsJe�Hȧ��L�RS���%*U
*�dR�Tሉ+}l#I���g�H�g��QxE�P���<.3�8�=�p�^��u���6rQ�Z��v� �3m)s����f/�y�Z�㖸�/nP�~����Xe�bp�]�t� �;�y�j�s6�
G.��5t&{�hA�@"4�|IH'ۈ�3a
�
T`��@:��N�����/�A�tp� 2����0LA�.K\b�.�AJ`��W�X�-f��  ���h1�3q�\ �����4x�
<��І�m	LƂ�P�$� 	mp��[�!�SpC|4�A�t�1�"����3f0#h�������/����y���������B�4�iB�m+�&T��2��©�]�� ��%�Q���m#����faF�f�L�t@��OJд�t��dC   @�����6��i��$�XD���
b6\b��<�^������*����܃���=F�U�"s�h�^i�bңҵol�1�W���M.r���]!�1�8h
��P �J9B��� !@���99�1��f������R@|���s��D`K(����@%@��&Ѐ `#K��	=�$p� �D`�:	8�����E
�(�#� �F�)�wpd9[� �D�ɍp���C8� |>CA�8C���{p��A
�:�Q� f#��H4�0H�
H` �i�u^/��R��5��!X9���FYW��.h�G�Ƣ�o{#u^�h'���3����H���f0��=�^�ƉҶ� "!rA.8��.r�Ǉ��AoW���;���qH=�t��]�^y[��ϛ�, �Z�
�768�t���b��Hp���s��p`'�A�0�k�k#�8�ɋ3�M�8h1��⁗ف@�3x�!  (��<����C;и��� �%���$� @�,#�hXB;1�н���('p��X�'0�$0����8�0J�! >O�
h���BK1�$�	 l{;$�C��S;P�AɊM��,!���ӛ��:2,�b
X뚸�,:�=:�D3���3��Ô�8���S̞Wh��Ї-��O�-�` ���~�à�_y�}о\T�`�^T{P�~��lʞP�l{�8�����v�$O*�
4$;i
�c�H7@�_pP���#8�#P�4`�h�#@�,�&Ё�)c٪��X��8�4@DlK0�5�������꫚��4��"�+\��R���>O�%��	�$�0��$Y��h����R�ʋ?
+Ķt���´h�)�M��&�I��� JΣ¬��γ���R��:�
G�I��X����ɋ9ZI�J��ؚ���` ��Ud��9h����"�	����E^<@`F�+�$a���g��}8Ƃ��j
Fedy� @A�?2�!0` /��t@#@�,( ��P )�f������f��_�2���AChG$B70` ��I3*(I�(��Ļ�`�r��ѣ���2�¯s���20�����|D:\�ea����" �&������6��>��%�0�6��"�?�#�s=q�M��6Tʽ8ʢ1I�����.�
>��y�D�#���������ˬ����xEy�H6qJ ��ȓ�T������H��}�hT������K�$�q�Ft�bqOB�ǡ>H�_�6���l@�l�3_)��lM_�c�aP"yG,@�R_p� �#0ǢJ5����4`&��6H�,P�Բ>�74(#�3(7 �4���`�08 �C��3f��+(��̭�3)P��4�2' �.c�1X�9�<�pH���",�x��{A����
� ���}I[ۼ�5��)��E����
��C,�P�Į!�N������j�ݓC�ˤ�J��j��}8G:���
'0�`�[��^ȅz�c0�[�e�0�{�W|Xcp��%X`�%X�up�qX؆��spg�w�sP�q��x�X���IÐ�M�$)�ذ�\9�	q�{ȇ�"f8��Ob�e�!(IN�0 3�_0a��%a�3e�6�`�*�@��`�GP���_8S�pGfX*8�,��(`�((�Z@�_��_p�����+�34�K��)PS_��`�M�a�1P��03_(fx�M�[P4`�_�*�N��Z1��3Z<�8S_���p�!HIc�
���4:)I5���ԋ8=�@C�ہ8ʣ��K�8���� ��ǜ'R�J����[�
�#@B�^���<5�tJ����>�P�\4�
cK�8*XI!�6�U�y�"t����]���9/� �� �}�����������	X�)(�+
����+8��X��3�+����
����
�X�`6��[�Wy=X�3`�\`��/�K9� ��W�J�7H��)[f( 7���M �] f��r,��=�4=_���t�_�s 4 �60�,H�t���-�(���3��a0_( ��"�= P���⛀)V �HS@�36�G@�G��GH��&�_���Z�Uc_hb+@�`�@�
)5��/�hI�5�V�4(
`�OM��Ѓ�Sƒ�����1��A=0���#��B�(�PV;���ѻ
�	�@	��sʿ�,��^��Jd�6�I�i�~���/����K������b�$�ސ��H<�ت���K�KL<8g#��������M#�9$r���9#�9�;��p9���@h�`�;�N��9�s�� ��
� �[ 8�Y�V�^p�7�E�(P�0FZ1 7�Ҽ5�uPS4��`@3&�b
(�+�&s�D[�=��ڠ�S&� G#�*pM8�`��Je��4*�[ �3e2�)�*�b&��(�5T"a�� �3��,*�D6C�Q��`���,^4��lØlC�� ��˲ĸ��0���
��#1	,��k@��p������
]���<(l�_��J
+����e������zƉ��K�f�Jei��+$�#�>���4�a��x2���A�C��6��a�����������A.Dnd1����0.Dtn�9N2�"����F���$��1101��1` )�oȂ)�ǲb��*�%�2zv�u�Pm�q5��P�rTS�P�``���41��,��ƭo
',09���_�������cG�`�'��g�b"�Z��+�����1���Fr�ʚ@|�����°��2�R�O:�[f�\�K�#�]��I�
^Ğ�SV��<�ǌKgn����]h�D	�\�h�S7ws�2�&��t@��y������>�G7$�hO�hI������Ǳ�����\�4#P_���NT��@C�
��hC y�wb�∙!H��e�!�\? �1 <_W�!PMZ/.0��v4�v�L<3��vP�707貝�'�s�\��P�[p���t�M@U�D����4�xI���ȏS�=�=Q�]P���k)wt4	���&MJf�PfD���*�>�S�k
����Q�ö���	�P/���1�ϲ`��C���v��Iw4ʸ��r����yH��n��nJ�+� ���w]6zm��IWII��n����L�!3
4����/D2��M��
ʫC��eIu�+�G2��N���+YG`���)�9���),hǽ��-����>��:���V�ܓ��/��0�R,��H�������"$��ݦ���[M|����Q7gm~�R5��%�6��(��cĐ��
����m�w�+1ޗԓB4�U
vmV��˧�|�����ʈn� ɠ0�g�~m�L�y&U��t�'�6�oJ���b���-����d�m���R�Ŷl�vD��(8d�D
t"p!�#Dv@�!P�":$hBE"A$i� ��aʀ�81$D�%!*�)�f�8���S�N%#K�T�$�5{���iɜ#��Ir�Ǜ3�Դ��+خ"���Z�Ɏ��e[vlٟ�"�+�&Gx0��0f�|��2��#X&:��3�R�M����ȥs}��y�С��~�*tiҤ@5�\�$f΅K�p&��5眴!R����m�/�X%�e_?\��ǂ)�����\G�:��Xk��٤!��R���ty�!�N$�hVK|Z~qvJ�8��M1U5V��$Sle�7�Mi��W9-H���5�z;�$bL0-RW���Y2U�QZ�)hq҅t��-��D���:0���с0�TZM�-)W��U�P��f�m���4��cM��Q��)��u�E)\o�qo��&c�*��9��#ݔUWi	��Q/qE�CᅃR��DA���gS�H�k��Р`�9bNX:w�E*��H���OO�V�{����c���[4�V՟�x�Dy8Vr��{��FD���5Ӯ�F7��`�\�q9��y:�[l�}ۦ\�A��u9LijDM8��G;<�i�5�P_%Zen��a�������=�$p�IDv[R�gm4�l���$�mK�zDp���E1��V�i����⅌��!�һ>"z��~<�G�&����Hks�jd�R<���~Jt�1N�^��klJP�܃1!4PJC�5["r��Y�DF�U�%v
D&��YlG<����K����虇R�hvA鄤Sr�Z��E�������e��^����n9��MCA隄�}\�f��Յ&�@��1J��B.i�KvD;1��G?�=DD��ǔ�D����!�c�0���WKڿ�C}Xܫ^GԤC�$ý���C�=��{u�}���sP*�8�<�2d�(;�Q�ץ��%k��Ky�S���'�"ɢVC�-$9jZ��,c����S�`��(aj3?�WLt����Z�j���R��Ԋ9��\�C-�7sɖ�z#	�`"�H�&
IKd����,� [<@��� (@��_���5z���Hb�!�qD�#� ��Q<8B� ��?��D0�j��7���v�2<�R"�<�$B$�$)"b��I2"���D)�!�le�V$�2Yb*P�� �FC���e|i���c��N�\���,�]�Tߚ೸(�F)���t:��?��y:����-o{��J����)2�H�9��6���kfΥ'�dے|�g���@�A�qfc��(�1�cHT���h.r�\\����-��6���%EIѠ�3��b8�`J/д�5݂p:���t
T8���D�
u��N��D
P�/NE*� ,^��T5����,�����x$8zQ�X5�T�T$76q�p��$�%˳]�G2Q�X�K��7�����ӛ�����u�x���#>N*�~��b�!,F#�_4��L���2D��|0�iF�<��W��1āo4x��q[%+5�?u9�T�c����cK4�D(��Hq$\�;�!��Rw��Їu��]l��ޕnw�������ny�[^|\W���<�_y�#���}�Q�w�C���HG:�`h ��0(��Pf0��FD#\ъ~�0�p|AR���mP)JU�a/��b@��@b����T��^�S* 	S@B[s<�:�1�F�#�( 둎T<H��c����F:� e0F��`�b��8�*.���lb"����̷�1�b0�ȧ�!y�dۡ9"I��'S��O�Y/>�b=�X��J8�\s��A�jb1���Z��d�yQ�j�I����ǧ��{�CԦ��yO��R���eu�E�So����uum��[�Z��ŵ����j׻��{�+d�C����u�W��:�=���Wݎ�3�aPq��>�3����hE9:a�jT�$)��]ґ��(F��=ӚN��;��p#�Q� ��)0��B�q[�� D�/Cx���D#�qH��INr7b���Ĉ�]>g$�y��#�9-�@�]恜�<�w%Iz���}>X�B�iN�G��jPS��6��c����Pou����O��.��]�갋�����G���Ͻ�.��Q�������w���v/{�^��7��/|�K_����0桑`?����`�b��n��GO�ӓ��Jp��T�[�������FH�ql$��M�q��=R��<����|#Aьbv�Aܮj�k=�q�:��~�g]�Z�u>��}隟�fG��Ż������?�{Sm��]�v׿t���ퟫ���  ���]��u%[ὗ>����W��~���u�����}`��38��B��AԄaTE�BG�TH}T<��tɃx��{���y�;�vI� VW���=�_ݙn]�a]��6���F!��]<D�^!f��> yU�u���Ń��C|1���>�A=�?��-(�2�B/�����u�Z�	` 6!��W��2�C=�>(C/ԡt�B!�_6��=D� �����N�^b^a&~��ubZb)f!*�")�"+�_(�>8�;��2�:�2�u!C/����.bt��2��&.b!:b=hb6���2�C=�Ct��1����x��"��;C6�,xZt���%�-�C��>�1��?�?����&�Z8<���2��/��t��������%r� nWA�ک�C(d+N��"EΝ@�������-,�?�;,�;��-d�/J9#2 c8܂;�C8��L�A6D>�::B6�J��6�-$�(RO*�-�#�,H�t��- �&�A1�?�!1�C8x�-�"��C/�$;��K�O��J��2�B/,#,(�Q~����98��?�%>�>�CN�#LcM�C6D#;��3��I�4�B��b��2�$>��#K~�;4�
�-(f �]J��+a���ƚ)^�kr?c��Z;�_t�CkF�+nd�I�#��-�:�B=Ԣd��M�d$�:4�0�B68�2�?d�2���$%�2�1,�J�Cp�!?t��U��ق"�![��"�?�B8�t�B9��#�dwfC2�'p��tš!��2�M���[��;������-�C!�$>ȀMzgI':x%p�',� �2̢"
%qJ�p��{��2 �~�dt��b��"f���_/��/�e/�dZ:"_��uڡt�C/��<<���3(�8�e1�?��`.CqΤ;Ħ;�g�k��?������t��;,C~�f���2�1� f,�h��$����y�_/d�nv�=t�#��x*",��e��,�"a�"1��; p.���#�b���o�u�6�aBf���e/�/�Ah�e/|�&��$�vf:��2|g�>�[`H�$vr�2L�?8�-�2�g�?ʟx)����$w>��"-��;X(�OZ�-�2��!�B2TgJ�;|
;��'n&b8�UV����;,�. �M�C &,�C*O�(?�`��!:L'p"�;Lj�~�2�%�nj2p�-�$_v����=�e24�2��;,�2��2�b/�!fC7fC/������F%�]_�,uY�9���Z'�����2@�0B+�Υ��&'��9�$.*��΢:�d;'> 1�/����!8#;���t�iƞ�ګ ��.��}��&���S�at݃}���ڢdN�Jz$U�"��W�d8�@^����;�`~JbuჄj�x6#�
'-�".��j*��j.�i8`�W�]�.�0��-����$2�k�N�-�BL~)i
����1�!�j�x�¤�C`jb.������?��Z�p2*��A�փJ��j�5v�Ǫmwfktyc-~h1��/�1�7~cJ�"7n'���%�.m�.���|�"����^��#\�@j��iֽ��#����;@?��2r&;؂j";,#?��������d9 d""�{��1B�2 �Χ\�����}�6*����9�BV
�d��a����nu����䏞M�%2̢;�e6�GJc~*"?�"�]8��S�%:��ɢdpz�Kc-����x�(-�K�%,�%�.�"��>Vj1D/>L'֦d�Gbn;�g~>oSV1�j�k*s�+uV�,h-¡x"C�����.2�&Jv%�B�u��A�B�gƶ�����C8���1-�E�)��n�2aF�ǲ�"��y��A�=
���&h����br9�����!�#1�C9%1n��2�K�8�|�:(C3��#�x�C�:���I�$+�B9��#�Z61�ev*1�C.�/<,-Z��"⢲��:��!_�=����9�.:�J�����3>P)yB�7(���-�CL���qum����ڤ�n�R��B#-�r�i�E4�����Hg�N��A�fٱ�?� �qWt�C/��G���`;��U꣨�4P?a�f�:)�)b���2<�7_r=��V7#Z:u^>�Gf8h�c
��*Jn��i�"��(,�"0�$��C9��03�U�]nv�.a�Y5b�&Et��t���H'fc��]*�D���4.�2i"�M�>����$���t��2�q��q����=�>�nccj{���=��m7��vo���)w�%p?�>�Zs��>wmB7�b7v�c��!oaf��CR]xe�)�4u�zkok2�x7!g��/C��wv?�=B�<Fw�z��"5C�=��q'wr�`�#x�/�qxq�D�Zu�Κ�����w 
��Q8~�C?�8�Gw/W]�Q�{���Ϧn��U����&V�x%de6�rxg7���~�g�r7���i��r�;8��C;�T�C�ߠ��v�/��ݠ�o9��&�_��������B]�#�v���s��uױ}�H�Z'�8k�x+�xc���4��9�;!�y7��7�K7��4'��.0�+���ڄK��Ez�C�ccfy�z�_z���|�%˷HOW��qwzU;�z����:��:���Kv����wv�!�S.�;B�v���vR���vm�8��������/;�K���z������!i/�6�y*�������Q�7��:�	w��=�C���280�C�=P�>L���#8P�=`f^a=P�t=c$�C2�g#�>�C��]�6���ƛ��p@�0��]?�=Իɗ|?�C�K�=�C��|������>�xe{7��xw�;����'b=�C��<��/��t9C��d� 1:�m��v��4�|/k�*�#��#"dB&>pp=�i�,�F�1��e���4O�>`9T�]�X�bю�1Z"�bǛ8�=�C��<5P=�Cſ�=lC7X|7PC7L~5t�˗|�üԭ��������[�t����(j>y��o>��/1;'V�i�f��~*���UC5��H�T��!M*�3�x?t�ST�d1N�]��/�a>��7G3b�g�f1���^&�WC;C�d4���Χ:�C=���0���j&�w=��d�%_B�8�@�w�_A~����Wp�n��a����5j�ғ������>���o_?j	�맒��}
�U�ֲ߶j]�,鯟?z��ٳǳ`Q�G�&U��iS�O�����[V�.��jק������W�e��k��l۶��˦k��^���s�-۹r����������U����`|�ꅋ�r^w���r��]�����[�����������2X��&�tQ���-SF�ٻ��zcM���ʲQ�ٙ;p�{ݺ�W��\��ˆ-[�}�͔�MZEi�n���O�I{�fV{/��P���m�ح��}ãg���Q,"�4���n��j��ϭ	)��)��A�[�!�]>�*)ƌr�w�R.r��Q˟{�*�t����!��pl�s�b�B��*'4�y�����ڊ�6����s���p�)�z3*,p���Dwʡ,9���r����:g7��Q�p�	���B�3 3����yΝx�kNe�9�2u:3�M9�1��
S5u�1�|��L��8î�l��FL2U�&�iΓH"����Z)B�<�b�j���F"��y���Qϣn�C��ee��_����
?̰a:���]����T�
�4oa�t�U��pn�f�^��
t޹E�|���Q�{���U.����	4/��}�5,y!M��
����^ć�[H;rr(,�1m���D+HK�h�Y����(~<jKW���G�qǙ�ˊG��9�*i�s|�yL�s
G�����^�4hz\*Ȝ��[lAl葍m���?_���n
tȞ�Je+��"��A��������q�o1��«nъX��EZ���(Z҄�d��%���bt�şw`�gb�]9�d�YF�[�\*�]ld��	�7�����Ce�	Gwؒ���M��ЉΝ]�瑖lv��rع��oq�������t����"��~}����ef3�J�(�(�或P7������IVB��n@��O�F����?���	O����`�-l������?���|�^Z�Vnw�w#z�S�-<3"��#^[�1v�īd���P�&V���p�2�a;�M�;��.��x�3�
Oh¦��~�B�	֕�0f�p����;Fˎ$��2H�p%���#!)�^8B�@��-�~�h���)�AD�T,�QɒU2����2v1)�좏H	-�A�E1:��aAΐ|8g����?|��p�PՋ)�c*�xG/ر m�{�@gdg��4
�h�6�a�F�c�8Oy�#�
���%-�����>���͇A�H�&4�D! ��
��	�1��E-�Q̙��&W�B�[�,e!�V��1Y�I4���81Ѩ�F吘�R�zP�;�xGr��].
�7)u3�'OӍ?�IM͐�d=j�T8Ӛ�Hs)7x������7XCYъֱ��7��.�6x���*.�W�����E,lQ�����Ea{X�v���/p���3��,�0������Fg�q�Ђc�)L�)O{���X�=�i�xhɟ���� �ϝ��%""HO��'�i��ŕRg�U��U7E"i[���0%�?������	J>�i ��x�C�$�yՋFU��lg����j����l/lљ���Hlba� �����\�V����gxk���5Lx�p�X/���խV0\��U;X��p��ڇW��o���/��Yؕ4��#l1��&V������"D��.z��] ���f��'WC�|�9�dSp`�~/9��Ԟ\�R=�������5�=h&@����P�R�b\o�)�����Q��#J&�QAC�Ш�'��lW��.�cKd��9�qs��`V'zA}��f��5uf����(�H��.`�X��خ��Eh��G�`u��������A�����حn�[�}l�@���Y�J�	�az%p`eqX�ʢ�����_����o.�ˋ���]��lg�['��
L<�1f{fZK��� 4�TK���?�?zEЀ�\D-�q�[�;<�3D>���d!nf�>_�%~h�E�v9���1��5�rͷ��^��C�����-�Y��B޶��.���J�k��`6�����W����~իئ�����5ľ�A��:ײ� �j�*�Ůb��X�	Vq�� X�6]���/�ӽcö{�H�l}}ދ'7��G5��^,o���8�<�J�H�\bsmw��k���7}$���ψ϶��m�h�;:ӞT<)��P�0<�*�HMo$�K�Hlr��=�G}��:�o0�@�c��]U�
�x�_�C[�O��!{��Wپ����n��/1���^Lp�#���ǌ�9;
ZToFP�R���µ�a΁��K�����Ɲ���Ф�����|��Vm�|LՖ�l�h��fa��b̰į�Ď���l�΀��oy�}����I��"���І�GJ.H���A m�Nn�Ld���
�0�p�����0�pɰ���0�pٰ���0�p����0�p����1q	��1q��!1%q)�-�115q9��=�A1Eq��H�Q1�L�(VQ�R�a1eq{�3
�l��zṔb��[�)x��{� �� ��f�q)��y-F���1+���� `2� �Eg̡������tf@�x��,JB/ @g�q%qn�F� ��� l!F���z!!�a�����H�l ��^��(� ��b;���a��$�F m� �!�%�.�� n�F��(΁ F`  �bZ�'�� �>�'�c�)KR �2�%��{�~�!G�@r�2
  �Jv�  � ~� b��� @�qKz�r�;��20	'	��!�� ~�-|�s�'~�'�� @�¡2���b�� ��j�
����*35��1��%��!Ѳ ����F m���6{�x� ��4w�m���.�A(3mT�9�b@�T�al��� ��F�U.�!!�f�!�w�8�q*�%���:�3�<��'Y��"h�d����6�@5)�tA�>��A'�AQ ���� ~QB�����)h>��.��,h*�����ԭ�L@A�pB2T��(n�+j�5r`�������mȈ��d�x�?�����k�d4�12#��D:c�t�Bb���ظ����^��������H�x�3�b�")�GO�$
���l^!�^!����N��4,�+����.�)�21K "�m��-uS/��175 6f6`SM�q6u�.zrf�rQ�0G#�ǋ�@��PM�NP�jaJ���G����(b    ��h.o 2��m^!2�22�d"5 %U�3-i `)7 )j�V��&���B�!Ǟ�BxN]�)�R)ā ����T�G�;��.�q�f��&��)��'�b\���H�f�]y��D$d)�{a�"�Bԋ�
�.Y��f�(���A�}�� `� Z� �`��[�i�a�Bh����ӵ�lL��(��Aj3H(���A�c!14����c�PX�B޼�>��� �RR]��ft n '��'�T��(���[}�G�1Ǵ&��Ls~1+��&���ar��"��A�cj'�"V���$WBWr��ABt��;�^��XWVR��q�r��4O� �QTϲ��.��(.�,M5x	7h	�4�.'�q�(�VsEarA�t�azg"Z��TW(V�z��tEazA�z��|�Vd���)x�X`��Kn1 #uRk� ��"i�,�R�;J5�H*v���M����uq%"�{�W�ar��qV�����{5�|��BX��t5x�w0m�}��}�V O�j��T�V+�h3x����(�! �KYoXg �l�h. B6��Ar���argr#��"s�ŉ%W2����!����1WpF��Ӂ�W6~yF��^�|�H��h�u^GK�V�K}���.��)i]!���6�cS���k���q �"����"�B�[ܘu��'�(b@ �gq�X����	 Q���!�r�3�Y��Pm�t��)s��xAX	�X"% j%������@7��U{����!u�4���p�7~z! psFF@ ���'�v�E5Rc�g���0G��t ���X��HB�Co?�p�|BFt�;�l�a�����z鐮�ym��5�)�5T)v�/z���YG�h�"����7�tJ���`3�!j�~���$���H�rQ�{Z)��~�Х�"��(��D�z1�؂-��m���������A@��k�A�(ș�c���gQH��z�Qe������'��sFϰ$ �;����@H���ȁ��'(±��~�@��#d���{b���X�;�i'�pJ|�~W��lfa^��D)�/xf��qI��x����~,9d�! ���������)�b �b��a��i�{A8�A[�g�D@�Ò%;tGj�ے��!�B���!?�ax�`��[��O�u���́μ�z� h�(��,m�-�&E�-�!�b�2 �(jAR����a .aI�Ua��jebk*�T$���F��N��`�L_�k h����EBj7���BMԄ�*�  �2���,�źk������|�1Y�����[�3��u��!2�,��/�l �����[�zA��ov!Sρ �v�[	 fp�~)u?���}�6� �ӡja`�b!�7 8z������nz����2�I5�Ay�B5f3�aI���"a\$/�.&F�0��CD��j�M����:��/2��Y�%?�\��aD��{j�A����z�)��,Y?�l�x��u���KoJ�vZvz�b#�m���2 h 0�KwXi��	`(�x� G`�]b.���z����x�Q��Ʉd�(>�P��:&9&f3�ad9҄/�!2��\ʆd��jd�j��P�O�$M"�3N��` �������ǜ�n>� ֘����}�){A��3��� �׏|������B��aa�l�bZ2 tQbis�A��U�& ]1��qS�';��Z�wyc@�\%c`I�!6�;�tZG`����a����?B�6\�9�#,��
56b��|-M&^-v3x$/rD2�	3�B��ExNe�EF]*��f�!` b\����s^�u��k��`�o�<��{�s]&���|������l�t!בLA��hN������Ѐ���=r(���6x�A �W�`��gi-9_� #�_�����g��6 ����s����! {1l��>�#��7���{�K3�̙4kڼY��~<��N��{�����ϝQ�B�D�&>�N�P(��8�z�j���Y����֭�j�t�(�L�����HW/6����O>..Qӕ��%]�u1f�+��L�yU�V-s�n�ș3g��={��E��:�j�QY2q��$����������� ��`޻@���W x�0��? z%��\涆�蝳E��?r���!��s1X��O8�#��K����p߸�8#��Z�v�RL=�� S^�SUUNH�����G-��C[�H�[{���`%H)�8�%��q	�~��0�_1��.�`V�6݀6Z�F�V�O.��W��s�#� �-0� @l�T�C�L��4@�?#�T�A}Ty�p	�P�l��D� �c�C��>@7�  �b�p�= p+� ��x4����d��%hO7�(��9�8��W�x��R\&t�N�Pji���3�.9��!�G���ч����.t0�#����>f�CN5���K/>��/�Y/e�COiIJ瓒��[i9# N5���i*P�Aʨ���<��Dń��WEeN 8�70��A�0tE �SO�7�p�+5��O \P.���9a)���rB
'ĭ��&�K8��=�ĦL/��jq�-�ܢ?�L��D5O�\�RdN���눾���[�������dȪi���m9�UC�hxr;�6��v�QS���=p���J �b��DOJ�@�`�rU�d�u�(��%>= �L���x�槿�WWO?�|yO�I�t�sΣ�מC*�=��vCZJ"SN6����;5O�3;��;�4�,>����F�:�;:����`xu ��u����� k-�x�A��c7���O�l��!)Ǖ P:0j�#�.��dH��lC� l�4hB�4��Y`�	�Ev���� ���RNҨ�����,FP|�������� X�����K
=�All#o5S�2�Q�l(��@�R���]��2dP�#�U/��<��Ll�c ����l{�C����;h`��c� $2!���-�W!q�q\�h��š��-δ���b��^���gw7�bs�I;9�ph�s '�X���H��Ġ��$tB 9��`� 7IK�#SQ�O�q��r\E��z��C��(�2�����h���y��E�o���F�:�lw���6����<p�H��cR�lhj���4�D(Z�I��Q/;�.,��}8�r>yEAD׸(Šg�M�D�W���y+�LG@��b� M�C`qH�~E�䉦8���8�zO;IVt* i��)�9�4�	�B���ϰB� �"�@�}�E<y hAr�tM=�'T��$� M�7��-�3	j]��׃�D��sL�a����V��ᾲ�u�z �!��8�a1�E>(ԋGJv��%�@�#��:���&��F���AN]�P �ڪf��@�V�"�<DX5��i���E-C�����ɡz��/���CNs4��ϝFu����^���v�[�v�\��
��7�z�/�� �jУ�	Uͳ*�IuQ�Sg2�
�c���m���X�lfC�8rv~\�g���R��F�gBQ�N�1�7F,���#@�U�����1s}��>dc��F��5�K���X�Lt�^�$���H`c��כ�c�����0Y؈���	i��QsZ�t�>	8�qNePڤ�� D=�m>٠^9�'bЊ�"i|��؞`����c��5q�C�f���<�~����� qR��k/��{p���x]�t�s��=9l=�s�cRQy]8F�d,�z�G�8Wj����O��Aα��O� r�S�t�w�&�TVZ��+��e�Y=�abr3�6q7��p��(�t�9�|�r�a����[�9����Ek�ZL��5?�ޛu�sgM�Y@���Nm���Y�pֽ�5)���Ա�S=N�s�ڢ}9:����(Oz�Ah�rl��؆-^�r����F���p�$+S�Aa�N�'Oְէ�1�{�*6� �UVu�xF6�ո���p��W���/~)��K��{җt���Q�	���Q��u����U׻�G�N�ˏHm&�Y��I�Eim��e=��]�7nqp\H�-P5�aQ�!�����m������8�ō�p����e��}�BY�<r�iO����@�5�O�l��֞�>��j{�c=���z�1�~d��h�o�z�������%����=�Q�f��
�E�Dև|,cw��	^�{as��O��Od6�5v	
g �f��rW*G;� f����q� ��`oc1�6�}�qr�ul*wU��:����,�}��4�rj����~� k�r,%IF{��� �����{�%p�Pu p�&�sP@�W� �r`�Bf�5��f&2�mR(��g��v�z�!�#.�'�2a�� 2*#�,t��	R`S�2����Gs�v��W��`�bXq@�@|Vf �U�;1�-��}��eF�z6bwXH׆��� ��H�7�Ȉ�(5�+�P��h��X���z0l�V���D�����t�tU4006�h��`��|�s�0n� �u'�a�@>����xyxW�_��61X�6�F��e��1��T��Հ\iҨ��e�&�hxd� �f��
|0��p6�
,�,�1}p110 �����`�8I@�x��wOB�Ww�`&1�(���0a"'1)�1k��;g�:�f2
 �%��%��>1m�`^
�9#�dsE;�� i��QG@����U�p��8��{���ҘeU�~0�{/1e���0�rhՠ6`#6r�1��D6p02�p	69 ,`��xk�
�)tx�Pm���p�`?���@@�|�!��0��T�
��ɩ��|ޡ֑Z6 ��LЂ'����s� ΁(<�����}��r�PF�;�D�y� �r� �p�wЦ�����VƐ���}�*������v���^�{���H�1E��U�0a��P�0���|����}��)y��#�)���u�-Ô��|]�@�D��g��,�S� �pR���疓��y�p�
��(��y�Xw�-���� � ���L�wj@u��H�'����̇�rAA�H�{�PA'��e��*���SS5ՠ��P��_��\_���GH���� �2�p@ P��j#6@}@/@�5`�#37��#���J*������P�`�|-@��E�H'��p��qRש�ɩr�����@;j�����Fj%�w�D1O
m��as�UQ�	7p�*�	&1WQ��w�X�'�8k������Z)�ʘZ8�z����h�+��Hi��l�V!� �_A"�������p�2S1��c�~!�5B�J�:`��^[��b�肋��qWA��ao�7ɹ��	�@k��K�4a��;�%������A����Qr/Xj�*Wwt�(=�d�yq�P�W9�0`[�k+/wkQ�r
�d� } �U���h˸�8���������Z}]q ��#P��� ���k��K&��.Ы/p��	4S}p�q14 c[>��X���;�ev�`Ca��f� :�[��;�6'qP�w���ei���s'������'3G;��i�+\���������˾^(�]|��n���P�`!2�K@�� � P�� ��q&���h����35����:�U����1�4���U!
���[��{fl%{�;��X�8��/\����t:!�`����z�?|�� �D P�ѿuR�"z�7,��; �0*M黛d�}iXl \<����$�U�l5���/L�0<�`ƃ�o�VՐ��ɳ���`8� `@��Ĩ�� ��":S0�,���{� R���0!�JƋ*�_���X�,���ɡ�ɝ�P�e�k<-܅�؅��Jiq@��{�qe�^Q. ,0?z��@\��j�u������� F:)[m���Kr����_񗌙���j<�)����Ӱl�X!L)���t���5�,,��{�#��l�|Ϭ��#,|�"?���w�1�|�������0񨡸�.�r`������`�Z��Ր+t�qp�!�5/����!�=��q`�Y� �`�!�fѴ^K=��O���s ��[Δ+�&������]Q�Dt@�g-Z~`��4ӹ2Ӕ��`�j��i�~��
�0 ��P���w�Wr �r TM�#��qpؖ(����� ���|c�e-���O���Mi|��#͸�P�)��p�@�P���ױ�$}����ojMعm�s  }�X�د��F*��k`���5���z�O2��z��݌���r0������ڧ۲m��]>����Y؃Pջ�q �,�,|BV�Y��K�.�ܢ�٣�O)��h=�p ����m��݈0ފp��M�������T؎=�%]�4͸k����j��_1־Bw�o~h�Vg}�[�
��
N� �`�@#l�N�MT���@�M�T]��4��q���\�@�&nٍ$Zf�oM��}4������ڭ��݄�n���A�qN!�`�t���<ߌ=����.�e��Q���Ң�;�\��;�/`���.އ��@��.��7��0�N�d����R��R�m���#��	+�`��!�֢��Н֍��`^��ښ��:�ڜ��uD���� 4�O�o����瀾�)����=�r	C&������ר�՝�h53>��D���i����^[�Bxו�RvX�� �����.X]҆��zN�p}�](|Ӡ"zG��4c�-}O|�O{�|` ��]�gn�:����Ѥ,�*wr�	 ��^[l���H��[)��$Ѿ�_>2���!�I�p}�հ�p�#��[�b��)����>g]� ~�s��6Ni��������d� 6}���yW���*��v�w���z$���p�cU��0��؛zr{r�f|}�S�傻j˻;����ۼѫ$������3៥�_��0P�S������H�`�~.Z��Q)a�Bc�$7��J��`S�#O�P��)��r d�����m�� �%�0V	�q�hb�qsTq��3� o�	�U�!w�`���;1�wrD�q�yH�(�����u�8p��!��ϟ����3��^�s���G�"Bg)jDx�<�&��aAp�	<�O�-s���gn�C�A�%Z��Q�I�.e��=]��h�'��X�nՠ!NWt:�u���.\H{�U���^�dZ�Ξ8�x���:��V���ā3GΜA�-D���F�%bD�����I�ܯ�=��z��vn_/r3��#hQ�N���%�'ng�f��*�s`B����5L�5V�)1��f�s���l��{��N>3m����)1�Hp�,&lM>�N������ �~m�o�>]�p@L�&�.��+8�"��� �+�F�.�`z��>  DF�i)��ij����k:��/�f�k0�[��� ������n�ɯ��bZmzv�������j�s��2"h��;ȸ{�4@%{ɩ���G��Ιm �H���r�j"���O7%e�R�p�	�pH=wfrǞq��N E��͙mB2ʎ:��6 ��THRK5�x��f�[m�	]�z��e�\���^�>HK;o5�j4��*�l�+?��c=��Ï�r,���||�2>O�4�������'6���3���.p��(;�z���s������H��v*.9(���"+;Bm�4�M$-�		!p��Q�r�N��K�!�xs����'<N-j�"���f�sn�^�	$F:��CƯ`�����u腵ؠ��� Xk�>�kE\�/e��k���Z�lS̐F����t��N��9��R�G�Q�����
3%��<%�����	(�Ô\&%c�7�/"��;����t=�8�:,2pZ�>(���u�r!�`��(sy1�)]bt�Y?���/��H~j��c=�:l9��1�Da���<��.'�@�~�����wg~⥗Kf�*��Ѕ�p�ꧏ=�@t���62�jd��E&��z�c4���I)v�C�Wz�k���78�!{�D"���D�0���������c�H�L�Qv�P�CdJ��x����#    �L�@y��L��ҏ}l��Ѕ4�K(0cD&@��R�g�`#�A�x�� ��� ���y{���-��-0��J
�#ʐ2��z�Q�r�b�G/x��!Q���d'�v�W0q�L$�|��j���@I_P�������C-���Z�ጀ(#(6��42��f5���Ll�H�;�4: B>�h�	�B��&{�qa��QXц)�9xՋr�&9�dg;��Nx���	p�<�v�?]yzTC}�%-��@t��&z	
��Q�\�D���������Ui�Eyw1� Mz�jeE,l["eh����ǔ��<Y$�;u%O}:Dp����\ˈ�u�5����b&��> �ʋj�0�> "��c&��3Zt3��((��ˉZ�����aG]x�Fq��IS��`�1.ti�!N�݃5\�6���dC�?elc��W .(шF9�R�`�(
���|8( ��3�~��4t�3�u3
�em})Q�Ӓ|�?�z���Z�y����q��L�5i�~��\{G����fW}���=D�O�\MXLtE��1�v��V�JPb�2��Mm&��U��׾�M�Eo��`��� ��p�U������A��![�����EJ��C��Q�\����I1�M\�yV�#�l[���}��[]�z�;��_�1k������cD��8Ư~��cY�F@�Ez@��� a:8o+�2_�[�:T�m}����~��!��p��Է��8(%n���|b<n�%�=�lԡ�^M�ö#�jc��	Hd���"})=d��Wg5cl!z�(#X��3pe�,��EF8�
�+<���p >�=�A����9Cy���(������eD(�:��2Y�P=���Ǳ��l���|�������<�L�}�3���� ��1/Ikh^*z���ä}�/BȬr���o #��@�t�!j�@ ���+]�96	f��}�k��}���2�a��aBu�er\)�W9 &��!�∢<b��턺�"���/���]�6Q�E0i�\$��х�l�%W��;��p�p��.��+�����5h��뇄λ�_�����E��ݩ��w��K鮞uY�[>h��2�Y�Ɗ��1�XOF'"�3�ϐ�>)H6걚�1$5���%���8G��)�NH�)��?��K��m�� �1S�&4�'_r�:���2���(s���ӓ��5Ɖ��%{����\��1���S�\��׏���eݶ���-�◾���7��"9�p'2�矉eA0@;iĹ<a
���yE������9�	gИ� �l�p��������#�x���=8�� 9�H6�� �m8)�<��p�jP�������c��H�=�0[�p�����(a��/!>嫈�@�q(=���1IB˾�پ3%_�>��t� 0�����$k�y��2#Q�4����#��@ЯL0CZ�*�B�2:0�2�+�ً>*D*�H�9H�B�!X��(�!�c2sp�p^a�-��J��e�������9�j�ܸ�sЗ�{����3�,��h���������ѩ�8q��ڛ$	�Í��X(�W��8	�h0�k�������	%�(S�C	�p샷2�Z��Y��w�%Yr�4좮�-j�C3��N�Ǹk(��LLP�[��a�:�h��.���!?H=h!FT����H`<"���	(ن���H�:������	6ue�E����A��E����x���F�F۠�ؚ��E!�� ��Z�[������Fo��s� ���ì�����>�����>�p�^�v�4���N#+��G3�/O�˼,�Lp(�ڃ�101���0�xH�<���Z�C8�D�Lo�!F�DNR3�[
Z�$���J	�����	M�aIɁ�����Ƭ�}�.����H�} (r��]pQ�x�7�6����&C�|�-��3��6<�N��L�?2L�yH㑰��&��X���QQ��$l�$�Č�|M�y�r�p:��zP��<���\+�l��!# ^�R�UP^p-P�(}��.�h�P�$�g�D�D���!l8��a&�� � Aj�O�Q�r� �M\�:HϤ腩dr83V�"���7J�nب���d#6r-��?�PVy�;���8�r��8�aR����#�������M�0��@��)�S;-Z`")����^��ː���!1�~�Z��r�/�1 Re����
�2�|��|LR�DPF��j8��^8KK�R�bR$:�SW}U�![�x"r��H9[�]�d�(��՛��7���RG+E�鱖�|�CXX�E�L�""[(V]���p�������Ur-W���Y�'{���gs��T,`}�
��$��cT���);�]����[#T�lȆ]��^ �l �z��U����2W�%�6���3�hU��Z��M2��ڇ�Ң�`TP�k�Q���Y�ņ%}#!�Ii%|@�{�~؅zM�(��ՍmZ;�X�*L*�X�v:��x�m�(b��n�Yl0&'-V4R"RXC-Z����;�t�N���$[�ʟ�ڷ�"z�*]���݇K���$�P�����ˀ����[�%�^���a,���j(�,{x�_e&!ڇn �~ȉ�(ɢ{�96��͘H�9��3�ə7;���]!�[�)[���\_�${�"ɏmH^��)z��dd�0z����b�ɋ �0�q���|��#�rM����
�p����H	�}_ԇ$䔒6F����U�x_݅Ut�A�:�3 ��¢W�����R@#��] �j`<�)�0'&�q�A���^X����'���������VpP�UEI�HE\���h\`�����A�ȩ��X�@Y�h�k�G	��9c�(��q��3���_�
� ��
{��M�.�5$.����I��I�Z�v!,1���0'�`�<��"	����ZИLA�Љ{�,�Јj�@� �]����@������6�����I~i��������
�^c-�${�\+��3�f��ݬ$$����	�8E2���41���������� N:���xFݠdݻ��[�N9��MdUJ�����ؔ��b��@��'f	��P^���x1}�����k�+i@`+����l=�u�r �`� ���rގ��l���mB�d1)��S��K��`�� %���y	�UiL=�XB�P͓@W ���$������Ւ��:v���X��]�{����(��%�2u�!�j� i��9�>��c���$�$l\���P�W �����vY\���*�j�֙�Qk���� T~ �7����k�n'���x2��؅��%Ɉ��m�x��S�k�~l"[ lxڇs��)!�b��R�l���1� ک{�)p ��V~���P�]mٞmq�^��$z�e8�G"a�]�]�$��m�.n��gxz�{Ї�P� �jkޡ6n���B��n��n��WV��v�ˀ����.���n�֮�*�s��������q�(~W���,��)���o��nX�А�
���@�9��|W<������p ��+�:�[�
��:��Pp�8Z�@�|�n o�q"�b�xPw���֐�����(���L"=��qY(B��hJ����A$t��Ĕ���@�s��hE^&�����Ag���T6+�KT�0�9_svJ����'�eP�ݞJ��ۘ���N�5�I��y�8=�{p퓉��˓B)	��f���=ZD����h� �F�3�sȆ�d��cƅ�fsU'�X��xE9?p-����c�����,_l��Jn��\�J��^eν�m=B���J#,��X	�P����!t��(=r����5Q�Uw���3\���s� ��f7�N������r��_�^�S��x�ԛ���f�S�9�`�����	^��XY��1r�xz��vU/A�voQV��z�o¹p�?�U�d���_��!VEj��y�y��y��y��y��y�/�~��d�ݰ�z���}��ZJ��^z��z��z��z��z��z��z�{�{�/��ly�O����+Nq��'j3"8S{���~>�����!�{>j���Y��Q��0j٘3�H��o��8���<<a	n��p��Mԓ���j��֡z=\�q���3x�`|�_�9�֨��	6q�*��:y6i#Ŕ���gm��o���Y��?~S	Ɓ�r��Ր�j�6�cW���=��a�fB���P>jʳrA~�'	,q�������m�-�M�w�IQ�e����LQ� 6J� ��8q�����o!ÆB�(q"Ŋ/b̨q#ǎ?�)r$ɒ�1����}�z����z,�i��G��L�9m.�Iʢ���;x�8�LB�*u*ժV�bͪ�>��m+v,ٲfϢM���ڶn�+w.ݺv��ͫw/߾~�,x0�#N�x1�Ǝ�����ǖ/cάy3�Ξ?�-z4�ҦO�N�z5�֮_Î-{6�ڶo�έ{7ﺽl5�-|�k|�b��Ռ8����# �Y���cϾ��5 `���]F|�"�ծ~��{7�ł�C|���s�l|������?��sN~���P�ރ�?焃>���;	����!�2B/��B }���~� �;���;�\�M80�؋�L��᳟2��1�(�2n���uǂ-#x�P6���;�d��;��2�e�O���Є?�������ٴ�JK�YgD��b ��8i�������":���2/�O����ȌWO/��:A�Ö��~�O/��3a8�t�;��#'y�dyd8?�5��T�'��(�ˊ2"	��z��u)�����֬��Z��� ;FLIS    A      ����        ����FCIS               �                 �
