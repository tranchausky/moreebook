A Song of Ice and Fire series by George R. R. Martin

Formats: PDF, EPUB & MOBI

Book 1:	Game of Thrones (1996)
Book2:	A Clash of Kings (1998)
Book 3:	A Storm of Swords (2000)
Book 4:	A Feast For Crows (2005)
Book 5:	A Dance With Dragons (2011)

Book 6 expected by 2015

Sometimes during file conversion some minor corruptions of content are visible due to varied fonts and formatting.
ALL conversions were based on a Generic e-ink format as there are countless readers these days. 
Usually this doesn't become a hinderance, however, if you load the files to your e-reader and you have unreadable 
files, I suggest you try converting from the source PDF using a document converter of your choice. I have not read 
ALL file type copies of these books but did a brief check and couldn't find any major flaws. Doubtless character
corruption may exist in some files. To convert to a specific reader format I suggest using Calibre Reader which has 
many reader types built in and will also allow you to read or check from you PC.

Enjoy! 

- Gryff 